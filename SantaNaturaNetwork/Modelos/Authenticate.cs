﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Authenticate
    {
        public Authenticate(string accessKey, string idService, string dateRequest, string hashString)
        {
            this.accessKey = accessKey;
            this.idService = idService;
            this.dateRequest = dateRequest;
            this.hashString = hashString;
        }

        public string accessKey { get; set; }
        public string idService { get; set; }
        public string dateRequest { get; set; }
        public string hashString { get; set; }

        public class Data
        {
            public string currency { get; set; }
            public decimal amount { get; set; }
            public string transactionCode { get; set; }
            public string adminEmail { get; set; }
            public string dateExpiry { get; set; }
            public string paymentConcept { get; set; }
            public string additionalData { get; set; }
            public string userEmail { get; set; }
            public string userId { get; set; }
            public string userName { get; set; }
            public string userLastName { get; set; }
            public string userUbigeo { get; set; }
            public string userCountry { get; set; }
            public string userDocumentType { get; set; }
            public string userDocumentNumber { get; set; }
            public string userCodeCountry { get; set; }
            public int serviceID { get; set; }
        }
    }
}
