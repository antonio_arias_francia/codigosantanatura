﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ReporteCDRModelo
    {
        public ReporteCDRModelo() { }

        public string Idop { get; set; }
        public string Establecimiento { get; set; }
        public string Fecha { get; set; }
        public string NombreProducto { get; set; }
        public int CantidadProducto { get; set; }
        public double PrecioCDR { get; set; }
        public double PuntosCDR { get; set; }
        public double PuntosTotal { get; set; }
        public double MontoTotal { get; set; }
    }
}
