﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Producto
    {
        public Producto()
        {

        }

        public Producto(string codigo, string nombreProducto, string unidadMedida, string presentacion, int unidadPresentacion, string descripcion, string linea, double precioUnitario, string foto, bool estado, double puntos, string idProdPeruShop)
        {
            Codigo = codigo;
            NombreProducto = nombreProducto;
            UnidadMedida = unidadMedida;
            Presentacion = presentacion;
            UnidadPresentacion = unidadPresentacion;
            Descripcion = descripcion;
            Linea = linea;
            PrecioUnitario = precioUnitario;
            Foto = foto;
            Estado = estado;
            Puntos = puntos;
            IdProdPeruShop = idProdPeruShop;
        }

        public string Codigo { get; set; }
        public string NombreProducto { get; set; }
        public string UnidadMedida { get; set; }
        public string Presentacion { get; set; }
        public int UnidadPresentacion { get; set; }
        public string Descripcion { get; set; }
        public string Linea { get; set; }
        public double PrecioUnitario { get; set; }
        public double IncrementoPVP { get; set; }
        public string Foto { get; set; }
        public bool Estado { get; set; }
        public double Puntos { get; set; }
        public double PuntosPromocion { get; set; }
        public double Corazones { get; set; }
        public string IdProdPeruShop { get; set; }
        public string idPaquete { get; set; }
        public string beneficios { get; set; }
        public string ingredientes { get; set; }
        public string consumo { get; set; }

    }
}
