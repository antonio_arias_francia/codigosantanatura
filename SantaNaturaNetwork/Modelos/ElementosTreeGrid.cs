﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ElementosTreeGrid
    {
        public ElementosTreeGrid() { }

        public long EmployeeID { get; set; }
        public long ReportsTo { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Title { get; set; }
        public string Rango { get; set; }
        public string Fecha { get; set; }
        public string Telefono { get; set; }
        public string Pais { get; set; }
        public string VR { get; set; }
        public string PP { get; set; }
        public string VP { get; set; }
        public string VG { get; set; }
        public string VQ { get; set; }
        public string VIP { get; set; }
        public string MAXIMORANGO { get; set; }
        public string IDRANGO { get; set; }
        public string TIPOC { get; set; }
        public string PAQUETE { get; set; }
        public string RED { get; set; }
        public string IDSOCIO { get; set; }
        public string IDUPLINE { get; set; }
        public string DOCUMENTO { get; set; }
        public string UPLINE { get; set; }
        public string NOMBRES { get; set; }
        public string CORAZONES { get; set; }
    }
}
