﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class DatosPedidosVisaNet
    {
        public string Despacho { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string IdPedido { get; set; }
        public string NumTarjeta { get; set; }
        public string FechaHora { get; set; }
        public string Importe { get; set; }
        public string Moneda { get; set; }
        public string TarjetaHabiente { get; set; }
        public string CodigoAccion { get; set; }
        public string Ticket { get; set; }
        public List<ListProductos> Productos { get; set; }

        public class ListProductos
        {
            public string Descripcion { get; set; }
        }
    }
}
