﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Compra
    {

        public Compra()
        {

        }

        //public Compra(string codidgo)
        //{
        //    Codidgo = codidgo;
        //}

        public Compra(string ticket, string idopPeruShop, string codCliente, int cantidad, double montoTotal,
                    double puntosTotal, double montoAPagar, DateTime fechaPago, int estado, string fotoVaucher,
                    double depositoTotal, double depositoFraccionado, DateTime fechaDeposito, string despacho)
        {
            Ticket = ticket;
            IdopPeruShop = idopPeruShop;
            CodCliente = codCliente;
            Cantidad = cantidad;
            MontoTotal = montoTotal;
            PuntosTotal = puntosTotal;
            MontoAPagar = montoAPagar;
            FechaPago = fechaPago;
            Estado = estado;
            FotoVaucher = fotoVaucher;
            DepositoTotal = depositoTotal;
            DepositoFraccionado = depositoFraccionado;
            FechaDeposito = fechaDeposito;
            Despacho = despacho;
        }

        public string Ticket { get; set; }
        public string IdopPeruShop { get; set; }
        public string IdProductoPais { get; set; }
        public string IdMove { get; set; }
        public string CodCliente { get; set; }
        public int Cantidad { get; set; }
        public double MontoTotal { get; set; }
        public double Corazones { get; set; }
        public double PuntosTotal { get; set; }
        public double PuntosTotalPromo { get; set; }
        public double MontoAPagar { get; set; }
        public DateTime FechaPago { get; set; }
        public string FechaPago2 { get; set; } //Charles 24/03/2021
        public string FechaPagoReporte { get; set; }
        public DateTime FechaComision { get; set; }
        public int Estado { get; set; }
        public string FotoVaucher { get; set; }
        public string TipoPago { get; set; }
        public string TipoCompra { get; set; }
        public string idTipoCompra { get; set; }
        public double DepositoTotal { get; set; }
        public double DepositoFraccionado { get; set; }
        public DateTime FechaDeposito { get; set; }
        public DateTime FechaStock { get; set; }
        public string Despacho { get; set; }
        public string DespachoVoucher { get; set; }
        public string NombreCliente { get; set; }
        public string DireccionCliente { get; set; }
        public string ApellidoPat { get; set; }
        public string ApellidoMat { get; set; }
        public string DNICliente { get; set; }
        public string DNIPatrocinador { get; set; }
        public string DNIFactorComision { get; set; }
        public string DNIDespacho { get; set; }
        public string IdPromo { get; set; }
        public string IdPago { get; set; }
        public string Transaction_Date { get; set; }
        public string Merchant { get; set; }
        public string Id_Unico { get; set; }
        public string Transaction_ID { get; set; }
        public string CardS { get; set; }
        public string AMOUNT { get; set; }
        public string CURRENCY { get; set; }
        public string BRAND { get; set; }
        public string STATUSS { get; set; }
        public string ACTION_DESCRIPTION { get; set; }
        public string Aauthorization_Code { get; set; }
        public double montoComision { get; set; }
        public double montoComisionCI { get; set; }
        public string fecha1 { get; set; }
        public string fecha2 { get; set; }
        public string idPaquete { get; set; }
        public string PaqueteSocio { get; set; }
        public string fechaSimplex { get; set; }
        public string idProductoPS { get; set; }
        public string tipoCliente { get; set; }
        public string Upline { get; set; }
        public int cantPromo { get; set; }
        public int cantRegalo { get; set; }
        public double puntosGastados { get; set; }
        public string descuento { get; set; }
        public string NotaDelivery { get; set; }
        public string NotaPS { get; set; }
        public string FechaExpiracion { get; set; }
        public string FechaCreacion { get; set; }
        public string FechaPagada { get; set; }
        public string CIP { get; set; }
        public string NumOperacion { get; set; }
        public string Comprobante { get; set; }
        public string IdCliente { get; set; }
        public string Ruc { get; set; }
        public int IDP { get; set; }

        public NuevoCIP nuevoCIP { get; set; }
        public Facturacion facturacion { get; set; }
        public PendienteFacturacion pendienteFacturacion { get; set; }

        public int IdVoucher { get; set; }
        public string Ruta { get; set; }
        public string Usuario { get; set; }

        public class NuevoCIP
        {
            public string Documento { get; set; }
            public string Correo { get; set; }
            public string IdCliente { get; set; }
            public string Nombres { get; set; }
            public string Apellidos { get; set; }
            public decimal MontoAPagar { get; set; }
        }

        public class Facturacion
        {
            public int Id_Datos { get; set; }
            public int IdPeriodo { get; set; }
            public string NombrePeriodo { get; set; }
            public string IdCliente { get; set; }
            public string TipoCliente { get; set; }
            public string RUC { get; set; }
            public string Archivo { get; set; }
            public string DniCliente { get; set; }
            public string Nombres { get; set; }
            public decimal Comision { get; set; }
            public decimal SaldoDisponible { get; set; }
            public DateTime FechaRegistro { get; set; }
            public string Observacion { get; set; }
            public decimal MontoCanje { get; set; }
            public bool SolicitudCanje { get; set; }
            public string SolicitudCanjeCombo { get; set; }
            public string EstadoCanje { get; set; }
            public string EstadoSaldoDisponible { get; set; }
            public string FechaS { get; set; }
            public string Tickets { get; set; }
            public string Correcion { get; set; }
            public bool Sellado { get; set; }
        }

        public class PendienteFacturacion
        {
            public string IdCliente { get; set; }
            public string DniCliente { get; set; }
            public string Nombres { get; set; }
            public string Celular { get; set; }
            public string Correo { get; set; }
            public int IdPeriodo { get; set; }
        }

    }
}
