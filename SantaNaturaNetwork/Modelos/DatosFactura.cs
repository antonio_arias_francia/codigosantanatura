﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class DatosFactura
    {
        public rucC consultaRuc { get; set; }
        public WS recepcionDatos { get; set; }
        public ErrorWs errorWS { get; set; }

        public class ErrorWs
        {
            public bool success { get; set; }
            public string result { get; set; }
        }

        public class WS
        {
            public bool success { get; set; }
            public resultado result { get; set; }

            public class resultado
            {
                public string ruc { get; set; }
                public string razonSocial { get; set; }
                public string estado { get; set; }
                public string condicion { get; set; }
                public string direccion { get; set; }
                public string departamento { get; set; }
                public string provincia { get; set; }
                public string distrito { get; set; }
            }
        }

        public class rucC
        {
            public rucC(string ruc)
            {
                this.ruc = ruc;
            }

            public string ruc { get; set; }
        }
    }
}
