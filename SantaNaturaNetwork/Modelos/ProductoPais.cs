﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ProductoPais
    {
        public string Pais { get; set; }
        public double IncrementoPVP { get; set; }
        public string Foto { get; set; }
        public string Nombre { get; set; }
        public bool Estado { get; set; }
        public string IdProductoPeruShop { get; set; }
        public string IdProductoxPais { get; set; }
        public string IdProducto { get; set; }
        public string Beneficios { get; set; }
        public string Ingredientes { get; set; }
        public string Consumo { get; set; }
        public ProductoPais() { }

        public class Linea_PrecioCDR
        {
            public double PrecioCDR { get; set; }
            public string idLinea { get; set; }
            public string idProductoPais { get; set; }
        }
    }
}
