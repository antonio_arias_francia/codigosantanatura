﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Promocion
    {
        public string id_promo { get; set; }
        public string nom_promo { get; set; }
        public string rango { get; set; }
        public string rangofin { get; set; }
        public bool estado_gen { get; set; }
        public int puntos1 { get; set; }
        public string producto1 { get; set; }
        public bool estado1 { get; set; }
        public int puntos2 { get; set; }
        public string producto2 { get; set; }
        public bool estado2 { get; set; }
        public int puntos3 { get; set; }
        public string producto3 { get; set; }
        public bool estado3 { get; set; }
        public int puntos4 { get; set; }
        public string producto4 { get; set; }
        public bool estado4 { get; set; }
        public int punt_basico { get; set; }
        public string prod_basico { get; set; }
        public bool esta_basico { get; set; }
        public int punt_prof { get; set; }
        public string prod_prof { get; set; }
        public bool esta_prof { get; set; }
        public int punt_emp { get; set; }
        public string prod_emp { get; set; }
        public bool esta_emp { get; set; }
        public int punt_mill { get; set; }
        public string prod_mill { get; set; }
        public bool esta_mill { get; set; }
        public int punt_con { get; set; }
        public string prod_con { get; set; }
        public bool esta_con { get; set; }
        public int punt_ci { get; set; }
        public string prod_ci { get; set; }
        public bool esta_ci { get; set; }
        public int cantidad { get; set; }
        public bool pxc1 { get; set; }
        public bool pxc2 { get; set; }
        public bool pxc3 { get; set; }
        public bool pxc4 { get; set; }
        public bool regalo1 { get; set; }
        public bool regalo2 { get; set; }
        public bool regalo3 { get; set; }
        public bool regalo4 { get; set; }
        public bool regBasico1 { get; set; }
        public bool regProfe1 { get; set; }
        public bool regEmpre1 { get; set; }
        public bool regMillo1 { get; set; }
        public bool regBasico2 { get; set; }
        public bool regProfe2 { get; set; }
        public bool regEmpre2 { get; set; }
        public bool regMillo2 { get; set; }
        public int punt_basico2 { get; set; }
        public int punt_prof2 { get; set; }
        public int punt_emp2 { get; set; }
        public int punt_mill2 { get; set; }
        public string prod_basico2 { get; set; }
        public string prod_prof2 { get; set; }
        public string prod_emp2 { get; set; }
        public string prod_mill2 { get; set; }
        public bool esta_basico2 { get; set; }
        public bool esta_prof2 { get; set; }
        public bool esta_emp2 { get; set; }
        public bool esta_mill2 { get; set; }
    }
}
