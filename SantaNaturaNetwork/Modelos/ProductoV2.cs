﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ProductoV2
    {
        public string IdProducto { get; set; }
        public string ProductoPais { get; set; }
        public string UnidadMedida { get; set; }
        public string Contenido { get; set; }
        public string IdPresentacion { get; set; }
        public string NombrePresen { get; set; }
        public int UnidadPresentacion { get; set; }
        public string Descripcion { get; set; }
        public string Linea { get; set; }
        public string NombreLinea { get; set; }
        public string NombreProducto { get; set; }
        public string Foto { get; set; }
        public string Pais { get; set; }
        public decimal PrecioUnitario { get; set; }
        public double IncrementoPVP { get; set; }
        public decimal Puntos { get; set; }
        public decimal PuntosPromocion { get; set; }
        public decimal Corazones { get; set; }
        public string LineaCDR { get; set; }
        public bool Estado { get; set; }
        public bool Promocion { get; set; }
        public string IdProductoPeruShop { get; set; }
        public string Paquete { get; set; }
        public decimal precioCDR { get; set; }
        public string Beneficios { get; set; }
        public string Ingredientes { get; set; }
        public string Consumo { get; set; }

        public class RetornarDatosInicio
        {
            public double montoTotalPagar { get; set; }
            public double puntosTotal { get; set; }
            public string RUC { get; set; }
            public int PreRegistro { get; set; }
        }

        public class AgregarCarrito
        {
            public string CantidadExistente { get; set; }
            public bool Estado { get; set; }
        }

        public class RetornarDatosCarrito
        {
            public string Total { get; set; }
            public string Monto { get; set; }
            public string Puntos { get; set; }
            public string PuntosPromo { get; set; }
            public string SubTotalProd { get; set; }
            public string SubTotalPunt { get; set; }
            public string Mensaje { get; set; }
            public string URL { get; set; }
        }

        public ProductoV2() { }

        public ProductoV2(string idProducto, string productoPais, string unidadMedida, string contenido, string idPresentacion, string nombrePresen,
            int unidadPresentacion, string descripcion, string linea, string nombreLinea, string nombreProducto, string foto, string pais, decimal precioUnitario,
            double incrementoPVP, int puntos, bool estado, string idproductoperushop)
        {
            IdProducto = idProducto;
            ProductoPais = productoPais;
            UnidadMedida = unidadMedida;
            Contenido = contenido;
            IdPresentacion = idPresentacion;
            NombrePresen = nombrePresen;
            UnidadPresentacion = unidadPresentacion;
            Descripcion = descripcion;
            Linea = linea;
            NombreLinea = nombreLinea;
            NombreProducto = nombreProducto;
            Foto = foto;
            Pais = pais;
            PrecioUnitario = precioUnitario;
            IncrementoPVP = incrementoPVP;
            Puntos = puntos;
            Estado = estado;
            IdProductoPeruShop = idproductoperushop;
        }
    }
}
