﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Packete
    {
        public string Codigo { get; set; }
        public string _Packete { get; set; }
        public string Descuento { get; set; }
        public int Puntos { get; set; }
        public double Monto { get; set; }
        public string fechaDescuento { get; set; }

        public Packete() { }

        public Packete(string codigo, string Packete, string descuento, int puntos)
        {
            Codigo = codigo;
            _Packete = Packete;
            Descuento = descuento;
            Puntos = puntos;
        }
    }
}
