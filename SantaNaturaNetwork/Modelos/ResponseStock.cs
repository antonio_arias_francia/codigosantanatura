﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ResponseStock
    {
        public ResponseStock(string establecimiento, bool stock, List<string> productos)
        {
            Establecimiento = establecimiento;
            Stock = stock;
            Productos = productos;
        }

        public ResponseStock()
        {

        }

        public string Establecimiento { get; set; }
        public bool Stock { get; set; }
        public List<string> Productos { get; set; }
    }
}
