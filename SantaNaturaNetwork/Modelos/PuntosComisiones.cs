﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class PuntosComisiones
    {
        public PuntosComisiones() { }

        public int Orden { get; set; }
        public int CanDirectos { get; set; }
        public int CanActivos { get; set; }
        public int canSuperiorIM { get; set; }
        public int canSuperiorTDM { get; set; }
        public int canSuperiorDDM { get; set; }
        public int canSuperiorDM { get; set; }
        public int canSuperiorTD { get; set; }
        public int canSuperiorDD { get; set; }
        public int canSuperiorD { get; set; }
        public int canSuperiorR { get; set; }
        public int canSuperiorZ { get; set; }
        public int canSuperiorO { get; set; }
        public int canSuperiorP { get; set; }
        public int canSuperiorB { get; set; }
        public double PuntosPersonales { get; set; }
        public double ComisionTotal { get; set; }
        public double PuntosPersonalesExtra { get; set; }
        public double VolumenRed { get; set; }
        public double VolumenPersonal { get; set; }
        public double VolumenGeneral { get; set; }
        public double VQ { get; set; }
        public double VIP { get; set; }
        public string NombreSocio { get; set; }
        public string NombreUpline { get; set; }
        public string NombrePatrocinador { get; set; }
        public string RangoDefinido { get; set; }
        public string PuntosPersonalesDirectos { get; set; }
        public string dni { get; set; }
        public string Celular { get; set; }
        public string CDR { get; set; }
        public string Premio { get; set; }
        public CalculoPuntos calculoPuntos { get; set; }
        public CantidadRangoMapaRED cantidadRangoMapaRED { get; set; }

        public class DatosIndividualRango
        {
            public int IdCalculo { get; set; }
            public int IdPeriodo { get; set; }
            public string Documento { get; set; }
        }

        public class RecalculoIndividual
        {
            public bool Estado { get; set; }
            public double VQ { get; set; }
            public string Rango { get; set; }
            public int IdRango { get; set; }
        }

        public class CalculoPuntos
        {
            public string UPLINE { get; set; }
            public string IDUPLINE { get; set; }
            public int IDPERIODO { get; set; }
        }

        public class CantidadRangoMapaRED
        {
            public int canSuperiorIM { get; set; }
            public int canSuperiorTDM { get; set; }
            public int canSuperiorDDM { get; set; }
            public int canSuperiorDM { get; set; }
            public int canSuperiorTD { get; set; }
            public int canSuperiorDD { get; set; }
            public int canSuperiorD { get; set; }
            public int canSuperiorR { get; set; }
            public int canSuperiorZ { get; set; }
            public int canSuperiorO { get; set; }
            public int canSuperiorP { get; set; }
            public int canSuperiorB { get; set; }
        }
    }
}
