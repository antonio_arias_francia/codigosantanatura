﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Transaccion
    {
        public Transaccion(string amount, int? antifraud, string channel, int? recurrenceMaxAmount)
        {
            this.amount = amount;
            this.antifraud = antifraud;
            this.channel = channel;
            this.recurrenceMaxAmount = recurrenceMaxAmount;
        }
        public string amount { get; set; }
        public int? antifraud { get; set; }
        public string channel { get; set; }
        public int? recurrenceMaxAmount { get; set; }

        public class DatosVisaNet
        {
            public string Transaction_Date { get; set; }
            public string Merchant { get; set; }
            public string Id_Unico { get; set; }
            public string Transaction_ID { get; set; }
            public string CardS { get; set; }
            public string Aauthorization_Code { get; set; }
            public string AMOUNT { get; set; }
            public string CURRENCY { get; set; }
            public string BRAND { get; set; }
            public string STATUSS { get; set; }
            public string ACTION_DESCRIPTION { get; set; }
        }

        public class sessionToken
        {
            public string sessionKey { get; set; }
            public string expirationTime { get; set; }
        }

        public class ErrorResponse
        {

            public string errorCode { get; set; }
            public string errorMessage { get; set; }

            public Data data { get; set; }


            public class Data
            {
                public string CURRENCY { get; set; }
                public string TRANSACTION_DATE { get; set; }
                public string TERMINAL { get; set; }
                public string ACTION_CODE { get; set; }
                public string TRACE_NUMBER { get; set; }
                public string ECI_DESCRIPTION { get; set; }
                public string ECI { get; set; }
                public string CARD { get; set; }
                public string MERCHANT { get; set; }
                public string STATUS { get; set; }
                public string ADQUIRENTE { get; set; }
                public string ACTION_DESCRIPTION { get; set; }
                public string AMOUNT { get; set; }
                public string PROCESS_CODE { get; set; }
            }

        }

        public class InfoUserByIdMerchant
        {

            public Header header { get; set; }
            public Fulfillment fulfillment { get; set; }
            public Order order { get; set; }
            public Token token { get; set; }
            public DataMap dataMap { get; set; }

            public class Header
            {
                public string ecoreTransactionUUID { get; set; }
                public string ecoreTransactionDate { get; set; }
                public string millis { get; set; }
            }

            public class Fulfillment
            {
                public string channel { get; set; }
                public string merchantId { get; set; }
                public string terminalId { get; set; }
                public string captureType { get; set; }
                public bool countable { get; set; }
                public bool fastPayment { get; set; }
                public string signature { get; set; }
            }

            public class Order
            {
                public string tokenId { get; set; }
                public string purchaseNumber { get; set; }
                public double amount { get; set; }
                public string currency { get; set; }
                public double authorizedAmount { get; set; }
                public string authorizationCode { get; set; }
                public string actionCode { get; set; }
                public string traceNumber { get; set; }
                public string transactionDate { get; set; }
                public string transactionId { get; set; }
            }

            public class Token
            {
                public string tokenId { get; set; }
                public string ownerId { get; set; }
                public string expireOn { get; set; }
            }

            public class DataMap
            {
                public string CURRENCY { get; set; }
                public string TERMINAL { get; set; }
                public string TRANSACTION_DATE { get; set; }
                public string ACTION_CODE { get; set; }
                public string TRACE_NUMBER { get; set; }
                public string CARD_TOKEN { get; set; }
                public string ECI_DESCRIPTION { get; set; }
                public string ECI { get; set; }
                public string SIGNATURE { get; set; }
                public string CARD { get; set; }
                public string MERCHANT { get; set; }
                public string BRAND { get; set; }
                public string STATUS { get; set; }
                public string ACTION_DESCRIPTION { get; set; }
                public string ADQUIRENTE { get; set; }
                public string ID_UNICO { get; set; }
                public string AMOUNT { get; set; }
                public string PROCESS_CODE { get; set; }
                public string VAULT_BLOCK { get; set; }
                public string TRANSACTION_ID { get; set; }
                public string AUTHORIZATION_CODE { get; set; }
            }

        }

        public class infoByUserMerchantId
        {

            public infoByUserMerchantId(int? antifraud, string captureType, string channel, bool countable, Order order)
            {
                this.antifraud = antifraud;
                this.captureType = captureType;
                this.channel = channel;
                this.countable = countable;
                this.order = order;
            }
            public int? antifraud { get; set; }
            public string captureType { get; set; }
            public string channel { get; set; }
            public bool countable { get; set; }

            public Order order { get; set; }

            public class Order
            {
                public Order(string amount, string tokenId, string purchaseNumber, string currency)
                {
                    this.amount = amount;
                    this.tokenId = tokenId;
                    this.purchaseNumber = purchaseNumber;
                    this.currency = currency;
                }

                public string amount { get; set; }
                public string tokenId { get; set; }
                public string purchaseNumber { get; set; }
                public string currency { get; set; }
            }

        }
    }
}
