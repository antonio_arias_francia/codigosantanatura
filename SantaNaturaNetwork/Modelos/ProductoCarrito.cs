﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ProductoCarrito
    {
        public ProductoCarrito()
        {

        }

        public ProductoCarrito(string codigo, string nombreProducto, string descripcion, string linea, double precioUnitario,
            string foto, double puntos, int cantidad, double subTotalNeto, double subTotal, double subTotalPuntos,
            string idProdPeruShop, string codValidar)
        {
            Codigo = codigo;
            NombreProducto = nombreProducto;
            Descripcion = descripcion;
            Linea = linea;
            PrecioUnitario = precioUnitario;
            Foto = foto;
            Puntos = puntos;
            Cantidad = cantidad;
            SubTotalNeto = subTotalNeto;
            SubTotal = subTotal;
            SubTotalPuntos = subTotalPuntos;
            IdProdPeruShop = idProdPeruShop;
            CodigoValidar = codValidar;
        }

        public string Codigo { get; set; }
        public string CodigoValidar { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public string Linea { get; set; }
        public double PrecioUnitario { get; set; }
        public string Foto { get; set; }
        public double Puntos { get; set; }
        public double PuntosPromocion { get; set; }
        public double Corazones { get; set; }
        public double PuntosDetalle { get; set; }
        public int Cantidad { get; set; }
        public double SubTotalNeto { get; set; }
        public double SubTotal { get; set; }
        public double SubTotalPuntos { get; set; }
        public double SubTotalCorazones { get; set; }
        public double SubTotalPuntosPromocion { get; set; }
        public string IdProdPeruShop { get; set; }
        public int cantidadPeruShop { get; set; }
        public double montoSend { get; set; }
        public string precioStr { get; set; }
        public string idPaquete { get; set; }
    }
}
