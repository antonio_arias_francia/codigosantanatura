﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class LimiteDias
    {
        public LimiteDias() { }

        public int idLimite { get; set; }
        public int numeroDias { get; set; }
    }
}
