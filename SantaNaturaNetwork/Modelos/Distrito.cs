﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Distrito
    {
        public string Codigo { get; set; }
        public string CodigoProvincia { get; set; }
        public string Nombre { get; set; }

        public Distrito() { }

        public Distrito(string codigo, string codigoProvincia, string nombre)
        {
            Codigo = codigo;
            CodigoProvincia = codigoProvincia;
            Nombre = nombre;
        }
    }
}
