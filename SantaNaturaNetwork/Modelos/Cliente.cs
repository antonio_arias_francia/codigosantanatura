﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Cliente
    {
        public string idCliente { get; set; }
        public string idClienteUpline { get; set; }
        public string usuario { get; set; }
        public string clave { get; set; }
        public string nombre { get; set; }
        public string apellidoPat { get; set; }
        public string apellidoMat { get; set; }
        public string apodo { get; set; }
        public string fechaNac { get; set; }
        public string sexo { get; set; }
        public string tipoDoc { get; set; }
        public string numeroDoc { get; set; }
        public string correo { get; set; }
        public string telefono { get; set; }
        public string celular { get; set; }
        public string pais { get; set; }
        public string paisTienda { get; set; }
        public string idpais { get; set; }
        public string departamento { get; set; }
        public string iddepartamento { get; set; }
        public string provincia { get; set; }
        public string idprovincia { get; set; }
        public string ditrito { get; set; }
        public string idditrito { get; set; }
        public string referencia { get; set; }
        public string direccion { get; set; }
        public string nroCtaDetraccion { get; set; }
        public string ruc { get; set; }
        public string nombreBanco { get; set; }
        public string nroCtaDeposito { get; set; }
        public string nroCtaInterbancaria { get; set; }
        public string tipoCliente { get; set; }
        public string idtipoCliente { get; set; }
        public string patrocinador { get; set; }
        public string idpatrocinador { get; set; }
        public string tipoEstablecimiento { get; set; }
        public string idtipoEstablecimiento { get; set; }
        public string idtipoEstablecimientoPremio { get; set; }

        public string correo_encrip { get; set; }
        public string celular_encrip { get; set; }


        public string upline { get; set; }
        public string idupline { get; set; }
        public string imagen { get; set; }

        public string Packete { get; set; }
        public string PacketePatrocinador { get; set; }
        public string Establecimiento { get; set; }
        public string CDRPremio { get; set; }
        public string Estado { get; set; }
        public string FechaRegistro { get; set; }
        public string IdPeruShop { get; set; }
        public int IdPackete { get; set; }
        public string FechaPackete { get; set; }
        public string idSimplex { get; set; }
        public string factorComision { get; set; }
        public string apellidos_completos { get; set; }
        public string rango { get; set; }
        public string rangoProximo { get; set; }
        public string VQProximo { get; set; }
        public int idRango { get; set; }
        public int Tiburon { get; set; }
        public decimal comision { get; set; }
        public double PP { get; set; }
        public double VPR { get; set; }
        public double VP { get; set; }
        public double VG { get; set; }
        public double VQ { get; set; }
        public double VIP { get; set; }
        public string regalo { get; set; }
        public string CodigoPostal { get; set; }
        public int cantRegalo { get; set; }
        public int PreRegistro { get; set; }
        public CDR CDRLinea { get; set; }
        public FacturacionAutomatica Facturacion { get; set; }
        public DatosFacturaPS DatosFactura { get; set; }
         public Compresion compresion { get; set; }

        public string Pregunta { get; set; }
        public string Respuesta { get; set; }

        public class RetornarDatosCombo
        {
            public double MontoPago { get; set; }
            public string IdPaquete { get; set; }
            public string TipoCliente { get; set; }
        } 

        public class RetornarDatosAfiliacion
        {
            public string IdCliente { get; set; }
            public bool Estado { get; set; }
            public string Mensaje { get; set; }
        }

        public class ClientePreRegistro
        {
            public string FechaRegistro { get; set; }
            public string Documento { get; set; }
            public string Nombres { get; set; }
            public string Apellidos { get; set; }
            public string Nombres_Upline { get; set; }
            public string IdPaquete { get; set; }
            public string Paquete { get; set; }
            public string IdCliente { get; set; }
            public string CboPreregistro { get; set; }
            public string TipoCliente { get; set; }
        }

        public class RecalculoPuntos
        {
            public double PuntosReales { get; set; }
            public double PuntosPromo { get; set; }
            public string Documento { get; set; }
            public string Factor { get; set; }
            public string Patrocinador { get; set; }
            public string Paquete { get; set; }
            public int IDP_PP { get; set; }
            public DateTime FechaPago { get; set; }
        }

        public class LineaCalificada
        {
            public int Cantidad { get; set; }
            public string Rango { get; set; }
        }

        public class FacturacionAutomatica
        {
            public int IdGeneral { get; set; }
            public DateTime Fecha { get; set; }
            public string Documento { get; set; }
            public string IdRuc { get; set; }
        }

        public class DatosFacturaPS
        {
            public string Nombre { get; set; }
            public string Direccion { get; set; }
            public string Mail { get; set; }
        }

        public class Compresion
        {
            public string IdCliente { get; set; }
            public string Documento { get; set; }
            public string TipoCliente { get; set; }
            public Directos directos { get; set; }
            public DocumentoUP documentoUP { get; set; }
            
            public class Directos
            {
                public string IdCliente { get; set; }
                public string Documento { get; set; }
                public string TipoCliente { get; set; }
            }

            public class DocumentoUP
            {
                public string Upline { get; set; }
                public string Patrocinador { get; set; }
                public string FactorComision { get; set; }
                public string IdUpline { get; set; }
                public string IdPatrocinador { get; set; }
                public string IdFactorComision { get; set; }
            }
        }

        public class CDR
        {
            public int IdLineaCDR { get; set; }
            public string IdLinea { get; set; }
            public string NombreLinea { get; set; }
            public string CDRPS { get; set; }
            public string Apodo { get; set; }
            public double Porcentaje { get; set; }
            public PERIODOCDR PeriodoCDR { get; set; }
            public SerieCDR SerieFacturacion { get; set; }
            public DatoSeriePS DatoSeriePeruShop { get; set; }
            public ListaCDRPuntoVenta ListaPuntoVenta { get; set; }
            public InversionCDR InversionGeneralCDR { get; set; }
            public DiasPedidosLineaCreditoCDR DiasPedidosLineaCredito { get; set; }
            public ComboTipoCompraCDR TipoCompraCDR { get; set; }
            public FechaPedidoComisionCDR FechasPedidoComisionCDR { get; set; }
            public SustraccionComisionesLC sustraccionComisionesLC { get; set; }
            public DatosPersonalesCDR datosPersonalesCDR { get; set; }
            public DatosPagoEfectivoCDR DatosPagoEfectivo { get; set; }
            public DatosProcesamientoComprasCDR datosProcesamientoComprasCDR { get; set; }
            public DatosCompraPE_IDOP datosCompraPE_IDOP { get; set; }

            public class DatosCompraPE_IDOP
            {
                public string IdPeruShop { get; set; }
                public string FechaPedido { get; set; }
                public string Direccion { get; set; }
                public string CIP { get; set; }
                public string DocExtorno { get; set; }
                public string RazonExtorno { get; set; }
                public string DirExtorno { get; set; }
            }

            public class DatosProcesamientoComprasCDR
            {
                public int IdPedido { get; set; }
                public string DniCDR { get; set; }
                public string TipoCompra { get; set; }
                public double Monto { get; set; }
                public DateTime Fecha { get; set; }
            }

            public class DatosPagoEfectivoCDR
            {
                public int IdDatos { get; set; }
                public string DniCDR { get; set; }
                public DateTime FechaCreacion { get; set; }
                public DateTime FechaExpiracion { get; set; }
                public string FechaCreacionStr { get; set; }
                public string FechaExpiracionStr { get; set; }
                public string CIP { get; set; }
                public double Monto { get; set; }
                public string CodeStatus { get; set; }
                public int Estado { get; set; }
                public string EstadoPedido { get; set; }
            }

            public class DatosPersonalesCDR
            {
                public int IdDatos { get; set; }
                public string DniCDR { get; set; }
                public string Razon { get; set; }
                public string Nombres { get; set; }
                public string ApellidoPat { get; set; }
                public string ApellidoMat { get; set; }
                public string Documento { get; set; }
                public string Apodo { get; set; }
                public string Direccion { get; set; }
                public string IdPS { get; set; }
                public string DocExtorno { get; set; }
                public string RazonExtorno { get; set; }
                public string DirExtorno { get; set; }
            }

            public class SustraccionComisionesLC
            {
                public int IdSustraccion { get; set; }
                public int IdPeriodo { get; set; }
                public int IdPedido { get; set; }
                public string DniCDR { get; set; }
                public DateTime Fecha { get; set; }
                public double Monto_General { get; set; }
                public double Monto_LC { get; set; }
                public double Monto_Comision { get; set; }
                public string Concepto { get; set; }
                public string Descripcion { get; set; }
            }

            public class FechaPedidoComisionCDR
            {
                public int ID_Pedido { get; set; }
                public string FechaInicio { get; set; }
                public string FechaFin { get; set; }
            }

            public class ComboTipoCompraCDR
            {
                public int IDTipoCompra { get; set; }
                public string Descripcion { get; set; }
            }

            public class DiasPedidosLineaCreditoCDR
            {
                public int IDDias { get; set; }
                public string Nombre { get; set; }
                public bool Estado { get; set; }
            }

            public class ComisionLineaCDR
            {
                public double Comision { get; set; }
                public double LineaCredito { get; set; }
                public double AbstencionComision { get; set; }
                public double AbstencionLineaCredito { get; set; }
                public int IDPeriodoCDR { get; set; }
                public string DniCDR { get; set; }
            }

            public class InversionCDR
            {
                public int IdInversion { get; set; }
                public int TipoCompra { get; set; }
                public string IdInicial { get; set; }
                public decimal Monto { get; set; }
                public string DNICDR { get; set; }
                public string ApodoCDR { get; set; }
                public string PSCDR { get; set; }
                public DateTime Fecha { get; set; }
            }

            public class ListaCDRPuntoVenta
            {
                public List<object> puntos { get; set; }
            }

            public class PERIODOCDR
            {
                public int IdPeriodo { get; set; }
                public string FechaInicio { get; set; }
                public string FechaFin { get; set; }
                public string Descripcion { get; set; }
            }

            public class SerieCDR
            {
                public int IdSerie { get; set; }
                public string Serie { get; set; }
                public string dniCDR { get; set; }
                public string apodoCDR { get; set; }
            }

            public class DatoSeriePS
            {
                public string TipoDoc { get; set; }
                public string SerieDoc { get; set; }
                public string IdPuntoVenta { get; set; }
            }
        }

        public Cliente() { }

        public Cliente(string idCliente, string usuario, string clave, string nombre, string apellidoPat, string apellidoMat, string apodo, string fechaNac, string sexo, string tipoDoc, string numeroDoc, string correo, string telefono, string celular, string pais, string idpais, string departamento, string iddepartamento, string provincia, string idprovincia, string ditrito, string idditrito, string referencia, string direccion, string nroCtaDetraccion, string ruc, string nombreBanco, string nroCtaDeposito, string nroCtaInterbancaria, string tipoCliente, string idtipoCliente, string patrocinador, string idpatrocinador, string tipoEstablecimiento, string idtipoEstablecimiento, string upline, string idupline, string imagen, string packete, string packetePatrocinador, string establecimiento, string estado, string fechaRegistro, string idPeruShop)
        {
            this.idCliente = idCliente;
            this.usuario = usuario;
            this.clave = clave;
            this.nombre = nombre;
            this.apellidoPat = apellidoPat;
            this.apellidoMat = apellidoMat;
            this.apodo = apodo;
            this.fechaNac = fechaNac;
            this.sexo = sexo;
            this.tipoDoc = tipoDoc;
            this.numeroDoc = numeroDoc;
            this.correo = correo;
            this.telefono = telefono;
            this.celular = celular;
            this.pais = pais;
            this.idpais = idpais;
            this.departamento = departamento;
            this.iddepartamento = iddepartamento;
            this.provincia = provincia;
            this.idprovincia = idprovincia;
            this.ditrito = ditrito;
            this.idditrito = idditrito;
            this.referencia = referencia;
            this.direccion = direccion;
            this.nroCtaDetraccion = nroCtaDetraccion;
            this.ruc = ruc;
            this.nombreBanco = nombreBanco;
            this.nroCtaDeposito = nroCtaDeposito;
            this.nroCtaInterbancaria = nroCtaInterbancaria;
            this.tipoCliente = tipoCliente;
            this.idtipoCliente = idtipoCliente;
            this.patrocinador = patrocinador;
            this.idpatrocinador = idpatrocinador;
            this.tipoEstablecimiento = tipoEstablecimiento;
            this.idtipoEstablecimiento = idtipoEstablecimiento;
            this.upline = upline;
            this.idupline = idupline;
            this.imagen = imagen;
            Packete = packete;
            PacketePatrocinador = packetePatrocinador;
            Establecimiento = establecimiento;
            Estado = estado;
            FechaRegistro = fechaRegistro;
            IdPeruShop = idPeruShop;
        }
    }
}
