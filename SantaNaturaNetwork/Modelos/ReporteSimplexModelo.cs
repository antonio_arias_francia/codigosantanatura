﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ReporteSimplexModelo
    {
        public string idPaquete { get; set; }
        public string fechaSimplex { get; set; }
        public string DNICliente { get; set; }
        public string DNIPatrocinador { get; set; }
        public string TipoCompra { get; set; }
        public double PuntosTotal { get; set; }
        public double montoComision { get; set; }
        public string IdopPeruShop { get; set; }
        public string NombreCliente { get; set; }
    }
}
