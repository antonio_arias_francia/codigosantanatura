﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class CombosProducto
    {
        public string idLinea { get; set; }
        public string nombreLinea { get; set; }
        public string idPresentacion { get; set; }
        public string nombrePresentacion { get; set; }

        public CombosProducto() { }

        public CombosProducto(string idLinea, string nombreLinea, string idPresentacion, string nombrePresentacion)
        {
            this.idLinea = idLinea;
            this.nombreLinea = nombreLinea;
            this.idPresentacion = idPresentacion;
            this.nombrePresentacion = nombrePresentacion;
        }
    }
}
