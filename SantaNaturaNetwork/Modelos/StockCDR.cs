﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class StockCDR
    {
        public int IDStock { get; set; }
        public string DNICDR { get; set; }
        public string CDRPS { get; set; }
        public string Apodo { get; set; }
        public string IDProducto { get; set; }
        public string IDProductoXPais { get; set; }
        public string NombreProducto { get; set; }
        public string IDPS { get; set; }
        public string Imagen { get; set; }
        public string Fila { get; set; }
        public string FechaSolicitud { get; set; }
        public string Pais { get; set; }
        public string Estado { get; set; }
        public string TipoCompra { get; set; }
        public int Cantidad { get; set; }
        public int IdSolicitud { get; set; }
        public double PrecioCDR { get; set; }
        public int UnidadesXPrese { get; set; }
        public bool ControlStock { get; set; }
        public double MontoTotal { get; set; }
        public List<StockCDR> ListaStock {get; set;}

        public class CodigoRespuesta
        {
            public int codigo { get; set; }
            public string mensaje { get; set; }
        }
    }
}
