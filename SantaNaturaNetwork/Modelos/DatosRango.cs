﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class DatosRango
    {
        public DatosRango() { }

        public int IdRango { get; set; }
        public string Nombre { get; set; }
        public int PP { get; set; }
        public int PuntajeRango { get; set; }
        public int PML { get; set; }
        public string Condiciones { get; set; }
    }
}
