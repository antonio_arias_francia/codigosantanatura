﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class SendProductsPeruShop
    {

        public SendProductsPeruShop()
        {

        }

        public SendProductsPeruShop(string idProductoSend, string idProdPeruShop, int cantidadPeruShop, double montoSend)
        {
            this.idProductoSend = idProductoSend;
            this.idProdPeruShop = idProdPeruShop;
            this.cantidadPeruShop = cantidadPeruShop;
            this.montoSend = montoSend;
        }

        public string idProductoSend { get; set; }
        public string idProductAlterna { get; set; }
        public string idProdPeruShop { get; set; }
        public int cantidadPeruShop { get; set; }
        public double montoSend { get; set; }
    }
}
