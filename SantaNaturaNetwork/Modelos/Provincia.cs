﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Provincia
    {
        public string Codigo { get; set; }
        public string Nombre { get; set; }

        public string CodigoDepartamento { get; set; }

        public Provincia() { }

        public Provincia(string codigo, string nombre, string codigoDepartamento)
        {
            Codigo = codigo;
            Nombre = nombre;
            CodigoDepartamento = codigoDepartamento;
        }
    }
}
