﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class MomentaneoComi
    {
        public MomentaneoComi() { }

        public MomentaneoComi(string documento, string rango, double monto, double unilevel, double afiliacion, double tiburon, double bronce, double ci, double con, double escolaridad, double mercadeo)
        {
            this.documento = documento;
            this.rango = rango;
            this.monto = monto;
            this.unilevel = unilevel;
            this.afiliacion = afiliacion;
            this.tiburon = tiburon;
            this.bronce = bronce;
            this.ci = ci;
            this.con = con;
            this.escolaridad = escolaridad;
            this.mercadeo = mercadeo;
        }

        public string documento { get; set; }
        public string rango { get; set; }
        public double monto { get; set; }
        public double unilevel { get; set; }
        public double afiliacion { get; set; }
        public double tiburon { get; set; }
        public double bronce { get; set; }
        public double ci { get; set; }
        public double con { get; set; }
        public double escolaridad { get; set; }
        public double mercadeo { get; set; }
    }
}
