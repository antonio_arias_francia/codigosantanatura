﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ProductoJSON
    {
        public ProductoJSON(string idPeruShop, string cantidad, string precio, string promo, string cero)
        {
            IdPeruShop = idPeruShop;
            Cantidad = cantidad;
            Precio = precio;
            Promo = promo;
            Cero = cero;
        }

        public ProductoJSON()
        {

        }

        public string IdPeruShop { get; set; }
        public string Cantidad { get; set; }
        public string Precio { get; set; }
        public string Promo { get; set; }
        public string Cero { get; set; }
    }
}
