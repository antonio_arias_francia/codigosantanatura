﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class DatosSocios
    {
        public string Documento { get; set; }
        public string NombreCompleto { get; set; }
        public int CantSocios { get; set; }
        public int CantCI { get; set; }
        public int CantCON { get; set; }
        public int CantSociosDirectos { get; set; }
        public int CantCIDirectos { get; set; }
        public int CantCONDirectos { get; set; }
        public int NuevoSocios { get; set; }
        public int NuevoCI { get; set; }
        public int NuevoCON { get; set; }
        public int ActivoSocios { get; set; }
        public int ActivoCI { get; set; }
        public int ActivoCON { get; set; }
        public int CantSociosRed { get; set; }
        public int CantCIRed { get; set; }
        public int CantCONRed { get; set; }
        public int CantSociosRedActivos { get; set; }
        public int CantCIRedActivos { get; set; }
        public int CantCONRedActivos { get; set; }
        public int CantNuevosSociosRed { get; set; }
        public int CantNuevosCIRed { get; set; }
        public int CantNuevosCONRed { get; set; }
    }
}
