﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class PeriodoComision
    {
        public int idPeriodo { get; set; }
        public string Documento { get; set; }
        public string idCliente { get; set; }
        public string nombrePeriodo { get; set; }
        public double ComisionLinea { get; set; }
        public double SUMATOTAL { get; set; }
        public double COMAFI { get; set; }
        public double COMUNI { get; set; }
        public double COMTIBU { get; set; }
        public double COMBRON { get; set; }
        public double COMCI { get; set; }
        public double COMCON { get; set; }
        public double COMESCO { get; set; }
        public double COMMERCA { get; set; }
        public double COMMATRI { get; set; }
        public double NIVEL1 { get; set; }
        public double NIVEL2 { get; set; }
        public double NIVEL3 { get; set; }
        public double NIVEL4 { get; set; }
        public double NIVEL5 { get; set; }
        public double UNIX1 { get; set; }
        public double UNIX2 { get; set; }
        public double UNIX3 { get; set; }
        public double UNIX4 { get; set; }
        public double UNIX5 { get; set; }
        public double UNIX6 { get; set; }
        public double UNIX7 { get; set; }
        public double UNIX8 { get; set; }
        public double UNIX9 { get; set; }
        public double UNIX10 { get; set; }
        public double UNIX11 { get; set; }
        public double UNIX12 { get; set; }
        public double UNIX13 { get; set; }
        public double UNIX14 { get; set; }
        public double UNIX15 { get; set; }
        public bool Estado { get; set; }

    }
}
