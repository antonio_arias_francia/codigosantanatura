﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Descuento
    {
        public string NumDocumentoCliente { get; set; }
        public string IdPackete { get; set; }
        public string FechaObtencionPackete { get; set; }

        public Descuento()
        {

        }
    }
}
