﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class SociosRetencion
    {
        public int IDP { get; set; }
        public string Documento { get; set; }
        public string IdSocio { get; set; }
        public int SOCIO_DIRECTO_TOTAL { get; set; }
        public int SOCIO_DIRECTO_ACTIVO { get; set; }
        public int SOCIO_DIRECTO_NUEVO { get; set; }
        public int CONSULTOR_DIRECTO_TOTAL { get; set; }
        public int CONSULTOR_DIRECTO_ACTIVO { get; set; }
        public int CONSULTOR_DIRECTO_NUEVO { get; set; }
        public int CI_DIRECTO_TOTAL { get; set; }
        public int CI_DIRECTO_ACTIVO { get; set; }
        public int CI_DIRECTO_NUEVO { get; set; }
        public int SOCIO_RED_TOTAL { get; set; }
        public int SOCIO_RED_ACTIVO { get; set; }
        public int SOCIO_RED_NUEVO { get; set; }
        public int CONSULTOR_RED_TOTAL { get; set; }
        public int CONSULTOR_RED_ACTIVO { get; set; }
        public int CONSULTOR_RED_NUEVO { get; set; }
        public int CI_RED_TOTAL { get; set; }
        public int CI_RED_ACTIVO { get; set; }
        public int CI_RED_NUEVO { get; set; }
    }
}
