﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ArchivosPDF
    {
        public int ID_DATOS { get; set; }
        public string Nombre { get; set; }
        public string Vigencia { get; set; }
        public string Archivo { get; set; }
        public ImagenesBanners imagenesBanners { get; set; }
        public DocumentosInformacion documentosInformacion { get; set; }
        public DocumentosMarketing documentosMarketing { get; set; }

        public class ImagenesBanners
        {
            public int ID_DATOS { get; set; }
            public string Nombre { get; set; }
            public string Archivo { get; set; }
        }

        public class DocumentosInformacion
        {
            public int ID_DATOS { get; set; }
            public string Nombre { get; set; }
            public string Archivo { get; set; }
            public string TipoArchivo { get; set; }
            public string CodigoClass { get; set; }
        }

        public class DocumentosMarketing
        {
            public int ID_DATOS { get; set; }
            public string Nombre { get; set; }
            public string Archivo { get; set; }
            public string TipoArchivo { get; set; }
            public string CodigoClass { get; set; }
            public bool Paq_Basico { get; set; }
            public bool Paq_Profesional { get; set; }
            public bool Paq_Empresarial { get; set; }
            public bool Paq_Millonario { get; set; }
            public bool Paq_Imperial { get; set; }
            public bool Paq_Consultor { get; set; }
            public bool Paq_C_Inteligente { get; set; }
        }
    }
}
