﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class DatosPagoEfectivo
    {
        public string Ticket { get; set; }
        public string IdCliente { get; set; }
        public DateTime FechaCreacion { get; set; }
        public DateTime FechaExpiracion { get; set; }
        public string CIP { get; set; }
        public double Monto { get; set; }
        public string CodeStatus { get; set; }
    }
}
