﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class DetalleCompra
    {
        public DetalleCompra()
        {

        }

        public DetalleCompra(string nombre, string foto, int cantidad, double montoTotalNeto, string idProductoPeruShop)
        {
            Nombre = nombre;
            Foto = foto;
            Cantidad = cantidad;
            MontoTotalNeto = montoTotalNeto;
            IdProductoPeruShop = idProductoPeruShop;
        }

        public string Nombre { get; set; }
        public string Foto { get; set; }
        public int Cantidad { get; set; }
        public double MontoTotalNeto { get; set; }
        public double SubTotalDC { get; set; }
        public double PrecioUnitarioDC { get; set; }
        public string CodigoProductoDC { get; set; }
        public string TipoPagoCOM { get; set; }
        public string IdopCOM { get; set; }
        public int EstadoCOM { get; set; }
        public int CantidadCOM { get; set; }
        public double MontoTotalCOM { get; set; }
        public double PuntosTotalCOM { get; set; }
        public double MontoPagarCOM { get; set; }
        public double MontoComisionCOM { get; set; }
        public string TicketCOM { get; set; }
        public string FechaCOM { get; set; }
        public string TipoCompraCOM { get; set; }
        public string IdProductoPeruShop { get; set; }
        public string CodigoDC { get; set; }
        public string FotoVoucher { get; set; }
        public int CantiPS { get; set; }
        public double PrecioPS { get; set; }
        public double Puntos { get; set; }
        public string LineaSend { get; set; }
        public string precioStr { get; set; }
        public string idPaquete { get; set; }
        public string idTipoCompra { get; set; }
        public string nombreTipoCom { get; set; }
        public string idTipoPago { get; set; }
        public string nombreTipoPago { get; set; }
        public int idEstado { get; set; }
        public string descEstado { get; set; }
        public string idDespacho { get; set; }
        public string descDespacho { get; set; }
    }
}
