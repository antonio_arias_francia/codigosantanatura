﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class ComAfiliacion
    {
        public string documento { get; set; }
        public string paquete { get; set; }
        public double PP { get; set; }
    }
}
