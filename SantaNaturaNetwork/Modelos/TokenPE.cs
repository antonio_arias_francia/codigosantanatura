﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class TokenPE
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public Data data { get; set; }
        public class Data
        {
            public string token { get; set; }
            public string codeService { get; set; }
            public string tokenStart { get; set; }
            public string tokenExpires { get; set; }
        }

        public class GetObjects
        {
            public string code { get; set; }
            public string message { get; set; }
            public int cip { get; set; }
            public string currency { get; set; }
            public decimal amount { get; set; }
            public string transactionCode { get; set; }
            public string dateExpiry { get; set; }
            public string cipURL { get; set; }
            public string codeData { get; set; }
            public string messageData { get; set; }
            public string field { get; set; }

        }

        public class GoodObjects
        {
            public string code { get; set; }
            public string message { get; set; }
            public DataC data { get; set; }

            public class DataC
            {
                public int cip { get; set; }
                public string currency { get; set; }
                public decimal amount { get; set; }
                public string transactionCode { get; set; }
                public string dateExpiry { get; set; }
                public string cipURL { get; set; }
            }
        }

        public class BadObjects
        {
            public string code { get; set; }
            public string message { get; set; }
            public DataE data { get; set; }

            public class DataE
            {
                public string code { get; set; }
                public string message { get; set; }
                public string field { get; set; }
            }
        }

        public class Notification
        {
            public DataE data { get; set; }

            public class DataE
            {
                public string cip { get; set; }
                public string currency { get; set; }
                public decimal amount { get; set; }
                public string paymentDate { get; set; }
                public string transactionCode { get; set; }
            }
        }
    }
}
