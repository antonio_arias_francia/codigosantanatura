﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class PromocionUnicaModel
    {
        public int IdPromo { get; set; }
        public bool Estado { get; set; }
        public string ProductoPais { get; set; }
        public string IdProducto { get; set; }
        public int Cantidad { get; set; }
        public int CantidadActiva { get; set; }
        public string FechaInicio { get; set; }
        public string FechaFin { get; set; }
    }
}
