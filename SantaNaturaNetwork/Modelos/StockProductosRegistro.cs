﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class StockProductosRegistro
    { 
        public string codigo { get; set; }
        public string PS { get; set; }
        public string imgP { get; set; }
        public string cantidad { get; set; }
        public string UP { get; set; }
        public bool controlStock { get; set; }
        public string idProductoPais { get; set; }
        public double precio { get; set; }
    }
}
