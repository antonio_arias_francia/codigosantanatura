﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Novedad
    {
        public string idNovedades { get; set; }
        public string titulo { get; set; }
        public string mensaje { get; set; }
        public string enlace { get; set; }
        public string imagen { get; set; }
        public bool estado { get; set; }
        public string fecha { get; set; }
        public Novedad() { }

        public Novedad(string idNovedades, string titulo, string mensaje, string enlace, string imagen, bool estado, string fecha)
        {
            this.idNovedades = idNovedades;
            this.titulo = titulo;
            this.mensaje = mensaje;
            this.enlace = enlace;
            this.imagen = imagen;
            this.estado = estado;
            this.fecha = fecha;
        }
    }
}
