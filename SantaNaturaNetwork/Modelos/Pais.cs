﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Pais
    {

        public string Codigo { get; set; }
        public string Nombre { get; set; }

        public Pais() { }

        public Pais(string codigo, string nombre)
        {
            this.Codigo = codigo;
            this.Nombre = nombre;
        }
    }
}
