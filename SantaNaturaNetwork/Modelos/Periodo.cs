﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class Periodo
    {
        public Periodo() { }

        public int idPeriodo { get; set; }
        public int idPeriodoComision { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string nombre { get; set; }
        public string rango { get; set; }
        public int idRango { get; set; }
        public bool estado { get; set; }
        public bool visualizar { get; set; }
        public int cantidad { get; set; }
        public int evaluacionMercadeo { get; set; }
        public int evaluacionBronce { get; set; }
        public int evaluacionTiburon { get; set; }
        public int evaluacionEscolaridad { get; set; }
        public int evaluacionMatrocial4x4 { get; set; }
        public int evaluacionMatrocial5x5 { get; set; }
        public int evaluacionMatrocial6x6 { get; set; }
        public int requerimientoBronce { get; set; }
        public int requerimientoEscolaridad { get; set; }
        public double sumaPP { get; set; }
        public double puntos { get; set; }
        public MaximoRango ClsMaximoRango { get; set; }

        public class MaximoRango{
            public string rango { get; set; }
            public int idRango { get; set; }
        }
}
}
