﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelos
{
    public class TipoCliente
    {
        public string IdTipo { get; set; }
        public string tipo { get; set; }

        public TipoCliente() { }

        public TipoCliente(string idTipo, string tipo)
        {
            IdTipo = idTipo;
            this.tipo = tipo;
        }
    }
}
