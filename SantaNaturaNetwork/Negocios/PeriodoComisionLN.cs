﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Datos;

namespace Negocios
{
    public class PeriodoComisionLN
    {
        #region PATRON SINGLETON
        private static PeriodoComisionLN objPeriodo = null;
        private PeriodoComisionLN() { }
        public static PeriodoComisionLN getInstance()
        {
            if (objPeriodo == null)
            {
                objPeriodo = new PeriodoComisionLN();
            }
            return objPeriodo;
        }
        #endregion

        public List<PeriodoComision> ListarPeriodoComisionCombo()
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaPeriodoComisionCombo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaComisionesLinea(string documento)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaComisionesLinea(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaComisionesXPeriodo(string documento, int IDP)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaComisionesXPeriodo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaComisionesXPeriodo_Historico(string documento, int IDP)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaComisionesXPeriodo_Historico(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaProgresionTotalBonificaciones()
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaProgresionTotalBonificaciones();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MostrarIdPeriodoComisionActivo()
        {
            try
            {
                return PeriodoComisionDAO.getInstance().MostrarIdPeriodoComisionActivo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaPeriodoComisionFacturacion(string idcliente)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaPeriodoComisionFacturacion(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaPeriodoModalCanjesAbono(int idPeriodo)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaPeriodoModalCanjesAbono(idPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaPeriodoComisionFacturacionModal(string idcliente, int idPeriodo)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaPeriodoComisionFacturacionModal(idcliente, idPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListaComisionUnilevelxNivel(int IDP, string proc)
        {
            try
            {
                return PeriodoComisionDAO.getInstance().ListaComisionUnilevelxNivel(IDP, proc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
