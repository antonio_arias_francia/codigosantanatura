﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class FechasLimiteLN
    {
        #region PATRON SINGLETON
        private static FechasLimiteLN objLimite = null;
        private FechasLimiteLN() { }
        public static FechasLimiteLN getInstance()
        {
            if (objLimite == null)
            {
                objLimite = new FechasLimiteLN();
            }
            return objLimite;
        }
        #endregion

        public bool RegistroLimite(LimiteDias objLimite)
        {
            try
            {
                return FechasLimiteDAO.getInstance().RegistroLimites(objLimite);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarLimite(LimiteDias objLimite)
        {
            try
            {
                return FechasLimiteDAO.getInstance().ActualizarLimite(objLimite);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<LimiteDias> ListaLimite(int idLimite)
        {
            try
            {
                return FechasLimiteDAO.getInstance().ListaLimites(idLimite);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<LimiteDias> ListaGeneralHorasLimites()
        {
            try
            {
                return FechasLimiteDAO.getInstance().ListaGeneralHorasLimites();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
