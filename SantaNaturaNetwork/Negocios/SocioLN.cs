﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Datos;

namespace Negocios
{
    public class SocioLN
    {
        #region PATRON SINGLETON
        private static SocioLN objSocio = null;
        private SocioLN() { }
        public static SocioLN getInstance()
        {
            if (objSocio == null)
            {
                objSocio = new SocioLN();
            }
            return objSocio;
        }
        #endregion

        public List<DatosSocios> ListarUpline(string numDocumento)
        {
            try
            {
                return SocioDAO.getInstance().ListarUpline(numDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<DatosSocios> ListarRetencionGenerada(string numDocumento, int IDP)
        {
            try
            {
                return SocioDAO.getInstance().ListarRetencionGenerada(numDocumento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<DatosSocios> ListarRetencionGeneral(int IDP)
        {
            try
            {
                return SocioDAO.getInstance().ListarRetencionGeneral(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<DatosSocios> ListarCantidadDirectos(string numDocumento, int IDP)
        {
            try
            {
                return SocioDAO.getInstance().ListarCantidadDirectos(numDocumento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int CantNuevosSociosPatrocinio(string numDocumento)
        {
            try
            {
                return SocioDAO.getInstance().CantNuevosSociosPatrocinio(numDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
