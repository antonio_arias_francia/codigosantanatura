﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Datos;

namespace Negocios
{
    public class ProductoLN
    {
        #region PATRON SINGLETON
        private static ProductoLN objProducto = null;
        private ProductoLN() { }
        public static ProductoLN getInstance()
        {
            if (objProducto == null)
            {
                objProducto = new ProductoLN();
            }
            return objProducto;
        }
        #endregion

        public bool RegistroProducto(ProductoV2 objProducto, List<ProductoPais> PPais)
        {
            try
            {
                return ProductoDatos.getInstance().RegistroProducto(objProducto, PPais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroProductoPais(List<ProductoPais> PPais)
        {
            try
            {
                return ProductoDatos.getInstance().RegistroProductoPais(PPais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarProductoXPais(List<ProductoPais> PPais)
        {
            try
            {
                return ProductoDatos.getInstance().ActualizarProductoXPais(PPais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarProducto(ProductoV2 objProducto)
        {
            try
            {
                return ProductoDatos.getInstance().ActualizarProducto(objProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CodigoGenerado()
        {
            try
            {
                return ProductoDatos.getInstance().GenerarProducto();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CombosProducto> ListaComboLinea()
        {
            try
            {
                return ProductoDatos.getInstance().ListadoComboLinea();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaqueteNatura> ListaComboPaquete()
        {
            try
            {
                return ProductoDatos.getInstance().ListadoComboPaquete();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CombosProducto> ListaComboPresentacion()
        {
            try
            {
                return ProductoDatos.getInstance().ListadoComboPresentacion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoV2> ListaProductos()
        {
            try
            {
                return ProductoDatos.getInstance().ListarProducto();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Producto> ListaMenosVendidos(string pais)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosMenosVendidos(pais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarProducto(string idProducto, string productoPais)
        {
            try
            {
                return ProductoDatos.getInstance().EliminarProducto(idProducto, productoPais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoV2> ListaIdopXNombre()
        {
            try
            {
                return ProductoDatos.getInstance().ListaIdopxNombre();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoPais> ListarProductoXIdProducto(string idProducto)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductoXIdProducto(idProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerLineaIdProducto(string IdProducto)
        {
            try
            {
                return ProductoDatos.getInstance().ObtenerLineaIdProducto(IdProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerIdProductoXIdProductoPais(string IdProductoPais)
        {
            try
            {
                return ProductoDatos.getInstance().ObtenerIdProductoXIdProductoPais(IdProductoPais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoPais.Linea_PrecioCDR> ObtenerLineaPrecioCDR_By_IdProductoPais(string idProductoPais)
        {
            try
            {
                return ProductoDatos.getInstance().ObtenerLineaPrecioCDR_By_IdProductoPais(idProductoPais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerPorcentajeLineaCDR(string DniCDR, string Linea)
        {
            try
            {
                return ProductoDatos.getInstance().ObtenerPorcentajeLineaCDR(DniCDR, Linea);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoV2> ListaProductoCDR()
        {
            try
            {
                return ProductoDatos.getInstance().ListaProductoCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoV2> ListaDatosProductosCDR(string IDPRODUCTO)
        {
            try
            {
                return ProductoDatos.getInstance().ListaDatosProductosCDR(IDPRODUCTO);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProductoV2> ListaUP_Producto()
        {
            try
            {
                return ProductoDatos.getInstance().ListaUP_Producto();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
