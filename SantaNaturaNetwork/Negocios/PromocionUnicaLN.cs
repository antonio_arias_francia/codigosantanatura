﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class PromocionUnicaLN
    {
        #region PATRON SINGLETON
        private static PromocionUnicaLN objPromo = null;
        private PromocionUnicaLN() { }
        public static PromocionUnicaLN getInstance()
        {
            if (objPromo == null)
            {
                objPromo = new PromocionUnicaLN();
            }
            return objPromo;
        }
        #endregion

        public List<PromocionUnicaModel> ListarPromocion()
        {
            try
            {
                return PromocionUnicaDAO.getInstance().ListarPromocion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListaCantPromoUnica()
        {
            try
            {
                return PromocionUnicaDAO.getInstance().ListaCantPromoUnica();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListaEvaluarCantProductosPromocionUnica(string idcliente)
        {
            try
            {
                return PromocionUnicaDAO.getInstance().ListaEvaluarCantProductosPromocionUnica(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ListarCodigoPromoActiva()
        {
            try
            {
                return PromocionUnicaDAO.getInstance().ListaCodigoPromoActiva();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ListaIdProductoPromoUnica()
        {
            try
            {
                return PromocionUnicaDAO.getInstance().ListaIdProductoPromoUnica();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroPromoUnica(PromocionUnicaModel objPromo)
        {
            try
            {
                return PromocionUnicaDAO.getInstance().RegistroPromoUnica(objPromo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPromocionUnica(PromocionUnicaModel objPromo)
        {
            try
            {
                return PromocionUnicaDAO.getInstance().ActualizarPromoUnica(objPromo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PromocionUnicaModel DatosPromoUnica()
        {
            try
            {
                return PromocionUnicaDAO.getInstance().DatosPromoUnica();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
