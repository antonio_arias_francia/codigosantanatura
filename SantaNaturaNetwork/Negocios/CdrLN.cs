﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class CdrLN
    {
        #region "PATRON SINGLETON"
        private static CdrLN objCDR = null;
        private CdrLN() { }
        public static CdrLN getInstance()
        {
            if (objCDR == null)
            {
                objCDR = new CdrLN();
            }
            return objCDR;
        }
        #endregion

        public bool RegistroPorcentajeLineaCDR(Cliente.CDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().RegistroPorcentajeLineaCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR> ListaCDRPorcentaje()
        {
            try
            {
                return CdrDAO.getInstance().ListaCDRPorcentaje();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente.CDR> ListaPorcentajeLineaXCDR(string DNICDR)
        {
            try
            {
                return CdrDAO.getInstance().ListaPorcentajeLineaXCDR(DNICDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ActualizarPorcentajeLineaCDR(Cliente.CDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarPorcentajeLineaCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaPeriodoCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int RegistroPeriodoCDR(Cliente.CDR.PERIODOCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().RegistroPeriodoCDR(objCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPeriodoCDR(Cliente.CDR.PERIODOCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarPeriodoCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool RegistroInicial_CLC_CDR(int IDPC, string dniCDR)
        {
            try
            {
                return CdrDAO.getInstance().RegistroInicial_CLC_CDR(IDPC, dniCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.SerieCDR> ListaSerieCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaSerieCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool RegistroSeriePuntoVenta(string dniCDR, string serie)
        {
            try
            {
                return CdrDAO.getInstance().RegistroSeriePuntoVenta(dniCDR, serie);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool ActualizarSeriePuntoVenta(int idSerie, string serie)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarSeriePuntoVenta(idSerie, serie);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public string ObtenerSerieCDR(string PSCDR)
        {
            try
            {
                return CdrDAO.getInstance().ObtenerSerieCDR(PSCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroInversionCDR(Cliente.CDR.InversionCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().RegistroInversionCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool ActualizarInversionInicialCDR(Cliente.CDR.InversionCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarInversionInicialCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.InversionCDR> ListaInversionInicialCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaInversionInicialCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string ObtenerDniCDR_By_IDPSCDR(string IDPSCDR)
        {
            try
            {
                return CdrDAO.getInstance().ObtenerDniCDR_By_IDPSCDR(IDPSCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionLineaCDR(Cliente.CDR.ComisionLineaCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarComisionLineaCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool ActualizarComisionLineaCDRLista(List<Cliente.CDR.ComisionLineaCDR> objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarComisionLineaCDRLista(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasPedidoLineaCreditoCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaDiasPedidoLineaCreditoCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ActualizarEstadoDiasPedidoLC(Cliente.CDR.DiasPedidosLineaCreditoCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarEstadoDiasPedidoLC(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.ComboTipoCompraCDR> ListaTipoCompraCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaTipoCompraCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasActivosLineaCreditoCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaDiasActivosLineaCreditoCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasActivosEmergencia()
        {
            try
            {
                return CdrDAO.getInstance().ListaDiasActivosEmergencia();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente.CDR.FechaPedidoComisionCDR> ListaFechasPedidoComisionCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaFechasPedidoComisionCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ActualizarFechasPedidoComision(Cliente.CDR.FechaPedidoComisionCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarFechasPedidoComision(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.ComisionLineaCDR> ListaComisionLineaByCDR(string dniCDR)
        {
            try
            {
                return CdrDAO.getInstance().ListaComisionLineaByCDR(dniCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.SustraccionComisionesLC> ListaSustraccionComisionLineaByCDR(string dniCDR)
        {
            try
            {
                return CdrDAO.getInstance().ListaSustraccionComisionLineaByCDR(dniCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrarSustraccionComisionesLC(Cliente.CDR.SustraccionComisionesLC objCDR)
        {
            try
            {
                return CdrDAO.getInstance().RegistrarSustraccionComisionesLC(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool RegistrarDatosPersonalesCDR(Cliente.CDR.DatosPersonalesCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().RegistrarDatosPersonalesCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.DatosPersonalesCDR> ListaDatosPersonalesCDR(string dniCDR)
        {
            try
            {
                return CdrDAO.getInstance().ListaDatosPersonalesCDR(dniCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDatosPersonalesCDR(Cliente.CDR.DatosPersonalesCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarDatosPersonalesCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public void RegistroDatosPagoEfectivoCDR(Cliente.CDR.DatosPagoEfectivoCDR objDatos)
        {
            try
            {
                CdrDAO.getInstance().RegistroDatosPagoEfectivoCDR(objDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.DatosPagoEfectivoCDR> ListaPedidosPE(string dniCDR)
        {
            try
            {
                return CdrDAO.getInstance().ListaPedidosPE(dniCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDatosCompraPE_CDR(int idDatos, string fechapago, string numoperacion)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarDatosCompraPE_CDR(idDatos, fechapago, numoperacion);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente.CDR.DatosProcesamientoComprasCDR> ListaDatosProcesamientoComprasCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaDatosProcesamientoComprasCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.DatosPagoEfectivoCDR> ListaComprasExpiradasCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaComprasExpiradasCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.DatosCompraPE_IDOP> ListaDatosCompraPE_IDOP(int IdPedido)
        {
            try
            {
                return CdrDAO.getInstance().ListaDatosCompraPE_IDOP(IdPedido);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarDatosComprasExpiradasCDR(int IdPedido)
        {
            try
            {
                CdrDAO.getInstance().EliminarDatosComprasExpiradasCDR(IdPedido);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DescontarStockCDR(List<StockProductosRegistro> listaStock, string dniCDR)
        {
            try
            {
                return CdrDAO.getInstance().DescontarStockCDR(listaStock, dniCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasCondicionEmergencia()
        {
            try
            {
                return CdrDAO.getInstance().ListaDiasCondicionEmergencia();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ActualizarCondicionEmergencia(Cliente.CDR.DiasPedidosLineaCreditoCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarCondicionEmergencia(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public string GetUltimaFechaCompraCDR(string documento)
        {
            try
            {
                return CdrDAO.getInstance().GetUltimaFechaCompraCDR(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.DatosPersonalesCDR> ListaDatosPersonalesCDR_Admin()
        {
            try
            {
                return CdrDAO.getInstance().ListaDatosPersonalesCDR_Admin();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDatosPersonalesCDR_ADMIN(Cliente.CDR.DatosPersonalesCDR objCDR)
        {
            try
            {
                CdrDAO.getInstance().ActualizarDatosPersonalesCDR_ADMIN(objCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.CDR.FechaPedidoComisionCDR> ListaFechasEvaluacionPedidoCDR()
        {
            try
            {
                return CdrDAO.getInstance().ListaFechasEvaluacionPedidoCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ActualizarFechasEvaluacionPedidoCDR(Cliente.CDR.FechaPedidoComisionCDR objCDR)
        {
            try
            {
                return CdrDAO.getInstance().ActualizarFechasEvaluacionPedidoCDR(objCDR);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public int CondicionPedidosCDR(string documento)
        {
            try
            {
                return CdrDAO.getInstance().CondicionPedidosCDR(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
