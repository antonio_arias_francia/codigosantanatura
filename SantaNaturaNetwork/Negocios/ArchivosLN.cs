﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class ArchivosLN
    {
        #region "PATRON SINGLETON"
        private static ArchivosLN objArchivos = null;
        private ArchivosLN() { }
        public static ArchivosLN getInstance()
        {
            if (objArchivos == null)
            {
                objArchivos = new ArchivosLN();
            }
            return objArchivos;
        }
        #endregion

        public List<ArchivosPDF> ListarPromocionesPDF()
        {
            try
            {
                return ArchivosDAO.getInstance().ListarPromocionesPDF();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistroPromocionPDF(string nombre, string vigencia, string archivo)
        {
            try
            {
                ArchivosDAO.getInstance().RegistroPromocionPDF(nombre, vigencia, archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarPromocionPDF(string nombre, string vigencia, string archivo, int idDatos)
        {
            try
            {
                ArchivosDAO.getInstance().ActualizarPromocionPDF(nombre, vigencia, archivo, idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ArchivosPDF> ListarPremiosPDF()
        {
            try
            {
                return ArchivosDAO.getInstance().ListarPremiosPDF();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistroPremioPDF(string nombre, string vigencia, string archivo)
        {
            try
            {
                ArchivosDAO.getInstance().RegistroPremioPDF(nombre, vigencia, archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarPremioPDF(string nombre, string vigencia, string archivo, int idDatos)
        {
            try
            {
                ArchivosDAO.getInstance().ActualizarPremioPDF(nombre, vigencia, archivo, idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ArchivosPDF.ImagenesBanners> ListarImagenesBanners()
        {
            try
            {
                return ArchivosDAO.getInstance().ListarImagenesBanners();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistroImagenBanner(string nombre,string archivo)
        {
            try
            {
                ArchivosDAO.getInstance().RegistroImagenBanner(nombre, archivo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarImagenBanner(string nombre, string archivo, int idDatos)
        {
            try
            {
                ArchivosDAO.getInstance().ActualizarImagenBanner(nombre, archivo, idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarImagenBanner(int idDatos)
        {
            try
            {
                ArchivosDAO.getInstance().EliminarImagenBanner(idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ArchivosPDF.DocumentosInformacion> ListarDocumentosInformacion()
        {
            try
            {
                return ArchivosDAO.getInstance().ListarDocumentosInformacion();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistroDocumentosInformacion(string nombre, string archivo, string tipoArchivo, string codigoClass)
        {
            try
            {
                ArchivosDAO.getInstance().RegistroDocumentosInformacion(nombre, archivo, tipoArchivo, codigoClass);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDocumentosInformacion(string nombre, string archivo, string tipoArchivo, string codigoClass, int IdDatos)
        {
            try
            {
                ArchivosDAO.getInstance().ActualizarDocumentosInformacion(nombre, archivo, tipoArchivo, codigoClass, IdDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarDocumentosInformacion(int idDatos)
        {
            try
            {
                ArchivosDAO.getInstance().EliminarDocumentosInformacion(idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ArchivosPDF.DocumentosMarketing> ListarDocumentosMarketing()
        {
            try
            {
                return ArchivosDAO.getInstance().ListarDocumentosMarketing();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ArchivosPDF.DocumentosMarketing> ListarDetallesDocumentosMarketing(int idDatos)
        {
            try
            {
                return ArchivosDAO.getInstance().ListarDetallesDocumentosMarketing(idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ArchivosPDF.DocumentosMarketing> ListarDocumentosMarketingSocios(string paquete)
        {
            try
            {
                return ArchivosDAO.getInstance().ListarDocumentosMarketingSocios(paquete);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistroDocumentosMarketing(ArchivosPDF.DocumentosMarketing objDatos)
        {
            try
            {
                ArchivosDAO.getInstance().RegistroDocumentosMarketing(objDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDocumentosMarketing(ArchivosPDF.DocumentosMarketing objDatos)
        {
            try
            {
                ArchivosDAO.getInstance().ActualizarDocumentosMarketing(objDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarDocumentosMarketing(int idDatos)
        {
            try
            {
                ArchivosDAO.getInstance().EliminarDocumentosMarketing(idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
