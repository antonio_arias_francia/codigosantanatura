﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class ProvinciaLN
    {
        #region PATRON SINGLETON
        private static ProvinciaLN objProvincia = null;
        private ProvinciaLN() { }
        public static ProvinciaLN getInstance()
        {
            if (objProvincia == null)
            {
                objProvincia = new ProvinciaLN();
            }
            return objProvincia;
        }
        #endregion

        public List<Provincia> ListarProvincia(string departamento)
        {
            try
            {
                return ProvinciaDAO.getInstance().ListarProvincia(departamento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Provincia> GetProvincia()
        {
            try
            {
                return ProvinciaDAO.getInstance().GetProvincia();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
