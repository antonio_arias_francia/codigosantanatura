﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class ProductoNegocios
    {
        #region "PATRON SINGLETON"
        private static ProductoNegocios _productoNegocios = null;
        private ProductoNegocios() { }
        public static ProductoNegocios getInstance()
        {
            if (_productoNegocios == null)
            {
                _productoNegocios = new ProductoNegocios();
            }
            return _productoNegocios;
        }
        #endregion

        public List<Producto> ListarProductosByPais(string pais, string TipoCl)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosByPais(pais, TipoCl);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<Producto> ListarProductosByPaisv3(string pais, string Buscar, string tipoCL)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosByPaisv3(pais, Buscar, tipoCL);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public List<Producto> ListarProductosMasVendidos(string pais)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosMasVendidos(pais);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Producto> ListarProductosByCodigo(string codigo)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosByCodigo(codigo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Producto> ListarProductosByNombre(string pais)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosByNombre(pais);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Producto> ListarProductosByCodigoPro(string codigo)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosByCodigoProdu(codigo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<string> FiltrarNombreProductos(string palabra)
        {
            try
            {
                return ProductoDatos.getInstance().FiltrarNombreProductos(palabra);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<Producto> ListarProductosByLinea(string pais)
        {
            try
            {
                return ProductoDatos.getInstance().ListarProductosByLinea(pais);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
