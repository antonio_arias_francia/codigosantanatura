﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class DistritoLN
    {
        #region PATRON SINGLETON
        private static DistritoLN objDistrito = null;
        private DistritoLN() { }
        public static DistritoLN getInstance()
        {
            if (objDistrito == null)
            {
                objDistrito = new DistritoLN();
            }
            return objDistrito;
        }
        #endregion

        public List<Distrito> ListarDistrito(string provincia)
        {

            try
            {
                return DistritoDAO.getInstance().ListarDistrito(provincia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Distrito> GetDistrito()
        {
            try
            {
                return DistritoDAO.getInstance().GetDistrito();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
