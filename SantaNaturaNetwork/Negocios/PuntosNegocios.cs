﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class PuntosNegocios
    {
        #region "PATRON SINGLETON"
        private static PuntosNegocios puntosNegocios = null;
        private PuntosNegocios() { }
        public static PuntosNegocios getInstance()
        {
            if (puntosNegocios == null)
            {
                puntosNegocios = new PuntosNegocios();
            }
            return puntosNegocios;
        }
        #endregion


        public List<PuntosComisiones> ListaReporteNegocios(string dni, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaPuntosComisiones(dni, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaDirectosVPR(string dni)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaDirectosVPR(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaDirectosVPRPeriodo(string dni, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaDirectosVPRPeriodo(dni, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListaGenerarPPDirectos(string dni)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaGenerarPPDirectos(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListaGenerarPPDirectosPeriodo(string dni, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaGenerarPPDirectosPeriodo(dni, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRango(string dni)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaUplinesRango(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRangoPeriodo(string dni, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaUplinesRangoPeriodo(dni, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ComisionCI(string dni)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ComisionCI(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DatosRango> ListaCalculaRango()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaCalculaRango();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstado(string dni, string rango, int idRango)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarRango(dni, rango, idRango);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstadoByPeriodo(string dni, string rango, int IDP, int IdRango)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarRangoByPeriodo(dni, rango, IDP, IdRango);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComision(string dni, double monto)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComision(dni, monto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarVP(string dni, double vp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarVP(dni, vp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPP(string dni, double pp, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarPP(dni, pp, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizaRecalculoPuntos(string dni, int idp, double PP, double VP, double VR, double VG, double VIP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizaRecalculoPuntos(dni, idp, PP, VP, VR, VG, VIP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarVQ(string documento, double VQ)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarVQ(documento, VQ);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarVQPeriodo(string documento, double VQ, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarVQPeriodo(documento, VQ, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaCalificacion(string dni)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaCalificacion(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PuntosComisiones ObtenerVPR(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerVPR(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComAfi(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComAfi(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComAfiAutomatico()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComAfiAutomatico();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionMercadeo(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionMercadeo(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionBronce(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionBronce(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerRequerimientoBronce(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerRequerimientoBronce(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionMercadeoRecalculo(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionMercadeoRecalculo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerRequerimientoBronceRecalculo(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerRequerimientoBronceRecalculo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerPuntosXFechas(string documento, string inicio, string fin)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerPuntosXFechas(documento, inicio, fin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionMercadeo(string documento, double monto)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComisionMercadeo(documento, monto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarComisionMercadeoRecalculo(string documento, double periodo, int IDP)
        {
            try
            {
                PuntosComisionesDAO.getInstance().ActualizarComisionMercadeoRecalculo(documento, periodo, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionBronce(string documento, double monto)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComisionBronce(documento, monto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionBronceRecalculo(string documento, double monto, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComisionBronceRecalculo(documento, monto, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionTiburon(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionTiburon(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComTiburon(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComTiburon(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComBronce(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComBronce(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComEscolaridad(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComEscolaridad(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComEscolaridadAutomatico()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComEscolaridadAutomatico();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        public List<Cliente> ListarDniXRangoComisiones(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListarDniXRangoComisiones(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarRangoComision(string documento, string rango, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarRangoComision(documento, rango, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionEscolaridadRecalculo(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionEscolaridadRecalculo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionEscolaridadAutomatico(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionEscolaridadAutomatico(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerRequerimientoEscolaridadRecalculo(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerRequerimientoEscolaridadRecalculo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerRequerimientoEscolaridadAutomatico(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerRequerimientoEscolaridadAutomatico(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEscolaridadRecalculo(string documento, double monto, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarEscolaridadRecalculo(documento, monto, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEscolaridadAutomatico(string documento, double monto)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarEscolaridadAutomatico(documento, monto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComMatricial(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComMatricial(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerSumaPPRecalculo(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerSumaPPRecalculo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerSumaPPRecalculoMatricial(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerSumaPPRecalculoMatricial(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListaEvaluacionMatricial(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaEvaluacionMatricial(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarEvaluacionDirectosMatricial(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListarEvaluacionDirectosMatricial(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarMatricialRecalculo(string documento, double monto, int IDP, int M4X4, int M5X5, int M6X6)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarMatricialRecalculo(documento, monto, IDP, M4X4, M5X5, M6X6);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionUnilevel(string documento, double COMUNI, int IDP, double nivel1, double nivel2, double nivel3,
                                                 double nivel4, double nivel5, double nivel6, double nivel7, double nivel8, double nivel9,
                                                 double nivel10, double nivel11, double nivel12, double nivel13, double nivel14, double nivel15)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComisionUnilevel(documento, COMUNI, IDP, nivel1, nivel2, nivel3, nivel4, nivel5, nivel6, nivel7, nivel8, nivel9,
                                                                                    nivel10, nivel11, nivel12, nivel13, nivel14, nivel15);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComUnilevel(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComUnilevel(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComCI(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComCI(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearComCon(int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ResetearComCon(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MostrarIdPeriodoComisionActivo()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().MostrarIdPeriodoComisionActivo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int MostrarIdPeriodoPuntosActivo()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().MostrarIdPeriodoPuntosActivo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComCIRecalculo(string documento, double monto, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComCIRecalculo(documento, monto, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComConsultorRecalculo(string documento, double monto, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarComConsultorRecalculo(documento, monto, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerRangoIDPComision(string documento, int IDP)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerRangoIDPComision(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarMaximoRango(string documento, string Mrango, int IDRangoMax)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ActualizarMaximoRango(documento, Mrango, IDRangoMax);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Periodo.MaximoRango ObtenerRangoMaximoXSocio(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerRangoMaximoXSocio(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaTop10VQ(string dni, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaTop10VQ(dni, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaTop10Comisiones(string dni, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaTop10Comisiones(dni, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaGanadoresPremio(string dni, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaGanadoresPremio(dni, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaSociosComprimir(string dni, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaSociosComprimir(dni, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones.CalculoPuntos> ListaSociosCalculoPuntos(int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaSociosCalculoPuntos(idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerIdSocioByDocumento(string documento)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerIdSocioByDocumento(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarIdSocioUplinePuntos(string idsocio, string patrocinador)
        {
            try
            {
                PuntosComisionesDAO.getInstance().ActualizarIdSocioUplinePuntos(idsocio, patrocinador);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerIdPeriodoComisionAnteriorActivo()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerIdPeriodoComisionAnteriorActivo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaLineaMultinivelUpline(int idp, string idcliente)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaLineaMultinivelUpline(idp, idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaLineaMultinivelPatrocinio(int idp, string idcliente)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaLineaMultinivelPatrocinio(idp, idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones> ListaDatosRangoIndividual(string documento, int idp)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaDatosRangoIndividual(documento, idp);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistrarDatosRangoIndividual(string Documento, int IDP)
        {
            try
            {
                PuntosComisionesDAO.getInstance().RegistrarDatosRangoIndividual(Documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PuntosComisiones.DatosIndividualRango> ListaSociosRangoIndividual()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ListaSociosRangoIndividual();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarDatosRangoIndividual(int IdCalculo)
        {
            try
            {
                PuntosComisionesDAO.getInstance().EliminarDatosRangoIndividual(IdCalculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerEvaluacionCalculoRango(int IdCalculo)
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEvaluacionCalculoRango(IdCalculo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarEstadoTareaRango(bool Estado)
        {
            try
            {
                PuntosComisionesDAO.getInstance().ActualizarEstadoTareaRango(Estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ObtenerEstadoTareaRangos()
        {
            try
            {
                return PuntosComisionesDAO.getInstance().ObtenerEstadoTareaRangos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
