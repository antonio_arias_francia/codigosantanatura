﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class TipoClienteLN
    {
        #region PATRON SINGLETON
        private static TipoClienteLN objTipoCliente = null;
        private TipoClienteLN() { }
        public static TipoClienteLN getInstance()
        {
            if (objTipoCliente == null)
            {
                objTipoCliente = new TipoClienteLN();
            }
            return objTipoCliente;
        }
        #endregion

        public List<TipoCliente> ListarTipo()
        {
            try
            {
                return TipoClienteDAO.getInstance().ListarTipo();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}

