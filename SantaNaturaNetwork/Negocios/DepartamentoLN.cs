﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class DepartamentoLN
    {
        #region PATRON SINGLETON
        private static DepartamentoLN objDepartamento = null;
        private DepartamentoLN() { }
        public static DepartamentoLN getInstance()
        {
            if (objDepartamento == null)
            {
                objDepartamento = new DepartamentoLN();
            }
            return objDepartamento;
        }
        #endregion

        public List<Departamento> ListarDepartamento(string pais)
        {
            try
            {
                return DepartamentoDAO.getInstance().ListaDepartamento(pais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Departamento> GetDepartamentos()
        {
            try
            {
                return DepartamentoDAO.getInstance().GetDepartamento();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
