﻿using Datos;
using Modelos;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Negocios
{
    public class CompraNegocios
    {
        #region "PATRON SINGLETON"
        private static CompraNegocios _compraNegocios = null;
        private CompraNegocios() { }
        public static CompraNegocios getInstance()
        {
            if (_compraNegocios == null)
            {
                _compraNegocios = new CompraNegocios();
            }
            return _compraNegocios;
        }
        #endregion

        string error = "";

        public string RegistrarCompraAndDetalle(Compra compra, List<ProductoCarrito> listaCarrito)
        {

            try
            {
                return CompraDatos.getInstance().RegistrarCompraAndDetalle(compra, listaCarrito);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public string RegistrarCompraAndDetalleVisa(Compra compra, List<ProductoCarrito> listaCarrito)
        {
            try
            {
                return CompraDatos.getInstance().RegistrarCompraAndDetalleVisa(compra, listaCarrito);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool RegistrarVoucherTemporal(string usuario, string ruta) //Charles 25/03/2021
        {
            try
            {
                return CompraDatos.getInstance().RegistrarVoucherTemporal(usuario, ruta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public bool EliminarVoucherTemporal(string usuario) //Charles 25/03/2021
        {
            try
            {
                return CompraDatos.getInstance().EliminarVoucherTemporal(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Compra> ListaComprasPendientesDelClienteDeposito(string numDocCliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasPendientesDelClienteDeposito(numDocCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasPendientesAprobacionDelCliente(string numDocCliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasPendientesAprobacionDelCliente(numDocCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasPendientesEfectivo(string idcliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasPendientesEfectivo(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasAnuladasDelCliente(string numDocCliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasAnuladasDelCliente(numDocCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasRealizadasDelCliente(string numDocCliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasRealizadasDelCliente(numDocCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasRealizadasVisaNet(string idcliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasRealizadasVisaNet(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaPuntajeCompra(string idCliente, string fecha1, string fecha2)
        {
            try
            {
                return CompraDatos.getInstance().ListaPuntajePromo(idCliente, fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool RegistrarDepositoAndActualizarCompra(string ticket, string numOperacion, string banco, DateTime fechaVaucher,
                                                        double monto, string vaucherFoto, string idopPeruShop, string despacho2)
        {
            try
            {
                return CompraDatos.getInstance().RegistrarDepositoAndActualizarCompra(ticket, numOperacion, banco, fechaVaucher, monto, vaucherFoto, idopPeruShop, despacho2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarCompraMove(string ticket, string idMove, string liqui, string estadMove)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarCompraMove(ticket, idMove, liqui, estadMove);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarIdopYEstadoCompra(string ticket, string idDop)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarIdopYEstadoCompra(ticket, idDop);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string EnviarDatosSimplex(string id, string dni, string dniPatro, string packete, string idop, string idTipoCom, string nombre,
                                         string upline, string fechaSimpl)
        {
            List<DetalleCompra> listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(id);
            DateTime fechaSimple = Convert.ToDateTime(fechaSimpl);
            string send = fechaSimple.AddHours(-3).ToString("yyyy/MM/dd");

            string fecha = Convert.ToString(send);
            string paque = "";
            string tipoC = "";

            if (packete == "01")
            {
                paque = "BASICO";
            }
            else if (packete == "02")
            {
                paque = "PROFESIONAL";
            }
            else if (packete == "03")
            {
                paque = "EMPRESARIAL";
            }
            else if (packete == "04")
            {
                paque = "MILLONARIO";
            }
            else if (packete == "05")
            {
                paque = "CONSULTOR";
            }
            else
            {
                paque = "CONSUMIDOR";
            }


            /////////////////////////////////////////////////////

            if (idTipoCom == "01" | idTipoCom == "02" | idTipoCom == "03" | idTipoCom == "04" | idTipoCom == "05" | idTipoCom == "06")
            {
                tipoC = "AF";
            }
            else if (idTipoCom == "07")
            {
                tipoC = "PE";
            }
            else
            {
                tipoC = "UP";
                if (idTipoCom == "08")
                {
                    paque = "PROFESIONAL";
                }
                else if (idTipoCom == "09")
                {
                    paque = "EMPRESARIAL";
                }
                else if (idTipoCom == "10")
                {
                    paque = "MILLONARIO";
                }
                else if (idTipoCom == "11")
                {
                    paque = "EMPRESARIAL";
                }
                else if (idTipoCom == "12")
                {
                    paque = "MILLONARIO";
                }
                else
                {
                    paque = "MILLONARIO";
                }
            }

            string response = "";
            double sumaPunto = 0;
            double sumaPrecio = 0;
            try
            {
                foreach (var item in listaDetalleCompra)//22 y 56
                {
                    if (item.LineaSend == "02")
                    {
                        sumaPrecio = item.PrecioPS + sumaPrecio;
                    }

                    sumaPunto = item.Puntos + sumaPunto;

                }

                string sendPrecio = Convert.ToString(sumaPrecio);
                string sendPuntos = Convert.ToString(sumaPunto);

                string url = "http://18.219.245.17:8080/Sistema/index.php/WebServices/setResumenPeriodo";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("periodoID", "");
                nvc.Add("fecha", fecha);
                nvc.Add("usuarioID", "");
                nvc.Add("usuarioCod", "");
                nvc.Add("usuarioDNI", dni);
                nvc.Add("patrocinadorID", "");
                nvc.Add("patrocinadorCod", "");
                nvc.Add("patrocinadorDNI", dniPatro);
                nvc.Add("uplineID", "");
                nvc.Add("uplineCod", "");
                nvc.Add("uplineDNI", upline);
                nvc.Add("paqueteID", "");
                nvc.Add("paqueteCod", paque);
                nvc.Add("puntos", sendPuntos);
                nvc.Add("monto", sendPrecio);
                nvc.Add("referenciaPS", idop);
                nvc.Add("tipoOperacion", tipoC);
                nvc.Add("nombre", nombre);

                var data = wc.UploadValues(url, "POST", nvc);

                response = UnicodeEncoding.UTF8.GetString(data);
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return response;
        }

        public bool ActualizarDepositoDuplicado(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarDepositoDuplicado(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDepositoValidado(string ticket, int estado)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarDepositoValidado(ticket, estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Compra DatosCompraParaObtenerIdopPeruShop(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().DatosCompraParaObtenerIdopPeruShop(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasGeneral()
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasGeneral();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasGeneralMisCompras()
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasGeneralMisCompras();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasGeneralFiltrado(string fecha1, string fecha2)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasGeneralFiltrado(fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasEfectivo()
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasEfectivo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasEfectivoFiltrado(string fecha1, string fecha2)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasEfectivoFiltrado(fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<TipoPago> ListaTipoPago()
        {
            try
            {
                return CompraDatos.getInstance().ListaTipoPago();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasPendientesGeneral()
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasPendientesGeneral();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListarComprasAntiguas(string idcliente)
        {
            try
            {
                return CompraDatos.getInstance().ListarComprasAntiguas(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListarProductosxTicket(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().ListarProductosxTicket(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DatosPedidosVisaNet> ListarComprasxIdPago(string idpago)
        {
            try
            {
                return CompraDatos.getInstance().ListarPedidoXIdPago(idpago);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadPromo(string idCliente, string idPromo, string fecha1, string fecha2)
        {
            try
            {
                return CompraDatos.getInstance().CantidadPromo(idCliente, idPromo, fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadRegalo(string idCliente, string idPromo, string fecha1, string fecha2)
        {
            try
            {
                return CompraDatos.getInstance().CantidadRegalo(idCliente, idPromo, fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadCompras(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().CantidadCompras(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HoraCantidad> ObtenerHoraCompraStock(string IDPPS)
        {
            try
            {
                return CompraDatos.getInstance().ObtenerHoraCompraStock(IDPPS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string cantidadTotalStock(string IDPPS)
        {
            try
            {
                return CompraDatos.getInstance().cantidadTotalStock(IDPPS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Compra> DevolverVoucherTemporal(string usuario)
        {
            try
            {
                return CompraDatos.getInstance().DevolverVoucherTemporal(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public List<Compra> CDRxTicket(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().CDRxTicket(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarCompraCliente(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().EliminarCompraCliente(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarClienteSinCompras(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().EliminarClienteSinCompras(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPuntosComisiones(string documento, double PP, double VP, double VR, double VG, double monto, int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarPuntosComisiones(documento, PP, VP, VR, VG, monto, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPuntosVIP(string documento, double VIP, int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarPuntosVIP(documento, VIP, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionesAutomatico(string documento, double COMUNI, double COMCI, double COMCON, double COMAFI, double COMTIBU,
                                                   double COMBRON, double COMESCO, double COMMERCA, double NIVEL1, double NIVEL2, double NIVEL3,
                                                   double NIVEL4, double NIVEL5, int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarComisionesAutomatico(documento, COMUNI, COMCI, COMCON, COMAFI, COMTIBU, COMBRON,
                                                                                COMESCO, COMMERCA, NIVEL1, NIVEL2, NIVEL3, NIVEL4, NIVEL5, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComisionAfiliacion(string documento, double COMAFI, int IDP, double nivel1,
                                                double nivel2, double nivel3, double nivel4, double nivel5)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarComisionAfiliacion(documento, COMAFI, IDP, nivel1, nivel2, nivel3, nivel4, nivel5);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarComUniIdcliente(string idcliente, double COMAFI, int IDP, string nom_nivel,
                                                double monto_nivel)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarComUniIdcliente(idcliente, COMAFI, IDP, nom_nivel, monto_nivel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerMontoTiburon(string documento)
        {
            try
            {
                return CompraDatos.getInstance().ObtenerMontoTiburon(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double PuntosGastados(string idCliente, string idPromo, string fecha1, string fecha2)
        {
            try
            {
                return CompraDatos.getInstance().PuntosGastados(idCliente, idPromo, fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarVPR(string documento, int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarVPR(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Compra DatosPuntosYMontosCompra(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().DatosPuntosYMontosCompra(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaTicketXCliente(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaTicketXCliente(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaCompraAfiliacion(string fechaInicio, string fechaFin)
        {
            try
            {
                return CompraDatos.getInstance().ListaCompraAfiliacion(fechaInicio, fechaFin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprUnilevel(string fechaInicio, string fechaFin)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprUnilevel(fechaInicio, fechaFin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListarComprasAntiguasGeneral()
        {
            try
            {
                return CompraDatos.getInstance().ListarComprasAntiguasGeneral();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListarComprasComicionCI(int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ListarComprasComicionCI(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarMontoComisionCI(string ticket, double monto)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarMontoComisionCI(ticket, monto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListarComprasCalculoComisionCI(int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ListarComprasCalculoComisionCI(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListarComprasCalculoComisionConsultor(int IDP)
        {
            try
            {
                return CompraDatos.getInstance().ListarComprasCalculoComisionConsultor(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadComprasConIDOP(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().CantidadComprasConIDOP(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadComprasPendientes(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().CantidadComprasPendientes(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UltimoTicketXCliente(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().UltimoTicketXCliente(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //IDOP

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        private string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        //servicios de VISANET
        public string GenerateToken()
        {
            string user = "inteligencia.comercial@santanaturaperu.net";
            string password = "_$06we5R";
            string credentials = Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes(user + ":" + password));
            WebRequest request = WebRequest.Create("https://apiprod.vnforapps.com/api.security/v1/security");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Basic " + credentials);
            var httpResponse = (HttpWebResponse)request.GetResponse();
            var result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }

            return result;
        }

        public object getSessionToken(string token, string amount)
        {
            var merchantId = "602114115";
            var res = amount.Split(' ');
            WebRequest request = WebRequest.Create("https://apiprod.vnforapps.com/api.ecommerce/v2/ecommerce/token/session/" + merchantId);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", token);
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var obj = new Transaccion(res[1], null, "web", null);
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)request.GetResponse();
            var result = "";
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                result = streamReader.ReadToEnd();
            }
            var sesTOK = JsonConvert.DeserializeObject<Transaccion.sessionToken>(result);
            return sesTOK.sessionKey;
        }

        public Transaccion.DatosVisaNet getInfoUserByIdMerchant(string tokenID, string amount, string purchaseNumber, string authorizationToken)
        {
            var merchantId = "602114115";
            var res = amount.Split(' ');
            WebRequest request = WebRequest.Create("https://apiprod.vnforapps.com/api.authorization/v3/authorization/ecommerce/" + merchantId);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", authorizationToken);
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                Transaccion.infoByUserMerchantId.Order obOrder = new Transaccion.infoByUserMerchantId.Order(amount, tokenID, purchaseNumber, "PEN");
                var obj = new Transaccion.infoByUserMerchantId(null, "manual", "web", false, obOrder);
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
            }
            HttpWebResponse httpResponse;
            var result = "";
            Transaccion.InfoUserByIdMerchant sesTOK = new Transaccion.InfoUserByIdMerchant();
            Transaccion.ErrorResponse sesError = new Transaccion.ErrorResponse();
            Transaccion.DatosVisaNet EnviarDatos = new Transaccion.DatosVisaNet();
            try
            {
                httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                sesTOK = JsonConvert.DeserializeObject<Transaccion.InfoUserByIdMerchant>(result);
                EnviarDatos.Transaction_Date = sesTOK.dataMap.TRANSACTION_DATE;
                EnviarDatos.Transaction_ID = sesTOK.dataMap.TRANSACTION_ID;
                EnviarDatos.Merchant = sesTOK.dataMap.MERCHANT;
                EnviarDatos.Id_Unico = sesTOK.dataMap.ID_UNICO;
                EnviarDatos.CardS = sesTOK.dataMap.CARD;
                EnviarDatos.Aauthorization_Code = sesTOK.dataMap.AUTHORIZATION_CODE;
                EnviarDatos.AMOUNT = sesTOK.dataMap.AMOUNT;
                EnviarDatos.CURRENCY = sesTOK.dataMap.CURRENCY;
                EnviarDatos.BRAND = sesTOK.dataMap.BRAND;
                EnviarDatos.STATUSS = sesTOK.dataMap.STATUS;
                EnviarDatos.ACTION_DESCRIPTION = sesTOK.dataMap.ACTION_DESCRIPTION;

            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                sesError = JsonConvert.DeserializeObject<Transaccion.ErrorResponse>(result);
                EnviarDatos.Transaction_Date = sesError.data.TRANSACTION_DATE;
                EnviarDatos.Merchant = sesError.data.MERCHANT;
                EnviarDatos.CardS = sesError.data.CARD;
                EnviarDatos.AMOUNT = sesError.data.AMOUNT;
                EnviarDatos.CURRENCY = sesError.data.CURRENCY;
                EnviarDatos.STATUSS = sesError.data.STATUS;
                EnviarDatos.ACTION_DESCRIPTION = sesError.data.ACTION_DESCRIPTION;

            }



            return EnviarDatos;
        }

        //fin de servicios VISANET
        public string getIdPurchase()
        {
            try
            {
                return CompraDatos.getInstance().getIdPago();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool savePurchase(string idpago, string idcliente, string idproducto, string precioProducto, string token)
        {
            try
            {
                return CompraDatos.getInstance().SavePagoPorCliente(idpago, idcliente, idproducto, precioProducto, token);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

        //PAGO EFECTIVO

        public object getTokenPE(string accessKey, string idService, string secretKey, string date)
        {
            var parametro = idService + "." + accessKey + "." + secretKey + "." + date;
            var hash = HashString(parametro);
            var result = "";
            var sesTOK = JsonConvert.DeserializeObject<TokenPE>(result);
            WebRequest request = WebRequest.Create("https://services.pagoefectivo.pe/v1/authorizations");
            request.Method = "POST";
            request.ContentType = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var obj = new Authenticate(accessKey, idService, date, hash);
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
            }
            try {
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                sesTOK = JsonConvert.DeserializeObject<TokenPE>(result);
            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }

            return sesTOK.data.token;
        }

        public static String HashString(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (SHA256 hash = SHA256.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }

        public TokenPE.GetObjects GetCIP(Authenticate.Data objectsCIP, string token)
        {
            WebRequest request = WebRequest.Create("https://services.pagoefectivo.pe/v1/cips");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Bearer " + token);
            request.Headers.Add("Accept-Language", "es-PE");
            request.Headers.Add("Origin", "web");
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var obj = objectsCIP;
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
            }
            HttpWebResponse httpResponse;
            var result = "";
            TokenPE.GoodObjects resOK = new TokenPE.GoodObjects();
            TokenPE.BadObjects resBad = new TokenPE.BadObjects();
            TokenPE.GetObjects EnviarDatos = new TokenPE.GetObjects();
            try
            {
                httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                resOK = JsonConvert.DeserializeObject<TokenPE.GoodObjects>(result);
                EnviarDatos.code = resOK.code;
                EnviarDatos.message = resOK.message;
                EnviarDatos.cip = resOK.data.cip;
                EnviarDatos.currency = resOK.data.currency;
                EnviarDatos.amount = resOK.data.amount;
                EnviarDatos.transactionCode = resOK.data.transactionCode;
                EnviarDatos.dateExpiry = resOK.data.dateExpiry;
                EnviarDatos.cipURL = resOK.data.cipURL;

            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                EnviarDatos.message = result;
                EnviarDatos.field = result;

            }

            return EnviarDatos;
        }

        public bool ConfirmarCorreo(string correo)
        {
            string expresion = @"\A(([a-zA-Z0-9])((\.|\-|_)?[a-zA-Z0-9])*)+(\@)(gmail|hotmail|outlook|yahoo)(\.)(com|net|es)\Z";
            if (Regex.IsMatch(correo, expresion))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void RegistroDatosPagoEfectivo(DatosPagoEfectivo objDatos)
        {
            try
            {
                CompraDatos.getInstance().RegistroDatosPagoEfectivo(objDatos);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public List<Compra> ListaComprasPendientesPagoEfectivo(string idcliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasPendientesPagoEfectivo(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaComprasRealizadasPagoEfectivo(string idcliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaComprasRealizadasPagoEfectivo(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDatosCompraPE(string cip, string fechapago, string numoperacion)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarDatosCompraPE(cip, fechapago, numoperacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaCompraDatosPagoEfectivoPendiente()
        {
            try
            {
                return CompraDatos.getInstance().ListaCompraDatosPagoEfectivoPendiente();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstadoPagoEfectivo(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().ActualizarEstadoPagoEfectivo(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ReestablecerEstadoPagoEfectivo(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().ReestablecerEstadoPagoEfectivo(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra> ListaCompraDatosxTicket(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().ListaCompraDatosxTicket(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ConsultaIdRUC(string documento)
        {
            try
            {
                return CompraDatos.getInstance().ConsultaIdRUC(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroIdRuc(string documento, string idruc, DateTime fecha)
        {
            try
            {
                return CompraDatos.getInstance().RegistroIdRuc(documento, idruc, fecha);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra.NuevoCIP> ListaDatosNuevoCIP(string ticket)
        {
            try
            {
                return CompraDatos.getInstance().ListaDatosNuevoCIP(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDatosPagoEfectivo(DatosPagoEfectivo objDatos)
        {
            try
            {
                CompraDatos.getInstance().ActualizarDatosPagoEfectivo(objDatos);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public List<Compra.Facturacion> ListaFacturacionSocios(string idCliente)
        {
            try
            {
                return CompraDatos.getInstance().ListaFacturacionSocios(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegistrarFacturacionSocios(Compra.Facturacion objFactura)
        {
            try
            {
                CompraDatos.getInstance().RegistrarFacturacionSocios(objFactura);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public List<Compra.Facturacion> ListaCanjesAbono(int periodo)
        {
            try
            {
                return CompraDatos.getInstance().ListaCanjesAbono(periodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Compra.Facturacion> ListaDatalleCanjesAbono(int idDatos)
        {
            try
            {
                return CompraDatos.getInstance().ListaDatalleCanjesAbono(idDatos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDetalleCanjesAbono(Compra.Facturacion objFactura)
        {
            try
            {
                CompraDatos.getInstance().ActualizarDetalleCanjesAbono(objFactura);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public void EliminarNombrePDF_ControlCanjes(int idDatos)
        {
            try
            {
                CompraDatos.getInstance().EliminarNombrePDF_ControlCanjes(idDatos);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public void SellarRegistro_ControlCanjes(int idDatos)
        {
            try
            {
                CompraDatos.getInstance().SellarRegistro_ControlCanjes(idDatos);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public void EliminarRegistro_ControlCanjes(int idDatos)
        {
            try
            {
                CompraDatos.getInstance().EliminarRegistro_ControlCanjes(idDatos);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public void ActualizarFacturacionSocios(Compra.Facturacion objFactura)
        {
            try
            {
                CompraDatos.getInstance().ActualizarFacturacionSocios(objFactura);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public List<Compra.PendienteFacturacion> ListaSociosPendienteFacturas(int periodo)
        {
            try
            {
                return CompraDatos.getInstance().ListaSociosPendienteFacturas(periodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarCorazonesRed(int idp, string idcliente, double corazones)
        {
            try
            {
                CompraDatos.getInstance().ActualizarCorazonesRed(idp, idcliente, corazones);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public void ActualizarEstadoTareaCompras(bool Estado)
        {
            try
            {
                CompraDatos.getInstance().ActualizarEstadoTareaCompras(Estado);
            }
            catch (Exception e)
            {
                error = e.Message;
            }
        }

        public bool ObtenerEstadoTareaCompras()
        {
            try
            {
                return CompraDatos.getInstance().ObtenerEstadoTareaCompras();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadTicket_DPE(string ticket, string idcliente)
        {
            try
            {
                return CompraDatos.getInstance().CantidadTicket_DPE(ticket, idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}    

