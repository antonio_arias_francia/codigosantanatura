﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class PromocionLN
    {
        #region PATRON SINGLETON
        private static PromocionLN objPromocion = null;
        private PromocionLN() { }
        public static PromocionLN getInstance()
        {
            if (objPromocion == null)
            {
                objPromocion = new PromocionLN();
            }
            return objPromocion;
        }
        #endregion

        public bool RegistroPromocion(Promocion objPromo)
        {
            try
            {
                return PromocionDAO.getInstance().RegistroPromocion(objPromo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPromocion(Promocion objPromo)
        {
            try
            {
                return PromocionDAO.getInstance().ActualizarPromocion(objPromo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Promocion> ListarPromo()
        {
            try
            {
                return PromocionDAO.getInstance().ListarPromo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Promocion> ListaValidarPromo()
        {
            try
            {
                return PromocionDAO.getInstance().ListaValidarPromo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListarCantPromo()
        {
            try
            {
                return PromocionDAO.getInstance().ListaCantPromo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ListarCodigoPromoActiva()
        {
            try
            {
                return PromocionDAO.getInstance().ListaCodigoPromoActiva();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
