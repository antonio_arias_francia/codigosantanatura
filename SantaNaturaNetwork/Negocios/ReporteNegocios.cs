﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class ReporteNegocios
    {

        #region "PATRON SINGLETON"
        private static ReporteNegocios _reporteNegocios = null;
        private ReporteNegocios() { }
        public static ReporteNegocios getInstance()
        {
            if (_reporteNegocios == null)
            {
                _reporteNegocios = new ReporteNegocios();
            }
            return _reporteNegocios;
        }
        #endregion


        public List<ReporteCDRModelo> ListaReporteCDR(string fecha1, string fecha2)
        {
            try
            {
                return ReportesDatos.getInstance().ListaReporteCDR(fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ReporteSimplexModelo> ListaReporteSimplex(string fecha1, string fecha2)
        {
            try
            {
                return ReportesDatos.getInstance().ListaReporteSimplex(fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
