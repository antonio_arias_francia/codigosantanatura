﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class DescuentoNegocios
    {
        #region "PATRON SINGLETON"
        private static DescuentoNegocios _descuentoNegocios = null;
        private DescuentoNegocios() { }
        public static DescuentoNegocios getInstance()
        {
            if (_descuentoNegocios == null)
            {
                _descuentoNegocios = new DescuentoNegocios();
            }
            return _descuentoNegocios;
        }
        #endregion

        public bool RegistrarPacketeCliente(Descuento descuento)
        {
            bool response = false;
            try
            {
                response = DescuentoDatos.getInstance().RegistrarPacketeCliente(descuento);
            }
            catch (Exception ex) 
            {
                throw ex;
            }

            return response;
        }
    }
}
