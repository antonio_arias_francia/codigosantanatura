﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Datos;

namespace Negocios
{
    public class ElementosTreeGridLN
    {
        #region PATRON SINGLETON
        private static ElementosTreeGridLN objSocio = null;
        private ElementosTreeGridLN() { }
        public static ElementosTreeGridLN getInstance()
        {
            if (objSocio == null)
            {
                objSocio = new ElementosTreeGridLN();
            }
            return objSocio;
        }
        #endregion

        public List<ElementosTreeGrid> ListaTreeGrid(string documento)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeGrid(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeGridByPeriodo(string documento, int IDP)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeGridByPeriodo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeGridPatrocinio(string documento)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeGridPatrocinador(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeGridSocio(string documento)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeGridSocio(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeGridConsultor(string documento)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeGridConsultor(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeGridCCI(string documento)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeGridCCI(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeTable(string idCliente, int condicion, int idPeriodo)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeTable(idCliente, condicion, idPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<ElementosTreeGrid> ListaTreeTableHistorico(string idCliente, int condicion, int idPeriodo)
        {
            try
            {
                return ElementosTreeGridDAO.getInstance().ListaTreeTableHistorico(idCliente, condicion, idPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
