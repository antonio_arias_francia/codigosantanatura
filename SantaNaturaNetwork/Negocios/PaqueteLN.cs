﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using Datos;

namespace Negocios
{
    public class PaqueteLN
    {
        #region PATRON SINGLETON
        private static PaqueteLN objPaquete = null;
        private PaqueteLN() { }
        public static PaqueteLN getInstance()
        {
            if (objPaquete == null)
            {
                objPaquete = new PaqueteLN();
            }
            return objPaquete;
        }
        #endregion

        public List<PaqueteNatura> ListaPaquetes()
        {
            try
            {
                return PaqueteDAO.getInstance().ListarPaquetes();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaqueteNatura> ListaPaqueteByCodigoPeru(string idPaquete)
        {
            try
            {
                return PaqueteDAO.getInstance().ListarPaqueteByCodigoPeru(idPaquete);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroPaquete(PaqueteNatura objPaquete, PaquetePais objPaquetePais)
        {
            try
            {
                return PaqueteDAO.getInstance().RegistroPaquete(objPaquete, objPaquetePais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPaquete(PaqueteNatura objPaquete, PaquetePais objPaquetePais)
        {
            try
            {
                return PaqueteDAO.getInstance().ActualizarPaquete(objPaquete, objPaquetePais);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
