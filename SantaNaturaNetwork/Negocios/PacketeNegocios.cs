﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class PacketeNegocios
    {
        #region PATRON SINGLETON
        private static PacketeNegocios _packeteNegocios = null;
        private PacketeNegocios() { }
        public static PacketeNegocios getInstance()
        {
            if (_packeteNegocios == null)
            {
                _packeteNegocios = new PacketeNegocios();
            }
            return _packeteNegocios;
        }
        #endregion

        public List<Packete> ListarPackete(string packete, string tipoCiente)
        {
            try
            {
                return PacketeDatos.getInstance().ListarPacketes(packete, tipoCiente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Packete> ListarPacketes_PreRegistro(string packete, string tipoCiente)
        {
            try
            {
                return PacketeDatos.getInstance().ListarPacketes_PreRegistro(packete, tipoCiente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Packete> ListarPacketeGeneral()
        {
            try
            {
                return PacketeDatos.getInstance().ListadoPacketesGeneral();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
