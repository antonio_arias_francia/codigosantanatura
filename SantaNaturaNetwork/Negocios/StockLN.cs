﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class StockLN
    {
        #region PATRON SINGLETON
        private static StockLN objCDR = null;
        private StockLN() { }
        public static StockLN getInstance()
        {
            if (objCDR == null)
            {
                objCDR = new StockLN();
            }
            return objCDR;
        }
        #endregion

        public List<StockCDR> ListarCDR()
        {
            try
            {
                return StockDAO.getInstance().ListadoCDR();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockCDR> ListarCDRStock()
        {
            try
            {
                return StockDAO.getInstance().ListarCDRStock();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockCDR> ListarProductosRegistro()
        {
            try
            {
                return StockDAO.getInstance().ListarProductosRegistro();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockCDR> ListarProductosPedidoCDR(string pais)
        {
            try
            {
                return StockDAO.getInstance().ListarProductosPedidoCDR(pais);
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }

        public List<StockCDR> ListarSolicitudesGeneradas(string dni)
        {
            try
            {
                return StockDAO.getInstance().ListarSolicitudesGeneradas(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockCDR> ListarSolicitudesTotales(string fecha1, string fecha2)
        {
            try
            {
                return StockDAO.getInstance().ListarSolicitudesTotales(fecha1, fecha2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockCDR> ListarProductosxCDR(string cdr)
        {
            try
            {
                return StockDAO.getInstance().ListaProductosXCDR(cdr);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<StockCDR> ListarProductosxSolicitud(int idSolicitud)
        {
            try
            {
                return StockDAO.getInstance().ListaProductosXSolicitud(idSolicitud);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroStock(StockCDR stock)
        {
            try
            {
                return StockDAO.getInstance().RegistroStock(stock);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroProductoSolicitud(StockCDR stock)
        {
            try
            {
                return StockDAO.getInstance().RegistroProductoSolicitud(stock);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GenerarIDSolicitud(string dni, string fecha, string TipoCompra, double monto)
        {
            try
            {
                return StockDAO.getInstance().GenerarIDSolicitud(dni, fecha, TipoCompra, monto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarStock(StockCDR stock)
        {
            try
            {
                return StockDAO.getInstance().ActualizarStock(stock);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarProductosenStock(StockCDR stock)
        {
            try
            {
                return StockDAO.getInstance().ActualizarProductoenStock(stock);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DescontarStock(string dniCdr, string idProducto, int cantidad)
        {
            try
            {
                return StockDAO.getInstance().DescontarStock(dniCdr, idProducto, cantidad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IncrementarStock(string dniCdr, string idProducto, int cantidad)
        {
            try
            {
                return StockDAO.getInstance().IncrementarStock(dniCdr, idProducto, cantidad); 
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstadoSolicitud(int idSoli, string idop, string idop2, string fechaVal)
        {
            try
            {
                return StockDAO.getInstance().ActualizarEstadoSolicitud(idSoli, idop, idop2, fechaVal);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarEstadoSolicitudPE(int idSoli, string idop)
        {
            try
            {
                StockDAO.getInstance().ActualizarEstadoSolicitudPE(idSoli, idop);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadStockCDR(string dniCDR, string IDPS, string pais, string IDPP)
        {
            try
            {
                return StockDAO.getInstance().CantidadStockCDR(dniCDR, IDPS, pais, IDPP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UnidadPorPresentacionXProducto(string idProducto)
        {
            try
            {
                return StockDAO.getInstance().UnidadPorPresentacionXProducto(idProducto);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ObtenerControlStockXIDPP(string Idpp, string DniCDR)
        {
            try
            {
                return StockDAO.getInstance().ObtenerControlStockXIDPP(Idpp, DniCDR);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerPrecioCdrXIDPP(string idPP)
        {
            try
            {
                return StockDAO.getInstance().ObtenerPrecioCdrXIDPP(idPP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public double ObtenerPrecioXidPP_SD(string idPP)
        {
            try
            {
                return StockDAO.getInstance().ObtenerPrecioXidPP_SD(idPP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EliminarPedidoCDR(int idSoli)
        {
            try
            {
                StockDAO.getInstance().EliminarPedidoCDR(idSoli);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
