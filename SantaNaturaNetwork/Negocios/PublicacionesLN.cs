﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class PublicacionesLN
    {
        #region PATRON SINGLETON
        private static PublicacionesLN objPublicacion = null;
        private PublicacionesLN() { }
        public static PublicacionesLN getInstance()
        {
            if (objPublicacion == null)
            {
                objPublicacion = new PublicacionesLN();
            }
            return objPublicacion;
        }
        #endregion

        public bool RegistroPublicacion(Publicacion objPublicacion)
        {
            try
            {
                return PublicacionesDAO.getInstance().RegistroPublicaciones(objPublicacion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPublicacion(Publicacion objPublicacion)
        {
            try
            {
                return PublicacionesDAO.getInstance().ActualizarPublicacion(objPublicacion);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Publicacion> ListaPublicacion()
        {
            try
            {
                return PublicacionesDAO.getInstance().ListaPublicacion();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool EliminarPublicacion(string id)
        {
            try
            {
                return PublicacionesDAO.getInstance().EliminarPublicacion(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
