﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class PaisLN
    {
        #region PATRON SINGLETON
        private static PaisLN objPais = null;
        private PaisLN() { }
        public static PaisLN getInstance()
        {
            if (objPais == null)
            {
                objPais = new PaisLN();
            }
            return objPais;
        }
        #endregion

        public List<Pais> ListarPais()
        {
            try
            {
                return PaisDAO.getInstance().ListadoPais();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
