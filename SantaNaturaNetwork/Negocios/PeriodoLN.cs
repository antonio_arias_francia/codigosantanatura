﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class PeriodoLN
    {
        #region PATRON SINGLETON
        private static PeriodoLN objPeriodo = null;
        private PeriodoLN() { }
        public static PeriodoLN getInstance()
        {
            if (objPeriodo == null)
            {
                objPeriodo = new PeriodoLN();
            }
            return objPeriodo;
        }
        #endregion

        public bool RegistroPeriodo(Periodo objPeriodo)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroPeriodo(objPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPeriodo(Periodo objPeriodo)
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarPeriodo(objPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodo()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarPeriodo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListarCantPeriodo()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaCantPeriodo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ListarCodigoPeriodoActiva()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaCodigoPeriodoActivo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroClientePeriodo(string documento, string upline, string idCliente, string idUpline, 
                                           string TipoC, string patrocinador)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroCLientePeriodo(documento, upline, idCliente, idUpline, TipoC, patrocinador);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDocumentoPeriodo(string documento, string idCliente) 
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarDocumentoPeriodo(documento, idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarUplinePeriodo(string documento, string idCliente, int IDP)
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarUplinePeriodo(documento, idCliente, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroClientePeriodoActivo(string documento, string upline, string TipoC)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroCLientePeriodoActivo(documento, upline, TipoC);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroClientePeriodoMultiple(string documento, string upline, int idperiodo, string TipoC, string patrocinador)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroCLientePeriodoMultiple(documento, upline, idperiodo, TipoC, patrocinador);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarIdPeriodo()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarIdPeriodo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarIdPeriodoTop2()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarIdPeriodoTop2();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodosTotales()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaPeriodosTotales();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodosTotalesByCliente()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaPeriodosTotalesByCliente();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*PERIODO COMISION*/

        public bool RegistroPeriodoComision(Periodo objPeriodo)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroPeriodoComision(objPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarPeriodoComision(Periodo objPeriodo)
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarPeriodoComision(objPeriodo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodoComision()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarPeriodoComision();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ListarCodigoPeriodoActivaComision()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaCodigoPeriodoActivoComision();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ListarCantPeriodoComision()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaCantPeriodoComision();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroClientePeriodoComision(string documento, string upline, string patrocinador, string idCliente, string idUpline, string TipoC)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroCLientePeriodoComision(documento, upline, patrocinador, idCliente, idUpline, TipoC);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroSociosRetencion(string idSocio, string dni)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroSociosRetencion(idSocio, dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDocumentoPeriodoComision(string documento, string idCliente)
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarDocumentoPeriodoComision(documento, idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDocumentoRetencion(string documento, string idCliente)
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarDocumentoRetencion(documento, idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarUplinePeriodoComision(string documento, string idCliente, int IDP)
        {
            try
            {
                return PeriodoDAO.getInstance().ActualizarUplinePeriodoComision(documento, idCliente, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroClientePeriodoMultipleComision(string documento, string upline, string patrocinador, int idperiodo, string TipoC)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroCLientePeriodoMultipleComision(documento, upline, patrocinador, idperiodo, TipoC);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroSocioRetencionXPeriodo(string dni, int IDP)
        {
            try
            {
                return PeriodoDAO.getInstance().RegistroSocioRetencionXPeriodo(dni, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarIdPeriodoComision()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarIdPeriodoComision();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodoComisionTop3()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarPeriodoComisionTop3();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodosCalculoComision()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarPeriodosCalculoComision();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodoComisionCombo()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaPeriodoComisionCombo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarPeriodosTotalesByClienteComision()
        {
            try
            {
                return PeriodoDAO.getInstance().ListaPeriodosTotalesByClienteComision();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerPeriodoxComision(int IDP)
        {
            try
            {
                return PeriodoDAO.getInstance().ObtenerPeriodoxComision(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarUltimos4Periodos()
        {
            try
            {
                return PeriodoDAO.getInstance().ListarUltimos4Periodos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Periodo> ListarUltimos4PeriodosRecalculo(int IDP)
        {
            try
            {
                return PeriodoDAO.getInstance().ListarUltimos4PeriodosRecalculo(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerIdperiodoRango_Comi(int IDP)
        {
            try
            {
                return PeriodoDAO.getInstance().ObtenerIdperiodoRango_Comi(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
