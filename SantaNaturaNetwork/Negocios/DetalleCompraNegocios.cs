﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class DetalleCompraNegocios
    {
        #region "PATRON SINGLETON"
        private static DetalleCompraNegocios _DetalleCompraNegocios = null;
        private DetalleCompraNegocios() { }
        public static DetalleCompraNegocios getInstance()
        {
            if (_DetalleCompraNegocios == null)
            {
                _DetalleCompraNegocios = new DetalleCompraNegocios();
            }
            return _DetalleCompraNegocios;
        }
        #endregion

        public List<DetalleCompra> ListaDetalleCompraDelCliente(string numDocCliente)
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListaDetalleCompraDelCliente(numDocCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DetalleCompra> ListaDetalleCompraGeneral(string ticket)
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListaDetalleCompraGeneral(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DetalleCompra> ListaCompraPendientePagoDelCliente(string numDocCliente)
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListaCompraPendientePagoDelCliente(numDocCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DetalleCompra> ListaTipoCompraDetalle()
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListadoTipoCompraDetalle();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DetalleCompra> ListaTipoPagoDetalle()
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListadoTipoPagoDetalle();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DetalleCompra> ListaDespachoDetalle()
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListadoDespachoDetalle();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DetalleCompra> ListaEstadoDetalle()
        {
            try
            {
                return DetalleCompraDatos.getInstance().ListadoEstadoDetalle();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarCompraDetalle(DetalleCompra objCompra)
        {
            try
            {
                return DetalleCompraDatos.getInstance().ActualizarCompraDetalle(objCompra);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarCompraDetalle(string ticket)
        {
            try
            {
                return DetalleCompraDatos.getInstance().RetornarCompra(ticket);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
