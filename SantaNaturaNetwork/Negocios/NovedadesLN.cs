﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Modelos;

namespace Negocios
{
    public class NovedadesLN
    {
        #region PATRON SINGLETON
        private static NovedadesLN objNovedad = null;
        private NovedadesLN() { }
        public static NovedadesLN getInstance()
        {
            if (objNovedad == null)
            {
                objNovedad = new NovedadesLN();
            }
            return objNovedad;
        }
        #endregion

        public bool RegistroNovedades(Novedad objNovedad)
        {
            try
            {
                return NovedadesDAO.getInstance().RegistroNovedades(objNovedad);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarNovedades(Novedad objNovedad)
        {
            try
            {
                return NovedadesDAO.getInstance().ActualizarNovedad(objNovedad);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Novedad> ListaNovedades()
        {
            try
            {
                return NovedadesDAO.getInstance().ListaNovedad();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool EliminarNovedad(string id)
        {
            try
            {
                return NovedadesDAO.getInstance().EliminarNovedad(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
