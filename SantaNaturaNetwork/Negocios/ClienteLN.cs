﻿using Datos;
using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocios
{
    public class ClienteLN
    {
        #region PATRON SINGLETON
        private static ClienteLN objSocio = null;
        private ClienteLN() { }
        public static ClienteLN getInstance()
        {
            if (objSocio == null)
            {
                objSocio = new ClienteLN();
            }
            return objSocio;
        }
        #endregion

        public Cliente AccesoSistema(string usuario, string clave)
        {
            try
            {
                return ClienteDAO.getInstance().AccesoSistema(usuario, clave);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool RegistroCliente(Cliente objCliente, Packete objPackete)
        {
            try
            {
                return ClienteDAO.getInstance().RegistroCliente(objCliente, objPackete);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void procesoUnilevel(int IDP)
        {
            try
            {
                ClienteDAO.getInstance().procesoUnilevel(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string ValidadCaducidadClave(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ValidadCaducidadClave(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ValidadPreguntasSeguridadUsuario(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ValidadPreguntasSeguridadUsuario(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool ActualizarCLiente(Cliente objCliente)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarCliente(objCliente);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool ActualizarPerfilSocio(Cliente objCliente)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarPerfilSocio(objCliente);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool ActualizarCLienteR(Cliente objCliente)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarClienteR(objCliente);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public bool ActualizarDescuento(string idcliente, string packete)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarDescuento(idcliente, packete);
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        public List<Cliente> ListaEstablecimiento()
        {
            try
            {
                return ClienteDAO.getInstance().ListaEstablecimiento();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente> ListaEstablecimientoColombia()
        {
            try
            {
                return ClienteDAO.getInstance().ListaEstablecimientoColombia();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente> ListaEstable()
        {
            try
            {
                return ClienteDAO.getInstance().ListaEstable();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente> ListarUpline(string numDocumento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarUpline(numDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente> ListarUplinePreRegistro(string numDocumento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarUplinePreRegistro(numDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Cliente> ListarCliente()
        {
            try
            {
                return ClienteDAO.getInstance().ListarCliente();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarCliente100()
        {
            try
            {
                return ClienteDAO.getInstance().ListarCliente100();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteFiltrado(string dni, string nombres)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteFiltro(dni, nombres);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteByDocumento(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteByDocumento(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ValidarClaveUsuario(string usuario, string clave,string tipo)
        {
            try
            {
                return ClienteDAO.getInstance().ValidarClaveUsuario(usuario,clave,tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ValidarClaveUsuario2(string usuario, string clave, string tipo)
        {
            try
            {
                return ClienteDAO.getInstance().ValidarClaveUsuario2(usuario, clave, tipo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ActualizarClaveUsuario(string usuario, string clave)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarClaveUsuario(usuario, clave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ActualizarPreguntasSeguridad(string usuario, string pregunta, string respuesta)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarPreguntasSeguridad(usuario, pregunta, respuesta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public string ActualizarClaveUsuario2(string usuario, string clave)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarClaveUsuario2(usuario, clave);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //public string ValidarPreguntasXUsuario(string usuario, string pregunta, string respuesta)
        //{
        //    try
        //    {
        //        return ClienteDAO.getInstance().ValidarPreguntasXUsuario(usuario, pregunta,respuesta);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        public List<Cliente> DNIEXISTE()
        {
            try
            {
                return ClienteDAO.getInstance().DNIEXISTE();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<string> AutocompletePatro(string palabra)
        {
            try
            {
                return ClienteDAO.getInstance().AutocompletePatro(palabra);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool EliminarCliente(string id, int idpaquete)
        {
            try
            {
                return ClienteDAO.getInstance().Eliminarcliente(id, idpaquete);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Eliminarcliente_DPE(string id)
        {
            try
            {
                ClienteDAO.getInstance().Eliminarcliente_DPE(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstadoCliente(string numDocCliente, bool estado)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarEstadoCliente(numDocCliente, estado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Cliente GetClienteByNumDocumento(string numDocumento)
        {
            try
            {
                return ClienteDAO.getInstance().GetClienteByNumDocumento(numDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Cliente ValidarPatrocinador(string nombres)
        {
            try
            {
                return ClienteDAO.getInstance().ValidarPatrocinador(nombres);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Cliente DatosClienteSimplex(string id)
        {
            try
            {
                return ClienteDAO.getInstance().DatosClienteSimplex(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string RegistrarAfiliacion(Cliente cliente, Descuento descuento)
        {
            try
            {
                return ClienteDAO.getInstance().RegistroClienteCarrito(cliente, descuento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EstructuraRedGeneral(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().EstructuraRedGeneral(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EstructuraRedGeneralCI(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().EstructuraRedGeneralCI(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable EstructuraRedGeneralConsultor(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().EstructuraRedGeneralConsultor(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MomentaneoComi ComisionMomentanea(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ComisionMomentanea(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClientexRango()
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRango();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteCalculoPuntos()
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRedCalculoPuntos();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteCalculoPuntosPeriodo(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRedCalculoPuntosPeriodo(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ComAfiliacion ListaDatosPatrocinioAfiliaciones(int IDP, string dni)
        {
            try
            {
                return ClienteDAO.getInstance().ListaDatosPatrocinioAfiliaciones(IDP, dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClientePeriodoVP()
        {
            try
            {
                return ClienteDAO.getInstance().ListarClientePeriodoVP();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.RecalculoPuntos> ListarClienteRecalculoPuntos(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRecalculoPuntos(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteRedVQ(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRedVQ(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarDatosIndex(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarDatosIndex(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarDatosRangoProximo(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarDatosRangoProximo(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteRedVQPeriodo(string documento, int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRedVQPeriodo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarDirectosUpline(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarDirectosUpline(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClientePeriodo()
        {
            try
            {
                return ClienteDAO.getInstance().ListarClientePeriodo();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ListarFactorComision(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarFactorComision(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string SociosActivosIndex(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().SociosActivosIndex(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DirectosActivosIndex(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().DirectosActivosIndex(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerFechaRegistro(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ObtenerFechaRegistro(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CantidadAfiliacionesGeneradas(string documento, string fechaInicio, string fechaFin)
        {
            try
            {
                return ClienteDAO.getInstance().CantidadAfiliacionesGeneradas(documento, fechaInicio, fechaFin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstadoTiburon(string dni, int tiburon)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarEstadoTiburon(dni, tiburon);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEstadoTiburonRecalculo(string dni, int tiburon, int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarEstadoTiburonRecalculo(dni, tiburon, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarEvalTiburonFec(string dni, int tiburon, int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarEvalTiburonFec(dni, tiburon, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteTiburon(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteTiburon(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteConCompras()
        {
            try
            {
                /*REVISAR ESTE CALCULO Y MODIFICAR LA FECHA*/
                return ClienteDAO.getInstance().ListarClienteConCompras();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteConComprasRecalculo(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteConComprasRecalculo(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteBonoEscolaridad(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteBonoEscolaridad(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteBonoBronce(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteBonoBronce(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PeriodoComision> ListarClienteBonoMercadeo(string FI0, string FF0, string FI1, string FF1,
                                                       string FI2, string FF2, string FI3, string FF3, int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteBonoMercadeo(FI0, FF0, FI1, FF1, FI2, FF2, FI3, FF3, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteConComprasRecalculoBronce(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteConComprasRecalculoBronce(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarDniCDR(string dni, string antiguo)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarDniCDR(dni, antiguo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetearEstablecimientoFavorito(string dni)
        {
            try
            {
                return ClienteDAO.getInstance().ResetearEstablecimientoFavorito(dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PatrocinadorUnilivel ListaDatosUplineUnilevel(int IDP, string dni)
        {
            try
            {
                return ClienteDAO.getInstance().ListaDatosUplineUnilevel(IDP, dni);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteRetencion(int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteRetencion(IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DatosSocios> ListarCantidadDirectos(string numDocumento, int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ListarCantidadDirectos(numDocumento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<Cliente> ListadoPreguntasUsuario(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ListadoPreguntasUsuario(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<Cliente> ListadoPreguntas(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ListadoPreguntas(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActualizarRetencionSocio(int SocioDirectoTotal, int SocioDirectoActivo, int SocioDirectoNuevo,
                                            int ConsultorDirectoTotal, int ConsultorDirectoActivo, int ConsultorDirectoNuevo,
                                            int CIDirectoTotal, int CIDirectoActivo, int CIDirectoNuevo,
                                            int SocioRedTotal, int SocioRedActivo, int SocioRedNuevo,
                                            int ConsultorRedTotal, int ConsultorRedActivo, int ConsultorRedNuevo,
                                            int CIRedTotal, int CIRedActivo, int CIRedNuevo, string dni, int IDP)
        {
            try
            {
                return ClienteDAO.getInstance().ActualizarRetencionSocio(SocioDirectoTotal, SocioDirectoActivo, SocioDirectoNuevo,
                                                                         ConsultorDirectoTotal, ConsultorDirectoActivo, ConsultorDirectoNuevo,
                                                                         CIDirectoTotal, CIDirectoActivo, CIDirectoNuevo,
                                                                         SocioRedTotal, SocioRedActivo, SocioRedNuevo,
                                                                         ConsultorRedTotal, ConsultorRedActivo, ConsultorRedNuevo,
                                                                         CIRedTotal, CIRedActivo, CIRedNuevo, dni, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ConsultaDirectosGeneral(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ConsultaDirectosGeneral(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteSinCompras()
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteSinCompras();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClientesMapaRed()
        {
            try
            {
                return ClienteDAO.getInstance().ListarClientesMapaRed();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarRegaloSocios()
        {
            try
            {
                return ClienteDAO.getInstance().ListarRegaloSocios();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistroModificaciones(string autor, DateTime fecha, string accion, string dato)
        {
            try
            {
                return ClienteDAO.getInstance().RegistroModificaciones(autor, fecha, accion, dato);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerPaqueteSocio(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ObtenerPaqueteSocio(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidarCantidadUsuario(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ValidarCantidadUsuario(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidarCantidadDocumento(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ValidarCantidadDocumento(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.Compresion> ListarSociosComprimir()
        {
            try
            {
                return ClienteDAO.getInstance().ListarSociosComprimir();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.Compresion.Directos> ListarDirectoSocios(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarDirectoSocios(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.Compresion.DocumentoUP> ListarDocumentoUP(string documento)
        {
            try
            {
                return ClienteDAO.getInstance().ListarDocumentoUP(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDocumentoSocioComprimido(string idcliente)
        {
            try
            {
                ClienteDAO.getInstance().ActualizarDocumentoSocioComprimido(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarMapaRedComisionCompresion(string upline, string idupline, string documento)
        {
            try
            {
                ClienteDAO.getInstance().ActualizarMapaRedComisionCompresion(upline, idupline, documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDatos_Directos_Compresion(string upline, string patrocinador, string factorComi, string documento)
        {
            try
            {
                ClienteDAO.getInstance().ActualizarDatos_Directos_Compresion(upline, patrocinador, factorComi, documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarDocumento_Consultor_CI_Comprimido(string idcliente)
        {
            try
            {
                ClienteDAO.getInstance().ActualizarDocumento_Consultor_CI_Comprimido(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente.ClientePreRegistro> ListarClientesPreRegistro(string patrocinador)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClientesPreRegistro(patrocinador);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Cliente> ListarClienteUsuario(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ListarClienteUsuario(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string DevolverToken(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().DevolverToken(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ValidarToken(string usuario, string codigo_correo)
        {
            try
            {
                return ClienteDAO.getInstance().ValidarToken(usuario,codigo_correo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerCorreo_By_Usuario(string usuario)
        {
            try
            {
                return ClienteDAO.getInstance().ObtenerCorreo_By_Usuario(usuario);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ObtenerFiltroPreregistro(string idcliente)
        {
            try
            {
                return ClienteDAO.getInstance().ObtenerFiltroPreregistro(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ActualizarEstadoPreregistro(string idcliente)
        {
            try
            {
                ClienteDAO.getInstance().ActualizarEstadoPreregistro(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerCorreo_By_IdCliente(string idcliente)
        {
            try
            {
                return ClienteDAO.getInstance().ObtenerCorreo_By_IdCliente(idcliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
