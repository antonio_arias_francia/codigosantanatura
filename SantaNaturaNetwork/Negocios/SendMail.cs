﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;

namespace Negocios
{
    public class SendMail
    {
        Boolean estado = true;
        String merror;
        public SendMail()
        {
            MailMessage mail = new MailMessage();
            
            mail.To.Add(new MailAddress("antonioariasfrancia@gmail.com"));
            mail.From = new MailAddress("santanaturavouchers@gmail.com", "MyWeb Site", System.Text.Encoding.UTF8);
            mail.Subject = "PRUEBA 02";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "MENSAJE DE PRUEBA";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            //mail.Attachments.Add(new Attachment(File));

            SmtpClient smtp = new SmtpClient();
            smtp.Credentials = new System.Net.NetworkCredential("santanaturavouchers@gmail.com", "santa2019");
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;

            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                estado = false;
                merror = ex.Message.ToString();
            }
        }

        public Boolean Estado
        {
            get { return estado; }
        }

        public String mensaje_Error
        {
            get { return merror; }
        }
    }
}
