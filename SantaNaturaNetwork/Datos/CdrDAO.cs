﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Modelos;

namespace Datos
{
    public class CdrDAO
    {
        #region "PATRON SINGLETON"
        private static CdrDAO daoCDR = null;
        private CdrDAO() { }
        public static CdrDAO getInstance()
        {
            if (daoCDR == null)
            {
                daoCDR = new CdrDAO();
            }
            return daoCDR;
        }
        #endregion

        public bool RegistroPorcentajeLineaCDR(Cliente.CDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_PORCENTAJE_LINEA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDLINEA", objCDR.IdLinea);
                cmd.Parameters.AddWithValue("@CDRPS", objCDR.CDRPS);
                cmd.Parameters.AddWithValue("@PORCENTAJE_LINEA", objCDR.Porcentaje);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistroPorcentajeLineaCDR", "USP_REGISTRO_PORCENTAJE_LINEA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }
         
        public List<Cliente.CDR> ListaCDRPorcentaje()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR> listaCDR = new List<Cliente.CDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CDR_PORCENTAJE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR cdr = new Cliente.CDR();
                    cdr.CDRPS = dr["CDRPS"].ToString().Trim();
                    cdr.Apodo = dr["Apodo"].ToString().Trim();
                    listaCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaCDRPorcentaje", "USP_LISTA_CDR_PORCENTAJE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<Cliente.CDR> ListaPorcentajeLineaXCDR(string DNICDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR> listaCDR = new List<Cliente.CDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PORCENTAJELINEA_CRPS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CDRPS", DNICDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR cdr = new Cliente.CDR();
                    cdr.IdLineaCDR = Convert.ToInt32(dr["ID_PORCENTAJE"]);
                    cdr.NombreLinea = dr["Nombre"].ToString().Trim();
                    cdr.Porcentaje = Convert.ToDouble(dr["PORCENTAJE_LINEA"]);
                    listaCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaPorcentajeLineaXCDR", "USP_LISTA_PORCENTAJELINEA_CRPS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public bool ActualizarPorcentajeLineaCDR(Cliente.CDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_PORCENTAJELINEA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_PORCENTAJE", objCDR.IdLineaCDR);
                cmd.Parameters.AddWithValue("@PORCENTAJE_LINEA", objCDR.Porcentaje);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarPorcentajeLineaCDR", "USP_UPDATE_PORCENTAJELINEA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.PERIODOCDR> listaPeriodoCDR = new List<Cliente.CDR.PERIODOCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PERIODOCDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.PERIODOCDR cdr = new Cliente.CDR.PERIODOCDR();
                    cdr.IdPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    cdr.FechaInicio = Convert.ToString(dr["FECHA_INICIO"]).Substring(0, 10);
                    cdr.FechaFin = Convert.ToString(dr["FECHA_FIN"]).Substring(0, 10);
                    cdr.Descripcion = dr["DESCRIPCION"].ToString();
                    listaPeriodoCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaPeriodoCDR", "USP_LISTA_PERIODOCDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPeriodoCDR;
        }

        public int RegistroPeriodoCDR(Cliente.CDR.PERIODOCDR objCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int IdPeriodo = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRAR_PERIODOCDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA_INICIO", objCDR.FechaInicio);
                cmd.Parameters.AddWithValue("@FECHA_FIN", objCDR.FechaFin);
                cmd.Parameters.AddWithValue("@DESCRIPCION", objCDR.Descripcion);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    IdPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistroPeriodoCDR", "USP_REGISTRAR_PERIODOCDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return IdPeriodo;
        }

        public bool ActualizarPeriodoCDR(Cliente.CDR.PERIODOCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_PERIODOCDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", objCDR.IdPeriodo);
                cmd.Parameters.AddWithValue("@FECHA_INICIO", objCDR.FechaInicio);
                cmd.Parameters.AddWithValue("@FECHA_FIN", objCDR.FechaFin);
                cmd.Parameters.AddWithValue("@DESCRIPCION", objCDR.Descripcion);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarPeriodoCDR", "USP_UPDATE_PERIODOCDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool EliminarPeriodoCDR(Cliente.CDR.PERIODOCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_PERIODOCDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", objCDR.IdPeriodo);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "EliminarPeriodoCDR", "USP_ELIMINAR_PERIODOCDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool RegistroInicial_CLC_CDR(int IDPC, string dniCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_INICIAL_CLC_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO_CDR", IDPC);
                cmd.Parameters.AddWithValue("@CDR", dniCDR);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistroInicial_CLC_CDR", "USP_REGISTRO_INICIAL_CLC_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool RegistroSeriePuntoVenta(string dniCDR, string serie)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_SERIE_PUNTOVENTA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CDR", dniCDR);
                cmd.Parameters.AddWithValue("@SERIE", serie);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistroSeriePuntoVenta", "USP_REGISTRO_SERIE_PUNTOVENTA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarSeriePuntoVenta(int idSerie, string serie)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_SERIE_PUNTOVENTA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSERIE", idSerie);
                cmd.Parameters.AddWithValue("@SERIE", serie);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarSeriePuntoVenta", "USP_UPDATE_SERIE_PUNTOVENTA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.SerieCDR> ListaSerieCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.SerieCDR> listaSerieCDR = new List<Cliente.CDR.SerieCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SERIE_PUNTOVENTA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.SerieCDR cdr = new Cliente.CDR.SerieCDR();
                    cdr.IdSerie = Convert.ToInt32(dr["IDSERIE"]);
                    cdr.Serie = Convert.ToString(dr["SERIE"]);
                    cdr.dniCDR = Convert.ToString(dr["CDR"]);
                    cdr.apodoCDR = Convert.ToString(dr["Apodo"]);
                    listaSerieCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaSerieCDR", "USP_LISTA_SERIE_PUNTOVENTA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaSerieCDR;
        }

        public string ObtenerSerieCDR(string PSCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string serie = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_SERIECDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPS", PSCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    serie = Convert.ToString(dr["SERIE"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ObtenerSerieCDR", "USP_OBTENER_SERIECDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return serie;
        }

        public bool RegistroInversionCDR(Cliente.CDR.InversionCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_INVERSION_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA", objCDR.Fecha);
                cmd.Parameters.AddWithValue("@MONTO", objCDR.Monto);
                cmd.Parameters.AddWithValue("@CDR", objCDR.DNICDR);
                cmd.Parameters.AddWithValue("@ID_INICIAL", objCDR.IdInicial);
                cmd.Parameters.AddWithValue("@TIPOCOMPRA", objCDR.TipoCompra);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistroInversionCDR", "USP_REGISTRO_INVERSION_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.InversionCDR> ListaInversionInicialCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.InversionCDR> listaCDR = new List<Cliente.CDR.InversionCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_INICIAL_INVERSION_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.InversionCDR cdr = new Cliente.CDR.InversionCDR();
                    cdr.IdInversion = Convert.ToInt32(dr["ID_INVERSION"]);
                    cdr.Monto = Convert.ToDecimal(dr["MONTO"]);
                    cdr.DNICDR = dr["CDR"].ToString().Trim();
                    cdr.ApodoCDR = dr["Apodo"].ToString().Trim();
                    cdr.PSCDR = dr["IdPeruShop"].ToString().Trim();
                    listaCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaInversionInicialCDR", "USP_LISTA_INICIAL_INVERSION_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public bool ActualizarInversionInicialCDR(Cliente.CDR.InversionCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_INICIAL_INVERSION_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MONTO", objCDR.Monto);
                cmd.Parameters.AddWithValue("@ID_INVERSION", objCDR.IdInversion);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarInversionInicialCDR", "USP_UPDATE_INICIAL_INVERSION_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public string ObtenerDniCDR_By_IDPSCDR(string IDPSCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string dniCDR = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GET_DNICDR_BY_IDPSCDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPS", IDPSCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dniCDR = Convert.ToString(dr["NumeroDoc"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ObtenerDniCDR_By_IDPSCDR", "USP_GET_DNICDR_BY_IDPSCDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return dniCDR;
        }

        public bool ActualizarComisionLineaCDR(Cliente.CDR.ComisionLineaCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_COMISION_LINEA_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COMISION", objCDR.Comision);
                cmd.Parameters.AddWithValue("@LINEA_CREDITO", objCDR.LineaCredito);
                cmd.Parameters.AddWithValue("@IDPERIODO", objCDR.IDPeriodoCDR);
                cmd.Parameters.AddWithValue("@CDR", objCDR.DniCDR);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarComisionLineaCDR", "USP_UPDATE_COMISION_LINEA_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComisionLineaCDRLista(List<Cliente.CDR.ComisionLineaCDR> objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlTransaction trans = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                con.Open();
                trans = con.BeginTransaction();

                foreach (var lista in objCDR)
                {
                   cmd = new SqlCommand("USP_UPDATE_COMISION_LINEA_CDR", con);
                   cmd.CommandType = CommandType.StoredProcedure;
                   cmd.Parameters.AddWithValue("@COMISION", lista.Comision);
                   cmd.Parameters.AddWithValue("@LINEA_CREDITO", lista.LineaCredito);
                   cmd.Parameters.AddWithValue("@IDPERIODO", lista.IDPeriodoCDR);
                   cmd.Parameters.AddWithValue("@CDR", lista.DniCDR);
                   cmd.Transaction = trans;
                   cmd.CommandTimeout = 0;
                   cmd.ExecuteNonQuery();
                }
                trans.Commit();
                ok = true;
            }
            catch (Exception ex)
            {   
                trans.Rollback();
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarComisionLineaCDRLista", "USP_UPDATE_COMISION_LINEA_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasPedidoLineaCreditoCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DiasPedidosLineaCreditoCDR> listaLCCDR = new List<Cliente.CDR.DiasPedidosLineaCreditoCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DIAS_PEDIDOS_LINEACREDITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DiasPedidosLineaCreditoCDR cdr = new Cliente.CDR.DiasPedidosLineaCreditoCDR();
                    cdr.IDDias = Convert.ToInt32(dr["ID_DIAS"]);
                    cdr.Nombre = Convert.ToString(dr["NOMBRE_DIA"]);
                    cdr.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    listaLCCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDiasPedidoLineaCreditoCDR", "USP_LISTA_DIAS_PEDIDOS_LINEACREDITO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaLCCDR;
        }

        public bool ActualizarEstadoDiasPedidoLC(Cliente.CDR.DiasPedidosLineaCreditoCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_DIAS_PEDIDOS_LINEA_CREDITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DIA", objCDR.IDDias);
                cmd.Parameters.AddWithValue("@ESTADO", objCDR.Estado);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarEstadoDiasPedidoLC", "USP_UPDATE_DIAS_PEDIDOS_LINEA_CREDITO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.ComboTipoCompraCDR> ListaTipoCompraCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.ComboTipoCompraCDR> listaTipo = new List<Cliente.CDR.ComboTipoCompraCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTA_TIPO_PAGO_PEDIDO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.ComboTipoCompraCDR CDR = new Cliente.CDR.ComboTipoCompraCDR();
                    CDR.IDTipoCompra = Convert.ToInt32(dr["ID_TIPO"]);
                    CDR.Descripcion = dr["DESCRIPCION"].ToString();
                    listaTipo.Add(CDR);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaTipoCompraCDR", "LISTA_TIPO_PAGO_PEDIDO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasActivosLineaCreditoCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DiasPedidosLineaCreditoCDR> listaLCCDR = new List<Cliente.CDR.DiasPedidosLineaCreditoCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DIAS_ACTIVOS_LINEACREDITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DiasPedidosLineaCreditoCDR cdr = new Cliente.CDR.DiasPedidosLineaCreditoCDR();
                    cdr.IDDias = Convert.ToInt32(dr["ID_DIAS"]);
                    cdr.Nombre = Convert.ToString(dr["NOMBRE_DIA"]);
                    cdr.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    listaLCCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDiasActivosLineaCreditoCDR", "USP_LISTA_DIAS_ACTIVOS_LINEACREDITO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaLCCDR;
        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasActivosEmergencia()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DiasPedidosLineaCreditoCDR> listaLCCDR = new List<Cliente.CDR.DiasPedidosLineaCreditoCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DIAS_ACTIVOS_EMERGENCIA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DiasPedidosLineaCreditoCDR cdr = new Cliente.CDR.DiasPedidosLineaCreditoCDR();
                    cdr.IDDias = Convert.ToInt32(dr["ID_DIAS"]);
                    cdr.Nombre = Convert.ToString(dr["NOMBRE_DIA"]);
                    cdr.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    listaLCCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDiasActivosEmergencia", "USP_LISTA_DIAS_ACTIVOS_EMERGENCIA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaLCCDR;
        }

        public List<Cliente.CDR.FechaPedidoComisionCDR> ListaFechasPedidoComisionCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.FechaPedidoComisionCDR> listaTipo = new List<Cliente.CDR.FechaPedidoComisionCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_FECHAS_PEDIDO_COMISION_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.FechaPedidoComisionCDR CDR = new Cliente.CDR.FechaPedidoComisionCDR();
                    CDR.ID_Pedido = Convert.ToInt32(dr["ID_FECHAS"]);
                    CDR.FechaInicio = dr["FECHA_INICIO"].ToString();
                    CDR.FechaFin = dr["FECHA_FIN"].ToString();
                    listaTipo.Add(CDR);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaFechasPedidoComisionCDR", "USP_LISTA_FECHAS_PEDIDO_COMISION_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }

        public bool ActualizarFechasPedidoComision(Cliente.CDR.FechaPedidoComisionCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_FECHAS_PEDIDO_COMISION_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDFECHAS", objCDR.ID_Pedido);
                cmd.Parameters.AddWithValue("@FECHA_INICIO", objCDR.FechaInicio);
                cmd.Parameters.AddWithValue("@FECHA_FIN", objCDR.FechaFin);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarFechasPedidoComision", "USP_UPDATE_FECHAS_PEDIDO_COMISION_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.ComisionLineaCDR> ListaComisionLineaByCDR(string dniCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.ComisionLineaCDR> listaDatos = new List<Cliente.CDR.ComisionLineaCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_SALDO_COMISIONES_LC_BY_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNICDR", dniCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.ComisionLineaCDR cdr = new Cliente.CDR.ComisionLineaCDR();
                    cdr.Comision = Convert.ToDouble(dr["COMISION"]);
                    cdr.LineaCredito = Convert.ToDouble(dr["LINEA_CREDITO"]);
                    cdr.AbstencionComision = Convert.ToDouble(dr["ABSTENCION_COMISION"]);
                    cdr.AbstencionLineaCredito = Convert.ToDouble(dr["ABSTENCION_LINEA_CREDITO"]);
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaComisionLineaByCDR", "USP_OBTENER_SALDO_COMISIONES_LC_BY_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public List<Cliente.CDR.SustraccionComisionesLC> ListaSustraccionComisionLineaByCDR(string dniCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.SustraccionComisionesLC> listaDatos = new List<Cliente.CDR.SustraccionComisionesLC>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_SUSTRACCION_COMICIONES_LC_BY_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNICDR", dniCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.SustraccionComisionesLC cdr = new Cliente.CDR.SustraccionComisionesLC();
                    cdr.Monto_Comision = (dr["MONTO_COMISION"] == DBNull.Value) ? 0: Convert.ToDouble(dr["MONTO_COMISION"]);
                    cdr.Monto_LC = (dr["MONTO_LC"] == DBNull.Value) ? 0 : Convert.ToDouble(dr["MONTO_LC"]);
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaSustraccionComisionLineaByCDR", "USP_OBTENER_SUSTRACCION_COMICIONES_LC_BY_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public bool RegistrarSustraccionComisionesLC(Cliente.CDR.SustraccionComisionesLC objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_SUSTRACCION_SALDO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO_CDR", objCDR.IdPeriodo);
                cmd.Parameters.AddWithValue("@IDPEDIDO", objCDR.IdPedido);
                cmd.Parameters.AddWithValue("@CDR", objCDR.DniCDR);
                cmd.Parameters.AddWithValue("@FECHA", objCDR.Fecha);
                cmd.Parameters.AddWithValue("@MONTO", objCDR.Monto_General);
                cmd.Parameters.AddWithValue("@CONCEPTO", objCDR.Concepto);
                cmd.Parameters.AddWithValue("@DESCRIPCION", objCDR.Descripcion);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistrarSustraccionComisionesLC", "USP_REGISTRO_SUSTRACCION_SALDO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool RegistrarDatosPersonalesCDR(Cliente.CDR.DatosPersonalesCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("REGISTRO_DATOS_PERSONALES_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI_CDR", objCDR.DniCDR.Trim());
                cmd.Parameters.AddWithValue("@NOMBRES", objCDR.Nombres.Trim());
                cmd.Parameters.AddWithValue("@APELLIDO_PAT", objCDR.ApellidoPat.Trim());
                cmd.Parameters.AddWithValue("@APELLIDO_MAT", objCDR.ApellidoMat.Trim());
                cmd.Parameters.AddWithValue("@DOCUMENTO", objCDR.Documento.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistrarDatosPersonalesCDR", "REGISTRO_DATOS_PERSONALES_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.DatosPersonalesCDR> ListaDatosPersonalesCDR(string dniCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DatosPersonalesCDR> listaDatos = new List<Cliente.CDR.DatosPersonalesCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_PERSONALES_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI_CDR", dniCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DatosPersonalesCDR cdr = new Cliente.CDR.DatosPersonalesCDR();
                    cdr.IdDatos = Convert.ToInt32(dr["ID_DATOS"]);
                    cdr.DniCDR = Convert.ToString(dr["DNI_CDR"]).Trim();
                    cdr.Nombres = Convert.ToString(dr["NOMBRES"]).Trim();
                    cdr.ApellidoPat = Convert.ToString(dr["APELLIDO_PAT"]).Trim();
                    cdr.ApellidoMat = Convert.ToString(dr["APELLIDO_MAT"]).Trim();
                    cdr.Documento = Convert.ToString(dr["DOCUMENTO"]).Trim();
                    cdr.Apodo = Convert.ToString(dr["Apodo"]).Trim();
                    cdr.Direccion = Convert.ToString(dr["Direccion"]).Trim();
                    cdr.IdPS = Convert.ToString(dr["IdPeruShop"]).Trim();
                    cdr.DocExtorno = Convert.ToString(dr["DOCUMENTO_EXTORNO"]).Trim();
                    cdr.RazonExtorno = Convert.ToString(dr["RAZON_EXTORNO"]).Trim();
                    cdr.DirExtorno = Convert.ToString(dr["DIRECCION_EXTORNO"]).Trim();
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                listaDatos.Add(new Cliente.CDR.DatosPersonalesCDR());
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDatosPersonalesCDR", "USP_LISTA_DATOS_PERSONALES_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public bool ActualizarDatosPersonalesCDR(Cliente.CDR.DatosPersonalesCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("UPDATE_DATOS_PERSONALES_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI_CDR", objCDR.DniCDR.Trim());
                cmd.Parameters.AddWithValue("@NOMBRES", objCDR.Nombres.Trim());
                cmd.Parameters.AddWithValue("@APELLIDO_PAT", objCDR.ApellidoPat.Trim());
                cmd.Parameters.AddWithValue("@APELLIDO_MAT", objCDR.ApellidoMat.Trim());
                cmd.Parameters.AddWithValue("@DOCUMENTO", objCDR.Documento.Trim());
                cmd.Parameters.AddWithValue("@DOC_EXTORNO", objCDR.DocExtorno.Trim());
                cmd.Parameters.AddWithValue("@RAZON_EXTORNO", objCDR.RazonExtorno.Trim());
                cmd.Parameters.AddWithValue("@DIR_EXTORNO", objCDR.DirExtorno.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarDatosPersonalesCDR", "UPDATE_DATOS_PERSONALES_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public void RegistroDatosPagoEfectivoCDR(Cliente.CDR.DatosPagoEfectivoCDR objCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("REGISTRO_DATOS_PAGO_EFECTIVO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPEDIDO", objCDR.IdDatos);
                cmd.Parameters.AddWithValue("@DNICDR", objCDR.DniCDR);
                cmd.Parameters.AddWithValue("@FECHACREACION", objCDR.FechaCreacion);
                cmd.Parameters.AddWithValue("@FECHAEXPIRACION", objCDR.FechaExpiracion);
                cmd.Parameters.AddWithValue("@CIP", objCDR.CIP);
                cmd.Parameters.AddWithValue("@MONTO", objCDR.Monto);
                cmd.Parameters.AddWithValue("@CODE_STATUS", "");
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "RegistroDatosPagoEfectivoCDR", "REGISTRO_DATOS_PAGO_EFECTIVO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Cliente.CDR.DatosPagoEfectivoCDR> ListaPedidosPE(string dniCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DatosPagoEfectivoCDR> listaDatos = new List<Cliente.CDR.DatosPagoEfectivoCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PEDIDOS_PE_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNICDR", dniCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DatosPagoEfectivoCDR cdr = new Cliente.CDR.DatosPagoEfectivoCDR();
                    cdr.IdDatos = Convert.ToInt32(dr["ID_DATOS"]);
                    cdr.FechaCreacionStr = Convert.ToString(dr["FECHACREACION"]);
                    cdr.FechaExpiracionStr = Convert.ToString(dr["FECHAEXPIRACION"]);
                    cdr.CIP = Convert.ToString(dr["CIP"]);
                    cdr.Monto = Convert.ToDouble(dr["MONTO"]);
                    cdr.EstadoPedido = Convert.ToString(dr["ESTADO"]);
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                listaDatos.Add(new Cliente.CDR.DatosPagoEfectivoCDR());
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaPedidosPE", "USP_LISTA_PEDIDOS_PE_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public bool ActualizarDatosCompraPE_CDR(int idDatos, string fechapago, string numoperacion)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_APROBACION_PAGO_EFECTIVO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", idDatos);
                cmd.Parameters.AddWithValue("@NUM_OPERACION", numoperacion);
                cmd.Parameters.AddWithValue("@FECHAPAGADA", fechapago);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarDatosCompraPE_CDR", "USP_APROBACION_PAGO_EFECTIVO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente.CDR.DatosProcesamientoComprasCDR> ListaDatosProcesamientoComprasCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DatosProcesamientoComprasCDR> listaDatos = new List<Cliente.CDR.DatosProcesamientoComprasCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_COMPRAS_NOTIFICADAS_CDR_PAGOEFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DatosProcesamientoComprasCDR cdr = new Cliente.CDR.DatosProcesamientoComprasCDR();
                    cdr.IdPedido = Convert.ToInt32(dr["IDSOLICITUD"]);
                    cdr.DniCDR = Convert.ToString(dr["DNICDR"]);
                    cdr.TipoCompra = Convert.ToString(dr["TipoCompra"]);
                    cdr.Monto = Convert.ToDouble(dr["MONTO"]);
                    cdr.Fecha = Convert.ToDateTime(dr["FECHASOLICITUD"]);
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                listaDatos.Add(new Cliente.CDR.DatosProcesamientoComprasCDR());
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDatosProcesamientoComprasCDR", "USP_LISTA_COMPRAS_NOTIFICADAS_CDR_PAGOEFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public List<Cliente.CDR.DatosPagoEfectivoCDR> ListaComprasExpiradasCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DatosPagoEfectivoCDR> listaDatos = new List<Cliente.CDR.DatosPagoEfectivoCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_COMPRAS_EXPIRADAS_CDR_PE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DatosPagoEfectivoCDR cdr = new Cliente.CDR.DatosPagoEfectivoCDR();
                    cdr.IdDatos = Convert.ToInt32(dr["ID_DATOS"]);
                    cdr.DniCDR = Convert.ToString(dr["DNICDR"]);
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                listaDatos.Add(new Cliente.CDR.DatosPagoEfectivoCDR());
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaComprasExpiradasCDR", "USP_LISTA_COMPRAS_EXPIRADAS_CDR_PE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public void EliminarDatosComprasExpiradasCDR(int IdPedido)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_DATOS_COMPRAS_EXPERIDAS_CDR_PE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", IdPedido);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "EliminarDatosComprasExpiradasCDR", "USP_ELIMINAR_DATOS_COMPRAS_EXPERIDAS_CDR_PE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Cliente.CDR.DatosCompraPE_IDOP> ListaDatosCompraPE_IDOP(int IdPedido)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DatosCompraPE_IDOP> listaDatos = new List<Cliente.CDR.DatosCompraPE_IDOP>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("OBTENER_DATOS_COMPRA_CDR_PE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPEDIDO", IdPedido);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DatosCompraPE_IDOP cdr = new Cliente.CDR.DatosCompraPE_IDOP();
                    cdr.FechaPedido = Convert.ToString(dr["FECHASOLICITUD"]).Trim();
                    cdr.IdPeruShop = Convert.ToString(dr["IdPeruShop"]).Trim();
                    cdr.Direccion = Convert.ToString(dr["Direccion"]).Trim();
                    cdr.CIP = Convert.ToString(dr["CIP"]).Trim();
                    cdr.DocExtorno = Convert.ToString(dr["DOCUMENTO_EXTORNO"]).Trim();
                    cdr.RazonExtorno = Convert.ToString(dr["RAZON_EXTORNO"]).Trim();
                    cdr.DirExtorno = Convert.ToString(dr["DIRECCION_EXTORNO"]).Trim();
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                listaDatos.Add(new Cliente.CDR.DatosCompraPE_IDOP());
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDatosCompraPE_IDOP", "OBTENER_DATOS_COMPRA_CDR_PE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public string DescontarStockCDR(List<StockProductosRegistro> listaStock, string dniCDR)
        {
            string mssg = "1";
            int i = 0;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            SqlTransaction trans = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                con.Open();
                trans = con.BeginTransaction();
                while (mssg == "1" && i < listaStock.Count())
                {
                    mssg = "";
                    int cantidad = Convert.ToInt32(listaStock[i].cantidad) * Convert.ToInt32(listaStock[i].UP);
                    cmd = new SqlCommand("USP_REDUCIR_STOCK_PLANTA", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPRODUCTO", listaStock[i].codigo);
                    cmd.Parameters.AddWithValue("@CANTIDAD", cantidad);
                    cmd.Parameters.AddWithValue("@DNICDR", dniCDR);
                    cmd.Transaction = trans;
                    cmd.CommandTimeout = 0;
                    dr = cmd.ExecuteReader();
                    //if(i == 0) { dr = cmd.ExecuteReader(); }
                    while (dr.Read())
                    {
                        mssg = Convert.ToString(dr["RESULT"]);
                    }
                    dr.Close();
                    i++;
                }

                if (mssg == "1")
                {
                    trans.Commit();
                }
                else
                {
                    trans.Rollback();
                }
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Conexion.getInstance().RegistrarError("CdrDAO", "DescontarStockCDR", "USP_REDUCIR_STOCK_PLANTA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return mssg;
        }

        public List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasCondicionEmergencia()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DiasPedidosLineaCreditoCDR> listaLCCDR = new List<Cliente.CDR.DiasPedidosLineaCreditoCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DIAS_CONDICION_EMERGENCIA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DiasPedidosLineaCreditoCDR cdr = new Cliente.CDR.DiasPedidosLineaCreditoCDR();
                    cdr.IDDias = Convert.ToInt32(dr["ID_DIAS"]);
                    cdr.Nombre = Convert.ToString(dr["NOMBRE_DIA"]);
                    cdr.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    listaLCCDR.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDiasCondicionEmergencia", "USP_LISTA_DIAS_CONDICION_EMERGENCIA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaLCCDR;
        }

        public bool ActualizarCondicionEmergencia(Cliente.CDR.DiasPedidosLineaCreditoCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_DIAS_CONDICION_EMERGENCIA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DIA", objCDR.IDDias);
                cmd.Parameters.AddWithValue("@ESTADO", objCDR.Estado);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarCondicionEmergencia", "USP_UPDATE_DIAS_CONDICION_EMERGENCIA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public string GetUltimaFechaCompraCDR(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string fecha = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GET_MAX_FECHAPEDIDO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    fecha = Convert.ToString(dr["FECHA"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "GetUltimaFechaCompraCDR", "USP_GET_MAX_FECHAPEDIDO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return fecha;
        }

        public List<Cliente.CDR.DatosPersonalesCDR> ListaDatosPersonalesCDR_Admin()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.DatosPersonalesCDR> listaDatos = new List<Cliente.CDR.DatosPersonalesCDR>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_PERSONALES_CDR_ADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.DatosPersonalesCDR cdr = new Cliente.CDR.DatosPersonalesCDR();
                    cdr.IdDatos = Convert.ToInt32(dr["ID_DATOS"]);
                    cdr.DniCDR = Convert.ToString(dr["DNI_CDR"]).Trim();
                    cdr.Nombres = Convert.ToString(dr["NOMBRES"]).Trim();
                    cdr.ApellidoPat = Convert.ToString(dr["APELLIDO_PAT"]).Trim();
                    cdr.ApellidoMat = Convert.ToString(dr["APELLIDO_MAT"]).Trim();
                    cdr.Documento = Convert.ToString(dr["DOCUMENTO"]).Trim();
                    cdr.Apodo = Convert.ToString(dr["Apodo"]).Trim();
                    cdr.Direccion = Convert.ToString(dr["Direccion"]).Trim();
                    cdr.IdPS = Convert.ToString(dr["IdPeruShop"]).Trim();
                    cdr.DocExtorno = Convert.ToString(dr["DOCUMENTO_EXTORNO"]).Trim();
                    cdr.RazonExtorno = Convert.ToString(dr["RAZON_EXTORNO"]).Trim();
                    cdr.DirExtorno = Convert.ToString(dr["DIRECCION_EXTORNO"]).Trim();
                    listaDatos.Add(cdr);
                }
            }
            catch (Exception ex)
            {
                listaDatos.Add(new Cliente.CDR.DatosPersonalesCDR());
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaDatosPersonalesCDR_Admin", "USP_LISTA_DATOS_PERSONALES_CDR_ADMIN", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDatos;
        }

        public void ActualizarDatosPersonalesCDR_ADMIN(Cliente.CDR.DatosPersonalesCDR objCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_DATOS_PERSONALES_CDR_ADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", objCDR.IdDatos);
                cmd.Parameters.AddWithValue("@DOCUMENTO", objCDR.Documento.Trim());
                cmd.Parameters.AddWithValue("@NOMBRES", objCDR.Nombres.Trim());
                cmd.Parameters.AddWithValue("@APELLIDO_PAT", objCDR.ApellidoPat.Trim());
                cmd.Parameters.AddWithValue("@APELLIDO_MAT", objCDR.ApellidoMat.Trim());
                cmd.Parameters.AddWithValue("@DOCUMENTO_EXTORNO", objCDR.DocExtorno.Trim());
                cmd.Parameters.AddWithValue("@RAZON_EXTORNO", objCDR.RazonExtorno.Trim());
                cmd.Parameters.AddWithValue("@DIR_EXTORNO", objCDR.DirExtorno.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarDatosPersonalesCDR_ADMIN", "USP_UPDATE_DATOS_PERSONALES_CDR_ADMIN", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Cliente.CDR.FechaPedidoComisionCDR> ListaFechasEvaluacionPedidoCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente.CDR.FechaPedidoComisionCDR> listaTipo = new List<Cliente.CDR.FechaPedidoComisionCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_FECHAS_EVALUACION_PEDIDO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.CDR.FechaPedidoComisionCDR CDR = new Cliente.CDR.FechaPedidoComisionCDR();
                    CDR.ID_Pedido = Convert.ToInt32(dr["ID_FECHAS"]);
                    CDR.FechaInicio = dr["FECHA_INICIO"].ToString();
                    CDR.FechaFin = dr["FECHA_FIN"].ToString();
                    listaTipo.Add(CDR);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ListaFechasEvaluacionPedidoCDR", "USP_LISTA_FECHAS_EVALUACION_PEDIDO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }

        public bool ActualizarFechasEvaluacionPedidoCDR(Cliente.CDR.FechaPedidoComisionCDR objCDR)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_FECHAS_EVALUACION_PEDIDO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDFECHAS", objCDR.ID_Pedido);
                cmd.Parameters.AddWithValue("@FECHA_INICIO", objCDR.FechaInicio);
                cmd.Parameters.AddWithValue("@FECHA_FIN", objCDR.FechaFin);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "ActualizarFechasEvaluacionPedidoCDR", "USP_UPDATE_FECHAS_EVALUACION_PEDIDO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public int CondicionPedidosCDR(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int condicion = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CONDICION_PEDIDOS_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNICDR", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    condicion = Convert.ToInt32(dr["VALIDAR"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CdrDAO", "CondicionPedidosCDR", "USP_CONDICION_PEDIDOS_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return condicion;
        }
    }
}
