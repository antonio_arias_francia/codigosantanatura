﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class PuntosComisionesDAO
    {

        #region "PATRON SINGLETON"
        private static PuntosComisionesDAO puntosDatos = null;
        private PuntosComisionesDAO() { }
        public static PuntosComisionesDAO getInstance()
        {
            if (puntosDatos == null)
            {
                puntosDatos = new PuntosComisionesDAO();
            }
            return puntosDatos;
        }
        #endregion

        string errores = "";

        public List<PuntosComisiones> ListaPuntosComisiones(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double VS = 0.0;
            double VCCI = 0.0;
            double VP = 0.0;
            double VPREAL = 0.0;
            double PP = 0.0;
            double PPA = 0.0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CALCULOPP", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.PuntosPersonales = (Convert.ToString(dr["PP"]) == "") ? 0 : Convert.ToDouble(dr["PP"]);
                    puntos.PuntosPersonalesExtra = (Convert.ToString(dr["PPA"]) == "") ? 0 : Convert.ToDouble(dr["PPA"]);
                    PP = puntos.PuntosPersonales;
                    PPA = puntos.PuntosPersonalesExtra;
                    VP = Convert.ToDouble(GenerarVP(dni.Trim(), idp));
                    VPREAL = Convert.ToDouble(GenerarVPREAL(dni.Trim(), idp));
                    VS = Convert.ToDouble(GenerarVR(dni.Trim(), idp));
                    VCCI = Convert.ToDouble(GenerarVCCI(dni.Trim(), idp));
                    puntos.dni = dni.Trim();
                    puntos.VolumenPersonal = PPA + VP;
                    puntos.VIP = PP + VPREAL;
                    puntos.VolumenRed = (VS - PPA) + (VCCI - VP);
                    puntos.VolumenGeneral = VS + VCCI;
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaPuntosComisiones", "USP_CALCULOPP", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones> ListaDirectosVPR(string dni)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADIRECTOVG", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.VolumenGeneral = Convert.ToDouble(dr["VG"]);
                    puntos.dni = Convert.ToString(dr["DOCUMENTO"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaDirectosVPR", "USP_LISTADIRECTOVG", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones> ListaDirectosVPRPeriodo(string dni, int IDP)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADIRECTOVGPERIODO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@IDP", IDP);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.VolumenGeneral = Convert.ToDouble(dr["VG"]);
                    puntos.dni = Convert.ToString(dr["DOCUMENTO"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaDirectosVPRPeriodo", "USP_LISTADIRECTOVGPERIODO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public int ListaGenerarPPDirectos(string dni)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADIRECTOSPATROCINIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cantidad = Convert.ToInt32(dr["CANT"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaGenerarPPDirectos", "USP_LISTADIRECTOSPATROCINIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return cantidad;
        }

        public int ListaGenerarPPDirectosPeriodo(string dni, int IDP)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADIRECTOSPATROCINIOPERIODO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@IDP", IDP);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cantidad = Convert.ToInt32(dr["CANT"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaGenerarPPDirectosPeriodo", "USP_LISTADIRECTOSPATROCINIOPERIODO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return cantidad;
        }

        public List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRango(string dni)
        {
            List<PuntosComisiones.CantidadRangoMapaRED> listaPuntos = new List<PuntosComisiones.CantidadRangoMapaRED>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAUPLINERANGO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones.CantidadRangoMapaRED puntos = new PuntosComisiones.CantidadRangoMapaRED();
                    puntos.canSuperiorB = Convert.ToInt32(dr["canSuperiorB"]);
                    puntos.canSuperiorP = Convert.ToInt32(dr["canSuperiorP"]);
                    puntos.canSuperiorO = Convert.ToInt32(dr["canSuperiorO"]);
                    puntos.canSuperiorZ = Convert.ToInt32(dr["canSuperiorZ"]);
                    puntos.canSuperiorR = Convert.ToInt32(dr["canSuperiorR"]);
                    puntos.canSuperiorD = Convert.ToInt32(dr["canSuperiorD"]);
                    puntos.canSuperiorDD = Convert.ToInt32(dr["canSuperiorDD"]);
                    puntos.canSuperiorTD = Convert.ToInt32(dr["canSuperiorTD"]);
                    puntos.canSuperiorDM = Convert.ToInt32(dr["canSuperiorDM"]);
                    puntos.canSuperiorDDM = Convert.ToInt32(dr["canSuperiorDDM"]);
                    puntos.canSuperiorTDM = Convert.ToInt32(dr["canSuperiorTDM"]);
                    puntos.canSuperiorIM = Convert.ToInt32(dr["canSuperiorIM"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaUplinesRango", "USP_LISTAUPLINERANGO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRangoPeriodo(string dni, int IDP)
        {
            List<PuntosComisiones.CantidadRangoMapaRED> listaPuntos = new List<PuntosComisiones.CantidadRangoMapaRED>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAUPLINERANGOPERIODO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones.CantidadRangoMapaRED puntos = new PuntosComisiones.CantidadRangoMapaRED();
                    puntos.canSuperiorB = Convert.ToInt32(dr["canSuperiorB"]);
                    puntos.canSuperiorP = Convert.ToInt32(dr["canSuperiorP"]);
                    puntos.canSuperiorO = Convert.ToInt32(dr["canSuperiorO"]);
                    puntos.canSuperiorZ = Convert.ToInt32(dr["canSuperiorZ"]);
                    puntos.canSuperiorR = Convert.ToInt32(dr["canSuperiorR"]);
                    puntos.canSuperiorD = Convert.ToInt32(dr["canSuperiorD"]);
                    puntos.canSuperiorDD = Convert.ToInt32(dr["canSuperiorDD"]);
                    puntos.canSuperiorTD = Convert.ToInt32(dr["canSuperiorTD"]);
                    puntos.canSuperiorDM = Convert.ToInt32(dr["canSuperiorDM"]);
                    puntos.canSuperiorDDM = Convert.ToInt32(dr["canSuperiorDDM"]);
                    puntos.canSuperiorTDM = Convert.ToInt32(dr["canSuperiorTDM"]);
                    puntos.canSuperiorIM = Convert.ToInt32(dr["canSuperiorIM"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaUplinesRangoPeriodo", "USP_LISTAUPLINERANGOPERIODO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public string GenerarVR(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string VR = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VOLUMENRED", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    VR = Convert.ToString(dr["VR"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "GenerarVR", "USP_VOLUMENRED", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            if (VR == "")
            {
                VR = "0";
            }

            return VR;
        }

        public string GenerarVP(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string VP = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VOLUMENPERSONAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    VP = Convert.ToString(dr["PP"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "GenerarVP", "USP_VOLUMENPERSONAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            if (VP == "")
            {
                VP = "0";
            }

            return VP;
        }

        public string GenerarVPREAL(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string VP = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VOLUMENPERSONAL_REAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    VP = Convert.ToString(dr["PP"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "GenerarVPREAL", "USP_VOLUMENPERSONAL_REAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            if (VP == "")
            {
                VP = "0";
            }

            return VP;
        }

        public string GenerarVCCI(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string VP = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VOLUMENCCI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.CommandTimeout = 0;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    VP = Convert.ToString(dr["VCCI"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "GenerarVCCI", "USP_VOLUMENCCI", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            if (VP == "")
            {
                VP = "0";
            }
            return VP;
        }

        public double ComisionCI(string dni)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string PCI = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_COMISIONCI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PCI = Convert.ToString(dr["COMISION"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ComisionCI", "USP_COMISIONCI", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            if (PCI == "" | PCI == null)
            {
                PCI = "0";
            }
            return Convert.ToDouble(PCI);
        }

        public List<DatosRango> ListaCalculaRango()
        {
            List<DatosRango> Lista = new List<DatosRango>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADORANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DatosRango objRango = new DatosRango();
                    objRango.IdRango = Convert.ToInt32(dr["IDRANGO"]);
                    objRango.Nombre = dr["NOMBRE"].ToString();
                    objRango.PP = Convert.ToInt32(dr["PP"]);
                    objRango.PuntajeRango = Convert.ToInt32(dr["PUNTAJERANGO"]);
                    objRango.PML = Convert.ToInt32(dr["PML"]);
                    Lista.Add(objRango);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaCalculaRango", "USP_LISTADORANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarRango(string dni, string rango, int idRango)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATERANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@RANGO", rango);
                cmd.Parameters.AddWithValue("@IDRANGO", idRango);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarRango", "USP_UPDATERANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarRangoByPeriodo(string dni, string rango, int IDP, int idRango)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATERANGOBYPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@RANGO", rango);
                cmd.Parameters.AddWithValue("@IDRANGO", idRango);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarRangoByPeriodo", "USP_UPDATERANGOBYPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComision(string dni, double comision)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATECOMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@MONTO", comision);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComision", "USP_UPDATECOMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarVP(string dni, double VP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEVP", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@VP", VP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarVP", "USP_UPDATEVP", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarPP(string dni, double PP, int idp)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEPP", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@PP", PP);
                cmd.Parameters.AddWithValue("@IDP", idp);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarPP", "USP_UPDATEPP", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizaRecalculoPuntos(string dni, int idp, double PP, double VP, double VR, double VG, double VIP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RECALCULOPUNTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.Parameters.AddWithValue("@PP", PP);
                cmd.Parameters.AddWithValue("@VP", VP);
                cmd.Parameters.AddWithValue("@VR", VR);
                cmd.Parameters.AddWithValue("@VG", VG);
                cmd.Parameters.AddWithValue("@VIP", VIP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizaRecalculoPuntos", "USP_RECALCULOPUNTOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarVQ(string documento, double VQ)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEVQ", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@VQ", VQ);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarVQ", "USP_UPDATEVQ", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarVQPeriodo(string documento, double VQ, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEVQPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@VQ", VQ);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarVQPeriodo", "USP_UPDATEVQPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public PuntosComisiones ObtenerVPR(string documento)
        {
            PuntosComisiones pts = null;
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAVPRXSOCIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    pts = new PuntosComisiones();
                    pts.PuntosPersonales = Convert.ToDouble(dr["PP"]);
                    pts.VQ = Convert.ToDouble(dr["VQ"]);
                    pts.VolumenPersonal = Convert.ToDouble(dr["VP"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerVPR", "USP_LISTAVPRXSOCIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return pts;
        }

        public List<PuntosComisiones> ListaCalificacion(string documento)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRango = getInstance().ListaUplinesRango(documento);
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CALIFICACION", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.PuntosPersonales = Convert.ToDouble(dr["PP"]);
                    puntos.CanActivos = Convert.ToInt32(dr["CANDACTIVOS"]);
                    puntos.VQ = Convert.ToDouble(dr["VQ"]);
                    puntos.CanDirectos = Convert.ToInt32(dr["CANTDIRECT"]);
                    puntos.canSuperiorIM = ListaUplinesRango[0].canSuperiorIM;
                    puntos.canSuperiorTDM = ListaUplinesRango[0].canSuperiorTDM;
                    puntos.canSuperiorDDM = ListaUplinesRango[0].canSuperiorDDM;
                    puntos.canSuperiorDM = ListaUplinesRango[0].canSuperiorDM;
                    puntos.canSuperiorTD = ListaUplinesRango[0].canSuperiorTD;
                    puntos.canSuperiorDD = ListaUplinesRango[0].canSuperiorDD;
                    puntos.canSuperiorD = ListaUplinesRango[0].canSuperiorD;
                    puntos.canSuperiorR = ListaUplinesRango[0].canSuperiorR;
                    puntos.canSuperiorZ = ListaUplinesRango[0].canSuperiorZ;
                    puntos.canSuperiorO = ListaUplinesRango[0].canSuperiorO;
                    puntos.canSuperiorP = ListaUplinesRango[0].canSuperiorP;
                    puntos.canSuperiorB = ListaUplinesRango[0].canSuperiorB;

                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaCalificacion", "USP_CALIFICACION", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public bool ResetearComAfi(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESET_COMAFI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComAfi", "USP_RESET_COMAFI", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComAfiAutomatico()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESET_COMAFI_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComAfiAutomatico", "USP_RESET_COMAFI_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public int ObtenerEvaluacionMercadeo(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVALUACION_MERCADEO_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.evaluacionMercadeo = Convert.ToInt32(dr["SUMA"].ToString());
                    cantidad = objPeriodo.evaluacionMercadeo;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionMercadeo", "USP_OBTENER_EVALUACION_MERCADEO_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerEvaluacionBronce(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVALBRONCE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.evaluacionBronce = Convert.ToInt32(dr["EVAL"].ToString());
                    cantidad = objPeriodo.evaluacionBronce;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionBronce", "USP_OBTENER_EVALBRONCE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerRequerimientoBronce(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REQUIRIMIENTO_BBRONCE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.requerimientoBronce = Convert.ToInt32(dr["CANT"].ToString());
                    cantidad = objPeriodo.requerimientoBronce;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerRequerimientoBronce", "USP_REQUIRIMIENTO_BBRONCE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerEvaluacionMercadeoRecalculo(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVALUACION_MERCADEO_AUTOMATICO_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.evaluacionMercadeo = Convert.ToInt32(dr["SUMA"].ToString());
                    cantidad = objPeriodo.evaluacionMercadeo;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionMercadeoRecalculo", "USP_OBTENER_EVALUACION_MERCADEO_AUTOMATICO_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerRequerimientoBronceRecalculo(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REQUIRIMIENTO_BBRONCE_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.requerimientoBronce = Convert.ToInt32(dr["CANT"].ToString());
                    cantidad = objPeriodo.requerimientoBronce;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerRequerimientoBronceRecalculo", "USP_REQUIRIMIENTO_BBRONCE_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public double ObtenerPuntosXFechas(string documento, string inicio, string fin)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OTBENER_PUNTOSXFECHA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@INICIO", inicio);
                cmd.Parameters.AddWithValue("@FIN", fin);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    if (dr["PP"].ToString() == "" | dr["PP"].ToString() == null) { objPeriodo.puntos = 0; }
                    else { objPeriodo.puntos = Convert.ToDouble(dr["PP"].ToString()); }
                    cantidad = objPeriodo.puntos;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerPuntosXFechas", "USP_OTBENER_PUNTOSXFECHA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public bool ActualizarComisionMercadeo(string documento, double monto)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_COMERCA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComisionMercadeo", "USP_ACTUALIZAR_COMERCA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public void ActualizarComisionMercadeoRecalculo(string documento, double monto, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_COMERCA_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComisionMercadeoRecalculo", "USP_ACTUALIZAR_COMERCA_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ActualizarComisionBronce(string documento, double monto)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_EVALBRON", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComisionBronce", "USP_UPDATE_EVALBRON", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComisionBronceRecalculo(string documento, double monto, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_EVALBRON_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComisionBronceRecalculo", "USP_UPDATE_EVALBRON_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public int ObtenerEvaluacionTiburon(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVALTIBURON", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.evaluacionTiburon = Convert.ToInt32(dr["EVALTIBU"].ToString());
                    cantidad = objPeriodo.evaluacionTiburon;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionTiburon", "USP_OBTENER_EVALTIBURON", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public bool ResetearComTiburon(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESETEAR_CALCULO_TIBURON", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComTiburon", "USP_RESETEAR_CALCULO_TIBURON", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComBronce(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESETEAR_BBRONCE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComBronce", "USP_RESETEAR_BBRONCE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComEscolaridad(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESETEAR_CALCULO_ESCOLARIDAD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComEscolaridad", "USP_RESETEAR_CALCULO_ESCOLARIDAD", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComEscolaridadAutomatico()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("DBO.USP_RESETEAR_CALCULO_ESCOLARIDAD_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComEscolaridadAutomatico", "DBO.USP_RESETEAR_CALCULO_ESCOLARIDAD_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente> ListarDniXRangoComisiones(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAR_DNIXRANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DOCUMENTO"].ToString();
                    if (Convert.ToString(dr["RANGO"]) == null)
                    {
                        objCliente.rango = "";
                    }
                    else { objCliente.rango = dr["RANGO"].ToString(); }
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListarDniXRangoComisiones", "USP_LISTAR_DNIXRANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarRangoComision(string documento, string rango, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_RANGO_PERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RANGO", rango);
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarRangoComision", "USP_UPDATE_RANGO_PERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public int ObtenerEvaluacionEscolaridadRecalculo(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVAL_ESCOLARIDAD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.evaluacionEscolaridad = Convert.ToInt32(dr["CANT"].ToString());
                    cantidad = objPeriodo.evaluacionEscolaridad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionEscolaridadRecalculo", "USP_OBTENER_EVAL_ESCOLARIDAD", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerEvaluacionEscolaridadAutomatico(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVAL_ESCOLARIDAD_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.evaluacionEscolaridad = Convert.ToInt32(dr["CANT"].ToString());
                    cantidad = objPeriodo.evaluacionEscolaridad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionEscolaridadAutomatico", "USP_OBTENER_EVAL_ESCOLARIDAD_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerRequerimientoEscolaridadRecalculo(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REQUERIMIENTO_COMESCOLA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.requerimientoEscolaridad = Convert.ToInt32(dr["CANT"].ToString());
                    cantidad = objPeriodo.requerimientoEscolaridad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerRequerimientoEscolaridadRecalculo", "USP_REQUERIMIENTO_COMESCOLA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ObtenerRequerimientoEscolaridadAutomatico(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REQUERIMIENTO_COMESCOLA_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.requerimientoEscolaridad = Convert.ToInt32(dr["CANT"].ToString());
                    cantidad = objPeriodo.requerimientoEscolaridad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerRequerimientoEscolaridadAutomatico", "USP_REQUERIMIENTO_COMESCOLA_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public bool ActualizarEscolaridadRecalculo(string documento, double monto, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_EVALXMONTO_ESCOLARIDAD_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarEscolaridadRecalculo", "USP_UPDATE_EVALXMONTO_ESCOLARIDAD_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarEscolaridadAutomatico(string documento, double monto)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_EVALXMONTO_ESCOLARIDAD_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarEscolaridadAutomatico", "USP_UPDATE_EVALXMONTO_ESCOLARIDAD_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComMatricial(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESETEAR_CALCULO_MATRICIAL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComMatricial", "USP_RESETEAR_CALCULO_MATRICIAL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public double ObtenerSumaPPRecalculo(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMA_PP_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.sumaPP = (dr["SUMA"] == DBNull.Value) ? 0 : Convert.ToDouble(dr["SUMA"]);
                    cantidad = objPeriodo.sumaPP;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerSumaPPRecalculo", "USP_SUMA_PP_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public double ObtenerSumaPPRecalculoMatricial(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMA_PP_COMISION_MATRICIAL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.sumaPP = (dr["SUMA"] == DBNull.Value) ? 0 : Convert.ToDouble(dr["SUMA"]);
                    cantidad = objPeriodo.sumaPP;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerSumaPPRecalculoMatricial", "USP_SUMA_PP_COMISION_MATRICIAL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public List<Periodo> ListaEvaluacionMatricial(string documento, int IDP)
        {
            List<Periodo> listaEvaluacion = new List<Periodo>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_EVAL_MATRICIAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo puntos = new Periodo();
                    puntos.evaluacionMatrocial4x4 = Convert.ToInt32(dr["CANT4X4"]);
                    puntos.evaluacionMatrocial5x5 = Convert.ToInt32(dr["CANT5X5"]);
                    puntos.evaluacionMatrocial6x6 = Convert.ToInt32(dr["CANT6X6"]);
                    listaEvaluacion.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaEvaluacionMatricial", "USP_OBTENER_EVAL_MATRICIAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return listaEvaluacion;
        }

        public List<Cliente> ListarEvaluacionDirectosMatricial(string documento, int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMA_PP_COMISION_DIRECTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.PP = Convert.ToDouble(dr["SUMA"].ToString());
                    if (objCliente.PP >= 60) { Lista.Add(objCliente); }
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListarEvaluacionDirectosMatricial", "USP_SUMA_PP_COMISION_DIRECTOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarMatricialRecalculo(string documento, double monto, int IDP, int M4X4, int M5X5, int M6X6)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_EVALXMONTO_MATRICIAL_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                cmd.Parameters.AddWithValue("@EVAL_MATR4X4", M4X4);
                cmd.Parameters.AddWithValue("@EVAL_MATR5X5", M5X5);
                cmd.Parameters.AddWithValue("@EVAL_MATR6X6", M6X6);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarMatricialRecalculo", "USP_UPDATE_EVALXMONTO_MATRICIAL_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComisionUnilevel(string documento, double COMUNI, int IDP, double nivel1, double nivel2, double nivel3,
                                                 double nivel4, double nivel5, double nivel6, double nivel7, double nivel8, double nivel9,
                                                 double nivel10, double nivel11, double nivel12, double nivel13, double nivel14, double nivel15)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARMONTOAUNILEVEL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@COMUNI", COMUNI);
                cmd.Parameters.AddWithValue("@UNIX1", nivel1);
                cmd.Parameters.AddWithValue("@UNIX2", nivel2);
                cmd.Parameters.AddWithValue("@UNIX3", nivel3);
                cmd.Parameters.AddWithValue("@UNIX4", nivel4);
                cmd.Parameters.AddWithValue("@UNIX5", nivel5);
                cmd.Parameters.AddWithValue("@UNIX6", nivel6);
                cmd.Parameters.AddWithValue("@UNIX7", nivel7);
                cmd.Parameters.AddWithValue("@UNIX8", nivel8);
                cmd.Parameters.AddWithValue("@UNIX9", nivel9);
                cmd.Parameters.AddWithValue("@UNIX10", nivel10);
                cmd.Parameters.AddWithValue("@UNIX11", nivel11);
                cmd.Parameters.AddWithValue("@UNIX12", nivel12);
                cmd.Parameters.AddWithValue("@UNIX13", nivel13);
                cmd.Parameters.AddWithValue("@UNIX14", nivel14);
                cmd.Parameters.AddWithValue("@UNIX15", nivel15);

                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComisionUnilevel", "USP_ACTUALIZARMONTOAUNILEVEL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComUnilevel(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESET_COMUNILEVEL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComUnilevel", "USP_RESET_COMUNILEVEL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComCI(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESET_COMCI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComCI", "USP_RESET_COMCI", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ResetearComCon(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RESET_COMCONSULTOR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ResetearComCon", "USP_RESET_COMCONSULTOR", ex.Message);
                throw ex;
                
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public int MostrarIdPeriodoComisionActivo()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int id = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_MOSTRAR_IDPERIODO_COMISION_ACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    id = objPeriodo.idPeriodoComision;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "MostrarIdPeriodoComisionActivo", "USP_MOSTRAR_IDPERIODO_COMISION_ACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return id;
        }

        public int MostrarIdPeriodoPuntosActivo()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int id = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_MOSTRAR_IDPERIODO_PUNTOS_ACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO"]);
                    id = objPeriodo.idPeriodoComision;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "MostrarIdPeriodoPuntosActivo", "USP_MOSTRAR_IDPERIODO_PUNTOS_ACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return id;
        }

        public bool ActualizarComCIRecalculo(string documento, double monto, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_COMISICONCI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComCIRecalculo", "USP_UPDATE_COMISICONCI", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComConsultorRecalculo(string documento, double monto, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_COMISICON_CONSULTOR_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarComConsultorRecalculo", "USP_UPDATE_COMISICON_CONSULTOR_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public int ObtenerRangoIDPComision(string documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int rango = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_RANGO_IDP_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idRango = Convert.ToInt32(dr["IDRANGO"]);
                    rango = objPeriodo.idRango;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerRangoIDPComision", "USP_OBTENER_RANGO_IDP_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return rango;
        }

        public bool ActualizarMaximoRango(string documento, string Mrango, int IDRangoMax)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_RANGOMAXIMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MAXIMORANGO", Mrango);
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDRANGO_MAXIMO", IDRangoMax);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarMaximoRango", "USP_ACTUALIZAR_RANGOMAXIMO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public Periodo.MaximoRango ObtenerRangoMaximoXSocio(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            Periodo.MaximoRango objPeriodo = new Periodo.MaximoRango();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_DATOS_RANGOMAXIMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objPeriodo.rango = Convert.ToString(dr["RANGO"].ToString());
                    objPeriodo.idRango = Convert.ToInt32(dr["IDRANGO"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerRangoMaximoXSocio", "USP_OBTENER_DATOS_RANGOMAXIMO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objPeriodo;
        }

        public List<PuntosComisiones> ListaTop10VQ(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_TOP10_VQ", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.NombreSocio = Convert.ToString(dr["Apellidos"]);
                    puntos.dni = Convert.ToString(dr["NumeroDoc"]);
                    puntos.VQ = Convert.ToDouble(dr["VQ"]);
                    puntos.RangoDefinido = Convert.ToString(dr["RANGO"]);
                    puntos.Celular = Convert.ToString(dr["TELEFONO"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaTop10VQ", "USP_TOP10_VQ", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones> ListaTop10Comisiones(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_TOP10_COMISIONES", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.NombreSocio = Convert.ToString(dr["NOMBRES"]);
                    puntos.ComisionTotal = Convert.ToDouble(dr["COMITOTAL"]);
                    puntos.RangoDefinido = Convert.ToString(dr["RANGO"]);
                    puntos.Celular = Convert.ToString(dr["Celular"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaTop10Comisiones", "USP_TOP10_COMISIONES", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones> ListaGanadoresPremio(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GANADORES_PREMIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@IDP", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.dni = Convert.ToString(dr["NumeroDoc"]);
                    puntos.NombreSocio = Convert.ToString(dr["Apellidos"]);
                    puntos.Celular = Convert.ToString(dr["CELULAR"]);
                    puntos.Premio = Convert.ToString(dr["PREMIO"]);
                    puntos.CDR = Convert.ToString(dr["CDR"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaGanadoresPremio", "USP_GANADORES_PREMIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones> ListaSociosComprimir(string dni, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SOCIOS_COMPRIMIR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@IDP", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.dni = Convert.ToString(dr["NumeroDoc"]);
                    puntos.NombreSocio = Convert.ToString(dr["Apellidos"]);
                    puntos.Celular = Convert.ToString(dr["CELULAR"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaSociosComprimir", "USP_SOCIOS_COMPRIMIR", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public List<PuntosComisiones.CalculoPuntos> ListaSociosCalculoPuntos(int idp)
        {
            List<PuntosComisiones.CalculoPuntos> listaPuntos = new List<PuntosComisiones.CalculoPuntos>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_PERIODO_PUNTOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDPERIODO", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones.CalculoPuntos puntos = new PuntosComisiones.CalculoPuntos();
                    puntos.UPLINE = Convert.ToString(dr["UPLINE"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaSociosCalculoPuntos", "USP_LISTA_SOCIOS_PERIODO_PUNTOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public string ObtenerIdSocioByDocumento(string documento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string idsocio = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("OBTENER_IDSOCIO_BY_DOCUMENTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    idsocio = Convert.ToString(dr["IDCLIENTE"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerIdSocioByDocumento", "OBTENER_IDSOCIO_BY_DOCUMENTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return idsocio;
        }

        public void ActualizarIdSocioUplinePuntos(string idsocio, string patrocinador)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("ACTUALIZAR_IDUPLINE_BY_DOCUMENTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOCIO", idsocio);
                cmd.Parameters.AddWithValue("@IDPATROCINADOR", patrocinador);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarIdSocioUplinePuntos", "ACTUALIZAR_IDUPLINE_BY_DOCUMENTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int ObtenerIdPeriodoComisionAnteriorActivo()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int id = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_IDP_COMISION_ANTERIOR_ACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    id = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerIdPeriodoComisionAnteriorActivo", "USP_OBTENER_IDP_COMISION_ANTERIOR_ACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return id;
        }

        public List<PuntosComisiones> ListaLineaMultinivelUpline(int idp, string idcliente)
        {
            List<PuntosComisiones> lista = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_UPLINES_CONTROL_CANJES", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@IDPERIODO", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.NombreUpline = Convert.ToString(dr["Nombres"]);
                    lista.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaLineaMultinivelUpline", "USP_LISTA_UPLINES_CONTROL_CANJES", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<PuntosComisiones> ListaLineaMultinivelPatrocinio(int idp, string idcliente)
        {
            List<PuntosComisiones> lista = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PATROCINADORES_CONTROL_CANJES", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@IDPERIODO", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.NombrePatrocinador = Convert.ToString(dr["Nombres"]);
                    lista.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaLineaMultinivelPatrocinio", "USP_LISTA_PATROCINADORES_CONTROL_CANJES", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<PuntosComisiones> ListaDatosRangoIndividual(string documento, int idp)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_CALCULO_RANGO_INDIVIDUAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", idp);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones puntos = new PuntosComisiones();
                    puntos.PuntosPersonales = Convert.ToDouble(dr["PP"]);
                    puntos.VolumenPersonal = Convert.ToDouble(dr["VP"]);
                    puntos.VolumenRed = Convert.ToDouble(dr["VPR"]);
                    listaPuntos.Add(puntos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaDatosRangoIndividual", "USP_LISTA_DATOS_CALCULO_RANGO_INDIVIDUAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public void RegistrarDatosRangoIndividual(string Documento, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRAR_DATOS_CALCULO_INDIVIDUAL_RANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", Documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "RegistrarDatosRangoIndividual", "USP_REGISTRAR_DATOS_CALCULO_INDIVIDUAL_RANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<PuntosComisiones.DatosIndividualRango> ListaSociosRangoIndividual()
        {
            List<PuntosComisiones.DatosIndividualRango> listaPuntos = new List<PuntosComisiones.DatosIndividualRango>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_CALCULO_INDIVIDUAL_RANGO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PuntosComisiones.DatosIndividualRango datos = new PuntosComisiones.DatosIndividualRango();
                    datos.IdCalculo = Convert.ToInt32(dr["ID_CALCULO"]);
                    datos.Documento = Convert.ToString(dr["DOCUMENTO"]);
                    datos.IdPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    listaPuntos.Add(datos);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ListaSociosRangoIndividual", "USP_LISTA_DATOS_CALCULO_INDIVIDUAL_RANGO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaPuntos;
        }

        public void EliminarDatosRangoIndividual(int IdCalculo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DELETE_DATOS_CALCULO_RANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_CALCULO", IdCalculo);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "EliminarDatosRangoIndividual", "USP_DELETE_DATOS_CALCULO_RANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public int ObtenerEvaluacionCalculoRango(int IdCalculo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int validar = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CONSULTA_EXISTENCIA_CALCULO_RANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_CALCULO", IdCalculo);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    validar = Convert.ToInt32(dr["VALIDAR"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEvaluacionCalculoRango", "USP_CONSULTA_EXISTENCIA_CALCULO_RANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return validar;
        }

        public void ActualizarEstadoTareaRango(bool Estado)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_ESTADO_TAREA_RANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ESTADO", Estado);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ActualizarEstadoTareaRango", "USP_ACTUALIZAR_ESTADO_TAREA_RANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ObtenerEstadoTareaRangos()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            bool estado = true;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CONSULTA_ESTADO_TAREA_RANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    estado = Convert.ToBoolean(dr["ESTADO"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PuntosComisionesDAO", "ObtenerEstadoTareaRangos", "USP_CONSULTA_ESTADO_TAREA_RANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return estado;
        }

    }
}
