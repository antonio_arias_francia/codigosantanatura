﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Modelos;

namespace Datos
{
    public class StockDAO
    {
        #region "PATRON SINGLETON"
        private static StockDAO daoStock = null;
        private StockDAO() { }
        public static StockDAO getInstance()
        {
            if (daoStock == null)
            {
                daoStock = new StockDAO();
            }
            return daoStock;
        }
        #endregion

        public List<StockCDR> ListadoCDR()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_COMBOCDRSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.DNICDR = dr["NumeroDoc"].ToString();
                    cdr.CDRPS = dr["IdPeruShop"].ToString();
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListadoCDR", "USP_COMBOCDRSTOCK", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<StockCDR> ListarCDRStock()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACDRGUARDADOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.DNICDR = dr["NumeroDoc"].ToString().Trim();
                    cdr.Apodo = dr["Apodo"].ToString().Trim();
                    cdr.CDRPS = dr["IdPeruShop"].ToString().Trim();
                    cdr.Pais = dr["Pais"].ToString();
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListarCDRStock", "USP_LISTACDRGUARDADOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<StockCDR> ListaProductosXCDR(string dniCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPRODUCTOSXSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dniCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.Fila = dr["FILA"].ToString();
                    cdr.IDProducto = dr["IDPRODUCTO"].ToString();
                    cdr.IDProductoXPais = dr["IdProductoxPais"].ToString();
                    cdr.IDPS = dr["IDPS"].ToString();
                    cdr.Imagen = dr["IMAGEN"].ToString();
                    cdr.Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                    cdr.NombreProducto = dr["Nombre"].ToString();
                    cdr.ControlStock = Convert.ToBoolean(dr["ActivarConsulta"]);
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListaProductosXCDR", "USP_LISTAPRODUCTOSXSTOCK", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<StockCDR> ListaProductosXSolicitud(int idSolicitud)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PRODUCTOSXSOLICITUD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOLICITUD", idSolicitud);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.Fila = dr["FILA"].ToString();
                    cdr.NombreProducto = dr["Nombre"].ToString();
                    cdr.Imagen = dr["Foto"].ToString();
                    cdr.IDPS = dr["IDPS"].ToString();
                    cdr.IDProductoXPais = dr["IdProductoxPais"].ToString();
                    cdr.Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                    cdr.TipoCompra = dr["TipoCompra"].ToString(); 
                    cdr.IDProducto = dr["IDPRODUCTO"].ToString();
                    cdr.PrecioCDR = Convert.ToDouble(dr["PRECIOCDR"]);
                    cdr.UnidadesXPrese = Convert.ToInt32(dr["UNIDADESPORPRESENTACION"]);
                    listaCDR.Add(cdr);
                }
            
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListaProductosXSolicitud", "USP_LISTA_PRODUCTOSXSOLICITUD", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<StockCDR> ListarProductosRegistro()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPRODUCTOSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.IDProducto = dr["IdProducto"].ToString();
                    cdr.IDProductoXPais = dr["IdProductoxPais"].ToString();
                    cdr.NombreProducto = dr["Nombre"].ToString();
                    cdr.Imagen = dr["Foto"].ToString();
                    cdr.IDPS = dr["IdProductoPeruShop"].ToString();
                    cdr.Fila = dr["FILA"].ToString();
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListarProductosRegistro", "USP_LISTAPRODUCTOSTOCK", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public bool RegistroStock(StockCDR Stocks)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", Stocks.DNICDR.Trim());
                cmd.Parameters.AddWithValue("@IDPRODUCTO", Stocks.IDProducto.Trim());
                cmd.Parameters.AddWithValue("@IDPS", Stocks.IDPS.Trim());
                cmd.Parameters.AddWithValue("@IMAGEN", Stocks.Imagen.Trim());
                cmd.Parameters.AddWithValue("@CANTIDAD", Stocks.Cantidad);
                cmd.Parameters.AddWithValue("@IDPRODUCTOXPAIS", Stocks.IDProductoXPais);
                cmd.Parameters.AddWithValue("@ACTIVARCONSULTA", Stocks.ControlStock);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "RegistroStock", "USP_REGISTROSTOCK", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool RegistroProductoSolicitud(StockCDR Stocks)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_SOLICITUD_PRODUCTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", Stocks.DNICDR.Trim());
                cmd.Parameters.AddWithValue("@IDPRODUCTO", Stocks.IDProducto.Trim());
                cmd.Parameters.AddWithValue("@IDPRODUCTOXPAIS", Stocks.IDProductoXPais.Trim());
                cmd.Parameters.AddWithValue("@IDPS", Stocks.IDPS.Trim());
                cmd.Parameters.AddWithValue("@CANTIDAD", Stocks.Cantidad);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "RegistroProductoSolicitud", "USP_REGISTRO_SOLICITUD_PRODUCTOS", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public string GenerarIDSolicitud(string dni, string fecha, string TipoCompra, double monto)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string response = "";

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_SOLICITUD_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni.Trim());
                cmd.Parameters.AddWithValue("@FECHA", fecha.Trim());
                cmd.Parameters.AddWithValue("@TIPOCOMPRA", TipoCompra.Trim());
                cmd.Parameters.AddWithValue("@MONTO", monto);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = dr["IDSOLICITUD"].ToString();
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "GenerarIDSolicitud", "USP_REGISTRO_SOLICITUD_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarStock(StockCDR Stocks)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNICDR", Stocks.DNICDR.Trim());
                cmd.Parameters.AddWithValue("@IDPRODUCTOPAIS", Stocks.IDProductoXPais.Trim());
                cmd.Parameters.AddWithValue("@CANT", Stocks.Cantidad);
                cmd.Parameters.AddWithValue("@ACTIVARCONSULTA", Stocks.ControlStock);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ActualizarStock", "USP_ACTUALIZARSTOCK", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarProductoenStock(StockCDR Stocks)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARPRODUCTOSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTOPAIS", Stocks.IDProductoXPais.Trim());
                cmd.Parameters.AddWithValue("@IDPS", Stocks.IDPS.Trim());
                cmd.Parameters.AddWithValue("@IMAGEN", Stocks.Imagen.Trim());
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ActualizarProductoenStock", "USP_ACTUALIZARPRODUCTOSTOCK", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public int CantidadStockCDR(string dniCDR, string IDPS, string pais, string IDPP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int Cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTARSTOCKTIENDA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPS", IDPS);
                cmd.Parameters.AddWithValue("@IDPP", IDPP);
                cmd.Parameters.AddWithValue("@DNI", dniCDR);
                cmd.Parameters.AddWithValue("@PAIS", pais);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "CantidadStockCDR", "USP_LISTARSTOCKTIENDA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Cantidad;
        }

        public bool DescontarStock(string dniCdr, string idProducto, int cantidad)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DESCONTARSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CANTIDAD", cantidad);
                cmd.Parameters.AddWithValue("@DNICDR", dniCdr);
                cmd.Parameters.AddWithValue("@IDPRODUCTOPAIS", idProducto);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "DescontarStock", "USP_DESCONTARSTOCK", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool IncrementarStock(string dniCdr, string idProducto, int cantidad)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RECUPERARSTOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CANTIDAD", cantidad);
                cmd.Parameters.AddWithValue("@DNICDR", dniCdr);
                cmd.Parameters.AddWithValue("@IDPRODUCTOPAIS", idProducto);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "IncrementarStock", "USP_RECUPERARSTOCK", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarEstadoSolicitud(int idSoli, string idop, string idop2, string fechaVal)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_ESTADO_PEDIDO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOLI", idSoli);
                cmd.Parameters.AddWithValue("@IDOP", idop);
                cmd.Parameters.AddWithValue("@IDOP2", idop2);
                cmd.Parameters.AddWithValue("@FECHAVALIDADA", fechaVal);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ActualizarEstadoSolicitud", "USP_ACTUALIZAR_ESTADO_PEDIDO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public void ActualizarEstadoSolicitudPE(int idSoli, string idop)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_ESTADO_PEDIDO_PE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOLI", idSoli);
                cmd.Parameters.AddWithValue("@IDOP", idop);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ActualizarEstadoSolicitudPE", "USP_ACTUALIZAR_ESTADO_PEDIDO_PE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<StockCDR> ListarProductosPedidoCDR(string pais)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPRODUCTOPEDIDOCDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PAIS", pais);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.IDProducto = dr["IdProducto"].ToString();
                    cdr.IDProductoXPais = dr["IdProductoxPais"].ToString();
                    cdr.NombreProducto = dr["Nombre"].ToString();
                    cdr.Imagen = dr["Foto"].ToString();
                    cdr.IDPS = dr["IdProductoPeruShop"].ToString();
                    cdr.Fila = dr["FILA"].ToString();
                    cdr.PrecioCDR = Convert.ToDouble(dr["precioCDR"]);
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListarProductosPedidoCDR", "USP_LISTAPRODUCTOPEDIDOCDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<StockCDR> ListarSolicitudesGeneradas(string dni)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOLICITUD_GENERADA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.Fila = dr["FILA"].ToString();
                    cdr.FechaSolicitud = dr["FECHASOLICITUD"].ToString();
                    cdr.CDRPS = dr["IdPeruShop"].ToString();
                    cdr.TipoCompra = dr["TipoCompra"].ToString();
                    cdr.IdSolicitud = Convert.ToInt32(dr["IDSOLICITUD"]);
                    cdr.MontoTotal = Convert.ToDouble(dr["Monto"]);
                    cdr.Cantidad = Convert.ToInt32(dr["CantTotal"]);
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListarSolicitudesGeneradas", "USP_LISTA_SOLICITUD_GENERADA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public List<StockCDR> ListarSolicitudesTotales(string fecha1, string fecha2)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<StockCDR> listaCDR = new List<StockCDR>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOLICITUDES_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StockCDR cdr = new StockCDR();
                    cdr.Fila = dr["FILA"].ToString();
                    cdr.FechaSolicitud = dr["FECHASOLICITUD"].ToString();
                    cdr.CDRPS = dr["IdPeruShop"].ToString();
                    cdr.IdSolicitud = Convert.ToInt32(dr["IDSOLICITUD"]);
                    cdr.Estado = dr["Estado"].ToString();
                    cdr.DNICDR = dr["DNICDR"].ToString();
                    cdr.TipoCompra = dr["TipoCompra"].ToString();
                    cdr.MontoTotal = Convert.ToDouble(dr["Monto"]);
                    listaCDR.Add(cdr);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ListarSolicitudesTotales", "USP_LISTA_SOLICITUDES_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCDR;
        }

        public int UnidadPorPresentacionXProducto(string idProducto)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int Cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANTIDADPACK_PRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", idProducto);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cantidad = Convert.ToInt32(dr["UnidadesPorPresentacion"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "UnidadPorPresentacionXProducto", "USP_OBTENER_CANTIDADPACK_PRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Cantidad;
        }

        public void RegistroDatosPagoEfectivoCDR(Cliente.CDR.DatosPagoEfectivoCDR objCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("REGISTRO_DATOS_PAGO_EFECTIVO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPEDIDO", objCDR.IdDatos);
                cmd.Parameters.AddWithValue("@DNICDR", objCDR.DniCDR);
                cmd.Parameters.AddWithValue("@FECHACREACION", objCDR.FechaCreacion);
                cmd.Parameters.AddWithValue("@FECHAEXPIRACION", objCDR.FechaExpiracion);
                cmd.Parameters.AddWithValue("@CIP", objCDR.CIP);
                cmd.Parameters.AddWithValue("@MONTO", objCDR.Monto);
                cmd.Parameters.AddWithValue("@CODE_STATUS", objCDR.CodeStatus);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "RegistroDatosPagoEfectivoCDR", "REGISTRO_DATOS_PAGO_EFECTIVO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ObtenerControlStockXIDPP(string Idpp, string DniCDR)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CONTROL_STOCK_IDPP", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPP", Idpp);
                cmd.Parameters.AddWithValue("@DNICDR", DniCDR);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    response = Convert.ToBoolean(dr["ActivarConsulta"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ObtenerControlStockXIDPP", "USP_OBTENER_CONTROL_STOCK_IDPP", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public double ObtenerPrecioCdrXIDPP(string idPP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double Monto = 0.0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_PRECIO_CDR_BY_IDPP", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPP", idPP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Monto = Convert.ToDouble(dr["precioCDR"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ObtenerPrecioCdrXIDPP", "USP_OBTENER_PRECIO_CDR_BY_IDPP", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Monto;
        }

        public double ObtenerPrecioXidPP_SD(string idPP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double Monto = 0.0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_PRECIO_BY_IDPP_SD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPP", idPP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Monto = Convert.ToDouble(dr["PRECIOUNITARIO"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "ObtenerPrecioXIDPP_SD", "USP_OBTENER_PRECIO_BY_IDPP_SD", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Monto;
        }

        public void EliminarPedidoCDR(int idSoli)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_PEDIDO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOLICITUD", idSoli);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("StockDAO", "EliminarPedidoCDR", "USP_ELIMINAR_PEDIDO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

    }
}
