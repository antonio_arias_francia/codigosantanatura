﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class PacketeDatos
    {
        #region "PATRON SINGLETON"
        private static PacketeDatos datosPackete = null;
        private PacketeDatos() { }
        public static PacketeDatos getInstance()
        {
            if (datosPackete == null)
            {
                datosPackete = new PacketeDatos();
            }
            return datosPackete;
        }
        #endregion

        public List<Packete> ListarPacketes(string packete, string tipoCiente)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Packete> listaPackete = new List<Packete>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarPacketesByPacketeAndTipoCliente", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tipoCliente", tipoCiente);
                cmd.Parameters.AddWithValue("@packete", packete);

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Packete objPackete = new Packete();
                    objPackete.Codigo = dr["IdPackete"].ToString();
                    objPackete._Packete = dr["Packete"].ToString();
                    objPackete.Descuento = dr["Descuento"].ToString();
                    objPackete.Puntos = Convert.ToInt32(dr["Puntos"]);
                    objPackete.Monto = Convert.ToDouble(dr["Monto"]);
                    listaPackete.Add(objPackete);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PacketeDatos", "ListarPacketes", "USP_ListarPacketesByPacketeAndTipoCliente", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPackete;
        }

        public List<Packete> ListarPacketes_PreRegistro(string packete, string tipoCiente)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Packete> listaPackete = new List<Packete>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_TIPOCOMPRA_PREREGISTRO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@tipoCliente", tipoCiente);
                cmd.Parameters.AddWithValue("@packete", packete);

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Packete objPackete = new Packete();
                    objPackete.Codigo = dr["IdPackete"].ToString();
                    objPackete._Packete = dr["Packete"].ToString();
                    objPackete.Descuento = dr["Descuento"].ToString();
                    objPackete.Puntos = Convert.ToInt32(dr["Puntos"]);
                    objPackete.Monto = Convert.ToDouble(dr["Monto"]);
                    listaPackete.Add(objPackete);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PacketeDatos", "ListarPacketes_PreRegistro", "USP_LISTA_TIPOCOMPRA_PREREGISTRO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPackete;
        }

        public List<Packete> ListadoPacketesGeneral()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Packete> listaPackete = new List<Packete>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPAQUETE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Packete packete = new Packete();
                    packete.Codigo = dr["IdPackete"].ToString();
                    packete._Packete = dr["Packete"].ToString();
                    listaPackete.Add(packete);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PacketeDatos", "ListadoPacketesGeneral", "USP_LISTAPAQUETE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPackete;
        }

    }
}
