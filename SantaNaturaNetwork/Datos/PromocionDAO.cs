﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class PromocionDAO
    {
        #region "PATRON SINGLETON"
        private static PromocionDAO _promoDatos = null;
        private PromocionDAO() { }
        public static PromocionDAO getInstance()
        {
            if (_promoDatos == null)
            {
                _promoDatos = new PromocionDAO();
            }
            return _promoDatos;
        }
        #endregion

        public bool RegistroPromocion(Promocion objPromocion)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROPROMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOM_PROM", objPromocion.nom_promo);
                cmd.Parameters.AddWithValue("@RANGO", objPromocion.rango);
                cmd.Parameters.AddWithValue("@RANGOFIN", objPromocion.rangofin);
                cmd.Parameters.AddWithValue("@ESTADO_G", objPromocion.estado_gen);
                cmd.Parameters.AddWithValue("@PUN1", objPromocion.puntos1);
                cmd.Parameters.AddWithValue("@PRODU1", objPromocion.producto1);
                cmd.Parameters.AddWithValue("@ESTAD1", objPromocion.estado1);
                cmd.Parameters.AddWithValue("@PUN2", objPromocion.puntos2);
                cmd.Parameters.AddWithValue("@PRODU2", objPromocion.producto2);
                cmd.Parameters.AddWithValue("@ESTAD2", objPromocion.estado2);
                cmd.Parameters.AddWithValue("@PUN3", objPromocion.puntos3);
                cmd.Parameters.AddWithValue("@PRODU3", objPromocion.producto3);
                cmd.Parameters.AddWithValue("@ESTAD3", objPromocion.estado3);
                cmd.Parameters.AddWithValue("@PUN4", objPromocion.puntos4);
                cmd.Parameters.AddWithValue("@PRODU4", objPromocion.producto4);
                cmd.Parameters.AddWithValue("@ESTAD4", objPromocion.estado4);
                cmd.Parameters.AddWithValue("@PUN_BASICO", objPromocion.punt_basico);
                cmd.Parameters.AddWithValue("@PROD_BASICO", objPromocion.prod_basico);
                cmd.Parameters.AddWithValue("@ESTADO_BASICO", objPromocion.esta_basico);
                cmd.Parameters.AddWithValue("@PUN_PROF", objPromocion.punt_prof);
                cmd.Parameters.AddWithValue("@PROD_PROF", objPromocion.prod_prof);
                cmd.Parameters.AddWithValue("@ESTADO_PROF", objPromocion.esta_prof);
                cmd.Parameters.AddWithValue("@PUN_EMP", objPromocion.punt_emp);
                cmd.Parameters.AddWithValue("@PROD_EMP", objPromocion.prod_emp);
                cmd.Parameters.AddWithValue("@ESTADO_EMP", objPromocion.esta_emp);
                cmd.Parameters.AddWithValue("@PUN_MILL", objPromocion.punt_mill);
                cmd.Parameters.AddWithValue("@PROD_MILL", objPromocion.prod_mill);
                cmd.Parameters.AddWithValue("@ESTADO_MILL", objPromocion.esta_mill);
                cmd.Parameters.AddWithValue("@PXC1", objPromocion.pxc1);
                cmd.Parameters.AddWithValue("@PXC2", objPromocion.pxc2);
                cmd.Parameters.AddWithValue("@PXC3", objPromocion.pxc3);
                cmd.Parameters.AddWithValue("@PXC4", objPromocion.pxc4);
                cmd.Parameters.AddWithValue("@PUNTO_CI", objPromocion.punt_ci);
                cmd.Parameters.AddWithValue("@PRODUC_CI", objPromocion.prod_ci);
                cmd.Parameters.AddWithValue("@ESTADO_CI", objPromocion.esta_ci);
                cmd.Parameters.AddWithValue("@PUNTO_CON", objPromocion.punt_con);
                cmd.Parameters.AddWithValue("@PRODUC_CON", objPromocion.prod_con);
                cmd.Parameters.AddWithValue("@ESTADO_CON", objPromocion.esta_con);
                cmd.Parameters.AddWithValue("@REGALO1", objPromocion.regalo1);
                cmd.Parameters.AddWithValue("@REGALO2", objPromocion.regalo2);
                cmd.Parameters.AddWithValue("@REGALO3", objPromocion.regalo3);
                cmd.Parameters.AddWithValue("@REGALO4", objPromocion.regalo4);
                cmd.Parameters.AddWithValue("@REGBASICO1", objPromocion.regBasico1);
                cmd.Parameters.AddWithValue("@REGPROFE1", objPromocion.regProfe1);
                cmd.Parameters.AddWithValue("@REGEMPRE1", objPromocion.regEmpre1);
                cmd.Parameters.AddWithValue("@REGMILLO1", objPromocion.regMillo1);
                cmd.Parameters.AddWithValue("@REGBASICO2", objPromocion.regBasico2);
                cmd.Parameters.AddWithValue("@REGPROFE2", objPromocion.regProfe2);
                cmd.Parameters.AddWithValue("@REGEMPRE2", objPromocion.regEmpre2);
                cmd.Parameters.AddWithValue("@REGMILLO2", objPromocion.regMillo2);
                cmd.Parameters.AddWithValue("@PUNTO_BASICO2", objPromocion.punt_basico2);
                cmd.Parameters.AddWithValue("@PUNTO_PROF2", objPromocion.punt_prof2);
                cmd.Parameters.AddWithValue("@PUNTO_EMP2", objPromocion.punt_emp2);
                cmd.Parameters.AddWithValue("@PUNTO_MILL2", objPromocion.punt_mill2);
                cmd.Parameters.AddWithValue("@PRODUC_BASICO2", objPromocion.prod_basico2);
                cmd.Parameters.AddWithValue("@PRODUC_PROF2", objPromocion.prod_prof2);
                cmd.Parameters.AddWithValue("@PRODUC_EMP2", objPromocion.prod_emp2);
                cmd.Parameters.AddWithValue("@PRODUC_MILL2", objPromocion.prod_mill2);
                cmd.Parameters.AddWithValue("@ESTADO_BASICO2", objPromocion.esta_basico2);
                cmd.Parameters.AddWithValue("@ESTADO_PROF2", objPromocion.esta_prof2);
                cmd.Parameters.AddWithValue("@ESTADO_EMP2", objPromocion.esta_emp2);
                cmd.Parameters.AddWithValue("@ESTADO_MILL2", objPromocion.esta_mill2);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionDAO", "RegistroPromocion", "USP_REGISTROPROMO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarPromocion(Promocion objPromocion)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEPROMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_PROMO", objPromocion.id_promo);
                cmd.Parameters.AddWithValue("@NOM_PROM", objPromocion.nom_promo);
                cmd.Parameters.AddWithValue("@RANGO", objPromocion.rango);
                cmd.Parameters.AddWithValue("@RANGOFIN", objPromocion.rangofin);
                cmd.Parameters.AddWithValue("@ESTADO_G", objPromocion.estado_gen);
                cmd.Parameters.AddWithValue("@PUN1", objPromocion.puntos1);
                cmd.Parameters.AddWithValue("@PRODU1", objPromocion.producto1);
                cmd.Parameters.AddWithValue("@ESTAD1", objPromocion.estado1);
                cmd.Parameters.AddWithValue("@PUN2", objPromocion.puntos2);
                cmd.Parameters.AddWithValue("@PRODU2", objPromocion.producto2);
                cmd.Parameters.AddWithValue("@ESTAD2", objPromocion.estado2);
                cmd.Parameters.AddWithValue("@PUN3", objPromocion.puntos3);
                cmd.Parameters.AddWithValue("@PRODU3", objPromocion.producto3);
                cmd.Parameters.AddWithValue("@ESTAD3", objPromocion.estado3);
                cmd.Parameters.AddWithValue("@PUN4", objPromocion.puntos4);
                cmd.Parameters.AddWithValue("@PRODU4", objPromocion.producto4);
                cmd.Parameters.AddWithValue("@ESTAD4", objPromocion.estado4);
                cmd.Parameters.AddWithValue("@PUN_BASICO", objPromocion.punt_basico);
                cmd.Parameters.AddWithValue("@PROD_BASICO", objPromocion.prod_basico);
                cmd.Parameters.AddWithValue("@ESTADO_BASICO", objPromocion.esta_basico);
                cmd.Parameters.AddWithValue("@PUN_PROF", objPromocion.punt_prof);
                cmd.Parameters.AddWithValue("@PROD_PROF", objPromocion.prod_prof);
                cmd.Parameters.AddWithValue("@ESTADO_PROF", objPromocion.esta_prof);
                cmd.Parameters.AddWithValue("@PUN_EMP", objPromocion.punt_emp);
                cmd.Parameters.AddWithValue("@PROD_EMP", objPromocion.prod_emp);
                cmd.Parameters.AddWithValue("@ESTADO_EMP", objPromocion.esta_emp);
                cmd.Parameters.AddWithValue("@PUN_MILL", objPromocion.punt_mill);
                cmd.Parameters.AddWithValue("@PROD_MILL", objPromocion.prod_mill);
                cmd.Parameters.AddWithValue("@ESTADO_MILL", objPromocion.esta_mill);
                cmd.Parameters.AddWithValue("@PUN_CON", objPromocion.punt_con);
                cmd.Parameters.AddWithValue("@PROD_CON", objPromocion.prod_con);
                cmd.Parameters.AddWithValue("@ESTADO_CON", objPromocion.esta_con);
                cmd.Parameters.AddWithValue("@PUN_CI", objPromocion.punt_ci);
                cmd.Parameters.AddWithValue("@PROD_CI", objPromocion.prod_ci);
                cmd.Parameters.AddWithValue("@ESTADO_CI", objPromocion.esta_ci);
                cmd.Parameters.AddWithValue("@REGALO1", objPromocion.regalo1);
                cmd.Parameters.AddWithValue("@REGALO2", objPromocion.regalo2);
                cmd.Parameters.AddWithValue("@REGALO3", objPromocion.regalo3);
                cmd.Parameters.AddWithValue("@REGALO4", objPromocion.regalo4);
                cmd.Parameters.AddWithValue("@PXC1", objPromocion.pxc1);
                cmd.Parameters.AddWithValue("@PXC2", objPromocion.pxc2);
                cmd.Parameters.AddWithValue("@PXC3", objPromocion.pxc3);
                cmd.Parameters.AddWithValue("@PXC4", objPromocion.pxc4);
                cmd.Parameters.AddWithValue("@REGBASICO1", objPromocion.regBasico1);
                cmd.Parameters.AddWithValue("@REGPROFE1", objPromocion.regProfe1);
                cmd.Parameters.AddWithValue("@REGEMPRE1", objPromocion.regEmpre1);
                cmd.Parameters.AddWithValue("@REGMILLO1", objPromocion.regMillo1);
                cmd.Parameters.AddWithValue("@REGBASICO2", objPromocion.regBasico2);
                cmd.Parameters.AddWithValue("@REGPROFE2", objPromocion.regProfe2);
                cmd.Parameters.AddWithValue("@REGEMPRE2", objPromocion.regEmpre2);
                cmd.Parameters.AddWithValue("@REGMILLO2", objPromocion.regMillo2);
                cmd.Parameters.AddWithValue("@PUNTO_BASICO2", objPromocion.punt_basico2);
                cmd.Parameters.AddWithValue("@PUNTO_PROF2", objPromocion.punt_prof2);
                cmd.Parameters.AddWithValue("@PUNTO_EMP2", objPromocion.punt_emp2);
                cmd.Parameters.AddWithValue("@PUNTO_MILL2", objPromocion.punt_mill2);
                cmd.Parameters.AddWithValue("@PRODUC_BASICO2", objPromocion.prod_basico2);
                cmd.Parameters.AddWithValue("@PRODUC_PROF2", objPromocion.prod_prof2);
                cmd.Parameters.AddWithValue("@PRODUC_EMP2", objPromocion.prod_emp2);
                cmd.Parameters.AddWithValue("@PRODUC_MILL2", objPromocion.prod_mill2);
                cmd.Parameters.AddWithValue("@ESTADO_BASICO2", objPromocion.esta_basico2);
                cmd.Parameters.AddWithValue("@ESTADO_PROF2", objPromocion.esta_prof2);
                cmd.Parameters.AddWithValue("@ESTADO_EMP2", objPromocion.esta_emp2);
                cmd.Parameters.AddWithValue("@ESTADO_MILL2", objPromocion.esta_mill2);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionDAO", "ActualizarPromocion", "USP_UPDATEPROMO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Promocion> ListarPromo()
        {
            List<Promocion> Lista = new List<Promocion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPROMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Promocion objPromo = new Promocion();
                    objPromo.id_promo = dr["ID_PROMO"].ToString();
                    objPromo.nom_promo = dr["NOMBRE_PROMO"].ToString();
                    objPromo.rango = dr["RANGO"].ToString();
                    objPromo.rangofin = dr["RANGOFIN"].ToString();
                    objPromo.estado_gen = Convert.ToBoolean(dr["ESTADO_GENERAL"].ToString());
                    objPromo.puntos1 = Convert.ToInt32(dr["PUNTOS1"].ToString());
                    objPromo.producto1 = dr["PRODUCTO1"].ToString();
                    objPromo.estado1 = Convert.ToBoolean(dr["ESTADO1"].ToString());
                    objPromo.puntos2 = Convert.ToInt32(dr["PUNTOS2"].ToString());
                    objPromo.producto2 = dr["PRODUCTO2"].ToString();
                    objPromo.estado2 = Convert.ToBoolean(dr["ESTADO2"].ToString());
                    objPromo.puntos3 = Convert.ToInt32(dr["PUNTOS3"].ToString());
                    objPromo.producto3 = dr["PRODUCTO3"].ToString();
                    objPromo.estado3 = Convert.ToBoolean(dr["ESTADO3"].ToString());
                    objPromo.puntos4 = Convert.ToInt32(dr["PUNTOS4"].ToString());
                    objPromo.producto4 = dr["PRODUCTO4"].ToString();
                    objPromo.estado4 = Convert.ToBoolean(dr["ESTADO4"].ToString());
                    objPromo.punt_basico = Convert.ToInt32(dr["PUNTO_BASICO"].ToString());
                    objPromo.prod_basico = dr["PRODUC_BASICO"].ToString();
                    objPromo.esta_basico = Convert.ToBoolean(dr["ESTADO_BASICO"].ToString());
                    objPromo.punt_prof = Convert.ToInt32(dr["PUNTO_PROF"].ToString());
                    objPromo.prod_prof = dr["PRODUC_PROF"].ToString();
                    objPromo.esta_prof = Convert.ToBoolean(dr["ESTADO_PROF"].ToString());
                    objPromo.punt_emp = Convert.ToInt32(dr["PUNTO_EMP"].ToString());
                    objPromo.prod_emp = dr["PRODUC_EMP"].ToString();
                    objPromo.esta_emp = Convert.ToBoolean(dr["ESTADO_EMP"].ToString());
                    objPromo.punt_mill = Convert.ToInt32(dr["PUNTO_MILL"].ToString());
                    objPromo.prod_mill = dr["PRODUC_MILL"].ToString();
                    objPromo.esta_mill = Convert.ToBoolean(dr["ESTADO_MILL"].ToString());
                    objPromo.punt_con = Convert.ToInt32(dr["PUNTO_CON"].ToString());
                    objPromo.prod_con = dr["PRODUC_CON"].ToString();
                    objPromo.esta_con = Convert.ToBoolean(dr["ESTADO_CON"].ToString());
                    objPromo.punt_ci = Convert.ToInt32(dr["PUNTO_CI"].ToString());
                    objPromo.prod_ci = dr["PRODUC_CI"].ToString();
                    objPromo.esta_ci = Convert.ToBoolean(dr["ESTADO_CI"].ToString());
                    objPromo.pxc1 = Convert.ToBoolean(dr["PXC1"].ToString());
                    objPromo.pxc2 = Convert.ToBoolean(dr["PXC2"].ToString());
                    objPromo.pxc3 = Convert.ToBoolean(dr["PXC3"].ToString());
                    objPromo.pxc4 = Convert.ToBoolean(dr["PXC4"].ToString());
                    objPromo.regalo1 = Convert.ToBoolean(dr["REGALO1"].ToString());
                    objPromo.regalo2 = Convert.ToBoolean(dr["REGALO2"].ToString());
                    objPromo.regalo3 = Convert.ToBoolean(dr["REGALO3"].ToString());
                    objPromo.regalo4 = Convert.ToBoolean(dr["REGALO4"].ToString());
                    objPromo.regBasico1 = Convert.ToBoolean(dr["REGBASICO1"].ToString());
                    objPromo.regProfe1 = Convert.ToBoolean(dr["REGPROFE1"].ToString());
                    objPromo.regEmpre1 = Convert.ToBoolean(dr["REGEMPRE1"].ToString());
                    objPromo.regMillo1 = Convert.ToBoolean(dr["REGMILLO1"].ToString());
                    objPromo.regBasico2 = Convert.ToBoolean(dr["REGBASICO2"].ToString());
                    objPromo.regProfe2 = Convert.ToBoolean(dr["REGPROFE2"].ToString());
                    objPromo.regEmpre2 = Convert.ToBoolean(dr["REGEMPRE2"].ToString());
                    objPromo.regMillo2 = Convert.ToBoolean(dr["REGMILLO2"].ToString());
                    objPromo.punt_basico2 = Convert.ToInt32(dr["PUNTO_BASICO2"].ToString());
                    objPromo.punt_prof2 = Convert.ToInt32(dr["PUNTO_PROF2"].ToString());
                    objPromo.punt_emp2 = Convert.ToInt32(dr["PUNTO_EMP2"].ToString());
                    objPromo.punt_mill2 = Convert.ToInt32(dr["PUNTO_MILL2"].ToString());
                    objPromo.prod_basico2 = dr["PRODUC_BASICO2"].ToString();
                    objPromo.prod_prof2 = dr["PRODUC_PROF2"].ToString();
                    objPromo.prod_emp2 = dr["PRODUC_EMP2"].ToString();
                    objPromo.prod_mill2 = dr["PRODUC_MILL2"].ToString();
                    objPromo.esta_basico2 = Convert.ToBoolean(dr["ESTADO_BASICO2"].ToString());
                    objPromo.esta_prof2 = Convert.ToBoolean(dr["ESTADO_PROF2"].ToString());
                    objPromo.esta_emp2 = Convert.ToBoolean(dr["ESTADO_EMP2"].ToString());
                    objPromo.esta_mill2 = Convert.ToBoolean(dr["ESTADO_MILL2"].ToString());
                    Lista.Add(objPromo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionDAO", "ListarPromo", "USP_LISTAPROMO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Promocion> ListaValidarPromo()
        {
            List<Promocion> Lista = new List<Promocion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTAVALIDARPROMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Promocion objPromo = new Promocion();
                    objPromo.id_promo = dr["ID_PROMO"].ToString();
                    objPromo.estado_gen = Convert.ToBoolean(dr["ESTADO_GENERAL"].ToString());
                    objPromo.rango = dr["RANGO"].ToString();
                    objPromo.rangofin = dr["RANGOFIN"].ToString();
                    objPromo.puntos1 = Convert.ToInt32(dr["PUNTOS1"].ToString());
                    objPromo.producto1 = dr["PRODUCTO1"].ToString();
                    objPromo.estado1 = Convert.ToBoolean(dr["ESTADO1"].ToString());
                    objPromo.puntos2 = Convert.ToInt32(dr["PUNTOS2"].ToString());
                    objPromo.producto2 = dr["PRODUCTO2"].ToString();
                    objPromo.estado2 = Convert.ToBoolean(dr["ESTADO2"].ToString());
                    objPromo.puntos3 = Convert.ToInt32(dr["PUNTOS3"].ToString());
                    objPromo.producto3 = dr["PRODUCTO3"].ToString();
                    objPromo.estado3 = Convert.ToBoolean(dr["ESTADO3"].ToString());
                    objPromo.puntos4 = Convert.ToInt32(dr["PUNTOS4"].ToString());
                    objPromo.producto4 = dr["PRODUCTO4"].ToString();
                    objPromo.estado4 = Convert.ToBoolean(dr["ESTADO4"].ToString());
                    objPromo.punt_basico = Convert.ToInt32(dr["PUNTO_BASICO"].ToString());
                    objPromo.prod_basico = dr["PRODUC_BASICO"].ToString();
                    objPromo.esta_basico = Convert.ToBoolean(dr["ESTADO_BASICO"].ToString());
                    objPromo.punt_prof = Convert.ToInt32(dr["PUNTO_PROF"].ToString());
                    objPromo.prod_prof = dr["PRODUC_PROF"].ToString();
                    objPromo.esta_prof = Convert.ToBoolean(dr["ESTADO_PROF"].ToString());
                    objPromo.punt_emp = Convert.ToInt32(dr["PUNTO_EMP"].ToString());
                    objPromo.prod_emp = dr["PRODUC_EMP"].ToString();
                    objPromo.esta_emp = Convert.ToBoolean(dr["ESTADO_EMP"].ToString());
                    objPromo.punt_mill = Convert.ToInt32(dr["PUNTO_MILL"].ToString());
                    objPromo.prod_mill = dr["PRODUC_MILL"].ToString();
                    objPromo.esta_mill = Convert.ToBoolean(dr["ESTADO_MILL"].ToString());
                    objPromo.punt_con = Convert.ToInt32(dr["PUNTO_CON"].ToString());
                    objPromo.prod_con = dr["PRODUC_CON"].ToString();
                    objPromo.esta_con = Convert.ToBoolean(dr["ESTADO_CON"].ToString());
                    objPromo.punt_ci = Convert.ToInt32(dr["PUNTO_CI"].ToString());
                    objPromo.prod_ci = dr["PRODUC_CI"].ToString();
                    objPromo.esta_ci = Convert.ToBoolean(dr["ESTADO_CI"].ToString());
                    objPromo.pxc1 = Convert.ToBoolean(dr["PXC1"].ToString());
                    objPromo.pxc2 = Convert.ToBoolean(dr["PXC2"].ToString());
                    objPromo.pxc3 = Convert.ToBoolean(dr["PXC3"].ToString());
                    objPromo.pxc4 = Convert.ToBoolean(dr["PXC4"].ToString());
                    objPromo.regalo1 = Convert.ToBoolean(dr["REGALO1"].ToString());
                    objPromo.regalo2 = Convert.ToBoolean(dr["REGALO2"].ToString());
                    objPromo.regalo3 = Convert.ToBoolean(dr["REGALO3"].ToString());
                    objPromo.regalo4 = Convert.ToBoolean(dr["REGALO4"].ToString());
                    objPromo.regBasico1 = Convert.ToBoolean(dr["REGBASICO1"].ToString());
                    objPromo.regProfe1 = Convert.ToBoolean(dr["REGPROFE1"].ToString());
                    objPromo.regEmpre1 = Convert.ToBoolean(dr["REGEMPRE1"].ToString());
                    objPromo.regMillo1 = Convert.ToBoolean(dr["REGMILLO1"].ToString());
                    objPromo.regBasico2 = Convert.ToBoolean(dr["REGBASICO2"].ToString());
                    objPromo.regProfe2 = Convert.ToBoolean(dr["REGPROFE2"].ToString());
                    objPromo.regEmpre2 = Convert.ToBoolean(dr["REGEMPRE2"].ToString());
                    objPromo.regMillo2 = Convert.ToBoolean(dr["REGMILLO2"].ToString());
                    objPromo.punt_basico2 = Convert.ToInt32(dr["PUNTO_BASICO2"].ToString());
                    objPromo.punt_prof2 = Convert.ToInt32(dr["PUNTO_PROF2"].ToString());
                    objPromo.punt_emp2 = Convert.ToInt32(dr["PUNTO_EMP2"].ToString());
                    objPromo.punt_mill2 = Convert.ToInt32(dr["PUNTO_MILL2"].ToString());
                    objPromo.prod_basico2 = dr["PRODUC_BASICO2"].ToString();
                    objPromo.prod_prof2 = dr["PRODUC_PROF2"].ToString();
                    objPromo.prod_emp2 = dr["PRODUC_EMP2"].ToString();
                    objPromo.prod_mill2 = dr["PRODUC_MILL2"].ToString();
                    objPromo.esta_basico2 = Convert.ToBoolean(dr["ESTADO_BASICO2"].ToString());
                    objPromo.esta_prof2 = Convert.ToBoolean(dr["ESTADO_PROF2"].ToString());
                    objPromo.esta_emp2 = Convert.ToBoolean(dr["ESTADO_EMP2"].ToString());
                    objPromo.esta_mill2 = Convert.ToBoolean(dr["ESTADO_MILL2"].ToString());
                    Lista.Add(objPromo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionDAO", "ListaValidarPromo", "LISTAVALIDARPROMO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public int ListaCantPromo()
        {
            List<Promocion> Lista = new List<Promocion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTACANTPROMO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Promocion objPromo = new Promocion();
                    objPromo.cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    cantidad = objPromo.cantidad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionDAO", "ListaCantPromo", "LISTACANTPROMO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public string ListaCodigoPromoActiva()
        {
            List<Promocion> Lista = new List<Promocion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string codigo = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTAPROMOACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Promocion objPromo = new Promocion();
                    objPromo.id_promo = dr["ID_PROMO"].ToString();
                    codigo = objPromo.id_promo;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionDAO", "ListaCodigoPromoActiva", "LISTAPROMOACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return codigo;
        }
    }
}
