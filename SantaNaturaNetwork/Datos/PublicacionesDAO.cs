﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class PublicacionesDAO
    {
        #region "PATRON SINGLETON"
        private static PublicacionesDAO daoPublicaciones = null;
        private PublicacionesDAO() { }
        public static PublicacionesDAO getInstance()
        {
            if (daoPublicaciones == null)
            {
                daoPublicaciones = new PublicacionesDAO();
            }
            return daoPublicaciones;
        }
        #endregion

        public bool RegistroPublicaciones(Publicacion objPublicacion)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROPUBLICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TITULO", objPublicacion.titulo);
                cmd.Parameters.AddWithValue("@MENSAJE", objPublicacion.mensaje);
                cmd.Parameters.AddWithValue("@ENLACE", objPublicacion.enlace);
                cmd.Parameters.AddWithValue("@IMAGEN", objPublicacion.imagen);
                cmd.Parameters.AddWithValue("@ESTADO", objPublicacion.estado);
                cmd.Parameters.AddWithValue("@FECHA", objPublicacion.fecha);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch(Exception ex)
            {
                Conexion.getInstance().RegistrarError("PublicacionesDAO", "RegistroPublicaciones", "USP_REGISTROPUBLICACION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Publicacion> ListaPublicacion()
        {
            List<Publicacion> Lista = new List<Publicacion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPUBLICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Publicacion objPublicacion = new Publicacion();
                    objPublicacion.idPublicacion = dr["ID_PUB"].ToString();
                    objPublicacion.titulo = dr["TITULO"].ToString();
                    objPublicacion.mensaje = dr["MENSAJE"].ToString();
                    objPublicacion.enlace = dr["ENLACE"].ToString();
                    objPublicacion.imagen = dr["IMAGEN"].ToString();
                    objPublicacion.estado = Convert.ToBoolean(dr["ESTADO"].ToString());
                    objPublicacion.fecha = dr["FECHA"].ToString();
                    Lista.Add(objPublicacion);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PublicacionesDAO", "ListaPublicacion", "USP_LISTAPUBLICACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarPublicacion(Publicacion objPublicacion)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEPUBLICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_PUB", objPublicacion.idPublicacion);
                cmd.Parameters.AddWithValue("@TITULO", objPublicacion.titulo);
                cmd.Parameters.AddWithValue("@MENSAJE", objPublicacion.mensaje);
                cmd.Parameters.AddWithValue("@ENLACE", objPublicacion.enlace);
                cmd.Parameters.AddWithValue("@IMAGEN", objPublicacion.imagen);
                cmd.Parameters.AddWithValue("@ESTADO", objPublicacion.estado);
                cmd.Parameters.AddWithValue("@FECHA", objPublicacion.fecha);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch(Exception ex)
            {
                Conexion.getInstance().RegistrarError("PublicacionesDAO", "ActualizarPublicacion", "USP_UPDATEPUBLICACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool EliminarPublicacion(string id)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DELETEPUBLICACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_PUB", id);

                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PublicacionesDAO", "EliminarPublicacion", "USP_DELETEPUBLICACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }
    }
}
