﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class CompraDatos
    {
        #region "PATRON SINGLETON"
        private static CompraDatos _compraDatos = null;
        private CompraDatos() { }
        public static CompraDatos getInstance()
        {
            if (_compraDatos == null)
            {
                _compraDatos = new CompraDatos();
            }
            return _compraDatos;
        }
        #endregion

        string errores = "";

        public bool RegistrarDepositoAndActualizarCompra(string ticket, string numOperacion, string banco, DateTime fechaVaucher,
                                                        double monto, string vaucherFoto, string idopPeruShop, string despacho2)
        {
            bool ok = false;
            SqlConnection cn = null;
            SqlCommand cmd = null;
            int resultado = 0;

            try
            {
                cn = Conexion.getInstance().Conectar();
                cn.Open();

                cmd = new SqlCommand("USP_RegistrarDepositoAndActualizarCompra", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);
                cmd.Parameters.AddWithValue("@numOperacion", numOperacion);
                cmd.Parameters.AddWithValue("@banco", banco);
                cmd.Parameters.AddWithValue("@fechaVaucher", fechaVaucher);
                cmd.Parameters.AddWithValue("@monto", monto);
                cmd.Parameters.AddWithValue("@fotoVaucher", vaucherFoto);
                cmd.Parameters.AddWithValue("@idopPeruShop", idopPeruShop);
                cmd.Parameters.AddWithValue("@despacho2", despacho2);

                resultado = cmd.ExecuteNonQuery();

                if (resultado > 0)
                {
                    ok = true;
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistrarDepositoAndActualizarCompra", "USP_RegistrarDepositoAndActualizarCompra", ex.Message);
                throw ex;
            }

            return ok;
        }
        public bool RegistrarVoucherTemporal(string usuario, string ruta)//Charles 25/03/2021
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("RegistrarVoucherTemporal", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@Ruta", ruta);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistrarVoucherTemporal", "RegistrarVoucherTemporal", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }
        public bool EliminarVoucherTemporal(string usuario)//Charles 25/03/2021
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("EliminarVoucherTemporal", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "EliminarVoucherTemporal", "EliminarVoucherTemporal", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }
        public bool ActualizarCompraMove(string ticket, string idMove, string idLiqui, string estadMove)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATECOMPRAMOVE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);
                cmd.Parameters.AddWithValue("@idMove", idMove);
                cmd.Parameters.AddWithValue("@idLiquid", idLiqui);
                cmd.Parameters.AddWithValue("@estadoIdmove", estadMove);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarCompraMove", "USP_UPDATECOMPRAMOVE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarIdopYEstadoCompra(string ticket, string idDop)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATECOMPRADEPOSITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);
                cmd.Parameters.AddWithValue("@idDop", idDop);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarIdopYEstadoCompra", "USP_UPDATECOMPRADEPOSITO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarDepositoDuplicado(string ticket)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEDEPOSITODUPLICADO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarDepositoDuplicado", "USP_UPDATEDEPOSITODUPLICADO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarDepositoValidado(string ticket, int estado)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEDEPOSITOVALIDADO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);
                cmd.Parameters.AddWithValue("@estado", estado);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarDepositoValidado", "USP_UPDATEDEPOSITOVALIDADO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public string RegistrarCompraAndDetalle(Compra compra, List<ProductoCarrito> listaCarrito)
        {
            SqlConnection conex = null;
            SqlCommand cmdCompra = null;
            SqlCommand cmdDetalle = null;

            string ticket = "";
            ticket = GenerarTicket();
            conex = Conexion.getInstance().Conectar();
            conex.Open();

            SqlTransaction trans = conex.BeginTransaction();

            cmdCompra = new SqlCommand("SP_RegistrarCompra", conex);
            cmdCompra.CommandType = CommandType.StoredProcedure;

            cmdCompra.Parameters.AddWithValue("@idopPeruShop", compra.IdopPeruShop);
            cmdCompra.Parameters.AddWithValue("@codCliente", compra.CodCliente.Trim());
            cmdCompra.Parameters.AddWithValue("@cantidad", compra.Cantidad);
            cmdCompra.Parameters.AddWithValue("@montoTotal", compra.MontoTotal);
            cmdCompra.Parameters.AddWithValue("@puntosTotal", compra.PuntosTotal);
            cmdCompra.Parameters.AddWithValue("@puntosTotalPromo", compra.PuntosTotalPromo);
            cmdCompra.Parameters.AddWithValue("@montoAPagar", compra.MontoAPagar);
            cmdCompra.Parameters.AddWithValue("@fechaPago", compra.FechaPago);
            cmdCompra.Parameters.AddWithValue("@fechaStock", compra.FechaStock);
            cmdCompra.Parameters.AddWithValue("@estado", compra.Estado);
            cmdCompra.Parameters.AddWithValue("@tipoPago", compra.TipoPago);
            cmdCompra.Parameters.AddWithValue("@idTipoCompra", compra.idTipoCompra);
            cmdCompra.Parameters.AddWithValue("@tipoCompra", compra.TipoCompra);
            cmdCompra.Parameters.AddWithValue("@montoComi", compra.montoComision);
            cmdCompra.Parameters.AddWithValue("@cantPromo", compra.cantPromo);
            cmdCompra.Parameters.AddWithValue("@idpromo", compra.IdPromo);
            cmdCompra.Parameters.AddWithValue("@cantRegalo", compra.cantRegalo);
            cmdCompra.Parameters.AddWithValue("@puntosGastados", compra.puntosGastados);
            cmdCompra.Parameters.AddWithValue("@montoComiCI", compra.montoComisionCI);
            cmdCompra.Parameters.AddWithValue("@IdPago", compra.IdPago);
            cmdCompra.Parameters.AddWithValue("@Transaction_Date", compra.Transaction_Date);
            cmdCompra.Parameters.AddWithValue("@Merchant", compra.Merchant);
            cmdCompra.Parameters.AddWithValue("@Id_Unico", compra.Id_Unico);
            cmdCompra.Parameters.AddWithValue("@Transaction_ID", compra.Transaction_ID);
            cmdCompra.Parameters.AddWithValue("@CardS", compra.CardS);
            cmdCompra.Parameters.AddWithValue("@Aauthorization_Code", compra.Aauthorization_Code);
            cmdCompra.Parameters.AddWithValue("@AMOUNT", compra.AMOUNT);
            cmdCompra.Parameters.AddWithValue("@CURRENCY", compra.CURRENCY);
            cmdCompra.Parameters.AddWithValue("@BRAND", compra.BRAND);
            cmdCompra.Parameters.AddWithValue("@STATUSS", compra.STATUSS);
            cmdCompra.Parameters.AddWithValue("@ACTION_DESCRIPTION", compra.ACTION_DESCRIPTION);
            cmdCompra.Parameters.AddWithValue("@NotaDelivery", compra.NotaDelivery);
            cmdCompra.Parameters.AddWithValue("@COMPROBANTE", compra.Comprobante);
            cmdCompra.Parameters.AddWithValue("@RUC", compra.Ruc);
            cmdCompra.Parameters.AddWithValue("@PAQUETE", compra.PaqueteSocio.Trim());
            cmdCompra.Parameters.AddWithValue("@CORAZONES", compra.Corazones);
            cmdCompra.Parameters.AddWithValue("@IDP", compra.IDP);
            cmdCompra.Parameters.AddWithValue("@PAIS", "01");
            if (compra.DepositoTotal == 0)
            {
                cmdCompra.Parameters.AddWithValue("@depositoTotal", 00.00);
            }
            else
            {
                cmdCompra.Parameters.AddWithValue("@depositoTotal", compra.DepositoTotal);
            }
            cmdCompra.Parameters.AddWithValue("@depostioFraccionado", compra.DepositoFraccionado);
            cmdCompra.Parameters.AddWithValue("@fechaDeposito", compra.FechaDeposito);
            if (compra.Despacho == "")
            {
                cmdCompra.Parameters.AddWithValue("@despacho", DBNull.Value);
            }
            else
            {
                cmdCompra.Parameters.AddWithValue("@despacho", compra.Despacho);
            }

            try
            {
                cmdCompra.Transaction = trans;

                cmdCompra.ExecuteNonQuery();

                foreach (var lista in listaCarrito)
                {
                    cmdDetalle = new SqlCommand("SP_RegistrarDetalleCompra", conex);
                    cmdDetalle.CommandType = CommandType.StoredProcedure;
                    cmdDetalle.Parameters.AddWithValue("@ticket", ticket);
                    cmdDetalle.Parameters.AddWithValue("@codProducto", lista.Codigo);
                    cmdDetalle.Parameters.AddWithValue("@cantidad", lista.Cantidad);
                    cmdDetalle.Parameters.AddWithValue("@subTotal", lista.SubTotal);
                    cmdDetalle.Parameters.AddWithValue("@precioUnitario", lista.PrecioUnitario);
                    cmdDetalle.Parameters.AddWithValue("@cantiPS", lista.cantidadPeruShop);
                    cmdDetalle.Parameters.AddWithValue("@preciPS", lista.montoSend);
                    cmdDetalle.Parameters.AddWithValue("@precioStr", lista.precioStr);
                    cmdDetalle.Parameters.AddWithValue("@puntos", lista.PuntosPromocion);
                    cmdDetalle.Parameters.AddWithValue("@corazones", lista.Corazones);

                    cmdDetalle.Transaction = trans;

                    cmdDetalle.CommandTimeout = 90000;
                    cmdDetalle.ExecuteNonQuery();
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistrarCompraAndDetalle", "SP_RegistrarCompra", ex.Message);
                errores = ex.Message;
                trans.Rollback();
            }
            finally
            {
                if (conex.State == ConnectionState.Open)
                {
                    conex.Close();
                }
                conex.Dispose();
                //cmd.Dispose();
                //cmdDetalle.Dispose();
            }
            return ticket;
        }

        public string RegistrarCompraAndDetalleVisa(Compra compra, List<ProductoCarrito> listaCarrito)
        {
            SqlConnection conex = null;
            SqlCommand cmdCompra = null;
            SqlCommand cmdDetalle = null;

            string ticket = "";
            ticket = GenerarTicket();
            conex = Conexion.getInstance().Conectar();
            conex.Open();

            SqlTransaction trans = conex.BeginTransaction();

            cmdCompra = new SqlCommand("SP_RegistrarCompra", conex);
            cmdCompra.CommandType = CommandType.StoredProcedure;

            cmdCompra.Parameters.AddWithValue("@idopPeruShop", compra.IdopPeruShop);
            cmdCompra.Parameters.AddWithValue("@codCliente", compra.CodCliente.Trim());
            cmdCompra.Parameters.AddWithValue("@cantidad", compra.Cantidad);
            cmdCompra.Parameters.AddWithValue("@montoTotal", compra.MontoTotal);
            cmdCompra.Parameters.AddWithValue("@puntosTotal", compra.PuntosTotal);
            cmdCompra.Parameters.AddWithValue("@puntosTotalPromo", compra.PuntosTotalPromo);
            cmdCompra.Parameters.AddWithValue("@montoAPagar", compra.MontoAPagar);
            cmdCompra.Parameters.AddWithValue("@fechaPago", compra.FechaPago);
            cmdCompra.Parameters.AddWithValue("@fechaStock", compra.FechaStock);
            cmdCompra.Parameters.AddWithValue("@estado", compra.Estado);
            cmdCompra.Parameters.AddWithValue("@tipoPago", compra.TipoPago);
            cmdCompra.Parameters.AddWithValue("@idTipoCompra", compra.idTipoCompra);
            cmdCompra.Parameters.AddWithValue("@tipoCompra", compra.TipoCompra);
            cmdCompra.Parameters.AddWithValue("@montoComi", compra.montoComision);
            cmdCompra.Parameters.AddWithValue("@cantPromo", compra.cantPromo);
            cmdCompra.Parameters.AddWithValue("@idpromo", compra.IdPromo);
            cmdCompra.Parameters.AddWithValue("@cantRegalo", compra.cantRegalo);
            cmdCompra.Parameters.AddWithValue("@puntosGastados", compra.puntosGastados);
            cmdCompra.Parameters.AddWithValue("@montoComiCI", compra.montoComisionCI);
            cmdCompra.Parameters.AddWithValue("@IdPago", compra.IdPago);
            cmdCompra.Parameters.AddWithValue("@Transaction_Date", compra.Transaction_Date);
            cmdCompra.Parameters.AddWithValue("@Merchant", compra.Merchant);
            cmdCompra.Parameters.AddWithValue("@Id_Unico", compra.Id_Unico);
            cmdCompra.Parameters.AddWithValue("@Transaction_ID", compra.Transaction_ID);
            cmdCompra.Parameters.AddWithValue("@CardS", compra.CardS);
            cmdCompra.Parameters.AddWithValue("@Aauthorization_Code", compra.Aauthorization_Code);
            cmdCompra.Parameters.AddWithValue("@AMOUNT", compra.AMOUNT);
            cmdCompra.Parameters.AddWithValue("@CURRENCY", compra.CURRENCY);
            cmdCompra.Parameters.AddWithValue("@BRAND", compra.BRAND);
            cmdCompra.Parameters.AddWithValue("@STATUSS", compra.STATUSS);
            cmdCompra.Parameters.AddWithValue("@ACTION_DESCRIPTION", compra.ACTION_DESCRIPTION);
            cmdCompra.Parameters.AddWithValue("@NotaDelivery", compra.NotaDelivery);
            cmdCompra.Parameters.AddWithValue("@COMPROBANTE", compra.Comprobante);
            cmdCompra.Parameters.AddWithValue("@RUC", compra.Ruc);
            cmdCompra.Parameters.AddWithValue("@PAQUETE", compra.PaqueteSocio.Trim());
            cmdCompra.Parameters.AddWithValue("@CORAZONES", compra.Corazones);
            if (compra.DepositoTotal == 0)
            {
                cmdCompra.Parameters.AddWithValue("@depositoTotal", 00.00);
            }
            else
            {
                cmdCompra.Parameters.AddWithValue("@depositoTotal", compra.DepositoTotal);
            }
            cmdCompra.Parameters.AddWithValue("@depostioFraccionado", compra.DepositoFraccionado);
            cmdCompra.Parameters.AddWithValue("@fechaDeposito", compra.FechaDeposito);
            if (compra.Despacho == "")
            {
                cmdCompra.Parameters.AddWithValue("@despacho", DBNull.Value);
            }
            else
            {
                cmdCompra.Parameters.AddWithValue("@despacho", compra.Despacho);
            }

            try
            {
                cmdCompra.Transaction = trans;

                cmdCompra.ExecuteNonQuery();

                foreach (var lista in listaCarrito)
                {
                    cmdDetalle = new SqlCommand("SP_RegistrarDetalleCompra", conex);
                    cmdDetalle.CommandType = CommandType.StoredProcedure;
                    cmdDetalle.Parameters.AddWithValue("@ticket", ticket);
                    cmdDetalle.Parameters.AddWithValue("@codProducto", lista.Codigo);
                    cmdDetalle.Parameters.AddWithValue("@cantidad", lista.Cantidad);
                    cmdDetalle.Parameters.AddWithValue("@subTotal", lista.SubTotal);
                    cmdDetalle.Parameters.AddWithValue("@precioUnitario", lista.PrecioUnitario);
                    cmdDetalle.Parameters.AddWithValue("@cantiPS", lista.cantidadPeruShop);
                    cmdDetalle.Parameters.AddWithValue("@preciPS", lista.montoSend);
                    cmdDetalle.Parameters.AddWithValue("@precioStr", lista.precioStr);
                    cmdDetalle.Parameters.AddWithValue("@puntos", lista.PuntosPromocion);

                    cmdDetalle.Transaction = trans;

                    cmdDetalle.CommandTimeout = 90000;
                    cmdDetalle.ExecuteNonQuery();
                }

                trans.Commit();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistrarCompraAndDetalleVisa", "SP_RegistrarCompra", ex.Message);
                errores = ex.Message;
                trans.Rollback();
            }
            finally
            {
                if (conex.State == ConnectionState.Open)
                {
                    conex.Close();
                }
                conex.Dispose();
                //cmd.Dispose();
                //cmdDetalle.Dispose();
            }

            return ticket;

        }

        public string GenerarTicket()
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            conexion = Conexion.getInstance().Conectar();
            conexion.Open();

            cmd = new SqlCommand("SP_GenerarTicketCompra ", conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CodGenerado", SqlDbType.VarChar, 9).Direction = ParameterDirection.Output;

            string ticket = "";

            try
            {
                cmd.ExecuteNonQuery();
                ticket = Convert.ToString(cmd.Parameters["@CodGenerado"].Value);
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "GenerarTicket", "SP_GenerarTicketCompra", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return ticket;
        }

        public string GenerarCodDetalleCompra()
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            conexion = Conexion.getInstance().Conectar();
            conexion.Open();

            cmd = new SqlCommand("SP_GenerarCodigoDetalleCompra ", conexion);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CodGenerado", SqlDbType.VarChar, 8).Direction = ParameterDirection.Output;


            string codDetalleCompra = "";

            try
            {
                cmd.CommandTimeout = 90000;
                cmd.ExecuteNonQuery();

                codDetalleCompra = Convert.ToString(cmd.Parameters["@CodGenerado"].Value);
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "GenerarCodDetalleCompra", "SP_GenerarCodigoDetalleCompra", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return codDetalleCompra;
        }

        public List<Compra> ListarComprasByCliente(string numDocCliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarComprasByCliente", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", numDocCliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.FechaPago = Convert.ToDateTime(dr["FechaPago"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarComprasByCliente", "SP_ListarComprasByCliente", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasPendientesDelClienteDeposito(string numDocCliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasPendientes", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", numDocCliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.FotoVaucher = Convert.ToString(dr["FotoVaucher"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    compra.NombreCliente = Convert.ToString(dr["NOMBRECOMPLETO"]);
                    compra.idTipoCompra = Convert.ToString(dr["idTipoCompra"]);
                    compra.TipoCompra = Convert.ToString(dr["TipoCompra"]);
                    compra.NotaDelivery = Convert.ToString(dr["NotaDelivery"]);
                    compra.Ruc = Convert.ToString(dr["RUC"]);
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasPendientesDelClienteDeposito", "USP_ListarComprasPendientes", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasRealizadasDelCliente(string numDocCliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasRealizadas", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", numDocCliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.FotoVaucher = Convert.ToString(dr["FotoVaucher"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasRealizadasDelCliente", "USP_ListarComprasRealizadas", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasRealizadasVisaNet(string idcliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasRealizadasVisaNet", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", idcliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.FotoVaucher = Convert.ToString(dr["FotoVaucher"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasRealizadasVisaNet", "USP_ListarComprasRealizadasVisaNet", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasGeneralMisCompras()
        {
            List<Compra> Lista = new List<Compra>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACOMPRASGENERALMISCOMPRAS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra objCompra = new Compra();
                    objCompra.Ticket = dr["TICKET"].ToString();
                    objCompra.FechaPagoReporte = dr["FECHA"].ToString();
                    objCompra.IdopPeruShop = dr["IDOP"].ToString();
                    objCompra.Cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    objCompra.MontoAPagar = Convert.ToDouble(dr["MONTO"].ToString());
                    objCompra.PuntosTotal = Convert.ToDouble(dr["PUNTOS"].ToString());
                    objCompra.Estado = Convert.ToInt32(dr["ESTADO"].ToString());
                    objCompra.NombreCliente = dr["NOMBRES"].ToString();
                    objCompra.ApellidoPat = dr["APELLIDOPAT"].ToString();
                    objCompra.ApellidoMat = dr["APELLIDOMAT"].ToString();
                    objCompra.FotoVaucher = dr["FOTOVOUCH"].ToString();
                    objCompra.Despacho = dr["DESCPACHO"].ToString();
                    objCompra.TipoPago = dr["TIPOPAG"].ToString();

                    Lista.Add(objCompra);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasGeneralMisCompras", "USP_LISTACOMPRASGENERALMISCOMPRAS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaComprasGeneral()
        {
            List<Compra> Lista = new List<Compra>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime hoy = DateTime.Now.AddHours(-3);
            DateTime dia1 = DateTime.Now.AddDays(-1);
            string fecha1S = hoy.ToString("yyyy/MM/dd");
            string fecha2S = dia1.ToString("yyyy/MM/dd");
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACOMPRASGENERAL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra objCompra = new Compra();
                    objCompra.Ticket = dr["TICKET"].ToString();
                    objCompra.FechaPagoReporte = dr["FECHA"].ToString();
                    objCompra.IdopPeruShop = dr["IDOP"].ToString();
                    objCompra.Cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    objCompra.MontoAPagar = Convert.ToDouble(dr["MONTO"].ToString());
                    objCompra.PuntosTotal = Convert.ToDouble(dr["PUNTOS"].ToString());
                    objCompra.Estado = Convert.ToInt32(dr["ESTADO"].ToString());
                    objCompra.NombreCliente = dr["NOMBRES"].ToString();
                    objCompra.ApellidoPat = dr["APELLIDOPAT"].ToString();
                    objCompra.ApellidoMat = dr["APELLIDOMAT"].ToString();
                    objCompra.Despacho = dr["DESCPACHO"].ToString();
                    objCompra.TipoPago = dr["TIPOPAG"].ToString();
                    objCompra.PaqueteSocio = dr["PAQUETECLIENTE"].ToString();

                    Lista.Add(objCompra);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasGeneral", "USP_LISTACOMPRASGENERAL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaComprasEfectivo()
        {
            List<Compra> Lista = new List<Compra>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime hoy = DateTime.Now.AddHours(-3);
            DateTime dia1 = DateTime.Now.AddDays(-3);
            string fecha1S = hoy.ToString("yyyy/MM/dd");
            string fecha2S = dia1.ToString("yyyy/MM/dd");
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACOMPRASEFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra objCompra = new Compra();
                    objCompra.Ticket = dr["TICKET"].ToString();
                    objCompra.FechaPagoReporte = dr["FECHA"].ToString();
                    objCompra.IdopPeruShop = dr["IDOP"].ToString();
                    objCompra.Cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    objCompra.MontoAPagar = Convert.ToDouble(dr["MONTO"].ToString());
                    objCompra.PuntosTotal = Convert.ToDouble(dr["PUNTOS"].ToString());
                    objCompra.Estado = Convert.ToInt32(dr["ESTADO"].ToString());
                    objCompra.NombreCliente = dr["NOMBRES"].ToString().Trim();
                    objCompra.ApellidoPat = dr["APELLIDOPAT"].ToString().Trim();
                    objCompra.ApellidoMat = dr["APELLIDOMAT"].ToString().Trim();
                    objCompra.Despacho = dr["DESCPACHO"].ToString().Trim();
                    objCompra.TipoPago = dr["TIPOPAG"].ToString();
                    objCompra.DNICliente = dr["NUMDOC"].ToString().Trim();
                    objCompra.DNIDespacho = dr["NUMDESPACHO"].ToString().Trim();
                    objCompra.idTipoCompra = dr["IDTIPOCOMPRA"].ToString();
                    objCompra.TipoCompra = dr["TIPOCOMPRA"].ToString();
                    objCompra.CodCliente = dr["IDCLIENTE"].ToString();
                    objCompra.NotaDelivery = dr["NOTADELIVERY"].ToString().Trim();
                    objCompra.Comprobante = dr["COMPROBANTE"].ToString();
                    objCompra.DireccionCliente = dr["Direccion"].ToString().Trim();
                    objCompra.Ruc = dr["RUC"].ToString().Trim();

                    Lista.Add(objCompra);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasEfectivo", "USP_LISTACOMPRASEFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaComprasGeneralFiltrado(string fecha1, string fecha2)
        {
            List<Compra> Lista = new List<Compra>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACOMPRASGENERAL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra objCompra = new Compra();
                    objCompra.Ticket = dr["TICKET"].ToString();
                    objCompra.FechaPagoReporte = dr["FECHA"].ToString();
                    objCompra.IdopPeruShop = dr["IDOP"].ToString();
                    objCompra.Cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    objCompra.MontoAPagar = Convert.ToDouble(dr["MONTO"].ToString());
                    objCompra.PuntosTotal = Convert.ToDouble(dr["PUNTOS"].ToString());
                    objCompra.Estado = Convert.ToInt32(dr["ESTADO"].ToString());
                    objCompra.NombreCliente = dr["NOMBRES"].ToString();
                    objCompra.ApellidoPat = dr["APELLIDOPAT"].ToString();
                    objCompra.ApellidoMat = dr["APELLIDOMAT"].ToString();
                    objCompra.Despacho = dr["DESCPACHO"].ToString();
                    objCompra.TipoPago = dr["TIPOPAG"].ToString();
                    objCompra.PaqueteSocio = dr["PAQUETECLIENTE"].ToString();
                    Lista.Add(objCompra);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasGeneralFiltrado", "USP_LISTACOMPRASGENERAL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaComprasEfectivoFiltrado(string fecha1, string fecha2)
        {
            List<Compra> Lista = new List<Compra>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACOMPRASEFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra objCompra = new Compra();
                    objCompra.Ticket = dr["TICKET"].ToString();
                    objCompra.FechaPagoReporte = dr["FECHA"].ToString();
                    objCompra.IdopPeruShop = dr["IDOP"].ToString();
                    objCompra.Cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    objCompra.MontoAPagar = Convert.ToDouble(dr["MONTO"].ToString());
                    objCompra.PuntosTotal = Convert.ToDouble(dr["PUNTOS"].ToString());
                    objCompra.Estado = Convert.ToInt32(dr["ESTADO"].ToString());
                    objCompra.NombreCliente = dr["NOMBRES"].ToString().Trim();
                    objCompra.ApellidoPat = dr["APELLIDOPAT"].ToString().Trim();
                    objCompra.ApellidoMat = dr["APELLIDOMAT"].ToString().Trim();
                    objCompra.Despacho = dr["DESCPACHO"].ToString().Trim();
                    objCompra.TipoPago = dr["TIPOPAG"].ToString();
                    objCompra.DNICliente = dr["NUMDOC"].ToString().Trim();
                    objCompra.DNIDespacho = dr["NUMDESPACHO"].ToString().Trim();
                    objCompra.idTipoCompra = dr["IDTIPOCOMPRA"].ToString();
                    objCompra.TipoCompra = dr["TIPOCOMPRA"].ToString();
                    objCompra.CodCliente = dr["IDCLIENTE"].ToString();
                    objCompra.NotaDelivery = dr["NOTADELIVERY"].ToString().Trim();
                    objCompra.Comprobante = dr["COMPROBANTE"].ToString();
                    objCompra.DireccionCliente = dr["Direccion"].ToString().Trim();
                    objCompra.Ruc = dr["RUC"].ToString().Trim();

                    Lista.Add(objCompra);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasEfectivoFiltrado", "USP_LISTACOMPRASEFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaComprasPendientesGeneral()
        {
            List<Compra> Lista = new List<Compra>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasPendientesGeneral", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.FechaPago = Convert.ToDateTime(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.FotoVaucher = Convert.ToString(dr["FotoVaucher"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    compra.IdopPeruShop = Convert.ToString(dr["IdopPeruShop"]);
                    compra.IdMove = Convert.ToString(dr["idmove"]);
                    Lista.Add(compra);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasPendientesGeneral", "USP_ListarComprasPendientesGeneral", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaComprasPendientesAprobacionDelCliente(string numDocCliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasPendientesAprobacion", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", numDocCliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.FotoVaucher = Convert.ToString(dr["FotoVaucher"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    compra.DespachoVoucher = Convert.ToString(dr["DespachoVoucher"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasPendientesAprobacionDelCliente", "USP_ListarComprasPendientesAprobacion", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasPendientesEfectivo(string idcliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasPendientesEfectivo", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", idcliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasPendientesEfectivo", "USP_ListarComprasPendientesEfectivo", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasAnuladasDelCliente(string numDocCliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasAnuladas", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", numDocCliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.Estado = Convert.ToInt32(dr["Estado"]);
                    compra.FotoVaucher = Convert.ToString(dr["FotoVaucher"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasAnuladasDelCliente", "USP_ListarComprasAnuladas", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public Compra DatosCompraParaObtenerIdopPeruShop(string ticket)
        {
            Compra compra = null;

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DatosCompraParaObtenerIdopPeruShop", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    compra = new Compra();
                    compra.CodCliente = Convert.ToString(dr["NumDocuCliente"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "DatosCompraParaObtenerIdopPeruShop", "USP_DatosCompraParaObtenerIdopPeruShop", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return compra;
        }

        public List<TipoPago> ListaTipoPago()
        {
            List<TipoPago> Lista = new List<TipoPago>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTATIPOPAGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    TipoPago tipoPago = new TipoPago();
                    tipoPago.idPago = dr["ID"].ToString();
                    tipoPago.descripcion = dr["DESCRIPCION"].ToString();

                    Lista.Add(tipoPago);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaTipoPago", "USP_LISTATIPOPAGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Compra> ListaPuntajePromo(string idCliente, string fecha1, string fecha2)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMAPUNTAJEPROMO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", idCliente);
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.PuntosTotal = Convert.ToInt32(dr["PuntosTotal"]);
                    compra.FechaPago = Convert.ToDateTime(dr["FechaPago"]);
                    compra.IdopPeruShop = Convert.ToString(dr["IdopPeruShop"]);
                    compra.IdPromo = Convert.ToString(dr["idpromo"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaPuntajePromo", "USP_SUMAPUNTAJEPROMO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public int CantidadPromo(string idCliente, string idPromo, string fecha1, string fecha2)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            int CP = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CANTPROMO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPROMO", idPromo.Trim());
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (Convert.ToString(dr["SUMACAN"]) == "")
                    {
                        CP = 0;
                    }
                    else
                    {
                        CP = Convert.ToInt32(dr["SUMACAN"]);
                    }

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CantidadPromo", "USP_CANTPROMO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return CP;
        }

        public int CantidadRegalo(string idCliente, string idPromo, string fecha1, string fecha2)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            int CP = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CANTREGALO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPROMO", idPromo.Trim());
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (Convert.ToString(dr["SUMACAN"]) == "")
                    {
                        CP = 0;
                    }
                    else
                    {
                        CP = Convert.ToInt32(dr["SUMACAN"]);
                    }

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CantidadRegalo", "USP_CANTREGALO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return CP;
        }

        public double PuntosGastados(string idCliente, string idPromo, string fecha1, string fecha2)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            double CP = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_PUNTOSGASTADOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPROMO", idPromo.Trim());
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (Convert.ToString(dr["SUMACAN"]) == "")
                    {
                        CP = 0;
                    }
                    else
                    {
                        CP = Convert.ToInt32(dr["SUMACAN"]);
                    }

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "PuntosGastados", "USP_PUNTOSGASTADOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return CP;
        }

        public List<Compra> ListarComprasAntiguas(string idcliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTARCOMPRASANTIGUAS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.FechaPago = Convert.ToDateTime(dr["FechaPago"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarComprasAntiguas", "USP_LISTARCOMPRASANTIGUAS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListarProductosxTicket(string ticket)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPRODUCTOSXTICKET", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.idProductoPS = Convert.ToString(dr["IdProductoPeruShop"]);
                    compra.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    compra.IdProductoPais = Convert.ToString(dr["IdProductoxPais"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarProductosxTicket", "USP_LISTAPRODUCTOSXTICKET", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public int CantidadCompras(string idCliente)
        {
            List<PuntosComisiones> listaPuntos = new List<PuntosComisiones>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int CP = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTARCOMPRASTOTALES", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CP = (Convert.ToString(dr["CANTIDAD"]) == "") ? 0 : Convert.ToInt32(dr["CANTIDAD"]);

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CantidadCompras", "USP_LISTARCOMPRASTOTALES", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return CP;
        }

        public List<HoraCantidad> ObtenerHoraCompraStock(string IDPPS)
        {
            List<HoraCantidad> listaStock = new List<HoraCantidad>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENERHORACOMPRASTOCK", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", IDPPS.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    HoraCantidad hc = new HoraCantidad();
                    if (Convert.ToString(dr["fechaStock"]) == "" | Convert.ToString(dr["fechaStock"]) == null)
                    {
                        hc.fecha = "0";
                    }
                    else
                    {
                        hc.fecha = Convert.ToString(dr["fechaStock"]);
                    }
                    hc.cantidad = dr["Cantidad"].ToString();
                    listaStock.Add(hc);

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ObtenerHoraCompraStock", "USP_OBTENERHORACOMPRASTOCK", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            if (listaStock.Count == 0)
            {
                HoraCantidad hc = new HoraCantidad();
                hc.fecha = "0";
                hc.cantidad = "0";
                listaStock.Add(hc);
            }

            return listaStock;
        }
        public List<Compra> DevolverVoucherTemporal(string usuario)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Compra> listaCompras = new List<Compra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("ListarVoucherTemporal", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.IdVoucher = Convert.ToInt32(dr["IdVoucher"]);
                    compra.Ruta = Convert.ToString(dr["Ruta"]);
                    compra.Usuario = Convert.ToString(dr["Usuario"]);
                    listaCompras.Add(compra);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "DevolverVoucherTemporal", "ListarVoucherTemporal", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCompras;
        }
          public string cantidadTotalStock(string IDPPS)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string cantidadGeneral = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMAXPRODUCTOSINSTOCK", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", IDPPS.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (Convert.ToString(dr["Cantidad"]) == "" | Convert.ToString(dr["Cantidad"]) == null)
                    {
                        cantidadGeneral = "0";
                    }
                    else
                    {
                        cantidadGeneral = Convert.ToString(dr["Cantidad"]);
                    }

                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "cantidadTotalStock", "USP_SUMAXPRODUCTOSINSTOCK", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return cantidadGeneral;
        }

        public bool EliminarCompraCliente(string ticket)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINARCOMPRACLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCOMPRA", ticket);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "EliminarCompraCliente", "USP_ELIMINARCOMPRACLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool EliminarClienteSinCompras(string idCliente)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINARCLIENTESINCOMPRAS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "EliminarClienteSinCompras", "USP_ELIMINARCLIENTESINCOMPRAS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarPuntosComisiones(string documento, double PP, double VP, double VR, double VG, double monto, int IDP)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARPUNTOSCOMISIONES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@PP", PP);
                cmd.Parameters.AddWithValue("@VP", VP);
                cmd.Parameters.AddWithValue("@VR", VR);
                cmd.Parameters.AddWithValue("@VG", VG);
                cmd.Parameters.AddWithValue("@MONTO", monto);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarPuntosComisiones", "USP_ACTUALIZARPUNTOSCOMISIONES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarPuntosVIP(string documento, double VIP, int IDP)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_VIP", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@VIP", VIP);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarPuntosVIP", "USP_ACTUALIZAR_VIP", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComisionesAutomatico(string documento, double COMUNI, double COMCI, double COMCON, double COMAFI, double COMTIBU,
                                                   double COMBRON, double COMESCO, double COMMERCA, double NIVEL1, double NIVEL2, double NIVEL3,
                                                   double NIVEL4, double NIVEL5, int IDP)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARCOMISIONES_AUTOMATICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@COMUNI", COMUNI);
                cmd.Parameters.AddWithValue("@COMCI", COMCI);
                cmd.Parameters.AddWithValue("@COMCON", COMCON);
                cmd.Parameters.AddWithValue("@COMAFI", COMAFI);
                cmd.Parameters.AddWithValue("@COMTIBU", COMTIBU);
                cmd.Parameters.AddWithValue("@COMBRON", COMBRON);
                cmd.Parameters.AddWithValue("@COMESCO", COMESCO);
                cmd.Parameters.AddWithValue("@COMMERCA", COMMERCA);
                cmd.Parameters.AddWithValue("@NIVEL1", NIVEL1);
                cmd.Parameters.AddWithValue("@NIVEL2", NIVEL2);
                cmd.Parameters.AddWithValue("@NIVEL3", NIVEL3);
                cmd.Parameters.AddWithValue("@NIVEL4", NIVEL4);
                cmd.Parameters.AddWithValue("@NIVEL5", NIVEL5);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarComisionesAutomatico", "USP_ACTUALIZARCOMISIONES_AUTOMATICO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComisionAfiliacion(string documento, double COMAFI, int IDP, double nivel1,
                                                double nivel2, double nivel3, double nivel4, double nivel5)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARMONTOAFILIACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@COMAFI", COMAFI);
                cmd.Parameters.AddWithValue("@NIVEL1", nivel1);
                cmd.Parameters.AddWithValue("@NIVEL2", nivel2);
                cmd.Parameters.AddWithValue("@NIVEL3", nivel3);
                cmd.Parameters.AddWithValue("@NIVEL4", nivel4);
                cmd.Parameters.AddWithValue("@NIVEL5", nivel5);

                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarComisionAfiliacion", "USP_ACTUALIZARMONTOAFILIACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarComUniIdcliente(string idcliente, double COMAFI, int IDP, string nom_nivel,
                                                double monto_nivel)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_COMUNI_IDCLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@COMAFI", COMAFI);
                cmd.Parameters.AddWithValue("@NOM_NIVEL", nom_nivel);
                cmd.Parameters.AddWithValue("@MONTO_NIVEL", monto_nivel);

                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarComUniIdcliente", "USP_ACTUALIZAR_COMUNI_IDCLIENTE", ex.Message);
                throw ex;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public double ObtenerMontoTiburon(string documento)
        {
            List<Cliente> listaFC = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double monto = 0.00;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_MONTO_TIBURON", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    monto = Convert.ToDouble(dr["COMTIBU"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ObtenerMontoTiburon", "USP_OBTENER_MONTO_TIBURON", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return monto;
        }

        public List<Compra> CDRxTicket(string ticket)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Compra> listaCompras = new List<Compra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DESPACHOXTICKET", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Despacho = Convert.ToString(dr["Despacho"]).Trim();
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);

                    listaCompras.Add(compra);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CDRxTicket", "USP_DESPACHOXTICKET", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCompras;
        }

        public Compra DatosPuntosYMontosCompra(string ticket)
        {
            Compra compra = null;
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_PUNTOSYMONTOCOMPRA", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    compra = new Compra();
                    compra.Corazones = Convert.ToDouble(dr["PuntosCorazones"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.montoComision = Convert.ToDouble(dr["montoComision"]);
                    compra.montoComisionCI = Convert.ToDouble(dr["montoComicionCI"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "DatosPuntosYMontosCompra", "USP_PUNTOSYMONTOCOMPRA", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return compra;
        }

        public bool ActualizarVPR(string documento, int IDP)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMAVPR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarVPR", "USP_SUMAVPR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public string getIdPago()
        {
            bool ok = false;
            SqlConnection cn = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int resultado = 0;
            string idPAGO = "";
            string pago = "";
            try
            {
                cn = Conexion.getInstance().Conectar();
                cn.Open();

                cmd = new SqlCommand("USP_BUSCAR_ID_PAGO_CLIENTE", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                resultado = cmd.ExecuteNonQuery();
                dr = null;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    if (Convert.ToString(dr["random_num"]) == "")
                    {
                        idPAGO = "00000001";
                    }
                    else
                    {
                        pago = Convert.ToString(dr["random_num"]);
                        string resPago = (pago).ToString();
                        idPAGO = (resPago.Length == 1) ? "0000000" + resPago : (resPago.Length == 2)
                                                       ? "000000" + resPago : (resPago.Length == 3)
                                                       ? "00000" + resPago : (resPago.Length == 4)
                                                       ? "0000" + resPago : (resPago.Length == 5)
                                                       ? "000" + resPago : (resPago.Length == 6)
                                                       ? "00" + resPago : (resPago.Length == 7)
                                                       ? "0" + resPago : resPago;
                    }

                }
                idPAGO = (idPAGO == "") ? "00000001" : pago;
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "getIdPago", "USP_BUSCAR_ID_PAGO_CLIENTE", ex.Message);
                throw ex;
            }

            return idPAGO.Trim();
        }

        public bool SavePagoPorCliente(string idpago, string idcliente, string idproducto, string precioProducto, string token)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GUARDAR_PAGO_CLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdPago", idpago);
                cmd.Parameters.AddWithValue("@IdCliente", idcliente);
                cmd.Parameters.AddWithValue("@IdProducto", idproducto);
                cmd.Parameters.AddWithValue("@PrecioProducto", precioProducto);
                cmd.Parameters.AddWithValue("@TokenAccess", token);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "SavePagoPorCliente", "USP_GUARDAR_PAGO_CLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<DatosPedidosVisaNet> ListarPedidoXIdPago(string idPago)
        {
            List<DatosPedidosVisaNet> listaCompras = new List<DatosPedidosVisaNet>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPEDIDOXIDPAGO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPAGO", idPago);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DatosPedidosVisaNet compra = new DatosPedidosVisaNet();
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    compra.Telefono = Convert.ToString(dr["Telefono"]);
                    compra.Direccion = Convert.ToString(dr["Direccion"]);
                    compra.IdPedido = Convert.ToString(dr["IdPago"]);
                    compra.NumTarjeta = Convert.ToString(dr["CardS"]);
                    compra.FechaHora = Convert.ToString(dr["Transaction_Date"]);
                    compra.Importe = Convert.ToString(dr["AMOUNT"]);
                    compra.TarjetaHabiente = Convert.ToString(dr["NOMBRES"]);
                    compra.CodigoAccion = Convert.ToString(dr["ACTION_DESCRIPTION"]);
                    compra.Productos = getInstance().ListarProductosXIdPago(idPago);
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarPedidoXIdPago", "USP_LISTAPEDIDOXIDPAGO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<DatosPedidosVisaNet.ListProductos> ListarProductosXIdPago(string idPago)
        {
            List<DatosPedidosVisaNet.ListProductos> listaCompras = new List<DatosPedidosVisaNet.ListProductos>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTAPRODUCTOSXIDPAGO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPAGO", idPago);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DatosPedidosVisaNet.ListProductos compra = new DatosPedidosVisaNet.ListProductos();
                    compra.Descripcion = Convert.ToString(dr["Nombre"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarProductosXIdPago", "LISTAPRODUCTOSXIDPAGO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaTicketXCliente(string idCliente)
        {

            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Compra> listaTicket = new List<Compra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTATICKETXSOCIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]).Trim();

                    listaTicket.Add(compra);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaTicketXCliente", "USP_LISTATICKETXSOCIO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTicket;
        }

        public List<Compra> ListaCompraAfiliacion(string fechaInicio, string fechaFin)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Compra> listaCompras = new List<Compra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_COMPRAS_AFILIACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@INICIO", fechaInicio);
                cmd.Parameters.AddWithValue("@FIN", fechaFin);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.montoComision = Convert.ToDouble(dr["montoComision"]);
                    compra.DNICliente = Convert.ToString(dr["NumeroDoc"]).Trim();

                    listaCompras.Add(compra);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaCompraAfiliacion", "USP_LISTA_COMPRAS_AFILIACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprUnilevel(string fechaInicio, string fechaFin)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Compra> listaCompras = new List<Compra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_COMPRAS_UNILEVEL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@INICIO", fechaInicio);
                cmd.Parameters.AddWithValue("@FIN", fechaFin);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.montoComision = Convert.ToDouble(dr["montComi"]);
                    compra.montoComisionCI = Convert.ToDouble(dr["montComiCI"]);
                    compra.DNICliente = Convert.ToString(dr["NumeroDoc"]).Trim();
                    compra.tipoCliente = Convert.ToString(dr["TipoCliente"]).Trim();
                    compra.DNIPatrocinador = Convert.ToString(dr["Patrocinador"]).Trim();

                    listaCompras.Add(compra);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprUnilevel", "USP_LISTA_COMPRAS_UNILEVEL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCompras;
        }

        public List<Compra> ListarComprasAntiguasGeneral()
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADO_COMPRAS_ANTIGUAS_GENERAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.FechaPago = Convert.ToDateTime(dr["fechaStock"]);
                    compra.TipoPago = Convert.ToString(dr["TipoPago"]).Trim();

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarComprasAntiguasGeneral", "USP_LISTADO_COMPRAS_ANTIGUAS_GENERAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListarComprasComicionCI(int IDP)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_COMPRAS_MONTO_COMISIONCI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.montoComision = Convert.ToDouble(dr["montoComision"]);
                    compra.descuento = Convert.ToString(dr["Descuento"].ToString().Replace(".", ","));
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarComprasComicionCI", "USP_COMPRAS_MONTO_COMISIONCI", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public bool ActualizarMontoComisionCI(string ticket, double monto)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_MONTO_COMISIONCI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@MONTOCOMICION", monto);
                cmd.Parameters.AddWithValue("@TICKET", ticket);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarMontoComisionCI", "USP_ACTUALIZAR_MONTO_COMISIONCI", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Compra> ListarComprasCalculoComisionCI(int IDP)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_COMPRAS_COMISICIONCI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.montoComisionCI = Convert.ToDouble(dr["montoComicionCI"]);
                    compra.DNIPatrocinador = Convert.ToString(dr["NumeroDoc"]);
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarComprasCalculoComisionCI", "USP_LISTA_COMPRAS_COMISICIONCI", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListarComprasCalculoComisionConsultor(int IDP)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_COMPRAS_COMISION_CONSULTOR_RECALCULO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.montoComision = Convert.ToDouble(dr["montoComision"]);
                    compra.DNIPatrocinador = Convert.ToString(dr["NumeroDoc"]);
                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListarComprasCalculoComisionConsultor", "USP_LISTA_COMPRAS_COMISION_CONSULTOR_RECALCULO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public int CantidadComprasConIDOP(string idCliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int CP = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CANTIDAD_COMPRAS_SOCIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CP = (Convert.ToString(dr["CANTIDAD"]) == "") ? 0 : Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CantidadComprasConIDOP", "USP_CANTIDAD_COMPRAS_SOCIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return CP;
        }

        public int CantidadComprasPendientes(string idCliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int CP = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CANTIDAD_COMPRAS_PENDIENTES_SOCIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CP = (Convert.ToString(dr["CANTIDAD"]) == "") ? 0 : Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CantidadComprasPendientes", "USP_CANTIDAD_COMPRAS_PENDIENTES_SOCIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return CP;
        }

        public string UltimoTicketXCliente(string idCliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string ticket = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("OBTENER_TICKET_ULTIMA_COMPRA", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ticket = Convert.ToString(dr["Ticket"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "UltimoTicketXCliente", "OBTENER_TICKET_ULTIMA_COMPRA", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return ticket;
        }

        public void RegistroDatosPagoEfectivo(DatosPagoEfectivo objPago)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("REGISTRO_DATOS_PAGO_EFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", objPago.Ticket);
                cmd.Parameters.AddWithValue("@IDCLIENTE", objPago.IdCliente);
                cmd.Parameters.AddWithValue("@FECHACREACION", objPago.FechaCreacion);
                cmd.Parameters.AddWithValue("@FECHAEXPIRACION", objPago.FechaExpiracion);
                cmd.Parameters.AddWithValue("@CIP", objPago.CIP);
                cmd.Parameters.AddWithValue("@MONTO", objPago.Monto);
                cmd.Parameters.AddWithValue("@CODE_STATUS", objPago.CodeStatus);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistroDatosPagoEfectivo", "REGISTRO_DATOS_PAGO_EFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Compra> ListaComprasPendientesPagoEfectivo(string idcliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasPendientesPagoEfec", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", idcliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaCreacion = Convert.ToString(dr["FECHACREACION"]);
                    compra.FechaExpiracion = Convert.ToString(dr["FECHAEXPIRACION"]);
                    compra.CIP = Convert.ToString(dr["CIP"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasPendientesPagoEfectivo", "USP_ListarComprasPendientesPagoEfec", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<Compra> ListaComprasRealizadasPagoEfectivo(string idcliente)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarComprasRealizadasPagoEfec", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idcliente", idcliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]);
                    compra.MontoAPagar = Convert.ToDouble(dr["MontoAPagar"]);
                    compra.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(dr["puntosTotalPromo"]);
                    compra.FechaPago2 = Convert.ToString(dr["FechaPago"]);
                    compra.FechaPagada = Convert.ToString(dr["FECHAPAGADA"]);
                    compra.CIP = Convert.ToString(dr["CIP"]);
                    compra.Despacho = Convert.ToString(dr["Despacho"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaComprasRealizadasPagoEfectivo", "USP_ListarComprasRealizadasPagoEfec", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public bool ActualizarDatosCompraPE(string cip, string fechapago, string numoperacion)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_APROBACION_PAGO_EFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CIP", cip);
                cmd.Parameters.AddWithValue("@NUM_OPERACION", numoperacion);
                cmd.Parameters.AddWithValue("@FECHAPAGADA", fechapago);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarDatosCompraPE", "USP_APROBACION_PAGO_EFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Compra> ListaCompraDatosPagoEfectivoPendiente()
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_COMPRAPE", conexion);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Ticket = Convert.ToString(dr["Ticket"]).Trim();
                    compra.Despacho = Convert.ToString(dr["Despacho"]).Trim();
                    compra.NotaPS = Convert.ToString(dr["NotaPS"]).Trim();
                    compra.NotaDelivery = Convert.ToString(dr["NotaDelivery"]).Trim();
                    compra.FechaPago = Convert.ToDateTime(dr["FechaPago"]);
                    compra.idTipoCompra = Convert.ToString(dr["idTipoCompra"]).Trim();
                    compra.DNICliente = Convert.ToString(dr["NumeroDoc"]).Trim();
                    compra.NombreCliente = Convert.ToString(dr["Nombres"]).Trim();
                    compra.DireccionCliente = Convert.ToString(dr["Direccion"]).Trim();
                    compra.DNIFactorComision = Convert.ToString(dr["factorComision"]).Trim();
                    compra.DNIPatrocinador = Convert.ToString(dr["Patrocinador"]).Trim();
                    compra.PaqueteSocio = Convert.ToString(dr["IdPackete"]).Trim();
                    compra.CIP = Convert.ToString(dr["CIP"]).Trim();
                    compra.FechaPagada = Convert.ToString(dr["FECHAPAGADA"]).Trim();
                    compra.MontoAPagar = Convert.ToDouble(dr["MONTO"]);
                    compra.NumOperacion = Convert.ToString(dr["NUM_OPERACION"]).Trim();
                    compra.Comprobante = Convert.ToString(dr["COMPROBANTE"]).Trim();
                    compra.tipoCliente = Convert.ToString(dr["TipoCliente"]).Trim();
                    compra.IdCliente = Convert.ToString(dr["IdCliente"]).Trim();
                    compra.Ruc = Convert.ToString(dr["RUC"]).Trim();

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaCompraDatosPagoEfectivoPendiente", "USP_LISTA_DATOS_COMPRAPE", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public bool ActualizarEstadoPagoEfectivo(string ticket)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_ESTADO_PAGOEFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarEstadoPagoEfectivo", "USP_ACTUALIZAR_ESTADO_PAGOEFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ReestablecerEstadoPagoEfectivo(string ticket)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REESTABLECER_ESTADO_PAGOEFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ReestablecerEstadoPagoEfectivo", "USP_REESTABLECER_ESTADO_PAGOEFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Compra> ListaCompraDatosxTicket(string ticket)
        {
            List<Compra> listaCompras = new List<Compra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_COMPRAXTICKET", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra compra = new Compra();
                    compra.Despacho = Convert.ToString(dr["Despacho"]);
                    compra.NotaPS = Convert.ToString(dr["NotaPS"]);
                    compra.DNICliente = Convert.ToString(dr["NumeroDoc"]);
                    compra.NombreCliente = Convert.ToString(dr["Nombres"]);
                    compra.DireccionCliente = Convert.ToString(dr["Direccion"]);
                    compra.TipoPago = Convert.ToString(dr["TipoPago"]);
                    compra.BRAND = Convert.ToString(dr["BRAND"]);
                    compra.Ruc = Convert.ToString(dr["RUC"]);

                    listaCompras.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaCompraDatosxTicket", "USP_LISTA_DATOS_COMPRAXTICKET", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public string ConsultaIdRUC(string documento)
        {
            SqlDataReader dr = null;
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            string result = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("CONSULTA_IDRUC", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result = (Convert.ToString(dr["IDRUC"]) == null) ? "" : Convert.ToString(dr["IDRUC"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ConsultaIdRUC", "CONSULTA_IDRUC", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return result;

        }

        public bool RegistroIdRuc(string documento, string idruc, DateTime fecha)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_IDRUC", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA", fecha);
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDRUC", idruc);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistroIdRuc", "USP_REGISTRO_IDRUC", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Compra.NuevoCIP> ListaDatosNuevoCIP(string ticket)
        {
            List<Compra.NuevoCIP> listaDatos = new List<Compra.NuevoCIP>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOS_NUEVO_CIP", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra.NuevoCIP compra = new Compra.NuevoCIP();
                    compra.Documento = Convert.ToString(dr["NumeroDoc"]).Trim();
                    compra.Correo = Convert.ToString(dr["Correo"]).Trim();
                    compra.IdCliente = Convert.ToString(dr["IdCliente"]).Trim();
                    compra.Nombres = Convert.ToString(dr["Nombres"]).Trim();
                    compra.Apellidos = Convert.ToString(dr["Apellidos"]).Trim();
                    compra.MontoAPagar = Convert.ToDecimal(dr["MontoAPagar"]);
                    listaDatos.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaDatosNuevoCIP", "USP_LISTA_DATOS_NUEVO_CIP", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaDatos;
        }

        public void ActualizarDatosPagoEfectivo(DatosPagoEfectivo objPago)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("ACTUALIZAR_DATOS_PAGO_EFECTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", objPago.Ticket);
                cmd.Parameters.AddWithValue("@FECHACREACION", objPago.FechaCreacion);
                cmd.Parameters.AddWithValue("@FECHAEXPIRACION", objPago.FechaExpiracion);
                cmd.Parameters.AddWithValue("@CIP", objPago.CIP);
                cmd.Parameters.AddWithValue("@MONTO", objPago.Monto);
                cmd.Parameters.AddWithValue("@CODE_STATUS", objPago.CodeStatus);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarDatosPagoEfectivo", "ACTUALIZAR_DATOS_PAGO_EFECTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Compra.Facturacion> ListaFacturacionSocios(string idCliente)
        {
            List<Compra.Facturacion> listaDatos = new List<Compra.Facturacion>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAR_FACTURACION_SOCIOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra.Facturacion compra = new Compra.Facturacion();
                    compra.DniCliente = Convert.ToString(dr["DNI_CLIENTE"]);
                    compra.Nombres = Convert.ToString(dr["NOMBRES_COMPLETO"]);
                    compra.Comision = Convert.ToDecimal(dr["COMISION"]);
                    compra.FechaS = Convert.ToString(dr["FECHA_REGISTRO"]).Substring(0,16);
                    compra.TipoCliente = Convert.ToString(dr["TIPO_CLIENTE"]);
                    compra.RUC = Convert.ToString(dr["RUC"]);
                    compra.Archivo = Convert.ToString(dr["ARCHIVO"]);
                    compra.Observacion = Convert.ToString(dr["OBSERVACION"]);
                    compra.MontoCanje = Convert.ToDecimal(dr["MONTO_CANJE"]);
                    compra.NombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    compra.EstadoCanje = Convert.ToString(dr["ESTADO_CANJE"]);
                    compra.EstadoSaldoDisponible = Convert.ToString(dr["ESTADO_SALDO_DISPONIBLE"]);
                    compra.Correcion = Convert.ToString(dr["HABILITAR_CORRECCION"]);
                    compra.Id_Datos = Convert.ToInt32(dr["ID_DATOS"]);
                    compra.IdPeriodo = Convert.ToInt32(dr["ID_PERIODO"]);
                    compra.Sellado = Convert.ToBoolean(dr["SELLADO"]);
                    listaDatos.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaFacturacionSocios", "USP_LISTAR_FACTURACION_SOCIOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaDatos;
        }

        public void RegistrarFacturacionSocios(Compra.Facturacion objFactura)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_FACTURACION_SOCIOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_PERIODO", objFactura.IdPeriodo);
                cmd.Parameters.AddWithValue("@ID_CLIENTE", objFactura.IdCliente);
                cmd.Parameters.AddWithValue("@TIPO_CLIENTE", objFactura.TipoCliente);
                cmd.Parameters.AddWithValue("@RUC", objFactura.RUC);
                cmd.Parameters.AddWithValue("@ARCHIVO", objFactura.Archivo);
                cmd.Parameters.AddWithValue("@DNI_CLIENTE", objFactura.DniCliente);
                cmd.Parameters.AddWithValue("@NOMBRES_COMPLETO", objFactura.Nombres);
                cmd.Parameters.AddWithValue("@FECHA_REGISTRO", objFactura.FechaRegistro);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "RegistrarFacturacionSocios", "USP_REGISTRO_FACTURACION_SOCIOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Compra.Facturacion> ListaCanjesAbono(int periodo)
        {
            List<Compra.Facturacion> listaDatos = new List<Compra.Facturacion>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CANJES_ABONO_ADMIN", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", periodo);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra.Facturacion compra = new Compra.Facturacion();
                    compra.Id_Datos = Convert.ToInt32(dr["ID_DATOS"]);
                    compra.DniCliente = Convert.ToString(dr["DNI_CLIENTE"]);
                    compra.Nombres = Convert.ToString(dr["NOMBRES_COMPLETO"]);
                    compra.Comision = Convert.ToDecimal(dr["COMISION"]);
                    compra.TipoCliente = Convert.ToString(dr["TERCERO"]);
                    compra.RUC = Convert.ToString(dr["RUC"]);
                    compra.Archivo = Convert.ToString(dr["ARCHIVO"]);
                    compra.EstadoSaldoDisponible = Convert.ToString(dr["ESTADO_SALDO_DISPONIBLE"]);
                    compra.SaldoDisponible = Convert.ToDecimal(dr["SALDO_DISPONIBLE"]);
                    compra.IdCliente = Convert.ToString(dr["ID_CLIENTE"]);
                    compra.Sellado = Convert.ToBoolean(dr["SELLADO"]);
                    compra.IdPeriodo = Convert.ToInt32(dr["ID_PERIODO"]);
                    listaDatos.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaCanjesAbono", "USP_LISTA_CANJES_ABONO_ADMIN", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaDatos;
        }

        public List<Compra.Facturacion> ListaDatalleCanjesAbono(int idDatos)
        {
            List<Compra.Facturacion> listaDatos = new List<Compra.Facturacion>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DETALLE_CANJES_ABONO_ADMIN", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", idDatos);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra.Facturacion compra = new Compra.Facturacion();
                    compra.IdPeriodo = Convert.ToInt32(dr["ID_PERIODO"]);
                    compra.EstadoSaldoDisponible = Convert.ToString(dr["ESTADO_SALDO_DISPONIBLE"]);
                    compra.SolicitudCanjeCombo = Convert.ToString(dr["SOLICITA_CANJE"]);
                    compra.MontoCanje = Convert.ToDecimal(dr["MONTO_CANJE"]);
                    compra.EstadoCanje = Convert.ToString(dr["ESTADO_CANJE"]);
                    compra.Tickets = Convert.ToString(dr["TICKETS"]);
                    compra.Observacion = Convert.ToString(dr["OBSERVACION"]);
                    compra.Correcion = Convert.ToString(dr["HABILITAR_CORRECCION"]);
                    listaDatos.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaDatalleCanjesAbono", "USP_DETALLE_CANJES_ABONO_ADMIN", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaDatos;
        }

        public void ActualizarDetalleCanjesAbono(Compra.Facturacion objFactura)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_REGISTRO_CONTROL_CANJES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", objFactura.Id_Datos);
                cmd.Parameters.AddWithValue("@ID_PERIODO", objFactura.IdPeriodo);
                cmd.Parameters.AddWithValue("@ESTADO_SALDO_DISPONIBLE", objFactura.EstadoSaldoDisponible);
                cmd.Parameters.AddWithValue("@SOLICITUD_CANJE", objFactura.SolicitudCanje);
                cmd.Parameters.AddWithValue("@MONTO_CANJE", objFactura.MontoCanje);
                cmd.Parameters.AddWithValue("@ESTADO_CANJE", objFactura.EstadoCanje);
                cmd.Parameters.AddWithValue("@TICKETS", objFactura.Tickets);
                cmd.Parameters.AddWithValue("@OBSERVACION", objFactura.Observacion);
                cmd.Parameters.AddWithValue("@HABILITAR_CORRECCION", objFactura.Correcion);
                cmd.Parameters.AddWithValue("@ID_CLIENTE", objFactura.IdCliente);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarDetalleCanjesAbono", "USP_ACTUALIZAR_REGISTRO_CONTROL_CANJES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void EliminarNombrePDF_ControlCanjes(int idDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_ARCHIVO_REGISTRO_CONTROL_CANJES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", idDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "EliminarNombrePDF_ControlCanjes", "USP_ELIMINAR_ARCHIVO_REGISTRO_CONTROL_CANJES", ex.Message);
                throw ex;
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void SellarRegistro_ControlCanjes(int idDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SELLAR_REGISTRO_CONTROL_CANJES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", idDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "SellarRegistro_ControlCanjes", "USP_SELLAR_REGISTRO_CONTROL_CANJES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void EliminarRegistro_ControlCanjes(int idDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_REGISTRO_CONTROL_CANJES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", idDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "EliminarRegistro_ControlCanjes", "USP_ELIMINAR_REGISTRO_CONTROL_CANJES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarFacturacionSocios(Compra.Facturacion objFactura)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_FACTURACION_SOCIOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", objFactura.Id_Datos);
                cmd.Parameters.AddWithValue("@ID_PERIODO", objFactura.IdPeriodo);
                cmd.Parameters.AddWithValue("@ID_CLIENTE", objFactura.IdCliente);
                cmd.Parameters.AddWithValue("@TIPO_CLIENTE", objFactura.TipoCliente);
                cmd.Parameters.AddWithValue("@RUC", objFactura.RUC);
                cmd.Parameters.AddWithValue("@ARCHIVO", objFactura.Archivo);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarFacturacionSocios", "USP_ACTUALIZAR_FACTURACION_SOCIOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<Compra.PendienteFacturacion> ListaSociosPendienteFacturas(int periodo)
        {
            List<Compra.PendienteFacturacion> listaDatos = new List<Compra.PendienteFacturacion>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_FACTURACION_PENDIENTE", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", periodo);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Compra.PendienteFacturacion compra = new Compra.PendienteFacturacion();
                    compra.IdCliente = Convert.ToString(dr["IDSOCIO"]);
                    compra.DniCliente = Convert.ToString(dr["NumeroDoc"]);
                    compra.Nombres = Convert.ToString(dr["NOMBRES"]);
                    compra.Celular = Convert.ToString(dr["Celular"]);
                    compra.Correo = Convert.ToString(dr["Correo"]);
                    compra.IdPeriodo = Convert.ToInt32(dr["IDPERIODO_COMI"]);
                    listaDatos.Add(compra);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListaSociosPendienteFacturas", "USP_LISTA_SOCIOS_FACTURACION_PENDIENTE", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaDatos;
        }

        public void ActualizarCorazonesRed(int idp, string idcliente, double corazones)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_PUNTOS_CORAZONES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", idp);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@CORAZONES", corazones);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarCorazonesRed", "USP_ACTUALIZAR_PUNTOS_CORAZONES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarEstadoTareaCompras(bool Estado)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_ESTADO_TAREA_COMPRAS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ESTADO", Estado);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ActualizarEstadoTareaCompras", "USP_ACTUALIZAR_ESTADO_TAREA_COMPRAS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ObtenerEstadoTareaCompras()
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            bool estado = false;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CONSULTA_ESTADO_TAREA_COMPRAS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    estado = Convert.ToBoolean(dr["ESTADO"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ObtenerEstadoTareaCompras", "USP_CONSULTA_ESTADO_TAREA_COMPRAS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return estado;
        }

        public int CantidadTicket_DPE(string ticket, string idcliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cant = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_COUNT_TICKET_DPE", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket.Trim());
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cant = (Convert.ToString(dr["CANTIDAD"]) == "") ? 0 : Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "CantidadTicket_DPE", "USP_COUNT_TICKET_DPE", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return cant;
        }

    }

}
