﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class TipoClienteDAO
    {
        #region "PATRON SINGLETON"
        private static TipoClienteDAO daoTipoCliente = null;
        private TipoClienteDAO() { }
        public static TipoClienteDAO getInstance()
        {
            if (daoTipoCliente == null)
            {
                daoTipoCliente = new TipoClienteDAO();
            }
            return daoTipoCliente;
        }
        #endregion

        public List<TipoCliente> ListarTipo()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<TipoCliente> listaTipo = new List<TipoCliente>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOTIPOCLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    TipoCliente cliente = new TipoCliente();
                    cliente.IdTipo = dr["IdCliente"].ToString();
                    cliente.tipo = dr["Tipo"].ToString();
                    listaTipo.Add(cliente);
                }
            }
            catch(Exception ex)
            {
                Conexion.getInstance().RegistrarError("TipoClienteDAO", "ListarTipo", "USP_LISTADOTIPOCLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }
    }
}
