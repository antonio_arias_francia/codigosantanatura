﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class DepartamentoDAO
    {
        #region "PATRON SINGLETON"
        private static DepartamentoDAO daoDepartamento = null;
        private DepartamentoDAO() { }
        public static DepartamentoDAO getInstance()
        {
            if (daoDepartamento == null)
            {
                daoDepartamento = new DepartamentoDAO();
            }
            return daoDepartamento;
        }
        #endregion

        public List<Departamento> ListaDepartamento(string pais)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Departamento> listaDepartamento = new List<Departamento>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADODEPARTAMENTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PAIS", pais);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Departamento departamento = new Departamento();
                    departamento.Codigo = dr["Codigo"].ToString();
                    departamento.Nombre = dr["Nombre"].ToString();
                    listaDepartamento.Add(departamento);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DepartamentoDAO", "ListaDepartamento", "USP_LISTADODEPARTAMENTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDepartamento;
        }

        public List<Departamento> GetDepartamento()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Departamento> listaDepartamento = new List<Departamento>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GETDEPARTAMENTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Departamento departamento = new Departamento();
                    departamento.Codigo = dr["Codigo"].ToString();
                    departamento.CodigoPais = dr["CodigoPais"].ToString();
                    departamento.Nombre = dr["Nombre"].ToString();
                    listaDepartamento.Add(departamento);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DepartamentoDAO", "GetDepartamento", "USP_GETDEPARTAMENTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDepartamento;
        }
    }
}
