﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Modelos;

namespace Datos
{
    public class ClienteDAO
    {
        #region "PATRON SINGLETON"
        private static ClienteDAO daoCliente = null;
        private ClienteDAO() { }
        public static ClienteDAO getInstance()
        {
            if (daoCliente == null)
            {
                daoCliente = new ClienteDAO();
            }
            return daoCliente;
        }
        #endregion

        string errores = "";

        public void procesoUnilevel(int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_PROCESO_UNILEVEL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@idp", IDP);
                con.Open();
                dr = cmd.ExecuteReader();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public Cliente AccesoSistema(string usuario, string clave)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            Cliente objSocio = null;
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LOGIN", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@user", usuario);
                cmd.Parameters.AddWithValue("@pass", clave);
                conexion.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    //Convert.ToInt32(dr["IdCliente"].ToString());
                    objSocio = new Cliente();
                    objSocio.numeroDoc = dr["NumeroDoc"].ToString().Trim();
                    objSocio.usuario = dr["Usuario"].ToString().Trim();
                    objSocio.idCliente = dr["IdCliente"].ToString().Trim();
                    objSocio.apellidoPat = dr["ApellidoPat"].ToString().Trim();
                    objSocio.clave = dr["Clave"].ToString().Trim();
                    objSocio.nombre = dr["Nombres"].ToString().Trim();
                    objSocio.Packete = dr["IdPackete"].ToString().Trim();
                    objSocio.patrocinador = dr["Patrocinador"].ToString().Trim().Trim();
                    objSocio.PacketePatrocinador = dr["PacketePatrocinador"].ToString().Trim();
                    objSocio.tipoCliente = dr["TipoCliente"].ToString().Trim();
                    objSocio.Establecimiento = dr["EstablecimientoRecojo"].ToString().Trim();
                    objSocio.pais = dr["Pais"].ToString();
                    objSocio.departamento = dr["Departamento"].ToString();
                    objSocio.provincia = dr["Provincia"].ToString();
                    objSocio.fechaNac = dr["FechaNac"].ToString();
                    objSocio.idSimplex = dr["idSimplex"].ToString();
                    objSocio.upline = dr["Upline"].ToString().Trim();
                    objSocio.factorComision = dr["factorComision"].ToString().Trim();
                    objSocio.correo = dr["Correo"].ToString().Trim();
                    objSocio.apellidos_completos = dr["Apellidos_completos"].ToString().Trim();
                    objSocio.direccion = dr["Direccion"].ToString().Trim();
                    objSocio.apodo = dr["Apodo"].ToString();
                    objSocio.IdPeruShop = dr["IdPeruShop"].ToString();
                    objSocio.celular = dr["Celular"].ToString().Trim();
                    objSocio.ruc = dr["Ruc"].ToString().Trim();
                    objSocio.paisTienda = dr["PAISTIENDA"].ToString().Trim();
                    objSocio.PreRegistro = Convert.ToInt32(dr["Filtro_PreRegistro"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "AccesoSistema", "USP_LOGIN", ex.Message);
                objSocio = null;
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return objSocio;
        }

        public bool RegistroCliente(Cliente objCliente, Packete objPackete)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCLIENTE_ADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@USUARIO", objCliente.usuario);
                cmd.Parameters.AddWithValue("@CLAVE", objCliente.clave);
                cmd.Parameters.AddWithValue("@NOMBRES", objCliente.nombre);
                cmd.Parameters.AddWithValue("@APELLIDOPAT", objCliente.apellidoPat);
                cmd.Parameters.AddWithValue("@APELLIDOMAT", objCliente.apellidoMat);
                cmd.Parameters.AddWithValue("@APODO", objCliente.apodo);
                cmd.Parameters.AddWithValue("@FECHANAC", objCliente.fechaNac);
                cmd.Parameters.AddWithValue("@SEXO", objCliente.sexo);
                cmd.Parameters.AddWithValue("@TIPODOC", objCliente.tipoDoc);
                cmd.Parameters.AddWithValue("@NUMERODOC", objCliente.numeroDoc);
                cmd.Parameters.AddWithValue("@CORREO", objCliente.correo);
                cmd.Parameters.AddWithValue("@TELEFONO", objCliente.telefono);
                cmd.Parameters.AddWithValue("@CELULAR", objCliente.celular);
                cmd.Parameters.AddWithValue("@PAIS", objCliente.pais);
                cmd.Parameters.AddWithValue("@DEPARTAMENTO", objCliente.departamento);
                cmd.Parameters.AddWithValue("@PROVINCIA", objCliente.provincia);
                cmd.Parameters.AddWithValue("@DISTRITO", objCliente.ditrito);
                cmd.Parameters.AddWithValue("@REFERENCIA", objCliente.referencia);
                cmd.Parameters.AddWithValue("@DIRECCION", objCliente.direccion);
                cmd.Parameters.AddWithValue("@NROCTADETRACCION", objCliente.nroCtaDetraccion);
                cmd.Parameters.AddWithValue("@RUC", objCliente.ruc);
                cmd.Parameters.AddWithValue("@NOMBREBANCO", objCliente.nombreBanco);
                cmd.Parameters.AddWithValue("@NROCTADEPOSITO", objCliente.nroCtaDeposito);
                cmd.Parameters.AddWithValue("@NROCTAINTERBANCARIA", objCliente.nroCtaInterbancaria);
                cmd.Parameters.AddWithValue("@TIPOCLIENTE", objCliente.tipoCliente);
                cmd.Parameters.AddWithValue("@CODIGO_POSTAL", "");
                cmd.Parameters.AddWithValue("@FILTRO_PREREGISTRO", 0);
                cmd.Parameters.AddWithValue("@CDRPREMIO", "00003");
                cmd.Parameters.AddWithValue("@PAISSTORE", objCliente.paisTienda);
                if (objCliente.tipoEstablecimiento == "" | objCliente.tipoEstablecimiento == "0")
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", objCliente.tipoEstablecimiento);
                }
                if (objCliente.patrocinador == "")
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", objCliente.patrocinador);
                }
                if (objCliente.upline == "")
                {
                    cmd.Parameters.AddWithValue("@UPLINE", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@UPLINE", objCliente.upline);
                }
                if (objCliente.factorComision == "")
                {
                    cmd.Parameters.AddWithValue("@FACTORCOMISION", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@FACTORCOMISION", objCliente.factorComision);
                }
                cmd.Parameters.AddWithValue("@IMAGEN", objCliente.imagen);
                cmd.Parameters.AddWithValue("@FECHAREGISTRO", objCliente.FechaRegistro);
                cmd.Parameters.AddWithValue("@IDPERUSHOP", objCliente.IdPeruShop);
                cmd.Parameters.AddWithValue("@idPackete", objPackete.Codigo);
                cmd.Parameters.AddWithValue("@fechaObtencionPackete", objPackete.fechaDescuento);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "RegistroCliente", "USP_REGISTROCLIENTE_ADMIN", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public string RegistroClienteCarrito(Cliente objCliente, Descuento objDesc)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string idCliente = "";
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCLIENTE_ADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@USUARIO", objCliente.usuario);
                cmd.Parameters.AddWithValue("@CLAVE", objCliente.clave);
                cmd.Parameters.AddWithValue("@NOMBRES", objCliente.nombre);
                cmd.Parameters.AddWithValue("@APELLIDOPAT", objCliente.apellidoPat);
                cmd.Parameters.AddWithValue("@APELLIDOMAT", objCliente.apellidoMat);
                cmd.Parameters.AddWithValue("@APODO", objCliente.apodo);
                cmd.Parameters.AddWithValue("@FECHANAC", objCliente.fechaNac);
                cmd.Parameters.AddWithValue("@SEXO", objCliente.sexo);
                cmd.Parameters.AddWithValue("@TIPODOC", objCliente.tipoDoc.Trim());
                cmd.Parameters.AddWithValue("@NUMERODOC", objCliente.numeroDoc);
                cmd.Parameters.AddWithValue("@CORREO", objCliente.correo);
                cmd.Parameters.AddWithValue("@TELEFONO", objCliente.telefono);
                cmd.Parameters.AddWithValue("@CELULAR", objCliente.celular);
                cmd.Parameters.AddWithValue("@PAIS", objCliente.pais);
                cmd.Parameters.AddWithValue("@DEPARTAMENTO", objCliente.departamento);
                cmd.Parameters.AddWithValue("@PROVINCIA", objCliente.provincia);
                cmd.Parameters.AddWithValue("@REFERENCIA", objCliente.referencia);
                cmd.Parameters.AddWithValue("@DIRECCION", objCliente.direccion);
                cmd.Parameters.AddWithValue("@NROCTADETRACCION", objCliente.nroCtaDetraccion);
                cmd.Parameters.AddWithValue("@RUC", objCliente.ruc);
                cmd.Parameters.AddWithValue("@NOMBREBANCO", objCliente.nombreBanco);
                cmd.Parameters.AddWithValue("@NROCTADEPOSITO", objCliente.nroCtaDeposito);
                cmd.Parameters.AddWithValue("@NROCTAINTERBANCARIA", objCliente.nroCtaInterbancaria);
                cmd.Parameters.AddWithValue("@TIPOCLIENTE", objCliente.tipoCliente);
                cmd.Parameters.AddWithValue("@CODIGO_POSTAL", objCliente.CodigoPostal);
                cmd.Parameters.AddWithValue("@FILTRO_PREREGISTRO", objCliente.PreRegistro);
                cmd.Parameters.AddWithValue("@PAISSTORE", objCliente.paisTienda);

                if (objCliente.ditrito == "" | objCliente.ditrito == "0")
                {
                    cmd.Parameters.AddWithValue("@DISTRITO", "03001");
                }
                else
                {
                    cmd.Parameters.AddWithValue("@DISTRITO", objCliente.ditrito);
                }

                if (objCliente.tipoEstablecimiento == "" | objCliente.tipoEstablecimiento == "0")
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", objCliente.tipoEstablecimiento);
                }

                if (objCliente.CDRPremio == "" | objCliente.CDRPremio == "0")
                {
                    cmd.Parameters.AddWithValue("@CDRPREMIO", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@CDRPREMIO", objCliente.CDRPremio);
                }

                if (objCliente.patrocinador == "")
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", objCliente.patrocinador);
                }
                if (objCliente.upline == "")
                {
                    cmd.Parameters.AddWithValue("@UPLINE", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@UPLINE", objCliente.upline);
                } 
                cmd.Parameters.AddWithValue("@IMAGEN", objCliente.imagen);
                cmd.Parameters.AddWithValue("@FECHAREGISTRO", objCliente.FechaRegistro);
                cmd.Parameters.AddWithValue("@IDPERUSHOP", objCliente.IdPeruShop);
                cmd.Parameters.AddWithValue("@FACTORCOMISION", objCliente.factorComision);
                cmd.Parameters.AddWithValue("@idPackete", objDesc.IdPackete);
                cmd.Parameters.AddWithValue("@fechaObtencionPackete", objDesc.FechaObtencionPackete);
                dr = null;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    idCliente = Convert.ToString(dr["IDCLIENTE"]);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "RegistroClienteCarrito", "USP_REGISTROCLIENTE_ADMIN", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return idCliente;
        }

        public string ValidadCaducidadClave(string usuario)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string mensaje = "";
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ValidarCaducidadClave", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                dr = null;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    mensaje = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidadCaducidadClave", "USP_ValidarCaducidadClave", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return mensaje;
        }

        public string ValidadPreguntasSeguridadUsuario(string usuario)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string mensaje = "";
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ValidarPreguntas_Usuario", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@usuario", usuario);
                dr = null;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    mensaje = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidadPreguntasSeguridadUsuario", "USP_ValidarPreguntas_Usuario", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return mensaje;
        }

        public bool Eliminarcliente(string id, int idpaquete)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINARCLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", id);
                cmd.Parameters.AddWithValue("@IDDESCUENTO", idpaquete);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "Eliminarcliente", "USP_ELIMINARCLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public void Eliminarcliente_DPE(string id)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINARCLIENTE_DPE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", id);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "Eliminarcliente_DPE", "USP_ELIMINARCLIENTE_DPE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public bool ActualizarCliente(Cliente objCliente)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARCLIENTE_ADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", objCliente.idCliente);
                cmd.Parameters.AddWithValue("@NUMERODOC", objCliente.numeroDoc);
                cmd.Parameters.AddWithValue("@USUARIO", objCliente.usuario);
                cmd.Parameters.AddWithValue("@CLAVE", objCliente.clave);
                cmd.Parameters.AddWithValue("@NOMBRES", objCliente.nombre);
                cmd.Parameters.AddWithValue("@APELLIDOPAT", objCliente.apellidoPat);
                cmd.Parameters.AddWithValue("@APELLIDOMAT", objCliente.apellidoMat);
                cmd.Parameters.AddWithValue("@APODO", objCliente.apodo);
                cmd.Parameters.AddWithValue("@FECHANAC", objCliente.fechaNac);
                cmd.Parameters.AddWithValue("@SEXO", objCliente.sexo);
                cmd.Parameters.AddWithValue("@TIPODOC", objCliente.tipoDoc);
                cmd.Parameters.AddWithValue("@CORREO", objCliente.correo);
                cmd.Parameters.AddWithValue("@TELEFONO", objCliente.telefono);
                cmd.Parameters.AddWithValue("@CELULAR", objCliente.celular);
                cmd.Parameters.AddWithValue("@PAIS", objCliente.pais);
                cmd.Parameters.AddWithValue("@DEPARTAMENTO", objCliente.departamento);
                cmd.Parameters.AddWithValue("@PROVINCIA", objCliente.provincia);
                cmd.Parameters.AddWithValue("@DISTRITO", objCliente.ditrito);
                cmd.Parameters.AddWithValue("@DIRECCION", objCliente.direccion);
                cmd.Parameters.AddWithValue("@REFERENCIA", objCliente.referencia);
                cmd.Parameters.AddWithValue("@NROCTADETRACCION", objCliente.nroCtaDetraccion);
                cmd.Parameters.AddWithValue("@RUC", objCliente.ruc);
                cmd.Parameters.AddWithValue("@NOMBREBANCO", objCliente.nombreBanco);
                cmd.Parameters.AddWithValue("@NROCTADEPOSITO", objCliente.nroCtaDeposito);
                cmd.Parameters.AddWithValue("@NROCTAINTERBANCARIA", objCliente.nroCtaInterbancaria);
                cmd.Parameters.AddWithValue("@TIPOCLIENTE", objCliente.tipoCliente);
                cmd.Parameters.AddWithValue("@PAISSTORE", objCliente.paisTienda);
                if (objCliente.tipoEstablecimiento == "" | objCliente.tipoEstablecimiento == null | objCliente.tipoEstablecimiento == "0")
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", objCliente.tipoEstablecimiento);
                }
                if (objCliente.patrocinador == "")
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", objCliente.patrocinador);
                }
                if (objCliente.upline == "")
                {
                    cmd.Parameters.AddWithValue("@UPLINE", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@UPLINE", objCliente.upline);
                }
                if (objCliente.factorComision == "")
                {
                    cmd.Parameters.AddWithValue("@FACTORCOMISION", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@FACTORCOMISION", objCliente.factorComision);
                }
                cmd.Parameters.AddWithValue("@IMAGEN", objCliente.imagen);
                cmd.Parameters.AddWithValue("@IDPERUSHOP", objCliente.IdPeruShop);
                cmd.Parameters.AddWithValue("@IDDESCUENTO", objCliente.IdPackete);
                cmd.Parameters.AddWithValue("@IDPACKETE", objCliente.Packete);
                cmd.Parameters.AddWithValue("@FECHAPACKETE", objCliente.FechaPackete);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarCliente", "USP_ACTUALIZARCLIENTE_ADMIN", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarClienteR(Cliente objCliente)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARCLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", objCliente.idCliente);
                cmd.Parameters.AddWithValue("@NUMERODOC", objCliente.numeroDoc);
                cmd.Parameters.AddWithValue("@USUARIO", objCliente.usuario);
                cmd.Parameters.AddWithValue("@CLAVE", objCliente.clave);
                cmd.Parameters.AddWithValue("@NOMBRES", objCliente.nombre);
                cmd.Parameters.AddWithValue("@APELLIDOPAT", objCliente.apellidoPat);
                cmd.Parameters.AddWithValue("@APELLIDOMAT", objCliente.apellidoMat);
                cmd.Parameters.AddWithValue("@APODO", objCliente.apodo);
                cmd.Parameters.AddWithValue("@FECHANAC", objCliente.fechaNac);
                cmd.Parameters.AddWithValue("@SEXO", objCliente.sexo);
                cmd.Parameters.AddWithValue("@TIPODOC", objCliente.tipoDoc);
                cmd.Parameters.AddWithValue("@CORREO", objCliente.correo);
                cmd.Parameters.AddWithValue("@TELEFONO", objCliente.telefono);
                cmd.Parameters.AddWithValue("@CELULAR", objCliente.celular);
                cmd.Parameters.AddWithValue("@PAIS", objCliente.pais);
                cmd.Parameters.AddWithValue("@DEPARTAMENTO", objCliente.departamento);
                cmd.Parameters.AddWithValue("@PROVINCIA", objCliente.provincia);
                cmd.Parameters.AddWithValue("@DISTRITO", objCliente.ditrito);
                cmd.Parameters.AddWithValue("@DIRECCION", objCliente.direccion);
                cmd.Parameters.AddWithValue("@REFERENCIA", objCliente.referencia);
                cmd.Parameters.AddWithValue("@NROCTADETRACCION", objCliente.nroCtaDetraccion);
                cmd.Parameters.AddWithValue("@RUC", objCliente.ruc);
                cmd.Parameters.AddWithValue("@NOMBREBANCO", objCliente.nombreBanco);
                cmd.Parameters.AddWithValue("@NROCTADEPOSITO", objCliente.nroCtaDeposito);
                cmd.Parameters.AddWithValue("@NROCTAINTERBANCARIA", objCliente.nroCtaInterbancaria);
                cmd.Parameters.AddWithValue("@TIPOCLIENTE", objCliente.tipoCliente);
                if (objCliente.tipoEstablecimiento == "" | objCliente.tipoEstablecimiento == null | objCliente.tipoEstablecimiento == "0")
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", objCliente.tipoEstablecimiento);
                }
                if (objCliente.patrocinador == "")
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@PATROCINADOR", objCliente.patrocinador);
                }
                if (objCliente.upline == "")
                {
                    cmd.Parameters.AddWithValue("@UPLINE", DBNull.Value);

                }
                else
                {
                    cmd.Parameters.AddWithValue("@UPLINE", objCliente.upline);
                }
                cmd.Parameters.AddWithValue("@IMAGEN", objCliente.imagen);
                cmd.Parameters.AddWithValue("@IDPERUSHOP", "");
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarClienteR", "USP_ACTUALIZARCLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarDescuento(string idCliente, string packete)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEDESCUENTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPAQUETE", packete);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idCliente);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarDescuento", "USP_UPDATEDESCUENTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente> ListaEstablecimiento()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente> listaTipo = new List<Cliente>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOTIPOESTABLECIMIENTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.IdPeruShop = dr["IdPeruShop"].ToString().Trim();
                    cliente.apodo = dr["Apodo"].ToString();
                    cliente.numeroDoc = dr["NumeroDoc"].ToString();
                    listaTipo.Add(cliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListaEstablecimiento", "USP_LISTADOTIPOESTABLECIMIENTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }

        public List<Cliente> ListaEstablecimientoColombia()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente> listaTipo = new List<Cliente>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("[mundosan_final].[USP_LISTADOTIPOESTABLECIMIENTO_MULTIPLE]", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.IdPeruShop = dr["IdPeruShop"].ToString().Trim();
                    cliente.apodo = dr["Apodo"].ToString();
                    listaTipo.Add(cliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListaEstablecimientoColombia", "[mundosan_final].[USP_LISTADOTIPOESTABLECIMIENTO_MULTIPLE]", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }

        public List<Cliente> ListaEstable()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente> listaTipo = new List<Cliente>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOTIPOESTABLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.IdPeruShop = dr["IdPeruShop"].ToString().Trim();
                    cliente.apodo = dr["Apodo"].ToString();
                    cliente.numeroDoc = dr["NumeroDoc"].ToString();
                    listaTipo.Add(cliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListaEstable", "USP_LISTADOTIPOESTABLE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaTipo;
        }

        public List<Cliente> ListarCliente()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOCLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.idCliente = dr["ID"].ToString();
                    objCliente.usuario = dr["USUARIO"].ToString();
                    objCliente.clave = dr["CLAVE"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.apellidoPat = dr["APELLIDOPAT"].ToString();
                    objCliente.apellidoMat = dr["APELLIDOMAT"].ToString();
                    objCliente.apodo = dr["APODO"].ToString();
                    objCliente.fechaNac = dr["FECHANAC"].ToString();
                    objCliente.sexo = dr["SEXO"].ToString();
                    objCliente.tipoDoc = dr["TIPODOC"].ToString();
                    objCliente.numeroDoc = dr["NUMERODOC"].ToString();
                    objCliente.tipoCliente = dr["TIPOC"].ToString();
                    objCliente.idtipoCliente = dr["IDTIPOC"].ToString();
                    objCliente.patrocinador = dr["PATRO"].ToString();
                    objCliente.idpatrocinador = dr["IDPATRO"].ToString();
                    objCliente.upline = dr["UPLINE"].ToString();
                    objCliente.idupline = dr["IDUPLINE"].ToString();
                    objCliente.correo = dr["CORREO"].ToString();
                    objCliente.telefono = dr["TELEFONO"].ToString();
                    objCliente.celular = dr["CELULAR"].ToString();
                    objCliente.pais = dr["PAIS"].ToString();
                    objCliente.idpais = dr["IDPAIS"].ToString();
                    objCliente.departamento = dr["DEPARTAMENTO"].ToString();
                    objCliente.iddepartamento = dr["IDDEPARTAMENTO"].ToString();
                    objCliente.provincia = dr["PROVINCIA"].ToString();
                    objCliente.idprovincia = dr["IDPROVINCIA"].ToString();
                    objCliente.ditrito = dr["DISTRITO"].ToString();
                    objCliente.idditrito = dr["IDDISTRITO"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.referencia = dr["REFERENCIA"].ToString();
                    objCliente.nroCtaDeposito = dr["DEPOSITO"].ToString();
                    objCliente.nroCtaDetraccion = dr["DETRACCION"].ToString();
                    objCliente.nroCtaInterbancaria = dr["INTERBANCARIA"].ToString();
                    objCliente.ruc = dr["RUC"].ToString();
                    objCliente.nombreBanco = dr["BANCO"].ToString();
                    objCliente.tipoEstablecimiento = dr["APODO"].ToString();
                    objCliente.idtipoEstablecimiento = dr["IDAPODO"].ToString();
                    objCliente.imagen = dr["IMAGEN"].ToString().Trim();
                    objCliente.IdPeruShop = dr["IDPERUSHOP"].ToString();
                    objCliente.Packete = dr["PAQUETE"].ToString();
                    objCliente.IdPackete = Convert.ToInt32(dr["IDDESCUENTO"].ToString());
                    objCliente.Establecimiento = dr["ESTABLERECOJO"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarCliente", "USP_LISTADOCLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarCliente100()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOCLIENTETOP100", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.idCliente = dr["ID"].ToString();
                    objCliente.usuario = dr["USUARIO"].ToString();
                    objCliente.clave = dr["CLAVE"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.apellidoPat = dr["APELLIDOPAT"].ToString();
                    objCliente.apellidoMat = dr["APELLIDOMAT"].ToString();
                    objCliente.apodo = dr["APODO"].ToString();
                    objCliente.fechaNac = dr["FECHANAC"].ToString();
                    objCliente.sexo = dr["SEXO"].ToString();
                    objCliente.tipoDoc = dr["TIPODOC"].ToString();
                    objCliente.numeroDoc = dr["NUMERODOC"].ToString();
                    objCliente.tipoCliente = dr["TIPOC"].ToString();
                    objCliente.idtipoCliente = dr["IDTIPOC"].ToString();
                    objCliente.patrocinador = dr["PATRO"].ToString();
                    objCliente.idpatrocinador = dr["IDPATRO"].ToString();
                    objCliente.upline = dr["UPLINE"].ToString();
                    objCliente.idupline = dr["IDUPLINE"].ToString();
                    objCliente.correo = dr["CORREO"].ToString();
                    objCliente.telefono = dr["TELEFONO"].ToString();
                    objCliente.celular = dr["CELULAR"].ToString();
                    objCliente.pais = dr["PAIS"].ToString();
                    objCliente.idpais = dr["IDPAIS"].ToString();
                    objCliente.departamento = dr["DEPARTAMENTO"].ToString();
                    objCliente.iddepartamento = dr["IDDEPARTAMENTO"].ToString();
                    objCliente.provincia = dr["PROVINCIA"].ToString();
                    objCliente.idprovincia = dr["IDPROVINCIA"].ToString();
                    objCliente.ditrito = dr["DISTRITO"].ToString();
                    objCliente.idditrito = dr["IDDISTRITO"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.referencia = dr["REFERENCIA"].ToString();
                    objCliente.nroCtaDeposito = dr["DEPOSITO"].ToString();
                    objCliente.nroCtaDetraccion = dr["DETRACCION"].ToString();
                    objCliente.nroCtaInterbancaria = dr["INTERBANCARIA"].ToString();
                    objCliente.ruc = dr["RUC"].ToString();
                    objCliente.nombreBanco = dr["BANCO"].ToString();
                    objCliente.tipoEstablecimiento = dr["APODO"].ToString();
                    objCliente.idtipoEstablecimiento = dr["IDAPODO"].ToString();
                    objCliente.imagen = dr["IMAGEN"].ToString().Trim();
                    objCliente.IdPeruShop = dr["IDPERUSHOP"].ToString();
                    objCliente.Packete = dr["PAQUETE"].ToString();
                    objCliente.IdPackete = Convert.ToInt32(dr["IDDESCUENTO"].ToString());
                    objCliente.Establecimiento = dr["ESTABLERECOJO"].ToString();
                    objCliente.paisTienda = dr["PAISTIENDA"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarCliente100", "USP_LISTADOCLIENTETOP100", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteFiltro(string dni, string nombres)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOCLIENTEFILTRADO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);
                cmd.Parameters.AddWithValue("@NOMBRES", nombres);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.idCliente = dr["ID"].ToString();
                    objCliente.usuario = dr["USUARIO"].ToString();
                    objCliente.clave = dr["CLAVE"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.apellidoPat = dr["APELLIDOPAT"].ToString();
                    objCliente.apellidoMat = dr["APELLIDOMAT"].ToString();
                    objCliente.apodo = dr["APODO"].ToString();
                    objCliente.fechaNac = dr["FECHANAC"].ToString();
                    objCliente.sexo = dr["SEXO"].ToString();
                    objCliente.tipoDoc = dr["TIPODOC"].ToString();
                    objCliente.numeroDoc = dr["NUMERODOC"].ToString();
                    objCliente.tipoCliente = dr["TIPOC"].ToString();
                    objCliente.idtipoCliente = dr["IDTIPOC"].ToString();
                    objCliente.patrocinador = dr["PATRO"].ToString();
                    objCliente.idpatrocinador = dr["IDPATRO"].ToString();
                    objCliente.upline = dr["UPLINE"].ToString();
                    objCliente.idupline = dr["IDUPLINE"].ToString();
                    objCliente.correo = dr["CORREO"].ToString();
                    objCliente.telefono = dr["TELEFONO"].ToString();
                    objCliente.celular = dr["CELULAR"].ToString();
                    objCliente.pais = dr["PAIS"].ToString();
                    objCliente.idpais = dr["IDPAIS"].ToString();
                    objCliente.departamento = dr["DEPARTAMENTO"].ToString();
                    objCliente.iddepartamento = dr["IDDEPARTAMENTO"].ToString();
                    objCliente.provincia = dr["PROVINCIA"].ToString();
                    objCliente.idprovincia = dr["IDPROVINCIA"].ToString();
                    objCliente.ditrito = dr["DISTRITO"].ToString();
                    objCliente.idditrito = dr["IDDISTRITO"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.referencia = dr["REFERENCIA"].ToString();
                    objCliente.nroCtaDeposito = dr["DEPOSITO"].ToString();
                    objCliente.nroCtaDetraccion = dr["DETRACCION"].ToString();
                    objCliente.nroCtaInterbancaria = dr["INTERBANCARIA"].ToString();
                    objCliente.ruc = dr["RUC"].ToString();
                    objCliente.nombreBanco = dr["BANCO"].ToString();
                    objCliente.tipoEstablecimiento = dr["APODO"].ToString();
                    objCliente.idtipoEstablecimiento = dr["IDAPODO"].ToString();
                    objCliente.imagen = dr["IMAGEN"].ToString().Trim();
                    objCliente.IdPeruShop = dr["IDPERUSHOP"].ToString();
                    objCliente.Packete = dr["PAQUETE"].ToString();
                    objCliente.IdPackete = Convert.ToInt32(dr["IDDESCUENTO"].ToString());
                    objCliente.Establecimiento = dr["ESTABLERECOJO"].ToString();
                    objCliente.paisTienda = dr["PaisTienda"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteFiltro", "USP_LISTADOCLIENTEFILTRADO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteByDocumento(string documento)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GetClienteByNumDocumento", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numDocumento", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.idCliente = dr["ID"].ToString();
                    objCliente.usuario = dr["USUARIO"].ToString();
                    objCliente.clave = dr["CLAVE"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.apellidoPat = dr["APELLIDOPAT"].ToString();
                    objCliente.apellidoMat = dr["APELLIDOMAT"].ToString();
                    objCliente.apodo = dr["APODO"].ToString();
                    objCliente.fechaNac = dr["FECHANAC"].ToString();
                    objCliente.sexo = dr["SEXO"].ToString();
                    objCliente.tipoDoc = dr["TIPODOC"].ToString();
                    objCliente.numeroDoc = dr["NUMERODOC"].ToString();
                    objCliente.tipoCliente = dr["TIPOC"].ToString();
                    objCliente.idtipoCliente = dr["IDTIPOC"].ToString();
                    objCliente.patrocinador = dr["PATRO"].ToString();
                    objCliente.idpatrocinador = dr["IDPATRO"].ToString();
                    objCliente.upline = dr["UPLINE"].ToString();
                    objCliente.idupline = dr["IDUPLINE"].ToString();
                    objCliente.correo = dr["CORREO"].ToString();
                    objCliente.telefono = dr["TELEFONO"].ToString();
                    objCliente.celular = dr["CELULAR"].ToString();
                    objCliente.pais = dr["PAIS"].ToString();
                    objCliente.idpais = dr["IDPAIS"].ToString();
                    objCliente.departamento = dr["DEPARTAMENTO"].ToString();
                    objCliente.iddepartamento = dr["IDDEPARTAMENTO"].ToString();
                    objCliente.provincia = dr["PROVINCIA"].ToString();
                    objCliente.idprovincia = dr["IDPROVINCIA"].ToString();
                    objCliente.ditrito = dr["DISTRITO"].ToString();
                    objCliente.idditrito = dr["IDDISTRITO"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.referencia = dr["REFERENCIA"].ToString();
                    objCliente.nroCtaDeposito = dr["DEPOSITO"].ToString();
                    objCliente.nroCtaDetraccion = dr["DETRACCION"].ToString();
                    objCliente.nroCtaInterbancaria = dr["INTERBANCARIA"].ToString();
                    objCliente.ruc = dr["RUC"].ToString();
                    objCliente.nombreBanco = dr["BANCO"].ToString();
                    objCliente.tipoEstablecimiento = dr["APODO"].ToString();
                    objCliente.idtipoEstablecimiento = dr["IDAPODO"].ToString();
                    objCliente.imagen = dr["IMAGEN"].ToString();
                    objCliente.idtipoEstablecimientoPremio = dr["CDRPREMIO"].ToString().Trim();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteByDocumento", "USP_GetClienteByNumDocumento", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> DNIEXISTE()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente> listaCliente = new List<Cliente>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DNIEXISTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente dni = new Cliente();
                    dni.numeroDoc = dr["NumeroDoc"].ToString();
                    dni.usuario = dr["Usuario"].ToString();
                    listaCliente.Add(dni);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "DNIEXISTE", "USP_DNIEXISTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCliente;
        }

        public bool ActualizarEstadoCliente(string numDocCliente, bool estado)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ActualizarEstadoCliente", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numDocuCliente", numDocCliente);
                cmd.Parameters.AddWithValue("@estado", estado);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarEstadoCliente", "SP_ActualizarEstadoCliente", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public Cliente GetClienteByNumDocumento(string numDocumento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            Cliente objCliente = null;
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GetClienteByNumDocumento", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numDocumento", numDocumento);

                conexion.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    objCliente = new Cliente();
                    objCliente.idCliente = dr["ID"].ToString();
                    objCliente.usuario = dr["USUARIO"].ToString();
                    objCliente.clave = dr["CLAVE"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.apellidoPat = dr["APELLIDOPAT"].ToString();
                    objCliente.apellidoMat = dr["APELLIDOMAT"].ToString();
                    objCliente.apodo = dr["APODO"].ToString();
                    objCliente.fechaNac = dr["FECHANAC"].ToString();
                    objCliente.sexo = dr["SEXO"].ToString();
                    objCliente.tipoDoc = dr["TIPODOC"].ToString();
                    objCliente.numeroDoc = dr["NUMERODOC"].ToString();
                    objCliente.tipoCliente = dr["TIPOC"].ToString();
                    objCliente.idtipoCliente = dr["IDTIPOC"].ToString();
                    objCliente.patrocinador = dr["PATRO"].ToString();
                    objCliente.idpatrocinador = dr["IDPATRO"].ToString();
                    objCliente.upline = dr["UPLINE"].ToString();
                    objCliente.idupline = dr["IDUPLINE"].ToString();
                    objCliente.correo = dr["CORREO"].ToString();
                    objCliente.telefono = dr["TELEFONO"].ToString();
                    objCliente.celular = dr["CELULAR"].ToString();
                    objCliente.pais = dr["PAIS"].ToString();
                    objCliente.idpais = dr["IDPAIS"].ToString();
                    objCliente.departamento = dr["DEPARTAMENTO"].ToString();
                    objCliente.iddepartamento = dr["IDDEPARTAMENTO"].ToString();
                    objCliente.provincia = dr["PROVINCIA"].ToString();
                    objCliente.idprovincia = dr["IDPROVINCIA"].ToString();
                    objCliente.ditrito = dr["DISTRITO"].ToString();
                    objCliente.idditrito = dr["IDDISTRITO"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.referencia = dr["REFERENCIA"].ToString();
                    objCliente.nroCtaDeposito = dr["DEPOSITO"].ToString();
                    objCliente.nroCtaDetraccion = dr["DETRACCION"].ToString();
                    objCliente.nroCtaInterbancaria = dr["INTERBANCARIA"].ToString();
                    objCliente.ruc = dr["RUC"].ToString();
                    objCliente.nombreBanco = dr["BANCO"].ToString();
                    objCliente.tipoEstablecimiento = dr["APODO"].ToString();
                    objCliente.idtipoEstablecimiento = dr["IDAPODO"].ToString();
                    objCliente.imagen = dr["IMAGEN"].ToString();
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "GetClienteByNumDocumento", "USP_GetClienteByNumDocumento", ex.Message);
                objCliente = null;
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return objCliente;
        } 

        public List<Cliente> ListarUpline(string numDocumento)
        {
            string errores = "";
            List<Cliente> ListaUpline = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarUpLine", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numeroDoc", numDocumento);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.numeroDoc = dr["NumeroDoc"].ToString();
                    cliente.nombre = dr["Nombre"].ToString();
                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarUpline", "USP_ListarUpLine", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public List<Cliente> ListarUplinePreRegistro(string numDocumento)
        {
            string errores = "";
            List<Cliente> ListaUpline = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTARUPLINE_PREREGISTRO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numeroDoc", numDocumento);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.numeroDoc = dr["NumeroDoc"].ToString();
                    cliente.nombre = dr["Nombre"].ToString();
                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarUplinePreRegistro", "USP_LISTARUPLINE_PREREGISTRO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public List<string> AutocompletePatro(string palabra)
        {
            List<string> listaNombrePatro = new List<string>();

            using (SqlConnection con = Conexion.getInstance().Conectar())
            {
                SqlCommand cmd = new SqlCommand("USP_ATUCOMPLETEPATRO", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter()
                {
                    ParameterName = "@palabra",
                    Value = palabra
                };
                cmd.Parameters.Add(parametro);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    listaNombrePatro.Add(dr["NOMBRES"].ToString().Trim());
                    listaNombrePatro.Add(dr["DNI"].ToString().Trim());
                }
                return listaNombrePatro;
            }
        }

        public bool RegistrarAfiliacion(Cliente objCliente, Descuento descuento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlCommand cmd2 = null;
            bool response = false;
            int resultado = 0;
            int resultado2 = 0;
            int resultado3 = 0;

            con = Conexion.getInstance().Conectar();
            con.Open();

            SqlTransaction trans = con.BeginTransaction();

            cmd = new SqlCommand("USP_REGISTROCLIENTE", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@USUARIO", objCliente.usuario);
            cmd.Parameters.AddWithValue("@CLAVE", objCliente.clave);
            cmd.Parameters.AddWithValue("@NOMBRES", objCliente.nombre);
            cmd.Parameters.AddWithValue("@APELLIDOPAT", objCliente.apellidoPat);
            cmd.Parameters.AddWithValue("@APELLIDOMAT", objCliente.apellidoMat);
            cmd.Parameters.AddWithValue("@APODO", objCliente.apodo);
            cmd.Parameters.AddWithValue("@FECHANAC", objCliente.fechaNac);
            cmd.Parameters.AddWithValue("@SEXO", objCliente.sexo);
            cmd.Parameters.AddWithValue("@TIPODOC", objCliente.tipoDoc);
            cmd.Parameters.AddWithValue("@NUMERODOC", objCliente.numeroDoc);
            cmd.Parameters.AddWithValue("@CORREO", objCliente.correo);
            cmd.Parameters.AddWithValue("@TELEFONO", objCliente.telefono);
            cmd.Parameters.AddWithValue("@CELULAR", objCliente.celular);
            cmd.Parameters.AddWithValue("@PAIS", objCliente.pais);
            cmd.Parameters.AddWithValue("@DEPARTAMENTO", objCliente.departamento);
            cmd.Parameters.AddWithValue("@PROVINCIA", objCliente.provincia);
            cmd.Parameters.AddWithValue("@DISTRITO", objCliente.ditrito);
            cmd.Parameters.AddWithValue("@REFERENCIA", objCliente.referencia);
            cmd.Parameters.AddWithValue("@DIRECCION", objCliente.direccion);
            cmd.Parameters.AddWithValue("@NROCTADETRACCION", objCliente.nroCtaDetraccion);
            cmd.Parameters.AddWithValue("@RUC", objCliente.ruc);
            cmd.Parameters.AddWithValue("@NOMBREBANCO", objCliente.nombreBanco);
            cmd.Parameters.AddWithValue("@NROCTADEPOSITO", objCliente.nroCtaDeposito);
            cmd.Parameters.AddWithValue("@NROCTAINTERBANCARIA", objCliente.nroCtaInterbancaria);
            cmd.Parameters.AddWithValue("@TIPOCLIENTE", objCliente.tipoCliente);
            if (objCliente.tipoEstablecimiento == "" | objCliente.tipoEstablecimiento == null | objCliente.tipoEstablecimiento == "0")
            {
                cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ESTABLECIMIENTORECOJO", objCliente.tipoEstablecimiento);
            }
            if (objCliente.patrocinador == "")
            {
                cmd.Parameters.AddWithValue("@PATROCINADOR", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@PATROCINADOR", objCliente.patrocinador);
            }
            if (objCliente.upline == "")
            {
                cmd.Parameters.AddWithValue("@UPLINE", DBNull.Value);

            }
            else
            {
                cmd.Parameters.AddWithValue("@UPLINE", objCliente.upline);
            }
            cmd.Parameters.AddWithValue("@IMAGEN", objCliente.imagen);
            cmd.Parameters.AddWithValue("@FECHAREGISTRO", objCliente.FechaRegistro);
            cmd.Parameters.AddWithValue("@IDPERUSHOP", objCliente.IdPeruShop);


            try
            {
                cmd.Transaction = trans;
                resultado2 = cmd.ExecuteNonQuery();

                cmd2 = new SqlCommand("USP_RegistrarPacketeCliente", con);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@numDocumento", descuento.NumDocumentoCliente);
                cmd2.Parameters.AddWithValue("@idPackete ", descuento.IdPackete);
                cmd2.Parameters.AddWithValue("@fechaObtencionPackete", Convert.ToString(descuento.FechaObtencionPackete));

                cmd2.Transaction = trans;
                cmd2.CommandTimeout = 90000;
                resultado3 = cmd2.ExecuteNonQuery();

                resultado = resultado2 + resultado3;

                if (resultado > 0)
                {
                    response = true;
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Conexion.getInstance().RegistrarError("ClienteDAO", "RegistrarAfiliacion", "USP_REGISTROCLIENTE", ex.Message);
                response = false;
                throw ex;

            }

            finally
            {
                con.Close();
            }

            return response;
        }

        public Cliente ValidarPatrocinador(string nombres)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            Cliente objCliente = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VALIDARPATRO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@palabra", nombres);
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DNI"].ToString();
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarPatrocinador", "USP_VALIDARPATRO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objCliente;
        }

        public Cliente DatosClienteSimplex(string id)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            Cliente objCliente = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTASIMPLEX", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", id);
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    objCliente = new Cliente();
                    objCliente.patrocinador = dr["Patrocinador"].ToString();
                    objCliente.Packete = dr["IdPackete"].ToString();
                    objCliente.upline = dr["Upline"].ToString();
                    objCliente.factorComision = dr["factorComision"].ToString();
                    objCliente.tipoCliente = dr["TipoCliente"].ToString();
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "DatosClienteSimplex", "USP_LISTASIMPLEX", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objCliente;
        }

        public DataTable EstructuraRedGeneral(string documento)
        {

            SqlConnection con = null;
            SqlCommand cmd = null;
            DataTable dataTable = new DataTable();
            SqlDataAdapter da = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTRUCTURARED", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "EstructuraRedGeneral", "USP_ESTRUCTURARED", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
                da.Dispose();
            }
            return dataTable;

        }

        public MomentaneoComi ComisionMomentanea(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            MomentaneoComi objSocio = null;
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTACOMI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                conexion.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    objSocio = new MomentaneoComi();
                    objSocio.rango = dr["RANGO"].ToString();
                    objSocio.monto = Convert.ToDouble(dr["MONTO"]);
                    objSocio.unilevel = Convert.ToDouble(dr["unilevel"]);
                    objSocio.afiliacion = Convert.ToDouble(dr["afiliacion"]);
                    objSocio.tiburon = Convert.ToDouble(dr["tiburon"]);
                    objSocio.bronce = Convert.ToDouble(dr["bronce"]);
                    objSocio.ci = Convert.ToDouble(dr["ci"]);
                    objSocio.con = Convert.ToDouble(dr["con"]);
                    objSocio.escolaridad = Convert.ToDouble(dr["escolaridad"]);
                    objSocio.mercadeo = Convert.ToDouble(dr["mercadeo"]);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ComisionMomentanea", "USP_LISTACOMI", ex.Message);
                objSocio = null;
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return objSocio;
        }

        public DataTable EstructuraRedGeneralCI(string documento)
        {

            SqlConnection con = null;
            SqlCommand cmd = null;
            DataTable dataTable = new DataTable();
            SqlDataAdapter da = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTRUCTURACI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "EstructuraRedGeneralCI", "USP_ESTRUCTURACI", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
                da.Dispose();
            }
            return dataTable;

        }

        public DataTable EstructuraRedGeneralConsultor(string documento)
        {

            SqlConnection con = null;
            SqlCommand cmd = null;
            DataTable dataTable = new DataTable();
            SqlDataAdapter da = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTRUCTURACONSULTOR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                da = new SqlDataAdapter(cmd);
                da.Fill(dataTable);

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "EstructuraRedGeneralConsultor", "USP_ESTRUCTURACONSULTOR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
                da.Dispose();
            }
            return dataTable;

        }

        public List<Cliente> ListarClienteRango()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTASOCIOXRANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    if (Convert.ToString(dr["RANGO"]) == null)
                    {
                        objCliente.rango = "";
                    }
                    else { objCliente.rango = dr["RANGO"].ToString(); }
                    objCliente.comision = 0;
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRango", "USP_LISTASOCIOXRANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClientePeriodoVP()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTASOCIOXPERIODOACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DOCUMENTO"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClientePeriodoVP", "USP_LISTASOCIOXPERIODOACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente.RecalculoPuntos> ListarClienteRecalculoPuntos(int IDP)
        {
            List<Cliente.RecalculoPuntos> Lista = new List<Cliente.RecalculoPuntos>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTASOCIO_RECALCULOPUNTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.RecalculoPuntos objCliente = new Cliente.RecalculoPuntos();
                    objCliente.PuntosReales = Convert.ToDouble(dr["PUNTOSREALES"].ToString());
                    objCliente.PuntosPromo = Convert.ToDouble(dr["PUNTOSPROMO"].ToString());
                    objCliente.Documento = dr["DOCUMENTO"].ToString();
                    objCliente.Factor = dr["FACTOR"].ToString();
                    objCliente.Patrocinador = dr["PATRO"].ToString();
                    objCliente.Paquete = dr["PAQUETE"].ToString();
                    objCliente.IDP_PP = Convert.ToInt32(dr["IDP"]);
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRecalculoPuntos", "USP_LISTASOCIO_RECALCULOPUNTOS_PRUEBA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteRedVQ(string documento)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAREDVQ", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.VPR = Convert.ToDouble(dr["VPR"].ToString());
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.VP = Convert.ToDouble(dr["VP"].ToString());
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRedVQ", "USP_LISTAREDVQ", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        } 

        public List<Cliente> ListarClienteRedVQPeriodo(string documento, int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAREDVQPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.VPR = Convert.ToDouble(dr["VPR"].ToString());
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.VP = Convert.ToDouble(dr["VP"].ToString());
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRedVQPeriodo", "USP_LISTAREDVQPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }
        
        public List<Cliente> ListarClienteRedCalculoPuntos()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAREDCALCULOPUNTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.VPR = Convert.ToDouble(dr["VPR"].ToString());
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.VP = Convert.ToDouble(dr["VP"].ToString());
                    if (Convert.ToString(dr["RANGO"]) == null)
                    { objCliente.rango = "SIN RANGO"; }
                    else { objCliente.rango = dr["RANGO"].ToString(); }
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRedCalculoPuntos", "USP_LISTAREDCALCULOPUNTOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteRedCalculoPuntosPeriodo(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAREDCALCULOPUNTOSPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.VPR = Convert.ToDouble(dr["VPR"].ToString());
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.VP = Convert.ToDouble(dr["VP"].ToString());
                    if (Convert.ToString(dr["RANGO"]) == null)
                    { objCliente.rango = "SIN RANGO"; }
                    else { objCliente.rango = dr["RANGO"].ToString(); }
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRedCalculoPuntosPeriodo", "USP_LISTAREDCALCULOPUNTOSPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public ComAfiliacion ListaDatosPatrocinioAfiliaciones(int IDP, string dni)
        {
            ComAfiliacion objCliente = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOSAPTROCINIO_AFILIACIONES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objCliente = new ComAfiliacion();
                    objCliente.documento = dr["NumeroDoc"].ToString().Trim();
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.paquete = Convert.ToString(dr["IdPackete"].ToString());
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListaDatosPatrocinioAfiliaciones", "USP_LISTA_DATOSAPTROCINIO_AFILIACIONES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objCliente;
        }

        public List<Cliente> ListarDirectosUpline(string documento)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADIRECTOSUPLINE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarDirectosUpline", "USP_LISTADIRECTOSUPLINE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClientePeriodo()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTASOCIOSCALCULOPUNTAJE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.idCliente = dr["IdCliente"].ToString();
                    objCliente.idClienteUpline = dr["IDU"].ToString();
                    objCliente.upline = dr["factorComision"].ToString();
                    objCliente.tipoCliente = dr["TipoCliente"].ToString();
                    objCliente.patrocinador = dr["Patrocinador"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClientePeriodo", "USP_LISTASOCIOSCALCULOPUNTAJE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string ListarFactorComision(string documento)
        {
            List<Cliente> listaFC = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string DOC = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAFACTORCOMISION", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DOC = Convert.ToString(dr["factorComision"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarFactorComision", "USP_LISTAFACTORCOMISION", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return DOC;
        }

        public bool ActualizarPerfilSocio(Cliente objCliente)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEPERFILSOCIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOCIO", objCliente.idCliente);
                cmd.Parameters.AddWithValue("@USUARIO", objCliente.usuario);
                cmd.Parameters.AddWithValue("@CLAVE", objCliente.clave);
                cmd.Parameters.AddWithValue("@NOMBRE", objCliente.nombre);
                cmd.Parameters.AddWithValue("@APEPAT", objCliente.apellidoPat);
                cmd.Parameters.AddWithValue("@APEMAT", objCliente.apellidoMat);
                cmd.Parameters.AddWithValue("@NACIMIENTO", objCliente.fechaNac);
                cmd.Parameters.AddWithValue("@SEXO", objCliente.sexo);
                cmd.Parameters.AddWithValue("@TIPODOC", objCliente.tipoDoc);
                cmd.Parameters.AddWithValue("@DOCUMENTO", objCliente.numeroDoc);
                cmd.Parameters.AddWithValue("@DIRECCION", objCliente.direccion);
                cmd.Parameters.AddWithValue("@REFERENCIA", objCliente.referencia);
                cmd.Parameters.AddWithValue("@TELEFONO", objCliente.telefono);
                cmd.Parameters.AddWithValue("@CELULAR", objCliente.celular);
                cmd.Parameters.AddWithValue("@PAIS", objCliente.pais);
                cmd.Parameters.AddWithValue("@DEPARTAMENTO", objCliente.departamento);
                cmd.Parameters.AddWithValue("@PROVINCIA", objCliente.provincia);
                cmd.Parameters.AddWithValue("@DISTRITO", objCliente.ditrito);
                cmd.Parameters.AddWithValue("@CORREO", objCliente.correo);
                cmd.Parameters.AddWithValue("@RUC", objCliente.ruc);
                cmd.Parameters.AddWithValue("@INTERBANCARIA", objCliente.nroCtaInterbancaria);
                cmd.Parameters.AddWithValue("@BANCO", objCliente.nombreBanco);
                cmd.Parameters.AddWithValue("@DEPOSITO", objCliente.nroCtaDeposito);
                cmd.Parameters.AddWithValue("@DETRACCIONES", objCliente.nroCtaDetraccion);
                cmd.Parameters.AddWithValue("@IMAGEN", objCliente.imagen);
                if (objCliente.tipoEstablecimiento == "" | objCliente.tipoEstablecimiento == null | objCliente.tipoEstablecimiento == "0")
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTO", DBNull.Value);
                }else
                {
                    cmd.Parameters.AddWithValue("@ESTABLECIMIENTO", objCliente.tipoEstablecimiento);
                }
                if (objCliente.idtipoEstablecimientoPremio == "" | objCliente.idtipoEstablecimientoPremio == null | objCliente.idtipoEstablecimientoPremio == "0")
                {
                    cmd.Parameters.AddWithValue("@CDPREMIO", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@CDPREMIO", objCliente.idtipoEstablecimientoPremio);
                }
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarPerfilSocio", "USP_UPDATEPERFILSOCIO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<Cliente> ListarDatosIndex(string documento)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_PUNTAJEPERSONAL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.VQ = Convert.ToDouble(dr["VQ"].ToString());
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.VP = Convert.ToDouble(dr["VP"].ToString());
                    objCliente.VG = Convert.ToDouble(dr["VG"].ToString());
                    objCliente.VIP = Convert.ToDouble(dr["VIP"].ToString());
                    objCliente.rango = Convert.ToString(dr["RANGO"].ToString());
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarDatosIndex", "USP_PUNTAJEPERSONAL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarDatosRangoProximo(string documento)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_PROGRESO_NUEVO_RANGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.VQProximo = Convert.ToString(dr["PUNTAJERANGO"].ToString());
                    objCliente.rangoProximo = Convert.ToString(dr["NOMBRE"].ToString());
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarDatosRangoProximo", "USP_PROGRESO_NUEVO_RANGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string SociosActivosIndex(string documento)
        {
            List<Cliente> listaFC = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string DOC = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SOCIOSACTIVOSPERSONAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DOC = Convert.ToString(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "SociosActivosIndex", "USP_SOCIOSACTIVOSPERSONAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return DOC;
        }

        public string DirectosActivosIndex(string documento)
        {
            List<Cliente> listaFC = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string DOC = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DIRECTOSACTIVOSPERSONAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DOC = Convert.ToString(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "DirectosActivosIndex", "USP_DIRECTOSACTIVOSPERSONAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return DOC;
        }

        public string ObtenerFechaRegistro(string documento)
        {
            List<Cliente> listaFC = new List<Cliente>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string fecha = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENERFECHA_REGISTRO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    fecha = Convert.ToString(dr["FechaRegistro"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ObtenerFechaRegistro", "USP_OBTENERFECHA_REGISTRO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return fecha;
        }

        public int CantidadAfiliacionesGeneradas(string documento, string fechaInicio, string fechaFin)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_AFILI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@INICIO", fechaInicio.Trim());
                cmd.Parameters.AddWithValue("@FIN", fechaFin.Trim());
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cantidad = Convert.ToInt32(dr["Cantidad"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantidadAfiliacionesGeneradas", "USP_OBTENER_CANT_AFILI", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return cantidad;
        }

        public bool ActualizarEstadoTiburon(string dni, int tiburon)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATECALIFICACION_TIBURON", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@TIBURON", tiburon);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarEstadoTiburon", "USP_UPDATECALIFICACION_TIBURON", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarEstadoTiburonRecalculo(string dni, int tiburon, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATECALIFICACION_TIBURON_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@TIBURON", tiburon);
                cmd.Parameters.AddWithValue("@IDP", IDP);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarEstadoTiburonRecalculo", "USP_UPDATECALIFICACION_TIBURON_RECALCULO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarEvalTiburonFec(string dni, int tiburon, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_EVAL_TIBURON_FEC", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@TIBURON", tiburon);
                cmd.Parameters.AddWithValue("@IDP", IDP);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarEvalTiburonFec", "USP_UPDATE_EVAL_TIBURON_FEC", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Cliente> ListarClienteTiburon(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAR_CLIENTES_FECREGISTRO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DOCUMENTO"].ToString();
                    objCliente.FechaRegistro = dr["FechaRegistro"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteTiburon", "USP_LISTAR_CLIENTES_FECREGISTRO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteConCompras()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_CONCOMPRAS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString().Trim();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteConCompras", "USP_LISTA_SOCIOS_CONCOMPRAS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteConComprasRecalculo(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_CONCOMPRAS_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString().Trim();
                    objCliente.FechaRegistro = dr["FechaRegistro"].ToString().Trim();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteConComprasRecalculo", "USP_LISTA_SOCIOS_CONCOMPRAS_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteBonoEscolaridad(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_ESCOLARIDAD", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PER", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DniSocio"].ToString().Trim();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteBonoEscolaridad", "USP_LISTA_SOCIOS_ESCOLARIDAD", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListarClienteBonoMercadeo(string FI0, string FF0, string FI1, string FF1, 
                                                               string FI2, string FF2, string FI3, string FF3,
                                                               int IDP)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CALCULO_MERCADEO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@FI0", Convert.ToDateTime(FI0).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FF0", Convert.ToDateTime(FF0).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FI1", Convert.ToDateTime(FI1).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FF1", Convert.ToDateTime(FF1).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FI2", Convert.ToDateTime(FI2).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FF2", Convert.ToDateTime(FF2).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FI3", Convert.ToDateTime(FI3).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@FF3", Convert.ToDateTime(FF3).ToString("yyyy/MM/dd"));
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objCliente = new PeriodoComision();
                    objCliente.Documento = dr["DOCUMENTO"].ToString().Trim();
                    objCliente.COMMERCA = Convert.ToDouble(dr["MONTO"].ToString());
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteBonoMercadeo", "USP_LISTA_CALCULO_MERCADEO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteBonoBronce(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CALCULO_BRONCE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Periodo", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DniSocio"].ToString().Trim();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteBonoBronce", "USP_LISTA_CALCULO_BRONCE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClienteConComprasRecalculoBronce(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_CONCOMPRAS_RECALCULO_BRONCE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString().Trim();
                    objCliente.FechaRegistro = dr["FechaRegistro"].ToString().Trim();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteConComprasRecalculoBronce", "USP_LISTA_SOCIOS_CONCOMPRAS_RECALCULO_BRONCE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarDniCDR(string dni, string antiguo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("DBO.USP_UPDATE_DNI_STOCK", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@ANTIGUO", antiguo);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarDniCDR", "DBO.USP_UPDATE_DNI_STOCK", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ResetearEstablecimientoFavorito(string dni)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("DBO.USP_RESETEAR_CDRFAVORITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", dni);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ResetearEstablecimientoFavorito", "DBO.USP_RESETEAR_CDRFAVORITO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public PatrocinadorUnilivel ListaDatosUplineUnilevel(int IDP, string dni)
        {
            PatrocinadorUnilivel objCliente = null;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DATOSUPLINE_UNILIVEL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    objCliente = new PatrocinadorUnilivel();
                    objCliente.documento = dr["NumeroDoc"].ToString().Trim();
                    objCliente.PP = Convert.ToDouble(dr["PP"].ToString());
                    objCliente.rango = Convert.ToString(dr["RANGO"].ToString());
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListaDatosUplineUnilevel", "USP_LISTA_DATOSUPLINE_UNILIVEL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objCliente;
        }

        public List<Cliente> ListarClienteRetencion(int IDP)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_ACTUALIZAR_RETENCION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DNISOCIO"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteRetencion", "USP_LISTA_ACTUALIZAR_RETENCION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<DatosSocios> ListarCantidadDirectos(string numDocumento, int IDP)
        {
            string errores = "";
            List<DatosSocios> ListaUpline = new List<DatosSocios>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_DIRECTOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", numDocumento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DatosSocios cliente = new DatosSocios();
                    cliente.CantSocios = Convert.ToInt32(dr["SOCIOS_DIRECTOS"].ToString());
                    cliente.CantCI = Convert.ToInt32(dr["CI_DIRECTOS"].ToString());
                    cliente.CantCON = Convert.ToInt32(dr["CON_DIRECTOS"].ToString());
                    cliente.CantSociosDirectos = Convert.ToInt32(dr["SOCIOS_ACTIVOS"].ToString());
                    cliente.CantCIDirectos = Convert.ToInt32(dr["CI_ACTIVOS"].ToString());
                    cliente.CantCONDirectos = Convert.ToInt32(dr["CON_ACTIVOS"].ToString());
                    cliente.NuevoSocios = Convert.ToInt32(dr["NUEVOS_SOCIOS"].ToString());
                    cliente.NuevoCI = Convert.ToInt32(dr["NUEVOS_CI"].ToString());
                    cliente.NuevoCON = Convert.ToInt32(dr["NUEVOS_CON"].ToString());
                    cliente.CantSociosRed = CantSociosRed(numDocumento, IDP, "01");
                    cliente.CantCIRed = CantSociosRedPatrocinador(numDocumento, IDP, "03");
                    cliente.CantCONRed = CantSociosRedPatrocinador(numDocumento, IDP, "05");
                    cliente.CantSociosRedActivos = CantSociosRedActivos(numDocumento, IDP, "01");
                    cliente.CantCIRedActivos = CantSociosRedActivosPatrocinador(numDocumento, IDP, "03");
                    cliente.CantCONRedActivos = CantSociosRedActivosPatrocinador(numDocumento, IDP, "05");
                    cliente.CantNuevosSociosRed = CantSociosRedNuevos(numDocumento, IDP, "01");
                    cliente.CantNuevosCIRed = CantSociosRedNuevosPatrocinador(numDocumento, IDP, "03");
                    cliente.CantNuevosCONRed = CantSociosRedNuevosPatrocinador(numDocumento, IDP, "05");
                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarCantidadDirectos", "USP_OBTENER_CANT_SOCIOS_DIRECTOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public int CantSociosRed(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantSociosRed", "USP_OBTENER_CANT_SOCIOS_RED", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedPatrocinador(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantSociosRedPatrocinador", "USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedActivos(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_ACTIVOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantSociosRedActivos", "USP_OBTENER_CANT_SOCIOS_RED_ACTIVOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedActivosPatrocinador(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR_ACTIVOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantSociosRedActivosPatrocinador", "USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR_ACTIVOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedNuevos(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_NUEVOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantSociosRedNuevos", "USP_OBTENER_CANT_SOCIOS_RED_NUEVOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedNuevosPatrocinador(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_NUEVOS_PATROCINADOR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "CantSociosRedNuevosPatrocinador", "USP_OBTENER_CANT_SOCIOS_RED_NUEVOS_PATROCINADOR", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public bool ActualizarRetencionSocio(int SocioDirectoTotal, int SocioDirectoActivo, int SocioDirectoNuevo,
                                            int ConsultorDirectoTotal, int ConsultorDirectoActivo, int ConsultorDirectoNuevo,
                                            int CIDirectoTotal, int CIDirectoActivo, int CIDirectoNuevo,
                                            int SocioRedTotal, int SocioRedActivo, int SocioRedNuevo,
                                            int ConsultorRedTotal, int ConsultorRedActivo, int ConsultorRedNuevo,
                                            int CIRedTotal, int CIRedActivo, int CIRedNuevo, string dni, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_RETENCION_SOCIOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@SOCIO_DIRECTO_TOTAL", SocioDirectoTotal);
                cmd.Parameters.AddWithValue("@SOCIO_DIRECTO_ACTIVO", SocioDirectoActivo);
                cmd.Parameters.AddWithValue("@SOCIO_DIRECTO_NUEVO", SocioDirectoNuevo);
                cmd.Parameters.AddWithValue("@CONSULTOR_DIRECTO_TOTAL", ConsultorDirectoTotal);
                cmd.Parameters.AddWithValue("@CONSULTOR_DIRECTO_ACTIVO", ConsultorDirectoActivo);
                cmd.Parameters.AddWithValue("@CONSULTOR_DIRECTO_NUEVO", ConsultorDirectoNuevo);
                cmd.Parameters.AddWithValue("@CI_DIRECTO_TOTAL", CIDirectoTotal);
                cmd.Parameters.AddWithValue("@CI_DIRECTO_ACTIVO", CIDirectoActivo);
                cmd.Parameters.AddWithValue("@CI_DIRECTO_NUEVO", CIDirectoNuevo);
                cmd.Parameters.AddWithValue("@SOCIO_RED_TOTAL", SocioRedTotal);
                cmd.Parameters.AddWithValue("@SOCIO_RED_ACTIVO", SocioRedActivo);
                cmd.Parameters.AddWithValue("@SOCIO_RED_NUEVO", SocioRedNuevo);
                cmd.Parameters.AddWithValue("@CONSULTOR_RED_TOTAL", ConsultorRedTotal);
                cmd.Parameters.AddWithValue("@CONSULTOR_RED_ACTIVO", ConsultorRedActivo);
                cmd.Parameters.AddWithValue("@CONSULTOR_RED_NUEVO", ConsultorRedNuevo);
                cmd.Parameters.AddWithValue("@CI_RED_TOTAL", CIRedTotal);
                cmd.Parameters.AddWithValue("@CI_RED_ACTIVO", CIRedActivo);
                cmd.Parameters.AddWithValue("@CI_RED_NUEVO", CIRedNuevo);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarRetencionSocio", "USP_ACTUALIZAR_RETENCION_SOCIOS", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public int ConsultaDirectosGeneral(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_DIRECTOS_GENERAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ConsultaDirectosGeneral", "USP_OBTENER_DIRECTOS_GENERAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public List<Cliente> ListarClienteSinCompras()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_SINCOMPRAS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["NumeroDoc"].ToString();
                    objCliente.idCliente = dr["IdCliente"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteSinCompras", "USP_LISTA_SOCIOS_SINCOMPRAS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarClientesMapaRed()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_LISTA_SOCIOS_RED", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DOCUMENTO"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClientesMapaRed", "USP_OBTENER_LISTA_SOCIOS_RED", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente> ListarRegaloSocios()
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_REGALOSXSOCIOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.numeroDoc = dr["DOCUMENTO"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.CDRPremio = dr["CDRPS"].ToString();
                    objCliente.regalo = dr["PRODUCTOPS"].ToString();
                    objCliente.cantRegalo = Convert.ToInt32(dr["CANTIDAD"]);
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarRegaloSocios", "USP_LISTA_REGALOSXSOCIOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool RegistroModificaciones(string autor, DateTime fecha, string accion, string dato)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_MODIFICACIONES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AUTOR", autor);
                cmd.Parameters.AddWithValue("@FECHA", fecha);
                cmd.Parameters.AddWithValue("@ACCION", accion);
                cmd.Parameters.AddWithValue("@DATO", dato);

                con.Open();

                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "RegistroModificaciones", "USP_REGISTRO_MODIFICACIONES", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public string ObtenerPaqueteSocio(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string Paquete = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_PAQUETE_BY_DNISOCIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NUMERODOC", documento.Trim());


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Paquete = Convert.ToString(dr["Packete"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ObtenerPaqueteSocio", "USP_OBTENER_PAQUETE_BY_DNISOCIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return Paquete;
        }

        public int ValidarCantidadUsuario(string usuario)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VALIDAR_CANTIDAD_USUARIOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@USUARIO", usuario);


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cantidad = Convert.ToInt32(dr["CANT"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarCantidadUsuario", "USP_VALIDAR_CANTIDAD_USUARIOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return cantidad;
        }

        public int ValidarCantidadDocumento(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VALIDAR_CANTIDAD_DOCUMENTO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NUMERODOC", documento);


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    cantidad = Convert.ToInt32(dr["CANT"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarCantidadDocumento", "USP_VALIDAR_CANTIDAD_DOCUMENTO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return cantidad;
        }

        public List<Cliente.Compresion> ListarSociosComprimir()
        {
            List<Cliente.Compresion> Lista = new List<Cliente.Compresion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_INACTIVOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.Compresion objCliente = new Cliente.Compresion();
                    objCliente.IdCliente = dr["IDCLIENTE"].ToString();
                    objCliente.Documento = dr["NumeroDoc"].ToString();
                    objCliente.Documento = dr["TipoCliente"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarSociosComprimir", "USP_LISTA_SOCIOS_INACTIVOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente.Compresion.Directos> ListarDirectoSocios(string documento)
        {
            List<Cliente.Compresion.Directos> Lista = new List<Cliente.Compresion.Directos>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_SOCIOS_DIRECTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.Compresion.Directos objCliente = new Cliente.Compresion.Directos();
                    objCliente.IdCliente = dr["IdCliente"].ToString();
                    objCliente.Documento = dr["NumeroDoc"].ToString();
                    objCliente.TipoCliente = dr["TipoCliente"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarDirectoSocios", "USP_LISTA_SOCIOS_DIRECTOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Cliente.Compresion.DocumentoUP> ListarDocumentoUP(string documento)
        {
            List<Cliente.Compresion.DocumentoUP> Lista = new List<Cliente.Compresion.DocumentoUP>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_PU_SOCIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.Compresion.DocumentoUP objCliente = new Cliente.Compresion.DocumentoUP();
                    objCliente.Upline = dr["Upline"].ToString();
                    objCliente.Patrocinador = dr["Patrocinador"].ToString();
                    objCliente.FactorComision = dr["factorComision"].ToString();
                    objCliente.IdUpline = dr["IDUPLINE"].ToString();
                    objCliente.IdPatrocinador = dr["IDPATROCINADOR"].ToString();
                    objCliente.IdFactorComision = dr["IDFACTORCOMI"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarDocumentoUP", "USP_OBTENER_PU_SOCIO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public void ActualizarDocumentoSocioComprimido(string idcliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_DOCUMENTO_SOCIO_COMPRIMIDO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                conexion.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarDocumentoSocioComprimido", "USP_ACTUALIZAR_DOCUMENTO_SOCIO_COMPRIMIDO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
        }

        public void ActualizarMapaRedComisionCompresion(string upline, string idupline, string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_MAPARED_COMISION_COMPRESION", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UPLINE", upline);
                cmd.Parameters.AddWithValue("@IDUPLINE", idupline);
                cmd.Parameters.AddWithValue("@DOCUMENTOSOCIO", documento);
                conexion.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarMapaRedComisionCompresion", "USP_ACTUALIZAR_MAPARED_COMISION_COMPRESION", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
        }

        public void ActualizarDatos_Directos_Compresion(string upline, string patrocinador, string factorComi, string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_DATOS_SOCIOSDIRECTOS_COMPRESION", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PATROCINADOR", patrocinador);
                cmd.Parameters.AddWithValue("@FACTORCOMI", factorComi);
                cmd.Parameters.AddWithValue("@DOCUMENTOSOCIO", documento);
                if (upline == "") { cmd.Parameters.AddWithValue("@UPLINE", DBNull.Value); }
                else { cmd.Parameters.AddWithValue("@UPLINE", upline); }
                conexion.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarDatos_Directos_Compresion", "USP_ACTUALIZAR_DATOS_SOCIOSDIRECTOS_COMPRESION", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
        }

        public void ActualizarDocumento_Consultor_CI_Comprimido(string idcliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_DOCUMENTO_CONSULTOR_CI_COMPRIMIDO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                conexion.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarDocumento_Consultor_CI_Comprimido", "USP_ACTUALIZAR_DOCUMENTO_CONSULTOR_CI_COMPRIMIDO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
        }

        public List<Cliente.ClientePreRegistro> ListarClientesPreRegistro(string patrocinador)
        {
            List<Cliente.ClientePreRegistro> Lista = new List<Cliente.ClientePreRegistro>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PREREGISTRO_BY_PATROCINADOR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PATROCINADOR", patrocinador);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente.ClientePreRegistro objCliente = new Cliente.ClientePreRegistro();
                    objCliente.FechaRegistro = dr["FechaRegistro"].ToString();
                    objCliente.Documento = dr["NumeroDoc"].ToString();
                    objCliente.Nombres = dr["Nombres"].ToString();
                    objCliente.Apellidos = dr["Apellidos"].ToString();
                    objCliente.Nombres_Upline = dr["Upline"].ToString();
                    objCliente.Paquete = dr["Paquete"].ToString();
                    objCliente.IdPaquete = dr["IdPackete"].ToString();
                    objCliente.IdCliente = dr["idcliente"].ToString();
                    objCliente.CboPreregistro = dr["CBO_PREREGISTRO"].ToString();
                    objCliente.TipoCliente = dr["TipoCliente"].ToString();
                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClientesPreRegistro", "USP_LISTA_PREREGISTRO_BY_PATROCINADOR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string ValidarClaveUsuario(string usuario,string clave,string tipo)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string result="";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VAlIDAR_LOGIN", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@Clave", clave);
                cmd.Parameters.AddWithValue("@Tipo", tipo);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarClaveUsuario", "USP_VAlIDAR_LOGIN", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string ValidarClaveUsuario2(string usuario, string clave, string tipo)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string result = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_VAlIDAR_LOGIN2", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@Clave", clave);
                cmd.Parameters.AddWithValue("@Tipo", tipo);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarClaveUsuario2", "USP_VAlIDAR_LOGIN2", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string ActualizarClaveUsuario(string usuario, string clave)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string result = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_CLAVE", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@Clave", clave);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarClaveUsuario", "USP_ACTUALIZAR_CLAVE", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string ActualizarPreguntasSeguridad(string usuario, string pregunta, string respuesta)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string result = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RegistrarPreguntasUsuario", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.Parameters.AddWithValue("@pregunta", pregunta);
                cmd.Parameters.AddWithValue("@respuesta", respuesta);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarPreguntasSeguridad", "USP_RegistrarPreguntasUsuario", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        public string ActualizarClaveUsuario2(string usuario, string clave)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string result = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_CLAVE2", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@Clave", clave);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    result = Convert.ToString(dr["mensaje"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarClaveUsuario2", "USP_ACTUALIZAR_CLAVE2", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return result;
        }

        //public string ValidarPreguntasXUsuario(string usuario, string pregunta, string respuesta)
        //{
        //    SqlConnection conexion = null;
        //    SqlCommand cmd = null;
        //    SqlDataReader dr = null;
        //    string result = "";

        //    try
        //    {
        //        conexion = Conexion.getInstance().Conectar();
        //        cmd = new SqlCommand("USP_ValidarPreguntasUsuario", conexion);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@usuario", usuario);
        //        cmd.Parameters.AddWithValue("@pregunta", pregunta);
        //        cmd.Parameters.AddWithValue("@respuesta", respuesta);

        //        dr = null;
        //        conexion.Open();
        //        dr = cmd.ExecuteReader();

        //        while (dr.Read())
        //        {
        //            result = Convert.ToString(dr["Resultado"]);
        //        }
        //        dr.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarPreguntasXUsuario", "USP_ValidarPreguntasUsuario", ex.Message);
        //        errores = ex.Message;
        //    }
        //    finally
        //    {
        //        if (conexion.State == ConnectionState.Open)
        //        {
        //            conexion.Close();
        //        }
        //        conexion.Dispose();
        //        cmd.Dispose();
        //    }
        //    return result;
        //}
        public List<Cliente> ListarClienteUsuario(string usuario)
        {
            List<Cliente> Lista = new List<Cliente>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GetClienteByUsuario", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuario", usuario);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente objCliente = new Cliente();
                    objCliente.idCliente = dr["ID"].ToString();
                    objCliente.usuario = dr["USUARIO"].ToString();
                    objCliente.clave = dr["CLAVE"].ToString();
                    objCliente.nombre = dr["NOMBRES"].ToString();
                    objCliente.apellidoPat = dr["APELLIDOPAT"].ToString();
                    objCliente.apellidoMat = dr["APELLIDOMAT"].ToString();
                    objCliente.apodo = dr["APODO"].ToString();
                    objCliente.fechaNac = dr["FECHANAC"].ToString();
                    objCliente.sexo = dr["SEXO"].ToString();
                    objCliente.tipoDoc = dr["TIPODOC"].ToString();
                    objCliente.numeroDoc = dr["NUMERODOC"].ToString();
                    objCliente.tipoCliente = dr["TIPOC"].ToString();
                    objCliente.idtipoCliente = dr["IDTIPOC"].ToString();
                    objCliente.patrocinador = dr["PATRO"].ToString();
                    objCliente.idpatrocinador = dr["IDPATRO"].ToString();
                    objCliente.upline = dr["UPLINE"].ToString();
                    objCliente.idupline = dr["IDUPLINE"].ToString();
                    objCliente.correo = dr["CORREO"].ToString();
                    objCliente.telefono = dr["TELEFONO"].ToString();
                    objCliente.celular = dr["CELULAR"].ToString();
                    objCliente.pais = dr["PAIS"].ToString();
                    objCliente.idpais = dr["IDPAIS"].ToString();
                    objCliente.departamento = dr["DEPARTAMENTO"].ToString();
                    objCliente.iddepartamento = dr["IDDEPARTAMENTO"].ToString();
                    objCliente.provincia = dr["PROVINCIA"].ToString();
                    objCliente.idprovincia = dr["IDPROVINCIA"].ToString();
                    objCliente.ditrito = dr["DISTRITO"].ToString();
                    objCliente.idditrito = dr["IDDISTRITO"].ToString();
                    objCliente.direccion = dr["DIRECCION"].ToString();
                    objCliente.referencia = dr["REFERENCIA"].ToString();
                    objCliente.nroCtaDeposito = dr["DEPOSITO"].ToString();
                    objCliente.nroCtaDetraccion = dr["DETRACCION"].ToString();
                    objCliente.nroCtaInterbancaria = dr["INTERBANCARIA"].ToString();
                    objCliente.ruc = dr["RUC"].ToString();
                    objCliente.nombreBanco = dr["BANCO"].ToString();
                    objCliente.tipoEstablecimiento = dr["APODO"].ToString();
                    objCliente.idtipoEstablecimiento = dr["IDAPODO"].ToString();
                    objCliente.imagen = dr["IMAGEN"].ToString();
                    objCliente.idtipoEstablecimientoPremio = dr["CDRPREMIO"].ToString().Trim();
                    objCliente.correo_encrip = dr["CORREO_ENCRIP"].ToString().Trim();
                    objCliente.celular_encrip = dr["CELULAR_ENCRIP"].ToString().Trim();

                    Lista.Add(objCliente);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ListarClienteUsuario", "USP_GetClienteByUsuario", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string DevolverToken(string usuario)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string Token = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DevolverToken", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Token = Convert.ToString(dr["TOKEN"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "DevolverToken", "USP_DevolverToken", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return Token;
        }

        public string ValidarToken(string usuario,string codigo_correo)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string Token = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ValidarToken", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", usuario);
                cmd.Parameters.AddWithValue("@CodigoSeguridad", codigo_correo);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Token = Convert.ToString(dr["TOKEN"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ValidarToken", "USP_ValidarToken", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return Token;
        }

        public List<Cliente> ListadoPreguntasUsuario(string usuario)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente> listaPreguntas = new List<Cliente>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_PreguntasUsuario", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@usuario", usuario);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.Pregunta = Convert.ToString(dr["Pregunta"]);
                    cliente.Respuesta = Convert.ToString(dr["Respuesta"]);
                    listaPreguntas.Add(cliente);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListadoPreguntasUsuario", "USP_PreguntasUsuario", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPreguntas;
        }

        public List<Cliente> ListadoPreguntas(string usuario)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Cliente> listaPreguntas = new List<Cliente>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_Preguntas", con);
                cmd.Parameters.AddWithValue("@usuario", usuario);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Cliente cliente = new Cliente();
                    cliente.Pregunta = Convert.ToString(dr["Pregunta"]);
                    cliente.Respuesta = Convert.ToString(dr["Respuesta"]);
                    listaPreguntas.Add(cliente);
                }
                dr.Close();

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("CompraDatos", "ListadoPreguntas", "USP_Preguntas", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPreguntas;
        }

        public string ObtenerCorreo_By_Usuario(string usuario)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string Token = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CORREO_BY_USUARIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@USUARIO", usuario);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Token = Convert.ToString(dr["CORREO"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ObtenerCorreo", "USP_OBTENER_CORREO_BY_USUARIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return Token;
        }

        public int ObtenerFiltroPreregistro(string idcliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int filtro = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_IDENTIFICAR_PREREGISTRO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    filtro = Convert.ToInt32(dr["Filtro_PreRegistro"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ObtenerFiltroPreregistro", "USP_IDENTIFICAR_PREREGISTRO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return filtro;
        }

        public void ActualizarEstadoPreregistro(string idcliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_PREREGISTRO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                conexion.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ActualizarEstadoPreregistro", "USP_ACTUALIZAR_PREREGISTRO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
        }

        public string ObtenerCorreo_By_IdCliente(string idcliente)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string Token = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CORREO_BY_IDCLIENTE", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Token = Convert.ToString(dr["CORREO"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ClienteDAO", "ObtenerCorreo", "USP_OBTENER_CORREO_BY_IDCLIENTE", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return Token;
        }
    }
}
