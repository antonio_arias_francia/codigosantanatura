﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class NovedadesDAO
    {
        #region "PATRON SINGLETON"
        private static NovedadesDAO daoNovedades = null;
        private NovedadesDAO() { }
        public static NovedadesDAO getInstance()
        {
            if (daoNovedades == null)
            {
                daoNovedades = new NovedadesDAO();
            }
            return daoNovedades;
        }
        #endregion

        public bool RegistroNovedades(Novedad objNovedad)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRONOVEDADES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TITULO", objNovedad.titulo);
                cmd.Parameters.AddWithValue("@MENSAJE", objNovedad.mensaje);
                cmd.Parameters.AddWithValue("@ENLACE", objNovedad.enlace);
                cmd.Parameters.AddWithValue("@IMAGEN", objNovedad.imagen);
                cmd.Parameters.AddWithValue("@ESTADO", objNovedad.estado);
                cmd.Parameters.AddWithValue("@FECHA", objNovedad.fecha);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "RegistroNovedades", "USP_REGISTRONOVEDADES", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Novedad> ListaNovedad()
        {
            List<Novedad> Lista = new List<Novedad>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTANOVEDADES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Novedad objNovedad = new Novedad();
                    objNovedad.idNovedades = dr["ID_NOVE"].ToString();
                    objNovedad.titulo = dr["TITULO"].ToString();
                    objNovedad.mensaje = dr["MENSAJE"].ToString();
                    objNovedad.enlace = dr["ENLACE"].ToString();
                    objNovedad.imagen = dr["IMAGEN"].ToString();
                    objNovedad.estado = Convert.ToBoolean(dr["ESTADO"].ToString());
                    objNovedad.fecha = dr["FECHA"].ToString();
                    Lista.Add(objNovedad);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "ListaNovedad", "USP_LISTANOVEDADES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarNovedad(Novedad objNovedad)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATENOVEDADES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_NOV", objNovedad.idNovedades);
                cmd.Parameters.AddWithValue("@TITULO", objNovedad.titulo);
                cmd.Parameters.AddWithValue("@MENSAJE", objNovedad.mensaje);
                cmd.Parameters.AddWithValue("@ENLACE", objNovedad.enlace);
                cmd.Parameters.AddWithValue("@IMAGEN", objNovedad.imagen);
                cmd.Parameters.AddWithValue("@ESTADO", objNovedad.estado);
                cmd.Parameters.AddWithValue("@FECHA", objNovedad.fecha);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "ActualizarNovedad", "USP_UPDATENOVEDADES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool EliminarNovedad(string id)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DELETENOVEDADES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_NOV", id);

                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "EliminarNovedad", "USP_DELETENOVEDADES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }
    }
}
