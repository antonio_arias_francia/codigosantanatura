﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class PeriodoComisionDAO
    {
        #region "PATRON SINGLETON"
        private static PeriodoComisionDAO _periodoDatos = null;
        private PeriodoComisionDAO() { }
        public static PeriodoComisionDAO getInstance()
        {
            if (_periodoDatos == null)
            {
                _periodoDatos = new PeriodoComisionDAO();
            }
            return _periodoDatos;
        }
        #endregion

        public List<PeriodoComision> ListaPeriodoComisionCombo()
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPERIODO_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaPeriodoComisionCombo", "USP_LISTAPERIODO_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaComisionesLinea(string documento)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_12PERIODOS_COMISION_SOCIO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMI"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    objPeriodo.ComisionLinea = Convert.ToDouble(dr["COMITOTAL"]);
                    objPeriodo.COMAFI = Convert.ToDouble(dr["COMAFI"]);
                    objPeriodo.COMUNI = Convert.ToDouble(dr["COMUNI"]);
                    objPeriodo.COMTIBU = Convert.ToDouble(dr["COMTIBU"]);
                    objPeriodo.COMBRON = Convert.ToDouble(dr["COMBRON"]);
                    objPeriodo.COMCI = Convert.ToDouble(dr["COMCI"]);
                    objPeriodo.COMCON = Convert.ToDouble(dr["COMCON"]);
                    objPeriodo.COMESCO = Convert.ToDouble(dr["COMESCO"]);
                    objPeriodo.COMMERCA = Convert.ToDouble(dr["COMMERCA"]);
                    objPeriodo.COMMATRI = Convert.ToDouble(dr["COMMATRI"]);
                    objPeriodo.NIVEL1 = Convert.ToDouble(dr["NIVEL1"]);
                    objPeriodo.NIVEL2 = Convert.ToDouble(dr["NIVEL2"]);
                    objPeriodo.NIVEL3 = Convert.ToDouble(dr["NIVEL3"]);
                    objPeriodo.NIVEL4 = Convert.ToDouble(dr["NIVEL4"]);
                    objPeriodo.NIVEL5 = Convert.ToDouble(dr["NIVEL5"]);
                    objPeriodo.UNIX1 = Convert.ToDouble(dr["UNIX1"]);
                    objPeriodo.UNIX2 = Convert.ToDouble(dr["UNIX2"]);
                    objPeriodo.UNIX3 = Convert.ToDouble(dr["UNIX3"]);
                    objPeriodo.UNIX4 = Convert.ToDouble(dr["UNIX4"]);
                    objPeriodo.UNIX5 = Convert.ToDouble(dr["UNIX5"]);
                    objPeriodo.UNIX6 = Convert.ToDouble(dr["UNIX6"]);
                    objPeriodo.UNIX7 = Convert.ToDouble(dr["UNIX7"]);
                    objPeriodo.UNIX8 = Convert.ToDouble(dr["UNIX8"]);
                    objPeriodo.UNIX9 = Convert.ToDouble(dr["UNIX9"]);
                    objPeriodo.UNIX10 = Convert.ToDouble(dr["UNIX10"]);
                    objPeriodo.UNIX11 = Convert.ToDouble(dr["UNIX11"]);
                    objPeriodo.UNIX12 = Convert.ToDouble(dr["UNIX12"]);
                    objPeriodo.UNIX13 = Convert.ToDouble(dr["UNIX13"]);
                    objPeriodo.UNIX14 = Convert.ToDouble(dr["UNIX14"]);
                    objPeriodo.UNIX15 = Convert.ToDouble(dr["UNIX15"]);
                    objPeriodo.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaComisionesLinea", "USP_OBTENER_12PERIODOS_COMISION_SOCIO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaComisionesXPeriodo(string documento, int IDP)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_COMISION_SOCIO_IDPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMI"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    objPeriodo.ComisionLinea = Convert.ToDouble(dr["COMITOTAL"]);
                    objPeriodo.COMAFI = Convert.ToDouble(dr["COMAFI"]);
                    objPeriodo.COMUNI = Convert.ToDouble(dr["COMUNI"]);
                    objPeriodo.COMTIBU = Convert.ToDouble(dr["COMTIBU"]);
                    objPeriodo.COMBRON = Convert.ToDouble(dr["COMBRON"]);
                    objPeriodo.COMCI = Convert.ToDouble(dr["COMCI"]);
                    objPeriodo.COMCON = Convert.ToDouble(dr["COMCON"]);
                    objPeriodo.COMESCO = Convert.ToDouble(dr["COMESCO"]);
                    objPeriodo.COMMERCA = Convert.ToDouble(dr["COMMERCA"]);
                    objPeriodo.COMMATRI = Convert.ToDouble(dr["COMMATRI"]);
                    objPeriodo.NIVEL1 = Convert.ToDouble(dr["NIVEL1"]);
                    objPeriodo.NIVEL2 = Convert.ToDouble(dr["NIVEL2"]);
                    objPeriodo.NIVEL3 = Convert.ToDouble(dr["NIVEL3"]);
                    objPeriodo.NIVEL4 = Convert.ToDouble(dr["NIVEL4"]);
                    objPeriodo.NIVEL5 = Convert.ToDouble(dr["NIVEL5"]);
                    objPeriodo.UNIX1 = Convert.ToDouble(dr["UNIX1"]);
                    objPeriodo.UNIX2 = Convert.ToDouble(dr["UNIX2"]);
                    objPeriodo.UNIX3 = Convert.ToDouble(dr["UNIX3"]);
                    objPeriodo.UNIX4 = Convert.ToDouble(dr["UNIX4"]);
                    objPeriodo.UNIX5 = Convert.ToDouble(dr["UNIX5"]);
                    objPeriodo.UNIX6 = Convert.ToDouble(dr["UNIX6"]);
                    objPeriodo.UNIX7 = Convert.ToDouble(dr["UNIX7"]);
                    objPeriodo.UNIX8 = Convert.ToDouble(dr["UNIX8"]);
                    objPeriodo.UNIX9 = Convert.ToDouble(dr["UNIX9"]);
                    objPeriodo.UNIX10 = Convert.ToDouble(dr["UNIX10"]);
                    objPeriodo.UNIX11 = Convert.ToDouble(dr["UNIX11"]);
                    objPeriodo.UNIX12 = Convert.ToDouble(dr["UNIX12"]);
                    objPeriodo.UNIX13 = Convert.ToDouble(dr["UNIX13"]);
                    objPeriodo.UNIX14 = Convert.ToDouble(dr["UNIX14"]);
                    objPeriodo.UNIX15 = Convert.ToDouble(dr["UNIX15"]);
                    objPeriodo.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaComisionesXPeriodo", "USP_OBTENER_COMISION_SOCIO_IDPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaComisionesXPeriodo_Historico(string documento, int IDP)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_COMISION_SOCIO_IDPERIODO_HISTORICO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMI"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    objPeriodo.ComisionLinea = Convert.ToDouble(dr["COMITOTAL"]);
                    objPeriodo.COMAFI = Convert.ToDouble(dr["COMAFI"]);
                    objPeriodo.COMUNI = Convert.ToDouble(dr["COMUNI"]);
                    objPeriodo.COMTIBU = Convert.ToDouble(dr["COMTIBU"]);
                    objPeriodo.COMBRON = Convert.ToDouble(dr["COMBRON"]);
                    objPeriodo.COMCI = Convert.ToDouble(dr["COMCI"]);
                    objPeriodo.COMCON = Convert.ToDouble(dr["COMCON"]);
                    objPeriodo.COMESCO = Convert.ToDouble(dr["COMESCO"]);
                    objPeriodo.COMMERCA = Convert.ToDouble(dr["COMMERCA"]);
                    objPeriodo.COMMATRI = Convert.ToDouble(dr["COMMATRI"]);
                    objPeriodo.NIVEL1 = Convert.ToDouble(dr["NIVEL1"]);
                    objPeriodo.NIVEL2 = Convert.ToDouble(dr["NIVEL2"]);
                    objPeriodo.NIVEL3 = Convert.ToDouble(dr["NIVEL3"]);
                    objPeriodo.NIVEL4 = Convert.ToDouble(dr["NIVEL4"]);
                    objPeriodo.NIVEL5 = Convert.ToDouble(dr["NIVEL5"]);
                    objPeriodo.UNIX1 = Convert.ToDouble(dr["UNIX1"]);
                    objPeriodo.UNIX2 = Convert.ToDouble(dr["UNIX2"]);
                    objPeriodo.UNIX3 = Convert.ToDouble(dr["UNIX3"]);
                    objPeriodo.UNIX4 = Convert.ToDouble(dr["UNIX4"]);
                    objPeriodo.UNIX5 = Convert.ToDouble(dr["UNIX5"]);
                    objPeriodo.UNIX6 = Convert.ToDouble(dr["UNIX6"]);
                    objPeriodo.UNIX7 = Convert.ToDouble(dr["UNIX7"]);
                    objPeriodo.UNIX8 = Convert.ToDouble(dr["UNIX8"]);
                    objPeriodo.UNIX9 = Convert.ToDouble(dr["UNIX9"]);
                    objPeriodo.UNIX10 = Convert.ToDouble(dr["UNIX10"]);
                    objPeriodo.UNIX11 = Convert.ToDouble(dr["UNIX11"]);
                    objPeriodo.UNIX12 = Convert.ToDouble(dr["UNIX12"]);
                    objPeriodo.UNIX13 = Convert.ToDouble(dr["UNIX13"]);
                    objPeriodo.UNIX14 = Convert.ToDouble(dr["UNIX14"]);
                    objPeriodo.UNIX15 = Convert.ToDouble(dr["UNIX15"]);
                    objPeriodo.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaComisionesXPeriodo", "USP_OBTENER_COMISION_SOCIO_IDPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaProgresionTotalBonificaciones()
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_SUMA_LINEA_PROGRESIVA_TOTAL_BONIFICACIONES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    objPeriodo.SUMATOTAL = Convert.ToDouble(dr["SUMATOTAL"]);
                    objPeriodo.COMAFI = Convert.ToDouble(dr["COMAFI"]);
                    objPeriodo.COMUNI = Convert.ToDouble(dr["COMUNI"]);
                    objPeriodo.COMCI = Convert.ToDouble(dr["COMCI"]);
                    objPeriodo.COMCON = Convert.ToDouble(dr["COMCON"]);
                    objPeriodo.COMTIBU = Convert.ToDouble(dr["COMTIBU"]);
                    objPeriodo.COMBRON = Convert.ToDouble(dr["COMBRON"]);
                    objPeriodo.COMESCO = Convert.ToDouble(dr["COMESCO"]);
                    objPeriodo.COMMERCA = Convert.ToDouble(dr["COMMERCA"]);
                    objPeriodo.COMMATRI = Convert.ToDouble(dr["COMMATRI"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaProgresionTotalBonificaciones", "USP_SUMA_LINEA_PROGRESIVA_TOTAL_BONIFICACIONES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public int MostrarIdPeriodoComisionActivo()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int id = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_MOSTRAR_IDPERIODO_COMISION_ACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    id = objPeriodo.idPeriodo;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "MostrarIdPeriodoComisionActivo", "USP_MOSTRAR_IDPERIODO_COMISION_ACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return id;
        }

        public List<PeriodoComision> ListaPeriodoComisionFacturacion(string idcliente)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PERIODO_FACTURACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_CLIENTE", idcliente);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaPeriodoComisionFacturacion", "USP_LISTA_PERIODO_FACTURACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaPeriodoModalCanjesAbono(int idPeriodo)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PERIODO_MODAL_FACTURACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", idPeriodo);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaPeriodoModalCanjesAbono", "USP_LISTA_PERIODO_MODAL_FACTURACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaPeriodoComisionFacturacionModal(string idcliente, int idPeriodo)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PERIODO_FACTURACION_MODAL", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_CLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@ID_PERIODO", idPeriodo);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.nombrePeriodo = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaPeriodoComisionFacturacionModal", "USP_LISTA_PERIODO_FACTURACION_MODAL", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PeriodoComision> ListaComisionUnilevelxNivel(int IDP, string proc)
        {
            List<PeriodoComision> Lista = new List<PeriodoComision>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand(proc, con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PeriodoComision objPeriodo = new PeriodoComision();
                    objPeriodo.idCliente = Convert.ToString(dr["PatrNv"]);
                    objPeriodo.COMUNI = Convert.ToDouble(dr["BFNv"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoComisionDAO", "ListaPeriodoComisionFacturacionModal", "ListaPeriodoComisionFacturacionModal", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

    }
}
