﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class PromocionUnicaDAO
    {
        #region "PATRON SINGLETON"
        private static PromocionUnicaDAO _promoDatos = null;
        private PromocionUnicaDAO() { }
        public static PromocionUnicaDAO getInstance()
        {
            if (_promoDatos == null)
            {
                _promoDatos = new PromocionUnicaDAO();
            }
            return _promoDatos;
        }
        #endregion

        public List<PromocionUnicaModel> ListarPromocion()
        {
            List<PromocionUnicaModel> Lista = new List<PromocionUnicaModel>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PROMOCION_UNICA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PromocionUnicaModel objPromo = new PromocionUnicaModel();
                    objPromo.IdPromo = Convert.ToInt32(dr["IDPROMO"]);
                    objPromo.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    objPromo.ProductoPais = dr["IDPRODUCTOXPAIS"].ToString();
                    objPromo.Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                    objPromo.FechaInicio = dr["FECHAINICIO"].ToString();
                    objPromo.FechaFin = dr["FECHAFIN"].ToString();
                    Lista.Add(objPromo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "ListarPromocion", "USP_LISTA_PROMOCION_UNICA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public int ListaCantPromoUnica()
        {
            List<PromocionUnicaModel> Lista = new List<PromocionUnicaModel>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CANT_PROMO_UNICA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PromocionUnicaModel objPromo = new PromocionUnicaModel();
                    objPromo.CantidadActiva = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    cantidad = objPromo.CantidadActiva;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "ListaCantPromoUnica", "USP_LISTA_CANT_PROMO_UNICA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public int ListaEvaluarCantProductosPromocionUnica(string idcliente)
        {
            List<PromocionUnicaModel> Lista = new List<PromocionUnicaModel>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_EVALUACION_PROMO_UNICA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PromocionUnicaModel objPromo = new PromocionUnicaModel();
                    objPromo.Cantidad = (dr["CANTIDAD"] == DBNull.Value) ? 0 : Convert.ToInt32(dr["CANTIDAD"]);
                    cantidad = objPromo.Cantidad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "ListaEvaluarCantProductosPromocionUnica", "USP_EVALUACION_PROMO_UNICA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public string ListaCodigoPromoActiva()
        {
            List<PromocionUnicaModel> Lista = new List<PromocionUnicaModel>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string codigo = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_IDPROMO_UNICA_ACTIVA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PromocionUnicaModel objPromo = new PromocionUnicaModel();
                    objPromo.IdPromo = Convert.ToInt32(dr["IDPROMO"].ToString());
                    codigo = Convert.ToString(objPromo.IdPromo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "ListaCodigoPromoActiva", "USP_LISTA_IDPROMO_UNICA_ACTIVA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return codigo;
        }

        public string ListaIdProductoPromoUnica()
        {
            List<PromocionUnicaModel> Lista = new List<PromocionUnicaModel>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string codigo = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_IDPRODUCTO_PROMOUNICA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PromocionUnicaModel objPromo = new PromocionUnicaModel();
                    objPromo.IdProducto = Convert.ToString(dr["IdProducto"].ToString());
                    codigo = Convert.ToString(objPromo.IdProducto);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "ListaIdProductoPromoUnica", "USP_OBTENER_IDPRODUCTO_PROMOUNICA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return codigo;
        }

        public bool RegistroPromoUnica(PromocionUnicaModel objPromo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_PROMOCION_ACTIVA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHAINICIO", objPromo.FechaInicio);
                cmd.Parameters.AddWithValue("@FECHAFIN", objPromo.FechaFin);
                cmd.Parameters.AddWithValue("@IDPRODUCTOXPAIS", objPromo.ProductoPais);
                cmd.Parameters.AddWithValue("@CANTIDAD", objPromo.Cantidad);
                cmd.Parameters.AddWithValue("@ESTADO", objPromo.Estado);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "RegistroPromoUnica", "USP_REGISTRO_PROMOCION_ACTIVA", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarPromoUnica(PromocionUnicaModel objPromo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATE_PROMOCION_ACTIVA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPROMO", objPromo.IdPromo);
                cmd.Parameters.AddWithValue("@FECHAINICIO", objPromo.FechaInicio);
                cmd.Parameters.AddWithValue("@FECHAFIN", objPromo.FechaFin);
                cmd.Parameters.AddWithValue("@IDPRODUCTOXPAIS", objPromo.ProductoPais);
                cmd.Parameters.AddWithValue("@ESTADO", objPromo.Estado);
                cmd.Parameters.AddWithValue("@CANTIDAD", objPromo.Cantidad);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "ActualizarPromoUnica", "USP_UPDATE_PROMOCION_ACTIVA", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public PromocionUnicaModel DatosPromoUnica()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            PromocionUnicaModel objPromo = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DATOS_EVAL_PROMOUNICA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    objPromo = new PromocionUnicaModel();
                    objPromo.Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                    objPromo.ProductoPais = dr["IDPRODUCTOXPAIS"].ToString();
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PromocionUnicaDAO", "DatosPromoUnica", "USP_DATOS_EVAL_PROMOUNICA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return objPromo;
        }

    }

}
