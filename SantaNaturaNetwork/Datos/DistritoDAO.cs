﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class DistritoDAO
    {
        #region "PATRON SINGLETON"
        private static DistritoDAO daoDistrito = null;
        private DistritoDAO() { }
        public static DistritoDAO getInstance()
        {
            if (daoDistrito == null)
            {
                daoDistrito = new DistritoDAO();
            }
            return daoDistrito;
        }
        #endregion

        public List<Distrito> ListarDistrito(string provincia)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Distrito> listaDistrito = new List<Distrito>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADODISTRITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PROVINCIA", provincia);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Distrito distrito = new Distrito();
                    distrito.Codigo = dr["Codigo"].ToString();
                    distrito.Nombre = dr["Nombre"].ToString();
                    listaDistrito.Add(distrito);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DistritoDAO", "ListarDistrito", "USP_LISTADODISTRITO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDistrito;
        }

        public List<Distrito> GetDistrito()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Distrito> listaDistrito = new List<Distrito>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GETDISTRITO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Distrito distrito = new Distrito();
                    distrito.Codigo = dr["Codigo"].ToString();
                    distrito.CodigoProvincia = dr["CodigoProvincia"].ToString();
                    distrito.Nombre = dr["Nombre"].ToString();
                    listaDistrito.Add(distrito);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DistritoDAO", "GetDistrito", "USP_GETDISTRITO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaDistrito;
        }

    }
}
