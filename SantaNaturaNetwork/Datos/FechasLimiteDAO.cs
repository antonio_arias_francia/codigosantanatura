﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class FechasLimiteDAO
    {
        #region "PATRON SINGLETON"
        private static FechasLimiteDAO daoLimite = null;
        private FechasLimiteDAO() { }
        public static FechasLimiteDAO getInstance()
        {
            if (daoLimite == null)
            {
                daoLimite = new FechasLimiteDAO();
            }
            return daoLimite;
        }
        #endregion

        public bool RegistroLimites(LimiteDias objLimite)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROLIMITE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@LIMITE", objLimite.numeroDias);
                con.Open();
                int filas = cmd.ExecuteNonQuery();
                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "RegistroLimites", "USP_REGISTROLIMITE", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<LimiteDias> ListaLimites(int idLimite)
        {
            List<LimiteDias> Lista = new List<LimiteDias>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOLIMITES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDLIMITE", idLimite);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    LimiteDias objLimite = new LimiteDias();
                    objLimite.idLimite = Convert.ToInt32(dr["IdLimite"].ToString());
                    objLimite.numeroDias = Convert.ToInt32(dr["DiasLimite"].ToString());
                    Lista.Add(objLimite);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "ListaLimites", "USP_LISTADOLIMITES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<LimiteDias> ListaGeneralHorasLimites()
        {
            List<LimiteDias> Lista = new List<LimiteDias>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAGENERAL_HORAS_LIMITE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    LimiteDias objLimite = new LimiteDias();
                    objLimite.idLimite = Convert.ToInt32(dr["IdLimite"].ToString());
                    objLimite.numeroDias = Convert.ToInt32(dr["DiasLimite"].ToString());
                    Lista.Add(objLimite);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "ListaGeneralHorasLimites", "USP_LISTAGENERAL_HORAS_LIMITE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public bool ActualizarLimite(LimiteDias objLimite)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATELIMITE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDLIMITE", objLimite.idLimite);
                cmd.Parameters.AddWithValue("@LIMITE", objLimite.numeroDias);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("FechasLimiteDAO", "ActualizarLimite", "USP_UPDATELIMITE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

    }
}
