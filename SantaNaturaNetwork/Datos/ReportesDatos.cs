﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using System.Data;
using System.Data.SqlClient;

namespace Datos
{
    public class ReportesDatos
    {

        #region "PATRON SINGLETON"
        private static ReportesDatos _reporteDatos = null;
        private ReportesDatos() { }
        public static ReportesDatos getInstance()
        {
            if (_reporteDatos == null)
            {
                _reporteDatos = new ReportesDatos();
            }
            return _reporteDatos;
        }
        #endregion

        string errores = "";

        public List<ReporteCDRModelo> ListaReporteCDR(string fecha1, string fecha2)
        {
            List<ReporteCDRModelo> listaReporte = new List<ReporteCDRModelo>();
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REPORTECDR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReporteCDRModelo reporte = new ReporteCDRModelo();
                    DateTime fechaS = Convert.ToDateTime(dr["FechaVaucher"]);
                    string format = fechaS.ToString("yyyy/MM/dd");

                    reporte.Idop = Convert.ToString(dr["IdopPeruShop"]).Trim();
                    reporte.Establecimiento = Convert.ToString(dr["Apodo"]).Trim();
                    reporte.Fecha = Convert.ToString(format);
                    reporte.NombreProducto = Convert.ToString(dr["Nombre"]).Trim();
                    reporte.CantidadProducto = Convert.ToInt32(dr["CantiPeruShop"]);
                    if (dr["precioCDR"] == DBNull.Value)
                    {
                        reporte.PrecioCDR = Convert.ToDouble(0.0);
                    }
                    else
                    {
                        reporte.PrecioCDR = Convert.ToDouble(dr["precioCDR"]);
                    }
                    reporte.PuntosCDR = Convert.ToDouble(dr["Puntos"]);
                    reporte.PuntosTotal = Convert.ToDouble(dr["PuntosTotal"]);
                    if (dr["PRECIO"] == DBNull.Value)
                    {
                        reporte.MontoTotal = Convert.ToDouble(0.0);
                    }
                    else
                    {
                        reporte.MontoTotal = Convert.ToDouble(dr["PRECIO"]);
                    }
                    

                    listaReporte.Add(reporte);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ReportesDatos", "ListaReporteCDR", "USP_REPORTECDR", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaReporte;
        }

        public List<ReporteSimplexModelo> ListaReporteSimplex(string fecha1, string fecha2)
        {
            List<ReporteSimplexModelo> listaSimplex = new List<ReporteSimplexModelo>();
            DateTime pFecha = Convert.ToDateTime(fecha1);
            DateTime sFecha = Convert.ToDateTime(fecha2);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            string fecha2S = sFecha.ToString("yyyy/MM/dd");
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CSV_SIMPLEX", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHA1", fecha1S);
                cmd.Parameters.AddWithValue("@FECHA2", fecha2S);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ReporteSimplexModelo simplex = new ReporteSimplexModelo();
                    DateTime fechaS = Convert.ToDateTime(dr["FECHA"]);
                    //var fechaC = fechaS.ToShortDateString();
                    string format = fechaS.ToString("yyyy/MM/dd");
                    simplex.DNICliente = Convert.ToString(dr["DNI"]).Trim();
                    simplex.DNIPatrocinador = Convert.ToString(dr["DNIPATRO"]).Trim();
                    simplex.idPaquete = Convert.ToString(dr["PAQUETE"]);
                    simplex.TipoCompra = Convert.ToString(dr["TIPOCOMPRA"]);
                    simplex.PuntosTotal = Convert.ToInt32(dr["PUNTOS"]);
                    if (dr["MONTO"] == DBNull.Value)
                    {
                        simplex.montoComision = 0.00;
                    }
                    else
                    {
                        simplex.montoComision = Convert.ToDouble(dr["MONTO"]);
                    }
                    simplex.IdopPeruShop = Convert.ToString(dr["IDOP"]).Trim();
                    simplex.fechaSimplex = Convert.ToString(format).Trim();
                    simplex.NombreCliente = Convert.ToString(dr["NOMBRES"]).Trim();

                    listaSimplex.Add(simplex);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ReportesDatos", "ListaReporteSimplex", "USP_LISTA_CSV_SIMPLEX", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaSimplex;
        }

    }
}
