﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;
using System.Data;
using System.Data.SqlClient;

namespace Datos
{
    public class PaqueteDAO
    {
        #region "PATRON SINGLETON"
        private static PaqueteDAO _paqueteDatos = null;
        private PaqueteDAO() { }
        public static PaqueteDAO getInstance()
        {
            if (_paqueteDatos == null)
            {
                _paqueteDatos = new PaqueteDAO();
            }
            return _paqueteDatos;
        }
        #endregion

        SqlConnection conexion = null;
        SqlDataReader dr = null;
        SqlCommand cmd = null;
        string errores = "";

        public bool RegistroPaquete(PaqueteNatura objPaquete, PaquetePais objPaquetePais)
        {
            SqlConnection con = null;
            SqlCommand cmdPaquete = null;
            SqlCommand cmdPPais = null;
            bool response = false;

            string cpaquete = "";
            cpaquete = GenerarCodigoGenerado();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmdPaquete = new SqlCommand("USP_REGISTROPAQUETENATURA", con);
                cmdPaquete.CommandType = CommandType.StoredProcedure;
                cmdPaquete.Parameters.AddWithValue("@NOMBRE", objPaquete.nombre);
                cmdPaquete.Parameters.AddWithValue("@OBSERVACION", objPaquete.observacion);
                con.Open();
                int filas = cmdPaquete.ExecuteNonQuery();

                cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                cmdPPais.CommandType = CommandType.StoredProcedure;
                cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPP);
                cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoPP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5PP);
                cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Peru);
                cmdPPais.ExecuteNonQuery();

                if (objPaquetePais.idProductoPais1Bolivia != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPBolivia);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoBolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Bolivia);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idProductoPais1Ecuador != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPEcuador);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoEcuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Ecuador);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idProductoPais1Panama != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPPanama);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoPanama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Panama);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idProductoPais1CR != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPCR);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoCR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5CR);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idProductoPais1Argentina != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPArgentina);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoArgentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Argentina);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idProductoPais1Chile != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", cpaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", objPaquetePais.paisPChile);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoChile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Chile);
                    cmdPPais.ExecuteNonQuery();
                }

                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PaqueteDAO", "RegistroPaquete", "USP_REGISTROPAQUETEPP", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        public string GenerarCodigoGenerado()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;

            con = Conexion.getInstance().Conectar();
            con.Open();

            cmd = new SqlCommand("SP_GenerarCodigoPaquete", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CodGenerado", SqlDbType.VarChar, 8).Direction = ParameterDirection.Output;

            string cproducto = "";

            try
            {
                cmd.ExecuteNonQuery();
                cproducto = Convert.ToString(cmd.Parameters["@CodGenerado"].Value);
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PaqueteDAO", "GenerarCodigoGenerado", "SP_GenerarCodigoPaquete", ex.Message);
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Dispose();
                cmd.Dispose();
            }

            return cproducto;
        }

        public bool ActualizarPaquete(PaqueteNatura objPaquete, PaquetePais objPaquetePais)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlCommand cmdPPais = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARPAQUETE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                cmd.Parameters.AddWithValue("@NOMBRE", objPaquete.nombre);
                cmd.Parameters.AddWithValue("@OBSERVACION", objPaquete.observacion);
                con.Open();
                int filas = cmd.ExecuteNonQuery();

                cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                cmdPPais.CommandType = CommandType.StoredProcedure;
                cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePais);
                cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoPP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4PP);
                cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4PP);
                cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5PP);
                cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Peru);
                cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Peru);
                cmdPPais.ExecuteNonQuery();

                if (objPaquetePais.idPaquetePaisBolivia != "")
                {
                    cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePaisBolivia);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoBolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Bolivia);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisEcuador != "")
                {
                    cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePaisEcuador);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoEcuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Ecuador);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisPanama != "")
                {
                    cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePaisPanama);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoPanama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Panama);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisCR != "")
                {
                    cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePaisCR);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoCR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5CR);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisArgentina != "")
                {
                    cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePaisArgentina);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoArgentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Argentina);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisChile != "")
                {
                    cmdPPais = new SqlCommand("USP_ACTUALIZARPAQUETEPAIS", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETEPP", objPaquetePais.idPaquetePaisChile);
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoChile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Chile);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisBolivia == "" & objPaquetePais.idProductoPais1Bolivia != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", "02");
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoBolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Bolivia);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Bolivia);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisEcuador == "" & objPaquetePais.idProductoPais1Ecuador != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", "03");
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoEcuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Ecuador);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Ecuador);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisPanama == "" & objPaquetePais.idProductoPais1Panama != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", "07");
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoPanama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Panama);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Panama);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Panama);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Panama);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisCR == "" & objPaquetePais.idProductoPais1CR != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", "06");
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoCR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4CR);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4CR);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4CR);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5CR);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisArgentina == "" & objPaquetePais.idProductoPais1Argentina != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", "04");
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoArgentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Argentina);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Argentina);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Argentina);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Argentina);
                    cmdPPais.ExecuteNonQuery();
                }

                if (objPaquetePais.idPaquetePaisChile == "" & objPaquetePais.idProductoPais1Chile != "")
                {
                    cmdPPais = new SqlCommand("USP_REGISTROPAQUETEPP", con);
                    cmdPPais.CommandType = CommandType.StoredProcedure;
                    cmdPPais.Parameters.AddWithValue("@IDPAQUETE", objPaquete.idPaquete);
                    cmdPPais.Parameters.AddWithValue("@CODIGOPAIS", "05");
                    cmdPPais.Parameters.AddWithValue("@ESTADO", objPaquetePais.estadoChile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO1", objPaquetePais.idProductoPais1Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO2", objPaquetePais.idProductoPais2Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO3", objPaquetePais.idProductoPais3Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO4", objPaquetePais.idProductoPais4Chile);
                    cmdPPais.Parameters.AddWithValue("@IDPRODUCTO5", objPaquetePais.idProductoPais5Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD1", objPaquetePais.cantidad1Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD2", objPaquetePais.cantidad2Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD3", objPaquetePais.cantidad3Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD4", objPaquetePais.cantidad4Chile);
                    cmdPPais.Parameters.AddWithValue("@CANTIDAD5", objPaquetePais.cantidad5Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE1", objPaquetePais.nombre1Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE2", objPaquetePais.nombre2Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE3", objPaquetePais.nombre3Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE4", objPaquetePais.nombre4Chile);
                    cmdPPais.Parameters.AddWithValue("@NOMBRE5", objPaquetePais.nombre5Chile);
                    cmdPPais.ExecuteNonQuery();
                }


                if (filas > 0) ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PaqueteDAO", "ActualizarPaquete", "USP_REGISTROPAQUETEPP", ex.Message);
                throw ex;
                ok = false;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<PaqueteNatura> ListarPaquetes()
        {
            List<PaqueteNatura> Lista = new List<PaqueteNatura>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPAQUETES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PaqueteNatura objPaquete = new PaqueteNatura();
                    objPaquete.idPaquete = dr["IDPAQUETE"].ToString().Trim();
                    objPaquete.nombre = dr["NOMBRE"].ToString().Trim();
                    objPaquete.observacion = dr["OBSERVACION"].ToString().Trim();


                    objPaquete.paquetePais = dr["IDPAQUETEPAISPE"].ToString().Trim();
                    objPaquete.estado = Convert.ToBoolean(dr["ESTADOPE"].ToString().Trim());
                    objPaquete.idProductoPais1 = dr["PRODUCTOPAIS1PE"].ToString().Trim();
                    objPaquete.idProductoPais2 = dr["PRODUCTOPAIS2PE"].ToString().Trim();
                    objPaquete.idProductoPais3 = dr["PRODUCTOPAIS3PE"].ToString().Trim();
                    objPaquete.idProductoPais4 = dr["PRODUCTOPAIS4PE"].ToString().Trim();
                    objPaquete.idProductoPais5 = dr["PRODUCTOPAIS5PE"].ToString().Trim();
                    objPaquete.cantidad1 = Convert.ToDouble(dr["CANTIDAD1PE"].ToString().Trim());
                    objPaquete.cantidad2 = Convert.ToDouble(dr["CANTIDAD2PE"].ToString().Trim());
                    objPaquete.cantidad3 = Convert.ToDouble(dr["CANTIDAD3PE"].ToString().Trim());
                    objPaquete.cantidad4 = Convert.ToDouble(dr["CANTIDAD4PE"].ToString().Trim());
                    objPaquete.cantidad5 = Convert.ToDouble(dr["CANTIDAD5PE"].ToString().Trim());
                    objPaquete.nombre1Peru = dr["NOMBRE1PE"].ToString().Trim();
                    objPaquete.nombre2Peru = dr["NOMBRE2PE"].ToString().Trim();
                    objPaquete.nombre3Peru = dr["NOMBRE3PE"].ToString().Trim();
                    objPaquete.nombre4Peru = dr["NOMBRE4PE"].ToString().Trim();
                    objPaquete.nombre5Peru = dr["NOMBRE5PE"].ToString().Trim();


                    if (dr["PRODUCTOPAIS1BO"].ToString().Trim() != "" | dr["PRODUCTOPAIS2BO"].ToString().Trim() != "")
                    {
                        objPaquete.idPaquetePaisBolivia = dr["IDPAQUETEPAISBO"].ToString().Trim();
                        objPaquete.estadoBolivia = Convert.ToBoolean(dr["ESTADOBO"].ToString().Trim());
                        objPaquete.idProductoPais1Bolivia = dr["PRODUCTOPAIS1BO"].ToString().Trim();
                        objPaquete.idProductoPais2Bolivia = dr["PRODUCTOPAIS2BO"].ToString().Trim();
                        objPaquete.idProductoPais3Bolivia = dr["PRODUCTOPAIS3BO"].ToString().Trim();
                        objPaquete.idProductoPais4Bolivia = dr["PRODUCTOPAIS4BO"].ToString().Trim();
                        objPaquete.idProductoPais5Bolivia = dr["PRODUCTOPAIS5BO"].ToString().Trim();
                        objPaquete.cantidad1Bolivia = Convert.ToDouble(dr["CANTIDAD1BO"].ToString().Trim());
                        objPaquete.cantidad2Bolivia = Convert.ToDouble(dr["CANTIDAD2BO"].ToString().Trim());
                        objPaquete.cantidad3Bolivia = Convert.ToDouble(dr["CANTIDAD3BO"].ToString().Trim());
                        objPaquete.cantidad4Bolivia = Convert.ToDouble(dr["CANTIDAD4BO"].ToString().Trim());
                        objPaquete.cantidad5Bolivia = Convert.ToDouble(dr["CANTIDAD5BO"].ToString().Trim());
                        objPaquete.nombre1Bolivia = dr["NOMBRE1BO"].ToString().Trim();
                        objPaquete.nombre2Bolivia = dr["NOMBRE2BO"].ToString().Trim();
                        objPaquete.nombre3Bolivia = dr["NOMBRE3BO"].ToString().Trim();
                        objPaquete.nombre4Bolivia = dr["NOMBRE4BO"].ToString().Trim();
                        objPaquete.nombre5Bolivia = dr["NOMBRE5BO"].ToString().Trim();
                    }


                    if (dr["PRODUCTOPAIS1EC"].ToString().Trim() != "" | dr["PRODUCTOPAIS2EC"].ToString().Trim() != "")
                    {
                        objPaquete.idPaquetePaisEcuador = dr["IDPAQUETEPAISEC"].ToString().Trim();
                        objPaquete.estadoEcuador = Convert.ToBoolean(dr["ESTADOEC"].ToString().Trim());
                        objPaquete.idProductoPais1Ecuador = dr["PRODUCTOPAIS1EC"].ToString().Trim();
                        objPaquete.idProductoPais2Ecuador = dr["PRODUCTOPAIS2EC"].ToString().Trim();
                        objPaquete.idProductoPais3Ecuador = dr["PRODUCTOPAIS3EC"].ToString().Trim();
                        objPaquete.idProductoPais4Ecuador = dr["PRODUCTOPAIS4EC"].ToString().Trim();
                        objPaquete.idProductoPais5Ecuador = dr["PRODUCTOPAIS5EC"].ToString().Trim();
                        objPaquete.cantidad1Ecuador = Convert.ToDouble(dr["CANTIDAD1EC"].ToString().Trim());
                        objPaquete.cantidad2Ecuador = Convert.ToDouble(dr["CANTIDAD2EC"].ToString().Trim());
                        objPaquete.cantidad3Ecuador = Convert.ToDouble(dr["CANTIDAD3EC"].ToString().Trim());
                        objPaquete.cantidad4Ecuador = Convert.ToDouble(dr["CANTIDAD4EC"].ToString().Trim());
                        objPaquete.cantidad5Ecuador = Convert.ToDouble(dr["CANTIDAD5EC"].ToString().Trim());
                        objPaquete.nombre1Ecuador = dr["NOMBRE1EC"].ToString().Trim();
                        objPaquete.nombre2Ecuador = dr["NOMBRE2EC"].ToString().Trim();
                        objPaquete.nombre3Ecuador = dr["NOMBRE3EC"].ToString().Trim();
                        objPaquete.nombre4Ecuador = dr["NOMBRE4EC"].ToString().Trim();
                        objPaquete.nombre5Ecuador = dr["NOMBRE5EC"].ToString().Trim();
                    }


                    if (dr["PRODUCTOPAIS1AR"].ToString().Trim() != "" | dr["PRODUCTOPAIS2AR"].ToString().Trim() != "")
                    {
                        objPaquete.idPaquetePaisArgentina = dr["IDPAQUETEPAISAR"].ToString().Trim();
                        objPaquete.estadoArgentina = Convert.ToBoolean(dr["ESTADOAR"].ToString().Trim());
                        objPaquete.idProductoPais1Argentina = dr["PRODUCTOPAIS1AR"].ToString().Trim();
                        objPaquete.idProductoPais2Argentina = dr["PRODUCTOPAIS2AR"].ToString().Trim();
                        objPaquete.idProductoPais3Argentina = dr["PRODUCTOPAIS3AR"].ToString().Trim();
                        objPaquete.idProductoPais4Argentina = dr["PRODUCTOPAIS4AR"].ToString().Trim();
                        objPaquete.idProductoPais5Argentina = dr["PRODUCTOPAIS5AR"].ToString().Trim();
                        objPaquete.cantidad1Argentina = Convert.ToDouble(dr["CANTIDAD1AR"].ToString().Trim());
                        objPaquete.cantidad2Argentina = Convert.ToDouble(dr["CANTIDAD2AR"].ToString().Trim());
                        objPaquete.cantidad3Argentina = Convert.ToDouble(dr["CANTIDAD3AR"].ToString().Trim());
                        objPaquete.cantidad4Argentina = Convert.ToDouble(dr["CANTIDAD4AR"].ToString().Trim());
                        objPaquete.cantidad5Argentina = Convert.ToDouble(dr["CANTIDAD5AR"].ToString().Trim());
                        objPaquete.nombre1Argentina = dr["NOMBRE1AR"].ToString().Trim();
                        objPaquete.nombre2Argentina = dr["NOMBRE2AR"].ToString().Trim();
                        objPaquete.nombre3Argentina = dr["NOMBRE3AR"].ToString().Trim();
                        objPaquete.nombre4Argentina = dr["NOMBRE4AR"].ToString().Trim();
                        objPaquete.nombre5Argentina = dr["NOMBRE5AR"].ToString().Trim();
                    }


                    if (dr["PRODUCTOPAIS1CH"].ToString().Trim() != "" | dr["PRODUCTOPAIS2CH"].ToString().Trim() != "")
                    {
                        objPaquete.idPaquetePaisChile = dr["IDPAQUETEPAISCH"].ToString().Trim();
                        objPaquete.estadoChile = Convert.ToBoolean(dr["ESTADOCH"].ToString().Trim());
                        objPaquete.idProductoPais1Chile = dr["PRODUCTOPAIS1CH"].ToString().Trim();
                        objPaquete.idProductoPais2Chile = dr["PRODUCTOPAIS2CH"].ToString().Trim();
                        objPaquete.idProductoPais3Chile = dr["PRODUCTOPAIS3CH"].ToString().Trim();
                        objPaquete.idProductoPais4Chile = dr["PRODUCTOPAIS4CH"].ToString().Trim();
                        objPaquete.idProductoPais5Chile = dr["PRODUCTOPAIS5CH"].ToString().Trim();
                        objPaquete.cantidad1Chile = Convert.ToDouble(dr["CANTIDAD1CH"].ToString().Trim());
                        objPaquete.cantidad2Chile = Convert.ToDouble(dr["CANTIDAD2CH"].ToString().Trim());
                        objPaquete.cantidad3Chile = Convert.ToDouble(dr["CANTIDAD3CH"].ToString().Trim());
                        objPaquete.cantidad4Chile = Convert.ToDouble(dr["CANTIDAD4CH"].ToString().Trim());
                        objPaquete.cantidad5Chile = Convert.ToDouble(dr["CANTIDAD5CH"].ToString().Trim());
                        objPaquete.nombre1Chile = dr["NOMBRE1CH"].ToString().Trim();
                        objPaquete.nombre2Chile = dr["NOMBRE2CH"].ToString().Trim();
                        objPaquete.nombre3Chile = dr["NOMBRE3CH"].ToString().Trim();
                        objPaquete.nombre4Chile = dr["NOMBRE4CH"].ToString().Trim();
                        objPaquete.nombre5Chile = dr["NOMBRE5CH"].ToString().Trim();
                    }


                    if (dr["PRODUCTOPAIS1CR"].ToString().Trim() != "" | dr["PRODUCTOPAIS2CR"].ToString().Trim() != "")
                    {
                        objPaquete.idPaquetePaisCR = dr["IDPAQUETEPAISCR"].ToString().Trim();
                        objPaquete.estadoCR = Convert.ToBoolean(dr["ESTADOCR"].ToString().Trim());
                        objPaquete.idProductoPais1CR = dr["PRODUCTOPAIS1CR"].ToString().Trim();
                        objPaquete.idProductoPais2CR = dr["PRODUCTOPAIS2CR"].ToString().Trim();
                        objPaquete.idProductoPais3CR = dr["PRODUCTOPAIS3CR"].ToString().Trim();
                        objPaquete.idProductoPais4CR = dr["PRODUCTOPAIS4CR"].ToString().Trim();
                        objPaquete.idProductoPais5CR = dr["PRODUCTOPAIS5CR"].ToString().Trim();
                        objPaquete.cantidad1CR = Convert.ToDouble(dr["CANTIDAD1CR"].ToString().Trim());
                        objPaquete.cantidad2CR = Convert.ToDouble(dr["CANTIDAD2CR"].ToString().Trim());
                        objPaquete.cantidad3CR = Convert.ToDouble(dr["CANTIDAD3CR"].ToString().Trim());
                        objPaquete.cantidad4CR = Convert.ToDouble(dr["CANTIDAD4CR"].ToString().Trim());
                        objPaquete.cantidad5CR = Convert.ToDouble(dr["CANTIDAD5CR"].ToString().Trim());
                        objPaquete.nombre1CR = dr["NOMBRE1CR"].ToString().Trim();
                        objPaquete.nombre2CR = dr["NOMBRE2CR"].ToString().Trim();
                        objPaquete.nombre3CR = dr["NOMBRE3CR"].ToString().Trim();
                        objPaquete.nombre4CR = dr["NOMBRE4CR"].ToString().Trim();
                        objPaquete.nombre5CR = dr["NOMBRE5CR"].ToString().Trim();
                    }


                    if (dr["PRODUCTOPAIS1PA"].ToString() != "" | dr["PRODUCTOPAIS2PA"].ToString() != "")
                    {
                        objPaquete.idPaquetePaisPanama = dr["IDPAQUETEPAISPA"].ToString().Trim();
                        objPaquete.estadoPanama = Convert.ToBoolean(dr["ESTADOPA"].ToString().Trim());
                        objPaquete.idProductoPais1Panama = dr["PRODUCTOPAIS1PA"].ToString().Trim();
                        objPaquete.idProductoPais2Panama = dr["PRODUCTOPAIS2PA"].ToString().Trim();
                        objPaquete.idProductoPais3Panama = dr["PRODUCTOPAIS3PA"].ToString().Trim();
                        objPaquete.idProductoPais4Panama = dr["PRODUCTOPAIS4PA"].ToString().Trim();
                        objPaquete.idProductoPais5Panama = dr["PRODUCTOPAIS5PA"].ToString().Trim();
                        objPaquete.cantidad1Panama = Convert.ToDouble(dr["CANTIDAD1PA"].ToString().Trim());
                        objPaquete.cantidad2Panama = Convert.ToDouble(dr["CANTIDAD2PA"].ToString().Trim());
                        objPaquete.cantidad3Panama = Convert.ToDouble(dr["CANTIDAD3PA"].ToString().Trim());
                        objPaquete.cantidad4Panama = Convert.ToDouble(dr["CANTIDAD4PA"].ToString().Trim());
                        objPaquete.cantidad5Panama = Convert.ToDouble(dr["CANTIDAD5PA"].ToString().Trim());
                        objPaquete.nombre1Panama = dr["NOMBRE1PA"].ToString().Trim();
                        objPaquete.nombre2Panama = dr["NOMBRE2PA"].ToString().Trim();
                        objPaquete.nombre3Panama = dr["NOMBRE3PA"].ToString().Trim();
                        objPaquete.nombre4Panama = dr["NOMBRE4PA"].ToString().Trim();
                        objPaquete.nombre5Panama = dr["NOMBRE5PA"].ToString().Trim();
                    }

                    Lista.Add(objPaquete);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PaqueteDAO", "ListarPaquetes", "USP_LISTADOPAQUETES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<PaqueteNatura> ListarPaqueteByCodigoPeru(string idPaquete)
        {
            List<PaqueteNatura> Lista = new List<PaqueteNatura>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int CA = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPAQUETEBYCODIGO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPAQUETE", idPaquete);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    string ID1 = Convert.ToString(dr["ID1"]);
                    string ID2 = Convert.ToString(dr["ID2"]);
                    string ID3 = Convert.ToString(dr["ID3"]);
                    string ID4 = Convert.ToString(dr["ID4"]);
                    string ID5 = Convert.ToString(dr["ID5"]);

                    PaqueteNatura objPaquete = new PaqueteNatura();
                    if (ID1.Trim() != "")
                    {
                        objPaquete = new PaqueteNatura();
                        objPaquete.idProductoGeneral = dr["ID1"].ToString().Trim();
                        objPaquete.cantidadGeneral = Convert.ToInt32(dr["CAN1"]);
                        objPaquete.cantidadAglomerada = Convert.ToInt32(dr["SUMAGENERAL"]);
                        Lista.Add(objPaquete);
                    }
                    if (ID2.Trim() != "")
                    {
                        objPaquete = new PaqueteNatura();
                        objPaquete.idProductoGeneral = dr["ID2"].ToString().Trim();
                        objPaquete.cantidadGeneral = Convert.ToInt32(dr["CAN2"]);
                        objPaquete.cantidadAglomerada = Convert.ToInt32(dr["SUMAGENERAL"]);
                        Lista.Add(objPaquete);
                    }
                    if (ID3.Trim() != "")
                    {
                        objPaquete = new PaqueteNatura();
                        objPaquete.idProductoGeneral = dr["ID3"].ToString().Trim();
                        objPaquete.cantidadGeneral = Convert.ToInt32(dr["CAN3"]);
                        objPaquete.cantidadAglomerada = Convert.ToInt32(dr["SUMAGENERAL"]);
                        Lista.Add(objPaquete);
                    }
                    if (ID4.Trim() != "")
                    {
                        objPaquete = new PaqueteNatura();
                        objPaquete.idProductoGeneral = dr["ID4"].ToString().Trim();
                        objPaquete.cantidadGeneral = Convert.ToInt32(dr["CAN4"]);
                        objPaquete.cantidadAglomerada = Convert.ToInt32(dr["SUMAGENERAL"]);
                        Lista.Add(objPaquete);
                    }
                    if (ID5.Trim() != "")
                    {
                        objPaquete = new PaqueteNatura();
                        objPaquete.idProductoGeneral = dr["ID5"].ToString().Trim();
                        objPaquete.cantidadGeneral = Convert.ToInt32(dr["CAN5"]);
                        objPaquete.cantidadAglomerada = Convert.ToInt32(dr["SUMAGENERAL"]);
                        Lista.Add(objPaquete);
                    }
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PaqueteDAO", "ListarPaqueteByCodigoPeru", "USP_LISTADOPAQUETEBYCODIGO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

    }
}
