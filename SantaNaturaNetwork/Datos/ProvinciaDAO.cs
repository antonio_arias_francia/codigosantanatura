﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class ProvinciaDAO
    {
        #region "PATRON SINGLETON"
        private static ProvinciaDAO daoProvincia = null;
        private ProvinciaDAO() { }
        public static ProvinciaDAO getInstance()
        {
            if (daoProvincia == null)
            {
                daoProvincia = new ProvinciaDAO();
            }
            return daoProvincia;
        }
        #endregion

        public List<Provincia> ListarProvincia(string departamento)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Provincia> listaProvincia = new List<Provincia>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPROVINCIA",con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DEPARTAMENTO", departamento);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Provincia provincia = new Provincia();
                    provincia.Codigo = dr["Codigo"].ToString();
                    provincia.Nombre = dr["Nombre"].ToString();
                    listaProvincia.Add(provincia);
                }

            }catch(Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProvinciaDAO", "ListarProvincia", "USP_LISTADOPROVINCIA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return listaProvincia;
        }

        public List<Provincia> GetProvincia()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Provincia> listaProvincia = new List<Provincia>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GETPROVINCIA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Provincia provincia = new Provincia();
                    provincia.Codigo = dr["Codigo"].ToString();
                    provincia.CodigoDepartamento = dr["CodigoDepartamento"].ToString();
                    provincia.Nombre = dr["Nombre"].ToString();
                    listaProvincia.Add(provincia);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProvinciaDAO", "GetProvincia", "USP_GETPROVINCIA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return listaProvincia;
        }

    }
}
