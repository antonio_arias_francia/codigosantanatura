﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class PeriodoDAO
    {
        #region "PATRON SINGLETON"
        private static PeriodoDAO _periodoDatos = null;
        private PeriodoDAO() { }
        public static PeriodoDAO getInstance()
        {
            if (_periodoDatos == null)
            {
                _periodoDatos = new PeriodoDAO();
            }
            return _periodoDatos;
        }
        #endregion

        public bool RegistroPeriodo(Periodo objPeriodo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRARPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHAINICIO", objPeriodo.fechaInicio);
                cmd.Parameters.AddWithValue("@FECHAFIN", objPeriodo.fechaFin);
                cmd.Parameters.AddWithValue("@NOMBRE", objPeriodo.nombre);
                cmd.Parameters.AddWithValue("@ESTADO", objPeriodo.estado);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroPeriodo", "USP_REGISTRARPERIODO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarPeriodo(Periodo objPeriodo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", objPeriodo.idPeriodo);
                cmd.Parameters.AddWithValue("@FECHAINICIO", objPeriodo.fechaInicio);
                cmd.Parameters.AddWithValue("@FECHAFIN", objPeriodo.fechaFin);
                cmd.Parameters.AddWithValue("@NOMBRE", objPeriodo.nombre);
                cmd.Parameters.AddWithValue("@ESTADO", objPeriodo.estado);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarPeriodo", "USP_UPDATEPERIODO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Periodo> ListarPeriodo()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTARPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    objPeriodo.fechaInicio = dr["FECHAINICIO"].ToString();
                    objPeriodo.fechaFin = dr["FECHAFIN"].ToString();
                    objPeriodo.nombre = dr["NOMBRE"].ToString();
                    objPeriodo.estado = Convert.ToBoolean(dr["ESTADO"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarPeriodo", "USP_LISTARPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string ListaCodigoPeriodoActivo()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string codigo = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTAPERIODOACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"].ToString());
                    codigo = Convert.ToString(objPeriodo.idPeriodo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaCodigoPeriodoActivo", "LISTAPERIODOACTIVO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return codigo;
        }

        public int ListaCantPeriodo()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTACANTPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    cantidad = objPeriodo.cantidad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaCantPeriodo", "LISTACANTPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public bool RegistroCLientePeriodo(string documento, string upline, string idCliente, string idUpline, 
                                           string TipoC, string patrocinador)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCALCULOPUNTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@UPLINE", upline);
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente);
                cmd.Parameters.AddWithValue("@IDSOCIOUPLINE", idUpline);
                cmd.Parameters.AddWithValue("@TIPOC", TipoC);
                cmd.Parameters.AddWithValue("@PATROCINADOR", patrocinador);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroCLientePeriodo", "USP_REGISTROCALCULOPUNTOS", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarDocumentoPeriodo(string documento, string idCliente)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATESOCIOPUNTAJE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarDocumentoPeriodo", "USP_UPDATESOCIOPUNTAJE", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarUplinePeriodo(string documento, string idCliente, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEUPLINEPUNTAJE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UPLINE", documento.Trim());
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente.Trim());
                cmd.Parameters.AddWithValue("@IDPERIODO", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarUplinePeriodo", "USP_UPDATEUPLINEPUNTAJE", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool RegistroCLientePeriodoActivo(string documento, string upline, string tipoC)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCALCULOPUNTOSACTIVO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@UPLINE", upline.Trim());
                cmd.Parameters.AddWithValue("@TIPOC", tipoC.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroCLientePeriodoActivo", "USP_REGISTROCALCULOPUNTOSACTIVO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool RegistroCLientePeriodoMultiple(string documento, string upline, int idperiodo, string TipoC, string patrocinador)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCALCULOPUNTOSMULTIPLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@UPLINE", upline.Trim());
                cmd.Parameters.AddWithValue("@PATROCINADOR", patrocinador.Trim());
                cmd.Parameters.AddWithValue("@IDP", idperiodo);
                cmd.Parameters.AddWithValue("@TIPOC", TipoC);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroCLientePeriodoMultiple", "USP_REGISTROCALCULOPUNTOSMULTIPLE", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Periodo> ListarIdPeriodo()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USPLISTAPERIODOMULTIPLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarIdPeriodo", "USPLISTAPERIODOMULTIPLE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListarIdPeriodoTop2()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USPLISTAPERIODOMULTIPLE_TOP2", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarIdPeriodoTop2", "USPLISTAPERIODOMULTIPLE_TOP2", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListaPeriodosTotales()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    objPeriodo.nombre = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaPeriodosTotales", "USP_LISTAPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListaPeriodosTotalesByCliente()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPERIODOBYCLIENTE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    objPeriodo.nombre = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaPeriodosTotalesByCliente", "USP_LISTAPERIODOBYCLIENTE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        /*REGISTRO DE PERIODO COMISION*/

        public bool RegistroPeriodoComision(Periodo objPeriodo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRARPERIODOCOMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FECHAINICIO", objPeriodo.fechaInicio);
                cmd.Parameters.AddWithValue("@FECHAFIN", objPeriodo.fechaFin);
                cmd.Parameters.AddWithValue("@NOMBRE", objPeriodo.nombre);
                cmd.Parameters.AddWithValue("@ESTADO", objPeriodo.estado);
                cmd.Parameters.AddWithValue("@IDPERIODO_RANGO", objPeriodo.idPeriodo);
                cmd.Parameters.AddWithValue("@VISUALIZAR", objPeriodo.visualizar);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroPeriodoComision", "USP_REGISTRARPERIODOCOMISION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarPeriodoComision(Periodo objPeriodo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEPERIODO_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO", objPeriodo.idPeriodoComision);
                cmd.Parameters.AddWithValue("@FECHAINICIO", objPeriodo.fechaInicio);
                cmd.Parameters.AddWithValue("@FECHAFIN", objPeriodo.fechaFin);
                cmd.Parameters.AddWithValue("@NOMBRE", objPeriodo.nombre);
                cmd.Parameters.AddWithValue("@ESTADO", objPeriodo.estado);
                cmd.Parameters.AddWithValue("@IDPERIODO_RANGO", objPeriodo.idPeriodo);
                cmd.Parameters.AddWithValue("@VISUALIZAR", objPeriodo.visualizar);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarPeriodoComision", "USP_UPDATEPERIODO_COMISION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Periodo> ListarPeriodoComision()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTARPERIODO_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_RANGO"]);
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.fechaInicio = dr["FECHAINICIO"].ToString();
                    objPeriodo.fechaFin = dr["FECHAFIN"].ToString();
                    objPeriodo.nombre = dr["NOMBRE"].ToString();
                    objPeriodo.estado = Convert.ToBoolean(dr["ESTADO"]);
                    objPeriodo.visualizar = Convert.ToBoolean(dr["VISUALIZAR"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarPeriodoComision", "USP_LISTARPERIODO_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string ListaCodigoPeriodoActivoComision()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string codigo = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTAPERIODOACTIVO_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"].ToString());
                    codigo = Convert.ToString(objPeriodo.idPeriodo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaCodigoPeriodoActivoComision", "LISTAPERIODOACTIVO_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return codigo;
        }

        public int ListaCantPeriodoComision()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTACANTPERIODO_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.cantidad = Convert.ToInt32(dr["CANTIDAD"].ToString());
                    cantidad = objPeriodo.cantidad;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaCantPeriodoComision", "LISTAPERIODLISTACANTPERIODO_COMISIONOACTIVO_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public bool RegistroCLientePeriodoComision(string documento, string upline, string patrocinador, string idCliente, string idUpline, string TipoC)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCALCULO_COMISIONES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@UPLINE", upline);
                cmd.Parameters.AddWithValue("@PATROCINADOR", patrocinador);
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente);
                cmd.Parameters.AddWithValue("@IDSOCIOUPLINE", idUpline);
                cmd.Parameters.AddWithValue("@TIPOC", TipoC);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroCLientePeriodoComision", "USP_REGISTROCALCULO_COMISIONES", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool RegistroSociosRetencion(string idSocio, string dni)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GENERAR_SOCIOS_RETENCION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDSOCIO", idSocio);
                cmd.Parameters.AddWithValue("@DNI", dni);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroSociosRetencion", "USP_GENERAR_SOCIOS_RETENCION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarDocumentoPeriodoComision(string documento, string idCliente)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEDNI_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarDocumentoPeriodoComision", "USP_UPDATEDNI_COMISION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarDocumentoRetencion(string documento, string idCliente)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_DNI_RETENCION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente.Trim());
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarDocumentoRetencion", "USP_ACTUALIZAR_DNI_RETENCION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool ActualizarUplinePeriodoComision(string documento, string idCliente, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_UPDATEUPLINEPUNTAJE_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UPLINE", documento.Trim());
                cmd.Parameters.AddWithValue("@IDSOCIO", idCliente.Trim());
                cmd.Parameters.AddWithValue("@IDPERIODO", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ActualizarUplinePeriodoComision", "USP_UPDATEUPLINEPUNTAJE_COMISION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool RegistroCLientePeriodoMultipleComision(string documento, string upline, string patrocinador, int idperiodo, string TipoC)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTROCALCULOPUNTOSMULTIPLE_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());
                cmd.Parameters.AddWithValue("@UPLINE", upline.Trim());
                cmd.Parameters.AddWithValue("@PATROCINADOR", patrocinador.Trim());
                cmd.Parameters.AddWithValue("@IDP", idperiodo);
                cmd.Parameters.AddWithValue("@TIPOC", TipoC);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroCLientePeriodoMultipleComision", "USP_REGISTROCALCULOPUNTOSMULTIPLE_COMISION", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public bool RegistroSocioRetencionXPeriodo(string dni, int IDP)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool response = false;
            try
            {

                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_GENERAR_SOCIOS_RETENCIONXPERIODO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", dni.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                cmd.ExecuteNonQuery();
                response = true;

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "RegistroSocioRetencionXPeriodo", "USP_GENERAR_SOCIOS_RETENCIONXPERIODO", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return response;
        }

        public List<Periodo> ListarIdPeriodoComision()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USPLISTAPERIODOMULTIPLE_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarIdPeriodoComision", "USPLISTAPERIODOMULTIPLE_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListarPeriodoComisionTop3()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USPLISTAPERIODOMULTIPLE_COMISION_TOP3", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarPeriodoComisionTop3", "USPLISTAPERIODOMULTIPLE_COMISION_TOP3", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListarPeriodosCalculoComision()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USPLISTAPERIODOTOTAL_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarPeriodosCalculoComision", "USPLISTAPERIODOTOTAL_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListaPeriodoComisionCombo()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPERIODO_COMISION_ADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.nombre = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaPeriodoComisionCombo", "USP_LISTAPERIODO_COMISION_ADMIN", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListaPeriodosTotalesByClienteComision()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAPERIODOBYCLIENTE_COMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodo = Convert.ToInt32(dr["IDPERIODO"]);
                    objPeriodo.nombre = Convert.ToString(dr["NOMBRE"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListaPeriodosTotalesByClienteComision", "USP_LISTAPERIODOBYCLIENTE_COMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public int ObtenerPeriodoxComision(int IDP)
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int cantidad = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_PERIODOXCOMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"].ToString());
                    cantidad = objPeriodo.idPeriodoComision;
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ObtenerPeriodoxComision", "USP_OBTENER_PERIODOXCOMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return cantidad;
        }

        public List<Periodo> ListarUltimos4Periodos()
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_ULIMOS4_PERIODOSCOMISION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarUltimos4Periodos", "USP_LISTA_ULIMOS4_PERIODOSCOMISION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<Periodo> ListarUltimos4PeriodosRecalculo(int IDP)
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_ULIMOS4_PERIODOSCOMISION_RECALCULO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);

                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Periodo objPeriodo = new Periodo();
                    objPeriodo.idPeriodoComision = Convert.ToInt32(dr["IDPERIODO_COMISION"]);
                    objPeriodo.fechaInicio = Convert.ToString(dr["FECHAINICIO"]);
                    objPeriodo.fechaFin = Convert.ToString(dr["FECHAFIN"]);
                    Lista.Add(objPeriodo);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ListarUltimos4PeriodosRecalculo", "USP_LISTA_ULIMOS4_PERIODOSCOMISION_RECALCULO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public int ObtenerIdperiodoRango_Comi(int idPeriodoComi)
        {
            List<Periodo> Lista = new List<Periodo>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int periodo = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_IDPERIODO_RANGO_COMI", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPERIODO_COMI", idPeriodoComi);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    periodo = Convert.ToInt32(dr["IDPERIODO_RANGO"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("PeriodoDAO", "ObtenerIdperiodoRango_Comi", "USP_OBTENER_IDPERIODO_RANGO_COMI", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return periodo;
        }

    }
}
