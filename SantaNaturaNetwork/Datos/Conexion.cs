﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Datos
{
    public class Conexion
    {
        #region "PATRON SINGLETON"
        private static Conexion conexion = null;
        private Conexion() { }
        public static Conexion getInstance()
        {
            if (conexion == null)
            {
                conexion = new Conexion();
            }

            return conexion;
        }
        #endregion

        public SqlConnection Conectar()
        {
            SqlConnection conexion = new SqlConnection();
            conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["Conexion"].ConnectionString);
            return conexion;
        }
        public string RegistrarError(string capa, string metodo,string store_procedure, string error)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string result = "";

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RegistrarLogErrores", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Capa", capa);
                cmd.Parameters.AddWithValue("@Metodo", metodo);
                cmd.Parameters.AddWithValue("@StoreProcedure", store_procedure);
                cmd.Parameters.AddWithValue("@DescripcionError", error);


                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result = Convert.ToString(dr["result"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                //errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return result;
        }


    }
}
