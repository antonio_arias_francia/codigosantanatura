﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class DescuentoDatos
    {
        #region "PATRON SINGLETON"
        private static DescuentoDatos _descuentoDatos = null;
        private DescuentoDatos() { }
        public static DescuentoDatos getInstance()
        {
            if (_descuentoDatos == null)
            {
                _descuentoDatos = new DescuentoDatos();
            }
            return _descuentoDatos;
        }
        #endregion

        public bool RegistrarPacketeCliente(Descuento descuento)
        {
            bool response = false;
            SqlConnection cn = null;
            SqlCommand cmd = null;
            int resultado = 0;

            try
            {
                cn = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RegistrarPacketeCliente", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numDocumento", descuento.NumDocumentoCliente);
                cmd.Parameters.AddWithValue("@idPackete ", descuento.IdPackete);
                cmd.Parameters.AddWithValue("@fechaObtencionPackete", descuento.FechaObtencionPackete);

                resultado = cmd.ExecuteNonQuery();

                if (resultado > 0)
                {
                    response = true;
                }    
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DescuentoDatos", "RegistrarPacketeCliente", "USP_RegistrarPacketeCliente", ex.Message);
                throw ex;
            }

            return response;
        }
    }
}
