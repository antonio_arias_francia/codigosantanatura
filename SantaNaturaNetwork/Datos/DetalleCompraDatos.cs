﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class DetalleCompraDatos
    {
        #region "PATRON SINGLETON"
        private static DetalleCompraDatos _detalleCompraDatos = null;
        private DetalleCompraDatos() { }
        public static DetalleCompraDatos getInstance()
        {
            if (_detalleCompraDatos == null)
            {
                _detalleCompraDatos = new DetalleCompraDatos();
            }
            return _detalleCompraDatos;
        }
        #endregion

        string errores = "";

        public List<DetalleCompra> ListaDetalleCompraDelCliente(string ticket)
        {
            List<DetalleCompra> listaCompras = new List<DetalleCompra>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarDetalleCompraByTicket", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DetalleCompra dc = new DetalleCompra();
                    dc.Nombre = Convert.ToString(dr["Nombre"]);
                    dc.Foto = Convert.ToString(dr["Foto"]);
                    dc.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    dc.MontoTotalNeto = Convert.ToDouble(dr["SubTotal"]);
                    dc.IdProductoPeruShop = Convert.ToString(dr["IdProductoPeruShop"]);
                    dc.CantiPS = Convert.ToInt32(dr["CantiPeruShop"]);
                    dc.PrecioPS = Convert.ToDouble(dr["preciPeruShop"]);
                    dc.LineaSend = Convert.ToString(dr["idLinea"]);
                    dc.Puntos = Convert.ToInt32(dr["Puntos"]);
                    dc.precioStr = Convert.ToString(dr["precioStr"]);
                    dc.idPaquete = Convert.ToString(dr["IdPaquete"]);
                    dc.CodigoProductoDC = Convert.ToString(dr["CodigoProducto"]);
                    listaCompras.Add(dc);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListaDetalleCompraDelCliente", "USP_ListarDetalleCompraByTicket", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<DetalleCompra> ListaCompraPendientePagoDelCliente(string ticket)
        {
            List<DetalleCompra> listaCompras = new List<DetalleCompra>();

            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListaCompraPendientePagoByTicket", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DetalleCompra dc = new DetalleCompra();
                    dc.Nombre = Convert.ToString(dr["Nombre"]);
                    dc.Foto = Convert.ToString(dr["Foto"]);
                    dc.Cantidad = Convert.ToInt32(dr["Cantidad"]);
                    dc.MontoTotalNeto = Convert.ToDouble(dr["SubTotal"]);
                    dc.IdProductoPeruShop = Convert.ToString(dr["IdProductoPeruShop"]);
                    dc.FotoVoucher = Convert.ToString(dr["FotoVaucher"]);

                    listaCompras.Add(dc);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListaCompraPendientePagoDelCliente", "USP_ListaCompraPendientePagoByTicket", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<DetalleCompra> ListaDetalleCompraGeneral(string ticket)
        {
            List<DetalleCompra> listaCompras = new List<DetalleCompra>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("LISTARDETALLECOMPRAGENERAL", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ticket", ticket);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DetalleCompra dc = new DetalleCompra();
                    dc.Nombre = Convert.ToString(dr["Nombre"]);
                    dc.Foto = Convert.ToString(dr["Foto"]);
                    dc.IdProductoPeruShop = Convert.ToString(dr["IdProductoPeruShop"]);
                    dc.Puntos = Convert.ToInt32(dr["Puntos"]);
                    dc.Cantidad = Convert.ToInt32(dr["CantiPeruShop"]);
                    dc.SubTotalDC = Convert.ToDouble(dr["SubTotal"]);
                    dc.PrecioPS = Convert.ToInt32(dr["preciPeruShop"]);
                    dc.CodigoDC = Convert.ToString(dr["Codigo"]);
                    dc.PrecioUnitarioDC = Convert.ToDouble(dr["PrecioUnitario"]);
                    dc.CodigoProductoDC = Convert.ToString(dr["CodigoProducto"]);
                    dc.EstadoCOM = Convert.ToInt32(dr["Estado"]);
                    dc.TipoPagoCOM = Convert.ToString(dr["TipoPago"]);
                    dc.IdopCOM = Convert.ToString(dr["IdopPeruShop"]);
                    dc.CantidadCOM = Convert.ToInt32(dr["Cantidad"]);
                    dc.MontoTotalCOM = Convert.ToDouble(dr["MontoTotal"]);
                    dc.PuntosTotalCOM = Convert.ToDouble(dr["PuntosTotal"]);
                    dc.MontoPagarCOM = Convert.ToDouble(dr["MontoAPagar"]);
                    dc.MontoComisionCOM = (dr["montoComision"] == DBNull.Value) ? 0 : Convert.ToDouble(dr["montoComision"]); 
                    dc.TicketCOM = Convert.ToString(dr["Ticket"]);
                    dc.FechaCOM = Convert.ToDateTime(dr["FechaPago"]).ToShortDateString();
                    dc.TipoCompraCOM = Convert.ToString(dr["idTipoCompra"]);
                    dc.descDespacho = Convert.ToString(dr["Despacho"]);
                    listaCompras.Add(dc);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListaDetalleCompraGeneral", "LISTARDETALLECOMPRAGENERAL", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return listaCompras;
        }

        public List<DetalleCompra> ListadoTipoCompraDetalle()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<DetalleCompra> listaCombo = new List<DetalleCompra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTATIPOCOMPRADETALLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DetalleCompra combo = new DetalleCompra();
                    combo.idTipoCompra = dr["IdPackete"].ToString();
                    combo.nombreTipoCom = dr["Packete"].ToString();
                    listaCombo.Add(combo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListadoTipoCompraDetalle", "USP_LISTATIPOCOMPRADETALLE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCombo;
        }

        public List<DetalleCompra> ListadoTipoPagoDetalle()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<DetalleCompra> listaCombo = new List<DetalleCompra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTATIPOCPAGODETALLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DetalleCompra combo = new DetalleCompra();
                    combo.idTipoPago = dr["ID"].ToString();
                    combo.nombreTipoPago = dr["DESCRIPCION"].ToString();
                    listaCombo.Add(combo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListadoTipoPagoDetalle", "USP_LISTATIPOCPAGODETALLE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCombo;
        }

        public List<DetalleCompra> ListadoDespachoDetalle()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<DetalleCompra> listaCombo = new List<DetalleCompra>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADESPACHODETALLE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DetalleCompra combo = new DetalleCompra();
                    combo.idDespacho = dr["IdPeruShop"].ToString().Trim();
                    combo.descDespacho = dr["IdPeruShop"].ToString().Trim();
                    listaCombo.Add(combo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListadoDespachoDetalle", "USP_LISTADESPACHODETALLE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCombo;
        }

        public List<DetalleCompra> ListadoEstadoDetalle()
        {
            List<DetalleCompra> listaCombo = new List<DetalleCompra>();
            try
            {
                DetalleCompra combo = new DetalleCompra();
                combo.idEstado = 0;
                combo.descEstado = "Realizado";
                listaCombo.Add(combo);

                DetalleCompra combo2 = new DetalleCompra();
                combo2.idEstado = 1;
                combo2.descEstado = "Pendiente";
                listaCombo.Add(combo2);

                DetalleCompra combo3 = new DetalleCompra();
                combo3.idEstado = 2;
                combo3.descEstado = "Pendiente de Aprobacion";
                listaCombo.Add(combo3);

                DetalleCompra combo4 = new DetalleCompra();
                combo4.idEstado = 3;
                combo4.descEstado = "Anulado";
                listaCombo.Add(combo4);

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ListadoEstadoDetalle", "ListadoEstadoDetalle", ex.Message);
                throw ex;
            }
            return listaCombo;
        }

        public bool ActualizarCompraDetalle(DetalleCompra objCompra)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARCOMPRAADMIN", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", objCompra.TicketCOM);
                cmd.Parameters.AddWithValue("@MONTOPAGAR", objCompra.MontoPagarCOM);
                cmd.Parameters.AddWithValue("@MONTOCOMISION", objCompra.MontoComisionCOM);
                cmd.Parameters.AddWithValue("@PUNTOSTOTAL", objCompra.PuntosTotalCOM);
                cmd.Parameters.AddWithValue("@MONTOTOTAL", objCompra.MontoTotalCOM);
                cmd.Parameters.AddWithValue("@FECHA", objCompra.FechaCOM);
                cmd.Parameters.AddWithValue("@CANTIDADPRO", objCompra.CantidadCOM);
                cmd.Parameters.AddWithValue("@IDOP", objCompra.IdopCOM);
                cmd.Parameters.AddWithValue("@IDTIPOCOM", objCompra.idTipoCompra);
                cmd.Parameters.AddWithValue("@TIPOCOM", objCompra.nombreTipoCom);
                cmd.Parameters.AddWithValue("@TIPOPAGO", objCompra.idTipoPago);
                cmd.Parameters.AddWithValue("@ESTADO", objCompra.idEstado);
                cmd.Parameters.AddWithValue("@DESPACHO", objCompra.idDespacho);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "ActualizarCompraDetalle", "USP_ACTUALIZARCOMPRAADMIN", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool RetornarCompra(string ticket)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RETORNARCOMPRA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TICKET", ticket);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("DetalleCompraDatos", "RetornarCompra", "USP_RETORNARCOMPRA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

    }
}
