﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Modelos;

namespace Datos
{
    public class PaisDAO
    {
        #region "PATRON SINGLETON"
        private static PaisDAO daoPais = null;
        private PaisDAO() { }
        public static PaisDAO getInstance()
        {
            if (daoPais == null)
            {
                daoPais = new PaisDAO();
            }
            return daoPais;
        }
        #endregion

        public List<Pais> ListadoPais()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<Pais> listaPais = new List<Pais>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPAIS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Pais pais = new Pais();
                    pais.Codigo = dr["Codigo"].ToString();
                    pais.Nombre = dr["Nombre"].ToString();
                    listaPais.Add(pais);
                }

            }catch(Exception ex)
            {
                Conexion.getInstance().RegistrarError("PaisDAO", "ListadoPais", "USP_LISTADOPAIS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaPais;
        }
    }
}
