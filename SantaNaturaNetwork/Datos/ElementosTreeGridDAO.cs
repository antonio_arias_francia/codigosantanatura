﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;

namespace Datos
{
    public class ElementosTreeGridDAO
    {
        #region "PATRON SINGLETON"
        private static ElementosTreeGridDAO daoCliente = null;
        private ElementosTreeGridDAO() { }
        public static ElementosTreeGridDAO getInstance()
        {
            if (daoCliente == null)
            {
                daoCliente = new ElementosTreeGridDAO();
            }
            return daoCliente;
        }
        #endregion

        string errores = "";


        public List<ElementosTreeGrid> ListaTreeGrid(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTRUCTURATREEGRID", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.EmployeeID = Convert.ToInt64(dr["NumeroDoc"]);
                    objSocio.ReportsTo = Convert.ToInt64(dr["Upline"]);
                    objSocio.LastName = dr["Apellidos"].ToString();
                    objSocio.Country = dr["PP"].ToString();
                    objSocio.City = dr["VP"].ToString();
                    objSocio.VR = dr["VR"].ToString();
                    objSocio.Address = dr["VG"].ToString();
                    objSocio.Title = dr["VQ"].ToString();
                    objSocio.Rango = dr["RANGO"].ToString();
                    objSocio.Fecha = dr["FECHA"].ToString();
                    objSocio.Telefono = dr["TELEFONO"].ToString();
                    objSocio.Pais = dr["PAIS"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    objSocio.VIP = dr["VIP"].ToString();
                    objSocio.MAXIMORANGO = dr["MAXIMORANGO"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeGrid", "USP_ESTRUCTURATREEGRID", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeGridByPeriodo(string documento, int IDP)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTRUCTURATREEGRIDBYPERIODO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.EmployeeID = Convert.ToInt64(dr["NumeroDoc"]);
                    objSocio.ReportsTo = Convert.ToInt64(dr["Upline"]);
                    objSocio.LastName = dr["Apellidos"].ToString();
                    objSocio.Country = dr["PP"].ToString();
                    objSocio.City = dr["VP"].ToString();
                    objSocio.VR = dr["VR"].ToString();
                    objSocio.Address = dr["VG"].ToString();
                    objSocio.Title = dr["VQ"].ToString();
                    objSocio.Rango = dr["RANGO"].ToString();
                    objSocio.Fecha = dr["FECHA"].ToString();
                    objSocio.Telefono = dr["TELEFONO"].ToString();
                    objSocio.Pais = dr["PAIS"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    objSocio.VIP = dr["VIP"].ToString();
                    objSocio.MAXIMORANGO = dr["MAXIMORANGO"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeGridByPeriodo", "USP_ESTRUCTURATREEGRIDBYPERIODO", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeGridPatrocinador(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTRUCTURATREEGRIDPATROCINIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.EmployeeID = Convert.ToInt64(dr["NumeroDoc"]);
                    objSocio.ReportsTo = Convert.ToInt64(dr["Patrocinador"]);
                    objSocio.LastName = dr["Apellidos"].ToString();
                    objSocio.Telefono = dr["TELEFONO"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    objSocio.TIPOC = dr["TIPOC"].ToString();
                    objSocio.PAQUETE = dr["PAQUETE"].ToString();
                    objSocio.Pais = dr["PAIS"].ToString();
                    objSocio.Fecha = dr["FECHAREG"].ToString();
                    objSocio.PP = dr["PP"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeGridPatrocinador", "USP_ESTRUCTURATREEGRIDPATROCINIO", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeGridSocio(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTADISTICASOCIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.EmployeeID = Convert.ToInt64(dr["NumeroDoc"]);
                    objSocio.ReportsTo = Convert.ToInt64(dr["Upline"]);
                    objSocio.LastName = dr["Apellidos"].ToString();
                    objSocio.PP = dr["PP"].ToString();
                    objSocio.VP = dr["VP"].ToString();
                    objSocio.Telefono = dr["TELEFONO"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeGridSocio", "USP_ESTADISTICASOCIO", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeGridConsultor(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTADISTICACONSULTOR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.EmployeeID = Convert.ToInt64(dr["NumeroDoc"]);
                    objSocio.ReportsTo = Convert.ToInt64(dr["factorComision"]);
                    objSocio.LastName = dr["Apellidos"].ToString();
                    objSocio.PP = dr["PP"].ToString();
                    objSocio.Telefono = dr["TELEFONO"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeGridConsultor", "USP_ESTADISTICACONSULTOR", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeGridCCI(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ESTADISTICACCI", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.EmployeeID = Convert.ToInt64(dr["NumeroDoc"]);
                    objSocio.ReportsTo = Convert.ToInt64(dr["factorComision"]);
                    objSocio.LastName = dr["Apellidos"].ToString();
                    objSocio.PP = dr["PP"].ToString();
                    objSocio.Telefono = dr["TELEFONO"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeGridCCI", "USP_ESTADISTICACCI", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeTable(string idcliente, int condicion, int idPeriodo)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CONSULTA_MAPA_RED", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDP", idPeriodo);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@CONDICION", condicion);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.DOCUMENTO = Convert.ToString(dr["NumeroDoc"]);
                    objSocio.UPLINE = Convert.ToString(dr["Upline"]);
                    objSocio.NOMBRES = dr["Apellidos"].ToString();
                    objSocio.CORAZONES = dr["CORAZONES"].ToString();
                    objSocio.PP = dr["PP"].ToString();
                    objSocio.VIP = dr["VIP"].ToString();
                    objSocio.VP = dr["VP"].ToString();
                    objSocio.VR = dr["VR"].ToString();
                    objSocio.VG = dr["VG"].ToString();
                    objSocio.VQ = dr["VQ"].ToString();
                    objSocio.Rango = dr["RANGO"].ToString();
                    objSocio.MAXIMORANGO = dr["MAXIMORANGO"].ToString();
                    objSocio.Fecha = dr["FechaRegistro"].ToString();
                    objSocio.Telefono = dr["Celular"].ToString();
                    objSocio.Pais = dr["PAIS"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    objSocio.RED = dr["RED"].ToString();
                    objSocio.IDSOCIO = dr["IDSOCIO"].ToString();
                    objSocio.IDUPLINE = dr["IDSOCIOUPLINE"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeTable", "USP_CONSULTA_MAPA_RED", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }

        public List<ElementosTreeGrid> ListaTreeTableHistorico(string idcliente, int condicion, int idPeriodo)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            List<ElementosTreeGrid> lista = new List<ElementosTreeGrid>();
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_CONSULTA_MAPA_RED_HISTORICO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 0;
                cmd.Parameters.AddWithValue("@IDP", idPeriodo);
                cmd.Parameters.AddWithValue("@IDCLIENTE", idcliente);
                cmd.Parameters.AddWithValue("@CONDICION", condicion);
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ElementosTreeGrid objSocio = new ElementosTreeGrid();
                    objSocio.DOCUMENTO = Convert.ToString(dr["NumeroDoc"]);
                    objSocio.UPLINE = Convert.ToString(dr["Upline"]);
                    objSocio.NOMBRES = dr["Apellidos"].ToString();
                    objSocio.CORAZONES = dr["CORAZONES"].ToString();
                    objSocio.PP = dr["PP"].ToString();
                    objSocio.VIP = dr["VIP"].ToString();
                    objSocio.VP = dr["VP"].ToString();
                    objSocio.VR = dr["VR"].ToString();
                    objSocio.VG = dr["VG"].ToString();
                    objSocio.VQ = dr["VQ"].ToString();
                    objSocio.Rango = dr["RANGO"].ToString();
                    objSocio.MAXIMORANGO = dr["MAXIMORANGO"].ToString();
                    objSocio.Fecha = dr["FechaRegistro"].ToString();
                    objSocio.Telefono = dr["Celular"].ToString();
                    objSocio.Pais = dr["PAIS"].ToString();
                    objSocio.IDRANGO = dr["IDRANGO"].ToString();
                    objSocio.RED = dr["RED"].ToString();
                    objSocio.IDSOCIO = dr["IDSOCIO"].ToString();
                    objSocio.IDUPLINE = dr["IDSOCIOUPLINE"].ToString();
                    lista.Add(objSocio);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ElementosTreeGridDAO", "ListaTreeTable", "USP_CONSULTA_MAPA_RED", ex.Message);
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            return lista;
        }
    }
}
