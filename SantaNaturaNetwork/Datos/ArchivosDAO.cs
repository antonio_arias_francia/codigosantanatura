﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Modelos;
namespace Datos
{
    public class ArchivosDAO
    {
        #region "PATRON SINGLETON"
        private static ArchivosDAO daoArchivos = null;
        private ArchivosDAO() { }
        public static ArchivosDAO getInstance()
        {
            if (daoArchivos == null)
            {
                daoArchivos = new ArchivosDAO();
            }
            return daoArchivos;
        }
        #endregion

        public List<ArchivosPDF> ListarPromocionesPDF()
        {
            List<ArchivosPDF> Lista = new List<ArchivosPDF>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PROMOCIONES_PDF", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF objPDF = new ArchivosPDF();
                    objPDF.ID_DATOS = Convert.ToInt32(dr["ID_DATOS"]);
                    objPDF.Nombre = dr["NOMBRE_PROMO"].ToString();
                    objPDF.Vigencia = dr["VIGENCIA"].ToString();
                    objPDF.Archivo = dr["ARCHIVO"].ToString();
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarPromocionesPDF", "USP_LISTA_PROMOCIONES_PDF", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public void RegistroPromocionPDF(string nombre, string vigencia, string archivo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRAR_PROMOCIONES_PDF", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_PROMO", nombre);
                cmd.Parameters.AddWithValue("@VIGENCIA", vigencia);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "RegistroPromocionPDF", "USP_REGISTRAR_PROMOCIONES_PDF", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarPromocionPDF(string nombre, string vigencia, string archivo, int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_PROMOCIONES_PDF", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_PROMO", nombre);
                cmd.Parameters.AddWithValue("@VIGENCIA", vigencia);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ActualizarPromocionPDF", "USP_ACTUALIZAR_PROMOCIONES_PDF", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<ArchivosPDF> ListarPremiosPDF()
        {
            List<ArchivosPDF> Lista = new List<ArchivosPDF>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PREMIOS_PDF", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF objPDF = new ArchivosPDF();
                    objPDF.ID_DATOS = Convert.ToInt32(dr["ID_DATOS"]);
                    objPDF.Nombre = dr["NOMBRE_PREMIO"].ToString();
                    objPDF.Vigencia = dr["VIGENCIA"].ToString();
                    objPDF.Archivo = dr["ARCHIVO"].ToString();
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarPremiosPDF", "USP_LISTA_PREMIOS_PDF", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public void RegistroPremioPDF(string nombre, string vigencia, string archivo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRAR_PREMIOS_PDF", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_PREMIO", nombre);
                cmd.Parameters.AddWithValue("@VIGENCIA", vigencia);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "RegistroPremioPDF", "USP_REGISTRAR_PREMIOS_PDF", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarPremioPDF(string nombre, string vigencia, string archivo, int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_PREMIOS_PDF", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_PREMIO", nombre);
                cmd.Parameters.AddWithValue("@VIGENCIA", vigencia);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ActualizarPremioPDF", "USP_ACTUALIZAR_PREMIOS_PDF", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<ArchivosPDF.ImagenesBanners> ListarImagenesBanners()
        {
            List<ArchivosPDF.ImagenesBanners> Lista = new List<ArchivosPDF.ImagenesBanners>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_BANNERS_IMAGENES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF.ImagenesBanners objPDF = new ArchivosPDF.ImagenesBanners();
                    objPDF.ID_DATOS = Convert.ToInt32(dr["ID_DATOS"]);
                    objPDF.Nombre = dr["NOMBRE_BANNER"].ToString();
                    objPDF.Archivo = dr["ARCHIVO"].ToString();
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarImagenesBanners", "USP_LISTA_BANNERS_IMAGENES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public void RegistroImagenBanner(string nombre, string archivo)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRAR_BANNERS_IMAGENES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_BANNER", nombre);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "RegistroImagenBanner", "USP_REGISTRAR_BANNERS_IMAGENES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarImagenBanner(string nombre, string archivo, int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_BANNERS_IMAGENES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_BANNER", nombre);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ActualizarImagenBanner", "USP_ACTUALIZAR_BANNERS_IMAGENES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void EliminarImagenBanner(int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_BANNERS_IMAGENES", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "EliminarImagenBanner", "USP_ELIMINAR_BANNERS_IMAGENES", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<ArchivosPDF.DocumentosInformacion> ListarDocumentosInformacion()
        {
            List<ArchivosPDF.DocumentosInformacion> Lista = new List<ArchivosPDF.DocumentosInformacion>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DOCUMENTOS_INFORMACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF.DocumentosInformacion objPDF = new ArchivosPDF.DocumentosInformacion();
                    objPDF.ID_DATOS = Convert.ToInt32(dr["ID_DATOS"]); 
                    objPDF.Nombre = dr["NOMBRE_ARCHIVO"].ToString();
                    objPDF.Archivo = dr["ARCHIVO"].ToString();
                    objPDF.TipoArchivo = dr["TIPO_ARCHIVO"].ToString();
                    objPDF.CodigoClass = dr["CODIGO_CLASS"].ToString();
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarDocumentosInformacion", "USP_LISTA_DOCUMENTOS_INFORMACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public void RegistroDocumentosInformacion(string nombre, string archivo, string tipoArchivo, string codigoClass)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRAR_DOCUMENTOS_INFORMACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_ARCHIVO", nombre);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                cmd.Parameters.AddWithValue("@TIPO_ARCHIVO", tipoArchivo);
                cmd.Parameters.AddWithValue("@CODIGO_CLASS", codigoClass);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "RegistroDocumentosInformacion", "USP_REGISTRAR_DOCUMENTOS_INFORMACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarDocumentosInformacion(string nombre, string archivo, string tipoArchivo, string codigoClass, int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_DOCUMENTOS_INFORMACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE_ARCHIVO", nombre);
                cmd.Parameters.AddWithValue("@ARCHIVO", archivo);
                cmd.Parameters.AddWithValue("@TIPO_ARCHIVO", tipoArchivo);
                cmd.Parameters.AddWithValue("@CODIGO_CLASS", codigoClass);
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ActualizarDocumentosInformacion", "USP_ACTUALIZAR_DOCUMENTOS_INFORMACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void EliminarDocumentosInformacion(int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_DOCUMENTOS_INFORMACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "EliminarDocumentosInformacion", "USP_ELIMINAR_DOCUMENTOS_INFORMACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public List<ArchivosPDF.DocumentosMarketing> ListarDocumentosMarketing()
        {
            List<ArchivosPDF.DocumentosMarketing> Lista = new List<ArchivosPDF.DocumentosMarketing>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DOCUMENTOS_MARKETING", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF.DocumentosMarketing objPDF = new ArchivosPDF.DocumentosMarketing();
                    objPDF.ID_DATOS = Convert.ToInt32(dr["ID_DATOS"]);
                    objPDF.Nombre = dr["NOMBRE"].ToString();
                    objPDF.Archivo = dr["ARCHIVO"].ToString();
                    objPDF.TipoArchivo = dr["TIPO_ARCHIVO"].ToString();
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarDocumentosMarketing", "USP_LISTA_DOCUMENTOS_MARKETING", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<ArchivosPDF.DocumentosMarketing> ListarDetallesDocumentosMarketing(int idDatos)
        {
            List<ArchivosPDF.DocumentosMarketing> Lista = new List<ArchivosPDF.DocumentosMarketing>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DETALLES_MARKETING", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", idDatos);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF.DocumentosMarketing objPDF = new ArchivosPDF.DocumentosMarketing();
                    objPDF.CodigoClass = dr["CODIGO_CLASS"].ToString();
                    objPDF.Paq_Basico = Convert.ToBoolean(dr["PAQ_BASICO"]);
                    objPDF.Paq_Profesional = Convert.ToBoolean(dr["PAQ_PROFESIONAL"]);
                    objPDF.Paq_Empresarial = Convert.ToBoolean(dr["PAQ_EMPRESARIAL"]);
                    objPDF.Paq_Millonario = Convert.ToBoolean(dr["PAQ_MILLONARIO"]);
                    objPDF.Paq_Imperial = Convert.ToBoolean(dr["PAQ_IMPERIAL"]);
                    objPDF.Paq_Consultor = Convert.ToBoolean(dr["CONSULTOR"]);
                    objPDF.Paq_C_Inteligente = Convert.ToBoolean(dr["C_INTELIGENTE"]);
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarDetallesDocumentosMarketing", "USP_LISTA_DETALLES_MARKETING", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<ArchivosPDF.DocumentosMarketing> ListarDocumentosMarketingSocios(string paquete)
        {
            List<ArchivosPDF.DocumentosMarketing> Lista = new List<ArchivosPDF.DocumentosMarketing>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_DOC_MARKETING_X_PAQUETE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PAQUETE", paquete);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ArchivosPDF.DocumentosMarketing objPDF = new ArchivosPDF.DocumentosMarketing();
                    objPDF.ID_DATOS = Convert.ToInt32(dr["ID_DATOS"]);
                    objPDF.Nombre = dr["NOMBRE"].ToString();
                    objPDF.Archivo = dr["ARCHIVO"].ToString();
                    objPDF.TipoArchivo = dr["TIPO_ARCHIVO"].ToString();
                    objPDF.CodigoClass = dr["CODIGO_CLASS"].ToString();
                    Lista.Add(objPDF);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ListarDocumentosMarketingSocios", "USP_LISTA_DOC_MARKETING_X_PAQUETE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public void RegistroDocumentosMarketing(ArchivosPDF.DocumentosMarketing objDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_REGISTRO_DOCUMENTOS_MARKETING", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@NOMBRE", objDatos.Nombre);
                cmd.Parameters.AddWithValue("@ARCHIVO", objDatos.Archivo);
                cmd.Parameters.AddWithValue("@TIPO_ARCHIVO", objDatos.TipoArchivo);
                cmd.Parameters.AddWithValue("@CODIGO_CLASS", objDatos.CodigoClass);
                cmd.Parameters.AddWithValue("@PAQ_BASICO", objDatos.Paq_Basico);
                cmd.Parameters.AddWithValue("@PAQ_PROFESIONAL", objDatos.Paq_Profesional);
                cmd.Parameters.AddWithValue("@PAQ_EMPRESARIAL", objDatos.Paq_Empresarial);
                cmd.Parameters.AddWithValue("@PAQ_MILLONARIO", objDatos.Paq_Millonario);
                cmd.Parameters.AddWithValue("@PAQ_IMPERIAL", objDatos.Paq_Imperial);
                cmd.Parameters.AddWithValue("@CONSULTOR", objDatos.Paq_Consultor);
                cmd.Parameters.AddWithValue("@C_INTELIGENTE", objDatos.Paq_C_Inteligente);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "RegistroDocumentosMarketing", "USP_REGISTRO_DOCUMENTOS_MARKETING", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void ActualizarDocumentosMarketing(ArchivosPDF.DocumentosMarketing objDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZAR_DOCUMENTOS_MARKETING", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", objDatos.ID_DATOS);
                cmd.Parameters.AddWithValue("@NOMBRE", objDatos.Nombre);
                cmd.Parameters.AddWithValue("@ARCHIVO", objDatos.Archivo);
                cmd.Parameters.AddWithValue("@TIPO_ARCHIVO", objDatos.TipoArchivo);
                cmd.Parameters.AddWithValue("@CODIGO_CLASS", objDatos.CodigoClass);
                cmd.Parameters.AddWithValue("@PAQ_BASICO", objDatos.Paq_Basico);
                cmd.Parameters.AddWithValue("@PAQ_PROFESIONAL", objDatos.Paq_Profesional);
                cmd.Parameters.AddWithValue("@PAQ_EMPRESARIAL", objDatos.Paq_Empresarial);
                cmd.Parameters.AddWithValue("@PAQ_MILLONARIO", objDatos.Paq_Millonario);
                cmd.Parameters.AddWithValue("@PAQ_IMPERIAL", objDatos.Paq_Imperial);
                cmd.Parameters.AddWithValue("@CONSULTOR", objDatos.Paq_Consultor);
                cmd.Parameters.AddWithValue("@C_INTELIGENTE", objDatos.Paq_C_Inteligente);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "ActualizarDocumentosMarketing", "USP_ACTUALIZAR_DOCUMENTOS_MARKETING", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

        public void EliminarDocumentosMarketing(int IdDatos)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINAR_DOCUMENTO_MARKETING", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID_DATOS", IdDatos);
                con.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ArchivosDAO", "EliminarDocumentosMarketing", "USP_ELIMINAR_DOCUMENTO_MARKETING", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
        }

    }
}
