﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class ProductoDatos
    {
        #region "PATRON SINGLETON"
        private static ProductoDatos _productoDatos = null;
        private ProductoDatos() { }
        public static ProductoDatos getInstance()
        {
            if (_productoDatos == null)
            {
                _productoDatos = new ProductoDatos();
            }
            return _productoDatos;
        }
        #endregion

        SqlConnection conexion = null;
        SqlDataReader dr = null;
        SqlCommand cmd = null;
        string errores = "";

        public List<Producto> ListarProductosByPais(string pais, string TipoCl)
        {

            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarProductosByPais", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombrePais", pais);
                cmd.Parameters.AddWithValue("@TIPOC", TipoCl);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosByPais", "SP_ListarProductosByPais", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }
        public List<Producto> ListarProductosByPaisv3(string pais, string Buscar, string tipoCL)
        {

            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarProductosByPaisv3", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombrePais", pais);
                cmd.Parameters.AddWithValue("@Buscar", Buscar);
                cmd.Parameters.AddWithValue("@TIPOC", tipoCL);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosByPaisv3", "SP_ListarProductosByPaisv3", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }
        public List<Producto> ListarProductosMasVendidos(string pais)
        {

            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarProductosMasVendidosxPais", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombrePais", pais);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosMasVendidos", "SP_ListarProductosMasVendidosxPais", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<Producto> ListarProductosMenosVendidos(string pais)
        {

            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarProductosMenosVendidosxPais", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombrePais", pais);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosMenosVendidos", "SP_ListarProductosMenosVendidosxPais", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<Producto> ListarProductosByCodigo(string codigo)
        {
            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarProductosByCodigo", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@codigo", codigo);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["IdProductoxPais"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.Linea = Convert.ToString(dr["idLinea"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);
                    producto.Descripcion = Convert.ToString(dr["Descripcion"]);
                    producto.Puntos = Convert.ToDouble(dr["Puntos"]);
                    producto.IdProdPeruShop = Convert.ToString(dr["IdProductoPeruShop"]);
                    producto.idPaquete = Convert.ToString(dr["IdPaquete"]);
                    producto.PuntosPromocion = Convert.ToDouble(dr["PuntosPromocion"]);
                    producto.IncrementoPVP = Convert.ToDouble(dr["IncrementoPVP"]);
                    producto.Corazones = Convert.ToDouble(dr["corazones"]);
                    producto.beneficios = Convert.ToString(dr["BENEFICIOS"]);
                    producto.ingredientes = Convert.ToString(dr["INGREDIENTES"]);
                    producto.consumo = Convert.ToString(dr["CONSUMO"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosByCodigo", "SP_ListarProductosByCodigo", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<Producto> ListarProductosByNombre(string nombre)
        {

            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarPorNombre", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@nombre", nombre);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosByNombre", "SP_ListarPorNombre", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<Producto> ListarProductosByCodigoProdu(string codigo)
        {

            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarPorCODIGO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CODIGO", codigo);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosByCodigoProdu", "SP_ListarPorCODIGO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public List<string> FiltrarNombreProductos(string palabra)
        {
            List<string> listaNombreProductos = new List<string>();

            using (SqlConnection con = Conexion.getInstance().Conectar())
            {
                SqlCommand cmd = new SqlCommand("SP_AutoCompletarParaNombreProducto", con);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter parametro = new SqlParameter()
                {
                    ParameterName = "@palabra",
                    Value = palabra
                };
                cmd.Parameters.Add(parametro);
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    listaNombreProductos.Add(rdr["Nombre"].ToString());
                }
                return listaNombreProductos;
            }
        }

        public List<Producto> ListarProductosByLinea(string linea)
        {
            List<Producto> lista = new List<Producto>();
            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("SP_ListarProductosByLinea", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@linea", linea);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Producto producto = new Producto();
                    producto.Codigo = Convert.ToString(dr["idProducto"]);
                    producto.Foto = Convert.ToString(dr["Foto"]);
                    producto.NombreProducto = Convert.ToString(dr["Nombre"]);
                    producto.PrecioUnitario = Convert.ToDouble(dr["PrecioUnitario"]);

                    lista.Add(producto);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductosByLinea", "SP_ListarProductosByLinea", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return lista;
        }

        public bool RegistroProducto(ProductoV2 objProducto, List<ProductoPais> PPais)
        {
            SqlConnection con = null;
            SqlCommand cmdProducto = null;
            SqlCommand cmdDetalle = null;
            bool response = false;

            string cproducto = "";
            cproducto = GenerarProducto();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmdProducto = new SqlCommand("USP_REGISTROPRODUCTO", con);
                cmdProducto.CommandType = CommandType.StoredProcedure;
                cmdProducto.Parameters.AddWithValue("@UNIDADMEDIDA", objProducto.UnidadMedida);
                cmdProducto.Parameters.AddWithValue("@CONTENIDO", objProducto.Contenido);
                cmdProducto.Parameters.AddWithValue("@IDPRESENTACION", objProducto.IdPresentacion);
                cmdProducto.Parameters.AddWithValue("@UNIDADESPP", objProducto.UnidadPresentacion);
                cmdProducto.Parameters.AddWithValue("@DESCRIPCION", objProducto.Descripcion);
                cmdProducto.Parameters.AddWithValue("@IDLINEA", objProducto.Linea);
                cmdProducto.Parameters.AddWithValue("@PRECIOUNIATRIO", objProducto.PrecioUnitario);
                cmdProducto.Parameters.AddWithValue("@PUNTOS", objProducto.Puntos);
                cmdProducto.Parameters.AddWithValue("@IDPAQUETE", objProducto.Paquete);
                cmdProducto.Parameters.AddWithValue("@PRECIOCDR", objProducto.precioCDR);
                cmdProducto.Parameters.AddWithValue("@PROMOCION", objProducto.Promocion);
                cmdProducto.Parameters.AddWithValue("@PUNTOSPROMO", objProducto.PuntosPromocion);
                cmdProducto.Parameters.AddWithValue("@LINEACDR", objProducto.LineaCDR);
                cmdProducto.Parameters.AddWithValue("@CORAZONES", objProducto.Corazones);
                con.Open();
                int filas = cmdProducto.ExecuteNonQuery();

                foreach (var item in PPais)
                {
                    cmdDetalle = new SqlCommand("USP_REGISTROPRODUCTOPP", con);
                    cmdDetalle.CommandType = CommandType.StoredProcedure;
                    cmdDetalle.Parameters.AddWithValue("@IDPRODUCTO", cproducto);
                    cmdDetalle.Parameters.AddWithValue("@CODIGOPAIS", item.Pais);
                    cmdDetalle.Parameters.AddWithValue("@INCREMENTOPVP", item.IncrementoPVP);
                    cmdDetalle.Parameters.AddWithValue("@FOTO", item.Foto);
                    cmdDetalle.Parameters.AddWithValue("@NOMBRE", item.Nombre.ToUpper());
                    cmdDetalle.Parameters.AddWithValue("@ESTADO", item.Estado);
                    cmdDetalle.Parameters.AddWithValue("@IDPERUSHOP", item.IdProductoPeruShop);
                    cmdDetalle.Parameters.AddWithValue("@BENEFICIOS", item.Beneficios);
                    cmdDetalle.Parameters.AddWithValue("@INGREDIENTES", item.Ingredientes);
                    cmdDetalle.Parameters.AddWithValue("@CONSUMO", item.Consumo);
                    cmdDetalle.ExecuteNonQuery();
                }

                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "RegistroProducto", "USP_REGISTROPRODUCTO,USP_REGISTROPRODUCTOPP", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        public bool RegistroProductoPais(List<ProductoPais> PPais)
        {
            SqlConnection con = null;
            SqlCommand cmdDetalle = null;
            bool response = false;

            try
            {
                con = Conexion.getInstance().Conectar();
                con.Open();
                int filas = 0;

                foreach (var item in PPais)
                {
                    cmdDetalle = new SqlCommand("USP_REGISTROPRODUCTOPP", con);
                    cmdDetalle.CommandType = CommandType.StoredProcedure;
                    cmdDetalle.Parameters.AddWithValue("@IDPRODUCTO", item.IdProducto);
                    cmdDetalle.Parameters.AddWithValue("@CODIGOPAIS", item.Pais);
                    cmdDetalle.Parameters.AddWithValue("@INCREMENTOPVP", item.IncrementoPVP);
                    cmdDetalle.Parameters.AddWithValue("@FOTO", item.Foto);
                    cmdDetalle.Parameters.AddWithValue("@NOMBRE", item.Nombre.ToUpper());
                    cmdDetalle.Parameters.AddWithValue("@ESTADO", item.Estado);
                    cmdDetalle.Parameters.AddWithValue("@IDPERUSHOP", item.IdProductoPeruShop);
                    cmdDetalle.Parameters.AddWithValue("@BENEFICIOS", item.Beneficios);
                    cmdDetalle.Parameters.AddWithValue("@INGREDIENTES", item.Ingredientes);
                    cmdDetalle.Parameters.AddWithValue("@CONSUMO", item.Consumo);
                    filas = cmdDetalle.ExecuteNonQuery();
                }

                if (filas > 0) response = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "RegistroProductoPais", "USP_REGISTROPRODUCTOPP", ex.Message);
                response = false;
                throw ex;
            }
            finally
            {
                con.Close();
            }

            return response;
        }

        public string GenerarProducto()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;

            con = Conexion.getInstance().Conectar();
            con.Open();

            cmd = new SqlCommand("SP_GenerarCodigoProducto", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@CodGenerado", SqlDbType.VarChar, 8).Direction = ParameterDirection.Output;

            string cproducto = "";

            try
            {
                cmd.ExecuteNonQuery();
                cproducto = Convert.ToString(cmd.Parameters["@CodGenerado"].Value);
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "GenerarProducto", "SP_GenerarCodigoProducto", ex.Message);
                throw ex;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Dispose();
                cmd.Dispose();
            }

            return cproducto;
        }

        public bool EliminarProducto(string idproducto, string productoPais)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            bool ok = false;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ELIMINARPRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", idproducto);
                cmd.Parameters.AddWithValue("@IDPRODUCTOPAIS", productoPais);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "EliminarProducto", "USP_ELIMINARPRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarProducto(ProductoV2 objProducto)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ACTUALIZARPRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", objProducto.IdProducto);
                cmd.Parameters.AddWithValue("@UNIDADMEDIDA", objProducto.UnidadMedida);
                cmd.Parameters.AddWithValue("@CONTENIDO", objProducto.Contenido);
                cmd.Parameters.AddWithValue("@IDPRESENTACION", objProducto.IdPresentacion);
                cmd.Parameters.AddWithValue("@UNIDADESPP", objProducto.UnidadPresentacion);
                cmd.Parameters.AddWithValue("@DESCRIPCION", objProducto.Descripcion);
                cmd.Parameters.AddWithValue("@IDLINEA", objProducto.Linea);
                cmd.Parameters.AddWithValue("@PRECIOUNIATRIO", objProducto.PrecioUnitario);
                cmd.Parameters.AddWithValue("@PUNTOS", objProducto.Puntos);
                cmd.Parameters.AddWithValue("@IDPAQUETE", objProducto.Paquete);
                cmd.Parameters.AddWithValue("@PRECIOCDR", objProducto.precioCDR);
                cmd.Parameters.AddWithValue("@PROMOCION", objProducto.Promocion);
                cmd.Parameters.AddWithValue("@PUNTOSPROMO", objProducto.PuntosPromocion);
                cmd.Parameters.AddWithValue("@LINEACDR", objProducto.LineaCDR);
                cmd.Parameters.AddWithValue("@CORAZONES", objProducto.Corazones);
                con.Open();
                cmd.ExecuteNonQuery();
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ActualizarProducto", "USP_ACTUALIZARPRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public bool ActualizarProductoXPais(List<ProductoPais> PPais)
        {
            bool ok = false;
            SqlConnection con = null;
            SqlCommand cmd = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                con.Open();
                foreach (var item in PPais)
                {
                    cmd = new SqlCommand("USP_ACTUALIZAR_PRODUCTO_PAIS", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IDPRODUCTOPP", item.IdProductoxPais);
                    cmd.Parameters.AddWithValue("@CODIGOPAIS", item.Pais);
                    cmd.Parameters.AddWithValue("@INCREMENTOPVP", item.IncrementoPVP);
                    cmd.Parameters.AddWithValue("@FOTO", item.Foto);
                    cmd.Parameters.AddWithValue("@NOMBRE", item.Nombre);
                    cmd.Parameters.AddWithValue("@ESTADO", item.Estado);
                    cmd.Parameters.AddWithValue("@IDPERUSHOP", item.IdProductoPeruShop);
                    cmd.Parameters.AddWithValue("@BENEFICIOS", item.Beneficios);
                    cmd.Parameters.AddWithValue("@INGREDIENTES", item.Ingredientes);
                    cmd.Parameters.AddWithValue("@CONSUMO", item.Consumo);
                    cmd.ExecuteNonQuery();
                }
                
                ok = true;
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ActualizarProductoXPais", "USP_ACTUALIZAR_PRODUCTO_PAIS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return ok;
        }

        public List<CombosProducto> ListadoComboLinea()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<CombosProducto> listaCombo = new List<CombosProducto>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOLINEA", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CombosProducto combo = new CombosProducto();
                    combo.idLinea = dr["idLinea"].ToString();
                    combo.nombreLinea = dr["Nombre"].ToString();
                    listaCombo.Add(combo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListadoComboLinea", "USP_LISTADOLINEA", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCombo;
        }

        public List<PaqueteNatura> ListadoComboPaquete()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<PaqueteNatura> listaCombo = new List<PaqueteNatura>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPAQUETECOMBO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PaqueteNatura combo = new PaqueteNatura();
                    combo.idPaquete = dr["IdPaquete"].ToString();
                    combo.nombre = dr["Nombre"].ToString();
                    listaCombo.Add(combo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListadoComboPaquete", "USP_LISTADOPAQUETECOMBO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCombo;
        }

        public List<CombosProducto> ListadoComboPresentacion()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<CombosProducto> listaCombo = new List<CombosProducto>();
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPRESENTACION", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    CombosProducto combo = new CombosProducto();
                    combo.idPresentacion = dr["idPresentacion"].ToString();
                    combo.nombrePresentacion = dr["Nombre"].ToString();
                    listaCombo.Add(combo);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListadoComboPresentacion", "USP_LISTADOPRESENTACION", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaCombo;
        }

        public List<ProductoV2> ListarProducto()
        {
            List<ProductoV2> Lista = new List<ProductoV2>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTADOPRODUCTOS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoV2 objProducto = new ProductoV2();
                    objProducto.IdProducto = dr["IDPRODUCTO"].ToString();
                    objProducto.ProductoPais = dr["PRODUCTOPAIS"].ToString();
                    objProducto.Linea = dr["IDLINEA"].ToString();
                    objProducto.NombreLinea = dr["NOMBRELINEA"].ToString();
                    objProducto.NombreProducto = dr["NOMBREPRODUCTO"].ToString();
                    objProducto.Foto = dr["FOTO"].ToString();
                    objProducto.UnidadMedida = dr["MEDIDA"].ToString();
                    objProducto.Contenido = dr["CONTENIDO"].ToString();
                    objProducto.UnidadPresentacion = Convert.ToInt32(dr["UNIDADPP"]);
                    objProducto.Descripcion = dr["DESCRIPCION"].ToString();
                    objProducto.IdPresentacion = dr["PRESENTACION"].ToString();
                    objProducto.NombrePresen = dr["NOMPRESENTACION"].ToString();
                    objProducto.IncrementoPVP = Convert.ToDouble(dr["AUMENTO"]);
                    objProducto.Puntos = Convert.ToDecimal(dr["PUNTOS"]);
                    objProducto.LineaCDR = dr["LINEACDR"].ToString();
                    objProducto.PuntosPromocion = Convert.ToDecimal(dr["PUNTOSPROMO"]);
                    objProducto.PrecioUnitario = Convert.ToDecimal(dr["PRECIO"]);
                    objProducto.Estado = Convert.ToBoolean(dr["ESTADO"]);
                    objProducto.Promocion = Convert.ToBoolean(dr["PROMOCION"]);
                    objProducto.IdProductoPeruShop = dr["IDPERUSHOP"].ToString();
                    objProducto.Paquete = dr["PAQUETE"].ToString();
                    objProducto.Corazones = Convert.ToDecimal(dr["CORAZONES"]);
                    objProducto.precioCDR = (dr["PRECIOCDR"].ToString() == "") ? Convert.ToDecimal(0.00) : Convert.ToDecimal(dr["PRECIOCDR"]);
                    objProducto.Beneficios = dr["BENEFICIOS"].ToString();
                    objProducto.Ingredientes = dr["INGREDIENTES"].ToString();
                    objProducto.Consumo = dr["CONSUMO"].ToString();
                    Lista.Add(objProducto);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProducto", "USP_LISTADOPRODUCTOS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<ProductoV2> ListaIdopxNombre()
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            List<ProductoV2> listaProducto = new List<ProductoV2>();

            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAIDOPXNOMBRE", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoV2 producto = new ProductoV2();
                    producto.NombreProducto = dr["Nombre"].ToString();
                    producto.IdProductoPeruShop = dr["IdProductoPeruShop"].ToString();
                    producto.IdProducto = dr["IdProducto"].ToString();
                    producto.ProductoPais = dr["IdProductoxPais"].ToString();
                    producto.PrecioUnitario = Convert.ToDecimal(dr["Precio"]);
                    listaProducto.Add(producto);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListaIdopxNombre", "USP_LISTAIDOPXNOMBRE", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return listaProducto;
        }

        public List<ProductoPais> ListarProductoXIdProducto(string idProducto)
        {
            List<ProductoPais> Lista = new List<ProductoPais>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTAR_PRODUCTOPAIS_IDPRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", idProducto);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoPais objProducto = new ProductoPais();
                    objProducto.Pais = dr["CodigoPais"].ToString();
                    objProducto.IncrementoPVP = Convert.ToDouble(dr["IncrementoPVP"].ToString());
                    objProducto.Foto = dr["Foto"].ToString();
                    objProducto.Nombre = dr["Nombre"].ToString();
                    objProducto.Estado = Convert.ToBoolean(dr["Estado"].ToString());
                    objProducto.IdProductoPeruShop = dr["IdProductoPeruShop"].ToString();
                    objProducto.IdProductoxPais = dr["IdProductoxPais"].ToString();

                    Lista.Add(objProducto);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListarProductoXIdProducto", "USP_LISTAR_PRODUCTOPAIS_IDPRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public string ObtenerLineaIdProducto(string IdProducto)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string linea = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_LINEA_IDPRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", IdProducto);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    linea = Convert.ToString(dr["idLinea"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ObtenerLineaIdProducto", "USP_OBTENER_LINEA_IDPRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return linea;
        }

        public string ObtenerIdProductoXIdProductoPais(string IdProductoPais)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string IDPP = "";
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_IDPRODUCTO_XIDPRODUCTOPAIS", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTOPAIS", IdProductoPais);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    IDPP = Convert.ToString(dr["IdProducto"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ObtenerIdProductoXIdProductoPais", "USP_OBTENER_IDPRODUCTO_XIDPRODUCTOPAIS", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return IDPP;
        }

        public List<ProductoPais.Linea_PrecioCDR> ObtenerLineaPrecioCDR_By_IdProductoPais(string IdProductoPais)
        {
            List<ProductoPais.Linea_PrecioCDR> Lista = new List<ProductoPais.Linea_PrecioCDR>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_LINEA_PRECIOCDR_BY_PRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdProductoxPais", IdProductoPais);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoPais.Linea_PrecioCDR objProducto = new ProductoPais.Linea_PrecioCDR();
                    objProducto.idLinea = dr["LineaCDR"].ToString();
                    objProducto.PrecioCDR = Convert.ToDouble(dr["precioCDR"].ToString());
                    Lista.Add(objProducto);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ObtenerLineaPrecioCDR_By_IdProductoPais", "USP_OBTENER_LINEA_PRECIOCDR_BY_PRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public double ObtenerPorcentajeLineaCDR(string DniCDR, string Linea)
        {
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            double porcentaje = 0;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_PLINEA_BY_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNICDR", DniCDR);
                cmd.Parameters.AddWithValue("@IDLINEA", Linea);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    porcentaje = Convert.ToDouble(dr["PORCENTAJE_LINEA"]);
                }

            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ObtenerPorcentajeLineaCDR", "USP_OBTENER_PLINEA_BY_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return porcentaje;
        }

        public List<ProductoV2> ListaProductoCDR()
        {
            List<ProductoV2> Lista = new List<ProductoV2>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_PRODUCTOS_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoV2 objProducto = new ProductoV2();
                    objProducto.NombreProducto = dr["NOMBRE"].ToString();
                    objProducto.IdProducto = dr["IDPRODUCTO"].ToString();
                    Lista.Add(objProducto);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListaProductoCDR", "USP_LISTA_PRODUCTOS_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<ProductoV2> ListaDatosProductosCDR(string IDPRODUCTO)
        {
            List<ProductoV2> Lista = new List<ProductoV2>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_DATOS_PRODUCTO_SELECCIONADO_CDR", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDPRODUCTO", IDPRODUCTO);
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoV2 objProducto = new ProductoV2();
                    objProducto.IdProducto = dr["IdProducto"].ToString();
                    objProducto.ProductoPais = dr["IdProductoxPais"].ToString();
                    objProducto.NombreProducto = dr["Nombre"].ToString();
                    objProducto.Foto = dr["Foto"].ToString();
                    objProducto.precioCDR = Convert.ToDecimal(dr["precioCDR"]);
                    objProducto.IdProductoPeruShop = dr["IdProductoPeruShop"].ToString();
                    objProducto.UnidadPresentacion = Convert.ToInt32(dr["UnidadesPorPresentacion"]);
                    Lista.Add(objProducto);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListaDatosProductosCDR", "USP_DATOS_PRODUCTO_SELECCIONADO_CDR", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

        public List<ProductoV2> ListaUP_Producto()
        {
            List<ProductoV2> Lista = new List<ProductoV2>();
            SqlConnection con = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                con = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_UP_PRODUCTO", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductoV2 objProducto = new ProductoV2();
                    objProducto.IdProducto = dr["IdProducto"].ToString();
                    objProducto.UnidadPresentacion = Convert.ToInt32(dr["UnidadesPorPresentacion"]);
                    Lista.Add(objProducto);
                }
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("ProductoDatos", "ListaUP_Producto", "USP_LISTA_UP_PRODUCTO", ex.Message);
                throw ex;
            }
            finally
            {
                con.Close();
            }
            return Lista;
        }

    }
}
