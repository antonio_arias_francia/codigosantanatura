﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelos;


namespace Datos
{
    public class SocioDAO
    {
        #region "PATRON SINGLETON"
        private static SocioDAO daoCliente = null;
        private SocioDAO() { }
        public static SocioDAO getInstance()
        {
            if (daoCliente == null)
            {
                daoCliente = new SocioDAO();
            }
            return daoCliente;
        }
        #endregion
        String errores;

        public List<DatosSocios> ListarUpline(string numDocumento)
        {
            string errores = "";
            List<DatosSocios> ListaUpline = new List<DatosSocios>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_ListarUpLine", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@numeroDoc", "0");
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DatosSocios cliente = new DatosSocios();
                    cliente.Documento = dr["NumeroDoc"].ToString();
                    cliente.NombreCompleto = dr["Nombre"].ToString();
                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "ListarUpline", "USP_ListarUpLine", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public List<DatosSocios> ListarRetencionGenerada(string numDocumento, int IDP)
        {
            string errores = "";
            List<DatosSocios> ListaUpline = new List<DatosSocios>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_LISTA_CANT_RETENCION", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", numDocumento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DatosSocios cliente = new DatosSocios();
                    cliente.CantSocios = Convert.ToInt32(dr["SOCIO_DIRECTO_TOTAL"]);
                    cliente.CantCI = Convert.ToInt32(dr["CI_DIRECTO_TOTAL"]);
                    cliente.CantCON = Convert.ToInt32(dr["CONSULTOR_DIRECTO_TOTAL"]);
                    cliente.CantSociosDirectos = Convert.ToInt32(dr["SOCIO_DIRECTO_ACTIVO"]);
                    cliente.CantCIDirectos = Convert.ToInt32(dr["CI_DIRECTO_ACTIVO"]);
                    cliente.CantCONDirectos = Convert.ToInt32(dr["CONSULTOR_DIRECTO_ACTIVO"]);
                    cliente.NuevoSocios = Convert.ToInt32(dr["SOCIO_DIRECTO_NUEVO"]);
                    cliente.NuevoCI = Convert.ToInt32(dr["CI_DIRECTO_NUEVO"]);
                    cliente.NuevoCON = Convert.ToInt32(dr["CONSULTOR_DIRECTO_NUEVO"]);
                    cliente.CantSociosRed = Convert.ToInt32(dr["SOCIO_RED_TOTAL"]);
                    cliente.CantCIRed = Convert.ToInt32(dr["CI_RED_TOTAL"]);
                    cliente.CantCONRed = Convert.ToInt32(dr["CONSULTOR_RED_TOTAL"]);
                    cliente.CantSociosRedActivos = Convert.ToInt32(dr["SOCIO_RED_ACTIVO"]);
                    cliente.CantCIRedActivos = Convert.ToInt32(dr["CI_RED_NUEVO"]);
                    cliente.CantCONRedActivos = Convert.ToInt32(dr["CONSULTOR_RED_ACTIVO"]);
                    cliente.CantNuevosSociosRed = Convert.ToInt32(dr["SOCIO_RED_NUEVO"]);
                    cliente.CantNuevosCIRed = Convert.ToInt32(dr["CI_RED_NUEVO"]);
                    cliente.CantNuevosCONRed = Convert.ToInt32(dr["CONSULTOR_RED_NUEVO"]);
                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "ListarRetencionGenerada", "USP_LISTA_CANT_RETENCION", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public List<DatosSocios> ListarRetencionGeneral(int IDP)
        {
            string errores = "";
            List<DatosSocios> ListaUpline = new List<DatosSocios>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_RETENCION_PERIODO_HISTORICO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IDP", IDP);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DatosSocios cliente = new DatosSocios();
                    cliente.CantSocios = Convert.ToInt32(dr["TOTAL_SOCIOS"]);
                    cliente.CantCI = Convert.ToInt32(dr["TOTAL_CI"]);
                    cliente.CantCON = Convert.ToInt32(dr["TOTAL_CON"]);
                    cliente.NuevoSocios = Convert.ToInt32(dr["NUEVO_SOCIOS"]);
                    cliente.NuevoCI = Convert.ToInt32(dr["NUEVO_CI"]);
                    cliente.NuevoCON = Convert.ToInt32(dr["NUEVO_CON"]);
                    cliente.ActivoSocios = Convert.ToInt32(dr["ACT_SOCIOS"]);
                    cliente.ActivoCI = Convert.ToInt32(dr["ACT_CI"]);
                    cliente.ActivoCON = Convert.ToInt32(dr["ACT_CON"]);

                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "ListarRetencionGeneral", "USP_RETENCION_PERIODO_HISTORICO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public List<DatosSocios> ListarCantidadDirectos(string numDocumento, int IDP)
        {
            string errores = "";
            List<DatosSocios> ListaUpline = new List<DatosSocios>();
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_DIRECTOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", numDocumento);
                cmd.Parameters.AddWithValue("@IDP", IDP);
                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    DatosSocios cliente = new DatosSocios();
                    cliente.CantSocios = Convert.ToInt32(dr["SOCIOS_DIRECTOS"].ToString());
                    cliente.CantCI = Convert.ToInt32(dr["CI_DIRECTOS"].ToString());
                    cliente.CantCON = Convert.ToInt32(dr["CON_DIRECTOS"].ToString());
                    cliente.CantSociosDirectos = Convert.ToInt32(dr["SOCIOS_ACTIVOS"].ToString());
                    cliente.CantCIDirectos = Convert.ToInt32(dr["CI_ACTIVOS"].ToString());
                    cliente.CantCONDirectos = Convert.ToInt32(dr["CON_ACTIVOS"].ToString());
                    cliente.NuevoSocios = Convert.ToInt32(dr["NUEVOS_SOCIOS"].ToString());
                    cliente.NuevoCI = Convert.ToInt32(dr["NUEVOS_CI"].ToString());
                    cliente.NuevoCON = Convert.ToInt32(dr["NUEVOS_CON"].ToString());
                    cliente.CantSociosRed = CantSociosRed(numDocumento, IDP, "01");
                    cliente.CantCIRed = CantSociosRedPatrocinador(numDocumento, IDP, "03");
                    cliente.CantCONRed = CantSociosRedPatrocinador(numDocumento, IDP, "05");
                    cliente.CantSociosRedActivos = CantSociosRedActivos(numDocumento, IDP, "01");
                    cliente.CantCIRedActivos = CantSociosRedActivosPatrocinador(numDocumento, IDP, "03");
                    cliente.CantCONRedActivos = CantSociosRedActivosPatrocinador(numDocumento, IDP, "05");
                    cliente.CantNuevosSociosRed = CantSociosRedNuevos(numDocumento, IDP, "01");
                    cliente.CantNuevosCIRed = CantSociosRedNuevosPatrocinador(numDocumento, IDP, "03");
                    cliente.CantNuevosCONRed = CantSociosRedNuevosPatrocinador(numDocumento, IDP, "05");
                    ListaUpline.Add(cliente);
                }

                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "ListarCantidadDirectos", "USP_OBTENER_CANT_SOCIOS_DIRECTOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }
            return ListaUpline;
        }

        public int CantNuevosSociosPatrocinio(string documento)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_NUEVOS_DIRECTOS_PATROCINIO", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DOCUMENTO", documento.Trim());

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = (dr["CANTIDAD"]== null)? 0 : Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantNuevosSociosPatrocinio", "USP_NUEVOS_DIRECTOS_PATROCINIO", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRed(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantSociosRed", "USP_OBTENER_CANT_SOCIOS_RED", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedPatrocinador(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantSociosRedPatrocinador", "USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedActivos(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_ACTIVOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantSociosRedActivos", "USP_OBTENER_CANT_SOCIOS_RED_ACTIVOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedActivosPatrocinador(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR_ACTIVOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantSociosRedActivosPatrocinador", "USP_OBTENER_CANT_SOCIOS_RED_PATROCINADOR_ACTIVOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedNuevos(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_NUEVOS", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantSociosRedNuevos", "USP_OBTENER_CANT_SOCIOS_RED_NUEVOS", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }

        public int CantSociosRedNuevosPatrocinador(string documento, int IDP, string TC)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            int dato = 0;

            try
            {
                conexion = Conexion.getInstance().Conectar();
                cmd = new SqlCommand("USP_OBTENER_CANT_SOCIOS_RED_NUEVOS_PATROCINADOR", conexion);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@DNI", documento.Trim());
                cmd.Parameters.AddWithValue("@IDP", IDP);
                cmd.Parameters.AddWithValue("@TC", TC);

                dr = null;
                conexion.Open();
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    dato = Convert.ToInt32(dr["CANTIDAD"]);
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                Conexion.getInstance().RegistrarError("SocioDAO", "CantSociosRedNuevosPatrocinador", "USP_OBTENER_CANT_SOCIOS_RED_NUEVOS_PATROCINADOR", ex.Message);
                errores = ex.Message;
            }
            finally
            {
                if (conexion.State == ConnectionState.Open)
                {
                    conexion.Close();
                }
                conexion.Dispose();
                cmd.Dispose();
            }

            return dato;
        }
    }
}
