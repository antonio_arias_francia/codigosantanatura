﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using NsExcel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Text;

namespace SantaNaturaNetwork
{
    public partial class ReporteCDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

           
            if (!Page.IsPostBack)
            {
                if (Session["UserSessionCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }


        public void ExportGenericListToExcel<T>(List<T> list)
        {
            string fileName = "MyFilename.xls";

            DataGrid dg = new DataGrid();
            dg.AllowPaging = false;
            dg.DataSource = list;
            dg.DataBind();

            System.Web.HttpContext.Current.Response.Clear();
            System.Web.HttpContext.Current.Response.Buffer = true;
            System.Web.HttpContext.Current.Response.ContentEncoding = Encoding.UTF8;
            System.Web.HttpContext.Current.Response.Charset = "";
            System.Web.HttpContext.Current.Response.AddHeader("Content-Disposition",
              "attachment; filename=" + fileName);

            System.Web.HttpContext.Current.Response.ContentType =
              "application/vnd.ms-excel";
            System.IO.StringWriter stringWriter = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter htmlTextWriter =
              new System.Web.UI.HtmlTextWriter(stringWriter);
            dg.RenderControl(htmlTextWriter);
            System.Web.HttpContext.Current.Response.Write(stringWriter.ToString());
            System.Web.HttpContext.Current.Response.End();
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            string fecha1Send = txtFecha.Text;
            string fecha2Send = txtFechaFin.Text;

            List<ReporteCDRModelo> listaReporte = null;
            listaReporte = ReporteNegocios.getInstance().ListaReporteCDR(fecha1Send, fecha2Send);
            ExportGenericListToExcel(listaReporte);
        }

        [WebMethod]
        public static List<ReporteCDRModelo> ListaDetalleCompraEfectivo(string fecha1Send, string fecha2Send)
        {
            List<ReporteCDRModelo> listaReporte = null;
            try
            {
                listaReporte = ReporteNegocios.getInstance().ListaReporteCDR(fecha1Send, fecha2Send);

            }
            catch (Exception ex)
            {
                listaReporte = null;
            }
            return listaReporte;
        }

        
    }
}