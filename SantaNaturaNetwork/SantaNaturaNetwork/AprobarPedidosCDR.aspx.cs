﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using Modelos;
using Negocios;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace SantaNaturaNetworkV3
{
    public partial class AprobarPedidosCDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931" && Convert.ToString(Session["NumDocCliente"]) != "17397862")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<StockCDR> ListarSolicitudesTotales()
        {
            List<StockCDR> Lista = null;
            try
            {
                Lista = StockLN.getInstance().ListarSolicitudesTotales(DateTime.Now.AddHours(-2).ToString("dd/MM/yyyy"), DateTime.Now.AddHours(-2).ToString("dd/MM/yyyy"));
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<StockCDR> ListarSolicitudesTotalesFiltro(string fecha1, string fecha2)
        {
            List<StockCDR> Lista = null;
            try
            {

                //Lista = StockLN.getInstance().ListarSolicitudesTotales(Convert.ToDateTime(fecha1).ToString("yyyy/MM/dd"), Convert.ToDateTime(fecha2).ToString("yyyy/MM/dd"));
                Lista = StockLN.getInstance().ListarSolicitudesTotales(fecha1, fecha2);
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }
        
        [WebMethod]
        public static List<StockCDR> ListarProductosxSolicitud(string idSolicitud)
        {
            int idSoli = Convert.ToInt32(idSolicitud);
            List<StockCDR> Lista = null;
            try
            {
                Lista = StockLN.getInstance().ListarProductosxSolicitud(idSoli);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static void EliminarSolicitudCDR(int idPedido, string documento, string TipoPagoS)
        {
            List<StockCDR> ListaPedido = null;
            ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(idPedido);
            string dniCDR = (TipoPagoS == "1" || TipoPagoS == "3" || documento == "00003") ? "99966698" : (TipoPagoS == "9") ? documento : "00003";
            foreach (var item in ListaPedido)
            {
                bool recuperar = StockLN.getInstance().IncrementarStock(dniCDR, item.IDProductoXPais, item.Cantidad);
            }
            StockLN.getInstance().EliminarPedidoCDR(idPedido);
        }

        [WebMethod]
        public static bool AprobarSolicitudCDR(string idSolicitud, string dni, string fecha, double monto)
        {
            bool ok = true;
            int idSoli = Convert.ToInt32(idSolicitud);
            try
            {
                int IDPeriodoCDR = 0, i2 = 0;
                bool okCDR = false;
                DateTime fechaCompra = Convert.ToDateTime(fecha);

                List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR = CdrLN.getInstance().ListaPeriodoCDR();
                while (!okCDR && i2 < ListaPeriodoCDR.Count())
                {
                    DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (fechaCompra >= primerDiafechaActual & fechaCompra <= ultimoDiafechaActual)
                    {
                        okCDR = true; IDPeriodoCDR = ListaPeriodoCDR[i2].IdPeriodo;
                    }
                    i2++;
                }

                CompraLineaCredito(idSoli, monto, IDPeriodoCDR, dni, fecha);
            }
            catch (Exception ex)
            {
                ok = false;
            }
            return ok;
        }

        public static void CompraLineaCredito(int idPedido, double montoPago, int idPeriodo, string documento, string fecha)
        {
            List<StockCDR> ListaPedido = null;
            string idop = "", idop2 = "";
            try
            {
                ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(idPedido);
                idop = ObtenerIdopCompraCDR(documento, ListaPedido, fecha);
                if (idop != "") { idop2 = ObtenerIdopCompraCDR("00003", ListaPedido, fecha); }
                bool actualizarEstado = StockLN.getInstance().ActualizarEstadoSolicitud(idPedido, idop, idop2, DateTime.Now.AddHours(-2).ToString("dd/MM/yyyy"));

                Cliente.CDR.SustraccionComisionesLC objCDR = new Cliente.CDR.SustraccionComisionesLC()
                {
                    IdPeriodo = idPeriodo,
                    IdPedido = 0,
                    DniCDR = documento,
                    Fecha = DateTime.Now.AddHours(-2),
                    Monto_General = montoPago,
                    Concepto = "LINEA CREDITO",
                    Descripcion = "Compra con Saldo Disponible"
                };
                bool sustraccionCDR = CdrLN.getInstance().RegistrarSustraccionComisionesLC(objCDR);

                foreach (var item in ListaPedido)
                {
                    bool recuperar = StockLN.getInstance().IncrementarStock(documento, item.IDProductoXPais, item.Cantidad);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string ObtenerIdopCompraCDR(string dniComprador, List<StockCDR> ListaPedido, string fecha)
        {
            List<List<string>> listProductos = new List<List<string>>();
            List<Cliente.CDR.DatosPersonalesCDR> Lista = CdrLN.getInstance().ListaDatosPersonalesCDR(dniComprador);
            List<ProductoV2> ListaUP = ProductoLN.getInstance().ListaUP_Producto();
            string NombreCompleto = Lista[0].Nombres + " " + Lista[0].ApellidoPat + " " + Lista[0].ApellidoMat;
            string direccionComprador = Lista[0].Direccion.Replace("'", "");
            string Nota = Lista[0].IdPS + " - " + fecha + " - REPOSICIÓN";
            string idop = "";
            string localdst = (dniComprador == "00003") ? "Planta" : "RedAndina";
            string localPed = (dniComprador == "00003") ? "RedAndina" : Lista[0].IdPS;
            string canal = (dniComprador == "00003") ? "DISTRIBUIDORAS" : "RED2";
            string fPago = (dniComprador == "00003") ? "CREDITO" : "";

            // preguntar que nombre, direccion y documento se enviarán cuando sea de redandina a planta

            try
            {
                foreach (var item in ListaPedido)//22 y 56
                {
                    int cantidad = 0;
                    if (localdst == "Planta")
                    {
                        int UP = (from up in ListaUP where up.IdProducto == item.IDProducto select up.UnidadPresentacion).FirstOrDefault();
                        cantidad = item.Cantidad;
                    }
                    else { cantidad = item.Cantidad; }
                    double precioCDR = (dniComprador == "00003") ? StockLN.getInstance().ObtenerPrecioXidPP_SD(item.IDProductoXPais) * 0.35 :
                                                                    StockLN.getInstance().ObtenerPrecioCdrXIDPP(item.IDProductoXPais);

                    List<string> productos = new List<string>();
                    productos.Add(item.IDPS);
                    productos.Add(Convert.ToString(cantidad));
                    productos.Add(precioCDR.ToString("N2").Replace(",", "."));
                    productos.Add("0");
                    productos.Add("0");
                    productos.Add(item.IDPS);
                    productos.Add("0");
                    listProductos.Add(productos);
                }

                string serializeProds = JsonConvert.SerializeObject(listProductos);
                string prod = serializeProds;

                string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("a", "pCatalogo");
                nvc.Add("canal", canal);
                nvc.Add("tipo", "2");
                nvc.Add("fpago", fPago);
                nvc.Add("pagado", "1");
                nvc.Add("ruc", Lista[0].Documento);
                nvc.Add("local", localdst);
                nvc.Add("localorg", localdst);
                nvc.Add("localdst", localPed);
                nvc.Add("razon", NombreCompleto);
                nvc.Add("dir_ruc", direccionComprador);
                nvc.Add("log", "CDR");
                nvc.Add("not", Nota);
                nvc.Add("delivery", "0");
                nvc.Add("dir", "");
                nvc.Add("ubi", "");
                nvc.Add("ref", "");
                nvc.Add("moneda", "PEN");
                nvc.Add("prod", prod);
                nvc.Add("comprobante", "0");

                var data = wc.UploadValues(url, "POST", nvc);
                var responseString = UnicodeEncoding.UTF8.GetString(data);
                idop = (responseString != "") ? responseString : "8989898";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return idop;
        }

    }
}