﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;

namespace SantaNaturaNetwork
{
    public partial class GestionarProductos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" 
                    && Convert.ToString(Session["NumDocCliente"]) != "01901931" && Convert.ToString(Session["NumDocCliente"]) != "17397862")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static bool RegistrarProducto(string codigoPro, string lineaPro, string unidadPro, string contenidoPro,
                                                 string presentacionPro, string codigoPresentacionPro, string precioPro,
                                                 string puntosPro, List<ProductoPais> PPais,
                                                 string idpaquetePro, string preciocdrPro, string promocionPro, string puntoPro, 
                                                 string LineaCDRPro, string corazonesPro)
        {
            puntosPro = puntosPro.Replace(".", ",");
            precioPro = precioPro.Replace(".", ",");
            preciocdrPro = preciocdrPro.Replace(".", ",");
            puntoPro = puntoPro.Replace(".", ",");
            ProductoV2 objProducto = new ProductoV2()
            {
                IdProducto = codigoPro,
                Linea = lineaPro,
                UnidadMedida = unidadPro,
                Contenido = contenidoPro,
                UnidadPresentacion = Convert.ToInt32(presentacionPro),
                Descripcion = "",
                IdPresentacion = codigoPresentacionPro,
                PrecioUnitario = Convert.ToDecimal(precioPro),
                Puntos = Convert.ToDecimal(puntosPro),
                Paquete = idpaquetePro,
                precioCDR = Convert.ToDecimal(preciocdrPro),
                Promocion = Convert.ToBoolean(promocionPro),
                PuntosPromocion = Convert.ToDecimal(puntoPro),
                LineaCDR = LineaCDRPro,
                Corazones =  Convert.ToDecimal(corazonesPro)
            };

            bool ok = ProductoLN.getInstance().RegistroProducto(objProducto, PPais);
            List<ProductoPais> ListaPP = ProductoLN.getInstance().ListarProductoXIdProducto(codigoPro);
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Crear Producto", "CodigoP: " + codigoPro + " Nombre: " + PPais[0].Nombre.Trim() +
                                                                           " Puntos: " + puntosPro + " PuntosPromo: " + puntoPro);

            if (lineaPro != "04")
            {
                List<StockCDR> ListaCDR = new List<StockCDR>();
                foreach (var item3 in ListaPP)
                {
                    ListaCDR = StockLN.getInstance().ListarCDRStock();
                    if (item3.Pais == "01") {
                        foreach (var itemCDR in ListaCDR)
                        {
                            if (itemCDR.Pais == "01")
                            {
                                RegistroStockPais(itemCDR.DNICDR, codigoPro, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                            }
                        }

                    }else if (item3.Pais == "03")
                    {
                        foreach (var itemCDR in ListaCDR)
                        {
                            if (itemCDR.Pais == "03")
                            {
                                RegistroStockPais(itemCDR.DNICDR, codigoPro, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCDR in ListaCDR)
                        {
                            if (itemCDR.Pais == "07")
                            {
                                RegistroStockPais(itemCDR.DNICDR, codigoPro, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                            }
                        }
                    }
                }
            }
            return true;
        }

        [WebMethod]
        public static bool RegistrarProductoPais(List<ProductoPais> PPais, string lineaPro, string idProdu)
        {
            try {
                bool ok = ProductoLN.getInstance().RegistroProductoPais(PPais);
                if (lineaPro != "04")
                {
                    List<StockCDR> ListaCDR = new List<StockCDR>();
                    foreach (var item3 in PPais)
                    {
                        ListaCDR = StockLN.getInstance().ListarCDRStock();
                        if (item3.Pais == "01")
                        {
                            foreach (var itemCDR in ListaCDR)
                            {
                                if (itemCDR.Pais == "01")
                                {
                                    RegistroStockPais(itemCDR.DNICDR, idProdu, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                                }
                            }

                        }
                        else if (item3.Pais == "03")
                        {
                            foreach (var itemCDR in ListaCDR)
                            {
                                if (itemCDR.Pais == "03")
                                {
                                    RegistroStockPais(itemCDR.DNICDR, idProdu, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                                }
                            }
                        }
                        else
                        {
                            foreach (var itemCDR in ListaCDR)
                            {
                                if (itemCDR.Pais == "07")
                                {
                                    RegistroStockPais(itemCDR.DNICDR, idProdu, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            
            return true;
        }

        public static void RegistroStockPais(string dniCDR, string codProducto, string IDPPS, string imagen, string idProPais)
        {
            StockCDR objStock = new StockCDR()
            {
                DNICDR = dniCDR,
                IDProducto = codProducto,
                IDPS = IDPPS,
                Imagen = imagen,
                Cantidad = Convert.ToInt32(0),
                IDProductoXPais = idProPais,
                ControlStock = true
            };
            bool okCDR = StockLN.getInstance().RegistroStock(objStock);
        }

        [WebMethod]
        public static bool ActualizarProducto(string codigoPro, string medidaPro, string contenidoPro, string codigoPresentacionPro,
                                                 string unidadesPPro, string lineaPro, string precioPro,
                                                 string puntosPro, string idpaquetePro, string preciocdrPro, string promocionPro, 
                                                 string puntoPro, List<ProductoPais> PPais, string LineaCDRPro, string corazonesPro)
        {
            puntosPro = puntosPro.Replace(".", ",");
            precioPro = precioPro.Replace(".", ",");
            preciocdrPro = preciocdrPro.Replace(".", ",");
            puntoPro = puntoPro.Replace(".", ",");
            ProductoV2 objProducto = new ProductoV2()
            {
                IdProducto = codigoPro,
                UnidadMedida = medidaPro,
                Contenido = contenidoPro,
                IdPresentacion = codigoPresentacionPro,
                UnidadPresentacion = Convert.ToInt32(unidadesPPro),
                Descripcion = "",
                Linea = lineaPro,
                PrecioUnitario = Convert.ToDecimal(precioPro),
                Puntos = Convert.ToDecimal(puntosPro),
                Paquete = idpaquetePro,
                precioCDR = Convert.ToDecimal(preciocdrPro),
                PuntosPromocion = Convert.ToDecimal(puntoPro),
                Promocion = Convert.ToBoolean(promocionPro),
                LineaCDR = LineaCDRPro,
                Corazones = Convert.ToDecimal(corazonesPro)
            };
            string linea = ProductoLN.getInstance().ObtenerLineaIdProducto(codigoPro);
            bool ok = ProductoLN.getInstance().ActualizarProducto(objProducto);
            bool okPP = ProductoLN.getInstance().ActualizarProductoXPais(PPais);
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Actualizar Producto", "CodigoP: " + codigoPro+ " Nombre: "+ PPais[0].Nombre.Trim()+
                                                                           " Puntos: "+ puntosPro+ " PuntosPromo: "+ puntoPro);

            if (linea != "04")
            {
                foreach (var item3 in PPais)
                {
                    StockCDR objStock = new StockCDR()
                    {
                        IDProducto = codigoPro,
                        IDPS = item3.IdProductoPeruShop,
                        Imagen = item3.Foto,
                        IDProductoXPais = item3.IdProductoxPais
                    };
                    bool okStock = StockLN.getInstance().ActualizarProductosenStock(objStock);
                }
            }
            else if ((linea == "04") & (lineaPro != "04"))
            {
                List<StockCDR> ListaCDR = new List<StockCDR>();
                foreach (var item3 in PPais)
                {
                    ListaCDR = StockLN.getInstance().ListarCDRStock();
                    if (item3.Pais == "01")
                    {
                        foreach (var itemCDR in ListaCDR)
                        {
                            if (itemCDR.Pais == "01")
                            {
                                RegistroStockPais(itemCDR.DNICDR, codigoPro, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                            }
                        }

                    }
                    else if (item3.Pais == "03")
                    {
                        foreach (var itemCDR in ListaCDR)
                        {
                            if (itemCDR.Pais == "03")
                            {
                                RegistroStockPais(itemCDR.DNICDR, codigoPro, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                            }
                        }
                    }
                    else
                    {
                        foreach (var itemCDR in ListaCDR)
                        {
                            if (itemCDR.Pais == "07")
                            {
                                RegistroStockPais(itemCDR.DNICDR, codigoPro, item3.IdProductoPeruShop, item3.Foto, item3.IdProductoxPais);
                            }
                        }
                    }
                }
            }

            return true;
        }

        [WebMethod]
        public static List<CombosProducto> GetLinea()
        {
            List<CombosProducto> ListaCombo = ProductoLN.getInstance().ListaComboLinea();
            var query = from item in ListaCombo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<PaqueteNatura> GetPaquetes()
        {
            List<PaqueteNatura> ListaCombo = ProductoLN.getInstance().ListaComboPaquete();
            var query = from item in ListaCombo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<CombosProducto> GetPresentacion()
        {
            List<CombosProducto> ListaCombo = ProductoLN.getInstance().ListaComboPresentacion();
            var query = from item in ListaCombo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<ProductoV2> ListaProductos()
        {
            List<ProductoV2> Lista = ProductoLN.getInstance().ListaProductos();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static string CodigoGenerado()
        {
            string producto = ProductoLN.getInstance().CodigoGenerado();
            return producto;
        }

        [WebMethod]
        public static bool EliminarFilaProducto(string idProducto, string productoPais)
        {
            if (System.Web.HttpContext.Current.Session["NumDocCliente"].ToString() == "45750970" | 
                System.Web.HttpContext.Current.Session["NumDocCliente"].ToString() == "730115832") {
                bool ok = ProductoLN.getInstance().EliminarProducto(idProducto, productoPais);
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                               "Eliminar Producto", "CodigoP: " + idProducto + " ProductoPais: " + productoPais.Trim());
                return true;
            }
            else
            {
                return false;
            }
        }

        [WebMethod]
        public static bool EliminarImagen(string imagen)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/products/" + imagen);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static List<ProductoPais> ListarProductoXIdProducto(string idProductoSend)
        {
            List<ProductoPais> Lista = null;
            try
            {
                Lista = ProductoLN.getInstance().ListarProductoXIdProducto(idProductoSend);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

    }
}