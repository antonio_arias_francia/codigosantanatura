﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
	public partial class HistorialFacturas1 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                if (Session["IdCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
            ddlCliente.Items.Insert(1, new ListItem(Convert.ToString(Session["Nombre_Completo"]), "PROPIO"));
            ddlCliente.Items.Insert(2, new ListItem("OTRA PERSONA", "TERCERO"));
        }

        [WebMethod]
        public static List<PeriodoComision> ListaPeriodoComisionFacturacion()
        {
            string idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            List<PeriodoComision> Lista = PeriodoComisionLN.getInstance().ListaPeriodoComisionFacturacion(idCliente);

            return Lista;
        }

        [WebMethod]
        public static List<Compra.Facturacion> ListaFacturacionSocios()
        {
            string idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            List<Compra.Facturacion> Lista = null;
            try
            {
                Lista = CompraNegocios.getInstance().ListaFacturacionSocios(idCliente);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PeriodoComision> ListaPeriodoComisionFacturacionModal(int idPeriodoS)
        {
            string idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            List<PeriodoComision> Lista = PeriodoComisionLN.getInstance().ListaPeriodoComisionFacturacionModal(idCliente, idPeriodoS);

            return Lista;
        }

        [WebMethod]
        public static bool RegistrarFacturacionSocios(int idPeriodoS, string tipoClienteS, string RucS, string archivoS)
        {
            string idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            string nombresC = Convert.ToString(System.Web.HttpContext.Current.Session["Nombre_Completo"]);
            if (RucS == "") { RucS = Convert.ToString(System.Web.HttpContext.Current.Session["RUCSocio"]); }
            bool ok = false;
            try
            {
                if (RucS != "") {
                    Compra.Facturacion facturacion = new Compra.Facturacion()
                    {
                        IdPeriodo = idPeriodoS,
                        IdCliente = idCliente,
                        TipoCliente = tipoClienteS,
                        RUC = RucS,
                        Archivo = archivoS,
                        DniCliente = documento,
                        Nombres = nombresC,
                        FechaRegistro = DateTime.Now
                    };
                    CompraNegocios.getInstance().RegistrarFacturacionSocios(facturacion);
                    ok = true;
                }
            }
            catch (Exception ex)
            {
                ok = false;
                throw ex;
            }
            return ok;
        }

        [WebMethod]
        public static bool ActualizarFacturacionSocios(int idDatos, int idPeriodoS, string tipoClienteS, 
                                                       string RucS, string archivoS)
        {
            string idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            if (RucS == "") { RucS = Convert.ToString(System.Web.HttpContext.Current.Session["RUCSocio"]); }
            bool ok = false;
            try
            {
                if (RucS != "")
                {
                    Compra.Facturacion facturacion = new Compra.Facturacion()
                    {
                        Id_Datos = idDatos,
                        IdPeriodo = idPeriodoS,
                        IdCliente = idCliente,
                        TipoCliente = tipoClienteS,
                        RUC = RucS,
                        Archivo = archivoS
                    };
                    CompraNegocios.getInstance().ActualizarFacturacionSocios(facturacion);
                    ok = true;
                }
            }
            catch (Exception ex)
            {
                ok = false;
                throw ex;
            }
            return ok;
        }
    }
}