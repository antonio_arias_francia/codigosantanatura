﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.IO;
using System.Data;
using System.Web.Services;

namespace SantaNaturaNetwork
{
    public partial class Novedades1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        [WebMethod]
        public static bool RegistrarNovedad(string tituloNove, string mensajeNove, string enlaceNove, string imagenNove,
                                                string estadoNove, string fechaNove)
        {
            Novedad objNovedad = new Novedad()
            {
                titulo = tituloNove,
                mensaje = mensajeNove,
                enlace = enlaceNove,
                imagen = imagenNove,
                estado = Convert.ToBoolean(estadoNove),
                fecha = fechaNove
            };

            bool ok = NovedadesLN.getInstance().RegistroNovedades(objNovedad);

            return true;
        }

        [WebMethod]
        public static bool ActualizarNovedad(string idNove, string tituloNove, string mensajeNove, string enlaceNove, string imagenNove,
                                                string estadoNove, string fechaNove)
        {
            Novedad objNovedad = new Novedad()
            {
                idNovedades = idNove,
                titulo = tituloNove,
                mensaje = mensajeNove,
                enlace = enlaceNove,
                imagen = imagenNove,
                estado = Convert.ToBoolean(estadoNove),
                fecha = fechaNove
            };
            bool ok = NovedadesLN.getInstance().ActualizarNovedades(objNovedad);
            return true;
        }

        [WebMethod]
        public static bool EliminarFilaNovedad(string idNove)
        {

            bool ok = NovedadesLN.getInstance().EliminarNovedad(idNove);

            return true;
        }

        [WebMethod]
        public static List<Novedad> ListaNovedad()
        {
            List<Novedad> Lista = NovedadesLN.getInstance().ListaNovedades();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool EliminarImagen(string imagen)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/novedad/" + imagen);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }
    }
}