﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Data.SqlClient;
using System.Data;
using System.Web.Services;
using System.IO;

namespace SantaNaturaNetwork
{
    public partial class EditarSocio : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                IniciarLlenadoCombo();
                LlenarEstablecimiento();
            }
        }

        private void IniciarLlenadoCombo()
        {
            List<TipoCliente> ListaTipo = TipoClienteLN.getInstance().ListarTipo();
            cboTipoCliente.DataSource = ListaTipo;
            cboTipoCliente.DataTextField = "tipo";
            cboTipoCliente.DataValueField = "IdTipo";
            cboTipoCliente.DataBind();
            cboTipoCliente.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }

        private void LlenarEstablecimiento()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimientoColombia();
            cboTipoEstablecimiento.DataSource = ListaTipo;
            cboTipoEstablecimiento.DataTextField = "apodo";
            cboTipoEstablecimiento.DataValueField = "numeroDoc";
            cboTipoEstablecimiento.DataBind();
            cboTipoEstablecimiento.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }

        private void GuardarArchivo(HttpPostedFile file)
        {
            string ruta = Server.MapPath("~/temp");

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

            string archivo = String.Format("{0}\\{1}", ruta, file.FileName);

            if (File.Exists(archivo))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "NombreImagen()", true);
            }
            else
            {
                file.SaveAs(archivo);
            }

        }

        private void carga()
        {
            try
            {
                if (fileUpload.HasFile)
                {
                    // Se verifica que la extensión sea de un formato válido
                    string ext = fileUpload.PostedFile.FileName;
                    ext = ext.Substring(ext.LastIndexOf(".") + 1).ToLower();
                    string[] formatos =
                      new string[] { "jpg", "jpeg", "bmp", "png", "gif" };
                    if (Array.IndexOf(formatos, ext) < 0)
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FormatoImagen()", true);
                    }
                    else
                    {
                        GuardarArchivo(fileUpload.PostedFile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void RegistrarCliente(object sender, EventArgs e)
        {
            string sexo = cboSexo.Items[cboSexo.SelectedIndex].Text;
            string tipoDoc = cboTipoDoc.Items[cboTipoDoc.SelectedIndex].Text;
            string sexoC = cboSexo.SelectedIndex.ToString();
            string tipoDocC = cboTipoDoc.Items[cboTipoDoc.SelectedIndex].Value;
            string tipoCliente = cboTipoCliente.SelectedItem.Value.ToString();
            //string pais = cboPais.Items[cboPais.SelectedValue].ToString();
            string departamento = cboDepartamento.SelectedValue.ToString();
            string provincia = cboProvincia.SelectedValue.ToString();
            string distrito = cboDistrito.SelectedValue.ToString();
            string fecha = txtFecNac.Value;
            DateTime fechaActual = DateTime.Now;
            DateTime fechaNac = DateTime.ParseExact(fecha, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            #region "CONDICIONES"
            if (txtUsuario.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaUsuario()", true);
            }
            else if (tipoCliente == "0")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaTipoCliente()", true);
            }
            else if (txtNombres.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaNombre()", true);
            }
            else if (txtApPat.Text == "" | txtApMat.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaApellido()", true);
            }
            else if (tipoDocC == "0")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaTipoDoc()", true);
            }
            else if (txtNroDoc.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaDocumento()", true);
            }
            else if (fechaNac.AddYears(18) >= fechaActual)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaFechaNac()", true);
            }
            else if (sexoC == "0")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaSexo()", true);
            }
            else if (txtCorreo.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaCorreo()", true);
            }
            else if (departamento == "0")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaDepartamento()", true);
            }
            else if (provincia == "0")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaProvincia()", true);
            }
            else if (distrito == "0")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaDistrito()", true);
            }
            else if (txtxDireccion.Text == "")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "FaltaDireccion()", true);
            }
            #endregion
            else
            {
                carga();
                string tipoEstablecimiento = cboTipoEstablecimiento.SelectedItem.Value.ToString();
                if (tipoEstablecimiento == "0")
                {
                    tipoEstablecimiento = "";
                }
                Cliente objCliente = new Cliente();
                objCliente.usuario = txtUsuario.Text;
                objCliente.clave = txtNroDoc.Text;
                objCliente.nombre = txtNombres.Text;
                objCliente.apellidoPat = txtApPat.Text;
                objCliente.apellidoMat = txtApMat.Text;
                objCliente.apodo = txtApodo.Text;
                objCliente.fechaNac = txtFecNac.Value;
                objCliente.sexo = sexo;
                objCliente.tipoDoc = tipoDoc;
                objCliente.numeroDoc = txtNroDoc.Text;
                objCliente.correo = txtCorreo.Text;
                objCliente.telefono = txtTelefono.Text;
                objCliente.celular = txtCelular.Text;
                objCliente.departamento = departamento;
                objCliente.provincia = provincia;
                objCliente.ditrito = distrito;
                objCliente.direccion = txtxDireccion.Text;
                objCliente.referencia = txtReferencia.Text;
                objCliente.nroCtaDetraccion = txtCtaDetraccion.Text;
                objCliente.ruc = txtRuc.Text;
                objCliente.nombreBanco = txtBanco.Text;
                objCliente.nroCtaDeposito = txtCtaDeposito.Text;
                objCliente.nroCtaInterbancaria = txtCtaInterbancaria.Text;
                objCliente.tipoCliente = tipoCliente;
                objCliente.patrocinador = txtPatrocinador.Text;
                objCliente.upline = txtUpLine.Text;
                objCliente.tipoEstablecimiento = tipoEstablecimiento;
                objCliente.imagen = fileUpload.PostedFile.FileName;


                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "myalert", "alertme()", true);
            }


        }

        protected void prueba(object sender, EventArgs e)
        {


        }

        [WebMethod(EnableSession = true)]
        public static List<Pais> GetPais()
        {
            List<Pais> ListaPais = PaisLN.getInstance().ListarPais();
            var query = from item in ListaPais
                        select item;

            return query.ToList();
        }

        [WebMethod(EnableSession = true)]
        public static List<Departamento> GetDepartamentosByPais(string pais)
        {
            List<Departamento> ListaDepartamento = DepartamentoLN.getInstance().GetDepartamentos();
            var query = from item in ListaDepartamento
                        where item.CodigoPais == pais
                        select item;

            return query.ToList();

        }

        [WebMethod(EnableSession = true)]
        public static List<Provincia> GetProvinciaByDepartamento(string departamento)
        {
            List<Provincia> ListaProvincia = ProvinciaLN.getInstance().GetProvincia();
            var query = from item in ListaProvincia
                        where item.CodigoDepartamento == departamento
                        select item;

            return query.ToList();

        }

        [WebMethod(EnableSession = true)]
        public static List<Distrito> GetDistritoByProvincia(string provincia)
        {
            List<Distrito> ListaDistrito = DistritoLN.getInstance().GetDistrito();
            var query = from item in ListaDistrito
                        where item.CodigoProvincia == provincia
                        select item;

            return query.ToList();

        }
    }
}