﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class RangoFechaEvaluacionPedidos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.FechaPedidoComisionCDR> ListaFechasEvaluacionPedidoCDR()
        {
            List<Cliente.CDR.FechaPedidoComisionCDR> ListaCDR = CdrLN.getInstance().ListaFechasEvaluacionPedidoCDR();
            return ListaCDR;
        }

        [WebMethod]
        public static bool ActualizarFechasEvaluacionPedidoCDR(int idPedidoS, string fechaInicioS, string fechaFinS)
        {
            bool respuesta;
            try
            {
                Cliente.CDR.FechaPedidoComisionCDR objCDR = new Cliente.CDR.FechaPedidoComisionCDR()
                {
                    ID_Pedido = idPedidoS,
                    FechaInicio = fechaInicioS,
                    FechaFin = fechaFinS
                };
                bool registro = CdrLN.getInstance().ActualizarFechasEvaluacionPedidoCDR(objCDR);
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }
    }
}