﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using Modelos;
using Negocios;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Globalization;

namespace SantaNaturaNetworkV3
{
    public partial class DatosPersonalesCDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "07")
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        [WebMethod]
        public static bool RegistroAutomaticoDatosCDR()
        {
            try
            {
                List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDR();
                foreach (var cdr in ListaCDR)
                {
                    Cliente.CDR.DatosPersonalesCDR objDatosPersonales = new Cliente.CDR.DatosPersonalesCDR()
                    {
                        DniCDR = cdr.DNICDR,
                        Nombres = "",
                        ApellidoPat = "",
                        ApellidoMat = "",
                        Documento = ""
                    };
                    bool registroDatosPersonales = CdrLN.getInstance().RegistrarDatosPersonalesCDR(objDatosPersonales);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        [WebMethod]
        public static List<Cliente.CDR.DatosPersonalesCDR> ListarDatosPersonalesCDR()
        {
            List<Cliente.CDR.DatosPersonalesCDR> Lista = null;
            try
            {
                Lista = CdrLN.getInstance().ListaDatosPersonalesCDR(Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]));

            }
            catch (Exception ex)
            {
                Lista = null;
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static bool ActualizarDatosCDR(string NombreS, string ApellidoPatS, string ApellidoMatS, 
                                              string documentoS, string docExtorno, string razonExtorno,
                                              string dirExtorno)
        {
            try
            {
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                Cliente.CDR.DatosPersonalesCDR objDatosPersonales = new Cliente.CDR.DatosPersonalesCDR()
                {
                    DniCDR = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]),
                    Nombres = NombreS,
                    ApellidoPat = ApellidoPatS,
                    ApellidoMat = ApellidoMatS,
                    Documento = documentoS,
                    DocExtorno = docExtorno,
                    RazonExtorno = razonExtorno,
                    DirExtorno = dirExtorno
                };
                bool actualizarDatosPersonales = CdrLN.getInstance().ActualizarDatosPersonalesCDR(objDatosPersonales);
                bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                               "Datos Personales", "Actualizó sus datos Personales:"+ docExtorno+" Razon: "+ razonExtorno+" Dire:"+ dirExtorno);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

    }
}