﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Net;
using System.Web.Script.Serialization;
using Modelos;
using Negocios;
using Newtonsoft.Json;

namespace SantaNaturaNetworkV3
{
    /// <summary>
    /// Descripción breve de Handler_UploadVoucher
    /// </summary>
    public class Handler_UploadVoucher : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string Usuario = context.Request.QueryString["Usuario"];
            string Result = "ERROR";
            string ruta =context.Server.MapPath("voucher_temporal/" + Convert.ToString(Usuario) + "/");


            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                string fname;
                string extension;

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    fname = ruta + file.FileName;
                    extension = System.IO.Path.GetExtension(file.FileName);
                    fname = fname.Replace(" ", "");

                    if (extension == ".png" || extension == ".PNG" || extension == ".jpg" || extension == ".JPG" || extension == ".jpeg" || extension == ".JPEG")
                    {
                        if (File.Exists(fname))
                        {
                            //context.Response.ContentType = "text/plain";
                            //context.Response.Write("El nombre de la imagen ya existe");
                            Result = "EXISTE";
                        }
                        else
                        {
                            bool registro = CompraNegocios.getInstance().RegistrarVoucherTemporal(Usuario, "voucher_temporal/" + Convert.ToString(Usuario) + "/" + file.FileName);
                            if (registro == true)
                            {
                                file.SaveAs(fname);
                                Result = "NO EXISTE";
                            }
                        }
                    }
                    else
                    {
                        Result = "FORMATO INCORRECTO";
                    }



                }

                string json = new JavaScriptSerializer().Serialize(new
                {
                    name = Result
                });
                context.Response.StatusCode = System.Convert.ToInt32(HttpStatusCode.OK);
                context.Response.ContentType = "text/json";
                context.Response.Write(json);
                context.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}