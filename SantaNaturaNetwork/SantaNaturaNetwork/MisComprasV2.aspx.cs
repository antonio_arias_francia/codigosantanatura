﻿using Modelos;
using Negocios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Services;
using System.Globalization;
using System.Web.Script.Serialization;

namespace SantaNaturaNetwork
{
    public partial class MisCompras2 : System.Web.UI.Page
    {
        public List<Compra> listaComprasRealizadas = new List<Compra>();
        public List<Compra> listaComprasRealizadasVisaNet = new List<Compra>();
        public List<Compra> listaComprasRealizadasPagoEfec = new List<Compra>();
        public List<Compra> listaComprasPendientes = new List<Compra>();
        public List<Compra> listaComprasPendientesAprobacion = new List<Compra>();
        public List<Compra> listaComprasPendientesEfectivo = new List<Compra>();
        public List<Compra> listaComprasPendientesPagoEfec = new List<Compra>();
        public List<Compra> listaComprasAnuladas = new List<Compra>();
        public List<Compra> listaComprasGeneral = new List<Compra>();
        public List<DetalleCompra> listaDetalleCompra = new List<DetalleCompra>();
        public List<PaqueteNatura> listaPaqueteNatura = new List<PaqueteNatura>();
        public string ticked = "";
        public string tickedEliminar = "";
        public string fotoVoucherCompra = "";
        string nombreEnviar = "";
        string idTipoCompra = "";
        string rucFactura = "";
        DateTime fechaSimple;
        string fechaSimple2;
        string tipoCompra = "";
        string NotaDelivery = "";
        string estableMail = "";
        Boolean estado = true;
        String merror;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IdCliente"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "")
            {
                Response.Redirect("Index.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("Principal.aspx");
            }
            string idcliente = (string)Session["IdCliente"];
            hf_IdCliente.Value = idcliente;
            ListaComprasRealizadasDelCliente(idcliente);
            ListaComprasRealizadasVisaNet(idcliente);
            ListaComprasRealizadasPagoEfecCliente(idcliente);
            ListaComprasPendientesDelCliente(idcliente);
            ListaComprasPendientesEfectivoCliente(idcliente);
            ListaComprasPendientesPagoEfecCliente(idcliente);
            ListaComprasRealizadasDeposito(idcliente);
            ListaComprasAnuladasDelCliente(idcliente);
            //comprasPen.Attributes.Add("display", "block");
            Session["mostrarCompraTerminada"] = 0;
            string idCliSend = Convert.ToString(Session["IdCliente"]);
            string documento = Convert.ToString(Session["NumDocCliente"]);
            if (Convert.ToString(Session["TipoCliente"]) != "")
            {
                //int cantCompras = CompraNegocios.getInstance().CantidadCompras(idCliSend);
                //int cantDirectos = ClienteLN.getInstance().ConsultaDirectosGeneral(documento.Trim());
                //if (cantCompras <= 0 & cantDirectos == 0) { bool eliminar = CompraNegocios.getInstance().EliminarClienteSinCompras(idCliSend); LimpiarSession(); Response.Redirect("Login.aspx"); }
            }

            //Validamos Caducidad de Clave
            string idCliente = Convert.ToString(Session["IdCliente"]);
            string result = "";

            result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


            if (result == "Clave Vencida")
            {
                Session["clave_vencida"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }

            //Validamos si ya agregó sus preguntas de Seguridad
            result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
            if (result == "No Existe")
            {
                Session["preguntas_seguridad"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }

        }

        private void LimpiarSession()
        {
            //Datos importantes para comprar
            Session["NumDocCliente"] = "";
            Session["NumDocClienteRegistrador"] = "";
            Session["TipoCliente"] = "";
            Session["NombrePatrocinador"] = "";
            Session["PatrocinadorDNI"] = "";
            Session["UplineDNI"] = "";
            Session["IdSimplex"] = "";
            Session["PromoAplicada"] = 0;
            Session["IdCliente"] = "";

            Session["Pais"] = "";
            Session["Departamento"] = "";
            Session["Provincia"] = "";
            Session["Distrito"] = "";
            Session["FechaNac"] = "";

            //
            Session["SubTotalPuntos"] = 0.00;
            Session["SubTotal"] = 0.00;
            Session["MontoAPagar"] = 0.00;
            Session["TipoPago"] = "";
            Session["Foto"] = "";
            Session["IdProducto"] = "";
            Session["Producto"] = "";
            Session["PrecioUnitario"] = 0.00;
            Session["Cantidad"] = 0;
            Session["VamosCompra"] = 0;
            Session["EliminarPromo"] = 0;
            Session["TiendaRetorna"] = 0;
            Session["ActualizaPag"] = 0;
            Session["codProducto"] = "";
            Session["codProducto2"] = "";
            Session["codProducto3"] = "";
            Session["codProducto4"] = "";
            Session["codProdBasico"] = "";
            Session["codProdProfe"] = "";
            Session["codProdEmpre"] = "";
            Session["codProdMillo"] = "";
            Session["codProdBasico2"] = "";
            Session["codProdProfe2"] = "";
            Session["codProdEmpre2"] = "";
            Session["codProdMillo2"] = "";
            Session["tipoCompPromo"] = "";
            Session["TipoAfiliacion"] = "";
            Session["cantRegalo"] = 0;
            Session["cboActivar"] = "0";
            Session["RecargaPagina"] = 0;

            Session["Ayuda"] = "NO";

            //Combo ayuda
            Session["sTipoCompraSelect"] = "";
            Session["sTipoEntregaSelect"] = "0";
            Session["sMedioPagoSelect"] = "0";
            Session["comboDepartSelect"] = "0";
            Session["comboProvinciaSelect"] = "0";
            Session["comboDistritoSelect"] = "0";
            Session["comboTiendaSelect"] = "";
            Session["Establecimiento"] = "";

            List<ProductoCarrito> carritoProductos = new List<ProductoCarrito>();

            Session["CarritoProducto"] = carritoProductos;

            //DatosClienteFormulario
            Session["Nombre"] = "";
            Session["Provincia"] = 0;

            //Login
            Session["isLogueado"] = "NO";
        }

        private void LlenarEstablecimiento()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimientoColombia();
            ComboTienda.DataSource = ListaTipo;
            ComboTienda.DataTextField = "apodo";
            ComboTienda.DataValueField = "IdPeruShop";
            ComboTienda.DataBind();
            ComboTienda.Items.Insert(0, new ListItem("Seleccione", ""));

        }
        [WebMethod()]
        public static List<Cliente> ListadoEstablecimiento() //Charles 25/03/2021 Listado de Establecimientos
        {

            return ClienteLN.getInstance().ListaEstablecimientoColombia();
        }
        [WebMethod()]
        public static List<Compra> DevolverDespachoTicket(string Ticket) //Charles 25/03/2021 Listado de Establecimientos
        {
            return CompraNegocios.getInstance().CDRxTicket(Ticket);
        }
        [WebMethod()]
        public static List<Compra> DevolverVouchersTemporal(string Usuario) //Charles 25/03/2021 Devolver Vouchers Temporales
        {
            return CompraNegocios.getInstance().DevolverVoucherTemporal(Usuario);
        }
        [WebMethod()] 
        public static bool EliminarVouchersTemporal(string Usuario) //Charles 02/04/2021 Eliminar Vouchers Temporales
        {

            HttpContext context = HttpContext.Current;
            DirectoryInfo dir3 = new DirectoryInfo(context.Server.MapPath("voucher_temporal/" + Usuario));
            bool folderExists = Directory.Exists(context.Server.MapPath("voucher_temporal/" + Usuario));
            if (folderExists == true)
            {
                foreach (FileInfo file3 in dir3.GetFiles())
                {
                    file3.Delete();
                }
                System.IO.Directory.Delete(context.Server.MapPath("voucher_temporal/" + Usuario));
            }

            return CompraNegocios.getInstance().EliminarVoucherTemporal(Usuario);
        }
        private void ListaComprasAnuladasDelCliente(string idcliente)
        {
            listaComprasAnuladas = CompraNegocios.getInstance().ListaComprasAnuladasDelCliente(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetalleCanceladas(string IdCliente) //Charles 24/03/2021 CANCELED
        {
            return CompraNegocios.getInstance().ListaComprasAnuladasDelCliente(IdCliente);
        }

        private void ListaComprasRealizadasDeposito(string idcliente)
        {
            listaComprasPendientesAprobacion = CompraNegocios.getInstance().ListaComprasPendientesAprobacionDelCliente(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetallePendingPurchasesMadeDeposit(string IdCliente) //Charles 24/03/2021 PENDING PURCHASES MADE DEPOSIT
        {
            return CompraNegocios.getInstance().ListaComprasPendientesAprobacionDelCliente(IdCliente);
        }

        private void ListaComprasPendientesEfectivoCliente(string idcliente)
        {
            listaComprasPendientesEfectivo = CompraNegocios.getInstance().ListaComprasPendientesEfectivo(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetallePendingPurchasesCash(string IdCliente) //Charles 24/03/2021 PENDING PURCHASES CASH
        {
            return CompraNegocios.getInstance().ListaComprasPendientesEfectivo(IdCliente);
        }

        private void ListaComprasPendientesPagoEfecCliente(string idcliente)
        {
            listaComprasPendientesPagoEfec = CompraNegocios.getInstance().ListaComprasPendientesPagoEfectivo(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetallePendingPurchasesCashPayment(string IdCliente) //Charles 24/03/2021 PENDING PURCHASES CASH PAYMENT
        {
            return CompraNegocios.getInstance().ListaComprasPendientesPagoEfectivo(IdCliente);
        }

        private void ListaComprasRealizadasPagoEfecCliente(string idcliente)
        {
            listaComprasRealizadasPagoEfec = CompraNegocios.getInstance().ListaComprasRealizadasPagoEfectivo(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetallePendingPurchasesMadeCashPayment(string IdCliente) //Charles 24/03/2021 PENDING PURCHASES MADE CASH PAYMENT
        {
            return CompraNegocios.getInstance().ListaComprasRealizadasPagoEfectivo(IdCliente);
        }

        private void ListaComprasPendientesDelCliente(string idcliente)
        {
            listaComprasPendientes = CompraNegocios.getInstance().ListaComprasPendientesDelClienteDeposito(idcliente);
        }

        [WebMethod()]
        public static List<Compra> DetallePendingPurchases(string IdCliente) //Charles 24/03/2021 PENDING PURCHASES
        {
            return CompraNegocios.getInstance().ListaComprasPendientesDelClienteDeposito(IdCliente);
        }
        private void ListaComprasRealizadasDelCliente(string idcliente)
        {
            listaComprasRealizadas = CompraNegocios.getInstance().ListaComprasRealizadasDelCliente(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetallePendingPurchasesMadeCash(string IdCliente) //Charles 24/03/2021 PENDING PURCHASES MADE CASH
        {
            return CompraNegocios.getInstance().ListaComprasRealizadasDelCliente(IdCliente);
        }
        private void ListaComprasRealizadasVisaNet(string idcliente)
        {
            listaComprasRealizadasVisaNet = CompraNegocios.getInstance().ListaComprasRealizadasVisaNet(idcliente);
        }
        [WebMethod()]
        public static List<Compra> DetallePendingPurchasesMadeVisanet(string IdCliente) //Charles 24/03/2021 PURCHASES MADE VISANET
        {
            return CompraNegocios.getInstance().ListaComprasRealizadasVisaNet(IdCliente);
        }
        protected void BtnDetalleComprasPendientes_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasPendientesByTicket(ticked);
        }

        private void VerDetalleComprasPendientesByTicket(string ticked)
        {
            listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticked);
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "MostrarComprasPendientes();", true);
        }

        protected void BtnDetalleComprasRealizadas_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasRealizadasByTicket(ticked);
        }

        protected void BtnDetalleComprasRealizadasEfectivoGO_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasRealizadasEfectivoGo(ticked);
        }

        protected void BtnDetalleComprasRealizadasVisa_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasRealizadasVisaNet(ticked);
        }

        protected void DetalleComprasPendienteEfectivo_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasRealizadasEfectivo(ticked);
        }

        protected void DetalleComprasPendientePagoEfec_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasPagoEfec(ticked);
        }

        protected void BtnDetalleComprasRealizadasPagoEfectGO_Click(object sender, EventArgs e)
        {
            ticked = Ticket.Value;
            VerDetalleComprasPagoEfec(ticked);
        }

        private void VerDetalleComprasPagoEfec(string ticked)
        {
            listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticked);
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "MostrarComprasPendientesPagoEfec();", true);
        }
        [WebMethod()]
        public static List<DetalleCompra> DetalleComprasPagoEfect(string Ticket) //Charles 24/03/2021
        {
            return DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(Ticket);
        }

        private void VerDetalleComprasRealizadasEfectivo(string ticked)
        {
            listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticked);
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "MostrarComprasPendientesEfectivo();", true);
        }
        [WebMethod()]
        public static List<DetalleCompra> DetalleComprasRealizadasEfectivo(string Ticket) //Charles 24/03/2021
        {
            return DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(Ticket);
        }


        private void VerDetalleComprasRealizadasByTicket(string ticked)
        {
            listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticked);
            listaComprasGeneral = CompraNegocios.getInstance().ListaComprasGeneralMisCompras();
            fotoVoucherCompra = (from c in listaComprasGeneral where c.Ticket.Trim() == ticked select c.FotoVaucher.Trim()).SingleOrDefault();
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "MostrarComprasRealizadas();", true);
        }

        private void VerDetalleComprasRealizadasVisaNet(string ticked)
        {
            listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticked);
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "MostrarComprasRealizadasVisa();", true);
        }

        private void VerDetalleComprasRealizadasEfectivoGo(string ticked)
        {
            listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticked);
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "MostrarComprasRealizadasEfectivo();", true);
        }

        protected void BtnEditar_Click(object sender, EventArgs e)
        {
            ViewState["Ticket"] = null;
            ViewState["Ticket"] = Ticket.Value;
            string tick = (string)ViewState["Ticket"];

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowModalEditar();", true);
            comprasPen.Attributes.Add("display", "block");
            LlenarEstablecimiento();
            List<Compra> valCDR = CompraNegocios.getInstance().CDRxTicket(tick.Trim());
            ComboTienda.Items.FindByValue(valCDR[0].Despacho).Selected = true;
            TxtMonto.Value = Convert.ToString(valCDR[0].MontoAPagar);
        }

        protected void BtnEliminar_Click(object sender, EventArgs e)
        {
            tickedEliminar = Ticket.Value;
            RecuperarStock(tickedEliminar);
            bool eliminar = CompraNegocios.getInstance().EliminarCompraCliente(tickedEliminar);
            Response.Redirect("MisComprasV2.aspx");
        }

        [WebMethod()]
        public static bool EliminarCompraCliente(string Ticket) //Charles 24/03/2021
        {
            return CompraNegocios.getInstance().EliminarCompraCliente(Ticket);
        }

        private void RecuperarStock(string ticket)
        {
            List<Compra> ListaP = CompraNegocios.getInstance().ListarProductosxTicket(ticket);
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();

            foreach (var item in ListaP)
            {
                string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == item.Despacho.Trim() select c.DNICDR.Trim()).FirstOrDefault();
                bool recuperar = StockLN.getInstance().IncrementarStock(dniCDR, item.IdProductoPais, item.Cantidad);
            }
        }
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    string prueba = "";
        //    string prueba2 = "";
        //}
        public decimal TextoaDecimal(string s)
        {
            var clone = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            clone.NumberFormat.NumberDecimalSeparator = ",";
            clone.NumberFormat.NumberGroupSeparator = ".";
            // ejemplo string s = "1,14535765" o string s="1.141516";
            decimal d = decimal.Parse(s, clone);

            return d;
        }

        protected void BtnAgregarVaucher_Click(object sender, EventArgs e)
        {
            //if (datepicker.Value == "" | datepicker.Value == null)
            //{
            //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Porfavor ingrese la fecha del voucher!');", true);
            //}
            //else
            //{
            //DATOS PARA EL REGISTRO   
            try
            {
                string ticket = "";
                ticket = HiddenTicket.Value;//  (string)ViewState["Ticket"];
                string numOperacion = TxtNumOperacion.Value;
                string banco = cboBanco.SelectedIndex.ToString();
                string comprobante = cboComprobante.SelectedIndex.ToString();
                string tipopago = cboTipoPago.SelectedIndex.ToString();
                string nomBanco = "";
                string nomComprobante = "";
                string sendTipoPago = "";
                DateTime fechaVaucher = DateTime.ParseExact(datepicker.Value, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                string mont = TxtMonto.Value;
                string goSend = mont.Replace(".", ",").ToString();
                double monto = Convert.ToDouble(goSend);
                string fotoVaucher = imagen.Value;

                //DATOS PARA OBTENER IdopPeruShóp
                Compra compra = new Compra();
                Compra compraPC = new Compra();
                List<Cliente> clientes = new List<Cliente>();
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticket);
                List<Cliente> send = new List<Cliente>();
                send = ClienteLN.getInstance().ListaEstable();
                clientes = ClienteLN.getInstance().ListarCliente();
                compra = CompraNegocios.getInstance().DatosCompraParaObtenerIdopPeruShop(ticket);
                compraPC = CompraNegocios.getInstance().DatosPuntosYMontosCompra(ticket);

                string dniComprador = Convert.ToString(Session["NumDocCliente"]);
                string dniPatrocinador = Convert.ToString(Session["PatrocinadorDNI"]);
                string dniFactorComision = Convert.ToString(Session["factorComision"]);
                string packetesocio = Convert.ToString(Session["PacketeSocio"]);
                string local = HiddenTienda.Value;//  cmbTienda.SelectedIndex.ToString();  //ComboTienda.SelectedItem.Value;
                string localORG = HiddenTienda.Value; //cmbTienda.SelectedIndex.ToString(); //ComboTienda.SelectedItem.Value;
                string nombreTienda = HiddenTienda.Value; // cmbTienda.SelectedIndex.ToString(); //ComboTienda.SelectedItem.Text;
                string direccionComprador = (from c in clientes where c.numeroDoc.Trim() == dniComprador.Trim() select c.direccion).SingleOrDefault();
                string dir = "";
                string ubi = "";
                string referen = "";
                string idCliente = (string)Session["IdCliente"];
                //string apodo = (from c in clientes where c.IdPeruShop.Trim() == local select c.apodo).FirstOrDefault();
                nombreEnviar = HiddenField_NombreCliente.Value; // (from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.NombreCliente.Trim()).SingleOrDefault();
                idTipoCompra = HiddenField_idTipoCompra.Value;// (from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.idTipoCompra.Trim()).SingleOrDefault();
                fechaSimple2 = HiddenField_FechaPago.Value;// (from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.FechaPago).FirstOrDefault();
                tipoCompra = HiddenField_TipoCompra.Value;// (from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.TipoCompra.Trim()).SingleOrDefault();
                NotaDelivery = HiddenField_NotaDelivery.Value;// (from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.NotaDelivery.Trim()).SingleOrDefault();
                rucFactura = HiddenField_Ruc.Value;// (from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.Ruc.Trim()).SingleOrDefault();
                string montoValidar_pre = HiddenMondoPagar.Value.ToString();
                //TextoaDecimal(montoValidar_pre);
                //decimal montoValidar = TextoaDecimal(montoValidar_pre); //Convert.ToDouble(montoValidar_pre);  // Convert.ToDouble((from c in listaComprasPendientes where c.Ticket.Trim() == Ticket.Value select c.MontoAPagar).SingleOrDefault());
                double montoValidar = Convert.ToDouble(montoValidar_pre.Replace(".", ","));
                estableMail = (from c in send where c.IdPeruShop.Trim() == local.Trim() select c.apodo.Trim()).FirstOrDefault();

                //if (File.Exists(Server.MapPath("~/voucher/" + fotoVaucher)))
                //{
                //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Cambie el nombre de la imagen porfavor!');", true);
                //}
                //else 

                if (monto != montoValidar)
                {
                    // ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Monto Incorrecto, Ingrese el monto total porfavor -" + montoValidar + "-" + monto + "!');", true);
                }
                else
                {
                    string notaSend = tipoCompra + " // " + Convert.ToString(fechaSimple2) + " // " + NotaDelivery;
                    //OBTENER IDOPERUSHOP
                    string idopPeruShop = ObtenerIdopCompra(dniComprador, local, localORG, nombreEnviar, direccionComprador, notaSend, dir, ubi, referen, listaDetalleCompra, idTipoCompra);
                    //string datosComisiones = RelacionarComisiones(dniComprador, dniPatrocinador, packetesocio, idopPeruShop, idTipoCompra, nombreEnviar, fechaSimple);

                    DateTime fechaSimple3 = DateTime.ParseExact(fechaSimple2, @"dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    //  Console.WriteLine(dateTime);


                    actualizarPuntos(dniComprador, dniFactorComision, dniPatrocinador, packetesocio, compraPC, fechaSimple3, idCliente);
                    actualizarComisiones(dniComprador, dniFactorComision, dniPatrocinador, packetesocio, compraPC, fechaSimple3, idTipoCompra);

                    //if (imagen.PostedFile.ContentLength > 0)
                    //{

                    //    fotoVaucher = "voucher/" + imagen.PostedFile.FileName;
                    //    GuardarImagenEnCarpeta(imagen.PostedFile);
                    //}
                    //else
                    //{
                    //    fotoVaucher = "";
                    //}
                    string idCliSend = Convert.ToString(Session["IdCliente"]);
                    GuardarVoucherEnCarpeta(idCliSend);

                    ////////////////////////////////////////
                    if (banco == "1")
                    {
                        nomBanco = "BCP";
                    }
                    else if (banco == "2")
                    {
                        nomBanco = "BBVA RECAUDO";
                    }
                    else
                    {
                        nomBanco = "BBVA";
                    }

                    ////////////////////////////////////////////
                    ///
                    ticket = HiddenTicket.Value;
                    DirectoryInfo dir3 = new DirectoryInfo(Server.MapPath("voucher/" + ticket));
                    bool folderExists = Directory.Exists(Server.MapPath("voucher/" + ticket));
                    string filename_voucher = "";
                    string imagen2 = "";
                    if (folderExists == true)
                    {
                        foreach (FileInfo file3 in dir3.GetFiles())
                        {
                            imagen2 = file3.Name;
                            filename_voucher = Server.MapPath("voucher/" + ticket + "/" + imagen2);
                        }
                    }


                    bool ok = CompraNegocios.getInstance().RegistrarDepositoAndActualizarCompra(ticket, numOperacion, nomBanco, fechaVaucher, monto, imagen2, idopPeruShop, local);

                    if (idTipoCompra == "08")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "02");
                    }
                    else if (idTipoCompra == "09")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "03");
                    }
                    else if (idTipoCompra == "10")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "04");
                    }
                    else if (idTipoCompra == "11")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "03");
                    }
                    else if (idTipoCompra == "12")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "04");
                    }
                    else if (idTipoCompra == "13")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "04");
                    }
                    else if (idTipoCompra == "24")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "23");
                    }
                    else if (idTipoCompra == "25")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "23");
                    }
                    else if (idTipoCompra == "26")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "23");
                    }
                    else if (idTipoCompra == "27")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idCliente, "23");
                    }

                    if (ok)
                    {
                        if (banco == "1") //215
                        {
                            nomBanco = "215";
                        }
                        else if (banco == "2")//227
                        {
                            nomBanco = "227";
                        }
                        else //197
                        {
                            nomBanco = "197";
                        }
                        string ruta = Server.MapPath("~/" + fotoVaucher);
                        //string imagenBase64 = Conversor(ruta);
                        string fechaMove = Convert.ToString(fechaVaucher.ToString("yyyy/MM/dd"));
                        string montoMove = Convert.ToString(monto);

                        //string idMoveSend = ObtenerIDMOVE(nomBanco, idopPeruShop, numOperacion, fechaMove, montoMove, local, tipopago, imagenBase64);

                        //var jsonObje = JObject.Parse(idMoveSend);
                        //string idmoveGo = (string)jsonObje["idmove"];
                        //string idliquiGo = (string)jsonObje["idliq"];
                        //string estadoGo = (string)jsonObje["0"];

                        //if (estadoGo == "2")
                        //{
                        //    bool duplicado = CompraNegocios.getInstance().ActualizarDepositoDuplicado(ticket);
                        //    ListaComprasPendientesDelCliente(dniComprador);
                        //    ListaComprasPendientesAprobacionDelCliente(dniComprador);
                        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Deposito duplicado! Porfavor vuelva a registrar su deposito');", true);
                        //} else if (idliquiGo == null | idliquiGo == "")
                        //{
                        //    bool duplicado = CompraNegocios.getInstance().ActualizarDepositoDuplicado(ticket);
                        //    ListaComprasPendientesDelCliente(dniComprador);
                        //    ListaComprasPendientesAprobacionDelCliente(dniComprador);
                        //    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Datos incorrectos, porfavor escriba otra vez!');", true);
                        //}
                        //else
                        //{
                        //bool oki = CompraNegocios.getInstance().ActualizarCompraMove(ticket, idmoveGo, idliquiGo, estadoGo);


                        if (banco == "1")
                        {
                            nomBanco = "BCP";
                        }
                        else if (banco == "2")
                        {
                            nomBanco = "BBVA RECAUDO";
                        }
                        else
                        {
                            nomBanco = "BBVA";
                        }

                        ///////////////////////////////////////
                        if (tipopago == "1")
                        {
                            sendTipoPago = "TRANSFERENCIA";
                        }
                        else
                        {
                            sendTipoPago = "DEPOSITO";
                        }

                        if (comprobante == "1")
                        {
                            nomComprobante = "Boleta";
                        }
                        else { nomComprobante = "Factura"; }

                        string estable = "";
                        string correo = "";
                        estable = HiddenTienda.Value;// ComboTienda.SelectedItem.Value;

                        //////////////////////////////////////////////////////////////////

                        if (estable == "AlmacenSUR")
                        {
                            correo = "compras.multinivel@santanaturaperu.net"; /*almacensur @santanaturaperu.net*/
                        }
                        else if (estable == "OlivosRed")
                        {
                            correo = "cdr.losolivos@santanaturaperu.net";
                        }
                        else
                        {
                            correo = "compras.multinivel@santanaturaperu.net";
                        }

                        //correo = "chajuraceentel@gmail.com";

                        MailMessage mail = new MailMessage();
                        mail.CC.Add(new MailAddress(correo));
                        mail.CC.Add(new MailAddress("pedidos.snn@gmail.com"));
                        mail.Bcc.Add(new MailAddress("santanaturavouchers@gmail.com"));

                        //mail.CC.Add(new MailAddress("chajuraceentel2@gmail.com"));
                        //mail.Bcc.Add(new MailAddress("chajuraceentel2@gmail.com"));

                        mail.From = new MailAddress("santanaturavoucher@mundosantanatura.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
                        //mail.From = new MailAddress("chajuraceentel@gmail.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
                        mail.Subject = "Nuevo Deposito de " + nombreEnviar + " Establecimiento: " + nombreTienda;
                        mail.SubjectEncoding = System.Text.Encoding.UTF8;
                        mail.Body = string.Format("Numero de Operacion: {1}{0}Banco:{2}{0}FechaVoucher: {3}{0}Monto: {4}{0}IDOP: {5}{0}TipoPago: {6}{0}DNI Cliente: {7}{0}TipoCompra: {8}{0}Comprobante: {9}{0}RUC: {10}{0}Fecha Compra: {11}{0}",
                                    Environment.NewLine, numOperacion, nomBanco, fechaMove, montoMove, idopPeruShop, sendTipoPago, dniComprador, tipoCompra + " // " + NotaDelivery, nomComprobante, rucFactura, Convert.ToString(fechaSimple3));
                        //Environment.NewLine, numOperacion, nomBanco, fechaMove, montoMove, idopPeruShop, sendTipoPago, dniComprador, tipoCompra + " // " + NotaDelivery, nomComprobante, rucFactura, Convert.ToString(fechaSimple));
                        mail.BodyEncoding = System.Text.Encoding.UTF8;
                        mail.IsBodyHtml = false;

                        // mail.Attachments.Add(new Attachment(imagen.PostedFile.InputStream, imagen.PostedFile.FileName));
                        mail.Attachments.Add(new Attachment(filename_voucher));

                        SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
                        // SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                        //smtp.Credentials = new System.Net.NetworkCredential("chajuraceentel@gmail.com", "Peru12345.");
                        smtp.Credentials = new System.Net.NetworkCredential("santanaturavoucher@mundosantanatura.com", "s@nt@2019");

                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                        //smtp.Port = 587;
                        //smtp.Host = "smtp.gmail.com";
                        //smtp.EnableSsl = true; //Charles
                        try
                        {
                            smtp.Send(mail);
                        }
                        catch (SmtpException ex)
                        {
                            estado = false;
                            merror = ex.Message.ToString();
                        }
                        //string correoSocio = Convert.ToString(Session["Correo"]);
                        //if (correoSocio != "") { EnviarCorreoSocio(correoSocio, idopPeruShop); }

                        //}
                        ViewState["Ticket"] = "";
                        nombreEnviar = "";
                        idTipoCompra = "";
                        tipoCompra = "";
                        estableMail = "";
                        Response.Redirect("MisComprasV2.aspx");
                        //ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + datosComisiones + "!');", true);
                    }
                    //}
                    else
                    {
                        bool duplicado = CompraNegocios.getInstance().ActualizarDepositoDuplicado(ticket);
                        ListaComprasPendientesDelCliente(idCliente);
                        ListaComprasRealizadasDeposito(idCliente);
                        ListaComprasPendientesEfectivoCliente(idCliente);
                        ListaComprasPendientesPagoEfecCliente(idCliente);
                        ViewState["Ticket"] = "";
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Wrong data!');", true);
                    }

                }
            }

            catch (SmtpException ex)
            {
                //estado = false;
                //merror = ex.Message.ToString();
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Register Error!');", true);
            }

            //}

        }

        private void EnviarCorreoSocio(string correo, string IDOP)
        {
            MailMessage mail = new MailMessage();
            mail.CC.Add(new MailAddress(correo));
            mail.Bcc.Add(new MailAddress("santanaturavouchers@gmail.com"));
            mail.From = new MailAddress("santanaturavoucher@mundosantanatura.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
            mail.Subject = "Nueva compra realizada en SantaNaturaNetwork";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = string.Format("Numero de Operacion: {1}{0}IDOP:",
                        Environment.NewLine, IDOP);
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            mail.Attachments.Add(new Attachment(imagen.PostedFile.InputStream, imagen.PostedFile.FileName));

            SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
            smtp.Credentials = new System.Net.NetworkCredential("santanaturavoucher@mundosantanatura.com", "s@nt@2019");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                estado = false;
                merror = ex.Message.ToString();
            }
        }

        private void GuardarImagenEnCarpeta(HttpPostedFile imagenFile)
        {
            string nombreImagen = imagenFile.FileName.ToString();
            string ruta = "voucher/" + nombreImagen;

            imagenFile.SaveAs(Server.MapPath(ruta));
        }
        private void GuardarVoucherEnCarpeta(string idCliSend)
        {
            DirectoryInfo dir3 = new DirectoryInfo(Server.MapPath("voucher_temporal/" + idCliSend));
            bool folderExists = Directory.Exists(Server.MapPath("voucher_temporal/" + idCliSend));
            string ticket = HiddenTicket.Value;
            string sourcePath;
            string targetPath;

            if (folderExists == true)
            {
                string imagen;
                foreach (FileInfo file3 in dir3.GetFiles())
                {

                    imagen = file3.Name;
                    sourcePath = Server.MapPath("voucher_temporal/" + idCliSend + "/" + imagen);

                    DirectoryInfo dir_voucher = new DirectoryInfo(Server.MapPath("voucher/" + ticket));
                    bool folderExistsvoucher = Directory.Exists(Server.MapPath("voucher/" + ticket));
                    if (folderExistsvoucher == true)
                    {
                        //foreach (FileInfo file in dir_voucher.GetFiles())
                        //{
                        //    file.Delete();                            
                        //}
                        targetPath = Server.MapPath("voucher/" + ticket + "/" + imagen);
                        System.IO.File.Copy(sourcePath, targetPath, true);
                    }
                    else
                    {
                        System.IO.Directory.CreateDirectory(Server.MapPath("voucher/" + ticket));
                        targetPath = Server.MapPath("voucher/" + ticket + "/" + imagen);
                        System.IO.File.Copy(sourcePath, targetPath, true);

                    }

                    file3.Delete();

                }

                System.IO.Directory.Delete(Server.MapPath("voucher_temporal/" + idCliSend));

            }
        }
        private string Conversor(string Path)
        {
            byte[] imageArray = System.IO.File.ReadAllBytes(Path);
            string base64ImageRepresentation = Convert.ToBase64String(imageArray);
            return base64ImageRepresentation;
        }

        private string ObtenerIdopCompra(string dniComprador, string local, string localORG, string nombreEnvia, string direccionComprador, string not, string dir, string ubi, string refe, List<DetalleCompra> listaDetalleCompra, string idTC)
        {
            string linqApodo = "";
            List<List<string>> listProductos = new List<List<string>>();
            List<ProductoV2> ListaProductoG = ProductoLN.getInstance().ListaIdopXNombre();
            //List<Cliente> clientes = new List<Cliente>();
            //clientes = ClienteLN.getInstance().ListaEstable();
            //linqApodo = (from c in clientes where c.IdPeruShop.Trim() == local.Trim() select c.numeroDoc.Trim()).FirstOrDefault();
            string estable = "";
            estable = HiddenTienda.Value; //ComboTienda.SelectedItem.Value;

            string idop = "";
            int numPromo = 150;
            try
            {
                foreach (var item in listaDetalleCompra)//22 y 56
                {
                    int cantC = 0;
                    string regalo = "0";
                    double sendMoney = 0.0000;
                    double precioSend = 0.0000;

                    //if (item.idPaquete != "0")
                    //{
                    //    listaPaqueteNatura = PaqueteLN.getInstance().ListaPaqueteByCodigoPeru(item.idPaquete);
                    //    foreach (var item2 in listaPaqueteNatura)
                    //    {
                    //        string IDPS = (from c in ListaProductoG where c.IdProducto.Trim() == item2.idProductoGeneral.Trim() select c.IdProductoPeruShop.Trim()).FirstOrDefault();
                    //        cantC = item.CantiPS * item2.cantidadGeneral;
                    //        precioSend = (item.PrecioPS * item.CantiPS) / (item2.cantidadAglomerada * item.CantiPS);
                    //        numPromo += 1;
                    //        if (item.PrecioPS <= 0.05) { regalo = "1"; }
                    //        List<string> productos = new List<string>();
                    //        productos.Add(IDPS);
                    //        productos.Add(Convert.ToString(cantC));
                    //        productos.Add(precioSend.ToString("N3").Replace(",", "."));
                    //        productos.Add(Convert.ToString(numPromo));
                    //        productos.Add("0");
                    //        productos.Add(IDPS);
                    //        productos.Add(regalo);
                    //        listProductos.Add(productos);
                    //    }

                    //}
                    //else {
                    //    string promo = "";
                    //    if (item.LineaSend == "05") { numPromo += 1; promo = Convert.ToString(numPromo); }
                    //    if (item.PrecioPS <= 0.05) { regalo = "1"; }
                    //    precioSend = item.PrecioPS;
                    //    List<string> productos = new List<string>();
                    //    productos.Add(item.IdProductoPeruShop);
                    //    productos.Add(Convert.ToString(item.CantiPS));
                    //    productos.Add(precioSend.ToString("N3").Replace(",", "."));
                    //    productos.Add(promo);
                    //    productos.Add("0");
                    //    productos.Add(item.IdProductoPeruShop);
                    //    productos.Add(regalo);
                    //    listProductos.Add(productos);110217
                    //}

                    if (item.IdProductoPeruShop == "110038")
                    {

                        precioSend = 6.00;
                        List<string> productos = new List<string>();
                        productos.Add("2069");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("1");
                        productos.Add("0");
                        productos.Add("2069");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        cantC = item.CantiPS * 2;
                        double precioSend2 = 7.00;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108683");
                        productos2.Add(Convert.ToString(cantC));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("2");
                        productos2.Add("0");
                        productos2.Add("108683");
                        productos2.Add(regalo);
                        listProductos.Add(productos2);

                    }
                    else if (item.IdProductoPeruShop == "0001")
                    {
                        cantC = item.CantiPS * 12;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110107");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("3");
                        productos.Add("0");
                        productos.Add("110107");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0002")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109949");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("4");
                        productos.Add("0");
                        productos.Add("109949");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0003")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109948");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("5");
                        productos.Add("0");
                        productos.Add("109948");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0004")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("6");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0005")
                    {
                        cantC = 3;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("7");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0006")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("8");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0007")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("9");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0008")
                    {
                        cantC = 10;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("10");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0009")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("11");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0010")
                    {
                        cantC = item.CantiPS * 10;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("12");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0011")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("13");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0012")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("14");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0013")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("15");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0014")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("16");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0015")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("17");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0016")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("18");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0017")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(1));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("19");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(2));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("20");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0018")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(2));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("21");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(4));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("22");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0019")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(4));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("23");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(8));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("24");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0020")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("47");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("25");
                        productos.Add("0");
                        productos.Add("47");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0021")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("255");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("26");
                        productos.Add("0");
                        productos.Add("255");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        double precioSend2 = item.PrecioPS * item.CantiPS;
                        double sendMoney2 = precioSend2 / cantC2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("256");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("27");
                        productos2.Add("0");
                        productos2.Add("256");
                        productos2.Add("0");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0022")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("28");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0023")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110242");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("29");
                        productos.Add("0");
                        productos.Add("110242");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("6063");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("30");
                        productos2.Add("0");
                        productos2.Add("6063");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0024")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("31");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0025")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110324");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("32");
                        productos.Add("0");
                        productos.Add("110324");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0026")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("33");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110342");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("34");
                        productos2.Add("0");
                        productos2.Add("110342");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0027")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / (item.CantiPS * 19);
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("35");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("328");
                        productos2.Add(Convert.ToString(cantC2));
                        productos2.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos2.Add("36");
                        productos2.Add("0");
                        productos2.Add("328");
                        productos2.Add("0");
                        listProductos.Add(productos2);

                        int cantC3 = item.CantiPS * 2;
                        List<string> productos3 = new List<string>();
                        productos3.Add("1750");
                        productos3.Add(Convert.ToString(cantC3));
                        productos3.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos3.Add("37");
                        productos3.Add("0");
                        productos3.Add("1750");
                        productos3.Add("0");
                        listProductos.Add(productos3);

                        int cantC4 = item.CantiPS * 3;
                        List<string> productos4 = new List<string>();
                        productos4.Add("47");
                        productos4.Add(Convert.ToString(cantC4));
                        productos4.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos4.Add("38");
                        productos4.Add("0");
                        productos4.Add("47");
                        productos4.Add("0");
                        listProductos.Add(productos4);

                        int cantC5 = item.CantiPS * 1;
                        List<string> productos5 = new List<string>();
                        productos5.Add("35");
                        productos5.Add(Convert.ToString(cantC5));
                        productos5.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos5.Add("39");
                        productos5.Add("0");
                        productos5.Add("35");
                        productos5.Add("0");
                        listProductos.Add(productos5);

                        int cantC6 = item.CantiPS * 2;
                        List<string> productos6 = new List<string>();
                        productos6.Add("107");
                        productos6.Add(Convert.ToString(cantC6));
                        productos6.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos6.Add("40");
                        productos6.Add("0");
                        productos6.Add("107");
                        productos6.Add("0");
                        listProductos.Add(productos6);

                        int cantC7 = item.CantiPS * 1;
                        List<string> productos7 = new List<string>();
                        productos7.Add("69");
                        productos7.Add(Convert.ToString(cantC7));
                        productos7.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos7.Add("41");
                        productos7.Add("0");
                        productos7.Add("69");
                        productos7.Add("0");
                        listProductos.Add(productos7);

                        int cantC8 = item.CantiPS * 3;
                        List<string> productos8 = new List<string>();
                        productos8.Add("269");
                        productos8.Add(Convert.ToString(cantC8));
                        productos8.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos8.Add("42");
                        productos8.Add("0");
                        productos8.Add("269");
                        productos8.Add("0");
                        listProductos.Add(productos8);

                        int cantC9 = item.CantiPS * 2;
                        List<string> productos9 = new List<string>();
                        productos9.Add("6");
                        productos9.Add(Convert.ToString(cantC9));
                        productos9.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos9.Add("43");
                        productos9.Add("0");
                        productos9.Add("6");
                        productos9.Add("0");
                        listProductos.Add(productos9);

                        int cantC10 = item.CantiPS * 1;
                        List<string> productos10 = new List<string>();
                        productos10.Add("81");
                        productos10.Add(Convert.ToString(cantC10));
                        productos10.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos10.Add("44");
                        productos10.Add("0");
                        productos10.Add("81");
                        productos10.Add("0");
                        listProductos.Add(productos10);
                    }
                    else if (item.IdProductoPeruShop == "0028")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / (item.CantiPS * 15);
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("45");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108954");
                        productos2.Add(Convert.ToString(cantC2));
                        productos2.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos2.Add("46");
                        productos2.Add("0");
                        productos2.Add("108954");
                        productos2.Add("0");
                        listProductos.Add(productos2);

                        int cantC3 = item.CantiPS * 2;
                        List<string> productos3 = new List<string>();
                        productos3.Add("110242");
                        productos3.Add(Convert.ToString(cantC3));
                        productos3.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos3.Add("47");
                        productos3.Add("0");
                        productos3.Add("110242");
                        productos3.Add("0");
                        listProductos.Add(productos3);

                        int cantC4 = item.CantiPS * 2;
                        List<string> productos4 = new List<string>();
                        productos4.Add("47");
                        productos4.Add(Convert.ToString(cantC4));
                        productos4.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos4.Add("48");
                        productos4.Add("0");
                        productos4.Add("47");
                        productos4.Add("0");
                        listProductos.Add(productos4);

                        int cantC5 = item.CantiPS * 2;
                        List<string> productos5 = new List<string>();
                        productos5.Add("311");
                        productos5.Add(Convert.ToString(cantC5));
                        productos5.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos5.Add("49");
                        productos5.Add("0");
                        productos5.Add("311");
                        productos5.Add("0");
                        listProductos.Add(productos5);

                        int cantC6 = item.CantiPS * 3;
                        List<string> productos6 = new List<string>();
                        productos6.Add("21");
                        productos6.Add(Convert.ToString(cantC6));
                        productos6.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos6.Add("50");
                        productos6.Add("0");
                        productos6.Add("21");
                        productos6.Add("0");
                        listProductos.Add(productos6);

                        int cantC7 = item.CantiPS * 2;
                        List<string> productos7 = new List<string>();
                        productos7.Add("35");
                        productos7.Add(Convert.ToString(cantC7));
                        productos7.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos7.Add("51");
                        productos7.Add("0");
                        productos7.Add("35");
                        productos7.Add("0");
                        listProductos.Add(productos7);
                    }
                    else if (item.IdProductoPeruShop == "0029")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("108954");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("52");
                        productos.Add("0");
                        productos.Add("108954");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("311");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("53");
                        productos2.Add("0");
                        productos2.Add("311");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0030")
                    {
                        cantC = 3;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("53");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0031")
                    {
                        cantC = 5;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("54");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0032")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110239");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("55");
                        productos.Add("0");
                        productos.Add("110239");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110395");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("56");
                        productos2.Add("0");
                        productos2.Add("110395");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0033")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("57");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0034")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("58");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0035")
                    {
                        cantC = 6;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("59");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0036")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110243");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("60");
                        productos.Add("0");
                        productos.Add("110243");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0037")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110100");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("61");
                        productos.Add("0");
                        productos.Add("110100");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0038")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("24");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("62");
                        productos.Add("0");
                        productos.Add("24");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0039")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110241");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("63");
                        productos.Add("0");
                        productos.Add("110241");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110415");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("64");
                        productos2.Add("0");
                        productos2.Add("110415");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0040")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("65");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0041")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("66");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0042")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("269");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("67");
                        productos.Add("0");
                        productos.Add("269");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0043")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110240");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("68");
                        productos.Add("0");
                        productos.Add("110240");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110191");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("69");
                        productos2.Add("0");
                        productos2.Add("110191");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0044")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("70");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0045")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("71");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0046")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("72");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0047")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("73");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else
                    {
                        string promo = "";
                        if (item.LineaSend == "05" | item.LineaSend == "02" | item.LineaSend == "01") { numPromo += 1; promo = Convert.ToString(numPromo); }
                        if (item.PrecioPS <= 0.06) { regalo = "1"; precioSend = 0.05; }
                        else { precioSend = item.PrecioPS; }
                        List<string> productos = new List<string>();
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add(promo);
                        productos.Add("0");
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }

                }

                string serializeProds = JsonConvert.SerializeObject(listProductos);

                string prod = serializeProds;

                string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("a", "pCatalogo");
                nvc.Add("canal", "RED");
                nvc.Add("tipo", "2");
                nvc.Add("fpago", "DEPOSITO");
                nvc.Add("pagado", "0");
                nvc.Add("ruc", dniComprador.Trim());
                nvc.Add("local", estable);
                nvc.Add("localorg", estable);
                nvc.Add("razon", nombreEnvia);
                nvc.Add("dir_ruc", direccionComprador);
                nvc.Add("log", "CREAVIR");
                nvc.Add("not", not);
                nvc.Add("delivery", "0");
                nvc.Add("dir", dir);
                nvc.Add("ubi", ubi);
                nvc.Add("ref", refe);
                nvc.Add("moneda", "PEN");
                nvc.Add("prod", prod);

                var data = wc.UploadValues(url, "POST", nvc);

                var responseString = UnicodeEncoding.UTF8.GetString(data);

                idop = responseString;
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return idop;
        }

        private string RelacionarComisiones(string dni, string dniPatro, string packete, string idop, string idTipoCom, string nombre, DateTime fechaSim)
        {
            string send = fechaSim.AddHours(-3).ToString("yyyy/MM/dd");
            string idSimplex = "";
            string uplineDNI = "";
            idSimplex = Convert.ToString(Session["IdSimplex"]);
            uplineDNI = Convert.ToString(Session["UplineDNI"]);
            //DateTime res2 = res.AddHours(-5);
            //string res3 = Convert.ToString(res2);
            //DateTime send = DateTime.ParseExact("07/08/2019", @"yyyy/M/d", System.Globalization.CultureInfo.InvariantCulture);

            string fecha = Convert.ToString(send);
            string paque = "";
            string tipoC = "";

            if (packete == "01")
            {
                paque = "BASICO";
            }
            else if (packete == "02")
            {
                paque = "PROFESIONAL";
            }
            else if (packete == "03")
            {
                paque = "EMPRESARIAL";
            }
            else if (packete == "04")
            {
                paque = "MILLONARIO";
            }
            else if (packete == "05")
            {
                paque = "CONSULTOR";
            }
            else
            {
                paque = "CONSUMIDOR";
            }


            /////////////////////////////////////////////////////

            if (idTipoCom == "01" | idTipoCom == "02" | idTipoCom == "03" | idTipoCom == "04" | idTipoCom == "05" | idTipoCom == "06")
            {
                tipoC = "AF";
            }
            else if (idTipoCom == "07")
            {
                tipoC = "PE";
            }
            else
            {
                tipoC = "UP";
                if (idTipoCom == "08")
                {
                    paque = "PROFESIONAL";
                }
                else if (idTipoCom == "09")
                {
                    paque = "EMPRESARIAL";
                }
                else if (idTipoCom == "10")
                {
                    paque = "MILLONARIO";
                }
                else if (idTipoCom == "11")
                {
                    paque = "EMPRESARIAL";
                }
                else if (idTipoCom == "12")
                {
                    paque = "MILLONARIO";
                }
                else if (idTipoCom == "12")
                {
                    paque = "MILLONARIO";
                }
                else
                {
                    paque = "MILLONARIO";
                }
            }

            string response = "";
            double sumaPunto = 0;
            double sumaPrecio = 0;
            try
            {
                foreach (var item in listaDetalleCompra)//22 y 56
                {
                    if (item.LineaSend == "02")
                    {
                        sumaPrecio = item.PrecioPS + sumaPrecio;
                    }

                    sumaPunto = item.Puntos + sumaPunto;

                }

                string sendPrecio = Convert.ToString(sumaPrecio);
                string sendPuntos = Convert.ToString(sumaPunto);

                string url = "http://18.219.245.17:8080/Sistema/index.php/WebServices/setResumenPeriodo";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("periodoID", "");
                nvc.Add("fecha", fecha);
                nvc.Add("usuarioID", idSimplex);
                nvc.Add("usuarioCod", "");
                nvc.Add("usuarioDNI", dni.Trim());
                nvc.Add("patrocinadorID", "");
                nvc.Add("patrocinadorCod", "");
                nvc.Add("patrocinadorDNI", dniPatro.Trim());
                nvc.Add("uplineID", "");
                nvc.Add("uplineCod", "");
                nvc.Add("uplineDNI", uplineDNI.Trim());
                nvc.Add("paqueteID", "");
                nvc.Add("paqueteCod", paque);
                nvc.Add("puntos", sendPuntos);
                nvc.Add("monto", sendPrecio);
                nvc.Add("referenciaPS", idop);
                nvc.Add("tipoOperacion", tipoC);
                nvc.Add("nombre", nombre);

                var data = wc.UploadValues(url, "POST", nvc);

                response = UnicodeEncoding.UTF8.GetString(data);
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return response;

        }

        private void actualizarPuntos(string dni, string dniFactorComision, string dniPatrocinador, string packetesocio, Compra compraPC, DateTime fechaSimple, string idCliente)
        {
            double PP = compraPC.PuntosTotal;
            double VIP = compraPC.PuntosTotal;
            double VP = compraPC.PuntosTotalPromo;
            double VR = compraPC.PuntosTotalPromo;
            double VG = compraPC.PuntosTotalPromo;
            string DOC = dni;
            int i = 0;
            int i2 = 0;
            int IDPeriodo = 0;
            int IDPeriodoComision = getPeriodoRango_Comision(fechaSimple);
            int IdPeriodoRango_Comi = PeriodoLN.getInstance().ObtenerIdperiodoRango_Comi(IDPeriodoComision);
            List<string> LFactorComision = new List<string>();
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarIdPeriodoTop2();
            bool ok = false;

            while (!ok && i2 < ListaPeriodos.Count())
            {
                DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (fechaSimple >= primerDiafechaActual & fechaSimple <= ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodo;
                }
                i2++;
            }

            while (DOC != "")
            {
                DOC = ClienteLN.getInstance().ListarFactorComision(DOC);
                if (DOC != "") { LFactorComision.Add(DOC); }
            }

            bool todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(dni, 0, VP, 0, VG, 0, IDPeriodo);
            todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(dni, PP, 0, 0, 0, 0, IdPeriodoRango_Comi);
            bool PVIP = CompraNegocios.getInstance().ActualizarPuntosVIP(dni, VIP, IdPeriodoRango_Comi);
            CompraNegocios.getInstance().ActualizarCorazonesRed(IdPeriodoRango_Comi, idCliente, compraPC.Corazones);

            if ((packetesocio == "05" | packetesocio == "06") & dniFactorComision != "")
            {
                bool VPP = CompraNegocios.getInstance().ActualizarPuntosComisiones(dniPatrocinador, 0, VP, 0, 0, 0, IDPeriodo);
                bool VIPP = CompraNegocios.getInstance().ActualizarPuntosVIP(dniPatrocinador, VIP, IdPeriodoRango_Comi);
            }


            foreach (var item in LFactorComision)
            {
                if ((packetesocio == "05" | packetesocio == "06") & i == 0)
                { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, 0, VG, 0, IDPeriodo); }
                else { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, VR, VG, 0, IDPeriodo); }
                i++;
            }

            if (packetesocio != "05" & packetesocio != "06")
            {
                bool VPR = CompraNegocios.getInstance().ActualizarVPR(dniFactorComision, IDPeriodo);
            }
            PuntosNegocios.getInstance().RegistrarDatosRangoIndividual(dni, IDPeriodo);

        }

        private void actualizarComisiones(string dni, string dniFactorComision, string dniPatrocinador, string packetesocio, Compra compraPC, DateTime fechaSimple, string idTPC)
        {
            double MCCI = compraPC.montoComisionCI;
            double MCCA = compraPC.montoComision;
            double MCCU = compraPC.montoComision;
            double MCCON = compraPC.montoComision * 0.05;
            int IDPeriodo = getPeriodoRango_Comision(fechaSimple);
            List<string> LFactorComision = new List<string>();
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            string fechaRegistro = ClienteLN.getInstance().ObtenerFechaRegistro(dniPatrocinador);
            double montoTiburon = CompraNegocios.getInstance().ObtenerMontoTiburon(dniPatrocinador);
            DateTime fechaConvertida = DateTime.ParseExact(fechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime primerDia = new DateTime(fechaConvertida.Year, fechaConvertida.Month, 1);
            DateTime ultimoDia = primerDia.AddMonths(1).AddDays(-1);
            DateTime DiaMenos5 = ultimoDia.AddDays(-5);
            DateTime PrimerDiaSig = ultimoDia.AddDays(1);
            string TCL = Convert.ToString(Session["TipoCliente"]);

            double PuntosPatro = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(dniFactorComision, IDPeriodo);
            int datoTiburon = PuntosNegocios.getInstance().ObtenerEvaluacionTiburon(dniPatrocinador, IDPeriodo);
            /*COMISION POR CONSULTOR*/
            if (packetesocio == "05" & dniFactorComision != "")
            { if (PuntosPatro >= 20) { bool COM = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniFactorComision, 0, 0, MCCON, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo); } }

            /*COMISION POR CLIENTE INTELIGENTE*/
            if (packetesocio == "06" & dniFactorComision != "")
            { if (PuntosPatro >= 20) { bool COM = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniFactorComision, 0, MCCI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo); } }

            /*COMISION POR AFILIACIONES*/
            if (idTPC != "05" & idTPC != "06" & idTPC != "07")
            {
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = dni;
                string dniUpline = Convert.ToString(Session["UplineDNI"]);
                List<ComAfiliacion> ListaPatrocinadores = new List<ComAfiliacion>();
                while (cc < 5 && !terminado)
                {
                    cc++;
                    ComAfiliacion agregar = ClienteLN.getInstance().ListaDatosPatrocinioAfiliaciones(IDPeriodo, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaPatrocinadores.Add(agregar); doc = agregar.documento.Trim(); }
                }
                foreach (var item in ListaPatrocinadores)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item.documento, IDPeriodo);
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "01" | ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 0)
                    {
                        double COMAFI = MCCA * 0.30;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, COMAFI, 0, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 1)
                    {
                        double COMAFI = MCCA * 0.05;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, COMAFI, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 2)
                    {
                        double COMAFI = MCCA * 0.03;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, COMAFI, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 3)
                    {
                        double COMAFI = MCCA * 0.02;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, 0, COMAFI, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 4)
                    {
                        double COMAFI = MCCA * 0.01;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, 0, 0, COMAFI);
                    }

                    ccc++;
                }
            }

            /*COMISION BONO TIBURON*/
            if (idTPC == "01" | idTPC == "02" | idTPC == "03" | idTPC == "04" | idTPC == "23")
            {
                if (PuntosPatro >= 20)
                {
                    if (datoTiburon < 2)
                    {
                        if (datoTiburon == 0)
                        {
                            if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                            {
                                string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                if (montoTiburon == 360)
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 0);
                                    }
                                }

                            }
                            else
                            {
                                string fecIniPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaInicio.Trim()).FirstOrDefault();
                                string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                if (montoTiburon == 360)
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 0);
                                    }
                                }

                            }
                        }
                        else if (datoTiburon == 1)
                        {
                            if (montoTiburon == 0)
                            {
                                if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                                {
                                    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 1);
                                    }
                                }
                                else
                                {
                                    string fecIniPe = Convert.ToString(PrimerDiaSig.ToShortDateString());
                                    string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 1);
                                    }
                                }
                            }
                            else if (montoTiburon == 360)
                            {
                                if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                                {
                                    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    string fecIniPe = Convert.ToString(PrimerDiaSig.ToShortDateString());
                                    string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /*COMISION BONO UNILEVEL -- ES UPLINE NO PATROCINADOR*/
            if (idTPC == "05" | idTPC == "06" | idTPC == "07")
            {
                if (TCL == "03") { MCCU = compraPC.montoComision - compraPC.montoComisionCI; }
                if (TCL == "05") { MCCU = compraPC.montoComision - MCCON; }
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = dni; if (TCL == "03" | TCL == "05") { doc = dniPatrocinador; ccc = 1; }
                double COMUNI = 0;
                List<PatrocinadorUnilivel> ListaUpline = new List<PatrocinadorUnilivel>();

                while (cc < 15 && !terminado)
                {
                    cc++;
                    PatrocinadorUnilivel agregar = ClienteLN.getInstance().ListaDatosUplineUnilevel(IDPeriodo, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaUpline.Add(agregar); doc = agregar.documento.Trim(); }
                }

                foreach (var item2 in ListaUpline)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDPeriodo);
                    if (PP >= 20 & ccc == 0 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.05;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 1 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 2 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 3 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 4 & (item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 5 & (item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.04;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 6 & (item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.03;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 7 & (item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.02;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 8 & (item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 9 & (item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 10 & (item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 11 & (item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 12 & (item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 13 & (item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 14 & (item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 15 & (item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 16 & (item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 17 & (item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 18 & (item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    ccc++;
                }
            }
        }

        private string ObtenerIDMOVE(string cueNatura, string idop, string numOpe, string fechaDep, string montoDep, string estable,
                                     string tipoDep, string imagenDep)
        {

            List<Cliente> clientes = new List<Cliente>();
            clientes = ClienteLN.getInstance().ListaEstable();
            string linqApodo = (from c in clientes where c.IdPeruShop.Trim() == estable.Trim() select c.numeroDoc.Trim()).FirstOrDefault();

            if (linqApodo == "0003" | linqApodo == "0510050" | linqApodo == "0004" | linqApodo == "0005" |
                linqApodo == "0006" | linqApodo == "0008" | linqApodo == "00009" | linqApodo == "000010" |
                linqApodo == "000011" | linqApodo == "000012" | linqApodo == "000013")
            {
                linqApodo = "RedAndina";
            }
            else
            {
                linqApodo = estable;
            }

            string idmove = "";
            string responseString = "";
            try
            {
                string url = "http://santanatura.cti.lat/santa2/webservices/pedidos.php";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("a", "pDepositos");
                nvc.Add("idc", cueNatura);
                nvc.Add("idop", "1101430");
                nvc.Add("nrodoc", numOpe);
                nvc.Add("fechadep", fechaDep);
                nvc.Add("monto", "30.10");
                nvc.Add("local", "CerroRED");
                nvc.Add("det", "");
                nvc.Add("tipopago", tipoDep);
                nvc.Add("canales", "RED");
                nvc.Add("imag64", imagenDep);

                var data = wc.UploadValues(url, "POST", nvc);
                responseString = UnicodeEncoding.UTF8.GetString(data);
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }

            return responseString;
        }

        private int getPeriodoRango_Comision(DateTime fechaSimple)
        {
            int i2 = 0;
            bool ok = false;
            int IDPeriodo = 0;
            DateTime primerDiafechaActual = DateTime.Now;
            DateTime ultimoDiafechaActual = DateTime.Now;
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            while (!ok && i2 < ListaPeriodos.Count())
            {
                primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (fechaSimple >= primerDiafechaActual & fechaSimple <= ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodoComision;
                }
                i2++;
            }
            return IDPeriodo;
        }


        private void LimpiarSessionSinCompra()
        {
            //Datos importantes para comprar
            Session["NumDocCliente"] = "";
            Session["NumDocClienteRegistrador"] = "";
            Session["TipoCliente"] = "";
            Session["NombrePatrocinador"] = "";
            Session["PatrocinadorDNI"] = "";
            Session["UplineDNI"] = "";
            Session["IdSimplex"] = "";
            Session["PromoAplicada"] = 0;
            Session["IdCliente"] = "";

            Session["Pais"] = "";
            Session["Departamento"] = "";
            Session["Provincia"] = "";
            Session["Distrito"] = "";
            Session["FechaNac"] = "";

            //
            Session["SubTotalPuntos"] = 0.00;
            Session["SubTotal"] = 0.00;
            Session["MontoAPagar"] = 0.00;
            Session["TipoPago"] = "";
            Session["Foto"] = "";
            Session["IdProducto"] = "";
            Session["Producto"] = "";
            Session["PrecioUnitario"] = 0.00;
            Session["Cantidad"] = 0;
            Session["VamosCompra"] = 0;
            Session["EliminarPromo"] = 0;
            Session["TiendaRetorna"] = 0;
            Session["ActualizaPag"] = 0;
            Session["codProducto"] = "";
            Session["codProducto2"] = "";
            Session["codProducto3"] = "";
            Session["codProducto4"] = "";
            Session["codProdBasico"] = "";
            Session["codProdProfe"] = "";
            Session["codProdEmpre"] = "";
            Session["codProdMillo"] = "";
            Session["codProdBasico2"] = "";
            Session["codProdProfe2"] = "";
            Session["codProdEmpre2"] = "";
            Session["codProdMillo2"] = "";
            Session["tipoCompPromo"] = "";
            Session["TipoAfiliacion"] = "";
            Session["cantRegalo"] = 0;
            Session["cboActivar"] = "0";
            Session["RecargaPagina"] = 0;

            Session["Ayuda"] = "NO";

            //Combo ayuda
            Session["sTipoCompraSelect"] = "";
            Session["sTipoEntregaSelect"] = "0";
            Session["sMedioPagoSelect"] = "0";
            Session["comboDepartSelect"] = "0";
            Session["comboProvinciaSelect"] = "0";
            Session["comboDistritoSelect"] = "0";
            Session["comboTiendaSelect"] = "";
            Session["Establecimiento"] = "";

            List<ProductoCarrito> carritoProductos = new List<ProductoCarrito>();

            Session["CarritoProducto"] = carritoProductos;

            //DatosClienteFormulario
            Session["Nombre"] = "";
            Session["Provincia"] = 0;

            //Login
            Session["isLogueado"] = "NO";
        }

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        private string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

    }
}