﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.Net;
using System.Text;
using System.Data;

namespace SantaNaturaNetwork
{
    public partial class ReporteSimplex : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["UserSessionCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }

        public static StringBuilder ExportList<T>(IEnumerable<T> list)
        {
            var stringBuilder = new StringBuilder();
            var headerPropierties = typeof(T).GetProperties();
            for (int i = 0; i <headerPropierties.Length -1; i++)
            {
                stringBuilder.Append(headerPropierties[i].Name + ",");
            }

            var lastProp = headerPropierties[headerPropierties.Length -1].Name;
            stringBuilder.Append(lastProp + Environment.NewLine);

            foreach (var item in list)
            {
                var rowValues = typeof(T).GetProperties();
                for (int i = 0; i < rowValues.Length -1; i++)
                {
                    var prop = rowValues[i];
                    stringBuilder.Append(prop.GetValue(item)+",");
                }
                stringBuilder.Append(rowValues[rowValues.Length-1].GetValue(item)+Environment.NewLine);
            }
            return stringBuilder;

        }


        [WebMethod]
        public static List<ReporteSimplexModelo> ListaDetalleCompraEfectivo(string fecha1Send, string fecha2Send)
        {
            List<ReporteSimplexModelo> listaCompra = null;
            try
            {
                listaCompra = ReporteNegocios.getInstance().ListaReporteSimplex(fecha1Send, fecha2Send);

            }
            catch (Exception ex)
            {
                listaCompra = null;
            }
            return listaCompra;
        }

        protected void btnExportar_Click(object sender, EventArgs e)
        {
            string fecha1Send = fecha.Text;
            string fecha2Send = fechaFin.Text;
            List<ReporteSimplexModelo> listaCompra = null;
            listaCompra = ReporteNegocios.getInstance().ListaReporteSimplex(fecha1Send, fecha2Send);
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DataSimplex.txt");
            Response.Charset = "";
            Response.ContentType = "application/text";
            Response.Output.Write(ExportList<ReporteSimplexModelo>(listaCompra).ToString());
            Response.Flush();
            Response.End();
        }
    }
}