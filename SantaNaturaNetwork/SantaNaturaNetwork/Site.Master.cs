﻿using Modelos;
using Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class SiteMaster : MasterPage
    {

        public string idSimplex;

        protected void Page_Load(object sender, EventArgs e)
        {
            string tipoCliente = Convert.ToString(Session["TipoCliente"]);
            string cerrarSession = Request.QueryString["cerrarSession"];

            if (tipoCliente == "01" || tipoCliente == "01" || tipoCliente == "02" || tipoCliente == "03" || tipoCliente == "05" || tipoCliente == "06" || tipoCliente == "07" || tipoCliente == "08" || tipoCliente == "09" || tipoCliente == "10")
            {   /*Comentado el 06/11/2019*/
                //NavBarEditarPerfil.Attributes.Add("style", "display:block;");
                //NavBarMisCompras.Attributes.Add("style", "display:block;");
                //NavBarRed.Attributes.Add("style", "display:block;");
            }

            if (Convert.ToString(Session["isLogueado"]) == "SI" && cerrarSession != "SI")
            {
                //NavBarLoginAndCerrarSesion.Text = "CERRAR SESSION";
                /*Comentado el 06/11/2019*/
                //NavBarINICIARSESSION.InnerText = "CERRAR SESSION";
            }
            if (cerrarSession == "SI")
            {
                LimpiarSession();
                string ll = Convert.ToString(Session["isLogueado"]);
                Response.Redirect("Index.aspx");
            }

            //CargarIdSimplex();

        }
        protected void NavBarLoginAndCerrarSesion_Click(object sender, EventArgs e)
        {
            if (Convert.ToString(Session["isLogueado"]) == "NO")
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                LimpiarSession();
                string ll = Convert.ToString(Session["isLogueado"]);
                Response.Redirect("Index.aspx");
            }
        }

        private void LimpiarSession()
        {
            //Datos importantes para comprar
            Session["NumDocCliente"] = "";
            Session["NumDocClienteRegistrador"] = "";
            Session["TipoCliente"] = "";
            Session["NombrePatrocinador"] = "";
            Session["PatrocinadorDNI"] = "";
            Session["UplineDNI"] = "";
            Session["IdSimplex"] = "";
            Session["PromoAplicada"] = 0;
            Session["IdCliente"] = "";

            Session["Pais"] = "";
            Session["Departamento"] = "";
            Session["Provincia"] = "";
            Session["Distrito"] = "";
            Session["FechaNac"] = "";

            //
            Session["FechaNacimientoInicial"] = "";
            Session["txtUsuario"] = "";
            Session["txtContra"] = "";
            Session["txtNombre"] = "";
            Session["txtApePat"] = "";
            Session["txtApeMat"] = "";
            Session["cboSexo"] = "";
            Session["cboTipoDoc"] = "";
            Session["txtDocumento"] = "";
            Session["txtCorreo"] = "";
            Session["txtTelefono"] = "";
            Session["txtCelular"] = "";
            Session["cboPais"] = "";
            Session["cboDepartamento"] = "";
            Session["cboProvincia"] = "";
            Session["cboDistrito"] = "";
            Session["txtReferencia"] = "";
            Session["txtDireccion"] = "";
            Session["txtCuentaTransa"] = "";
            Session["txtRuc"] = "";
            Session["txtBanco"] = "";
            Session["txtDeposito"] = "";
            Session["txtInterbancaria"] = "";
            Session["cboTipoCliente"] = "";
            Session["cboUpline"] = "";
            Session["cboEstablecimiento"] = "";
            Session["cboTipoCompra"] = "";
            Session["EliminaProductoTienda"] = 0;

            //

            //
            Session["SubTotalPuntos"] = 0.00;
            Session["SubTotal"] = 0.00;
            Session["MontoAPagar"] = 0.00;
            Session["TipoPago"] = "";
            Session["Foto"] = "";
            Session["IdProducto"] = "";
            Session["Producto"] = "";
            Session["PrecioUnitario"] = 0.00;
            Session["Cantidad"] = 0;
            Session["VamosCompra"] = 0;
            Session["EliminarPromo"] = 0;
            Session["TiendaRetorna"] = 0;
            Session["ActualizaPag"] = 0;
            Session["codProducto"] = "";
            Session["codProducto2"] = "";
            Session["codProducto3"] = "";
            Session["codProducto4"] = "";
            Session["codProdBasico"] = "";
            Session["codProdProfe"] = "";
            Session["codProdEmpre"] = "";
            Session["codProdMillo"] = "";
            Session["codProdBasico2"] = "";
            Session["codProdProfe2"] = "";
            Session["codProdEmpre2"] = "";
            Session["codProdMillo2"] = "";
            Session["tipoCompPromo"] = "";
            Session["TipoAfiliacion"] = "";
            Session["cantRegalo"] = 0;
            Session["cboActivar"] = "0";
            Session["RecargaPagina"] = 0;
            Session["descuentoCI"] = 0;
            Session["montoMomentaneo"] = 0.0;
            Session["ok"] = false;
            Session["ok2"] = false;
            Session["Transaction_Date"] = "";
            Session["Merchant"] = "";
            Session["Id_Unico"] = "";
            Session["Transaction_ID"] = "";
            Session["CardS"] = "";
            Session["Aauthorization_Code"] = "";
            Session["AMOUNT"] = "";
            Session["CURRENCY"] = "";
            Session["BRAND"] = "";
            Session["STATUSS"] = "";
            Session["ACTION_DESCRIPTION"] = "";
            Session["Nombre_Completo"] = "";
            Session["DireccionSocio"] = "";
            Session["PaginaAnterior"] = "";
            Session["ModalSesion"] = 0;

            Session["Ayuda"] = "NO";

            //Combo ayuda
            Session["sTipoCompraSelect"] = "";
            Session["sTipoEntregaSelect"] = "0";
            Session["sMedioPagoSelect"] = "0";
            Session["comboDepartSelect"] = "0";
            Session["comboProvinciaSelect"] = "0";
            Session["comboDistritoSelect"] = "0";
            Session["comboTiendaSelect"] = "";
            Session["Establecimiento"] = "";

            List<ProductoCarrito> carritoProductos = new List<ProductoCarrito>();

            Session["CarritoProducto"] = carritoProductos;

            //DatosClienteFormulario
            Session["Nombre"] = "";
            Session["Provincia"] = 0;

            //Login
            Session["isLogueado"] = "NO";
        }

        //void CargarIdSimplex()
        //{
        //    numeroDoc = Convert.ToString(Session["NumDocCliente"]);
        //}

    }
}