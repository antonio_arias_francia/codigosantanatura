﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class FechaLimiteCompras : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }

                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static bool RegistrarLimite(string limiteSend)
        {
            LimiteDias objLimite = new LimiteDias()
            {
                numeroDias = Convert.ToInt32(limiteSend)
            };

            bool ok = FechasLimiteLN.getInstance().RegistroLimite(objLimite);

            return true;
        }

        [WebMethod]
        public static bool ActualizarLimite(string idLimite, string limiteSend)
        {
            LimiteDias objLimite = new LimiteDias()
            {
                idLimite = Convert.ToInt32(idLimite),
                numeroDias = Convert.ToInt32(limiteSend)
            };
            bool ok = FechasLimiteLN.getInstance().ActualizarLimite(objLimite);
            return true;
        }

        [WebMethod]
        public static List<LimiteDias> ListaLimite()
        {
            List<LimiteDias> Lista = FechasLimiteLN.getInstance().ListaGeneralHorasLimites();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }
    }
}