﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class Estadisticas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IdCliente"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "" | Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
            {
                Response.Redirect("Index.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("Principal.aspx");
            }
            Session["mostrarCompraTerminada"] = 0;

            //Validamos Caducidad de Clave
            string idCliente = Convert.ToString(Session["IdCliente"]);
            string result = "";

            result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


            if (result == "Clave Vencida")
            {
                Session["clave_vencida"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }
            //Validamos si ya agregó sus preguntas de Seguridad
            result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
            if (result == "No Existe")
            {
                Session["preguntas_seguridad"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }
        }

        [WebMethod]
        public static List<ElementosTreeGrid> ListaEstadisticaSocio()
        {
            List<ElementosTreeGrid> listaSocio = null;
            string dni = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                listaSocio = ElementosTreeGridLN.getInstance().ListaTreeGridSocio(dni.Trim());

            }
            catch (Exception ex)
            {
                listaSocio = null;
            }
            return listaSocio;
        }


        [WebMethod]
        public static List<ElementosTreeGrid> ListaEstadisticaConsultor()
        {
            List<ElementosTreeGrid> listaConsultor = null;
            string dni = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                listaConsultor = ElementosTreeGridLN.getInstance().ListaTreeGridConsultor(dni.Trim());

            }
            catch (Exception ex)
            {
                listaConsultor = null;
            }
            return listaConsultor;
        }


        [WebMethod]
        public static List<ElementosTreeGrid> ListaEstadisticaCCI()
        {
            List<ElementosTreeGrid> listaCCI = null;
            string dni = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                listaCCI = ElementosTreeGridLN.getInstance().ListaTreeGridCCI(dni.Trim());

            }
            catch (Exception ex)
            {
                listaCCI = null;
            }
            return listaCCI;
        }

    }
}