﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.IO;
using System.Data;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace SantaNaturaNetwork
{
    public partial class Publicaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["UserSessionCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
            }
        }
        
        [WebMethod]
        public static bool RegistrarPublicacion(string tituloPu, string mensajePu, string enlacePu, string imagenPu,
                                                string estadoPu, string fechaPu)
        {
           Publicacion objPublicacion = new Publicacion()
            { 
                titulo = tituloPu,
                mensaje = mensajePu,
                enlace = enlacePu,
                imagen = imagenPu,
                estado = Convert.ToBoolean(estadoPu),
                fecha = fechaPu
            };

            bool ok = PublicacionesLN.getInstance().RegistroPublicacion(objPublicacion);

            return true;
        }

        [WebMethod]
        public static bool ActualizarPublicacion(string idPubli,string tituloPu, string mensajePu, string enlacePu, string imagenPu,
                                                string estadoPu, string fechaPu)
        {
            Publicacion objPublicacion = new Publicacion()
            {
                idPublicacion = idPubli,
                titulo = tituloPu,
                mensaje = mensajePu,
                enlace = enlacePu,
                imagen = imagenPu,
                estado = Convert.ToBoolean(estadoPu),
                fecha = fechaPu
            };
            bool ok = PublicacionesLN.getInstance().ActualizarPublicacion(objPublicacion);
            return true;
        }

        [WebMethod]
        public static bool EliminarFilaPublicacion(string idPubli)
        {

            bool ok = PublicacionesLN.getInstance().EliminarPublicacion(idPubli);

            return true;
        }

        [WebMethod]
        public static List<Publicacion> ListaPublicaciones()
        {
            List<Publicacion> Lista = PublicacionesLN.getInstance().ListaPublicacion();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool EliminarImagen(string imagen)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/publis/" + imagen);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        protected void prueba_Click(object sender, EventArgs e)
        {
        }

    }
}