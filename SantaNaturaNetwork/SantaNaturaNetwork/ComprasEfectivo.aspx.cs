﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Text;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Net;
using Newtonsoft.Json;
using System.Net.Mail;
using System.Web.Script.Serialization;
using System.Collections;

namespace SantaNaturaNetwork
{
    public partial class ComprasEfectivo : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }

                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "02357051" && Convert.ToString(Session["NumDocCliente"]) != "03438813" &&
                    Convert.ToString(Session["NumDocCliente"]) != "04603721" && Convert.ToString(Session["NumDocCliente"]) != "47637470")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        public string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

        [WebMethod]
        public static List<Compra> ListarCompraEfectivo()
        {
            List<Compra> Lista = null;
            try
            {
                Lista = CompraNegocios.getInstance().ListaComprasEfectivo();

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<Compra> ListarCompraEfectivoFiltrado(string fecha1, string fecha2)
        {
            List<Compra> Lista = null;
            try
            {
                Lista = CompraNegocios.getInstance().ListaComprasEfectivoFiltrado(fecha1, fecha2);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<DetalleCompra> ListaDetalleCompraEfectivo(string id)
        {
            List<DetalleCompra> listaDetalleCompra = null;
            try
            {
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(id);

            }
            catch (Exception ex)
            {
                listaDetalleCompra = null;
            }
            return listaDetalleCompra;
        }

        [WebMethod]
        public static string EnviarPeruShop(string id, string dniSend, string nombreSend, string estabSend, string idclienteSend,
                                          string tipocomSend, string tcomSend, string fechaSimpl, string notaDely, string comprobanteS,
                                          string rucS, string direccionS, string tipoPagoS)
        {
            List<DetalleCompra> listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(id);
            string Paquete = ClienteLN.getInstance().ObtenerPaqueteSocio(dniSend);
            string dniPuntosYComi = dniSend.Trim();
            string NotaPS = Paquete.Substring(12, Paquete.Length - 12) + " - " +tcomSend+" - "+ tipoPagoS.Trim()+" - "+fechaSimpl.Substring(0,10);
            List<List<string>> listProductos = new List<List<string>>();
            string idop = "", documentoF = "";
            dniSend = dniSend.Substring(0, 8);
            int numPromo = 150;
            try
            {
                foreach (var item in listaDetalleCompra)//22 y 56
                {
                    int cantC = 0;
                    string regalo = "0";
                    double sendMoney = 0.0000;
                    double precioSend = 0.0000;
                    if (item.IdProductoPeruShop == "110038")
                    {

                        precioSend = 6.00;
                        List<string> productos = new List<string>();
                        productos.Add("2069");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("1");
                        productos.Add("0");
                        productos.Add("2069");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        cantC = item.CantiPS * 2;
                        double precioSend2 = 7.00;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108683");
                        productos2.Add(Convert.ToString(cantC));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("2");
                        productos2.Add("0");
                        productos2.Add("108683");
                        productos2.Add(regalo);
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0001")
                    {
                        cantC = item.CantiPS * 12;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110107");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("3");
                        productos.Add("0");
                        productos.Add("110107");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0002")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109949");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("4");
                        productos.Add("0");
                        productos.Add("109949");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0003")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109948");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("5");
                        productos.Add("0");
                        productos.Add("109948");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0004")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("6");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0005")
                    {
                        cantC = 3;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("7");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0006")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("8");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0007")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("9");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0008")
                    {
                        cantC = 10;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("10");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0009")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("11");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0010")
                    {
                        cantC = item.CantiPS * 10;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("12");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0011")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("13");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0012")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("14");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0013")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("15");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0014")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("16");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0015")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("17");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0016")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("18");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0017")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(1));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("19");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(2));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("20");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0018")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(2));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("21");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(4));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("22");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0019")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(4));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("23");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(8));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("24");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0020")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("47");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("25");
                        productos.Add("0");
                        productos.Add("47");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0021")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("255");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("26");
                        productos.Add("0");
                        productos.Add("255");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        double precioSend2 = item.PrecioPS * item.CantiPS;
                        double sendMoney2 = precioSend2 / cantC2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("256");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("27");
                        productos2.Add("0");
                        productos2.Add("256");
                        productos2.Add("0");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0022")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("28");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0023")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110242");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("29");
                        productos.Add("0");
                        productos.Add("110242");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("6063");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("30");
                        productos2.Add("0");
                        productos2.Add("6063");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0024")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("30");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0025")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110324");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("31");
                        productos.Add("0");
                        productos.Add("110324");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0026")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("32");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110342");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("33");
                        productos2.Add("0");
                        productos2.Add("110342");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0027")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / (item.CantiPS * 19);
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("35");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("328");
                        productos2.Add(Convert.ToString(cantC2));
                        productos2.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos2.Add("36");
                        productos2.Add("0");
                        productos2.Add("328");
                        productos2.Add("0");
                        listProductos.Add(productos2);

                        int cantC3 = item.CantiPS * 2;
                        List<string> productos3 = new List<string>();
                        productos3.Add("1750");
                        productos3.Add(Convert.ToString(cantC3));
                        productos3.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos3.Add("37");
                        productos3.Add("0");
                        productos3.Add("1750");
                        productos3.Add("0");
                        listProductos.Add(productos3);

                        int cantC4 = item.CantiPS * 3;
                        List<string> productos4 = new List<string>();
                        productos4.Add("47");
                        productos4.Add(Convert.ToString(cantC4));
                        productos4.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos4.Add("38");
                        productos4.Add("0");
                        productos4.Add("47");
                        productos4.Add("0");
                        listProductos.Add(productos4);

                        int cantC5 = item.CantiPS * 1;
                        List<string> productos5 = new List<string>();
                        productos5.Add("35");
                        productos5.Add(Convert.ToString(cantC5));
                        productos5.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos5.Add("39");
                        productos5.Add("0");
                        productos5.Add("35");
                        productos5.Add("0");
                        listProductos.Add(productos5);

                        int cantC6 = item.CantiPS * 2;
                        List<string> productos6 = new List<string>();
                        productos6.Add("107");
                        productos6.Add(Convert.ToString(cantC6));
                        productos6.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos6.Add("40");
                        productos6.Add("0");
                        productos6.Add("107");
                        productos6.Add("0");
                        listProductos.Add(productos6);

                        int cantC7 = item.CantiPS * 1;
                        List<string> productos7 = new List<string>();
                        productos7.Add("69");
                        productos7.Add(Convert.ToString(cantC7));
                        productos7.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos7.Add("41");
                        productos7.Add("0");
                        productos7.Add("69");
                        productos7.Add("0");
                        listProductos.Add(productos7);

                        int cantC8 = item.CantiPS * 3;
                        List<string> productos8 = new List<string>();
                        productos8.Add("269");
                        productos8.Add(Convert.ToString(cantC8));
                        productos8.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos8.Add("42");
                        productos8.Add("0");
                        productos8.Add("269");
                        productos8.Add("0");
                        listProductos.Add(productos8);

                        int cantC9 = item.CantiPS * 2;
                        List<string> productos9 = new List<string>();
                        productos9.Add("6");
                        productos9.Add(Convert.ToString(cantC9));
                        productos9.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos9.Add("43");
                        productos9.Add("0");
                        productos9.Add("6");
                        productos9.Add("0");
                        listProductos.Add(productos9);

                        int cantC10 = item.CantiPS * 1;
                        List<string> productos10 = new List<string>();
                        productos10.Add("81");
                        productos10.Add(Convert.ToString(cantC10));
                        productos10.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos10.Add("44");
                        productos10.Add("0");
                        productos10.Add("81");
                        productos10.Add("0");
                        listProductos.Add(productos10);
                    }
                    else if (item.IdProductoPeruShop == "0028")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / (item.CantiPS * 15);
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("45");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108954");
                        productos2.Add(Convert.ToString(cantC2));
                        productos2.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos2.Add("46");
                        productos2.Add("0");
                        productos2.Add("108954");
                        productos2.Add("0");
                        listProductos.Add(productos2);

                        int cantC3 = item.CantiPS * 2;
                        List<string> productos3 = new List<string>();
                        productos3.Add("110242");
                        productos3.Add(Convert.ToString(cantC3));
                        productos3.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos3.Add("47");
                        productos3.Add("0");
                        productos3.Add("110242");
                        productos3.Add("0");
                        listProductos.Add(productos3);

                        int cantC4 = item.CantiPS * 2;
                        List<string> productos4 = new List<string>();
                        productos4.Add("47");
                        productos4.Add(Convert.ToString(cantC4));
                        productos4.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos4.Add("48");
                        productos4.Add("0");
                        productos4.Add("47");
                        productos4.Add("0");
                        listProductos.Add(productos4);

                        int cantC5 = item.CantiPS * 2;
                        List<string> productos5 = new List<string>();
                        productos5.Add("311");
                        productos5.Add(Convert.ToString(cantC5));
                        productos5.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos5.Add("49");
                        productos5.Add("0");
                        productos5.Add("311");
                        productos5.Add("0");
                        listProductos.Add(productos5);

                        int cantC6 = item.CantiPS * 3;
                        List<string> productos6 = new List<string>();
                        productos6.Add("21");
                        productos6.Add(Convert.ToString(cantC6));
                        productos6.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos6.Add("50");
                        productos6.Add("0");
                        productos6.Add("21");
                        productos6.Add("0");
                        listProductos.Add(productos6);

                        int cantC7 = item.CantiPS * 2;
                        List<string> productos7 = new List<string>();
                        productos7.Add("35");
                        productos7.Add(Convert.ToString(cantC7));
                        productos7.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos7.Add("51");
                        productos7.Add("0");
                        productos7.Add("35");
                        productos7.Add("0");
                        listProductos.Add(productos7);
                    }
                    else if (item.IdProductoPeruShop == "0029")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("108954");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("52");
                        productos.Add("0");
                        productos.Add("108954");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("311");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("53");
                        productos2.Add("0");
                        productos2.Add("311");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0030")
                    {
                        cantC = 3;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("53");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0031")
                    {
                        cantC = 5;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("54");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0032")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110239");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("55");
                        productos.Add("0");
                        productos.Add("110239");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110395");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("56");
                        productos2.Add("0");
                        productos2.Add("110395");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0033")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("57");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0034")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("58");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0035")
                    {
                        cantC = 6;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("59");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0036")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110243");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("60");
                        productos.Add("0");
                        productos.Add("110243");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0037")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110100");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("61");
                        productos.Add("0");
                        productos.Add("110100");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0038")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("24");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("62");
                        productos.Add("0");
                        productos.Add("24");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0039")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110241");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("63");
                        productos.Add("0");
                        productos.Add("110241");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110415");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("64");
                        productos2.Add("0");
                        productos2.Add("110415");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0040")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("65");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0041")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("66");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0042")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("269");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("67");
                        productos.Add("0");
                        productos.Add("269");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0043")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110240");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("68");
                        productos.Add("0");
                        productos.Add("110240");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110191");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("69");
                        productos2.Add("0");
                        productos2.Add("110191");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0044")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("70");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0045")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("71");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0046")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("72");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0047")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("73");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else
                    {
                        string promo = "";
                        if (item.LineaSend == "05" | item.LineaSend == "02" | item.LineaSend == "01") { numPromo += 1; promo = Convert.ToString(numPromo); }
                        if (item.PrecioPS <= 0.06) { regalo = "1"; precioSend = 0.05; }
                        else { precioSend = item.PrecioPS; }
                        double cantiSend = item.CantiPS;
                        List<string> productos = new List<string>();
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N3").Replace(",", "."));
                        productos.Add(promo);
                        productos.Add("0");
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }

                }

                string serializeProds = JsonConvert.SerializeObject(listProductos);
                string prod = serializeProds;
                documentoF = (comprobanteS == "Boleta") ? dniSend : rucS;
                string idruc = ObtenerIdRUC(documentoF, comprobanteS, nombreSend, direccionS);
                string idpuntov = ObtenerIdPuntoVenta(estabSend, comprobanteS);

                if (idruc != "")
                {
                    string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                    WebClient wc = new WebClient();
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("a", "pCatalogo");
                    nvc.Add("canal", "RED");
                    nvc.Add("tipo", "2");
                    nvc.Add("fpago", tipoPagoS.Trim());
                    nvc.Add("pagado", "1");
                    nvc.Add("ruc", documentoF);
                    nvc.Add("local", estabSend);
                    nvc.Add("localorg", estabSend);
                    nvc.Add("razon", nombreSend);
                    nvc.Add("dir_ruc", "");
                    nvc.Add("log", "CREAVIR");
                    nvc.Add("not", NotaPS);
                    nvc.Add("delivery", "0");
                    nvc.Add("dir", "");
                    nvc.Add("ubi", "");
                    nvc.Add("ref", "");
                    nvc.Add("moneda", "PEN");
                    nvc.Add("prod", prod);
                    nvc.Add("comprobante", "1");
                    nvc.Add("idpuntov", idpuntov);
                    nvc.Add("idruc", idruc);
                    nvc.Add("fpagodet", "");

                    var data = wc.UploadValues(url, "POST", nvc);

                    var responseString = UnicodeEncoding.UTF8.GetString(data);

                    idop = responseString;
                    Compra compraPC = new Compra();
                    bool ok = CompraNegocios.getInstance().ActualizarIdopYEstadoCompra(id, idop);
                    Cliente ListaCliente = ClienteLN.getInstance().DatosClienteSimplex(idclienteSend);
                    compraPC = CompraNegocios.getInstance().DatosPuntosYMontosCompra(id);
                    string dniPatro = ListaCliente.patrocinador;
                    string dniFactorComision = ListaCliente.factorComision;
                    string idPaquete = ListaCliente.Packete;
                    string tipocliente = ListaCliente.tipoCliente;
                    int FiltroPreRegistro = ClienteLN.getInstance().ObtenerFiltroPreregistro(idclienteSend);
                    if (FiltroPreRegistro == 1) { ClienteLN.getInstance().ActualizarEstadoPreregistro(idclienteSend); ClienteLN.getInstance().ActualizarDescuento(idclienteSend, tipocomSend); }
                    actualizarPuntos(dniPuntosYComi, dniFactorComision.Trim(), idPaquete, compraPC, dniPatro, Convert.ToDateTime(fechaSimpl), idclienteSend);
                    actualizarComisiones(dniPuntosYComi, dniFactorComision.Trim(), idPaquete, compraPC, dniPatro, Convert.ToDateTime(fechaSimpl), tipocomSend, tipocliente);

                    if (tipocomSend == "08")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "02");
                    }
                    else if (tipocomSend == "09")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "03");
                    }
                    else if (tipocomSend == "10")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "04");
                    }
                    else if (tipocomSend == "11")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "03");
                    }
                    else if (tipocomSend == "12")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "04");
                    }
                    else if (tipocomSend == "13")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "04");
                    }
                    else if (tipocomSend == "13")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "04");
                    }
                    else if (tipocomSend == "24")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "23");
                    }
                    else if (tipocomSend == "25")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "23");
                    }
                    else if (tipocomSend == "26")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "23");
                    }
                    else if (tipocomSend == "27")
                    {
                        ok = ClienteLN.getInstance().ActualizarDescuento(idclienteSend, "23");
                    }

                    string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                    bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                                   "Enviar Compra PS Efectivo", "Ticket: " + id + " Idop: " + idop + " Documento: " + dniPuntosYComi);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return idop;
        }

        private static void actualizarPuntos(string dni, string dniFactorComision, string packetesocio, Compra compraPC, string dniPatrocinador, DateTime fechaSimpl, string idCliente)
        {
            double PP = compraPC.PuntosTotal;
            double VIP = compraPC.PuntosTotal;
            double VP = compraPC.PuntosTotalPromo;
            double VR = compraPC.PuntosTotalPromo;
            double VG = compraPC.PuntosTotalPromo;
            string DOC = dni;
            int i = 0;
            int i2 = 0;
            int IDPeriodo = 0;
            int IDPeriodoComision = getPeriodoRango_Comision(fechaSimpl);
            int IdPeriodoRango_Comi = PeriodoLN.getInstance().ObtenerIdperiodoRango_Comi(IDPeriodoComision);
            List<string> LFactorComision = new List<string>();
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarIdPeriodoTop2();
            bool ok = false;

            while (!ok && i2 < ListaPeriodos.Count())
            {
                DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (fechaSimpl >= primerDiafechaActual & fechaSimpl <= ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodo;
                }
                i2++;
            }

            while (DOC != "")
            {
                DOC = ClienteLN.getInstance().ListarFactorComision(DOC);
                if (DOC != "") { LFactorComision.Add(DOC); }
            }

            bool todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(dni, 0, VP, 0, VG, 0, IDPeriodo);
            todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(dni, PP, 0, 0, 0, 0, IdPeriodoRango_Comi);
            bool PVIP = CompraNegocios.getInstance().ActualizarPuntosVIP(dni, VIP, IdPeriodoRango_Comi);
            CompraNegocios.getInstance().ActualizarCorazonesRed(IdPeriodoRango_Comi, idCliente, compraPC.Corazones);

            if ((packetesocio == "05" | packetesocio == "06") & dniFactorComision != "")
            {
                bool VPP = CompraNegocios.getInstance().ActualizarPuntosComisiones(dniPatrocinador, 0, VP, 0, 0, 0, IDPeriodo);
                bool VIPP = CompraNegocios.getInstance().ActualizarPuntosVIP(dniPatrocinador, VIP, IdPeriodoRango_Comi);
            }


            foreach (var item in LFactorComision)
            {
                if ((packetesocio == "05" | packetesocio == "06") & i == 0)
                { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, 0, VG, 0, IDPeriodo); }
                else { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, VR, VG, 0, IDPeriodo); }
                i++;
            }

            if (packetesocio != "05" & packetesocio != "06")
            {
                bool VPR = CompraNegocios.getInstance().ActualizarVPR(dniFactorComision, IDPeriodo);
            }
            PuntosNegocios.getInstance().RegistrarDatosRangoIndividual(dni, IDPeriodo);
        }

        private static void actualizarComisiones(string dni, string dniFactorComision, string packetesocio, Compra compraPC, string dniPatrocinador, DateTime fechaSimple, string idTPC, string tipocliente)
        {
            double MCCI = compraPC.montoComisionCI;
            double MCCA = compraPC.montoComision;
            double MCCU = compraPC.montoComision;
            double MCCON = compraPC.montoComision * 0.05;
            int IDPeriodo = getPeriodoRango_Comision(fechaSimple);
            List<string> LFactorComision = new List<string>();
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            string fechaRegistro = ClienteLN.getInstance().ObtenerFechaRegistro(dniPatrocinador);
            double montoTiburon = CompraNegocios.getInstance().ObtenerMontoTiburon(dniPatrocinador);
            DateTime fechaConvertida = DateTime.ParseExact(fechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime primerDia = new DateTime(fechaConvertida.Year, fechaConvertida.Month, 1);
            DateTime ultimoDia = primerDia.AddMonths(1).AddDays(-1);
            DateTime DiaMenos5 = ultimoDia.AddDays(-5);
            DateTime PrimerDiaSig = ultimoDia.AddDays(1);
            string TCL = tipocliente;
            
            double PuntosPatro = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(dniFactorComision, IDPeriodo);
            int datoTiburon = PuntosNegocios.getInstance().ObtenerEvaluacionTiburon(dniPatrocinador, IDPeriodo);
            /*COMISION POR CONSULTOR*/
            if (packetesocio == "05" & dniFactorComision != "")
            { if (PuntosPatro >= 20) { bool COM = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniFactorComision, 0, 0, MCCON, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo); } }

            /*COMISION POR CLIENTE INTELIGENTE*/
            if (packetesocio == "06" & dniFactorComision != "")
            { if (PuntosPatro >= 20) { bool COM = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniFactorComision, 0, MCCI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo); } }

            /*COMISION POR AFILIACIONES*/
            if (idTPC != "05" & idTPC != "06" & idTPC != "07")
            {
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = dni;
                List<ComAfiliacion> ListaPatrocinadores = new List<ComAfiliacion>();
                while (cc < 5 && !terminado)
                {
                    cc++;
                    ComAfiliacion agregar = ClienteLN.getInstance().ListaDatosPatrocinioAfiliaciones(IDPeriodo, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaPatrocinadores.Add(agregar); doc = agregar.documento.Trim(); }
                }
                foreach (var item in ListaPatrocinadores)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item.documento, IDPeriodo);
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "01" | ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 0)
                    {
                        double COMAFI = MCCA * 0.30;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, COMAFI, 0, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 1)
                    {
                        double COMAFI = MCCA * 0.05;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, COMAFI, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 2)
                    {
                        double COMAFI = MCCA * 0.03;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, COMAFI, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 3)
                    {
                        double COMAFI = MCCA * 0.02;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, 0, COMAFI, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 4)
                    {
                        double COMAFI = MCCA * 0.01;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, 0, 0, COMAFI);
                    }

                    ccc++;
                }
            }

            /*COMISION BONO TIBURON*/
            if (idTPC == "01" | idTPC == "02" | idTPC == "03" | idTPC == "04" | idTPC == "23")
            {
                if (PuntosPatro >= 20)
                {
                    if (datoTiburon < 2)
                    {
                        if (datoTiburon == 0)
                        {
                            if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                            {
                                string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                if (montoTiburon == 360)
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 0);
                                    }
                                }

                            }
                            else
                            {
                                string fecIniPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaInicio.Trim()).FirstOrDefault();
                                string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                if (montoTiburon == 360)
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 0);
                                    }
                                }

                            }
                        }
                        else if (datoTiburon == 1)
                        {
                            if (montoTiburon == 0)
                            {
                                if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                                {
                                    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 1);
                                    }
                                }
                                else
                                {
                                    string fecIniPe = Convert.ToString(PrimerDiaSig.ToShortDateString());
                                    string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 1);
                                    }
                                }
                            }
                            else if (montoTiburon == 360)
                            {
                                if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                                {
                                    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    string fecIniPe = Convert.ToString(PrimerDiaSig.ToShortDateString());
                                    string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            /*COMISION BONO UNILEVEL -- ES UPLINE NO PATROCINADOR*/
            if (idTPC == "05" | idTPC == "06" | idTPC == "07")
            {
                if (TCL == "03") { MCCU = compraPC.montoComision - compraPC.montoComisionCI; }
                if (TCL == "05") { MCCU = compraPC.montoComision - MCCON; }
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = dni; if (TCL == "03" | TCL == "05") { doc = dniPatrocinador; ccc = 1; }
                double COMUNI = 0;
                List<PatrocinadorUnilivel> ListaUpline = new List<PatrocinadorUnilivel>();

                while (cc < 15 && !terminado)
                {
                    cc++;
                    PatrocinadorUnilivel agregar = ClienteLN.getInstance().ListaDatosUplineUnilevel(IDPeriodo, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaUpline.Add(agregar); doc = agregar.documento.Trim(); }
                }

                foreach (var item2 in ListaUpline)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDPeriodo);
                    if (PP >= 20 & ccc == 0 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.05;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 1 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 2 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 3 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 4 & (item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 5 & (item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.04;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 6 & (item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.03;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 7 & (item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.02;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 8 & (item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 9 & (item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 10 & (item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 11 & (item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 12 & (item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 13 & (item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 14 & (item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 15 & (item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 16 & (item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 17 & (item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 18 & (item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    ccc++;
                }
            }

        }

        private static string ObtenerIdRUC(string documento, string comprobante, string nombreDNI, string direccionDNI)
        {
            string idruc = "", Nombre = "", Direccion = "", Mail = "", Tipo = "";
            Tipo = (comprobante == "Boleta") ? "1" : "6";
            idruc = CompraNegocios.getInstance().ConsultaIdRUC(documento);

            if (idruc == "")
            {
                if (comprobante == "Factura")
                {
                    Cliente.DatosFacturaPS DatosEnviar = DatosFacturaWS(documento);
                    if (DatosEnviar != null)
                    {
                        Nombre = DatosEnviar.Nombre;
                        Direccion = DatosEnviar.Direccion;
                    }
                }
                else
                {
                    Nombre = nombreDNI;
                    Direccion = direccionDNI;
                }
                if (Nombre != "")
                {
                    string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                    WebClient wc = new WebClient();
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("a", "addruc");
                    nvc.Add("login", documento);
                    nvc.Add("ruc", documento);
                    nvc.Add("razon", Nombre);
                    nvc.Add("dir_ruc", Direccion);
                    nvc.Add("tipod", Tipo);
                    nvc.Add("mail", Mail);

                    var data = wc.UploadValues(url, "POST", nvc);
                    var responseString = UnicodeEncoding.UTF8.GetString(data);
                    idruc = responseString;
                    bool IdR = CompraNegocios.getInstance().RegistroIdRuc(documento, idruc, DateTime.Now.AddHours(-2).AddMinutes(-32));
                }
            }

            return idruc;
        }

        private static string ObtenerIdPuntoVenta(string cdrVenta, string Comprobante)
        {
            string idpuntoVenta = "", Serie = "", Sigla = "";
            Sigla = (Comprobante == "Factura") ? "F" : "B";
            Serie = Sigla + CdrLN.getInstance().ObtenerSerieCDR(cdrVenta).Trim();
            List<Cliente.CDR.DatoSeriePS> ListaPS = new List<Cliente.CDR.DatoSeriePS>();

            string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("a", "puntosvemta");
            nvc.Add("local", cdrVenta);

            var data = wc.UploadValues(url, "POST", nvc);
            var responseString = UnicodeEncoding.UTF8.GetString(data);
            JavaScriptSerializer js = new JavaScriptSerializer();
            Cliente.CDR.ListaCDRPuntoVenta resultado = js.Deserialize<Cliente.CDR.ListaCDRPuntoVenta>(responseString);
            foreach (object[] item in resultado.puntos)
            {
                Cliente.CDR.DatoSeriePS PS = new Cliente.CDR.DatoSeriePS();
                PS.TipoDoc = item[0].ToString();
                PS.SerieDoc = item[1].ToString();
                PS.IdPuntoVenta = item[2].ToString();
                ListaPS.Add(PS);
            }
            idpuntoVenta = (from c in ListaPS where c.SerieDoc.Trim() == Serie select c.IdPuntoVenta.Trim()).FirstOrDefault();

            return idpuntoVenta;
        }

        private static int getPeriodoRango_Comision(DateTime fechaSimple)
        {
            int i2 = 0;
            bool ok = false;
            int IDPeriodo = 0;
            DateTime primerDiafechaActual = DateTime.Now;
            DateTime ultimoDiafechaActual = DateTime.Now;
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            while (!ok && i2 < ListaPeriodos.Count())
            {
                primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                if (fechaSimple >= primerDiafechaActual & fechaSimple <= ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodoComision;
                }
                i2++;
            }
            return IDPeriodo;
        }

        private static Cliente.DatosFacturaPS DatosFacturaWS(string documento)
        {
            Cliente.DatosFacturaPS Datos = new Cliente.DatosFacturaPS();
        
            WebRequest request = WebRequest.Create("https://servicio.apirest.pe/api/getRuc");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Bearer " + "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgzMmUzYTI3ZDA0YzI2ZjFmNTIwNTJhMjE5ODRmOTI1NDY2ZmM1YjA2MzcyOWNiNDc4NjExM2VlYzRjZDA5NjY2OTRjNzEzY2U5YTNjNmMyIn0.eyJhdWQiOiIxIiwianRpIjoiODMyZTNhMjdkMDRjMjZmMWY1MjA1MmEyMTk4NGY5MjU0NjZmYzViMDYzNzI5Y2I0Nzg2MTEzZWVjNGNkMDk2NjY5NGM3MTNjZTlhM2M2YzIiLCJpYXQiOjE1ODk5Mjc5MTAsIm5iZiI6MTU4OTkyNzkxMCwiZXhwIjoxOTA1NDYwNzEwLCJzdWIiOiIxMDcyIiwic2NvcGVzIjpbIioiXX0.b_D5SKP8vifhZ38XY88NLKoO_0djb42S8KRqG76ZRLoSYI8f_Y4hEbGiyA0lmEk1miTd5I7_qbE7VPYIYpwy4CxaEN2lVJaqyTTnjupALsDiV0i2a5JMZ5kklXs-HdE6m7Ikovax-ZREcRMTKHxcpAIznYvcL-j3lutmlXRLoUVpRdyWFpVLGpWLagxhu7wHIrURW3ssGZPZCcv6hci3Xs9y8iJUKCt1AU1oNRBiwoL25O-4740jrQRzLLX2PP5KnHsFVNxy79y4LeC4zlhpPmpAuiJKi4KnHj4m3A0oBy9mql8rlCXzGGK9pyd8XjiO4gpiqa82xaYSawk6F6M6h8RkVAaEXZKeFuofDIQjKsp0QClw6egRA24TA-j_ZyvzDX5HzTG6y_1-H1zjPhSmVPH-3fsU8YZ52EFQcHJ28m3AaEDR9NOmaOgy-nLEIvAjunFMc67tO8A6f4I6QInnpcexW4S6FHcDT3Dws3v38BBCac31Pkmn4OKNl4ymIX37h-cztmBKC-tUDFI5Xssy26GxRPt-7NRlO-3BSU9HPEGke855TaigjnObSf6opHUxR9NjDzqVecsEAtNsz9cgozX_EqyGhRiSH2YwnYFOp-IU22v3SI1VNu0ijd263pUhVFfH1JM1OtqWVU8-vOLSC68tvFsF0ylS2HT3LO96U2Q");
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var obj = new DatosFactura.rucC(documento);
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
            }
            HttpWebResponse httpResponse;
            var result = "";
            DatosFactura.WS resOK = new DatosFactura.WS();
            try
            {
                httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                resOK = JsonConvert.DeserializeObject<DatosFactura.WS>(result);
                Datos.Nombre = (resOK.result.direccion != "-") ? resOK.result.razonSocial : "";
                Datos.Direccion = resOK.result.direccion + " / " + resOK.result.distrito + " / " + resOK.result.provincia + " / " + resOK.result.departamento;
                Datos.Mail = "";
            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                Datos = null;
            }

            return Datos;
        }
    }
}