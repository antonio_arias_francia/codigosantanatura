﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using SantaNaturaNetwork.Custom;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Net.Mail;

namespace SantaNaturaNetwork
{
    public partial class Login : System.Web.UI.Page
    {
        public List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
        public List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("InicioAdmin.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "01" | Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        protected void LoginUser_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool auth = Membership.ValidateUser(LoginUser.UserName, LoginUser.Password);

            if (auth)
            {
                Cliente objsocio = null;
                objsocio = ClienteLN.getInstance().AccesoSistema(LoginUser.UserName, LoginUser.Password);
                if (objsocio.paisTienda == "08")
                {
                    HttpContext.Current.Session.Clear();
                    HttpContext.Current.Session.RemoveAll();
                    HttpContext.Current.Session.Abandon();
                    FormsAuthentication.SignOut();
                    Response.Write("<script>alert('Acceso Denegado. Debe ingresar a la plataforma correspondiente a su País')</script>");
                }
                else
                {
                    if (objsocio != null)
                    {
                        List<Packete> ListaPackete = PacketeNegocios.getInstance().ListarPackete("todo", "todo");
                        Session["ObjCliente"] = objsocio;
                        Session["ListPacketes"] = ListaPackete;
                        Session["NumDocCliente"] = objsocio.numeroDoc;
                        Session["IdCliente"] = objsocio.idCliente;
                        Session["Establecimiento"] = objsocio.Establecimiento;
                        Session["TipoCliente"] = objsocio.tipoCliente;
                        Session["PacketeSocio"] = objsocio.Packete;
                        Session["Pais"] = objsocio.pais;
                        Session["Departamento"] = objsocio.departamento;
                        Session["Provincia"] = objsocio.provincia;
                        Session["Distrito"] = objsocio.ditrito;
                        Session["FechaNac"] = objsocio.fechaNac;
                        Session["NombrePatrocinador"] = objsocio.nombre + ' ' + objsocio.apellidoPat;
                        Session["PatrocinadorDNI"] = objsocio.patrocinador;
                        Session["UplineDNI"] = objsocio.upline;
                        Session["IdSimplex"] = objsocio.idSimplex;
                        Session["factorComision"] = objsocio.factorComision;
                        Session["Correo"] = objsocio.correo;
                        Session["Nombres"] = objsocio.nombre;
                        Session["Apellidos_Full"] = objsocio.apellidos_completos;
                        Session["Nombre_Completo"] = objsocio.nombre + ' ' + objsocio.apellidos_completos;
                        Session["DireccionSocio"] = objsocio.direccion;
                        Session["Apodo"] = objsocio.apodo;
                        Session["NombrePS"] = objsocio.IdPeruShop.Trim();
                        Session["txtCelular"] = objsocio.celular.Trim();
                        Session["RUCSocio"] = objsocio.ruc.Trim();
                        Session["PreRegistro"] = objsocio.PreRegistro;
                        Session["isLogueado"] = "SI";
                        Session["PatrocinadorPackete"] = (objsocio.PacketePatrocinador == "") ? objsocio.Packete : objsocio.PacketePatrocinador;
                        FormsAuthenticationTicket tkt;
                        string cookiestr;
                        HttpCookie ck;
                        tkt = new FormsAuthenticationTicket(1, LoginUser.UserName, DateTime.Now,
                        DateTime.Now.AddMinutes(30), false, "your custom data");
                        cookiestr = FormsAuthentication.Encrypt(tkt);
                        ck = new HttpCookie(FormsAuthentication.FormsCookieName, cookiestr);
                        ck.Path = FormsAuthentication.FormsCookiePath;
                        Response.Cookies.Add(ck);

                        if (Convert.ToString(Session["TipoCliente"]) == "07")
                        {
                            Response.Redirect("Principal.aspx");
                        }
                        else if (Convert.ToString(Session["TipoCliente"]) == "10")
                        {
                            Response.Redirect("InicioAdmin.aspx");
                        }
                        else
                        {
                            Session["cerrarSession"] = "NO";
                            Response.Redirect("Index.aspx");
                        }
                    }
                    else
                    {
                        Response.Write("<script>alert('USUARIO INCORRECTO.')</script>");
                    }
                }
            }
            else
            {
                Response.Write("<script>alert('USUARIO INCORRECTO.')</script>");
            }
        }
        [WebMethod()]
        public static List<Cliente> ListadoPreguntasUsuario(string usuario) //Charles 13/04/2021 Preguntas de Seguridad
        {
            return ClienteLN.getInstance().ListadoPreguntasUsuario(usuario);
        }
        //[WebMethod]
        //public static string ValidarPreguntas(string usuario, string pregunta,string respuesta)
        //{
        //    string result = "";
        //    result = ClienteLN.getInstance().ValidarPreguntasXUsuario(usuario, pregunta, respuesta);
        //    return result;
        //}

        [WebMethod]
        public static string ActualizarClaveUsuario2(string usuario, string clave)
        {
            string result = "";
            result = ClienteLN.getInstance().ActualizarClaveUsuario2(usuario, clave);
            return result;
        }
        [WebMethod]
        public static string ValidarClaveUsuario(string usuario)
        {
            string result = "";
            result = ClienteLN.getInstance().ValidarClaveUsuario(usuario, "", "Usuario");
            return result;
        }
        [WebMethod]
        public static string ValidarClaveUsuario2(string usuario, string clave, string tipo)
        {
            string result = "";
            result = ClienteLN.getInstance().ValidarClaveUsuario2(usuario, clave, tipo);
            return result;
        }
        [WebMethod]
        public static List<Cliente> ListaDatosClienteUsuario(string usuario)
        {
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarClienteUsuario(usuario);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }
        [WebMethod]
        public static string EnviarCorreo(string usuario)
        {
            string resultado = "";
            string Token = ClienteLN.getInstance().DevolverToken(usuario);
            string correo = ClienteLN.getInstance().ObtenerCorreo_By_Usuario(usuario);

            if (Token == "0")
            {
                resultado = "0";
            }
            else
            {
                MailMessage mail = new MailMessage();
                //mail.CC.Add(new MailAddress(correo));
                //mail.CC.Add(new MailAddress("pedidos.snn@gmail.com"));
                //mail.Bcc.Add(new MailAddress("santanaturavouchers@gmail.com"));
                mail.CC.Add(new MailAddress(correo));
                //mail.Bcc.Add(new MailAddress("chajuraceentel2@gmail.com"));

                //mail.From = new MailAddress("santanaturavoucher@mundosantanatura.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
                mail.From = new MailAddress("recuperarclave@mundosantanatura.com", "Santa Natura - Token de Seguridad", System.Text.Encoding.UTF8);
                mail.Subject = "Envío de Token";
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                //mail.Body = string.Format("Numero de Operacion: {1}{0}Banco:{2}{0}FechaVoucher: {3}{0}Monto: {4}{0}IDOP: {5}{0}TipoPago: {6}{0}DNI Cliente: {7}{0}TipoCompra: {8}{0}Comprobante: {9}{0}RUC: {10}{0}Fecha Compra: {11}{0}",
                //            Environment.NewLine, numOperacion, nomBanco, fechaMove, montoMove, idopPeruShop, sendTipoPago, dniComprador, tipoCompra + " // " + NotaDelivery, nomComprobante, rucFactura, Convert.ToString(fechaSimple3));

                mail.Body = "<h5 class='modal-title font-weight-bold' style='color:#1B1464'>Estimado Cliente, su código de seguridad es: " + Token + "</h5></br></br>" +
                    "<img id='imgLogoParaLogin' width='250' src='https://tienda.mundosantanatura.com/img/LOGO-PARA-LOGIN-NEW.png' alt='Logo' style='padding-top: -40px; padding-bottom: -40px' />";
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;

                // mail.Attachments.Add(new Attachment(imagen.PostedFile.InputStream, imagen.PostedFile.FileName));
                // mail.Attachments.Add(new Attachment(filename_voucher));

                SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
                //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                //smtp.Credentials = new System.Net.NetworkCredential("chajuraceentel@gmail.com", "Peru12345.");
                smtp.Credentials = new System.Net.NetworkCredential("recuperarclave@mundosantanatura.com", "s@nt@2019");

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtp.Port = 587;
                //smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true; //Charles
                try
                {
                    smtp.Send(mail);
                    resultado = "ok";
                }
                catch (SmtpException ex)
                {
                    //estado = false;
                    //merror = ex.Message.ToString();
                }
            }



            return resultado;
        }

        [WebMethod]
        public static string ValidarCodigoCorreo(string usuario,string codigo_correo)
        {
            string resultado = "";
             resultado = ClienteLN.getInstance().ValidarToken(usuario,codigo_correo);  
            return resultado;
        }
    }
}
