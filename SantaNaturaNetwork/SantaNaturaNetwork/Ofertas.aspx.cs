﻿using Modelos;
using Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class Ofertas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) != "10")
            {
                Response.Redirect("Index.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "07")
            {
                Response.Redirect("Principal.aspx");
            }
            if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
            {
                Response.Redirect("Principal.aspx");
            }
        }

        [WebMethod]
        public static List<Promocion> ListaPromo()
        {
            List<Promocion> Lista = PromocionLN.getInstance().ListarPromo();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static int ListaCantPromo()
        {
            int cantidad = PromocionLN.getInstance().ListarCantPromo();

            return cantidad;
        }

        [WebMethod]
        public static string ListaCodigoPromoActiva()
        {
            string codigo = PromocionLN.getInstance().ListarCodigoPromoActiva();

            return codigo;
        }

        [WebMethod]
        public static bool RegistrarPromocion(string nom_promoSend, string rangoSend, string rangoFinSend, string estado_genSend,
                                              string puntos1Send, string producto1Send, string estado1Send, string puntos2Send,
                                              string producto2Send, string estado2Send, string puntos3Send, string producto3Send,
                                              string estado3Send, string puntos4Send, string producto4Send, string estado4Send,
                                              string punt_basicoSend, string prod_basiSend, string esta_basicoSend, string punt_profSend,
                                              string prod_profSend, string esta_profSend, string punt_empSend, string prod_empSend,
                                              string esta_empSend, string punt_millSend, string prod_millSend, string esta_millSend,
                                              string punt_conSend, string prod_conSend, string esta_conSend, string punt_ciSend,
                                              string prod_ciSend, string esta_ciSend, string pxc1Send, string pxc2Send, string pxc3Send,
                                              string pxc4Send, string regalo1Send, string regalo2Send, string regalo3Send, string regalo4Send,
                                              string regBasico1Send, string regProfe1Send, string regEmpre1Send, string regMillo1Send, string regBasico2Send,
                                              string regProfe2Send, string regEmpre2Send, string regMillo2Send, string punt_basico2Send, string punt_prof2Send,
                                              string punt_emp2Send, string punt_mill2Send, string prod_basico2Send, string prod_prof2Send, string prod_emp2Send,
                                              string prod_mill2Send, string esta_basico2Send, string esta_prof2Send, string esta_emp2Send, string esta_mill2Send)
        {
            #region LINQ NOMBREXIDO
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaIdopXNombre();
            producto1Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto1Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            producto2Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            producto3Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto3Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            producto4Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto4Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_basiSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_basiSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_profSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_profSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_empSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_empSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_millSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_millSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_conSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_conSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_ciSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_ciSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_basico2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_basico2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_prof2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_prof2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_emp2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_emp2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_mill2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_mill2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            #endregion

            #region CONDICIONES NULL
            if (producto1Send == null)
            {
                producto1Send = "";
            }
            if (producto2Send == null)
            {
                producto2Send = "";
            }
            if (producto3Send == null)
            {
                producto3Send = "";
            }
            if (producto4Send == null)
            {
                producto4Send = "";
            }
            if (prod_basiSend == null)
            {
                prod_basiSend = "";
            }
            if (prod_profSend == null)
            {
                prod_profSend = "";
            }
            if (prod_empSend == null)
            {
                prod_empSend = "";
            }
            if (prod_millSend == null)
            {
                prod_millSend = "";
            }
            if (prod_conSend == null)
            {
                prod_conSend = "";
            }
            if (prod_ciSend == null)
            {
                prod_ciSend = "";
            }
            if (prod_basico2Send == null)
            {
                prod_basico2Send = "";
            }
            if (prod_prof2Send == null)
            {
                prod_prof2Send = "";
            }
            if (prod_emp2Send == null)
            {
                prod_emp2Send = "";
            }
            if (prod_mill2Send == null)
            {
                prod_mill2Send = "";
            }
            #endregion

            Promocion objPromo = new Promocion()
            {
                nom_promo = nom_promoSend,
                rango = rangoSend,
                rangofin = rangoFinSend,
                estado_gen = Convert.ToBoolean(estado_genSend),
                puntos1 = Convert.ToInt32(puntos1Send),
                producto1 = producto1Send,
                estado1 = Convert.ToBoolean(estado1Send),
                puntos2 = Convert.ToInt32(puntos2Send),
                producto2 = producto2Send,
                estado2 = Convert.ToBoolean(estado2Send),
                puntos3 = Convert.ToInt32(puntos3Send),
                producto3 = producto3Send,
                estado3 = Convert.ToBoolean(estado3Send),
                puntos4 = Convert.ToInt32(puntos4Send),
                producto4 = producto4Send,
                estado4 = Convert.ToBoolean(estado4Send),
                punt_basico = Convert.ToInt32(punt_basicoSend),
                prod_basico = prod_basiSend,
                esta_basico = Convert.ToBoolean(esta_basicoSend),
                punt_prof = Convert.ToInt32(punt_profSend),
                prod_prof = prod_profSend,
                esta_prof = Convert.ToBoolean(esta_profSend),
                punt_emp = Convert.ToInt32(punt_empSend),
                prod_emp = prod_empSend,
                esta_emp = Convert.ToBoolean(esta_empSend),
                punt_mill = Convert.ToInt32(punt_millSend),
                prod_mill = prod_millSend,
                esta_mill = Convert.ToBoolean(esta_millSend),
                punt_con = Convert.ToInt32(punt_conSend),
                prod_con = prod_conSend,
                esta_con = Convert.ToBoolean(esta_conSend),
                punt_ci = Convert.ToInt32(punt_ciSend),
                prod_ci = prod_ciSend,
                esta_ci = Convert.ToBoolean(esta_ciSend),
                pxc1 = Convert.ToBoolean(pxc1Send),
                pxc2 = Convert.ToBoolean(pxc2Send),
                pxc3 = Convert.ToBoolean(pxc3Send),
                pxc4 = Convert.ToBoolean(pxc4Send),
                regalo1 = Convert.ToBoolean(regalo1Send),
                regalo2 = Convert.ToBoolean(regalo2Send),
                regalo3 = Convert.ToBoolean(regalo3Send),
                regalo4 = Convert.ToBoolean(regalo4Send),
                regBasico1 = Convert.ToBoolean(regBasico1Send),
                regProfe1 = Convert.ToBoolean(regProfe1Send),
                regEmpre1 = Convert.ToBoolean(regEmpre1Send),
                regMillo1 = Convert.ToBoolean(regMillo1Send),
                regBasico2 = Convert.ToBoolean(regBasico2Send),
                regProfe2 = Convert.ToBoolean(regProfe2Send),
                regEmpre2 = Convert.ToBoolean(regEmpre2Send),
                regMillo2 = Convert.ToBoolean(regMillo2Send),
                punt_basico2 = Convert.ToInt32(punt_basico2Send),
                punt_prof2 = Convert.ToInt32(punt_prof2Send),
                punt_emp2 = Convert.ToInt32(punt_emp2Send),
                punt_mill2 = Convert.ToInt32(punt_mill2Send),
                prod_basico2 = prod_basico2Send,
                prod_prof2 = prod_prof2Send,
                prod_emp2 = prod_emp2Send,
                prod_mill2 = prod_mill2Send,
                esta_basico2 = Convert.ToBoolean(esta_basico2Send),
                esta_prof2 = Convert.ToBoolean(esta_prof2Send),
                esta_emp2 = Convert.ToBoolean(esta_emp2Send),
                esta_mill2 = Convert.ToBoolean(esta_mill2Send)
            };

            bool ok = PromocionLN.getInstance().RegistroPromocion(objPromo);

            return true;
        }

        [WebMethod]
        public static bool ActualizarPromocion(string id_promoSend, string nom_promoSend, string rangoSend, string rangoFinSend, string estado_genSend,
                                              string puntos1Send, string producto1Send, string estado1Send, string puntos2Send,
                                              string producto2Send, string estado2Send, string puntos3Send, string producto3Send,
                                              string estado3Send, string puntos4Send, string producto4Send, string estado4Send,
                                              string punt_basicoSend, string prod_basiSend, string esta_basicoSend, string punt_profSend,
                                              string prod_profSend, string esta_profSend, string punt_empSend, string prod_empSend,
                                              string esta_empSend, string punt_millSend, string prod_millSend, string esta_millSend,
                                              string punt_conSend, string prod_conSend, string esta_conSend, string punt_ciSend,
                                              string prod_ciSend, string esta_ciSend, string pxc1Send, string pxc2Send, string pxc3Send,
                                              string pxc4Send, string regalo1Send, string regalo2Send, string regalo3Send, string regalo4Send,
                                              string regBasico1Send, string regProfe1Send, string regEmpre1Send, string regMillo1Send, string regBasico2Send,
                                              string regProfe2Send, string regEmpre2Send, string regMillo2Send, string punt_basico2Send, string punt_prof2Send,
                                              string punt_emp2Send, string punt_mill2Send, string prod_basico2Send, string prod_prof2Send, string prod_emp2Send,
                                              string prod_mill2Send, string esta_basico2Send, string esta_prof2Send, string esta_emp2Send, string esta_mill2Send)
        {

            #region LINQ NOMBREXIDO
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaIdopXNombre();
            producto1Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto1Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            producto2Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            producto3Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto3Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            producto4Send = (from c in ListaProducto where c.NombreProducto.Trim() == producto4Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_basiSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_basiSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_profSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_profSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_empSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_empSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_millSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_millSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_conSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_conSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_ciSend = (from c in ListaProducto where c.NombreProducto.Trim() == prod_ciSend.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_basico2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_basico2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_prof2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_prof2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_emp2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_emp2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            prod_mill2Send = (from c in ListaProducto where c.NombreProducto.Trim() == prod_mill2Send.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            #endregion

            #region CONDICIONES NULL
            if (producto1Send == null)
            {
                producto1Send = "";
            }
            if (producto2Send == null)
            {
                producto2Send = "";
            }
            if (producto3Send == null)
            {
                producto3Send = "";
            }
            if (producto4Send == null)
            {
                producto4Send = "";
            }
            if (prod_basiSend == null)
            {
                prod_basiSend = "";
            }
            if (prod_profSend == null)
            {
                prod_profSend = "";
            }
            if (prod_empSend == null)
            {
                prod_empSend = "";
            }
            if (prod_millSend == null)
            {
                prod_millSend = "";
            }
            if (prod_conSend == null)
            {
                prod_conSend = "";
            }
            if (prod_ciSend == null)
            {
                prod_ciSend = "";
            }
            if (prod_basico2Send == null)
            {
                prod_basico2Send = "";
            }
            if (prod_prof2Send == null)
            {
                prod_prof2Send = "";
            }
            if (prod_emp2Send == null)
            {
                prod_emp2Send = "";
            }
            if (prod_mill2Send == null)
            {
                prod_mill2Send = "";
            }
            #endregion

            Promocion objPromo = new Promocion()
            {
                id_promo = id_promoSend,
                nom_promo = nom_promoSend,
                rango = rangoSend,
                rangofin = rangoFinSend,
                estado_gen = Convert.ToBoolean(estado_genSend),
                puntos1 = Convert.ToInt32(puntos1Send),
                producto1 = producto1Send,
                estado1 = Convert.ToBoolean(estado1Send),
                puntos2 = Convert.ToInt32(puntos2Send),
                producto2 = producto2Send,
                estado2 = Convert.ToBoolean(estado2Send),
                puntos3 = Convert.ToInt32(puntos3Send),
                producto3 = producto3Send,
                estado3 = Convert.ToBoolean(estado3Send),
                puntos4 = Convert.ToInt32(puntos4Send),
                producto4 = producto4Send,
                estado4 = Convert.ToBoolean(estado4Send),
                punt_basico = Convert.ToInt32(punt_basicoSend),
                prod_basico = prod_basiSend,
                esta_basico = Convert.ToBoolean(esta_basicoSend),
                punt_prof = Convert.ToInt32(punt_profSend),
                prod_prof = prod_profSend,
                esta_prof = Convert.ToBoolean(esta_profSend),
                punt_emp = Convert.ToInt32(punt_empSend),
                prod_emp = prod_empSend,
                esta_emp = Convert.ToBoolean(esta_empSend),
                punt_mill = Convert.ToInt32(punt_millSend),
                prod_mill = prod_millSend,
                esta_mill = Convert.ToBoolean(esta_millSend),
                punt_con = Convert.ToInt32(punt_conSend),
                prod_con = prod_conSend,
                esta_con = Convert.ToBoolean(esta_conSend),
                punt_ci = Convert.ToInt32(punt_ciSend),
                prod_ci = prod_ciSend,
                esta_ci = Convert.ToBoolean(esta_ciSend),
                pxc1 = Convert.ToBoolean(pxc1Send),
                pxc2 = Convert.ToBoolean(pxc2Send),
                pxc3 = Convert.ToBoolean(pxc3Send),
                pxc4 = Convert.ToBoolean(pxc4Send),
                regalo1 = Convert.ToBoolean(regalo1Send),
                regalo2 = Convert.ToBoolean(regalo2Send),
                regalo3 = Convert.ToBoolean(regalo3Send),
                regalo4 = Convert.ToBoolean(regalo4Send),
                regBasico1 = Convert.ToBoolean(regBasico1Send),
                regProfe1 = Convert.ToBoolean(regProfe1Send),
                regEmpre1 = Convert.ToBoolean(regEmpre1Send),
                regMillo1 = Convert.ToBoolean(regMillo1Send),
                regBasico2 = Convert.ToBoolean(regBasico2Send),
                regProfe2 = Convert.ToBoolean(regProfe2Send),
                regEmpre2 = Convert.ToBoolean(regEmpre2Send),
                regMillo2 = Convert.ToBoolean(regMillo2Send),
                punt_basico2 = Convert.ToInt32(punt_basico2Send),
                punt_prof2 = Convert.ToInt32(punt_prof2Send),
                punt_emp2 = Convert.ToInt32(punt_emp2Send),
                punt_mill2 = Convert.ToInt32(punt_mill2Send),
                prod_basico2 = prod_basico2Send,
                prod_prof2 = prod_prof2Send,
                prod_emp2 = prod_emp2Send,
                prod_mill2 = prod_mill2Send,
                esta_basico2 = Convert.ToBoolean(esta_basico2Send),
                esta_prof2 = Convert.ToBoolean(esta_prof2Send),
                esta_emp2 = Convert.ToBoolean(esta_emp2Send),
                esta_mill2 = Convert.ToBoolean(esta_mill2Send)
            };
            bool ok = PromocionLN.getInstance().ActualizarPromocion(objPromo);
            return true;
        }

    }
}

  