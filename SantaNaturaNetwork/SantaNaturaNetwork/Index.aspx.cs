﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using SantaNaturaNetwork.Custom;

using Modelos;
using Negocios;

namespace SantaNaturaNetwork
{
    public partial class Index : System.Web.UI.Page
    {
        public List<Producto> listaProductos = new List<Producto>();
        public List<Producto> listaProductos2 = new List<Producto>();
        public List<LimiteDias> listaFecha = new List<LimiteDias>();
        public List<ArchivosPDF.ImagenesBanners> ListaBanners = new List<ArchivosPDF.ImagenesBanners>();
        public List<Compra> listaCompraAntigua = new List<Compra>();
        public string PP;
        public string VG;
        public string VIP;
        public string VP;
        public string VQ;
        public string RANGO;
        public string RANGOPROXIMO;
        public string PORCENTAJE;
        public string DIRECTOS;
        public string COMISION;
        public string TOTALSOCIOS;
        public string ACTIVOS_SOCIOS;
        public string AFILIACIONES_RED;
        public string SOCIOS;
        public string IMG;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["detalleCompra"] = 0;
            Session["mostrarCompraTerminada"] = 0;

            if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("InicioAdmin.aspx");
            }


            string idCliSend = Convert.ToString(Session["IdCliente"]);
            string documento = Convert.ToString(Session["NumDocCliente"]);
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                cargarDatos();
                ListaBanners = ArchivosLN.getInstance().ListarImagenesBanners();
                //panel.Attributes.Add("style", "display:block;");
                if (Convert.ToString(Session["TipoCliente"]) == "01" && Convert.ToString(Session["NumDocCliente"]).Trim() != "99999999")
                {
                    bloqueNoLogueado.Attributes.Add("style", "display:none;");
                    bloqueLogueado.Attributes.Add("style", "display:block;");
                    if ( Convert.ToInt32(Session["ModalIndex"]) == 0) {
                        Session["ModalIndex"] = 1;
                        modal.Attributes.Add("style", "display:block;");
                    }

                    //Validamos Caducidad de Clave
                    string idCliente = Convert.ToString(Session["IdCliente"]);
                    string result = "";

                    result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                    if (result == "Clave Vencida")
                    {
                        Session["clave_vencida"] = result;
                        Response.Redirect("EditarPerfil.aspx");
                    }

                    //Validamos si ya agregó sus preguntas de Seguridad
                    result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                    if (result == "No Existe")
                    {
                        Session["preguntas_seguridad"] = result;
                        Response.Redirect("EditarPerfil.aspx");
                    }

                    //  ModalClave.Attributes.Add("style", "display:block;");
                }
                //int cantCompras = CompraNegocios.getInstance().CantidadCompras(idCliSend);
                //int cantDirectos = ClienteLN.getInstance().ConsultaDirectosGeneral(documento.Trim());
                if (Convert.ToInt32(Session["ModalSesion"]) == 0)
                {
                    Session["ModalSesion"] = 1;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }
                //if (cantCompras <= 0 & cantDirectos == 0) { bool eliminar = CompraNegocios.getInstance().EliminarClienteSinCompras(idCliSend); LimpiarSession(); Response.Redirect("Login.aspx"); }
            }
            else
            {
                System.Web.HttpContext.Current.Session["cerrarSession"] = "SI";
                System.Web.HttpContext.Current.Session.Clear();
                System.Web.HttpContext.Current.Session.RemoveAll();
                System.Web.HttpContext.Current.Session.Abandon();
            }
            CargarProductosMasVendidosByPais();


            

                
        }

        void CargarProductosMasVendidosByPais()
        {
            listaProductos = ProductoNegocios.getInstance().ListarProductosMasVendidos("Peru");
        }

        void CargarProductosMenosVendidosByPais()
        {
            //listaProductos2 = ProductoNegocios.getInstance().ListaProductosMenosVendidos("Peru");
        }

        private void EliminarComprasAntiguas()
        {
            string idCliente = Convert.ToString(Session["IdCliente"]);
            listaFecha = FechasLimiteLN.getInstance().ListaLimite(1);
            int diasNumero = listaFecha[0].numeroDias;
            listaCompraAntigua = CompraNegocios.getInstance().ListarComprasAntiguas(idCliente);
            DateTime fechaActual = DateTime.Now.AddHours(-2);
            DateTime FechaCompra;

            foreach (var item in listaCompraAntigua)
            {
                FechaCompra = item.FechaPago.AddHours(diasNumero);
                if (FechaCompra < fechaActual) {
                    RecuperarStock(item.Ticket);
                    bool ok = CompraNegocios.getInstance().EliminarCompraCliente(item.Ticket);
                }
            }

        }

        private void RecuperarStock(string ticket) {
            List<Compra> ListaP = CompraNegocios.getInstance().ListarProductosxTicket(ticket);
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();

            foreach (var item in ListaP) {
                string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == item.Despacho.Trim() select c.DNICDR.Trim()).FirstOrDefault();
                bool recuperar = StockLN.getInstance().IncrementarStock(dniCDR, item.IdProductoPais, item.Cantidad);
            }
        }

        private void LimpiarSession()
        {
            //Datos importantes para comprar
            Session["NombreDelivery"] = "";
            Session["DNIDelivery"] = "";
            Session["CelularDelivery"] = "";
            Session["DireccionDelivery"] = "";
            Session["TransporteDelivery"] = "";
            Session["DirecTransporteDelivery"] = "";
            Session["ProvinciaDelivery"] = "";
            Session["DirecProvinciaDelivery"] = "";
            Session["EvalDelivery"] = 0;
            Session["NumDocCliente"] = "";
            Session["NumDocClienteRegistrador"] = "";
            Session["TipoCliente"] = "";
            Session["NombrePatrocinador"] = "";
            Session["PatrocinadorDNI"] = "";
            Session["UplineDNI"] = "";
            Session["IdSimplex"] = "";
            Session["PromoAplicada"] = 0;
            Session["IdCliente"] = "";
            Session["RUCSocio"] = "";
            Session["NumRUCOP"] = "";
            Session["TiendaAyuda"] = null;

            Session["Pais"] = "";
            Session["Departamento"] = "";
            Session["Provincia"] = "";
            Session["Distrito"] = "";
            Session["FechaNac"] = "";

            //
            Session["FechaNacimientoInicial"] = "";
            Session["txtUsuario"] = "";
            Session["txtContra"] = "";
            Session["txtNombre"] = "";
            Session["txtApePat"] = "";
            Session["txtApeMat"] = "";
            Session["cboSexo"] = "";
            Session["cboTipoDoc"] = "";
            Session["txtDocumento"] = "";
            Session["txtCorreo"] = "";
            Session["txtTelefono"] = "";
            Session["txtCelular"] = "";
            Session["cboPais"] = "";
            Session["cboDepartamento"] = "";
            Session["cboProvincia"] = "";
            Session["cboDistrito"] = "";
            Session["txtReferencia"] = "";
            Session["txtDireccion"] = "";
            Session["txtCuentaTransa"] = "";
            Session["txtRuc"] = "";
            Session["txtBanco"] = "";
            Session["txtDeposito"] = "";
            Session["txtInterbancaria"] = "";
            Session["cboTipoCliente"] = "";
            Session["cboUpline"] = "";
            Session["cboEstablecimiento"] = "";
            Session["cboPremio"] = "";
            Session["cboTipoCompra"] = "";
            Session["ProcesoVisa"] = 0;
            Session["EliminaProductoTienda"] = 0;
            Session["mostrarCompraTerminada"] = 0;
            Session["EliminaProductoxCDR"] = 0;
            Session["PPSOCIO"] = 0;
            Session["ValidarRUC"] = 0;
            Session["ModalIndex"] = 0;

            //

            //
            Session["SubTotalCorazones"] = 0.00;
            Session["SubTotalPuntos"] = 0.00;
            Session["SubTotalPuntosPromocion"] = 0.00;
            Session["SubTotal"] = 0.00;
            Session["MontoAPagar"] = 0.00;
            Session["TipoPago"] = "";
            Session["Foto"] = "";
            Session["IdProducto"] = "";
            Session["Producto"] = "";
            Session["PrecioUnitario"] = 0.00;
            Session["Cantidad"] = 0;
            Session["VamosCompra"] = 0;
            Session["EliminarPromo"] = 0;
            Session["TiendaRetorna"] = 0;
            Session["ActualizaPag"] = 0;
            Session["codProducto"] = "";
            Session["codProducto2"] = "";
            Session["codProducto3"] = "";
            Session["codProducto4"] = "";
            Session["codProdBasico"] = "";
            Session["codProdProfe"] = "";
            Session["codProdEmpre"] = "";
            Session["codProdMillo"] = "";
            Session["codProdBasico2"] = "";
            Session["codProdProfe2"] = "";
            Session["codProdEmpre2"] = "";
            Session["codProdMillo2"] = "";
            Session["tipoCompPromo"] = "";
            Session["TipoAfiliacion"] = "";
            Session["cantRegalo"] = 0;
            Session["cboActivar"] = "0";
            Session["cboComprobante"] = "0";
            Session["cboTitular"] = "0";
            Session["RecargaPagina"] = 0;
            Session["descuentoCI"] = 0;
            Session["montoMomentaneo"] = 0.0;
            Session["ok"] = false;
            Session["ok2"] = false;
            Session["Transaction_Date"] = "";
            Session["Merchant"] = "";
            Session["Id_Unico"] = "";
            Session["Transaction_ID"] = "";
            Session["CardS"] = "";
            Session["Aauthorization_Code"] = "";
            Session["AMOUNT"] = "";
            Session["CURRENCY"] = "";
            Session["BRAND"] = "";
            Session["STATUSS"] = "";
            Session["ACTION_DESCRIPTION"] = "";
            Session["Nombre_Completo"] = "";
            Session["DireccionSocio"] = "";
            Session["PaginaAnterior"] = "";
            Session["ModalSesion"] = 0;

            Session["Ayuda"] = "NO";

            //Combo ayuda
            Session["sTipoCompraSelect"] = "";
            Session["sTipoEntregaSelect"] = "0";
            Session["sMedioPagoSelect"] = "0";
            Session["comboDepartSelect"] = "0";
            Session["comboProvinciaSelect"] = "0";
            Session["comboDistritoSelect"] = "0";
            Session["comboTiendaSelect"] = "";
            Session["Establecimiento"] = "";

            List<ProductoCarrito> carritoProductos = new List<ProductoCarrito>();
            List<ProductoCarrito> carritoProductosP = new List<ProductoCarrito>();

            Session["CarritoProducto"] = carritoProductos;
            Session["CarritoProductoPaquete2"] = carritoProductosP;

            //DatosClienteFormulario
            Session["Nombre"] = "";
            Session["Provincia"] = 0;

            //Login
            Session["isLogueado"] = "NO";
        }

        private void cargarDatos()
        {
            string documento = Convert.ToString(Session["NumDocCliente"]);
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            List<Cliente> DatosPuntos = ClienteLN.getInstance().ListarDatosIndex(documento);
            List<Cliente> RangoProximo = ClienteLN.getInstance().ListarDatosRangoProximo(documento);
            List<PeriodoComision> Lista = PeriodoComisionLN.getInstance().ListaComisionesXPeriodo(documento, IDP);
            List<DatosSocios> ListaRetencion = SocioLN.getInstance().ListarRetencionGenerada(documento, IDP);

            if (documento != "") // Charles 26/03/2021 Se Agregó el if para controlar en caso no este activo el usuario(documento)
            {
                PP = Convert.ToString(DatosPuntos[0].PP);
                VP = Convert.ToString(DatosPuntos[0].VP);
                VQ = Convert.ToString(DatosPuntos[0].VQ);
                VG = Convert.ToString(DatosPuntos[0].VG);
                VIP = Convert.ToString(DatosPuntos[0].VIP);
                RANGO = Convert.ToString(DatosPuntos[0].rango);

                COMISION = Convert.ToString(Lista[0].ComisionLinea);
                DIRECTOS = Convert.ToString(SocioLN.getInstance().CantNuevosSociosPatrocinio(documento));
                TOTALSOCIOS = Convert.ToString(ListaRetencion[0].CantSociosRed);
                ACTIVOS_SOCIOS = Convert.ToString(ListaRetencion[0].CantSociosRedActivos);
                AFILIACIONES_RED = Convert.ToString(ListaRetencion[0].CantNuevosSociosRed);
                RANGOPROXIMO = Convert.ToString(RangoProximo[0].rangoProximo);
                TotalSociosPB.Value = TOTALSOCIOS;
                ActivosSociosPB.Value = ACTIVOS_SOCIOS;
                VQActualPB.Value = VQ.Replace(",", ".");
                VQProximoPB.Value = Convert.ToString(RangoProximo[0].VQProximo);
                decimal PorcentajeCalculado = (Convert.ToDecimal(RangoProximo[0].VQProximo) == 0) ? 0 : (Convert.ToDecimal(VQ) / Convert.ToDecimal(RangoProximo[0].VQProximo)) * 100;
                PORCENTAJE = (PorcentajeCalculado >= 100) ? "99" : Convert.ToString(decimal.Round(PorcentajeCalculado, 0));
            }


        }

        void DefinirImagen() {
            if (RANGO == "BRONCE")
            {
                IMG = "1-Bronce-270x270.png";
            }
            else if (RANGO == "PLATA")
            {
                IMG = "2-Plata-270x270.png";
            }
            else if (RANGO == "ORO")
            {
                IMG = "3-Oro-270x270.png";
            }
            else if (RANGO == "ZAFIRO")
            {
                IMG = "4-Zafiro-270x270.png";
            }
            else if (RANGO == "RUBI")
            {
                IMG = "5-Rubí-270x270.png";
            }
            else if (RANGO == "DIAMANTE")
            {
                IMG = "6-Diamante-270x270.png";
            }
            else if (RANGO == "D. DIAMANTE")
            {
                IMG = "7-DDiamante-270x270.png";
            }
            else if (RANGO == "T. DIAMANTE")
            {
                IMG = "8-TDiamante-270x270.png";
            }
            else if (RANGO == "D. MILLONARIO")
            {
                IMG = "9-DiamanteMillonario-270x270.png";
            }
            else if (RANGO == "D.D. MILLONARIO")
            {
                IMG = "10-DDiamanteMillonario-270x270.png";
            }
            else if (RANGO == "T.D. MILLONARIO")
            {
                IMG = "11-TDiamanteMillonario-270x270.png";
            }
            else if (RANGO == "D. IMPERIAL")
            {
                IMG = "12-DiamanteImperial-270x270.png";
            }
            else {
                IMG = "SN.jpg";
            }
        }

        [WebMethod]
        public static string Rango()
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            string rango = "";
            try
            {
                List<Cliente> DatosPuntos = ClienteLN.getInstance().ListarDatosIndex(documento);
                rango = Convert.ToString(DatosPuntos[0].rango);
            }
            catch (Exception ex)
            {
                rango = "";
            }
            return rango;
        }

        [WebMethod]
        public static bool Logout()
        {
            System.Web.HttpContext.Current.Session["cerrarSession"] = "SI";
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.RemoveAll();
            System.Web.HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
            return true;
        }

    }
}