﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;

namespace SantaNaturaNetwork
{
    public partial class Master : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Cliente objCliente = null;
                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    objCliente = (Cliente)Session["ObjCliente"];
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Admin.Attributes.Add("style", "display:none");
                    SociosRed.Attributes.Add("style", "display:none");
                    GestionarProductos.Attributes.Add("style", "display:none");
                    GestionarCDR.Attributes.Add("style", "display:none");
                    GestionarCDR.Attributes.Add("style", "display:none");
                    YachayWasi.Attributes.Add("style", "display:block");
                    SolicitarStock.Attributes.Add("style", "display:block");
                    DatosPersonalesCDR.Attributes.Add("style", "display:block");
                    ListaPedidosPECDR.Attributes.Add("style", "display:block");
                    //Publicaciones.Attributes.Add("style", "display:none");
                    //Novedades.Attributes.Add("style", "display:none");
                    //ReporteCDRGene.Attributes.Add("style", "display:none");
                    //ProductosVendidos.Attributes.Add("style", "display:none");
                    GestionCompras.Attributes.Add("style", "display:none");
                    GestionRedes.Attributes.Add("style", "display:none");
                    GestionArchivos.Attributes.Add("style", "display:none");
                    txtNom.Text = objCliente.nombre;
                    txtApe.Text = objCliente.apellidoPat;
                    txtTipo.Text = objCliente.apodo.Trim();
                    txtIDPS.Text = objCliente.IdPeruShop.Trim();
                }
                else {
                    Admin.Attributes.Add("style", "display:block");
                    SociosRed.Attributes.Add("style", "display:block");
                    GestionarProductos.Attributes.Add("style", "display:block");
                    GestionarCDR.Attributes.Add("style", "display:block");
                    YachayWasi.Attributes.Add("style", "display:none");
                    IndexCDR.Attributes.Add("style", "display:none");
                    SolicitarStock.Attributes.Add("style", "display:none");
                    DatosPersonalesCDR.Attributes.Add("style", "display:none");
                    ListaPedidosPECDR.Attributes.Add("style", "display:none");
                    //Publicaciones.Attributes.Add("style", "display:block");
                    //Novedades.Attributes.Add("style", "display:block");
                    //ReporteCDRGene.Attributes.Add("style", "display:block");
                    //ProductosVendidos.Attributes.Add("style", "display:block");
                    GestionCompras.Attributes.Add("style", "display:block");
                    GestionRedes.Attributes.Add("style", "display:block");
                    GestionArchivos.Attributes.Add("style", "display:block");
                    logoYW.Attributes.Add("style", "display:none");
                    txtNom.Text = objCliente.nombre;
                    txtApe.Text = objCliente.apellidoPat;
                    txtTipo.Text = "Administrador";
                }
                
            }
        }
    }
}