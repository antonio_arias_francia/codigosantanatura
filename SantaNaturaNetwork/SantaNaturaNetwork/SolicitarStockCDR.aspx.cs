﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using Modelos;
using Negocios;
using System.Web.Script.Serialization;
using System.Net.Mail;
using System.Globalization;

namespace SantaNaturaNetworkV3
{
    public partial class SolicitarStockCDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "07")
                {
                    Response.Redirect("Index.aspx");
                }
                //txtCDR.Text = Convert.ToString(Session["NombrePS"]).Trim();
                lblNameCDR.Text = Convert.ToString(Session["NombrePS"]).Trim();
                List<Cliente.CDR.ComisionLineaCDR> ListaDatos = CdrLN.getInstance().ListaComisionLineaByCDR(Convert.ToString(Session["NumDocCliente"]).Trim());
                List<Cliente.CDR.SustraccionComisionesLC> ListaDatoSust = CdrLN.getInstance().ListaSustraccionComisionLineaByCDR(Convert.ToString(Session["NumDocCliente"]).Trim());
                double MontoComision = ListaDatos[0].Comision - ListaDatoSust[0].Monto_Comision, MontoLC = ListaDatos[0].LineaCredito - ListaDatoSust[0].Monto_LC;
                txtLineaCredito.Text = Convert.ToString(Math.Round(MontoLC,2));
                txtSaldoComision.Text = Convert.ToString(MontoComision);
            }
            IniciarLlenadoCombo();
            ListaProductos();
        }

        private void IniciarLlenadoCombo()
        {
            //DateTime 
            //List<Cliente.CDR.FechaPedidoComisionCDR> FechaComision = CdrLN.getInstance().ListaFechasPedidoComisionCDR();
            //DateTime fechaInicio = DateTime.ParseExact(FechaComision[0].FechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //DateTime fechaFin = DateTime.ParseExact(FechaComision[0].FechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            //string fecha1 = hoy.AddDays(-(diaActual + 2)).ToShortDateString();
            //if (DateTime.Now.AddHours(-2) < fechaInicio || DateTime.Now.AddHours(-2) > fechaFin.AddHours(24))
            //{
            //    cboTipoPago.Items.FindByValue("4").Enabled = false;
            //    //cboTipoPago.Items.FindByValue("7").Enabled = false;
            //    cboTipoPago.Items.FindByValue("8").Enabled = false;
            //}
            
            string ultimaCompra = CdrLN.getInstance().GetUltimaFechaCompraCDR(Session["NumDocCliente"].ToString());
            int condicion = CdrLN.getInstance().CondicionPedidosCDR(Session["NumDocCliente"].ToString());
            if (condicion == 0)
            {
                List<Cliente.CDR.DiasPedidosLineaCreditoCDR> DiasActivos = CdrLN.getInstance().ListaDiasActivosLineaCreditoCDR();
                List<Cliente.CDR.DiasPedidosLineaCreditoCDR> CondicionEmergencia = CdrLN.getInstance().ListaDiasActivosEmergencia();
                List<Cliente.CDR.FechaPedidoComisionCDR> ListaCDR = CdrLN.getInstance().ListaFechasEvaluacionPedidoCDR();

                DateTime hoy = DateTime.Now.AddHours(-2);
                int UC = (ultimaCompra == "") ? 0 : (int)Convert.ToDateTime(ultimaCompra).DayOfWeek;
                int diaActual = (int)hoy.DayOfWeek;
                diaActual = (diaActual == 0) ? 7 : diaActual;
                UC = (UC == 0) ? 7 : UC;

                int cantDias = DiasActivos.Count(dias => Convert.ToString(dias.IDDias).Contains(Convert.ToString(diaActual)));
                int cantDiasE = CondicionEmergencia.Count(dias => Convert.ToString(dias.IDDias).Contains(UC.ToString()));

                List<Cliente.CDR.ComboTipoCompraCDR> ListaTipo = CdrLN.getInstance().ListaTipoCompraCDR();
                cboTipoPago.DataSource = ListaTipo;
                cboTipoPago.DataTextField = "Descripcion";
                cboTipoPago.DataValueField = "IDTipoCompra";
                cboTipoPago.DataBind();
                cboTipoPago.Items.Insert(0, new ListItem("SELECCIONE", "0"));
                if (cantDias <= 0)
                {
                    cboTipoPago.Items.FindByValue("1").Enabled = false;
                    cboTipoPago.Items.FindByValue("3").Enabled = false;
                }
                if (cantDiasE <= 0 || Convert.ToDateTime(ultimaCompra) < Convert.ToDateTime(ultimaCompra).AddDays(-7) || ultimaCompra == "")
                {
                    cboTipoPago.Items.FindByValue("2").Enabled = false;
                }
            }
            else
            {
                List<Cliente.CDR.ComboTipoCompraCDR> ListaTipo = CdrLN.getInstance().ListaTipoCompraCDR();
                cboTipoPago.DataSource = ListaTipo;
                cboTipoPago.DataTextField = "Descripcion";
                cboTipoPago.DataValueField = "IDTipoCompra";
                cboTipoPago.DataBind();
                cboTipoPago.Items.Insert(0, new ListItem("SELECCIONE", "0"));
                cboTipoPago.Items.FindByValue("1").Enabled = false;
                cboTipoPago.Items.FindByValue("2").Enabled = false;
                cboTipoPago.Items.FindByValue("3").Enabled = false;
            }
        }

        private void ListaProductos()
        {
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaProductoCDR();
            cboProductoCDR.DataSource = ListaProducto;
            cboProductoCDR.DataTextField = "NombreProducto";
            cboProductoCDR.DataValueField = "IdProducto";
            cboProductoCDR.DataBind();
        }

        [WebMethod]
        public static List<ProductoV2> ListarDatosProductosCDR(string IDPRODUCTO)
        {
            List<ProductoV2> Lista = null;
            try
            {
                Lista = ProductoLN.getInstance().ListaDatosProductosCDR(IDPRODUCTO);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<StockCDR> ListarSolicitudesGeneradas()
        {
            List<StockCDR> Lista = null;
            try
            {
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                Lista = StockLN.getInstance().ListarSolicitudesGeneradas(documento.Trim());
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<StockCDR.CodigoRespuesta> RegistrarStock(List<StockProductosRegistro> prueba, string TipoPagoS, double montoPagoS,
                                            double montoLCS, double montoComisionS)
        {
            if (Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]) == "07")
            {
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]), url = "";
                string dniCDR = (TipoPagoS == "1" || TipoPagoS == "3" || documento == "00003") ? "99966698" : (TipoPagoS == "9") ? documento : "00003";
                string dsctStock = CdrLN.getInstance().DescontarStockCDR(prueba, dniCDR);
                List<StockCDR.CodigoRespuesta> request = new List<StockCDR.CodigoRespuesta>();

                if (dsctStock == "1")
                {
                    string generarID = StockLN.getInstance().GenerarIDSolicitud(documento, DateTime.Now.Date.ToShortDateString(), TipoPagoS, montoPagoS);
                    double resLineaPagoEfec = montoPagoS - montoLCS, resComisionPagoEfec = montoPagoS - montoComisionS;
                    int IDPeriodoCDR = 0, i2 = 0;
                    bool okCDR = false;

                    foreach (var item in prueba)
                    {
                        int Pack = StockLN.getInstance().UnidadPorPresentacionXProducto(item.codigo);
                        StockCDR objStock = new StockCDR()
                        {
                            DNICDR = documento.Trim(),
                            IDProducto = item.codigo,
                            IDPS = item.PS,
                            Cantidad = Convert.ToInt32(item.cantidad) * Pack,
                            IDProductoXPais = item.idProductoPais
                        };
                        bool ok = StockLN.getInstance().RegistroProductoSolicitud(objStock);
                    }

                    List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR = CdrLN.getInstance().ListaPeriodoCDR();
                    while (!okCDR && i2 < ListaPeriodoCDR.Count())
                    {
                        DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        if (DateTime.Now.AddHours(-2) >= primerDiafechaActual & DateTime.Now.AddHours(-2) <= ultimoDiafechaActual)
                        {
                            okCDR = true; IDPeriodoCDR = ListaPeriodoCDR[i2].IdPeriodo;
                        }
                        i2++;
                    }

                    switch (Convert.ToInt32(TipoPagoS))
                    {
                        case 1:
                            url = PagoEfectivo(documento, montoPagoS, Convert.ToInt32(generarID));
                            break;
                        case 2:
                            url = PagoEfectivo(documento, montoPagoS, Convert.ToInt32(generarID));
                            break;
                        case 3:
                            //CompraLineaCredito(Convert.ToInt32(generarID), montoPagoS, IDPeriodoCDR, prueba);
                            break;
                        case 9:
                            url = PagoEfectivo(documento, montoPagoS, Convert.ToInt32(generarID));
                            break;
                            //case 4: CompraComisiones(Convert.ToInt32(generarID), montoPagoS, IDPeriodoCDR);
                            //    break;
                            //case 5: LineaCVisa();
                            //    break;
                            //case 6: url = LineaCPagoEfectivo(documento, resLineaPagoEfec, Convert.ToInt32(generarID), IDPeriodoCDR, montoLCS);
                            //    break;
                            //case 7: ComisionesVisa();
                            //    break;
                            //case 8: url = ComisionesPagoEfectivo(documento, resComisionPagoEfec, Convert.ToInt32(generarID), IDPeriodoCDR, montoComisionS);
                            //    break;
                    }
                    //EnviarCorreo();
                    request.Add(new StockCDR.CodigoRespuesta { codigo = 1, mensaje = url });
                }
                else
                {
                    request.Add(new StockCDR.CodigoRespuesta { codigo = 2, mensaje = dsctStock });
                }
                return request;
            }
            else
            {
                List<StockCDR.CodigoRespuesta> request = new List<StockCDR.CodigoRespuesta>();
                request.Add(new StockCDR.CodigoRespuesta { codigo = 3, mensaje = "Debe loguearse nuevamente como CDR para realizar el pedido" });
                return request;
            }
            
        }

        public static void CompraComisiones(int idPedido, double montoPago, int idPeriodo)
        {
            List<StockCDR> ListaPedido = null;
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                Cliente.CDR.SustraccionComisionesLC objCDR = new Cliente.CDR.SustraccionComisionesLC()
                {
                    IdPeriodo = idPeriodo,
                    IdPedido = 0,
                    DniCDR = documento,
                    Fecha = DateTime.Now.AddHours(-2),
                    Monto_General = montoPago,
                    Concepto = "COMISIONES",
                    Descripcion = "Compra con Saldo Disponible"
                };
                bool sustraccionCDR = CdrLN.getInstance().RegistrarSustraccionComisionesLC(objCDR);
                Cliente.CDR.InversionCDR objInversion = new Cliente.CDR.InversionCDR()
                {
                    Fecha = DateTime.Now.AddHours(-2),
                    Monto = Convert.ToDecimal(montoPago),
                    DNICDR = documento,
                    IdInicial = "04",
                    TipoCompra = 4
                };
                bool inversion = CdrLN.getInstance().RegistroInversionCDR(objInversion);
                bool actualizarEstado = StockLN.getInstance().ActualizarEstadoSolicitud(idPedido, "", "", "");
                ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(idPedido);
                foreach (var item in ListaPedido)
                {
                    bool recuperar = StockLN.getInstance().IncrementarStock(documento, item.IDProductoXPais, item.Cantidad);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public static void CompraLineaCredito(int idPedido, double montoPago, int idPeriodo, List<StockProductosRegistro> prueba)
        //{
        //    List<StockCDR> ListaPedido = null;
        //    string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
        //    try
        //    {
        //        Cliente.CDR.SustraccionComisionesLC objCDR = new Cliente.CDR.SustraccionComisionesLC()
        //        {
        //            IdPeriodo = idPeriodo,
        //            IdPedido = 0,
        //            DniCDR = documento,
        //            Fecha = DateTime.Now.AddHours(-2),
        //            Monto_General = montoPago,
        //            Concepto = "LINEA CREDITO",
        //            Descripcion = "Compra con Saldo Disponible"
        //        };
        //        bool sustraccionCDR = CdrLN.getInstance().RegistrarSustraccionComisionesLC(objCDR);
        //        bool actualizarEstado = StockLN.getInstance().ActualizarEstadoSolicitud(idPedido, "");
        //        ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(idPedido);
        //        foreach (var item in ListaPedido)
        //        {
        //            bool recuperar = StockLN.getInstance().IncrementarStock(documento, item.IDProductoXPais, item.Cantidad);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public static string PagoEfectivo(string documento, double montoPago, int idPedido)
        {
            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(4);
            List<Cliente.CDR.DatosPersonalesCDR> Lista = CdrLN.getInstance().ListaDatosPersonalesCDR(Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]));
            int diasLimite = listaFecha[0].numeroDias;
            DateTime DayPE = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time"));
            DateTime DayPEExpi = DayPE.AddHours(diasLimite);
            string accessKey = "N2JmODAyZTlhYWFjZDQx";
            string idService = "8434";
            string secretKey = "iXK8YpOVlz8EIGo/pUHWJwtw9baNZ5jbxG3hOw+E";
            string date = Convert.ToString(DayPE.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
            string dateExpi = Convert.ToString(DayPEExpi.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
            string idCliente = System.Web.HttpContext.Current.Session["IdCliente"].ToString();
            string correo = System.Web.HttpContext.Current.Session["Correo"].ToString();
            decimal monto = TextoDecimal(Convert.ToString(Math.Round(montoPago, 2)));

            var token = CompraNegocios.getInstance().getTokenPE(accessKey, idService, secretKey, date);
            Authenticate.Data objectsSend = new Authenticate.Data()
            {
                currency = "PEN",
                amount = monto,
                transactionCode = idPedido.ToString(),
                adminEmail = "pagoefectivosnn@gmail.com",
                dateExpiry = dateExpi,
                paymentConcept = "Compra CDR",
                additionalData = "Datos adicionales",
                userEmail = correo,
                userId = idCliente,
                userName = Lista[0].Nombres,
                userLastName = Lista[0].ApellidoPat + " " + Lista[0].ApellidoMat,
                userUbigeo = "",
                userCountry = "PERU",
                userDocumentType = "DNI",
                userDocumentNumber = Lista[0].Documento,
                userCodeCountry = "+51",
                serviceID = 8434
            };
            TokenPE.GetObjects objectsCIP = CompraNegocios.getInstance().GetCIP(objectsSend, Convert.ToString(token));
            Cliente.CDR.DatosPagoEfectivoCDR objDatos = new Cliente.CDR.DatosPagoEfectivoCDR()
            {
                IdDatos = idPedido,
                DniCDR = documento,
                FechaCreacion = DayPE,
                FechaExpiracion = DayPEExpi,
                CIP = Convert.ToString(objectsCIP.cip),
                Monto = montoPago,
                CodeStatus = objectsCIP.code
            };
            CdrLN.getInstance().RegistroDatosPagoEfectivoCDR(objDatos);
            return objectsCIP.cipURL;
        }

        public static string LineaCPagoEfectivo(string documento, double montoPago, int idPedido, int idPeriodo, double montoLC)
        {
            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(4);
            List<Cliente.CDR.DatosPersonalesCDR> Lista = CdrLN.getInstance().ListaDatosPersonalesCDR(Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]));
            int diasLimite = listaFecha[0].numeroDias;
            DateTime DayPE = DateTime.Now;
            DateTime DayPEExpi = DayPE.AddHours(diasLimite);
            string accessKey = "N2JmODAyZTlhYWFjZDQx";
            string idService = "8434";
            string secretKey = "iXK8YpOVlz8EIGo/pUHWJwtw9baNZ5jbxG3hOw+E";
            string date = Convert.ToString(DayPE.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
            string dateExpi = Convert.ToString(DayPEExpi.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
            string idCliente = System.Web.HttpContext.Current.Session["IdCliente"].ToString();
            string correo = System.Web.HttpContext.Current.Session["Correo"].ToString();
            decimal monto = TextoDecimal(Convert.ToString(Math.Round(montoPago, 2)));

            try
            {
                var token = CompraNegocios.getInstance().getTokenPE(accessKey, idService, secretKey, date);
                Authenticate.Data objectsSend = new Authenticate.Data()
                {
                    currency = "PEN",
                    amount = monto,
                    transactionCode = idPedido.ToString(),
                    adminEmail = "pagoefectivosnn@gmail.com",
                    dateExpiry = dateExpi,
                    paymentConcept = "Compra CDR",
                    additionalData = "Datos adicionales",
                    userEmail = correo,
                    userId = idCliente,
                    userName = Lista[0].Nombres,
                    userLastName = Lista[0].ApellidoPat + " " + Lista[0].ApellidoMat,
                    userUbigeo = "",
                    userCountry = "PERU",
                    userDocumentType = "DNI",
                    userDocumentNumber = Lista[0].Documento,
                    userCodeCountry = "+51",
                    serviceID = 8434
                };
                TokenPE.GetObjects objectsCIP = CompraNegocios.getInstance().GetCIP(objectsSend, Convert.ToString(token));
                Cliente.CDR.SustraccionComisionesLC objCDR = new Cliente.CDR.SustraccionComisionesLC()
                {
                    IdPeriodo = idPeriodo,
                    IdPedido = idPedido,
                    DniCDR = documento,
                    Fecha = DateTime.Now.AddHours(-2),
                    Monto_General = montoLC,
                    Concepto = "LINEA CREDITO",
                    Descripcion = "Compra con Saldo Disponible - PAGOEFECTIVO"
                };
                bool sustraccionCDR = CdrLN.getInstance().RegistrarSustraccionComisionesLC(objCDR);
                Cliente.CDR.DatosPagoEfectivoCDR objDatos = new Cliente.CDR.DatosPagoEfectivoCDR()
                {
                    IdDatos = idPedido,
                    DniCDR = documento,
                    FechaCreacion = DayPE,
                    FechaExpiracion = DayPEExpi,
                    CIP = Convert.ToString(objectsCIP.cip),
                    Monto = montoPago,
                    CodeStatus = objectsCIP.code
                };
                CdrLN.getInstance().RegistroDatosPagoEfectivoCDR(objDatos);
                return objectsCIP.cipURL;
            }
            catch (Exception ex)
            {
                return "error";
                throw ex;
            }
        }

        public static string ComisionesPagoEfectivo(string documento, double montoPago, int idPedido, int idPeriodo, double montoComision)
        {
            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(4);
            List<Cliente.CDR.DatosPersonalesCDR> Lista = CdrLN.getInstance().ListaDatosPersonalesCDR(Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]));
            int diasLimite = listaFecha[0].numeroDias;
            DateTime DayPE = DateTime.Now;
            DateTime DayPEExpi = DayPE.AddHours(diasLimite);
            string accessKey = "N2JmODAyZTlhYWFjZDQx";
            string idService = "8434";
            string secretKey = "iXK8YpOVlz8EIGo/pUHWJwtw9baNZ5jbxG3hOw+E";
            string date = Convert.ToString(DayPE.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
            string dateExpi = Convert.ToString(DayPEExpi.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
            string idCliente = System.Web.HttpContext.Current.Session["IdCliente"].ToString();
            string correo = System.Web.HttpContext.Current.Session["Correo"].ToString();
            string nombre = System.Web.HttpContext.Current.Session["Nombres"].ToString();
            string apellidos = System.Web.HttpContext.Current.Session["Apellidos_Full"].ToString();
            decimal monto = TextoDecimal(Convert.ToString(Math.Round(montoPago, 2)));

            try
            {
                var token = CompraNegocios.getInstance().getTokenPE(accessKey, idService, secretKey, date);
                Authenticate.Data objectsSend = new Authenticate.Data()
                {
                    currency = "PEN",
                    amount = monto,
                    transactionCode = idPedido.ToString(),
                    adminEmail = "pagoefectivosnn@gmail.com",
                    dateExpiry = dateExpi,
                    paymentConcept = "Compra CDR",
                    additionalData = "Datos adicionales",
                    userEmail = correo,
                    userId = idCliente,
                    userName = Lista[0].Nombres,
                    userLastName = Lista[0].ApellidoPat + " " + Lista[0].ApellidoMat,
                    userUbigeo = "",
                    userCountry = "PERU",
                    userDocumentType = "DNI",
                    userDocumentNumber = Lista[0].Documento,
                    userCodeCountry = "+51",
                    serviceID = 8434
                };
                TokenPE.GetObjects objectsCIP = CompraNegocios.getInstance().GetCIP(objectsSend, Convert.ToString(token));
                Cliente.CDR.SustraccionComisionesLC objCDR = new Cliente.CDR.SustraccionComisionesLC()
                {
                    IdPeriodo = idPeriodo,
                    IdPedido = idPedido,
                    DniCDR = documento,
                    Fecha = DateTime.Now.AddHours(-2),
                    Monto_General = montoComision,
                    Concepto = "COMISIONES",
                    Descripcion = "Compra con Saldo Comisiones - PAGOEFECTIVO"
                };
                bool sustraccionCDR = CdrLN.getInstance().RegistrarSustraccionComisionesLC(objCDR);
                Cliente.CDR.DatosPagoEfectivoCDR objDatos = new Cliente.CDR.DatosPagoEfectivoCDR()
                {
                    IdDatos = idPedido,
                    DniCDR = documento,
                    FechaCreacion = DayPE,
                    FechaExpiracion = DayPEExpi,
                    CIP = Convert.ToString(objectsCIP.cip),
                    Monto = montoPago,
                    CodeStatus = objectsCIP.code
                };
                CdrLN.getInstance().RegistroDatosPagoEfectivoCDR(objDatos);
                return objectsCIP.cipURL;
            }
            catch (Exception ex)
            {
                return "error";
                throw ex;
            }
        }

        public static decimal TextoDecimal(string numero)
        {
            var clone = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            clone.NumberFormat.NumberDecimalSeparator = ",";
            clone.NumberFormat.NumberGroupSeparator = ".";
            decimal retorno = decimal.Parse(numero, clone);
            return retorno;
        }

        public static void EnviarCorreo()
        {

            Boolean estado = true;
            String merror;
            string nombreCDR = Convert.ToString(System.Web.HttpContext.Current.Session["NombrePS"]);

            MailMessage mail = new MailMessage();
            mail.CC.Add(new MailAddress("supervisor.cdr@santanaturaperu.net"));
            mail.Bcc.Add(new MailAddress("santanaturavouchers@gmail.com"));
            mail.From = new MailAddress("santanaturavoucher@mundosantanatura.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
            mail.Subject = "Nuevo pedido de " + nombreCDR + "";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Un nuevo pedido ha sido ingresado, porfavor acceda a la plataforma para poder aprobar la solicitud de " + nombreCDR + "";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
            smtp.Credentials = new System.Net.NetworkCredential("santanaturavoucher@mundosantanatura.com", "s@nt@2019");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                estado = false;
                merror = ex.Message.ToString();
            }
        }

        [WebMethod]
        public static List<StockCDR> ListarProductosxSolicitud(string idSolicitud)
        {
            int idSoli = Convert.ToInt32(idSolicitud);
            List<StockCDR> Lista = null;
            try
            {
                Lista = StockLN.getInstance().ListarProductosxSolicitud(idSoli);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

    }
}