﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using Modelos;
using Negocios;
using System.Web.Script.Serialization;

namespace SantaNaturaNetworkV3
{
    public partial class GestionarStock : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931" && Convert.ToString(Session["NumDocCliente"]) != "17397862")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        public string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

        [WebMethod]
        public static List<ProductosCantidad> RellenarStockPS(string cdrPS)
        {
            List<ProductosCantidad> ListaPC = new List<ProductosCantidad>();
            string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("a", "stock_local");
            nvc.Add("local", cdrPS.Trim());

            var data = wc.UploadValues(url, "POST", nvc);

            var responseString = UnicodeEncoding.UTF8.GetString(data);
            JavaScriptSerializer js = new JavaScriptSerializer();
            dynamic LocalesConStock = js.Deserialize<dynamic>(responseString);
            foreach (var item in LocalesConStock)
            {
                string can = item[1];
                ProductosCantidad PC = new ProductosCantidad();
                PC.cantidad = can.Substring(0, can.Length - 4);
                PC.IDPPS = item[0];
                ListaPC.Add(PC);
            }
            return ListaPC;
        }

        [WebMethod]
        public static List<StockCDR> ListadoCDRCombo()
        {
            List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDR();
            var query = from item in ListaCDR
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<StockCDR> ListarCDRStock()
        {
            List<StockCDR> Lista = null;
            try
            {
                Lista = StockLN.getInstance().ListarCDRStock();

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<StockCDR> ListarProductosRegistro()
        {
            List<StockCDR> Lista = null;
            try
            {
                Lista = StockLN.getInstance().ListarProductosRegistro();

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<StockCDR> ListarProductosxStock(string dniCDR)
        {
            List<StockCDR> Lista = null;
            try
            {
                Lista = StockLN.getInstance().ListarProductosxCDR(dniCDR);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static bool RegistrarStock(List<StockProductosRegistro> prueba, string CDR)
        {
            foreach (var item in prueba)
            {
                StockCDR objStock = new StockCDR()
                {
                    DNICDR = CDR,
                    IDProducto = item.codigo,
                    IDPS = item.PS,
                    Imagen = item.imgP,
                    Cantidad = Convert.ToInt32(item.cantidad),
                    IDProductoXPais = item.idProductoPais,
                    ControlStock = item.controlStock
                };
                bool ok = StockLN.getInstance().RegistroStock(objStock);
            }
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Registrar Stock", "CDR: " + CDR);

            return true;
        }

        [WebMethod]
        public static bool ActualizarStock(List<StockProductosRegistro> prueba, string CDR)
        {
            foreach (var item in prueba)
            {
                if (item.cantidad !=null){
                    StockCDR objStock = new StockCDR()
                    {
                        DNICDR = CDR,
                        IDProducto = item.codigo,
                        Cantidad = Convert.ToInt32(item.cantidad),
                        IDProductoXPais = item.idProductoPais,
                        ControlStock = item.controlStock
                    };
                    bool ok = StockLN.getInstance().ActualizarStock(objStock);
                }
            }

            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Actualizar Stock", "CDR: " + CDR);

            return true;
        }

    }
}