﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class ListadoComprasPECDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "07")
                {
                    Response.Redirect("Index.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.DatosPagoEfectivoCDR> ListaPedidosPE()
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<Cliente.CDR.DatosPagoEfectivoCDR> Lista = CdrLN.getInstance().ListaPedidosPE(documento);
            var query = from item in Lista
                        select item;

            return query.ToList();
        }
    }
}