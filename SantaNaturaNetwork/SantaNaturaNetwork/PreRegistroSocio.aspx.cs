﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class PreRegistroSocio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }
                LlenarEstablecimiento();
                LlenarEstablecimientoPremio();
                CargarListaUpline();
                txtPatrocinador.Text = Convert.ToString(Session["NumDocCliente"]);

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
        }

        private void LlenarEstablecimiento()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimiento();
            cboTipoEstablecimiento.DataSource = ListaTipo;
            cboTipoEstablecimiento.DataTextField = "apodo";
            cboTipoEstablecimiento.DataValueField = "IdPeruShop";
            cboTipoEstablecimiento.DataBind();
            cboTipoEstablecimiento.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        private void LlenarEstablecimientoPremio()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimiento();
            cboPremio.DataSource = ListaTipo;
            cboPremio.DataTextField = "apodo";
            cboPremio.DataValueField = "IdPeruShop";
            cboPremio.DataBind();
            cboPremio.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        private void CargarListaUpline()
        {
            string numDocumento = Convert.ToString(Session["NumDocCliente"]);
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListarUplinePreRegistro(numDocumento);
            CboUpLine.DataSource = ListaTipo;
            CboUpLine.DataTextField = "Nombre";
            CboUpLine.DataValueField = "NumeroDoc";
            CboUpLine.DataBind();

            CboUpLine.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        [WebMethod]
        public static string ValidarDatos(string correoS, string documento, string usuario)
        {
            string expresion = @"\A(([a-zA-Z0-9])((\.|\-|_)?[a-zA-Z0-9])*)+(\@)(gmail|hotmail|outlook|yahoo)(\.)(com|net|es)\Z";
            if (!Regex.IsMatch(correoS.Trim(), expresion))
            {
                return "Formato de correo incorrecto";
            }
            int cantUsuario = ClienteLN.getInstance().ValidarCantidadUsuario(usuario.Trim());
            if (cantUsuario >= 1)
            {
                return "El Usuario ingresado ya existe, inserte uno nuevo porfavor";
            }
            int cantDocumento = ClienteLN.getInstance().ValidarCantidadDocumento(documento.Trim());
            if (cantDocumento >= 1)
            {
                return "El Documento ingresado ya existe, inserte uno nuevo porfavor";
            }
            return "0";
        }

        [WebMethod]
        public static void PreRegistro(string numeroDocUd, string usuarioUd, string claveUd, string paisTienda,
                                                 string nombresUd, string apellidoPatUd, string apellidoMatUd, 
                                                 string fechaNacUd, string sexoUd, string tipoDocUd,
                                                 string correoUd, string telefonoUd, string celularUd, string paisUd,
                                                 string departamentoUd, string provinciaUd, string distritoUd,
                                                 string direccionUd, string referenciaUd, string detraccionUd,
                                                 string rucUd, string bancoUd, string depositoUd, string interbancariaUd,
                                                 string patrocinadorUd, string uplineUd, string paqueteUd, string cdrPremioUd,
                                                 string establecimientoUd, string imagenUd, string fecharegistro, string CodigoPostalUd)
        {
            string factorComi = "", tipoClienteUd = "";
            tipoClienteUd = (paqueteUd == "05") ? "05": 
                            (paqueteUd == "06") ? "03": "01";
            factorComi = (uplineUd == "" & (tipoClienteUd == "03" | tipoClienteUd == "05")) ? patrocinadorUd : uplineUd;
            //List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDRStock();
            //establecimientoUd = (establecimientoUd != "0") ? (from c in ListaCDR where c.CDRPS == establecimientoUd select c.DNICDR.Trim()).FirstOrDefault() : establecimientoUd;
            //cdrPremioUd = (from c in ListaCDR where c.CDRPS == cdrPremioUd select c.DNICDR.Trim()).FirstOrDefault();

            Cliente objCliente = new Cliente()
            {
                numeroDoc = numeroDocUd,
                usuario = usuarioUd.ToUpper(),
                clave = claveUd,
                nombre = nombresUd.ToUpper(),
                apellidoPat = apellidoPatUd.ToUpper(),
                apellidoMat = apellidoMatUd.ToUpper(),
                apodo = "",
                fechaNac = fechaNacUd,
                sexo = sexoUd,
                tipoDoc = tipoDocUd,
                correo = correoUd,
                telefono = telefonoUd,
                celular = celularUd,
                pais = paisUd,
                departamento = departamentoUd,
                provincia = provinciaUd,
                ditrito = distritoUd,
                direccion = direccionUd.ToUpper(),
                referencia = referenciaUd.ToUpper(),
                nroCtaDetraccion = detraccionUd,
                ruc = rucUd,
                nombreBanco = bancoUd.ToUpper(),
                nroCtaDeposito = depositoUd,
                nroCtaInterbancaria = interbancariaUd,
                tipoCliente = tipoClienteUd,
                patrocinador = patrocinadorUd,
                upline = uplineUd,
                tipoEstablecimiento = "",
                imagen = imagenUd,
                FechaRegistro = fecharegistro,
                IdPeruShop = "",
                factorComision = factorComi,
                CDRPremio =  "",
                CodigoPostal = "",
                PreRegistro = 1,
                paisTienda = paisTienda
            };

            Descuento objPackete = new Descuento()
            {
                IdPackete = paqueteUd,
                FechaObtencionPackete = fecharegistro
            };
            string ok = ClienteLN.getInstance().RegistrarAfiliacion(objCliente, objPackete);
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarIdPeriodo();
            List<Periodo> ListaPeriodosComision = PeriodoLN.getInstance().ListarIdPeriodoComision();
            foreach (var item in ListaPeriodos)
            {
                bool ok2 = PeriodoLN.getInstance().RegistroClientePeriodoMultiple(numeroDocUd, factorComi, item.idPeriodo, tipoClienteUd, patrocinadorUd);
            }
            foreach (var item in ListaPeriodosComision)
            {
                bool ok2 = PeriodoLN.getInstance().RegistroClientePeriodoMultipleComision(numeroDocUd, factorComi, patrocinadorUd, item.idPeriodoComision, tipoClienteUd);
                bool retencion = PeriodoLN.getInstance().RegistroSocioRetencionXPeriodo(numeroDocUd, item.idPeriodoComision);
            }
        }
    }
}