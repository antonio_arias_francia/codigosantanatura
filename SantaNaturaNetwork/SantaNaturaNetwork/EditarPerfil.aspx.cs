﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;


namespace SantaNaturaNetworkV3
{
    public partial class EditarPerfil : System.Web.UI.Page
    {
        public string nombres = "";
        public string celular = "";
        public string correo = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("InicioAdmin.aspx");
                }
                IniciarLlenadoCombo();
                LlenarEstablecimiento();
                LlenarEstablecimientoPremio();
                nombres = Convert.ToString(Session["Nombres"]) ;
                string idcliente = (string)Session["IdCliente"];
                hf_IdCliente.Value = idcliente;
                Hdn_CaducidadClave.Value = (string)Session["clave_vencida"];
                

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                   // Response.Redirect("EditarPerfil.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "open_cambiar_password();", true);
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    // Response.Redirect("EditarPerfil.aspx");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "open_config_preguntas_seguridad();", true);
                }

            }
        }
        [WebMethod()]
        public static List<Cliente> ListadoPreguntas(string usuario) //Charles 14/04/2021 Preguntas de Seguridad
        {
            return ClienteLN.getInstance().ListadoPreguntas(usuario);
        }
        private void IniciarLlenadoCombo()
        {
            List<TipoCliente> ListaTipo = TipoClienteLN.getInstance().ListarTipo();
            cboTipoCliente.DataSource = ListaTipo;
            cboTipoCliente.DataTextField = "tipo";
            cboTipoCliente.DataValueField = "IdTipo";
            cboTipoCliente.DataBind();
            cboTipoCliente.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        private void LlenarEstablecimiento()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimiento();
            cboTipoEstablecimiento.DataSource = ListaTipo;
            cboTipoEstablecimiento.DataTextField = "apodo";
            cboTipoEstablecimiento.DataValueField = "IdPeruShop";
            cboTipoEstablecimiento.DataBind();
            cboTipoEstablecimiento.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        private void LlenarEstablecimientoPremio()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimiento();
            ddlPremio.DataSource = ListaTipo;
            ddlPremio.DataTextField = "apodo";
            ddlPremio.DataValueField = "IdPeruShop";
            ddlPremio.DataBind();
            ddlPremio.Items.Insert(0, new ListItem("Seleccione", "0"));
        }

        [WebMethod]
        public static List<Pais> GetPais()
        {
            List<Pais> ListaPais = PaisLN.getInstance().ListarPais();
            var query = from item in ListaPais
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<Departamento> GetDepartamentosByPais(string pais)
        {
            List<Departamento> ListaDepartamento = DepartamentoLN.getInstance().GetDepartamentos();
            var query = from item in ListaDepartamento
                        where item.CodigoPais == pais
                        select item;

            return query.ToList();

        }

        [WebMethod]
        public static List<Provincia> GetProvinciaByDepartamento(string departamento)
        {
            List<Provincia> ListaProvincia = ProvinciaLN.getInstance().GetProvincia();
            var query = from item in ListaProvincia
                        where item.CodigoDepartamento == departamento
                        select item;

            return query.ToList();

        }

        [WebMethod]
        public static List<Distrito> GetDistritoByProvincia(string provincia)
        {
            List<Distrito> ListaDistrito = DistritoLN.getInstance().GetDistrito();
            var query = from item in ListaDistrito
                        where item.CodigoProvincia == provincia
                        select item;

            return query.ToList();

        }

        [WebMethod]
        public static List<Cliente> ListaDatosClienteByDocumento() {

            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarClienteByDocumento(documento);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static bool EliminarImagen(string imagen)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/uploads/" + imagen);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static bool ActualizarCliente(string usuarioUd, string claveUd, string nombresUd, 
                                             string apellidoPatUd, string apellidoMatUd, string fechaNacUd, 
                                             string sexoUd, string tipoDocUd, string numeroDocUd, string direccionUd, 
                                             string referenciaUd, string telefonoUd, string celularUd, string paisUd, 
                                             string departamentoUd, string provinciaUd, string distritoUd, string correoUd,
                                             string rucUd, string interbancariaUd, string bancoUd, string depositoUd, 
                                             string detraccionUd, string imagenUd, string establecimientoUd, string establecimientoUdPremio)
        {
            List<Cliente> clientes = new List<Cliente>();
            clientes = ClienteLN.getInstance().ListaEstable();
            string linqApodo = "";
            string linqApodoPremio = "";
            if (paisUd != "01") { establecimientoUd = "Seleccione"; establecimientoUdPremio = "Seleccione"; }
            if (establecimientoUd == "Seleccione") { linqApodo = ""; } else { linqApodo = (from c in clientes where c.apodo.Trim() == establecimientoUd.Trim() select c.numeroDoc.Trim()).FirstOrDefault(); }
            if (establecimientoUdPremio == "Seleccione") { linqApodoPremio = ""; } else { linqApodoPremio = (from c in clientes where c.apodo.Trim() == establecimientoUdPremio.Trim() select c.numeroDoc.Trim()).FirstOrDefault(); }
            string idSocio = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            Cliente objCliente = new Cliente()
            {
                idCliente = idSocio,
                usuario = usuarioUd,
                clave = claveUd,
                nombre = nombresUd.ToUpper(),
                apellidoPat = apellidoPatUd.ToUpper(),
                apellidoMat = apellidoMatUd.ToUpper(),
                fechaNac = fechaNacUd,
                sexo = sexoUd,
                tipoDoc = tipoDocUd,
                numeroDoc = numeroDocUd.Trim(),
                direccion = direccionUd.ToUpper(),
                referencia = referenciaUd.ToUpper(),
                telefono = telefonoUd,
                celular = celularUd,
                pais = paisUd,
                departamento = departamentoUd,
                provincia = provinciaUd,
                ditrito = distritoUd,
                correo = correoUd,
                ruc = rucUd,
                nroCtaInterbancaria = interbancariaUd,
                nombreBanco = bancoUd.ToUpper(),
                nroCtaDeposito = depositoUd,
                nroCtaDetraccion = detraccionUd,
                imagen = imagenUd,
                tipoEstablecimiento = linqApodo,
                idtipoEstablecimientoPremio = linqApodoPremio
            };
            bool ok = ClienteLN.getInstance().ActualizarPerfilSocio(objCliente);
            System.Web.HttpContext.Current.Session["txtCelular"] = celularUd;
            System.Web.HttpContext.Current.Session["Correo"] = correoUd;
            System.Web.HttpContext.Current.Session["RUCSocio"] = rucUd;
            return true;
        }
        [WebMethod]
        public static bool ValidarCorreo(string correoS)
        {
            string expresion = @"\A(([a-zA-Z0-9])((\.|\-|_)?[a-zA-Z0-9])*)+(\@)(gmail|hotmail|outlook|yahoo)(\.)(com|net|es)\Z";            if (Regex.IsMatch(correoS, expresion))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [WebMethod]
        public static string ValidarClaveUsuario(string usuario,string clave,string tipo)
        {
            string result = "";
            result = ClienteLN.getInstance().ValidarClaveUsuario(usuario,clave,tipo);
            return result;
        }
        [WebMethod]
        public static string ActualizarClaveUsuario(string usuario, string clave)
        {
            string result = "";
            result = ClienteLN.getInstance().ActualizarClaveUsuario(usuario, clave);
            return result;
        }
        [WebMethod]
        public static string ActualizarPreguntasSeguridad(string usuario, string pregunta,string respuesta)
        {
            string result = "";
            result = ClienteLN.getInstance().ActualizarPreguntasSeguridad(usuario, pregunta,respuesta);
            return result;
        }
    }
}