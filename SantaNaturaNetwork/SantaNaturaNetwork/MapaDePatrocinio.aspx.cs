﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class MapaDePatrocinio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }
                Session["mostrarCompraTerminada"] = 0;

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
        }

        [WebMethod]
        public static string CapturarDNI()
        {
            string documento = "";
            try
            {
                documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return documento;
        }

        [WebMethod]
        public static List<ElementosTreeGrid> ListarEstructuraPatrocinio()
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<ElementosTreeGrid> Lista = null;
            try
            {
                Lista = ElementosTreeGridLN.getInstance().ListaTreeGridPatrocinio(documento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

    }
}