﻿using Datos;
using Modelos;
using Negocios;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Specialized;
using System.Text;
using System.Web.Services;
using System.Text.RegularExpressions;

namespace SantaNaturaNetwork
{
    public partial class DetalleDeCompra : System.Web.UI.Page
    {
        public List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
        public List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();

        protected void Page_Load(object sender, EventArgs e)
        {
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);

            if (productosCarrito.Count != 0)
            {
                ListaTipoPago();
                ListaTienda();
                LlenarPackete();
                ListaPreRegistroCBO();
                CargarListaUpline();
                CargarPatrocinador();
                LlenarPais();
                LlenarEstablecimiento();
                ListaTipoCliente();
                DatosCompra();
            }
            else
            {
                Response.Redirect("TiendaSN.aspx");
            }
        }

        private void DatosCompra()
        {
            double totalPuntos = (double)(Session["SubTotalPuntos"]);
            double totalCorazones = (double)(Session["SubTotalCorazones"]);
            double totalPuntosPromo = (double)(Session["SubTotalPuntosPromocion"]);
            double montoTotal = (double)(Session["SubTotal"]);
            double montoAPagar = (double)Session["MontoAPagar"];

            LbCorazones.Text = totalCorazones.ToString("N2").Replace(",", ".");
            LbPuntosCompra.Text = totalPuntos.ToString("N2").Replace(",", ".");
            LbPuntosRango.Text = totalPuntosPromo.ToString("N2").Replace(",", ".");
            LbPrecioTotal.Text = "S/. " + montoTotal.ToString("N2").Replace(",", ".");
            LbPrecioPagar.Text = "S/. " + montoAPagar.ToString("N2").Replace(",", ".");

            lbTCompra2.Text = STipoCompra.Items[STipoCompra.SelectedIndex].Text;
            lbPuntosRango2.Text = totalPuntosPromo.ToString("N2").Replace(",", ".");
            lblPuntos2.Text = totalPuntos.ToString("N2").Replace(",", ".");
            lblPTotal2.Text = "S/. " + montoTotal.ToString("N2").Replace(",", ".");
            lblPPagar2.Text = "S/. " + montoAPagar.ToString("N2").Replace(",", ".");
            lbTienda2.Text = ComboTienda.Items[ComboTienda.SelectedIndex].Text;
            lbMPago2.Text = SMedioPago.Items[SMedioPago.SelectedIndex].Text;
        }

        private void ListaTipoPago()
        {
            List<TipoPago> ListaTipo = CompraNegocios.getInstance().ListaTipoPago();
            SMedioPago.DataSource = ListaTipo;
            SMedioPago.DataTextField = "descripcion";
            SMedioPago.DataValueField = "idPago";
            SMedioPago.DataBind();
            SMedioPago.Items.Insert(0, new ListItem("Seleccione", "0"));
            string TP = Convert.ToString(Session["TipoPago"]);

            if (TP != "")
            {
                SMedioPago.Value = TP;
            }
        }

        private void ListaTienda()
        {
            string sTipoCom = Convert.ToString(Session["sTipoCompraSelect"]);
            List<Cliente> ListaCDR = ClienteLN.getInstance().ListaEstablecimiento();
            ComboTienda.DataSource = ListaCDR;
            ComboTienda.DataTextField = "apodo";
            ComboTienda.DataValueField = "IdPeruShop";
            ComboTienda.DataBind();
            ComboTienda.Items.Insert(0, new ListItem("Seleccione", "0"));
            string tiendaSeleccionada = Convert.ToString(Session["comboTiendaSelect"]);
            string establecimiento = Convert.ToString(Session["Establecimiento"]);
            string linqApodo = (establecimiento == "") ? "" : (from c in ListaCDR where c.numeroDoc.Trim() == establecimiento select c.IdPeruShop.Trim()).FirstOrDefault();

            ComboTienda.Value = tiendaSeleccionada;
        }

        private void LlenarEstablecimiento()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimiento();
            cboTipoEstablecimiento.DataSource = ListaTipo;
            cboTipoEstablecimiento.DataTextField = "apodo";
            cboTipoEstablecimiento.DataValueField = "IdPeruShop";
            cboTipoEstablecimiento.DataBind();
            cboTipoEstablecimiento.Items.Insert(0, new ListItem("SELECCIONE", ""));

            cboPremio.DataSource = ListaTipo;
            cboPremio.DataTextField = "apodo";
            cboPremio.DataValueField = "IdPeruShop";
            cboPremio.DataBind();
            cboPremio.Items.Insert(0, new ListItem("SELECCIONE", ""));

            ddlTitularRUC.Items.Insert(1, new ListItem(Convert.ToString(Session["Nombre_Completo"]), "1"));
            ddlTitularRUC.Items.Insert(2, new ListItem("OTRA PERSONA", "2"));
        }

        private void LlenarPackete()
        {
            string tipoCiente = Convert.ToString(Session["TipoCliente"]);
            string packete = Convert.ToString(Session["PacketeSocio"]);
            string sessionTipoC = Session["sTipoCompraSelect"].ToString();
            List<Packete> ListaPackete = PacketeNegocios.getInstance().ListarPackete(packete, tipoCiente);
            STipoCompra.DataSource = ListaPackete;
            STipoCompra.DataTextField = "_Packete";
            STipoCompra.DataValueField = "Codigo";
            STipoCompra.DataBind();
            STipoCompra.Items.Insert(0, new ListItem("Seleccione", "0"));
            if (sessionTipoC != "")
            {
                STipoCompra.Value = sessionTipoC;
            }
        }

        private void ListaPreRegistroCBO()
        {
            string patro = Convert.ToString(Session["NumDocCliente"]);
            List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
            cboPreRegistro.DataSource = ListaPreRe;
            cboPreRegistro.DataTextField = "CboPreregistro";
            cboPreRegistro.DataValueField = "idcliente";
            cboPreRegistro.DataBind();
            cboPreRegistro.Items.Insert(0, new ListItem("Ninguno", "0"));

            string PR = Convert.ToString(Session["CBOPreRegistro"]);
            if (PR != "0" && PR != "" && PR != null)
            {
                cboPreRegistro.Value = PR;
            }
        }

        private void CargarListaUpline()
        {
            string numDocumento = Convert.ToString(Session["NumDocCliente"]);
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListarUpline(numDocumento);
            CboUpLine.DataSource = ListaTipo;
            CboUpLine.DataTextField = "Nombre";
            CboUpLine.DataValueField = "NumeroDoc";
            CboUpLine.DataBind();

            CboUpLine.Items.Insert(0, new ListItem("SELECCIONE", ""));
        }

        private void CargarPatrocinador()
        {
            txtPatrocinador.Text = (string)Session["NombrePatrocinador"];
            txtPatrocinador.Enabled = false;
        }

        private void LlenarPais()
        {
            List<Pais> ListaPais = PaisLN.getInstance().ListarPais();
            cboPais.DataSource = ListaPais;
            cboPais.DataTextField = "Nombre";
            cboPais.DataValueField = "Codigo";
            cboPais.DataBind();
            cboPais.Items.Insert(0, new ListItem("SELECCIONE", ""));
            cboDepartamento.Items.Insert(0, new ListItem("SELECCIONE", ""));
            cboProvincia.Items.Insert(0, new ListItem("SELECCIONE", ""));
            cboDistrito.Items.Insert(0, new ListItem("SELECCIONE", ""));
        }

        private void ListaTipoCliente()
        {
            List<TipoCliente> ListaTipo = TipoClienteLN.getInstance().ListarTipo();
            cboTipoCliente.DataSource = ListaTipo;
            cboTipoCliente.DataTextField = "tipo";
            cboTipoCliente.DataValueField = "IdTipo";
            cboTipoCliente.DataBind();

            cboTipoCliente.Items.FindByValue("06").Enabled = false;
            cboTipoCliente.Items.FindByValue("07").Enabled = false;
            cboTipoCliente.Items.FindByValue("08").Enabled = false;
            cboTipoCliente.Items.FindByValue("09").Enabled = false;
            cboTipoCliente.Items.FindByValue("10").Enabled = false;
            cboTipoCliente.Items.Insert(0, new ListItem("SELECCIONE", ""));
        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito EliminarProducto(string idProdPais)
        {
            double montoTotalARestar = 0.00;
            double corazonesTotalARestar = 0.00;
            double puntosTotalARestar = 0.00;
            double puntosTotalARestarPromo = 0.00;
            double montoTotalAPagarARestar = 0.00;
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            string idProducto = ProductoLN.getInstance().ObtenerIdProductoXIdProductoPais(idProdPais);
            List<Producto> productoEvaluar = ProductoNegocios.getInstance().ListarProductosByCodigo(idProducto);
            string idPaqueteU = productoEvaluar[0].idPaquete;
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);

            foreach (var item in productosCarrito)
            {
                if (idPaqueteU != "0" && item.idPaquete == idPaqueteU)
                {
                    montoTotalARestar = montoTotalARestar + item.SubTotal;
                    corazonesTotalARestar = corazonesTotalARestar + item.SubTotalCorazones;
                    puntosTotalARestar = puntosTotalARestar + item.SubTotalPuntos;
                    puntosTotalARestarPromo = puntosTotalARestarPromo + item.SubTotalPuntosPromocion;
                    montoTotalAPagarARestar = montoTotalAPagarARestar + item.SubTotalNeto;
                }
                else if (item.Codigo == idProdPais && item.idPaquete == "0")
                {
                    montoTotalARestar = item.SubTotal;
                    corazonesTotalARestar = item.SubTotalCorazones;
                    puntosTotalARestar = item.SubTotalPuntos;
                    puntosTotalARestarPromo = item.SubTotalPuntosPromocion;
                    montoTotalAPagarARestar = item.SubTotalNeto;
                }
            }

            double montoTotal = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotal"]);
            double montoAPagar = Convert.ToDouble(System.Web.HttpContext.Current.Session["MontoAPagar"]);

            System.Web.HttpContext.Current.Session["SubTotalPuntos"] = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntos"]) - puntosTotalARestar;
            System.Web.HttpContext.Current.Session["SubTotalCorazones"] = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalCorazones"]) - corazonesTotalARestar;
            System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]) - puntosTotalARestarPromo;
            System.Web.HttpContext.Current.Session["SubTotal"] = montoTotal - montoTotalARestar;
            System.Web.HttpContext.Current.Session["MontoAPagar"] = montoAPagar - montoTotalAPagarARestar;

            if (idPaqueteU == "0") { productosCarritoPaquete.RemoveAll(x => x.Codigo == idProdPais); productosCarrito.RemoveAll(x => x.Codigo == idProdPais); }
            else { productosCarritoPaquete.RemoveAll(x => x.CodigoValidar == idProducto); productosCarrito.RemoveAll(x => x.idPaquete == idPaqueteU); }

            System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
            System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;


            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
            {
                Mensaje = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]).Count().ToString(),
                Total = System.Web.HttpContext.Current.Session["SubTotal"].ToString(),
                Monto = System.Web.HttpContext.Current.Session["MontoAPagar"].ToString(),
                Puntos = System.Web.HttpContext.Current.Session["SubTotalPuntos"].ToString(),
                PuntosPromo = System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"].ToString()
            };

            return RetornarDatos;
        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito ActualizarProducto(string idProdPais, string nuevaCantidad, string tipCompra)
        {
            string tipCliente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            string tipoCompra = Convert.ToString(tipCompra);
            string packeteSocio = Convert.ToString(System.Web.HttpContext.Current.Session["PacketeSocio"]);
            string idProducto = ProductoLN.getInstance().ObtenerIdProductoXIdProductoPais(idProdPais);
            List<Producto> productoEvaluar = ProductoNegocios.getInstance().ListarProductosByCodigo(idProducto);
            List<PaqueteNatura> productosPaquete = new List<PaqueteNatura>();
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);

            string idPaqueteU = productoEvaluar[0].idPaquete;

            double nuevoMontoTotal = 0.00;
            double nuevoCorazonesTotal = 0.00;
            double nuevoPuntosTotal = 0.00;
            double nuevoPuntosTotalPromo = 0.00;
            double nuevoMontoAPagar = 0.00;
            double subTotalNetoPaquete = 0.00;
            double subTotalPuntosPaquete = 0.00;
            double subTotalPPromocionPaquete = 0.00;
            double subTotalCorazonesPaquete = 0.00;
            int CC = 0;

            foreach (var item in productosCarrito)
            {
                if (idPaqueteU != "0")
                {
                    if (item.idPaquete == idPaqueteU)
                    {
                        CC++;
                        productosPaquete = PaqueteLN.getInstance().ListaPaqueteByCodigoPeru(idPaqueteU);
                        int CantG = (from c in productosPaquete where c.idProductoGeneral.Trim() == item.CodigoValidar select c.cantidadGeneral).FirstOrDefault();
                        item.Cantidad = Convert.ToInt32(nuevaCantidad) * CantG;
                        item.cantidadPeruShop = Convert.ToInt32(nuevaCantidad) * CantG;
                        item.precioStr = item.precioStr;
                        item.SubTotal = (Convert.ToInt32(nuevaCantidad) * CantG) * item.PrecioUnitario;
                        item.SubTotalNeto = ObtenerSubTotalNeto(tipoCompra, packeteSocio, item.SubTotal, tipCliente, item.Linea);
                        subTotalNetoPaquete = subTotalNetoPaquete + item.SubTotalNeto;
                        item.SubTotalPuntos = (Convert.ToInt32(nuevaCantidad) * CantG) * item.Puntos;
                        subTotalPuntosPaquete = subTotalPuntosPaquete + item.SubTotalPuntos;
                        item.SubTotalCorazones = (Convert.ToInt32(nuevaCantidad) * CantG) * item.Corazones;
                        subTotalCorazonesPaquete = subTotalCorazonesPaquete + item.SubTotalCorazones;
                        item.SubTotalPuntosPromocion = (Convert.ToInt32(nuevaCantidad) * CantG) * item.PuntosPromocion;
                        subTotalPPromocionPaquete = subTotalPPromocionPaquete + item.SubTotalPuntosPromocion;
                        if (CC == productosPaquete.Count)
                        {
                            foreach (var item2 in productosCarritoPaquete)
                            {
                                if (item2.CodigoValidar == idProducto & item2.idPaquete == idPaqueteU)
                                {
                                    item2.Cantidad = Convert.ToInt32(nuevaCantidad);
                                    item2.cantidadPeruShop = Convert.ToInt32(nuevaCantidad);
                                    item2.SubTotal = Convert.ToInt32(nuevaCantidad) * item2.PrecioUnitario;
                                    item2.SubTotalNeto = subTotalNetoPaquete;
                                    item2.SubTotalCorazones = subTotalCorazonesPaquete;
                                    item2.SubTotalPuntos = subTotalPuntosPaquete;
                                    item2.SubTotalPuntosPromocion = subTotalPPromocionPaquete;

                                    System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
                                }
                            }
                        }

                        System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
                    }
                }
                else if (item.Codigo == idProdPais && item.idPaquete == "0")
                {
                    item.Cantidad = Convert.ToInt32(nuevaCantidad);
                    item.cantidadPeruShop = Convert.ToInt32(nuevaCantidad);
                    item.precioStr = item.precioStr;
                    item.SubTotal = Convert.ToInt32(nuevaCantidad) * item.PrecioUnitario;
                    item.SubTotalNeto = ObtenerSubTotalNeto(tipoCompra, packeteSocio, item.SubTotal, tipCliente, item.Linea);
                    item.SubTotalCorazones = Convert.ToInt32(nuevaCantidad) * item.Corazones;
                    item.SubTotalPuntos = Convert.ToInt32(nuevaCantidad) * item.Puntos;
                    item.SubTotalPuntosPromocion = Convert.ToInt32(nuevaCantidad) * item.PuntosPromocion;

                    System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
                }

                nuevoCorazonesTotal = nuevoCorazonesTotal + item.SubTotalCorazones;
                nuevoPuntosTotal = nuevoPuntosTotal + item.SubTotalPuntos;
                nuevoPuntosTotalPromo = nuevoPuntosTotalPromo + item.SubTotalPuntosPromocion;
                nuevoMontoAPagar = nuevoMontoAPagar + item.SubTotalNeto;
                nuevoMontoTotal = nuevoMontoTotal + item.SubTotal;
            }
            foreach (var lista in productosCarritoPaquete)
            {
                if (lista.Codigo == idProdPais && lista.idPaquete == "0")
                {
                    lista.Cantidad = Convert.ToInt32(nuevaCantidad);
                    lista.cantidadPeruShop = Convert.ToInt32(nuevaCantidad);
                    lista.precioStr = lista.precioStr;
                    lista.SubTotal = Convert.ToInt32(nuevaCantidad) * lista.PrecioUnitario;
                    lista.SubTotalNeto = ObtenerSubTotalNeto(tipoCompra, packeteSocio, lista.SubTotal, tipCliente, lista.Linea);
                    lista.SubTotalCorazones = Convert.ToInt32(nuevaCantidad) * lista.Corazones;
                    lista.SubTotalPuntos = Convert.ToInt32(nuevaCantidad) * lista.Puntos;
                    lista.SubTotalPuntosPromocion = Convert.ToInt32(nuevaCantidad) * lista.PuntosPromocion;

                    System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
                }
            }

            System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
            System.Web.HttpContext.Current.Session["SubTotalPuntos"] = nuevoPuntosTotal;
            System.Web.HttpContext.Current.Session["SubTotalCorazones"] = nuevoCorazonesTotal;
            System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = nuevoPuntosTotalPromo;
            System.Web.HttpContext.Current.Session["SubTotal"] = nuevoMontoTotal;
            System.Web.HttpContext.Current.Session["MontoAPagar"] = nuevoMontoAPagar;
            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
            {
                Mensaje = "OK",
                Total = System.Web.HttpContext.Current.Session["SubTotal"].ToString(),
                Monto = System.Web.HttpContext.Current.Session["MontoAPagar"].ToString(),
                Puntos = System.Web.HttpContext.Current.Session["SubTotalPuntos"].ToString(),
                PuntosPromo = System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"].ToString(),
                SubTotalProd = (from d in productosCarritoPaquete where d.Codigo == idProdPais select d.SubTotalNeto.ToString()).SingleOrDefault(),
                SubTotalPunt = (from d in productosCarritoPaquete where d.Codigo == idProdPais select d.SubTotalPuntos.ToString()).SingleOrDefault()
            };

            return RetornarDatos;
        }

        private static double ObtenerSubTotalNeto(string tipoCompraSeleccionado, string packeteSocio, double subTotal, string tipCliente, string lineaDelProducto)
        {
            string patrocinadorPackete = Convert.ToString(System.Web.HttpContext.Current.Session["PatrocinadorPackete"]);
            List<Packete> listaPacketes = PacketeNegocios.getInstance().ListarPackete("", "");

            string consultorDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Consultor" select d.Descuento).SingleOrDefault();
            string patrocinaPacketeDescuento = (from d in listaPacketes where d.Codigo == patrocinadorPackete select d.Descuento).SingleOrDefault();
            string socioPacketeDescuento = (from d in listaPacketes where d.Codigo == packeteSocio select d.Descuento).SingleOrDefault();

            string emprendedorDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Básico" select d.Descuento).SingleOrDefault();
            string profesionalDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Profesional" select d.Descuento).SingleOrDefault();
            string empresarialDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Empresarial" select d.Descuento).SingleOrDefault();
            string millonarioDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Millonario" select d.Descuento).SingleOrDefault();
            string imperialDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Imperial" select d.Descuento).SingleOrDefault();

            double subTotalNeto = 0.0;
            if (tipoCompraSeleccionado == "07") //Tipo consumo
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //Linea general
                {
                    if (tipCliente == "03") //C. Inteligente
                    {
                        double descuento = Convert.ToDouble(patrocinaPacketeDescuento, CultureInfo.InvariantCulture);
                        System.Web.HttpContext.Current.Session["descuentoCI"] = descuento;
                        subTotalNeto = subTotal * (1 - (descuento / 2));
                    }
                    else if (tipCliente == "05") //Consultor
                    {
                        double descuento = Convert.ToDouble(consultorDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuento);
                    }
                    else //Socio
                    {
                        subTotalNeto = ObtenerSubTotalNetoLineaGeneral(packeteSocio, subTotal, tipoCompraSeleccionado);
                    }
                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else //Consumo saludable
                {
                    if (tipCliente == "03") //C. Inteligente
                    {
                        subTotalNeto = subTotal * (1 - 0.075);
                    }
                    else if (tipCliente == "05") //Consultor
                    {
                        subTotalNeto = subTotal * (1 - 0.1);
                    }
                    else
                    {
                        subTotalNeto = subTotal * (1 - 0.15);
                    }
                }
            } // AFILIACION
            else if (tipoCompraSeleccionado == "01" || tipoCompraSeleccionado == "02" || tipoCompraSeleccionado == "03" || tipoCompraSeleccionado == "04" || tipoCompraSeleccionado == "05" || tipoCompraSeleccionado == "06" || tipoCompraSeleccionado == "23")
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //General
                {
                    if (tipoCompraSeleccionado == "01") // Afi: Emprendedor
                    {
                        double descuentoEmprend = Convert.ToDouble(emprendedorDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoEmprend);
                    }
                    else if (tipoCompraSeleccionado == "02") // Afi: Profesional
                    {
                        double descuentoProfesio = Convert.ToDouble(profesionalDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoProfesio);
                    }
                    else if (tipoCompraSeleccionado == "03") // Afi: Empresarial
                    {
                        double descuentoEmpresa = Convert.ToDouble(empresarialDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoEmpresa);
                    }
                    else if (tipoCompraSeleccionado == "04") // Afi: Millonario
                    {
                        double descuentoMillonar = Convert.ToDouble(millonarioDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoMillonar);
                    }
                    else if (tipoCompraSeleccionado == "05") //Afi: Consultor
                    {
                        double descuento = Convert.ToDouble(consultorDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuento);
                    }
                    else if (tipoCompraSeleccionado == "23") //Afi: Imperial
                    {
                        double descuento = Convert.ToDouble(imperialDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuento);
                    }
                    else //Afi: C. Inteligente
                    {
                        double descuento = Convert.ToDouble(socioPacketeDescuento, CultureInfo.InvariantCulture);
                        System.Web.HttpContext.Current.Session["descuentoCI"] = descuento;
                        subTotalNeto = subTotal * (1 - (descuento / 2));
                    }

                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else //Consumo saludable
                {
                    if (tipoCompraSeleccionado == "06") //C. Inteligente
                    {
                        subTotalNeto = subTotal * (1 - 0.075);
                    }
                    else if (tipoCompraSeleccionado == "05") //Consultor
                    {
                        subTotalNeto = subTotal * (1 - 0.1);
                    }
                    else
                    {
                        subTotalNeto = subTotal * (1 - 0.15);
                    }
                }
            }//UPGRADE
            else if (tipoCompraSeleccionado == "08" || tipoCompraSeleccionado == "09" || tipoCompraSeleccionado == "10" || tipoCompraSeleccionado == "11" || tipoCompraSeleccionado == "12" || tipoCompraSeleccionado == "13" || tipoCompraSeleccionado == "24" ||
                     tipoCompraSeleccionado == "25" || tipoCompraSeleccionado == "26" || tipoCompraSeleccionado == "27")
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //Linea general
                {
                    subTotalNeto = ObtenerSubTotalNetoLineaGeneral(packeteSocio, subTotal, tipoCompraSeleccionado); //Socio
                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else
                {
                    subTotalNeto = subTotal * (1 - 0.15);
                }
            }//MIGRACIÓN
            else
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //Linea general
                {
                    subTotalNeto = ObtenerSubTotalNetoLineaGeneral(packeteSocio, subTotal, tipoCompraSeleccionado); //Socio
                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else
                {
                    subTotalNeto = subTotal * (1 - 0.15);
                }
            }
            return subTotalNeto;
        }

        private static double ObtenerSubTotalNetoLineaGeneral(string packeteSocio, double subTotal, string tipoCompraSeleccionado)
        {
            string tipoCiente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            List<Packete> listaPacketes = PacketeNegocios.getInstance().ListarPackete(packeteSocio, tipoCiente);
            double subTotalNeto = 0.0;

            //CONSUMO
            if (tipoCompraSeleccionado == "07")
            {
                foreach (var packete in listaPacketes)
                {
                    if (packeteSocio == packete.Codigo)
                    {
                        double descuento = Convert.ToDouble(packete.Descuento, CultureInfo.InvariantCulture);
                        return subTotalNeto = subTotal * (1 - descuento);
                    }
                }
            } //UPGRADE
            else if (tipoCompraSeleccionado == "08" || tipoCompraSeleccionado == "09" || tipoCompraSeleccionado == "10" || tipoCompraSeleccionado == "11" || tipoCompraSeleccionado == "12" || tipoCompraSeleccionado == "13" ||
                     tipoCompraSeleccionado == "24" || tipoCompraSeleccionado == "25" || tipoCompraSeleccionado == "26" || tipoCompraSeleccionado == "27")
            {
                foreach (var packete in listaPacketes)
                {
                    if (tipoCompraSeleccionado == packete.Codigo)
                    {
                        double descuento = Convert.ToDouble(packete.Descuento, CultureInfo.InvariantCulture);
                        return subTotalNeto = subTotal * (1 - descuento);
                    }
                }
            } //MIGRACIÓN
            else
            {
                foreach (var packete in listaPacketes)
                {
                    if (tipoCompraSeleccionado == packete.Codigo)
                    {
                        double descuento = Convert.ToDouble(packete.Descuento, CultureInfo.InvariantCulture);
                        return subTotalNeto = subTotal * (1 - descuento);
                    }
                }
            }
            return subTotalNeto;
        }

        [WebMethod]
        public static Cliente.RetornarDatosCombo GuardarIDCombos(string comboS, string idComboS)
        {
            double MontoPago = 0; string paquete = "";
            Cliente.RetornarDatosCombo RetornarDatos = null;
            System.Web.HttpContext.Current.Session["sTipoCompraSelect"] = (comboS == "TCompra") ? idComboS : System.Web.HttpContext.Current.Session["sTipoCompraSelect"].ToString();
            System.Web.HttpContext.Current.Session["TipoPago"] = (comboS == "MPago") ? idComboS : System.Web.HttpContext.Current.Session["TipoPago"].ToString();
            System.Web.HttpContext.Current.Session["comboTiendaSelect"] = (comboS == "TiendaS") ? idComboS : System.Web.HttpContext.Current.Session["comboTiendaSelect"].ToString();
            System.Web.HttpContext.Current.Session["CBOPreRegistro"] = (comboS == "Pregis") ? idComboS : System.Web.HttpContext.Current.Session["CBOPreRegistro"].ToString();
            if (comboS == "TCompra") { MontoPago = AplicarDescuento(idComboS); }
            if (comboS == "Pregis" && (idComboS != "0" && idComboS != "" && idComboS != null))
            {
                string patro = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
                paquete = (from d in ListaPreRe where d.IdCliente == idComboS select d.IdPaquete).FirstOrDefault();
                System.Web.HttpContext.Current.Session["sTipoCompraSelect"] = paquete;
            }
            RetornarDatos = new Cliente.RetornarDatosCombo()
            {
                MontoPago = MontoPago,
                IdPaquete = paquete
            };
            return RetornarDatos;
        }

        private static double AplicarDescuento(string sTipoCom)
        {
            List<ProductoCarrito> productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            List<ProductoCarrito> productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            string packeteSocio = Convert.ToString(System.Web.HttpContext.Current.Session["PacketeSocio"]);
            string tipCliente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            double montoAPagar = 0.0;

            foreach (var itemR in productosCarritoPaquete)
            {
                itemR.SubTotalNeto = 0;
            }
            foreach (var item in productosCarrito)
            {

                item.SubTotalNeto = ObtenerSubTotalNeto(sTipoCom, packeteSocio, item.SubTotal, tipCliente, item.Linea);
                foreach (var itemP in productosCarritoPaquete)
                {
                    if (item.idPaquete != "0" && itemP.idPaquete == item.idPaquete)
                    {
                        itemP.SubTotalNeto = itemP.SubTotalNeto + item.SubTotalNeto;
                    }
                    else if (itemP.idPaquete == "0" && item.idPaquete == "0")
                    {
                        itemP.SubTotalNeto = ObtenerSubTotalNeto(sTipoCom, packeteSocio, itemP.SubTotal, tipCliente, itemP.Linea);
                    }
                    System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
                }

                System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
                //System.Web.HttpContext.Current.Session["MontoAPagar"] = 0.0; //Reseteo para darle el montoAPagar con descuento
                montoAPagar = montoAPagar + item.SubTotalNeto;
            }
            System.Web.HttpContext.Current.Session["MontoAPagar"] = montoAPagar;
            return montoAPagar;
        }

        private static void DescontarStockProducto(string cdr, string idPS, int cantidad, string idProPais)
        {
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();
            string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == cdr.Trim() select c.DNICDR.Trim()).FirstOrDefault();
            bool descontar = StockLN.getInstance().DescontarStock(dniCDR, idProPais, cantidad);
        }

        private static ProductoV2.AgregarCarrito VerificarSiHayStock2(string IDPS, int cantidad, string idProdPais)
        {
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            ProductoV2.AgregarCarrito retornoStock = null;
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);

            bool stock = false;
            string cantidadExisteM = "";
            string tiendaSeleccionada = System.Web.HttpContext.Current.Session["comboTiendaSelect"].ToString();
            string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == tiendaSeleccionada.Trim() select c.DNICDR.Trim()).FirstOrDefault();
            bool ControlStock = StockLN.getInstance().ObtenerControlStockXIDPP(idProdPais, dniCDR);

            if (ControlStock == true)
            {
                int cantProductosCarrito = (from c in productosCarrito where c.IdProdPeruShop.Trim() == IDPS.Trim() select c.Cantidad).FirstOrDefault();
                int cantidadExistente = StockLN.getInstance().CantidadStockCDR(dniCDR, IDPS, "01", idProdPais);
                cantidadExisteM = cantidadExistente.ToString();
                if ((cantProductosCarrito) <= cantidadExistente) { stock = true; }
            }
            else { stock = true; }

            retornoStock = new ProductoV2.AgregarCarrito() { CantidadExistente = cantidadExisteM, Estado = stock };

            return retornoStock;
        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito RealizarCompraConsumoCanje(string cTienda, string txTienda, string cComprobante, string txComprobante, string cTitularRUC,
                                                                                  string idTCompra, string txTCompra, string cTipoPago, string txRUCCOM)
        {
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            Compra compra = new Compra();
            ResponseStock responseStock = new ResponseStock();
            responseStock.Productos = new List<string>();
            ProductoV2.AgregarCarrito Agregar = null;
            string cantidadExisteM = "", idProdPeruShop = "";

            //Verificar stock de los productos
            foreach (var item in productosCarrito)
            {
                idProdPeruShop = item.IdProdPeruShop;

                if (idProdPeruShop == "110038")
                {
                    idProdPeruShop = "2069";
                    Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                    cantidadExisteM = Agregar.CantidadExistente;
                    bool stock = Agregar.Estado;
                    if (stock)
                    {
                        idProdPeruShop = "108683";
                        int cant2 = item.Cantidad * 2;

                        Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                        cantidadExisteM = (Agregar.Estado == true) ? "0" : Agregar.CantidadExistente;
                        stock = Agregar.Estado;
                        if (stock)
                        {
                            responseStock.Stock = true;
                        }
                        else
                        {
                            responseStock.Stock = false;
                            responseStock.Establecimiento = txTienda;
                            responseStock.Productos.Add(item.NombreProducto);
                        }
                    }
                    else
                    {
                        responseStock.Stock = false;
                        responseStock.Establecimiento = txTienda;
                        responseStock.Productos.Add(item.NombreProducto);
                    }
                }
                else
                {
                    Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                    cantidadExisteM = Agregar.CantidadExistente;
                    bool stock = Agregar.Estado;
                    if (stock)
                    {
                        responseStock.Stock = true;
                    }
                    else
                    {
                        responseStock.Stock = false;
                        responseStock.Establecimiento = txTienda;
                        responseStock.Productos.Add(item.NombreProducto);
                    }
                }
            }

            if (ValidarPromocion() == false)
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "Ocurrió un error con el paquete seleccionado, porfavor vuelva a realizar la compra"
                };
                LimpiarSession();
                return RetornarDatos;
            }
            else
            {
                if (responseStock.Stock)//Si hay stock
                {
                    double puntoPromo = 0.00, montoAPagar = 0.0, montoComi = 0.0000, montoComiCI = 0.0000, pe = 0.00, puntosExtras = 0, descuentoCI = 0;
                    string tipoCliente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
                    int cantidad = productosCarrito.Sum(x => x.Cantidad);
                    string PR = Convert.ToString(System.Web.HttpContext.Current.Session["CBOPreRegistro"]);
                    string rucFactura = "", idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
                    string dniComprador = (string)System.Web.HttpContext.Current.Session["NumDocCliente"];
                    List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(dniComprador);
                    productosCarrito.RemoveAll(x => x.cantidadPeruShop == 0);
                    descuentoCI = Convert.ToDouble(System.Web.HttpContext.Current.Session["descuentoCI"]);

                    foreach (var item in productosCarrito)
                    {
                        montoAPagar = montoAPagar + item.SubTotalNeto;
                    }

                    foreach (var item in productosCarrito)
                    {
                        if (item.Linea == "02") { montoComi = montoComi + item.SubTotalNeto; }
                        if (item.Linea == "02" | item.Linea == "01") { puntoPromo = puntoPromo + item.SubTotalPuntos; }
                        //if (idtipoCompra == "08" | idtipoCompra == "09" | idtipoCompra == "10" |
                        //    idtipoCompra == "11" | idtipoCompra == "12" | idtipoCompra == "13")
                        //{

                        //    if (item.Linea == "02") { pe = pe + item.SubTotalPuntos; }
                        //}
                        item.montoSend = item.SubTotalNeto / item.cantidadPeruShop;
                        DescontarStockProducto(cTienda, item.IdProdPeruShop, item.cantidadPeruShop, item.Codigo);
                    }

                    //if (idtipoCompra == "07")
                    //{
                    //    foreach (var itemP in productosCarritoPaquete)
                    //    {
                    //        if (itemP.CodigoValidar != "P0261" && itemP.CodigoValidar != "P0231" && itemP.CodigoValidar != "P0197")
                    //        {
                    //            puntosExtras += itemP.SubTotalPuntos;
                    //        }
                    //    }
                    //}
                    if (tipoCliente == "03") { montoComiCI = montoComiCI = (montoComi / (1 - (descuentoCI / 2))) * (descuentoCI / 2); }
                    if (idTCompra == "23" | idTCompra == "25" | idTCompra == "26" | idTCompra == "27") { puntosExtras += 500; }
                    if (cComprobante == "2" && cTitularRUC == "1") { rucFactura = Convert.ToString(System.Web.HttpContext.Current.Session["RUCSocio"]); }
                    else if (cComprobante == "2" && cTitularRUC == "2") { rucFactura = txRUCCOM; }
                    dniComprador = (PR != "0" && PR != "" && PR != null) ? (from c in ListaPreRe where c.IdCliente == PR select c.Documento.Trim()).FirstOrDefault(): dniComprador;
                    idCliente = (PR != "0" && PR != "" && PR != null) ? PR: idCliente;
                    string Paquete = (PR != "0" && PR != "" && PR != null) ? txTCompra : ClienteLN.getInstance().ObtenerPaqueteSocio(dniComprador);
                    //if (idtipoCompra == "01" | idtipoCompra == "02" | idtipoCompra == "03" | idtipoCompra == "04" | idtipoCompra == "05" | idtipoCompra == "06" |
                    //    idtipoCompra == "08" | idtipoCompra == "09" | idtipoCompra == "10" | idtipoCompra == "11" | idtipoCompra == "12" | idtipoCompra == "13" |
                    //    idtipoCompra == "23" | idtipoCompra == "24" | idtipoCompra == "25" | idtipoCompra == "26" | idtipoCompra == "27")
                    //{
                    //    puntosExtras = puntoPromo + puntosExtras;
                    //}

                    double montoTotal = (double)System.Web.HttpContext.Current.Session["SubTotal"];
                    DateTime send = DateTime.Now.AddHours(-2);
                    compra.IdopPeruShop = "";
                    compra.CodCliente = idCliente;
                    compra.Cantidad = cantidad;
                    compra.MontoTotal = montoTotal;
                    compra.PuntosTotal = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntos"]);
                    compra.Corazones = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalCorazones"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]) + puntosExtras;
                    compra.MontoAPagar = montoAPagar;
                    compra.FechaPago = send;
                    compra.FechaStock = send;
                    compra.Estado = 2;
                    compra.TipoPago = cTipoPago; //DEPOSITO
                    compra.idTipoCompra = idTCompra;
                    compra.TipoCompra = txTCompra;
                    compra.DepositoTotal = 00.00;
                    compra.DepositoFraccionado = 00.00;
                    compra.FechaDeposito = send;
                    compra.Despacho = cTienda;
                    compra.montoComision = montoComi;
                    compra.IdPromo = "";
                    compra.cantPromo = 0;
                    compra.cantRegalo = 0;
                    compra.puntosGastados = 0;
                    compra.montoComisionCI = montoComiCI;
                    compra.IdPago = "00000000";
                    compra.Transaction_Date = "";
                    compra.Merchant = "";
                    compra.Id_Unico = "";
                    compra.Transaction_ID = "";
                    compra.CardS = "";
                    compra.Aauthorization_Code = "";
                    compra.AMOUNT = "";
                    compra.CURRENCY = "";
                    compra.BRAND = "";
                    compra.STATUSS = "";
                    compra.ACTION_DESCRIPTION = "";
                    compra.NotaDelivery = "";
                    compra.Comprobante = txComprobante;
                    compra.Ruc = rucFactura;
                    compra.PaqueteSocio = Paquete.Substring(12, Paquete.Length - 12);
                    compra.IDP = getPeriodoRango_Comision(send);

                    CompraNegocios.getInstance().RegistrarCompraAndDetalle(compra, productosCarrito);
                    LimpiarSession();
                    RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                    {
                        Mensaje = "OK"
                    };
                    return RetornarDatos;
                }
                else
                {
                    string productosSinStock = "";

                    foreach (var item in responseStock.Productos)
                    {
                        productosSinStock = productosSinStock + "<br />" + item;
                    }
                    string establecimientoSinStock = responseStock.Establecimiento;
                    string horaRestante = HorasRestantes(idProdPeruShop);

                    if (horaRestante == "0")
                    {
                        if (Convert.ToDouble(cantidadExisteM) < 0)
                        {
                            cantidadExisteM = "0";
                        }

                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                        {
                            Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock + ". Actualmente contamos con " + cantidadExisteM + ""
                        };
                        return RetornarDatos;
                    }
                    else
                    {
                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                        {
                            Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock
                        };
                        return RetornarDatos;

                    }
                }
            }

        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito RealizarCompraConsumoPE(string cTienda, string txTienda, string cComprobante, string txComprobante, string cTitularRUC,
                                                                              string idTCompra, string txTCompra, string cTipoPago, string txRUCCOM)
        {
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            Compra compra = new Compra();
            ResponseStock responseStock = new ResponseStock();
            responseStock.Productos = new List<string>();
            ProductoV2.AgregarCarrito Agregar = null;
            string cantidadExisteM = "", idProdPeruShop = "";

            //Verificar stock de los productos
            foreach (var item in productosCarrito)
            {
                idProdPeruShop = item.IdProdPeruShop;

                if (idProdPeruShop == "110038")
                {
                    idProdPeruShop = "2069";
                    Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                    cantidadExisteM = Agregar.CantidadExistente;
                    bool stock = Agregar.Estado;
                    if (stock)
                    {
                        idProdPeruShop = "108683";
                        int cant2 = item.Cantidad * 2;

                        Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                        cantidadExisteM = Agregar.CantidadExistente;
                        stock = Agregar.Estado;
                        if (stock)
                        {
                            responseStock.Stock = true;
                        }
                        else
                        {
                            responseStock.Stock = false;
                            responseStock.Establecimiento = txTienda;
                            responseStock.Productos.Add(item.NombreProducto);
                        }
                    }
                    else
                    {
                        responseStock.Stock = false;
                        responseStock.Establecimiento = txTienda;
                        responseStock.Productos.Add(item.NombreProducto);
                    }
                }
                else
                {
                    Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                    cantidadExisteM = Agregar.CantidadExistente;
                    bool stock = Agregar.Estado;
                    if (stock)
                    {
                        responseStock.Stock = true;
                    }
                    else
                    {
                        responseStock.Stock = false;
                        responseStock.Establecimiento = txTienda;
                        responseStock.Productos.Add(item.NombreProducto);
                    }
                }
            }
            ///COMPRA POR CONSUMO - EFECTIVO O DEPOSITO

            if (ValidarPromocion() == false)
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "Ocurrió un error con el paquete seleccionado, porfavor vuelva a realizar la compra"
                };
                LimpiarSession();
                return RetornarDatos;
            }
            else
            {
                if (responseStock.Stock)//Si hay stock
                {
                    double puntoPromo = 0.00, montoAPagar = 0.0, montoComi = 0.0000, montoComiCI = 0.0000, pe = 0.00, puntosExtras = 0, descuentoCI = 0;
                    string tipoCliente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
                    int cantidad = productosCarrito.Sum(x => x.Cantidad);
                    string PR = Convert.ToString(System.Web.HttpContext.Current.Session["CBOPreRegistro"]);
                    string rucFactura = "", idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
                    string dniComprador = (string)System.Web.HttpContext.Current.Session["NumDocCliente"];
                    List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(dniComprador);
                    productosCarrito.RemoveAll(x => x.cantidadPeruShop == 0);
                    descuentoCI = Convert.ToDouble(System.Web.HttpContext.Current.Session["descuentoCI"]);

                    foreach (var item in productosCarrito)
                    {
                        montoAPagar = montoAPagar + item.SubTotalNeto;
                    }

                    foreach (var item in productosCarrito)
                    {
                        if (item.Linea == "02") { montoComi = montoComi + item.SubTotalNeto; }
                        if (item.Linea == "02" | item.Linea == "01") { puntoPromo = puntoPromo + item.SubTotalPuntos; }
                        item.montoSend = item.SubTotalNeto / item.cantidadPeruShop;
                        DescontarStockProducto(cTienda, item.IdProdPeruShop, item.cantidadPeruShop, item.Codigo);
                    }

                    if (tipoCliente == "03") { montoComiCI = montoComiCI = (montoComi / (1 - (descuentoCI / 2))) * (descuentoCI / 2); }
                    if (idTCompra == "23" | idTCompra == "24" | idTCompra == "25" | idTCompra == "26" | idTCompra == "27") { puntosExtras += 500; }
                    if (cComprobante == "2" && cTitularRUC == "1") { rucFactura = Convert.ToString(System.Web.HttpContext.Current.Session["RUCSocio"]); }
                    else if (cComprobante == "2" && cTitularRUC == "2") { rucFactura = txRUCCOM; }
                    dniComprador = (PR != "0" && PR != "" && PR != null) ? (from c in ListaPreRe where c.IdCliente == PR select c.Documento.Trim()).FirstOrDefault() : dniComprador;
                    idCliente = (PR != "0" && PR != "" && PR != null) ? PR : idCliente;
                    string Paquete = (PR != "0" && PR != "" && PR != null) ? txTCompra : ClienteLN.getInstance().ObtenerPaqueteSocio(dniComprador);

                    double montoTotal = (double)System.Web.HttpContext.Current.Session["SubTotal"];
                    DateTime send = DateTime.Now.AddHours(-2);
                    compra.IdopPeruShop = "";
                    compra.CodCliente = idCliente;
                    compra.Cantidad = cantidad;
                    compra.MontoTotal = montoTotal;
                    compra.PuntosTotal = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntos"]);
                    compra.Corazones = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalCorazones"]);
                    compra.PuntosTotalPromo = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]) + puntosExtras;
                    compra.MontoAPagar = montoAPagar;
                    compra.FechaPago = send;
                    compra.FechaStock = send;
                    compra.Estado = 1;
                    compra.TipoPago = cTipoPago;
                    compra.idTipoCompra = idTCompra;
                    compra.TipoCompra = txTCompra;
                    compra.DepositoTotal = 00.00;
                    compra.DepositoFraccionado = 00.00;
                    compra.FechaDeposito = send;
                    compra.Despacho = cTienda;
                    compra.montoComision = montoComi;
                    compra.IdPromo = "";
                    compra.cantPromo = 0;
                    compra.cantRegalo = 0;
                    compra.puntosGastados = 0;
                    compra.montoComisionCI = montoComiCI;
                    compra.IdPago = "00000000";
                    compra.Transaction_Date = "";
                    compra.Merchant = "";
                    compra.Id_Unico = "";
                    compra.Transaction_ID = "";
                    compra.CardS = "";
                    compra.Aauthorization_Code = "";
                    compra.AMOUNT = "";
                    compra.CURRENCY = "";
                    compra.BRAND = "";
                    compra.STATUSS = "";
                    compra.ACTION_DESCRIPTION = "";
                    compra.NotaDelivery = "";
                    compra.Comprobante = txComprobante;
                    compra.Ruc = rucFactura;
                    compra.PaqueteSocio = Paquete.Substring(12, Paquete.Length - 12);
                    compra.IDP = getPeriodoRango_Comision(send);

                    string ticket = CompraNegocios.getInstance().RegistrarCompraAndDetalle(compra, productosCarrito);
                    if (ticket != "" || ticket != null)
                    {
                        int countTicket = CompraNegocios.getInstance().CantidadTicket_DPE(ticket, idCliente);
                        if (countTicket == 1)
                        {
                            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(3);
                            int diasLimite = listaFecha[0].numeroDias;
                            DateTime xxx = DateTime.Now;
                            DateTime DayPE = TimeZoneInfo.ConvertTime(xxx, TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time"));
                            DateTime DayPEExpi = DayPE.AddHours(diasLimite);
                            string accessKey = "N2JmODAyZTlhYWFjZDQx";
                            string idService = "8434";
                            string secretKey = "iXK8YpOVlz8EIGo/pUHWJwtw9baNZ5jbxG3hOw+E";
                            string date = Convert.ToString(DayPE.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
                            string dateExpi = Convert.ToString(DayPEExpi.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
                            string nombres = (PR != "0" && PR != "" && PR != null) ? (from c in ListaPreRe where c.IdCliente == PR select c.Nombres.Trim()).FirstOrDefault() : 
                                             Convert.ToString(System.Web.HttpContext.Current.Session["Nombres"]).Trim();
                            string apellidos = (PR != "0" && PR != "" && PR != null) ? (from c in ListaPreRe where c.IdCliente == PR select c.Apellidos.Trim()).FirstOrDefault() :
                                               Convert.ToString(System.Web.HttpContext.Current.Session["Apellidos_Full"]).Trim();
                            string correo = (PR != "0" && PR != "" && PR != null) ? ClienteLN.getInstance().ObtenerCorreo_By_IdCliente(PR) : Convert.ToString(System.Web.HttpContext.Current.Session["Correo"]).Trim();
                            decimal monto = TextoDecimal(Convert.ToString(Math.Round(montoAPagar, 2)));

                            var token = CompraNegocios.getInstance().getTokenPE(accessKey, idService, secretKey, date);
                            Authenticate.Data objectsSend = new Authenticate.Data()
                            {
                                currency = "PEN",
                                amount = monto,
                                transactionCode = ticket,
                                adminEmail = "pagoefectivosnn@gmail.com",
                                dateExpiry = dateExpi,
                                paymentConcept = "Validar",
                                additionalData = "datos adicionales",
                                userEmail = correo,
                                userId = idCliente,
                                userName = nombres,
                                userLastName = apellidos,
                                userUbigeo = "",
                                userCountry = "PERU",
                                userDocumentType = "DNI",
                                userDocumentNumber = dniComprador,
                                userCodeCountry = "+51",
                                serviceID = 8434
                            };
                            TokenPE.GetObjects objectsCIP = CompraNegocios.getInstance().GetCIP(objectsSend, Convert.ToString(token));
                            if (objectsCIP.field == "" | objectsCIP.field == null)
                            {
                                DatosPagoEfectivo objDatos = new DatosPagoEfectivo()
                                {
                                    Ticket = ticket,
                                    IdCliente = idCliente,
                                    FechaCreacion = DayPE,
                                    FechaExpiracion = DayPEExpi,
                                    CIP = Convert.ToString(objectsCIP.cip),
                                    Monto = montoAPagar,
                                    CodeStatus = objectsCIP.code
                                };
                                CompraNegocios.getInstance().RegistroDatosPagoEfectivo(objDatos);
                                
                                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                                {
                                    Mensaje = "OK",
                                    URL = objectsCIP.cipURL
                                };
                                LimpiarSession();
                                return RetornarDatos;
                            }
                            else
                            {
                                bool registro = ClienteLN.getInstance().RegistroModificaciones(System.Web.HttpContext.Current.Session["NumDocCliente"].ToString(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                                       "Error Sistema", objectsCIP.message);
                                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                                {
                                    Mensaje = "Ocurrió un error inesperado, porfavor vuelva a realizar la compra"
                                };
                                LimpiarSession();
                                return RetornarDatos;
                            }
                        }
                        else
                        {
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Ocurrió un error inesperado, porfavor vuelva a dar click en Comprar"
                            };
                            return RetornarDatos;
                        }
                    }
                    else
                    {
                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                        {
                            Mensaje = "Ocurrió un error inesperado, porfavor vuelva a dar click en Comprar"
                        };
                        return RetornarDatos;
                    }
                }
                else
                {
                    string productosSinStock = "";

                    foreach (var item in responseStock.Productos)
                    {
                        productosSinStock = productosSinStock + "<br />" + item;
                    }
                    string establecimientoSinStock = responseStock.Establecimiento;
                    string horaRestante = HorasRestantes(idProdPeruShop);

                    if (horaRestante == "0")
                    {
                        if (Convert.ToDouble(cantidadExisteM) < 0)
                        {
                            cantidadExisteM = "0";
                        }

                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                        {
                            Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock + ". Actualmente contamos con " + cantidadExisteM + ""
                        };
                        return RetornarDatos;
                    }
                    else
                    {
                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                        {
                            Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock
                        };
                        return RetornarDatos;

                    }
                }
            }
        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito RealizarCompraAfiliacionCanje(string cTienda, string txTienda, string cComprobante, string txComprobante, string cTitularRUC,
                                                                                    string idTCompra, string txTCompra, string cTipoPago, string txCorreo, string txDocumento,
                                                                                    string txUsuario, string txFechaNac, string txCDRPreferido, string txCDRPremio, string txClave,
                                                                                    string txNombre, string txApellidoPat, string txApellidoMat, string txSexo, string txTipoDoc,
                                                                                    string txTelefono, string txCelular, string cPais, string cDepartamento, string cProvincia,
                                                                                    string cDistrito, string txDireccion, string txReferencia, string txDetraccion, string txRUC,
                                                                                    string txBanco, string txNumCuenta, string txInterbancaria, string cTCliente, string txUpline,
                                                                                    string txRUCCOM)
        {
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            int cantUsuario = ClienteLN.getInstance().ValidarCantidadUsuario(txUsuario.Trim());
            Compra compra = new Compra();
            ResponseStock responseStock = new ResponseStock();
            responseStock.Productos = new List<string>();
            ProductoV2.AgregarCarrito Agregar = null;
            string cantidadExisteM = "", idProdPeruShop = "";

            if (ValidarPromocion() == false)
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "Ocurrió un error con el paquete seleccionado, porfavor vuelva a realizar la compra"
                };
                LimpiarSession();
                return RetornarDatos;
            }
            else if (cantUsuario >= 1)
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "El Usuario ingresado ya existe, inserte uno nuevo porfavor"
                };
                return RetornarDatos;
            }
            else
            {
                bool FormatoCorreo = CompraNegocios.getInstance().ConfirmarCorreo(txCorreo.Trim());

                if (FormatoCorreo == false)
                {
                    RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                    {
                        Mensaje = "Ingrese el correo en un formato válido: ejemplo@gmail.com"
                    };
                    return RetornarDatos;
                }
                else if (ValidacionNumero(txDocumento.Trim()) == false)
                {
                    RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                    {
                        Mensaje = "El número de Documento de Identidad es incorrecto"
                    };
                    return RetornarDatos;
                }
                else
                {
                    //Verificar stock de los productos
                    foreach (var item in productosCarrito)
                    {
                        idProdPeruShop = item.IdProdPeruShop;

                        if (idProdPeruShop == "110038")
                        {
                            idProdPeruShop = "2069";
                            Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                            cantidadExisteM = Agregar.CantidadExistente;
                            bool stock = Agregar.Estado;
                            if (stock)
                            {
                                idProdPeruShop = "108683";
                                int cant2 = item.Cantidad * 2;

                                Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                                cantidadExisteM = Agregar.CantidadExistente;
                                stock = Agregar.Estado;
                                if (stock)
                                {
                                    responseStock.Stock = true;
                                }
                                else
                                {
                                    responseStock.Stock = false;
                                    responseStock.Establecimiento = txTienda;
                                    responseStock.Productos.Add(item.NombreProducto);
                                }
                            }
                            else
                            {
                                responseStock.Stock = false;
                                responseStock.Establecimiento = txTienda;
                                responseStock.Productos.Add(item.NombreProducto);
                            }
                        }
                        else
                        {
                            Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                            cantidadExisteM = Agregar.CantidadExistente;
                            bool stock = Agregar.Estado;
                            if (stock)
                            {
                                responseStock.Stock = true;
                            }
                            else
                            {
                                responseStock.Stock = false;
                                responseStock.Establecimiento = txTienda;
                                responseStock.Productos.Add(item.NombreProducto);
                            }
                        }
                    }

                    if (responseStock.Stock)
                    {
                        Cliente.RetornarDatosAfiliacion RetornoAfiliacion = RegistrarAfiliacion(txDocumento, txFechaNac, txCDRPreferido, txCDRPremio, txUsuario, txClave, txNombre,
                                                                                                txApellidoPat, txApellidoMat, txSexo, txTipoDoc, txCorreo, txTelefono, txCelular,
                                                                                                cPais, cDepartamento, cProvincia, cDistrito, txDireccion, txReferencia, txDetraccion,
                                                                                                txRUC, txBanco, txNumCuenta, txInterbancaria, cTCliente, txUpline, idTCompra);
                        int cantidad = productosCarrito.Sum(x => x.Cantidad);

                        if (RetornoAfiliacion.Estado)
                        {
                            double puntoPromo = 0.00;
                            double descuentoCI = Convert.ToDouble(System.Web.HttpContext.Current.Session["descuentoCI"]);
                            string rucFactura = "";
                            double montoAPagar = 0.0;
                            string idopPeruShop = "";
                            if (cComprobante == "2" && cTitularRUC == "1") { rucFactura = Convert.ToString(System.Web.HttpContext.Current.Session["RUCSocio"]); }
                            else if (cComprobante == "2" && cTitularRUC == "2") { rucFactura = txRUCCOM; }
                            string Paquete = ClienteLN.getInstance().ObtenerPaqueteSocio(txDocumento.Trim());

                            if (productosCarrito != null)
                            {
                                foreach (var item in productosCarrito)
                                {
                                    montoAPagar = montoAPagar + item.SubTotalNeto;
                                }
                            }

                            double montoComi = 0.0000;
                            double montoComiCI = 0.0000;
                            double puntosExtras = 0;

                            foreach (var item in productosCarrito)
                            {
                                if (item.Linea == "02") { montoComi = montoComi + item.SubTotalNeto; }
                                if (item.Linea == "02" | item.Linea == "01") { puntoPromo = puntoPromo + item.SubTotalPuntos; }
                                item.montoSend = item.SubTotalNeto / item.cantidadPeruShop;
                                DescontarStockProducto(cTienda, item.IdProdPeruShop, item.cantidadPeruShop, item.Codigo);
                            }

                            if (idTCompra == "06") { montoComiCI = (montoComi / (1 - (descuentoCI / 2))) * (descuentoCI / 2); }
                            if (idTCompra == "23") { puntosExtras += 500; }

                            DateTime send = DateTime.Now.AddHours(-2);
                            compra.IdopPeruShop = idopPeruShop;
                            compra.CodCliente = RetornoAfiliacion.IdCliente;
                            compra.Cantidad = cantidad;
                            compra.MontoTotal = (double)System.Web.HttpContext.Current.Session["SubTotal"];
                            compra.Corazones = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalCorazones"]);
                            compra.PuntosTotal = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntos"]);
                            compra.PuntosTotalPromo = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]) + puntosExtras;
                            compra.MontoAPagar = montoAPagar;
                            compra.FechaPago = send;
                            compra.Estado = 2; // CAMBIAR EN PE
                            compra.TipoPago = cTipoPago; //DEPOSITO
                            compra.idTipoCompra = idTCompra;
                            compra.TipoCompra = txTCompra;
                            compra.DepositoTotal = 00.00;
                            compra.DepositoFraccionado = 00.00;
                            compra.FechaDeposito = send;
                            compra.FechaStock = send;
                            compra.Despacho = cTienda;
                            compra.montoComision = montoComi;
                            compra.IdPromo = "";
                            compra.cantPromo = 0;
                            compra.cantRegalo = 0;
                            compra.puntosGastados = 0;
                            compra.montoComisionCI = montoComiCI;
                            compra.IdPago = "00000000";
                            compra.Transaction_Date = "";
                            compra.Merchant = "";
                            compra.Id_Unico = "";
                            compra.Transaction_ID = "";
                            compra.CardS = "";
                            compra.Aauthorization_Code = "";
                            compra.AMOUNT = "";
                            compra.CURRENCY = "";
                            compra.BRAND = "";
                            compra.STATUSS = "";
                            compra.ACTION_DESCRIPTION = "";
                            compra.NotaDelivery = "";
                            compra.Comprobante = txComprobante;
                            compra.Ruc = rucFactura;
                            compra.PaqueteSocio = Paquete.Substring(12, Paquete.Length - 12);
                            compra.IDP = getPeriodoRango_Comision(send);

                            CompraNegocios.getInstance().RegistrarCompraAndDetalle(compra, productosCarrito);

                            LimpiarSession();
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "OK"
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            ClienteLN.getInstance().Eliminarcliente_DPE(RetornoAfiliacion.IdCliente);
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = RetornoAfiliacion.Mensaje
                            };
                            return RetornarDatos;
                        }
                    }
                    else
                    {
                        string productosSinStock = "";

                        foreach (var item in responseStock.Productos)
                        {
                            productosSinStock = productosSinStock + "<br />" + item;
                        }
                        string establecimientoSinStock = responseStock.Establecimiento;
                        string horaRestante = HorasRestantes(idProdPeruShop);

                        if (horaRestante == "0")
                        {
                            if (Convert.ToDouble(cantidadExisteM) < 0)
                            {
                                cantidadExisteM = "0";
                            }

                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock + ". Actualmente contamos con " + cantidadExisteM + ""
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock
                            };
                            return RetornarDatos;

                        }
                    }
                }

            }

        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito RealizarCompraAfiliacionPE(string cTienda, string txTienda, string cComprobante, string txComprobante, string cTitularRUC,
                                                                                 string idTCompra, string txTCompra, string cTipoPago, string txCorreo, string txDocumento,
                                                                                 string txUsuario, string txFechaNac, string txCDRPreferido, string txCDRPremio, string txClave,
                                                                                 string txNombre, string txApellidoPat, string txApellidoMat, string txSexo, string txTipoDoc,
                                                                                 string txTelefono, string txCelular, string cPais, string cDepartamento, string cProvincia,
                                                                                 string cDistrito, string txDireccion, string txReferencia, string txDetraccion, string txRUC,
                                                                                 string txBanco, string txNumCuenta, string txInterbancaria, string cTCliente, string txUpline,
                                                                                 string txRUCCOM)
        {
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            int cantUsuario = ClienteLN.getInstance().ValidarCantidadUsuario(txUsuario.Trim());
            Compra compra = new Compra();
            ResponseStock responseStock = new ResponseStock();
            responseStock.Productos = new List<string>();
            ProductoV2.AgregarCarrito Agregar = null;
            string cantidadExisteM = "", idProdPeruShop = "";

            if (ValidarPromocion() == false)
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "Ocurrió un error con el paquete seleccionado, porfavor vuelva a realizar la compra"
                };
                LimpiarSession();
                return RetornarDatos;
            }
            else if (cantUsuario >= 1)
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "El Usuario ingresado ya existe, inserte uno nuevo porfavor"
                };
                return RetornarDatos;
            }
            else
            {
                bool FormatoCorreo = CompraNegocios.getInstance().ConfirmarCorreo(txCorreo.Trim());

                if (FormatoCorreo == false)
                {
                    RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                    {
                        Mensaje = "Ingrese el correo en un formato válido: ejemplo@gmail.com"
                    };
                    return RetornarDatos;
                }
                else if (ValidacionNumero(txDocumento.Trim()) == false)
                {
                    RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                    {
                        Mensaje = "El número de Documento de Identidad es incorrecto"
                    };
                    return RetornarDatos;
                }
                else
                {
                    //Verificar stock de los productos
                    foreach (var item in productosCarrito)
                    {
                        idProdPeruShop = item.IdProdPeruShop;

                        if (idProdPeruShop == "110038")
                        {
                            idProdPeruShop = "2069";
                            Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                            cantidadExisteM = Agregar.CantidadExistente;
                            bool stock = Agregar.Estado;
                            if (stock)
                            {
                                idProdPeruShop = "108683";
                                int cant2 = item.Cantidad * 2;

                                Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                                cantidadExisteM = Agregar.CantidadExistente;
                                stock = Agregar.Estado;
                                if (stock)
                                {
                                    responseStock.Stock = true;
                                }
                                else
                                {
                                    responseStock.Stock = false;
                                    responseStock.Establecimiento = txTienda;
                                    responseStock.Productos.Add(item.NombreProducto);
                                }
                            }
                            else
                            {
                                responseStock.Stock = false;
                                responseStock.Establecimiento = txTienda;
                                responseStock.Productos.Add(item.NombreProducto);
                            }
                        }
                        else
                        {
                            Agregar = VerificarSiHayStock2(idProdPeruShop, item.Cantidad, item.Codigo);
                            cantidadExisteM = Agregar.CantidadExistente;
                            bool stock = Agregar.Estado;
                            if (stock)
                            {
                                responseStock.Stock = true;
                            }
                            else
                            {
                                responseStock.Stock = false;
                                responseStock.Establecimiento = txTienda;
                                responseStock.Productos.Add(item.NombreProducto);
                            }
                        }
                    }

                    if (responseStock.Stock)
                    {
                        Cliente.RetornarDatosAfiliacion RetornoAfiliacion = RegistrarAfiliacion(txDocumento, txFechaNac, txCDRPreferido, txCDRPremio, txUsuario, txClave, txNombre,
                                                                                                txApellidoPat, txApellidoMat, txSexo, txTipoDoc, txCorreo, txTelefono, txCelular,
                                                                                                cPais, cDepartamento, cProvincia, cDistrito, txDireccion, txReferencia, txDetraccion,
                                                                                                txRUC, txBanco, txNumCuenta, txInterbancaria, cTCliente, txUpline, idTCompra);
                        int cantidad = productosCarrito.Sum(x => x.Cantidad);

                        if (RetornoAfiliacion.Estado)
                        {
                            double puntoPromo = 0.00;
                            double descuentoCI = Convert.ToDouble(System.Web.HttpContext.Current.Session["descuentoCI"]);
                            string rucFactura = "";
                            double montoAPagar = 0.0;
                            string idopPeruShop = "";
                            if (cComprobante == "2" && cTitularRUC == "1") { rucFactura = Convert.ToString(System.Web.HttpContext.Current.Session["RUCSocio"]); }
                            else if (cComprobante == "2" && cTitularRUC == "2") { rucFactura = txRUCCOM; }
                            string Paquete = ClienteLN.getInstance().ObtenerPaqueteSocio(txDocumento.Trim());

                            if (productosCarrito != null)
                            {
                                foreach (var item in productosCarrito)
                                {
                                    montoAPagar = montoAPagar + item.SubTotalNeto;
                                }
                            }

                            double montoComi = 0.0000;
                            double montoComiCI = 0.0000;
                            double puntosExtras = 0;

                            foreach (var item in productosCarrito)
                            {
                                if (item.Linea == "02") { montoComi = montoComi + item.SubTotalNeto; }
                                if (item.Linea == "02" | item.Linea == "01") { puntoPromo = puntoPromo + item.SubTotalPuntos; }
                                item.montoSend = item.SubTotalNeto / item.cantidadPeruShop;
                                DescontarStockProducto(cTienda, item.IdProdPeruShop, item.cantidadPeruShop, item.Codigo);
                            }

                            if (idTCompra == "06") { montoComiCI = (montoComi / (1 - (descuentoCI / 2))) * (descuentoCI / 2); }
                            if (idTCompra == "23") { puntosExtras += 500; }

                            DateTime send = DateTime.Now.AddHours(-2);
                            compra.IdopPeruShop = idopPeruShop;
                            compra.CodCliente = RetornoAfiliacion.IdCliente;
                            compra.Cantidad = cantidad;
                            compra.MontoTotal = (double)System.Web.HttpContext.Current.Session["SubTotal"];
                            compra.PuntosTotal = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntos"]);
                            compra.Corazones = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalCorazones"]);
                            compra.PuntosTotalPromo = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]) + puntosExtras;
                            compra.MontoAPagar = montoAPagar;
                            compra.FechaPago = send;
                            compra.Estado = 1;
                            compra.TipoPago = "04"; //PAGO EFECCTIVO
                            compra.idTipoCompra = idTCompra;
                            compra.TipoCompra = txTCompra;
                            compra.DepositoTotal = 00.00;
                            compra.DepositoFraccionado = 00.00;
                            compra.FechaDeposito = send;
                            compra.FechaStock = send;
                            compra.Despacho = cTienda;
                            compra.montoComision = montoComi;
                            compra.IdPromo = "";
                            compra.cantPromo = 0;
                            compra.cantRegalo = 0;
                            compra.puntosGastados = 0;
                            compra.montoComisionCI = montoComiCI;
                            compra.IdPago = "00000000";
                            compra.Transaction_Date = "";
                            compra.Merchant = "";
                            compra.Id_Unico = "";
                            compra.Transaction_ID = "";
                            compra.CardS = "";
                            compra.Aauthorization_Code = "";
                            compra.AMOUNT = "";
                            compra.CURRENCY = "";
                            compra.BRAND = "";
                            compra.STATUSS = "";
                            compra.ACTION_DESCRIPTION = "";
                            compra.NotaDelivery = "";
                            compra.Comprobante = txComprobante;
                            compra.Ruc = rucFactura;
                            compra.PaqueteSocio = Paquete.Substring(12, Paquete.Length - 12);
                            compra.IDP = getPeriodoRango_Comision(send);

                            string ticket = CompraNegocios.getInstance().RegistrarCompraAndDetalle(compra, productosCarrito);
                            if (ticket != "" || ticket != null)
                            {
                                int countTicket = CompraNegocios.getInstance().CantidadTicket_DPE(ticket, RetornoAfiliacion.IdCliente);
                                if (countTicket == 1)
                                {
                                    List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(3);
                                    int diasLimite = listaFecha[0].numeroDias;
                                    DateTime DayPE = DateTime.Now.AddHours(-2).AddMinutes(-2).AddSeconds(-40);
                                    DateTime DayPEExpi = DayPE.AddHours(diasLimite);
                                    string accessKey = "N2JmODAyZTlhYWFjZDQx";
                                    string idService = "8434";
                                    string secretKey = "iXK8YpOVlz8EIGo/pUHWJwtw9baNZ5jbxG3hOw+E";
                                    string date = Convert.ToString(DayPE.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
                                    string dateExpi = Convert.ToString(DayPEExpi.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
                                    decimal monto = TextoDecimal(Convert.ToString(Math.Round(montoAPagar, 2)));

                                    var token = CompraNegocios.getInstance().getTokenPE(accessKey, idService, secretKey, date);
                                    Authenticate.Data objectsSend = new Authenticate.Data()
                                    {
                                        currency = "PEN",
                                        amount = monto,
                                        transactionCode = ticket,
                                        adminEmail = "pagoefectivosnn@gmail.com",
                                        dateExpiry = dateExpi,
                                        paymentConcept = "Prueba - Validar",
                                        additionalData = "Datos adicionales",
                                        userEmail = txCorreo.Trim(),
                                        userId = RetornoAfiliacion.IdCliente,
                                        userName = txNombre.Trim(),
                                        userLastName = txApellidoPat.Trim() + " " + txApellidoMat.Trim(),
                                        userUbigeo = "",
                                        userCountry = "PERU",
                                        userDocumentType = "DNI",
                                        userDocumentNumber = txDocumento.Trim(),
                                        userCodeCountry = "+51",
                                        serviceID = 8434
                                    };
                                    TokenPE.GetObjects objectsCIP = CompraNegocios.getInstance().GetCIP(objectsSend, Convert.ToString(token));
                                    if (objectsCIP.field == "" | objectsCIP.field == null)
                                    {
                                        DatosPagoEfectivo objDatos = new DatosPagoEfectivo()
                                        {
                                            Ticket = ticket,
                                            IdCliente = RetornoAfiliacion.IdCliente,
                                            FechaCreacion = DayPE,
                                            FechaExpiracion = DayPEExpi,
                                            CIP = Convert.ToString(objectsCIP.cip),
                                            Monto = montoAPagar,
                                            CodeStatus = objectsCIP.code
                                        };
                                        CompraNegocios.getInstance().RegistroDatosPagoEfectivo(objDatos);

                                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                                        {
                                            Mensaje = "OK",
                                            URL = objectsCIP.cipURL
                                        };
                                        LimpiarSession();
                                        return RetornarDatos;
                                    }
                                    else
                                    {
                                        bool registro = ClienteLN.getInstance().RegistroModificaciones(System.Web.HttpContext.Current.Session["NumDocCliente"].ToString(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                                       "Error Sistema", objectsCIP.message);
                                        RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                                        {
                                            Mensaje = "Ocurrió un error inesperado, porfavor vuelva a realizar la compra"
                                        };
                                        ClienteLN.getInstance().Eliminarcliente_DPE(RetornoAfiliacion.IdCliente);
                                        LimpiarSession();
                                        return RetornarDatos;
                                    }
                                }
                                else
                                {
                                    ClienteLN.getInstance().Eliminarcliente_DPE(RetornoAfiliacion.IdCliente);
                                    RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                                    {
                                        Mensaje = "Ocurrió un error inesperado, porfavor vuelva a dar click en Comprar"
                                    };
                                    return RetornarDatos;
                                }
                            }
                            else
                            {
                                ClienteLN.getInstance().Eliminarcliente_DPE(RetornoAfiliacion.IdCliente);
                                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                                {
                                    Mensaje = "Ocurrió un error inesperado, porfavor vuelva a dar click en Comprar"
                                };
                                return RetornarDatos;
                            }
                        }
                        else
                        {
                            if (RetornoAfiliacion.IdCliente != null) { ClienteLN.getInstance().Eliminarcliente_DPE(RetornoAfiliacion.IdCliente); }
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = RetornoAfiliacion.Mensaje
                            };
                            return RetornarDatos;
                        }
                    }
                    else
                    {
                        string productosSinStock = "";

                        foreach (var item in responseStock.Productos)
                        {
                            productosSinStock = productosSinStock + "<br />" + item;
                        }
                        string establecimientoSinStock = responseStock.Establecimiento;
                        string horaRestante = HorasRestantes(idProdPeruShop);

                        if (horaRestante == "0")
                        {
                            if (Convert.ToDouble(cantidadExisteM) < 0)
                            {
                                cantidadExisteM = "0";
                            }

                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock + ". Actualmente contamos con " + cantidadExisteM + ""
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock
                            };
                            return RetornarDatos;

                        }
                    }
                }
            }
        }

        private static void LimpiarSession()
        {
            ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]).Clear();
            ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]).Clear();
            System.Web.HttpContext.Current.Session["SubTotalCorazones"] = 0.00;
            System.Web.HttpContext.Current.Session["SubTotal"] = 0.00;
            System.Web.HttpContext.Current.Session["SubTotalPuntos"] = 0.00;
            System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = 0.00;
            System.Web.HttpContext.Current.Session["MontoAPagar"] = 0.00;
            System.Web.HttpContext.Current.Session["EvalDelivery"] = 0;
            System.Web.HttpContext.Current.Session["NumRUCOP"] = "";
            System.Web.HttpContext.Current.Session["CBOPreRegistro"] = "";

            //Limpiar session combo
            System.Web.HttpContext.Current.Session["NombreDelivery"] = "";
            System.Web.HttpContext.Current.Session["DNIDelivery"] = "";
            System.Web.HttpContext.Current.Session["CelularDelivery"] = "";
            System.Web.HttpContext.Current.Session["DireccionDelivery"] = "";
            System.Web.HttpContext.Current.Session["TransporteDelivery"] = "";
            System.Web.HttpContext.Current.Session["DirecTransporteDelivery"] = "";
            System.Web.HttpContext.Current.Session["ProvinciaDelivery"] = "";
            System.Web.HttpContext.Current.Session["DirecProvinciaDelivery"] = "";

            System.Web.HttpContext.Current.Session["sTipoCompraSelect"] = "";
            System.Web.HttpContext.Current.Session["sTipoEntregaSelect"] = "0";
            System.Web.HttpContext.Current.Session["sMedioPagoSelect"] = "0";
            System.Web.HttpContext.Current.Session["comboDepartSelect"] = "0";
            System.Web.HttpContext.Current.Session["comboProvinciaSelect"] = "0";
            System.Web.HttpContext.Current.Session["comboDistritoSelect"] = "0";
            System.Web.HttpContext.Current.Session["comboTiendaSelect"] = "";
            System.Web.HttpContext.Current.Session["PromoAplicada"] = 0;
            System.Web.HttpContext.Current.Session["VamosCompra"] = 0;
            System.Web.HttpContext.Current.Session["EliminarPromo"] = 0;
            System.Web.HttpContext.Current.Session["TiendaRetorna"] = 0;
            System.Web.HttpContext.Current.Session["ActualizaPag"] = 0;
            System.Web.HttpContext.Current.Session["codProducto"] = "";
            System.Web.HttpContext.Current.Session["codProducto2"] = "";
            System.Web.HttpContext.Current.Session["codProducto3"] = "";
            System.Web.HttpContext.Current.Session["codProducto4"] = "";
            System.Web.HttpContext.Current.Session["codProdBasico"] = "";
            System.Web.HttpContext.Current.Session["codProdProfe"] = "";
            System.Web.HttpContext.Current.Session["codProdEmpre"] = "";
            System.Web.HttpContext.Current.Session["codProdMillo"] = "";
            System.Web.HttpContext.Current.Session["codProdBasico2"] = "";
            System.Web.HttpContext.Current.Session["codProdProfe2"] = "";
            System.Web.HttpContext.Current.Session["codProdEmpre2"] = "";
            System.Web.HttpContext.Current.Session["codProdMillo2"] = "";
            System.Web.HttpContext.Current.Session["TipoPago"] = "";
            System.Web.HttpContext.Current.Session["tipoCompPromo"] = "";
            System.Web.HttpContext.Current.Session["TipoAfiliacion"] = "";
            System.Web.HttpContext.Current.Session["cantRegalo"] = 0;
            System.Web.HttpContext.Current.Session["cboActivar"] = "0";
            System.Web.HttpContext.Current.Session["cboComprobante"] = "0";
            System.Web.HttpContext.Current.Session["cboTitular"] = "0";
            System.Web.HttpContext.Current.Session["cboPremio"] = "";
            System.Web.HttpContext.Current.Session["RecargaPagina"] = 0;
            System.Web.HttpContext.Current.Session["descuentoCI"] = 0;
            System.Web.HttpContext.Current.Session["montoMomentaneo"] = 0.0;
            System.Web.HttpContext.Current.Session["tokenVISA"] = "";
            System.Web.HttpContext.Current.Session["SessiontokenVISA"] = "";
            System.Web.HttpContext.Current.Session["IdPagoCliente"] = "00000000";
            System.Web.HttpContext.Current.Session["Transaction_Date"] = "";
            System.Web.HttpContext.Current.Session["Merchant"] = "";
            System.Web.HttpContext.Current.Session["Id_Unico"] = "";
            System.Web.HttpContext.Current.Session["CardS"] = "";
            System.Web.HttpContext.Current.Session["Aauthorization_Code"] = "";
            System.Web.HttpContext.Current.Session["EliminaProductoTienda"] = 0;
            System.Web.HttpContext.Current.Session["EliminaProductoxCDR"] = 0;
            System.Web.HttpContext.Current.Session["mostrarCompraTerminada"] = 1;
            System.Web.HttpContext.Current.Session["ValidarRUC"] = 0;
        }

        private static int getPeriodoRango_Comision(DateTime fechaSimple)
        {
            int i2 = 0;
            bool ok = false;
            int IDPeriodo = 0;
            DateTime primerDiafechaActual = DateTime.Now;
            DateTime ultimoDiafechaActual = DateTime.Now;
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            while (!ok && i2 < ListaPeriodos.Count())
            {
                primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                if (fechaSimple >= primerDiafechaActual & fechaSimple < ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodoComision;
                }
                i2++;
            }
            int IdPeriodoRango_Comi = PeriodoLN.getInstance().ObtenerIdperiodoRango_Comi(IDPeriodo);
            return IdPeriodoRango_Comi;
        }

        private static string HorasRestantes(string IDPPS)
        {

            string respuesta = "";
            string tipoPago = System.Web.HttpContext.Current.Session["TipoPago"].ToString();
            int idLimite = (tipoPago == "04") ? 2 : 1;
            List<HoraCantidad> ListaHC = CompraNegocios.getInstance().ObtenerHoraCompraStock(IDPPS);
            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(idLimite);
            int horasLimite = listaFecha[0].numeroDias;
            if (ListaHC[0].fecha != "0")
            {
                string cantidadTotal = CompraNegocios.getInstance().cantidadTotalStock(IDPPS);
                DateTime fechaCompra = Convert.ToDateTime(ListaHC[0].fecha);
                DateTime fechaLimite = fechaCompra.AddHours(horasLimite);
                DateTime ahora = DateTime.Now.AddHours(-2);
                TimeSpan resta = fechaLimite - ahora;
                if (resta.Minutes < 0)
                {
                    respuesta = "0";
                }
                else
                {
                    respuesta = "Stock total separado (" + cantidadTotal + " unds). En caso de no finalizarse la compra, en " + Convert.ToString(resta.Hours) + "h. y " + Convert.ToString(resta.Minutes) + " m. se podrán habilitar " + ListaHC[0].cantidad + " unds";
                }

            }
            else
            {
                respuesta = "0";
            }

            return respuesta;
        }

        private static decimal TextoDecimal(string numero)
        {
            var clone = (CultureInfo)CultureInfo.InvariantCulture.Clone();
            clone.NumberFormat.NumberDecimalSeparator = ",";
            clone.NumberFormat.NumberGroupSeparator = ".";
            decimal retorno = decimal.Parse(numero, clone);
            return retorno;
        }

        [WebMethod]
        public static ProductoV2.RetornarDatosInicio ObtenerRUC_Perfil()
        {
            ProductoV2.RetornarDatosInicio Retornar = null;
            Retornar = new ProductoV2.RetornarDatosInicio()
            {
                RUC = System.Web.HttpContext.Current.Session["RUCSocio"].ToString(),
                montoTotalPagar = Convert.ToDouble(System.Web.HttpContext.Current.Session["MontoAPagar"]),
                puntosTotal = Convert.ToDouble(System.Web.HttpContext.Current.Session["SubTotalPuntos"]),
                PreRegistro = Convert.ToInt32(System.Web.HttpContext.Current.Session["PreRegistro"])
            };
            return Retornar;
        }

        // importante
        private static bool ValidarPromocion()
        {
            bool ok = false;
            bool recorrido = false;
            int i = 0;
            List<string> Paquetes = new List<string>();
            List<PaqueteNatura> listaProductosPaquete = null;
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            foreach (var item in productosCarritoPaquete)
            {
                if (item.idPaquete != "0")
                {
                    Paquetes.Add(item.idPaquete);
                }
            }
            if (Paquetes.Count() == 0)
            {
                ok = true;
            }
            else
            {
                while (!recorrido && i < Paquetes.Count())
                {
                    int con = 0;
                    listaProductosPaquete = PaqueteLN.getInstance().ListaPaqueteByCodigoPeru(Paquetes[i]);
                    foreach (var prodPaq in listaProductosPaquete)
                    {
                        if (productosCarrito.Count(prom => prom.CodigoValidar.Contains(prodPaq.idProductoGeneral.Trim())) > 0)
                        {
                            con++;
                        }
                    }
                    if (con != listaProductosPaquete.Count())
                    {
                        recorrido = true;
                    }
                    i++;
                }
                ok = (recorrido == true) ? false : true;
            }
            return ok;
        }

        private static bool ValidacionNumero(string num)
        {
            try
            {
                Int32.Parse(num);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private static Cliente.RetornarDatosAfiliacion RegistrarAfiliacion(string txDocumento, string txFechaNac, string txCDRPreferido, string txCDRPremio, string txUsuario,
                                                                           string txClave, string txNombre, string txApellidoPat, string txApellidoMat, string txSexo, string txTDoc,
                                                                           string txCorreo, string txTelefono, string txCelular, string cPais, string cDepartamento, string cProvincia,
                                                                           string cDistrito, string txDireccion, string txReferencia, string txDetraccion, string txRUC, string txBanco,
                                                                           string txNumCuenta, string txInterbancaria, string cTCliente, string txUpline, string cTCompra)
        {

            Cliente.RetornarDatosAfiliacion RetornarDatos = null;
            //DateTime fechaNacimiento = DateTime.ParseExact(txFechaNac, @"dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime fechaNacimiento = Convert.ToDateTime(txFechaNac, CultureInfo.InvariantCulture);
            int cantDocumento = ClienteLN.getInstance().ValidarCantidadDocumento(txDocumento.Trim());
            string patrocinador = (string)System.Web.HttpContext.Current.Session["NumDocCliente"], idClienteAfiliacion = "";

            if (cantDocumento > 0)
            {
                RetornarDatos = new Cliente.RetornarDatosAfiliacion()
                {
                    Mensaje = "El DNI ya existe",
                    Estado = false
                };
                return RetornarDatos;
            }
            else if (fechaNacimiento > DateTime.Now)
            {
                RetornarDatos = new Cliente.RetornarDatosAfiliacion()
                {
                    Mensaje = "Tiene que escoger una fecha no mayor a la actual",
                    Estado = false
                };
                return RetornarDatos;
            }
            else if (fechaNacimiento.AddYears(18) >= DateTime.Now)
            {
                RetornarDatos = new Cliente.RetornarDatosAfiliacion()
                {
                    Mensaje = "Tiene que ser mayor de edad",
                    Estado = false
                };
                return RetornarDatos;
            }
            else
            {
                List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDRStock();
                List<Cliente> estab = new List<Cliente>();
                estab = ClienteLN.getInstance().ListaEstable();
                string establecimiento = "";
                string cdrPremio = (from c in ListaCDR where c.CDRPS.Trim() == txCDRPremio.Trim() select c.DNICDR.Trim()).FirstOrDefault();
                if (txCDRPreferido == "0" | txCDRPreferido == "" | txCDRPreferido == "SELECCIONE")
                {
                    establecimiento = "";
                }
                else
                {

                    establecimiento = (from c in ListaCDR where c.CDRPS.Trim() == txCDRPreferido.Trim() select c.DNICDR.Trim()).FirstOrDefault();
                }

                Cliente objCliente = new Cliente();
                objCliente.usuario = txUsuario.Trim();
                objCliente.clave = txClave.Trim();
                objCliente.nombre = txNombre.ToUpper().Trim();
                objCliente.apellidoPat = txApellidoPat.ToUpper().Trim();
                objCliente.apellidoMat = txApellidoMat.ToUpper().Trim();
                objCliente.apodo = "";
                objCliente.fechaNac = txFechaNac;
                objCliente.sexo = txSexo;
                objCliente.tipoDoc = txTDoc;
                objCliente.numeroDoc = txDocumento.Trim();
                objCliente.correo = txCorreo.Trim();
                objCliente.telefono = txTelefono;
                objCliente.celular = txCelular.Trim();
                objCliente.pais = cPais;
                objCliente.departamento = cDepartamento;
                objCliente.provincia = cProvincia;
                objCliente.ditrito = cDistrito;
                objCliente.direccion = txDireccion.ToUpper().Trim();
                objCliente.referencia = txReferencia.ToUpper().Trim();
                objCliente.nroCtaDetraccion = txDetraccion.Trim();
                objCliente.ruc = txRUC.Trim();
                objCliente.nombreBanco = txBanco.ToUpper().Trim();
                objCliente.nroCtaDeposito = txNumCuenta.Trim();
                objCliente.nroCtaInterbancaria = txInterbancaria.Trim();
                objCliente.tipoCliente = cTCliente;
                objCliente.IdPeruShop = "";
                objCliente.CodigoPostal = "";
                objCliente.PreRegistro = 0;
                objCliente.patrocinador = patrocinador;
                objCliente.upline = txUpline;
                objCliente.tipoEstablecimiento = establecimiento;
                objCliente.CDRPremio = cdrPremio;
                objCliente.factorComision = (txUpline == "" && (cTCliente == "03" || cTCliente == "05")) ? patrocinador : txUpline;
                objCliente.imagen = "";
                objCliente.FechaRegistro = DateTime.Now.AddHours(-2).AddMinutes(-32).ToString("dd/MM/yyyy");
                objCliente.paisTienda = "01";

                try
                {
                    Descuento descuento = new Descuento();
                    descuento.IdPackete = cTCompra;
                    descuento.FechaObtencionPackete = Convert.ToString(DateTime.Now);

                    idClienteAfiliacion = ClienteLN.getInstance().RegistrarAfiliacion(objCliente, descuento);
                    List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarIdPeriodo();
                    List<Periodo> ListaPeriodosComision = PeriodoLN.getInstance().ListarIdPeriodoComision();

                    foreach (var item in ListaPeriodos)
                    {
                        bool ok2 = PeriodoLN.getInstance().RegistroClientePeriodoMultiple(txDocumento, objCliente.factorComision, item.idPeriodo, objCliente.tipoCliente, objCliente.patrocinador);
                    }
                    foreach (var item in ListaPeriodosComision)
                    {
                        bool ok2 = PeriodoLN.getInstance().RegistroClientePeriodoMultipleComision(txDocumento, objCliente.factorComision, patrocinador,
                                                                                                  item.idPeriodoComision, objCliente.tipoCliente);
                        bool retencion = PeriodoLN.getInstance().RegistroSocioRetencionXPeriodo(txDocumento, item.idPeriodoComision);
                    }

                    RetornarDatos = new Cliente.RetornarDatosAfiliacion()
                    {
                        Estado = true,
                        IdCliente = idClienteAfiliacion
                    };
                    return RetornarDatos;
                }
                catch (Exception ex)
                {
                    RetornarDatos = new Cliente.RetornarDatosAfiliacion()
                    {
                        Mensaje = "Ocurrió un error en el registro del socio, porfavor vuelva a dar click en comprar",
                        Estado = false,
                        IdCliente = idClienteAfiliacion
                    };
                    return RetornarDatos;
                }
            }
        }

        //public static bool IsPhoneNumber(string number)
        //{
        //    return Regex.Match(number, @"^[0-9]{9}$").Success;
        //}
    }
}