﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PaginaError.aspx.cs" Inherits="SantaNaturaNetworkV3.PaginaError" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Santa Natura</title>
    <link rel="icon" href="img/Natural_Food_icon.png" type="image/x-icon" />
    <link href="css/proyecto2/fonts-v2.css" rel="stylesheet" type="text/css" />
    <link href="css/proyecto2/estilosPaginaError.css" rel="stylesheet" />
</head>
<body>
<%--    <form id="form1" runat="server">
        <div>
        </div>
    </form>--%>
    <div>
        <h1 class="claseFuente5">ESTAMOS REALIZANDO ACTUALIZACIONES <br />
            PARA MEJORAR<br />
            SU EXPERIENCIA.<br />
            POR FAVOR ACTUALICE <br />
            LA PÁGINA EN<br />
            UNOS MINUTOS.
        </h1>
        <div style="display: flex; justify-content: center">
            <img id="imgError" src="img/imgError.png" style="width: 50%"/>
        </div>
    </div>
</body>
</html>
