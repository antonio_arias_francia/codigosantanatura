﻿using Modelos;
using Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class _Default : Page
    {

        public List<Producto> listaProductos = new List<Producto>();
        public List<Producto> listaProductos2 = new List<Producto>();

        protected void Page_Load(object sender, EventArgs e)
        {
            //txtPruebita.Text = Session["Nombre"].ToString();
            if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("Principal.aspx");
            }
            CargarProductosMasVendidoByPais();
            CargarProductosMenosVendidoByPais();
            Response.Redirect("Index.aspx");
        }

        void CargarProductosMasVendidoByPais()
        {
            listaProductos = ProductoNegocios.getInstance().ListarProductosMasVendidos("Peru");
        }

        void CargarProductosMenosVendidoByPais()
        {
            listaProductos2 = ProductoLN.getInstance().ListaMenosVendidos("Peru");
        }
    }
}