﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;

namespace SantaNaturaNetworkV3
{
    public partial class GestionarBanner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<ArchivosPDF.ImagenesBanners> ListarImagenesBanner()
        {
            List<ArchivosPDF.ImagenesBanners> Lista = ArchivosLN.getInstance().ListarImagenesBanners();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistroImagenesBanner(string nombreS,string archivoS)
        {
            try
            {
                ArchivosLN.getInstance().RegistroImagenBanner(nombreS, archivoS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        [WebMethod]
        public static bool EliminarArchivo(string imagen)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/banners/" + "");
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static bool ActualizarImagenesBanner(string nombreS, string archivoS, int idS)
        {
            try
            {
                ArchivosLN.getInstance().ActualizarImagenBanner(nombreS, archivoS, idS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        [WebMethod]
        public static bool EliminarImagenesBanner(int idS)
        {
            try
            {
                ArchivosLN.getInstance().EliminarImagenBanner(idS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }
    }
}