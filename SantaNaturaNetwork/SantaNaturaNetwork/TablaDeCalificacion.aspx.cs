﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class TablaDeCalificacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IdCliente"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "" | Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
            {
                Response.Redirect("Index.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("Principal.aspx");
            }
            Session["mostrarCompraTerminada"] = 0;

            //Validamos Caducidad de Clave
            string idCliente = Convert.ToString(Session["IdCliente"]);
            string result = "";

            result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


            if (result == "Clave Vencida")
            {
                Session["clave_vencida"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }

            //Validamos si ya agregó sus preguntas de Seguridad
            result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
            if (result == "No Existe")
            {
                Session["preguntas_seguridad"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaCalificacion()
        {
            List<PuntosComisiones> listaCalificacion = null;
            string dni = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);

            try
            {
                listaCalificacion = PuntosNegocios.getInstance().ListaCalificacion(dni.Trim());
            }
            catch (Exception ex)
            {
                listaCalificacion = null;
            }
            return listaCalificacion;
        }

    }
}