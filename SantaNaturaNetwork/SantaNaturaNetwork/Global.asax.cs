﻿using Modelos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace SantaNaturaNetwork
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Código que se ejecuta al iniciar la aplicación
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        void Session_Start()
        {
            //Datos importantes para comprar
            Session["CBOPreRegistro"] = "";
            Session["cerrarSession"] = "SI";
            Session["NombreDelivery"] = "";
            Session["DNIDelivery"] = "";
            Session["CelularDelivery"] = "";
            Session["DireccionDelivery"] = "";
            Session["TransporteDelivery"] = "";
            Session["DirecTransporteDelivery"] = "";
            Session["ProvinciaDelivery"] = "";
            Session["DirecProvinciaDelivery"] = "";

            Session["EvalDelivery"] = 0;
            Session["NumDocCliente"] = "";
            Session["IdSimplex"] = "";
            Session["NumDocClienteRegistrador"] = "";
            Session["TipoCliente"] = "";
            Session["PacketeSocio"] = "";
            Session["PatrocinadorPackete"] = "";
            Session["Establecimiento"] = "";
            Session["TipoPago"] = "";
            Session["FotoCliente"] = "";
            Session["IdCliente"] = "";
            Session["RUCSocio"] = "";
            Session["NumRUCOP"] = "";
            Session["PatrocinadorDNI"] = "";
            Session["UplineDNI"] = "";
            Session["NombrePatrocinador"] = "";
            Session["MedioPagoSend"] = "";
            Session["TiendaAyuda"] = null;
            Session["PromoAplicada"] = 0;
            Session["VamosCompra"] = 0;
            Session["EliminarPromo"] = 0;
            Session["TiendaRetorna"] = 0;
            Session["ActualizaPag"] = 0;
            Session["codProducto"] = "";
            Session["codProducto2"] = "";
            Session["codProducto3"] = "";
            Session["codProducto4"] = "";
            Session["codProdBasico"] = "";
            Session["codProdProfe"] = "";
            Session["codProdEmpre"] = "";
            Session["codProdMillo"] = "";
            Session["codProdBasico2"] = "";
            Session["codProdProfe2"] = "";
            Session["codProdEmpre2"] = "";
            Session["codProdMillo2"] = "";
            Session["tipoCompPromo"] = "";
            Session["TipoAfiliacion"] = "";
            Session["cantRegalo"] = 0;
            Session["cboActivar"] = "0";
            Session["cboComprobante"] = "0";
            Session["cboTitular"] = "0";
            Session["RecargaPagina"] = 0;
            Session["factorComision"] = "";
            Session["descuentoCI"] = 0;
            Session["montoMomentaneo"] = 0.0;
            Session["modiMonto"] = false;
            Session["ok"] = false;
            Session["ok2"] = false;
            Session["Transaction_Date"] = "";
            Session["Merchant"] = "";
            Session["Id_Unico"] = "";
            Session["Transaction_ID"] = "";
            Session["CardS"] = "";
            Session["Aauthorization_Code"] = "";
            Session["AMOUNT"] = "";
            Session["CURRENCY"] = "";
            Session["BRAND"] = "";
            Session["STATUSS"] = "";
            Session["ACTION_DESCRIPTION"] = "";
            Session["Nombre_Completo"] = "";
            Session["DireccionSocio"] = "";
            Session["PaginaAnterior"] = "";
            Session["ProcesoVisa"] = 0;
            Session["ModalSesion"] = 0;
            Session["ModalIndex"] = 0;
            Session["EliminaProductoTienda"] = 0;
            Session["EliminaProductoxCDR"] = 0;
            Session["PPSOCIO"] = 0;


            //
            Session["FechaNacimientoInicial"] = "";
            Session["txtUsuario"] = "";
            Session["txtContra"] = "";
            Session["txtNombre"] = "";
            Session["txtApePat"] = "";
            Session["txtApeMat"] = "";
            Session["cboSexo"] = "";
            Session["cboTipoDoc"] = "";
            Session["txtDocumento"] = "";
            Session["txtCorreo"] = "";
            Session["txtTelefono"] = "";
            Session["txtCelular"] = "";
            Session["cboPais"] = "";
            Session["cboDepartamento"] = "";
            Session["cboProvincia"] = "";
            Session["cboDistrito"] = "";
            Session["txtDireccion"] = "";
            Session["txtReferencia"] = "";
            Session["txtCuentaTransa"] = "";
            Session["txtRuc"] = "";
            Session["txtBanco"] = "";
            Session["txtDeposito"] = "";
            Session["txtInterbancaria"] = "";
            Session["cboTipoCliente"] = "";
            Session["cboUpline"] = "";
            Session["cboEstablecimiento"] = "";
            Session["cboPremio"] = "";
            Session["cboTipoCompra"] = "";
            Session["mostrarCompraTerminada"] = 0;
            Session["Apodo"] = "";
            Session["NombrePS"] = "";
            Session["ValidarRUC"] = 0;
            

            //

            Session["Pais"] = "";
            Session["Departamento"] = "";
            Session["Provincia"] = "";
            Session["Distrito"] = "";
            Session["FechaNac"] = "";
            Session["IdPagoCliente"] = "00000000";

            //
            Session["SubTotalCorazones"] = 0.00;
            Session["SubTotalPuntos"] = 0.00;
            Session["SubTotalPuntosPromocion"] = 0.00;
            Session["SubTotal"] = 0.00;
            Session["MontoAPagar"] = 0.00;

            Session["Foto"] = "";
            Session["IdProducto"] = "";
            Session["Producto"] = "";
            Session["PrecioUnitario"] = 0.00;
            Session["Cantidad"] = 0;

            Session["Ayuda"] = "NO";

            //Combo ayuda
            Session["sTipoCompraSelect"] = "";
            Session["sTipoEntregaSelect"] = "0";
            Session["sMedioPagoSelect"] = "0";
            Session["comboDepartSelect"] = "0";
            Session["comboProvinciaSelect"] = "0";
            Session["comboDistritoSelect"] = "0";
            Session["comboTiendaSelect"] = "";

            List<ProductoCarrito> carritoProductos = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            List<SendProductsPeruShop> sendProducts = new List<SendProductsPeruShop>();
            List<Packete> listPacketes = new List<Packete>();
            Cliente objsocioG = new Cliente();

            Session["CarritoProducto"] = carritoProductos;
            Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
            Session["SendProductSession"] = sendProducts;
            Session["ListPacketes"] = listPacketes;
            Session["ObjCliente"] = objsocioG;

            //DatosClienteFormulario
            Session["Nombre"] = "";


            //Login
            Session["isLogueado"] = "NO";
        }
    }
}