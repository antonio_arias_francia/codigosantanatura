﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class atc : System.Web.UI.Page
    {
        public string Nombre_Completo;
        public string Documento;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["mostrarCompraTerminada"] = 0;
            if (!Page.IsPostBack)
            {
                if (Session["IdCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "" | Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
            Documento = Convert.ToString(Session["NumDocCliente"]);
            Nombre_Completo = Convert.ToString(Session["Nombre_Completo"]);
        }

        [WebMethod]
        public static bool EnviarCorreo(string nombreS, string documentoS, string telefonoS, string emailS, 
                                        string empresarioS, string temaS, string temaEspecificoS, string detalleS,
                                        List<string> correoS)
        {
            String merror;
            string correo = "";
            
            MailMessage mail = new MailMessage();
            //mail.To.Add(new MailAddress(correo));
            foreach (var item in correoS)
            {
                mail.To.Add(new MailAddress(item));
            }
            mail.From = new MailAddress("atc@mundosantanatura.com", "Santa Natura ATC", System.Text.Encoding.UTF8);
            mail.Subject = nombreS+" - "+empresarioS+" - "+temaEspecificoS;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = string.Format("Nombres: {1}{0}Documento: {2}{0}Telefono: {3}{0}Email: {4}{0}Tipo de empresario: {5}{0}Tema general: {6}{0}" +
                                      "Tema específico: {7}{0}Detalle: {8}{0}",
                                       Environment.NewLine, nombreS, documentoS, telefonoS, emailS, empresarioS, temaS, temaEspecificoS, detalleS);
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
            smtp.Credentials = new System.Net.NetworkCredential("atc@mundosantanatura.com", "s@nt@2019");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                smtp.Send(mail);
                return true;
            }
            catch (SmtpException ex)
            {
                bool registro = ClienteLN.getInstance().RegistroModificaciones("73011583", DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Error Sistema", Convert.ToString(ex));
                merror = ex.Message.ToString();
                return false;
            }

        }
    }
}