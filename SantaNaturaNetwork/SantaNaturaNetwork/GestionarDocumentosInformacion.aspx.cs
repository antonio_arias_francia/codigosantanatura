﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;

namespace SantaNaturaNetworkV3
{
    public partial class GestionarDocumentosInformacion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<ArchivosPDF.DocumentosInformacion> ListarDocumentosInformacion()
        {
            List<ArchivosPDF.DocumentosInformacion> Lista = ArchivosLN.getInstance().ListarDocumentosInformacion();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistroDocumentosInformacion(string nombreS, string archivoS, string tipoArchivoS, string codigoS)
        {
            try
            {
                ArchivosLN.getInstance().RegistroDocumentosInformacion(nombreS, archivoS, tipoArchivoS, codigoS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        [WebMethod]
        public static bool EliminarArchivo(string archivo)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/documentos/" + archivo);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static bool ActualizarDocumentosInformacion(string nombreS, string archivoS, string tipoArchivoS, string codigoS, int IdS)
        {
            try
            {
                ArchivosLN.getInstance().ActualizarDocumentosInformacion(nombreS, archivoS, tipoArchivoS, codigoS, IdS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        [WebMethod]
        public static bool EliminarDocumentosInformacion(int idS, string archivoS)
        {
            try
            {
                var filePath = HttpContext.Current.Server.MapPath("~/documentos/" + archivoS);
                File.Delete(filePath);
                ArchivosLN.getInstance().EliminarDocumentosInformacion(idS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }
    }
}