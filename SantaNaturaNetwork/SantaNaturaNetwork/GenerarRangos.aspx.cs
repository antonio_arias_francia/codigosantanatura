﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Net.Mail;

namespace SantaNaturaNetworkV3
{
    public partial class GenerarRangos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static string ActualizarCantidadRetencion(string prueba)
        {
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            List<Cliente> ListaCliente = ClienteLN.getInstance().ListarClienteRetencion(IDP);

            foreach (var item in ListaCliente)
            {
                List<DatosSocios> Lista = ClienteLN.getInstance().ListarCantidadDirectos(item.numeroDoc, IDP);
                bool updateRetencion = ClienteLN.getInstance().ActualizarRetencionSocio(Lista[0].CantSocios, Lista[0].CantSociosDirectos, Lista[0].NuevoSocios,
                                                                                        Lista[0].CantCON, Lista[0].CantCONDirectos, Lista[0].NuevoCON,
                                                                                        Lista[0].CantCI, Lista[0].CantCIDirectos, Lista[0].NuevoCI,
                                                                                        Lista[0].CantSociosRed, Lista[0].CantSociosRedActivos, Lista[0].CantNuevosSociosRed,
                                                                                        Lista[0].CantCONRed, Lista[0].CantCONRedActivos, Lista[0].CantNuevosCONRed,
                                                                                        Lista[0].CantCIRed, Lista[0].CantCIRedActivos, Lista[0].CantNuevosCIRed, item.numeroDoc, IDP);
            }

            return "2xx";
        }

        [WebMethod]
        public static List<Cliente> ListarClienteRango()
        {
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarClientexRango();

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static bool GeneraRangoCompleto2(int IDP)
        {
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteCalculoPuntosPeriodo(IDP);
            List<PuntosComisiones> ListaDirectos = null;
            List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();

            foreach (var item in Lista)
            {
                int canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectosPeriodo(item.numeroDoc, IDP);
                if (canPPDirectos <= 1)
                {
                    if (item.PP < 20)
                    {
                        bool update = PuntosNegocios.getInstance().ActualizarEstadoByPeriodo(item.numeroDoc, "NO ACTIVO", IDP, 0);
                    }
                    else
                    {
                        bool update = PuntosNegocios.getInstance().ActualizarEstadoByPeriodo(item.numeroDoc, "EMPRENDEDOR", IDP, 1);
                    }
                }
                else
                {

                }
                //int canPPDirectos = 0;
                //double VPRSocio = item.VPR;
                //double PP = item.PP;
                //double VP = item.VP;
                //string RangoDefinido = "";
                //int IDRangoDefinido = 1;
                //int i = 0;
                //bool ok = false;
                //ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPRPeriodo(item.numeroDoc, IDP);
                //canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectosPeriodo(item.numeroDoc, IDP);

                //if (PP >= 20)
                //{

                //    if (canPPDirectos >= 2)
                //    {

                //        while (!ok && i < ListaRangos.Count())
                //        {
                //            int CS1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                //                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                //                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                //                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                //                                    (i == 12) ? 2 : 0;

                //            if (ListaDirectos.Count() >= CS1)
                //            {
                //                ok = ObtenerRango(VPRSocio, ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP, i, IDP, item.numeroDoc);
                //                if (ok == true) { RangoDefinido = ListaRangos[i].Nombre; IDRangoDefinido = ListaRangos[i].IdRango; }
                //            }
                //            i++;
                //        }
                //    }
                //    else { RangoDefinido = "EMPRENDEDOR"; IDRangoDefinido = 1; }
                //}
                //else { RangoDefinido = "NO ACTIVO"; IDRangoDefinido = 0; }

                //bool update = PuntosNegocios.getInstance().ActualizarEstadoByPeriodo(item.numeroDoc, RangoDefinido, IDP, IDRangoDefinido);
            }


            return true;
        }

        [WebMethod]
        public static bool GeneraRangoCompleto(int IDP)
        {
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteCalculoPuntosPeriodo(IDP);
            List<PuntosComisiones> ListaDirectos = null;
            List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();

            int ccc = 0;
            foreach (var item in Lista)
            {
                ccc++;
                int canPPDirectos = 0;
                double VPRSocio = item.VPR;
                double PP = item.PP;
                double VP = item.VP;
                string RangoDefinido = "";
                int IDRangoDefinido = 1;
                int i = 0;
                bool ok = false;
                ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPRPeriodo(item.numeroDoc, IDP);
                canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectosPeriodo(item.numeroDoc, IDP);

                if (PP >= 20)
                {

                    if (canPPDirectos >= 2)
                    {

                        while (!ok && i < ListaRangos.Count())
                        {
                            int CS1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                                                    (i == 12) ? 2 : 0;

                            if (ListaDirectos.Count() >= CS1)
                            {
                                ok = ObtenerRango(VPRSocio, ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP, i, IDP, item.numeroDoc);
                                if (ok == true) { RangoDefinido = ListaRangos[i].Nombre; IDRangoDefinido = ListaRangos[i].IdRango; }
                            }
                            i++;
                        }
                    }
                    else { RangoDefinido = "EMPRENDEDOR"; IDRangoDefinido = 1; }
                }
                else { RangoDefinido = "NO ACTIVO"; IDRangoDefinido = 0; }

                bool update = PuntosNegocios.getInstance().ActualizarEstadoByPeriodo(item.numeroDoc, RangoDefinido, IDP, IDRangoDefinido);
            }


            return true;
        }

        private static bool ObtenerRango(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos, double VP, int i, int IDP, string documento)
        {

            bool Rango = false;
            double VRVP = 0;

            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VP = (VP >= PML) ? PML : VP;
                VRVP = VRA + VP;
                if (VRVP >= PR) { Rango = true; }
            }

            if (Rango == true)
            {
                int CR1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                                                    (i == 12) ? 2 : 0;
                int CR2 = (i == 5) ? 1 : 0;
                Rango = ValidarLineasCalificadas(i, CR1, CR2, Directos, IDP);
            }
            if (Rango == true)
            {
                bool AVQ = PuntosNegocios.getInstance().ActualizarVQPeriodo(documento, VRVP, IDP);
            }

            return Rango;
        }

        private static bool ValidarLineasCalificadas(int posicion, int CS1, int CS2, List<PuntosComisiones> Directos, int IDP)
        {
            int sumaCS1 = 0, sumaCS2 = 0;
            bool resultado = false;
            List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRango = null;

            foreach (var item in Directos)
            {
                ListaUplinesRango = PuntosNegocios.getInstance().ListaUplinesRangoPeriodo(item.dni, IDP);
                if (ListaUplinesRango.Count() > 0)
                {
                    if (posicion == 0 && ListaUplinesRango[0].canSuperiorIM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 1 && ListaUplinesRango[0].canSuperiorTDM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 2 && ListaUplinesRango[0].canSuperiorDDM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 3 && ListaUplinesRango[0].canSuperiorDM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 4 && ListaUplinesRango[0].canSuperiorTD >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 5 && (ListaUplinesRango[0].canSuperiorTD >= 1 || ListaUplinesRango[0].canSuperiorDD >= 1))
                    {
                        if (ListaUplinesRango[0].canSuperiorTD >= 1) { sumaCS1 += 1; }
                        if (ListaUplinesRango[0].canSuperiorDD >= 1) { sumaCS2 += 1; }
                    }
                    else if (posicion == 6 && ListaUplinesRango[0].canSuperiorDD >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 7 && ListaUplinesRango[0].canSuperiorD >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 8 && ListaUplinesRango[0].canSuperiorR >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 9 && ListaUplinesRango[0].canSuperiorZ >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 10 && ListaUplinesRango[0].canSuperiorO >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 11 && ListaUplinesRango[0].canSuperiorP >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 12 && ListaUplinesRango[0].canSuperiorB >= 1)
                    {
                        sumaCS1 += 1;
                    }
                }
            }

            resultado = (sumaCS1 >= CS1 && sumaCS2 >= CS2) ? true : false;
            return resultado;
        }

        [WebMethod]
        public static bool GenerarComisionCompleto()
        {

            List<Cliente> Lista = ClienteLN.getInstance().ListarClientexRango();
            double sumaCI = 0;
            foreach (var item in Lista)
            {
                sumaCI = PuntosNegocios.getInstance().ComisionCI(item.numeroDoc);
                bool update = PuntosNegocios.getInstance().ActualizarComision(item.numeroDoc, sumaCI);
            }

            return true;
        }

        [WebMethod]
        public static bool RecalculoPuntajes(int IDP)
        {
            try
            {
                List<Cliente.RecalculoPuntos> Lista = ClienteLN.getInstance().ListarClienteRecalculoPuntos(IDP);
                //foreach (var item in Lista)
                //{
                //    List<PuntosComisiones> ListaRecalculo = new List<PuntosComisiones>();
                //    ListaRecalculo = PuntosNegocios.getInstance().ListaReporteNegocios(item.numeroDoc, IDP);
                //    bool update = PuntosNegocios.getInstance().ActualizaRecalculoPuntos(ListaRecalculo[0].dni, IDP, ListaRecalculo[0].PuntosPersonales, ListaRecalculo[0].VolumenPersonal, ListaRecalculo[0].VolumenRed, ListaRecalculo[0].VolumenGeneral, ListaRecalculo[0].VIP);
                //}
                int ddd = 0;
                foreach (var recal in Lista)
                {
                    ddd++;
                    double PP = recal.PuntosReales;
                    double VIP = recal.PuntosReales;
                    double VP = recal.PuntosPromo;
                    double VR = recal.PuntosPromo;
                    double VG = recal.PuntosPromo;
                    string DOC = recal.Documento;
                    int i = 0;
                    int IDPeriodo = IDP;
                    //int IDPeriodoComision = getPeriodoRango_Comision(recal.FechaPago);
                    //int IdPeriodoRango_Comi = PeriodoLN.getInstance().ObtenerIdperiodoRango_Comi(IDPeriodoComision);
                    List<string> LFactorComision = new List<string>();

                    while (DOC != "")
                    {
                        DOC = ClienteLN.getInstance().ListarFactorComision(DOC);
                        if (DOC != "") { LFactorComision.Add(DOC); }
                    }

                    bool todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(recal.Documento, 0, VP, 0, VG, 0, IDPeriodo);
                    if(IDPeriodo == recal.IDP_PP) {todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(recal.Documento, PP, 0, 0, 0, 0, recal.IDP_PP); }
                    if(IDPeriodo == recal.IDP_PP) { bool PVIP = CompraNegocios.getInstance().ActualizarPuntosVIP(recal.Documento, VIP, recal.IDP_PP); }

                    if ((recal.Paquete == "05" | recal.Paquete == "06") & recal.Factor != "")
                    {
                        bool VPP = CompraNegocios.getInstance().ActualizarPuntosComisiones(recal.Patrocinador, 0, VP, 0, 0, 0, IDPeriodo);
                        if (IDPeriodo == recal.IDP_PP) {bool VIPP = CompraNegocios.getInstance().ActualizarPuntosVIP(recal.Patrocinador, VIP, recal.IDP_PP); }
                    }

                    foreach (var item in LFactorComision)
                    {
                        if ((recal.Paquete == "05" | recal.Paquete == "06") & i == 0)
                        { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, 0, VG, 0, IDPeriodo); }
                        else { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, VR, VG, 0, IDPeriodo); }
                        i++;
                    }

                    if (recal.Paquete != "05" & recal.Paquete != "06")
                    {
                        bool VPR = CompraNegocios.getInstance().ActualizarVPR(recal.Patrocinador, IDPeriodo);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        private static int getPeriodoRango_Comision(DateTime fechaSimple)
        {
            int i2 = 0;
            bool ok = false;
            int IDPeriodo = 0;
            DateTime primerDiafechaActual = DateTime.Now;
            DateTime ultimoDiafechaActual = DateTime.Now;
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            while (!ok && i2 < ListaPeriodos.Count())
            {
                primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                if (fechaSimple >= primerDiafechaActual & fechaSimple < ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodoComision;
                }
                i2++;
            }
            return IDPeriodo;
        }

        [WebMethod]
        public static bool CalculoTiburon(int IDP)
        {
            List<Cliente> ListaTiburon = ClienteLN.getInstance().ListarClienteTiburon(IDP);
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodosCalculoComision();
            DateTime primerDiafechaActual = DateTime.Now;
            DateTime ultimoDiafechaActual = DateTime.Now;
            DateTime fechaActual = DateTime.Now;
            int IDPeriodo = IDP;
            bool recalculo = PuntosNegocios.getInstance().ResetearComTiburon(IDP);

            foreach (var item in ListaTiburon)
            {
                if (item.numeroDoc == "05603749")
                {
                    string xxx = "sdf";
                }
                double PuntoSocio = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item.numeroDoc, IDP);
                if (PuntoSocio >= 20)
                {
                    double montoTiburon = CompraNegocios.getInstance().ObtenerMontoTiburon(item.numeroDoc);
                    int Tiburon = PuntosNegocios.getInstance().ObtenerEvaluacionTiburon(item.numeroDoc, IDP);
                    DateTime fechaConvertida = DateTime.ParseExact(item.FechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime primerDia = new DateTime(fechaConvertida.Year, fechaConvertida.Month, 1);
                    DateTime ultimoDia = primerDia.AddMonths(1).AddDays(-1);
                    DateTime DiaMenos5 = ultimoDia.AddDays(-0);
                    DateTime PrimerDiaSig = ultimoDia.AddDays(1);
                    if (Tiburon == 1)
                    {
                        if (montoTiburon == 360)
                        {
                            if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                            {
                                string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(item.numeroDoc, RangoInicio, RangoFin);
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                            }
                            else
                            {
                                string fecIniPe = Convert.ToString(primerDia.ToShortDateString());
                                string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(item.numeroDoc, fecIniPe, fecFinPe);
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                            }
                        }
                        else
                        {
                            if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                            {
                                string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(item.numeroDoc, RangoInicio, RangoFin);
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                                else if (cantAfi >= 3)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 1, IDP);
                                }
                            }
                            else
                            {
                                string fecIniPe = Convert.ToString(primerDia.ToShortDateString());
                                string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(item.numeroDoc, fecIniPe, fecFinPe);
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                                else if (cantAfi >= 3)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 1, IDP);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                        {
                            string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                            string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                            int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(item.numeroDoc, RangoInicio, RangoFin);
                            if (montoTiburon == 360)
                            {
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                            }
                            else
                            {
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                                else if (cantAfi >= 3)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 0, IDP);
                                }
                            }
                        }
                        else
                        {
                            string fecIniPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaInicio.Trim()).FirstOrDefault();
                            string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                            int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(item.numeroDoc, fecIniPe, fecFinPe);
                            if (montoTiburon == 360)
                            {
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                            }
                            else
                            {
                                if (cantAfi >= 6)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 2, IDP);
                                }
                                else if (cantAfi >= 3)
                                {
                                    bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(item.numeroDoc, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                    bool updt = ClienteLN.getInstance().ActualizarEstadoTiburonRecalculo(item.numeroDoc, 0, IDP);
                                }
                            }

                        }
                    }
                }
            }
            return true;
        }

        //[WebMethod]
        //public static bool CalculoComAfiliacion(int IDP)
        //{

        //    List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodosCalculoComision();
        //    bool resetar = PuntosNegocios.getInstance().ResetearComAfi(IDP);
        //    string RangoInicio = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaInicio.Trim()).FirstOrDefault();
        //    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaFin.Trim()).FirstOrDefault();
        //    List<Compra> ListaCompras = CompraNegocios.getInstance().ListaCompraAfiliacion(RangoInicio, RangoFin);

        //    foreach (var item in ListaCompras)
        //    {
        //        int cc = 0;
        //        int ccc = 0;
        //        bool terminado = false;
        //        string doc = item.DNICliente;
        //        List<ComAfiliacion> ListaPatrocinadores = new List<ComAfiliacion>();
        //        while (cc < 5 && !terminado)
        //        {
        //            cc++;
        //            ComAfiliacion agregar = ClienteLN.getInstance().ListaDatosPatrocinioAfiliaciones(IDP, doc);
        //            if (agregar == null) { terminado = true; }
        //            else { ListaPatrocinadores.Add(agregar); doc = agregar.documento.Trim(); }
        //        }

        //        foreach (var item2 in ListaPatrocinadores)
        //        {
        //            double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDP);
        //            if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "01" | ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 0)
        //            {
        //                double COMAFI = item.montoComision * 0.30;
        //                bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, COMAFI, 0, 0, 0, 0);
        //            }
        //            if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 1)
        //            {
        //                double COMAFI = item.montoComision * 0.05;
        //                bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, COMAFI, 0, 0, 0);
        //            }
        //            if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 2)
        //            {
        //                double COMAFI = item.montoComision * 0.03;
        //                bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, 0, COMAFI, 0, 0);
        //            }
        //            if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 3)
        //            {
        //                double COMAFI = item.montoComision * 0.02;
        //                bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, 0, 0, COMAFI, 0);
        //            }
        //            if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "23") & ccc == 4)
        //            {
        //                double COMAFI = item.montoComision * 0.01;
        //                bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, 0, 0, 0, COMAFI);
        //            }
        //            ccc++;
        //        }
        //    }
        //    return true;
        //}

        [WebMethod]
        public static bool CalculoComAfiliacion(int IDP)
        {
            bool resetar = PuntosNegocios.getInstance().ResetearComAfi(IDP);
            int i = 0;
            while (i < 5)
            {
                string proc = (i == 0) ? "USP_CALCULO_BONO_UNI_1ERNIVEL" : (i == 1) ? "USP_CALCULO_BONO_UNI_2DONIVEL"
                            : (i == 2) ? "USP_CALCULO_BONO_UNI_3ERNIVEL" : (i == 3) ? "USP_CALCULO_BONO_UNI_4TONIVEL"
                            : "USP_CALCULO_BONO_UNI_5TONIVEL";
                string nom_nivel = (i == 0) ? "NIVEL1" : (i == 1) ? "NIVEL2"
                                : (i == 2) ? "NIVEL3" : (i == 3) ? "NIVEL4"
                                : "NIVEL5";
                List<PeriodoComision> ListaBonoUnilevel = PeriodoComisionLN.getInstance().ListaComisionUnilevelxNivel(IDP, proc);
                foreach (var item in ListaBonoUnilevel)
                {
                    bool ok1 = CompraNegocios.getInstance().ActualizarComUniIdcliente(item.idCliente, item.COMUNI, IDP, nom_nivel, item.COMUNI);
                }
                i++;
            }
            
            return true;
        }

        //[WebMethod]
        //public static bool CalculoUnilevel(int IDP)
        //{
        //    ClienteLN.getInstance().procesoUnilevel(IDP);
        //    return true;
        //}

        [WebMethod]
        public static bool CalculoUnilevel(int IDP)
        {
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodosCalculoComision();
            bool resetar = PuntosNegocios.getInstance().ResetearComUnilevel(IDP);
            string RangoInicio = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaInicio.Trim()).FirstOrDefault();
            string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaFin.Trim()).FirstOrDefault();
            List<Compra> ListaCompras = CompraNegocios.getInstance().ListaComprUnilevel(RangoInicio, RangoFin);

            int dddd = 0;
            foreach (var item in ListaCompras)
            {
                dddd++;
                double MCCU = item.montoComision;
                double MCCON = item.montoComision * 0.05;

                if (item.tipoCliente == "03") { MCCU = item.montoComision - item.montoComisionCI; }
                if (item.tipoCliente == "05") { MCCU = item.montoComision - MCCON; }
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = item.DNICliente; if (item.tipoCliente == "03" | item.tipoCliente == "05") { doc = item.DNIPatrocinador; ccc = 1; }
                double COMUNI = 0;
                List<PatrocinadorUnilivel> ListaUpline = new List<PatrocinadorUnilivel>();

                while (cc < 15 && !terminado)
                {
                    cc++;
                    PatrocinadorUnilivel agregar = ClienteLN.getInstance().ListaDatosUplineUnilevel(IDP, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaUpline.Add(agregar); doc = agregar.documento.Trim(); }
                }

                foreach (var item2 in ListaUpline)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDP);
                    if (PP >= 20 & ccc == 0 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.05;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 1 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 2 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 3 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 4 & (item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 5 & (item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.04;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 6 & (item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.03;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 7 & (item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.02;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 8 & (item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 9 & (item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 10 & (item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 11 & (item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 12 & (item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 13 & (item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 14 & (item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 15 & (item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 16 & (item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 17 & (item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 18 & (item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    ccc++;
                }

            }

            return true;
        }

        [WebMethod]
        public static bool CalculoComMercadeo(int IDP)
        {
            List<Periodo> Ultimos4Periodos = PeriodoLN.getInstance().ListarUltimos4PeriodosRecalculo(IDP);
            List<PeriodoComision> ListaCalculoMercadeo = ClienteLN.getInstance().ListarClienteBonoMercadeo(Ultimos4Periodos[3].fechaInicio, Ultimos4Periodos[3].fechaFin,
                                                                                                           Ultimos4Periodos[2].fechaInicio, Ultimos4Periodos[2].fechaFin,
                                                                                                           Ultimos4Periodos[1].fechaInicio, Ultimos4Periodos[1].fechaFin,
                                                                                                           Ultimos4Periodos[0].fechaInicio, Ultimos4Periodos[0].fechaFin, IDP);

            foreach (var item in ListaCalculoMercadeo)
            {
                int evaluacion = PuntosNegocios.getInstance().ObtenerEvaluacionMercadeoRecalculo(item.Documento, IDP);
                if (evaluacion == 0)
                {
                    PuntosNegocios.getInstance().ActualizarComisionMercadeoRecalculo(item.Documento, item.COMMERCA, IDP);
                }

                //int evaluacion = PuntosNegocios.getInstance().ObtenerEvaluacionMercadeoRecalculo(item.numeroDoc, IDP);

                //DateTime FecRgistro = DateTime.ParseExact(item.FechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //double pp1 = 0, pp2 = 0, pp3 = 0, pp4 = 0, puntosCiclo = 0;
                //int cc = 0;
                //bool acMerca = false;

                //while (cc < Ultimos4Periodos.Count)
                //{

                //    DateTime FecFin = DateTime.ParseExact(Ultimos4Periodos[cc].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                //    puntosCiclo = PuntosNegocios.getInstance().ObtenerPuntosXFechas(item.numeroDoc, Ultimos4Periodos[cc].fechaInicio, Ultimos4Periodos[cc].fechaFin);

                //    if (cc == 0) { pp4 = puntosCiclo; }
                //    else if (cc == 1) { pp3 = puntosCiclo; }
                //    else if (cc == 2) { pp2 = puntosCiclo; } else if (cc == 3) { pp1 = puntosCiclo; }
                //    cc++;
                //}

                //if (evaluacion == 0)
                //{
                //    if (pp4 >= 800 & pp3 >= 700 & pp2 >= 600 & pp1 >= 500)
                //    {
                //        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeoRecalculo(item.numeroDoc, 700, IDP);
                //    }
                //    else if (pp4 >= 700 & pp3 >= 600 & pp2 >= 500 & pp1 >= 400)
                //    {
                //        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeoRecalculo(item.numeroDoc, 600, IDP);
                //    }
                //    else if (pp4 >= 600 & pp3 >= 500 & pp2 >= 400 & pp1 >= 300)
                //    {
                //        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeoRecalculo(item.numeroDoc, 500, IDP);
                //    }
                //    else if (pp4 >= 500 & pp3 >= 400 & pp2 >= 300 & pp1 >= 200)
                //    {
                //        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeoRecalculo(item.numeroDoc, 400, IDP);
                //    }
                //    else if (pp4 >= 400 & pp3 >= 300 & pp2 >= 200 & pp1 >= 100)
                //    {
                //        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeoRecalculo(item.numeroDoc, 300, IDP);
                //    }
                //}
            }

            return true;
        }

        [WebMethod]
        public static bool CalculoComBronce(int IDP)
        {
            bool resetear = PuntosNegocios.getInstance().ResetearComBronce(IDP);
            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteBonoBronce(IDP);

            foreach (var item in ClienteConCompra)
            {
                bool update = PuntosNegocios.getInstance().ActualizarComisionBronceRecalculo(item.numeroDoc, 200, IDP);
            }

            return true;
        }

        [WebMethod]
        public static bool CalculoComEscolaridad(int IDP)
        {
            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteBonoEscolaridad(IDP);
            bool resetear = PuntosNegocios.getInstance().ResetearComEscolaridad(IDP);

            foreach (var item in ClienteConCompra)
            {
                int idRango = PuntosNegocios.getInstance().ObtenerRangoIDPComision(item.numeroDoc, IDP);
                if (idRango >= 5)
                {
                    int evaluacion = PuntosNegocios.getInstance().ObtenerEvaluacionEscolaridadRecalculo(item.numeroDoc, IDP);
                    //int requerimiento = PuntosNegocios.getInstance().ObtenerRequerimientoEscolaridadRecalculo(item.numeroDoc, IDP);

                    if (evaluacion < 1)
                    {
                        bool update = PuntosNegocios.getInstance().ActualizarEscolaridadRecalculo(item.numeroDoc, 800, IDP);
                    }
                    else if (evaluacion >= 1)
                    {
                        //if (requerimiento >= 2)
                        bool update = PuntosNegocios.getInstance().ActualizarEscolaridadRecalculo(item.numeroDoc, 500, IDP);
                    }
                }
            }

            return true;
        }

        [WebMethod]
        public static bool CalculoCoMatricial(int IDP)
        {
            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteConComprasRecalculo(IDP);
            bool resetear = PuntosNegocios.getInstance().ResetearComMatricial(IDP);

            foreach (var item in ClienteConCompra)
            {
                double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculoMatricial(item.numeroDoc, IDP);
                List<Periodo> ListaEvaluacion = PuntosNegocios.getInstance().ListaEvaluacionMatricial(item.numeroDoc, IDP);
                List<Cliente> ListaDirectosEvaluacion = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item.numeroDoc, IDP);

                int Directos60PP = (from x in ListaDirectosEvaluacion where x.PP >= 60 select x.PP).Count();
                int contador = 0;
                bool aprobado = false;

                if (ListaEvaluacion[0].evaluacionMatrocial4x4 < 1 | ListaEvaluacion[0].evaluacionMatrocial5x5 < 1 | ListaEvaluacion[0].evaluacionMatrocial6x6 < 1)
                {
                    if (ListaEvaluacion[0].evaluacionMatrocial6x6 < 1 && aprobado == false)
                    {
                        if (PP >= 60 && Directos60PP >= 6)
                        {
                            foreach (var item2 in ListaDirectosEvaluacion)
                            {
                                List<Cliente> ListaDirectosNivel2 = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item2.numeroDoc, IDP);
                                int Directos60PPNivel2 = (from x in ListaDirectosNivel2 where x.PP >= 60 select x.PP).Count();
                                if (Directos60PPNivel2 >= 6) { contador++; }
                            }
                            if (contador >= 6)
                            {
                                bool actualizar = PuntosNegocios.getInstance().ActualizarMatricialRecalculo(item.numeroDoc, 420, IDP, 0, 0, 1);
                                aprobado = true;
                            }
                        }
                    }
                    if (ListaEvaluacion[0].evaluacionMatrocial5x5 < 1 && aprobado == false)
                    {
                        if (PP >= 60 && Directos60PP >= 5)
                        {
                            contador = 0;
                            foreach (var item2 in ListaDirectosEvaluacion)
                            {
                                List<Cliente> ListaDirectosNivel2 = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item2.numeroDoc, IDP);
                                int Directos60PPNivel2 = (from x in ListaDirectosNivel2 where x.PP >= 60 select x.PP).Count();
                                if (Directos60PPNivel2 >= 5) { contador++; }
                            }
                            if (contador >= 5)
                            {
                                bool actualizar = PuntosNegocios.getInstance().ActualizarMatricialRecalculo(item.numeroDoc, 320, IDP, 0, 1, 0);
                                aprobado = true;
                            }
                        }
                    }
                    if (ListaEvaluacion[0].evaluacionMatrocial4x4 < 1 && aprobado == false)
                    {
                        if (PP >= 60 && Directos60PP >= 4)
                        {
                            contador = 0;
                            foreach (var item2 in ListaDirectosEvaluacion)
                            {
                                List<Cliente> ListaDirectosNivel2 = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item2.numeroDoc, IDP);
                                int Directos60PPNivel2 = (from x in ListaDirectosNivel2 where x.PP >= 60 select x.PP).Count();
                                if (Directos60PPNivel2 >= 4) { contador++; }
                            }
                            if (contador >= 4)
                            {
                                bool actualizar = PuntosNegocios.getInstance().ActualizarMatricialRecalculo(item.numeroDoc, 220, IDP, 1, 0, 0);
                                aprobado = true;
                            }
                        }
                    }
                }
            }

            return true;
        }

        [WebMethod]
        public static bool CalculoComCI(int IDP)
        {
            bool resetear = PuntosNegocios.getInstance().ResetearComCI(IDP);
            List<Compra> ListaCompras = CompraNegocios.getInstance().ListarComprasCalculoComisionCI(IDP);
            foreach (var item in ListaCompras)
            {
                double PuntosPatro = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item.DNIPatrocinador, IDP);
                if (PuntosPatro >= 20) { bool update = PuntosNegocios.getInstance().ActualizarComCIRecalculo(item.DNIPatrocinador, item.montoComisionCI, IDP); }
            }
            return true;
        }

        [WebMethod]
        public static bool CalculoComConsultor(int IDP)
        {
            bool resetear = PuntosNegocios.getInstance().ResetearComCon(IDP);
            List<Compra> ListaCompras = CompraNegocios.getInstance().ListarComprasCalculoComisionConsultor(IDP);
            foreach (var item in ListaCompras)
            {
                double PuntosPatro = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item.DNIPatrocinador, IDP);
                double montoComCon = item.montoComision * 0.05;
                if (PuntosPatro >= 20) { bool update = PuntosNegocios.getInstance().ActualizarComConsultorRecalculo(item.DNIPatrocinador, montoComCon, IDP); }
            }
            return true;
        }

        [WebMethod]
        public static bool ActualizarRangoComision(int IDP, int IDPCom)
        {
            List<Cliente> ListaSocios = PuntosNegocios.getInstance().ListarDniXRangoComisiones(IDP);

            foreach (var item in ListaSocios)
            {
                bool update = PuntosNegocios.getInstance().ActualizarRangoComision(item.numeroDoc, item.rango, IDPCom);
            }

            return true;
        }

        [WebMethod]
        public static bool ActualizarMontoComicionCI(int IDP)
        {

            List<Compra> ComprasCiclo = CompraNegocios.getInstance().ListarComprasComicionCI(IDP);
            foreach (var item in ComprasCiclo)
            {
                double descuentoCI = Convert.ToDouble(item.descuento);
                double montoCorregido = (item.montoComision / (1 - (descuentoCI / 2))) * (descuentoCI / 2);
                bool Update = CompraNegocios.getInstance().ActualizarMontoComisionCI(item.Ticket, montoCorregido);
            }
            return true;
        }

        [WebMethod]
        public static List<Periodo> ListaPeriodos()
        {
            List<Periodo> ListaPeriodo = PeriodoLN.getInstance().ListarPeriodosTotales();
            var query = from item in ListaPeriodo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<Periodo> ListaPeriodosComision()
        {
            List<Periodo> ListaPeriodo = PeriodoLN.getInstance().ListarPeriodoComisionCombo();
            var query = from item in ListaPeriodo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool GenerarVQ(int IDP)
        {
            string documento = "99999999";
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteRedVQPeriodo(documento, IDP);
            List<PuntosComisiones> ListaDirectos = new List<PuntosComisiones>();
            foreach (var item in Lista)
            {
                int canPPDirectos = 0;
                double VPRSocio = item.VPR;
                double PP = item.PP;
                double VP = item.VP;
                double VQSocio = 0.0;
                ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPRPeriodo(item.numeroDoc, IDP);
                canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectosPeriodo(item.numeroDoc, IDP);
                if (PP >= 20)
                {
                    if (canPPDirectos >= 2)
                    {
                        List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();
                        bool ok = Convert.ToBoolean(System.Web.HttpContext.Current.Session["ok2"]);
                        int i = 0;
                        while (!ok && i < ListaRangos.Count())
                        {
                            VQSocio = CalculoVQ(Convert.ToDouble(VPRSocio), ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP);
                            ok = Convert.ToBoolean(System.Web.HttpContext.Current.Session["ok2"]);
                            i++;
                        }
                        bool AVQ = PuntosNegocios.getInstance().ActualizarVQPeriodo(item.numeroDoc, VQSocio, IDP);
                        System.Web.HttpContext.Current.Session["ok2"] = false;
                    }
                    else
                    {
                        bool AVQ = PuntosNegocios.getInstance().ActualizarVQPeriodo(item.numeroDoc, 0, IDP);
                        System.Web.HttpContext.Current.Session["ok2"] = false;
                    }
                }
                else
                {
                    bool AVQ = PuntosNegocios.getInstance().ActualizarVQPeriodo(item.numeroDoc, 0, IDP);
                    System.Web.HttpContext.Current.Session["ok2"] = false;
                }

            }

            return true;
        }

        [WebMethod]
        public static bool ObtenerIDOPRegalo()
        {
            ///comentario
            List<Cliente> Lista = ClienteLN.getInstance().ListarRegaloSocios();
            foreach (var item in Lista)
            {
                string idopPShop = ObtenerIdopCompra(item.numeroDoc, item.CDRPremio, item.nombre, item.direccion, "", "", "", item.regalo, item.cantRegalo);
                bool ok2 = SendMail(item.numeroDoc, item.CDRPremio, idopPShop, item.nombre);
            }
            return true;
        }

        private static double CalculoVQ(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos, double VP)
        {
            double VPRA = 0;
            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VP = (VP >= PML) ? PML : VP;
                VRA = VRA + VP;
                if (VRA >= PR) { System.Web.HttpContext.Current.Session["ok2"] = true; VPRA = VRA; }
            }

            return VPRA;
        }

        private static string ObtenerIdopCompra(string dniComprador, string local, string nombreEnvia, string direccionComprador, string dir, string ubi, string refe, string regalo, int cantRegalo)
        {
            List<List<string>> listProductos = new List<List<string>>();
            List<ProductoV2> ListaProductoG = ProductoLN.getInstance().ListaIdopXNombre();
            string fechaActual = Convert.ToString(DateTime.Now.AddHours(-2).AddMinutes(-32).ToShortDateString());
            string idop = "";

            try
            {
                List<string> productos = new List<string>();
                productos.Add(regalo);
                productos.Add(Convert.ToString(cantRegalo));
                productos.Add("0.05");
                productos.Add("2");
                productos.Add("0");
                productos.Add(regalo);
                productos.Add("1");
                listProductos.Add(productos);

                string serializeProds = JsonConvert.SerializeObject(listProductos);

                string prod = serializeProds;

                string url = "http://santanatura.cti.lat/santa2/webservices/pedidos.php";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("a", "pCatalogo");
                nvc.Add("canal", "RED");
                nvc.Add("tipo", "2");
                nvc.Add("fpago", "EFECTIVO");
                nvc.Add("pagado", "0");
                nvc.Add("ruc", dniComprador.Trim());
                nvc.Add("local", local);
                nvc.Add("localorg", local);
                nvc.Add("razon", nombreEnvia);
                nvc.Add("dir_ruc", direccionComprador);
                nvc.Add("log", "Multinivel");
                nvc.Add("not", "Regalo - " + fechaActual);
                nvc.Add("delivery", "0");
                nvc.Add("dir", dir);
                nvc.Add("ubi", ubi);
                nvc.Add("ref", refe);
                nvc.Add("moneda", "PEN");
                nvc.Add("prod", prod);

                var data = wc.UploadValues(url, "POST", nvc);

                var responseString = UnicodeEncoding.UTF8.GetString(data);

                idop = responseString;
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return idop;
        }

        public static bool SendMail(string DNI, string estableMail, string idopPShop, string nombreEnviar)
        {
            bool estado = true;
            String merror;

            bool ok = true;
            MailMessage mail = new MailMessage();
            mail.To.Add(new MailAddress("regalosypremios.snn@gmail.com"));
            mail.From = new MailAddress("santanaturavoucher@mundosantanatura.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
            mail.Subject = "Nuevo Efectico de " + nombreEnviar + " Establecimiento: " + estableMail;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = string.Format("DNI: {1}{0}IDOP: {2}{0}",
                                       Environment.NewLine, DNI, idopPShop);
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
            smtp.Credentials = new System.Net.NetworkCredential("santanaturavoucher@mundosantanatura.com", "s@nt@2019");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                estado = false;
                merror = ex.Message.ToString();
            }

            return ok;
        }

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        private static string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

    }
}