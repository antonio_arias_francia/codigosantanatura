﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;

namespace SantaNaturaNetwork
{
    /// <summary>
    /// Descripción breve de FileUploadNovedad
    /// </summary>
    public class FileUploadNovedad : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string ruta = context.Server.MapPath("~/novedad/");

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname = context.Server.MapPath("~/novedad/" + file.FileName);
                    if (File.Exists(fname))
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("El nombre de la imagen ya existe");

                    }
                    else
                    {
                        file.SaveAs(fname);
                    }
                }

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}