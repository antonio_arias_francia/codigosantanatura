﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SantaNaturaNetwork
{
    /// <summary>
    /// Descripción breve de FileUploadProduct
    /// </summary>
    public class FileUploadProduct : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string ruta = context.Server.MapPath("~/products/");

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname = context.Server.MapPath("~/products/" + file.FileName);
                    if (File.Exists(fname))
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("El nombre de la imagen ya existe");

                    }
                    else
                    {
                        file.SaveAs(fname);
                    }
                }

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}