﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Principal.aspx.cs" Inherits="SantaNaturaNetwork.Novedades" %>

<%@ OutputCache Location="None" NoStore="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/proyecto2/IndexCDRStyles.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="fondoYacha">
        <img class="imgLogoYachaPagPrin" src="img/logoYACHAYWASIPagPrincipal.png" alt="Alternate Text" />
        <img class="imgPagPrin" src="img/imgPagPrincipal1920x1378.png" alt="Alternate Text" />
    </div>

    <script>
        window.onload = function () {
            document.getElementById("principal").style.color = '#79B729';
            document.getElementById("principal").style.textShadow = '0px 0px 4px rgbA(229, 246, 27, 0.8)';
        }
    </script>
</asp:Content>
