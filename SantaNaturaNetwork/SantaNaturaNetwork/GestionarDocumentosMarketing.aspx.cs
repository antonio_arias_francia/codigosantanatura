﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;

namespace SantaNaturaNetworkV3
{
    public partial class GestionarDocumentosMarketing : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<ArchivosPDF.DocumentosMarketing> ListarDocumentosMarketing()
        {
            List<ArchivosPDF.DocumentosMarketing> Lista = ArchivosLN.getInstance().ListarDocumentosMarketing();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<ArchivosPDF.DocumentosMarketing> ListarDetallesDocumentosMarketing(int idDatos)
        {
            List<ArchivosPDF.DocumentosMarketing> Lista = ArchivosLN.getInstance().ListarDetallesDocumentosMarketing(idDatos);
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistroDocumentosMarketing(ArchivosPDF.DocumentosMarketing objDatos)
        {
            try
            {
                ArchivosLN.getInstance().RegistroDocumentosMarketing(objDatos);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        [WebMethod]
        public static bool EliminarArchivo(string archivo)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/marketing/" + archivo);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static bool ActualizarDocumentosMarketing(ArchivosPDF.DocumentosMarketing objDatos)
        {
            try
            {
                ArchivosLN.getInstance().ActualizarDocumentosMarketing(objDatos);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        [WebMethod]
        public static bool EliminarDocumentosMarketing(int idS, string archivoS)
        {
            try
            {
                var filePath = HttpContext.Current.Server.MapPath("~/documentos/" + archivoS);
                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }
                ArchivosLN.getInstance().EliminarDocumentosMarketing(idS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }
    }
}