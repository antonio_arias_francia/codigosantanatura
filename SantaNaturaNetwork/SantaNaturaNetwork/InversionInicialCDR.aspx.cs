﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class InversionInicialCDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static bool InicioInversionInicialCDR()
        {
            bool respuesta;
            try
            {
                List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDR();

                foreach (var cdr in ListaCDR)
                {
                    Cliente.CDR.InversionCDR objCDR = new Cliente.CDR.InversionCDR()
                    {
                        Fecha = DateTime.Now.AddHours(-2),
                        Monto = 0,
                        DNICDR = cdr.DNICDR,
                        IdInicial = "01",
                        TipoCompra = 0
                    };
                    bool registro = CdrLN.getInstance().RegistroInversionCDR(objCDR);
                }
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }

        [WebMethod]
        public static List<Cliente.CDR.InversionCDR> ListaInversionInicialCDR()
        {
            List<Cliente.CDR.InversionCDR> ListaCDR = CdrLN.getInstance().ListaInversionInicialCDR();
            return ListaCDR;
        }

        [WebMethod]
        public static bool ActualizarInversionInicialCDR(decimal montoS, int idInversionS)
        {
            bool respuesta;
            try
            {
                Cliente.CDR.InversionCDR objCDR = new Cliente.CDR.InversionCDR()
                {
                    Monto = montoS,
                    IdInversion = idInversionS
                };
                bool registro = CdrLN.getInstance().ActualizarInversionInicialCDR(objCDR);
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }

    }
}