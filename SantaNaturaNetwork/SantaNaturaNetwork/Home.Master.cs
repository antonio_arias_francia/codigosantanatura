﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

using System.Web.Script.Serialization;

namespace SantaNaturaNetwork
{
    public partial class Home : System.Web.UI.MasterPage
    {
        public string NombresCompletos = Convert.ToString(System.Web.HttpContext.Current.Session["Nombres"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            //string tipoCliente = Convert.ToString(Session["TipoCliente"]);
            //string cerrarSession = Session["cerrarSession"].ToString();
            //if (tipoCliente == "01" || tipoCliente == "02" || tipoCliente == "03" || tipoCliente == "05" || tipoCliente == "06" || tipoCliente == "07" || tipoCliente == "08" || tipoCliente == "09" || tipoCliente == "10")
            //{
            //    //navDesplegable.Attributes.Add("style", "display:block;");
            //    //navbonificiones.Attributes.Add("style", "display:block;");

            //    //NavBarRed.Attributes.Add("style", "display:block;");
            //    SubMenuMisCompras.Attributes.Add("style", "display:block;");
            //    SubMenuPreRegistro.Attributes.Add("style", "display:none;");
            //    navPerfil.Attributes.Add("style", "display:block;");
            //    navDocumentos.Attributes.Add("style", "display:block; margin:0");
            //    if (tipoCliente == "01")
            //    {
            //        navDesplegable.Attributes.Add("style", "display:block;margin:0");
            //        navbonificiones.Attributes.Add("style", "display:block;");
            //        //NavBarDocumentos.Attributes.Add("style", "display:block;");
            //        //NavBarCalificacion.Attributes.Add("style", "display:block;");
            //        //NavBarTerminos.Attributes.Add("style", "display:block;");
            //        navContactenos.Attributes.Add("style", "display:block;");
            //    }
            //}
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                string tipoCliente = Convert.ToString(Session["TipoCliente"]);
                navlogin.Attributes.Add("style", "display:none");
                navCerrar.Attributes.Add("style", "display:block");

                SubMenuMisCompras.Attributes.Add("style", "display:block;");
                navPerfil.Attributes.Add("style", "display:block;");
                navDocumentos.Attributes.Add("style", "display:block; margin:0");

                if (tipoCliente == "01")
                {
                    navDesplegable.Attributes.Add("style", "display:block;margin:0");
                    navbonificiones.Attributes.Add("style", "display:block;");
                    navContactenos.Attributes.Add("style", "display:block;");
                    SubMenuPreRegistro.Attributes.Add("style", "display:block;");
                }

            }
            else
            {
                navlogin.Attributes.Add("style", "display:block");
                navCerrar.Attributes.Add("style", "display:none");
            }
            //if (Convert.ToString(Session["isLogueado"]) == "SI" && cerrarSession != "SI")
            //{
            //    navlogin.Attributes.Add("style", "display:none");
            //    navCerrar.Attributes.Add("style", "display:block");
            //} else if (Convert.ToString(Session["isLogueado"]) == "NO" && cerrarSession == "SI") {
            //    navlogin.Attributes.Add("style", "display:block");
            //    navCerrar.Attributes.Add("style", "display:none");
            //}

            //if (cerrarSession == "SI")
            //{
            //    LimpiarSession();
            //    Response.Redirect("Index.aspx");
            //}


            if (!IsPostBack)
            {
                if (Convert.ToInt32(Session["detalleCompra"]) == 1) {
                    Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache);
                    Response.Cache.SetAllowResponseInBrowserHistory(false);
                    Response.Cache.SetNoStore();
                }
                
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "open_vencimiento_clave();", true);
            }
        }

        private void LimpiarSession()
        {
            //Datos importantes para comprar
            Session["NombreDelivery"] = "";
            Session["DNIDelivery"] = "";
            Session["CelularDelivery"] = "";
            Session["DireccionDelivery"] = "";
            Session["TransporteDelivery"] = "";
            Session["DirecTransporteDelivery"] = "";
            Session["ProvinciaDelivery"] = "";
            Session["DirecProvinciaDelivery"] = "";
            Session["EvalDelivery"] = 0;
            Session["NumDocCliente"] = "";
            Session["NumDocClienteRegistrador"] = "";
            Session["TipoCliente"] = "";
            Session["NombrePatrocinador"] = "";
            Session["PatrocinadorDNI"] = "";
            Session["UplineDNI"] = "";
            Session["IdSimplex"] = "";
            Session["PromoAplicada"] = 0;
            Session["IdCliente"] = "";
            Session["RUCSocio"] = "";
            Session["NumRUCOP"] = "";
            Session["TiendaAyuda"] = null;

            Session["Pais"] = "";
            Session["Departamento"] = "";
            Session["Provincia"] = "";
            Session["Distrito"] = "";
            Session["FechaNac"] = "";

            //
            Session["FechaNacimientoInicial"] = "";
            Session["txtUsuario"] = "";
            Session["txtContra"] = "";
            Session["txtNombre"] = "";
            Session["txtApePat"] = "";
            Session["txtApeMat"] = "";
            Session["cboSexo"] = "";
            Session["cboTipoDoc"] = "";
            Session["txtDocumento"] = "";
            Session["txtCorreo"] = "";
            Session["txtTelefono"] = "";
            Session["txtCelular"] = "";
            Session["cboPais"] = "";
            Session["cboDepartamento"] = "";
            Session["cboProvincia"] = "";
            Session["cboDistrito"] = "";
            Session["txtReferencia"] = "";
            Session["txtDireccion"] = "";
            Session["txtCuentaTransa"] = "";
            Session["txtRuc"] = "";
            Session["txtBanco"] = "";
            Session["txtDeposito"] = "";
            Session["txtInterbancaria"] = "";
            Session["cboTipoCliente"] = "";
            Session["cboUpline"] = "";
            Session["cboEstablecimiento"] = "";
            Session["cboPremio"] = "";
            Session["cboTipoCompra"] = "";
            Session["ProcesoVisa"] = 0;
            Session["EliminaProductoTienda"] = 0;
            Session["mostrarCompraTerminada"] = 0;
            Session["EliminaProductoxCDR"] = 0;
            Session["PPSOCIO"] = 0;
            Session["ValidarRUC"] = 0;
            Session["ModalIndex"] = 0;

            //

            //
            Session["SubTotalCorazones"] = 0.00;
            Session["SubTotalPuntos"] = 0.00;
            Session["SubTotalPuntosPromocion"] = 0.00;
            Session["SubTotal"] = 0.00;
            Session["MontoAPagar"] = 0.00;
            Session["TipoPago"] = "";
            Session["Foto"] = "";
            Session["IdProducto"] = "";
            Session["Producto"] = "";
            Session["PrecioUnitario"] = 0.00;
            Session["Cantidad"] = 0;
            Session["VamosCompra"] = 0;
            Session["EliminarPromo"] = 0;
            Session["TiendaRetorna"] = 0;
            Session["ActualizaPag"] = 0;
            Session["codProducto"] = "";
            Session["codProducto2"] = "";
            Session["codProducto3"] = "";
            Session["codProducto4"] = "";
            Session["codProdBasico"] = "";
            Session["codProdProfe"] = "";
            Session["codProdEmpre"] = "";
            Session["codProdMillo"] = "";
            Session["codProdBasico2"] = "";
            Session["codProdProfe2"] = "";
            Session["codProdEmpre2"] = "";
            Session["codProdMillo2"] = "";
            Session["tipoCompPromo"] = "";
            Session["TipoAfiliacion"] = "";
            Session["cantRegalo"] = 0;
            Session["cboActivar"] = "0";
            Session["cboComprobante"] = "0";
            Session["cboTitular"] = "0";
            Session["RecargaPagina"] = 0;
            Session["descuentoCI"] = 0;
            Session["montoMomentaneo"] = 0.0;
            Session["ok"] = false;
            Session["ok2"] = false;
            Session["Transaction_Date"] = "";
            Session["Merchant"] = "";
            Session["Id_Unico"] = "";
            Session["Transaction_ID"] = "";
            Session["CardS"] = "";
            Session["Aauthorization_Code"] = "";
            Session["AMOUNT"] = "";
            Session["CURRENCY"] = "";
            Session["BRAND"] = "";
            Session["STATUSS"] = "";
            Session["ACTION_DESCRIPTION"] = "";
            Session["Nombre_Completo"] = "";
            Session["DireccionSocio"] = "";
            Session["PaginaAnterior"] = "";
            Session["ModalSesion"] = 0;

            Session["Ayuda"] = "NO";

            //Combo ayuda
            Session["sTipoCompraSelect"] = "";
            Session["sTipoEntregaSelect"] = "0";
            Session["sMedioPagoSelect"] = "0";
            Session["comboDepartSelect"] = "0";
            Session["comboProvinciaSelect"] = "0";
            Session["comboDistritoSelect"] = "0";
            Session["comboTiendaSelect"] = "";
            Session["Establecimiento"] = "";

            List<ProductoCarrito> carritoProductos = new List<ProductoCarrito>();
            List<ProductoCarrito> carritoProductosP = new List<ProductoCarrito>();

            Session["CarritoProducto"] = carritoProductos;
            Session["CarritoProductoPaquete2"] = carritoProductosP;

            //DatosClienteFormulario
            Session["Nombre"] = "";
            Session["Provincia"] = 0;

            //Login
            Session["isLogueado"] = "NO";

        }

        protected void Unnamed_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Redirect("Index.aspx");
        }
    }
}