﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;

namespace SantaNaturaNetworkV3
{
    public partial class GestionarCanjesComisiones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "04603721" && Convert.ToString(Session["NumDocCliente"]) != "02357051" &&
                    Convert.ToString(Session["NumDocCliente"]) != "43027399" && Convert.ToString(Session["NumDocCliente"]) != "03002548")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Compra.Facturacion> ListaCanjesAbono(int periodoS)
        {
            List<Compra.Facturacion> Lista = null;
            try
            {
                if (periodoS == 0) { periodoS = PuntosNegocios.getInstance().ObtenerIdPeriodoComisionAnteriorActivo(); }
                Lista = CompraNegocios.getInstance().ListaCanjesAbono(periodoS);
            }
            catch (Exception ex)
            {
                Lista = null;
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<Compra.Facturacion> ListaDatalleCanjesAbono(int idDatos)
        {
            List<Compra.Facturacion> Lista = null;
            try
            {
                Lista = CompraNegocios.getInstance().ListaDatalleCanjesAbono(idDatos);
            }
            catch (Exception ex)
            {
                Lista = null;
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PeriodoComision> ListaPeriodoModalCanjesAbono(int periodoS)
        {
            List<PeriodoComision> Lista = null;
            try
            {
                if (periodoS == 0) { periodoS = PuntosNegocios.getInstance().ObtenerIdPeriodoComisionAnteriorActivo(); }
                Lista = PeriodoComisionLN.getInstance().ListaPeriodoModalCanjesAbono(periodoS);
            }
            catch (Exception ex)
            {
                Lista = null;
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static bool ActualizarDetalleCanjesAbono(int idDatoS, int idPeriodoS, string estadoSaldoDiS, string solicitudCanjeS,
                                                        decimal montoS, string estadoCanjeS, string ticketS, string observacionS,
                                                        string correcionS, string idClienteS)
        {
            bool ok = false;
            bool SolicitudCanjeB = (solicitudCanjeS == "SI") ? true : false;
            try
            {
                Compra.Facturacion facturacion = new Compra.Facturacion()
                {
                    Id_Datos = idDatoS,
                    IdPeriodo = idPeriodoS,
                    EstadoSaldoDisponible = estadoSaldoDiS,
                    SolicitudCanje = SolicitudCanjeB,
                    MontoCanje = montoS,
                    EstadoCanje = estadoCanjeS,
                    Tickets = ticketS,
                    Observacion = observacionS,
                    Correcion = correcionS,
                    IdCliente = idClienteS
                };
                CompraNegocios.getInstance().ActualizarDetalleCanjesAbono(facturacion);
            }
            catch (Exception ex)
            {
                ok = false;
                throw ex;
            }
            return ok;
        }

        [WebMethod]
        public static bool EliminarArchivo(string archivo, int idDatoS)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/facturacion/" + archivo);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                CompraNegocios.getInstance().EliminarNombrePDF_ControlCanjes(idDatoS);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static bool SellarRegistro_ControlCanjes(int idDatoS)
        {
            bool ok = false;
            try
            {
                CompraNegocios.getInstance().SellarRegistro_ControlCanjes(idDatoS);
                ok = true;
            }
            catch (Exception ex)
            {
                ok = false;
                throw ex;
            }
            return ok;
        }

        [WebMethod]
        public static bool EliminarRegistro_ControlCanjes(string archivoS, int idDatoS)
        {
            bool ok = false;
            try
            {
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                if (documento == "45750970" || documento == "730115832")
                {
                    var filePath = HttpContext.Current.Server.MapPath("~/facturacion/" + archivoS);
                    if (File.Exists(filePath))
                    {
                        File.Delete(filePath);
                    }
                    CompraNegocios.getInstance().EliminarRegistro_ControlCanjes(idDatoS);
                    ok = true;
                }
            }
            catch (Exception ex)
            {
                ok = false;
                throw ex;
            }
            return ok;
        }

    }
}