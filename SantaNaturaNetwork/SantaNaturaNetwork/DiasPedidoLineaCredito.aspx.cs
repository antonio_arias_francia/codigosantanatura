﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class DiasPedidoLineaCredito : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasPedidoLineaCreditoCDR()
        {
            List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaCDR = CdrLN.getInstance().ListaDiasPedidoLineaCreditoCDR();
            return ListaCDR;
        }

        [WebMethod]
        public static bool ActualizarEstadoDiasPedidoLC(int idDiaS, bool estadoS)
        {
            bool respuesta;
            try
            {
                Cliente.CDR.DiasPedidosLineaCreditoCDR objCDR = new Cliente.CDR.DiasPedidosLineaCreditoCDR()
                {
                    IDDias = idDiaS,
                    Estado = estadoS
                };
                bool registro = CdrLN.getInstance().ActualizarEstadoDiasPedidoLC(objCDR);
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }


    }
}