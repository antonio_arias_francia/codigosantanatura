﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class DiasConficionalEmergencia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaDiasCondicionEmergencia()
        {
            List<Cliente.CDR.DiasPedidosLineaCreditoCDR> ListaCDR = CdrLN.getInstance().ListaDiasCondicionEmergencia();
            return ListaCDR;
        }

        [WebMethod]
        public static bool ActualizarEstadoDiasEmergencia(int idDiaS, bool estadoS)
        {
            bool respuesta;
            try
            {
                Cliente.CDR.DiasPedidosLineaCreditoCDR objCDR = new Cliente.CDR.DiasPedidosLineaCreditoCDR()
                {
                    IDDias = idDiaS,
                    Estado = estadoS
                };
                bool registro = CdrLN.getInstance().ActualizarCondicionEmergencia(objCDR);
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }
    }
}