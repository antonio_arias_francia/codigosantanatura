﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class MapaRedSocios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["mostrarCompraTerminada"] = 0;
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("InicioAdmin.aspx");
                }

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
        }

        [WebMethod]
        public static List<ElementosTreeGrid> ListarEstructuraPropio(int idPeriodoS)
        {
            string idCliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            List<ElementosTreeGrid> Lista = null;
            try
            {
                if (idPeriodoS == 0) { idPeriodoS = PuntosNegocios.getInstance().MostrarIdPeriodoPuntosActivo(); }
                Lista = (idPeriodoS <= 27 ) ? ElementosTreeGridLN.getInstance().ListaTreeTableHistorico(idCliente, 0, idPeriodoS) : 
                                              ElementosTreeGridLN.getInstance().ListaTreeTable(idCliente, 0, idPeriodoS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<ElementosTreeGrid> ListarEstructuraRed(string idClienteS, int idPeriodoS)
        {
            List<ElementosTreeGrid> Lista = null;
            try
            {
                if (idPeriodoS == 0) { idPeriodoS = PuntosNegocios.getInstance().MostrarIdPeriodoPuntosActivo(); }
                Lista = (idPeriodoS <= 27) ? ElementosTreeGridLN.getInstance().ListaTreeTableHistorico(idClienteS, 1, idPeriodoS) : 
                                             ElementosTreeGridLN.getInstance().ListaTreeTable(idClienteS, 1, idPeriodoS);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }
    }
}