﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
	public partial class PeriodoCDR : System.Web.UI.Page
	{
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR()
        {
            List<Cliente.CDR.PERIODOCDR> Lista = CdrLN.getInstance().ListaPeriodoCDR();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistrarPeriodoCDR(string nom_periodoSend, string fecInicioSend, string fecFinSend)
        {
            Cliente.CDR.PERIODOCDR objPeriodo = new Cliente.CDR.PERIODOCDR()
            {
                Descripcion = nom_periodoSend,
                FechaInicio = fecInicioSend,
                FechaFin = fecFinSend
            };

            int idPeriodoCDR = CdrLN.getInstance().RegistroPeriodoCDR(objPeriodo);
            List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDRStock();

            foreach (var item in ListaCDR)
            {
                if (item.Pais == "01")
                {
                    bool registroCLC = CdrLN.getInstance().RegistroInicial_CLC_CDR(idPeriodoCDR, item.DNICDR);
                }
            }
            return true;
        }

        [WebMethod]
        public static bool ActualizarPeriodoCDR(string id_periodoSend, string nom_periodoSend, string fecInicioSend, string fecFinSend)
        {

            Cliente.CDR.PERIODOCDR objPeriodo = new Cliente.CDR.PERIODOCDR()
            {
                IdPeriodo = Convert.ToInt32(id_periodoSend),
                Descripcion = nom_periodoSend,
                FechaInicio = fecInicioSend,
                FechaFin = fecFinSend
            };
            bool ok = CdrLN.getInstance().ActualizarPeriodoCDR(objPeriodo);
            return true;
        }
    }
}