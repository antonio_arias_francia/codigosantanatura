﻿using Modelos;
using Negocios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using Newtonsoft.Json;
using System.Net;
using System.Net.Mail;
using System.IO;
using System.Collections.Specialized;
using System.Web.Script.Serialization;

namespace SantaNaturaNetwork
{
    /// <summary>
    /// Descripción breve de Autocompletado
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    [System.Web.Script.Services.ScriptService]
    public class Autocompletado : System.Web.Services.WebService
    {

        public List<string> listaNombreProductos = new List<string>();
        public List<string> listaNombreSocio = new List<string>();
        public string respuestaPrueba = "";

        [WebMethod]
        public List<string> FiltrarNombreProductos(string palabra)
        {
            listaNombreProductos = ProductoNegocios.getInstance().FiltrarNombreProductos(palabra);

            return listaNombreProductos;
        }

        [WebMethod]
        public bool Notificacion(string eventType, string operationNumber, TokenPE.Notification.DataE data)
        {
            var authorization = HttpContext.Current.Request.Headers["PE-Signature"];
            bool ValidarCode = ValidacionNumero(data.transactionCode);
            if (ValidarCode == false)
            {
                bool datosPE = CompraNegocios.getInstance().ActualizarDatosCompraPE(data.cip, data.paymentDate, operationNumber);
            }
            else
            {
                bool datosPECDR = CdrLN.getInstance().ActualizarDatosCompraPE_CDR(Convert.ToInt32(data.transactionCode), data.paymentDate, operationNumber);
                bool actualizarEstado = StockLN.getInstance().ActualizarEstadoSolicitud(Convert.ToInt32(data.transactionCode), "", "", DateTime.Now.AddHours(-2).ToString("dd/MM/yyyy"));
            }

            return true;
        }

        [WebMethod]
        public string ObtenerIDOP_Puntos_Comision(string prueba)
        {
            bool estado = CompraNegocios.getInstance().ObtenerEstadoTareaCompras();
            if (estado == false)
            {
                CompraNegocios.getInstance().ActualizarEstadoTareaCompras(true);
                List<Compra> Lista = CompraNegocios.getInstance().ListaCompraDatosPagoEfectivoPendiente();
                bool ok = false;
                foreach (var item in Lista)
                {
                    try
                    {
                        List<DetalleCompra> listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(item.Ticket);
                        bool estadoPE = CompraNegocios.getInstance().ActualizarEstadoPagoEfectivo(item.Ticket);
                        string Paquete = ClienteLN.getInstance().ObtenerPaqueteSocio(item.DNICliente);
                        string NotaPS = Paquete.Substring(12, Paquete.Length - 12) + " - " + item.NotaPS + " - " + Convert.ToString(item.FechaPago).Substring(0, 10);
                        string idopPShop = ObtenerIdopCompra(item.DNICliente, item.Despacho, item.NombreCliente, item.DireccionCliente, NotaPS, "", "", "",
                                                             listaDetalleCompra, item.Ruc, item.Comprobante, item.CIP);
                        if (idopPShop != "" || idopPShop !=  null)
                        {
                            bool okVisa = CompraNegocios.getInstance().ActualizarIdopYEstadoCompra(item.Ticket, idopPShop);
                            Compra compraPC = CompraNegocios.getInstance().DatosPuntosYMontosCompra(item.Ticket);
                            int FiltroPreRegistro = ClienteLN.getInstance().ObtenerFiltroPreregistro(item.IdCliente);
                            if (FiltroPreRegistro == 1) { ClienteLN.getInstance().ActualizarEstadoPreregistro(item.IdCliente); ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, item.idTipoCompra); }
                            actualizarPuntos(item.DNICliente.Trim(), item.DNIFactorComision.Trim(), item.DNIPatrocinador.Trim(), item.PaqueteSocio.Trim(), compraPC, item.FechaPago, item.IdCliente);
                            actualizarComisiones(item.DNICliente, item.DNIFactorComision, item.DNIPatrocinador, item.PaqueteSocio, compraPC, item.FechaPago, item.idTipoCompra, item.tipoCliente);
                            Comisiones_Linea_CDR(listaDetalleCompra, item.Despacho, item.FechaPago);
                            if (item.idTipoCompra == "08")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "02");
                            }
                            else if (item.idTipoCompra == "09")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "03");
                            }
                            else if (item.idTipoCompra == "10")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "04");
                            }
                            else if (item.idTipoCompra == "11")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "03");
                            }
                            else if (item.idTipoCompra == "12")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "04");
                            }
                            else if (item.idTipoCompra == "13")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "04");
                            }
                            else if (item.idTipoCompra == "13")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "04");
                            }
                            else if (item.idTipoCompra == "24")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "23");
                            }
                            else if (item.idTipoCompra == "25")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "23");
                            }
                            else if (item.idTipoCompra == "26")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "23");
                            }
                            else if (item.idTipoCompra == "27")
                            {
                                ok = ClienteLN.getInstance().ActualizarDescuento(item.IdCliente, "23");
                            }
                            bool SMail = SendMailPE(item.DNICliente, item.Despacho, Convert.ToString(item.FechaPago), Convert.ToString(item.MontoAPagar), item.NotaDelivery,
                                                  idopPShop, item.NombreCliente, item.CIP, item.FechaPagada, item.NumOperacion, item.Comprobante + " " + item.Ruc, item.NotaPS);
                        }
                        else
                        {
                            bool reestablecer = CompraNegocios.getInstance().ReestablecerEstadoPagoEfectivo(item.Ticket);
                            bool registro = ClienteLN.getInstance().RegistroModificaciones("73011583", DateTime.Now.AddHours(-2),
                                                                               "Error Sistema" + item.Ticket, "Falla en sistema de Consulta RUC");
                            CompraNegocios.getInstance().ActualizarEstadoTareaCompras(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        bool registro = ClienteLN.getInstance().RegistroModificaciones("73011583", DateTime.Now.AddHours(-2),
                                                                               "Error Sistema" + item.Ticket, Convert.ToString(ex));
                        CompraNegocios.getInstance().ActualizarEstadoTareaCompras(false);
                        throw ex;
                    }
                }
                CompraNegocios.getInstance().ActualizarEstadoTareaCompras(false);
            }
            return "2xx";
        }

        [WebMethod]
        public string ProcesamientoComprasCDR(string prueba)
        {
            List<Cliente.CDR.DatosProcesamientoComprasCDR> Lista = CdrLN.getInstance().ListaDatosProcesamientoComprasCDR();
            foreach (var item in Lista)
            {
                List<StockCDR> ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(item.IdPedido);
                string idop = ObtenerIdopCompraCDR(item.DniCDR, ListaPedido, item.IdPedido, item.TipoCompra);
                StockLN.getInstance().ActualizarEstadoSolicitudPE(item.IdPedido, idop);

                if (item.TipoCompra != "9") {
                    string idInicialInversion = "0";
                    Cliente.CDR.InversionCDR objInversion = new Cliente.CDR.InversionCDR()
                    {
                        Fecha = DateTime.Now.AddHours(-2),
                        Monto = Convert.ToDecimal(item.Monto),
                        DNICDR = item.DniCDR,
                        IdInicial = idInicialInversion,
                        TipoCompra = Convert.ToInt32(item.TipoCompra)
                    };
                    bool inversion = CdrLN.getInstance().RegistroInversionCDR(objInversion);
                    foreach (var item2 in ListaPedido)
                    {
                        bool recuperar = StockLN.getInstance().IncrementarStock(item.DniCDR, item2.IDProductoXPais, item2.Cantidad);
                    }
                }
                else
                {
                    int IDPeriodoCDR = 0, i2 = 0;
                    bool okCDR = false;
                    List<Cliente.CDR.ComisionLineaCDR> ListaSD = new List<Cliente.CDR.ComisionLineaCDR>();
                    List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR = CdrLN.getInstance().ListaPeriodoCDR();
                    while (!okCDR && i2 < ListaPeriodoCDR.Count())
                    {
                        DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                        DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                        if (item.Fecha >= primerDiafechaActual & item.Fecha < ultimoDiafechaActual)
                        {
                            okCDR = true; IDPeriodoCDR = ListaPeriodoCDR[i2].IdPeriodo;
                        }
                        i2++;
                    }

                    foreach (var item2 in ListaPedido)
                    { 
                        List<ProductoPais.Linea_PrecioCDR> ListaLineaPrecioCDR = ProductoLN.getInstance().ObtenerLineaPrecioCDR_By_IdProductoPais(item2.IDProductoXPais);
                        Cliente.CDR.ComisionLineaCDR objCDR = new Cliente.CDR.ComisionLineaCDR()
                        {
                            Comision = 0,
                            LineaCredito = item2.Cantidad * ListaLineaPrecioCDR[0].PrecioCDR,
                            IDPeriodoCDR = IDPeriodoCDR,
                            DniCDR = item.DniCDR
                        };
                        ListaSD.Add(objCDR);
                    }
                    
                    bool ActualizarComisionLinea = CdrLN.getInstance().ActualizarComisionLineaCDRLista(ListaSD);
                }

            }
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        private void DescontarStockProducto(string cdr, string idPS, int cantidad, string idProPais)
        {
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();
            string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == cdr.Trim() select c.DNICDR.Trim()).FirstOrDefault();
            bool descontar = StockLN.getInstance().DescontarStock(dniCDR, idProPais, cantidad);
        }

        [WebMethod]
        public string PruebaCSV(string prueba)
        {
            List<StockCDR> ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(27);
            EnviarArchivoCSV(ListaPedido, "123456");
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string CalculoIndividualRango(string prueba)
        {
            bool estado = PuntosNegocios.getInstance().ObtenerEstadoTareaRangos();
            if (estado == false)
            {
                PuntosNegocios.getInstance().ActualizarEstadoTareaRango(true);
                List<PuntosComisiones.DatosIndividualRango> ListaSocios = PuntosNegocios.getInstance().ListaSociosRangoIndividual();
                List<string> LFactorRango = new List<string>();
                foreach (var item in ListaSocios)
                {
                    string DOC = item.Documento;
                    LFactorRango.Add(DOC);

                    while (DOC != "")
                    {
                        DOC = ClienteLN.getInstance().ListarFactorComision(DOC);
                        if (DOC != "") { LFactorRango.Add(DOC); }
                    }

                    PuntosNegocios.getInstance().EliminarDatosRangoIndividual(item.IdCalculo);
                }
                PuntosNegocios.getInstance().ActualizarEstadoTareaRango(false);
                LFactorRango = LFactorRango.Distinct().ToList();
                int iR = 0;
                int periodo = ListaSocios[0].IdPeriodo;
                foreach (var rango in LFactorRango)
                {

                    GenerarRangoIndividual(LFactorRango[iR], periodo);
                    iR++;
                }
            }

            return "2xx";
        }

        [WebMethod]
        public string EliminarComprasExpiradasCDR(string prueba)
        {
            List<Cliente.CDR.DatosPagoEfectivoCDR> Lista = CdrLN.getInstance().ListaComprasExpiradasCDR();
            foreach (var item in Lista)
            {
                RecuperarStockCDR(item.IdDatos, item.DniCDR);
                CdrLN.getInstance().EliminarDatosComprasExpiradasCDR(item.IdDatos);
            }
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        private void RecuperarStockCDR(int idPedido, string documento)
        {
            List<StockCDR> ListaPedido = StockLN.getInstance().ListarProductosxSolicitud(idPedido);

            foreach (var item in ListaPedido)
            {
                bool recuperar = StockLN.getInstance().IncrementarStock(documento, item.IDProductoXPais, item.Cantidad);
            }
        }

        private string ObtenerIdopCompra(string dniComprador, string local, string nombreEnvia, string direccionComprador, string not, string dir, string ubi,
                                         string refe, List<DetalleCompra> listaDetalleCompra, string rucS, string comprobanteS, string CIP)
        {
            List<List<string>> listProductos = new List<List<string>>();
            dniComprador = dniComprador.Substring(0, 8);
            direccionComprador = direccionComprador.Replace("'", "");
            string idop = "8989898", documentoF = "";
            int numPromo = 100;

            try
            {
                foreach (var item in listaDetalleCompra)//22 y 56
                {
                    int cantC = 0;
                    string regalo = "0";
                    double sendMoney = 0.0000;
                    double precioSend = 0.0000;

                    if (item.IdProductoPeruShop == "110038")
                    {

                        precioSend = 6.00;
                        List<string> productos = new List<string>();
                        productos.Add("2069");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("1");
                        productos.Add("0");
                        productos.Add("2069");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        cantC = item.CantiPS * 2;
                        double precioSend2 = 7.00;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108683");
                        productos2.Add(Convert.ToString(cantC));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("2");
                        productos2.Add("0");
                        productos2.Add("108683");
                        productos.Add(regalo);
                        listProductos.Add(productos2);

                    }
                    else if (item.IdProductoPeruShop == "0001")
                    {
                        cantC = item.CantiPS * 12;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110107");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("3");
                        productos.Add("0");
                        productos.Add("110107");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0002")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109949");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("4");
                        productos.Add("0");
                        productos.Add("109949");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0003")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109948");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("5");
                        productos.Add("0");
                        productos.Add("109948");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0004")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("6");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0005")
                    {
                        cantC = 3;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("7");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0006")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("8");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0007")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("9");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0008")
                    {
                        cantC = 10;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("10");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0009")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("11");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0010")
                    {
                        cantC = item.CantiPS * 10;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("12");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0011")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("13");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0012")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("14");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0013")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("15");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0014")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("16");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0015")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("17");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0016")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("18");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0017")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(1));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("19");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(2));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("20");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0018")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(2));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("21");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(4));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("22");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0019")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(4));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("23");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(8));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("24");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0020")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("47");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("25");
                        productos.Add("0");
                        productos.Add("47");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0021")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("255");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("26");
                        productos.Add("0");
                        productos.Add("255");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        double precioSend2 = item.PrecioPS * item.CantiPS;
                        double sendMoney2 = precioSend2 / cantC2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("256");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("27");
                        productos2.Add("0");
                        productos2.Add("256");
                        productos2.Add("0");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0022")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("28");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0023")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110242");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("29");
                        productos.Add("0");
                        productos.Add("110242");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("6063");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("30");
                        productos2.Add("0");
                        productos2.Add("6063");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0024")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("31");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0025")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110324");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("32");
                        productos.Add("0");
                        productos.Add("110324");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0026")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("33");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110342");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("34");
                        productos2.Add("0");
                        productos2.Add("110342");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0029")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("108954");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("52");
                        productos.Add("0");
                        productos.Add("108954");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("311");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("53");
                        productos2.Add("0");
                        productos2.Add("311");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0030")
                    {
                        cantC = 3;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("53");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0031")
                    {
                        cantC = 5;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("54");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0032")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110239");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("55");
                        productos.Add("0");
                        productos.Add("110239");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110395");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("56");
                        productos2.Add("0");
                        productos2.Add("110395");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0033")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("57");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0034")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("58");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0035")
                    {
                        cantC = 6;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("59");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0036")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110243");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("60");
                        productos.Add("0");
                        productos.Add("110243");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0037")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110100");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("61");
                        productos.Add("0");
                        productos.Add("110100");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0038")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("24");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("62");
                        productos.Add("0");
                        productos.Add("24");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0039")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110241");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("63");
                        productos.Add("0");
                        productos.Add("110241");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110415");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("64");
                        productos2.Add("0");
                        productos2.Add("110415");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0040")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("65");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0041")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("66");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0042")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("269");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("67");
                        productos.Add("0");
                        productos.Add("269");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0043")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110240");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("68");
                        productos.Add("0");
                        productos.Add("110240");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110191");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("69");
                        productos2.Add("0");
                        productos2.Add("110191");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0044")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("70");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0045")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("71");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0046")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("72");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0047")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("73");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else
                    {
                        string promo = "";
                        if (item.LineaSend == "05" | item.LineaSend == "02" | item.LineaSend == "01") { numPromo += 1; promo = Convert.ToString(numPromo); }
                        if (item.PrecioPS <= 0.06) { regalo = "1"; precioSend = 0.05; }
                        else { precioSend = item.PrecioPS; }
                        List<string> productos = new List<string>();
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add(promo);
                        productos.Add("0");
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }

                }

                string serializeProds = JsonConvert.SerializeObject(listProductos);
                string prod = serializeProds;
                documentoF = (comprobanteS == "Boleta") ? dniComprador : rucS;
                string idruc = ObtenerIdRUC(documentoF, comprobanteS, nombreEnvia, direccionComprador);
                string idpuntov = ObtenerIdPuntoVenta(local, comprobanteS);

                if (idruc != "")
                {
                    string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                    WebClient wc = new WebClient();
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("a", "pCatalogo");
                    nvc.Add("canal", "RED");
                    nvc.Add("tipo", "2");
                    nvc.Add("fpago", "ECOMERCIO");
                    nvc.Add("pagado", "1");
                    nvc.Add("ruc", documentoF.Trim());
                    nvc.Add("local", local);
                    nvc.Add("localorg", local);
                    nvc.Add("razon", nombreEnvia);
                    nvc.Add("dir_ruc", direccionComprador);
                    nvc.Add("log", "Multinivel");
                    nvc.Add("not", not);
                    nvc.Add("delivery", "0");
                    nvc.Add("dir", dir);
                    nvc.Add("ubi", ubi);
                    nvc.Add("ref", refe);
                    nvc.Add("moneda", "PEN");
                    nvc.Add("prod", prod);
                    nvc.Add("comprobante", "1");
                    nvc.Add("idpuntov", idpuntov);
                    nvc.Add("idruc", idruc);
                    nvc.Add("fpagodet", CIP);

                    var data = wc.UploadValues(url, "POST", nvc);
                    var responseString = UnicodeEncoding.UTF8.GetString(data);
                    idop = (responseString != "") ? responseString : "8989898";
                }
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return idop;
        }

        private string ObtenerIdopCompraCDR(string dniComprador, List<StockCDR> ListaPedido, int IdPedido, string tipoCompra)
        {
            List<List<string>> listProductos = new List<List<string>>();
            List<Cliente.CDR.DatosPersonalesCDR> Lista = CdrLN.getInstance().ListaDatosPersonalesCDR(dniComprador);
            List<Cliente.CDR.DatosCompraPE_IDOP> Datos = CdrLN.getInstance().ListaDatosCompraPE_IDOP(IdPedido);
            string NombreCompleto = Lista[0].Nombres + " " + Lista[0].ApellidoPat + " " + Lista[0].ApellidoMat;
            string direccionComprador = Datos[0].Direccion.Replace("'", "");
            string Nota = Datos[0].IdPeruShop + " - " + Datos[0].FechaPedido + " - PagoEfectivo - " + Datos[0].CIP + " "+tipoCompra;
            string idop = "";
            string localEnv = (tipoCompra == "9") ? Datos[0].IdPeruShop : "RedAndina";
            string canal = (tipoCompra == "9") ? "RED3" : "RED2";
            string documento = (tipoCompra == "9") ? Datos[0].DocExtorno : Lista[0].Documento;
            NombreCompleto = (tipoCompra == "9") ? Datos[0].RazonExtorno : NombreCompleto;
            direccionComprador = (tipoCompra == "9") ? Datos[0].DirExtorno : direccionComprador;

            // san isidro a planta

            try
            {
                foreach (var item in ListaPedido)//22 y 56
                {
                    double precioCDR = StockLN.getInstance().ObtenerPrecioCdrXIDPP(item.IDProductoXPais);
                    List<string> productos = new List<string>();
                    productos.Add(item.IDPS);
                    productos.Add(Convert.ToString(item.Cantidad));
                    productos.Add(precioCDR.ToString("N2").Replace(",", "."));
                    productos.Add("0");
                    productos.Add("0");
                    productos.Add(item.IDPS);
                    productos.Add("0");
                    listProductos.Add(productos);
                }

                string serializeProds = JsonConvert.SerializeObject(listProductos);
                string prod = serializeProds;

                string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("a", "pCatalogo");
                nvc.Add("canal", canal);
                nvc.Add("tipo", "2");
                nvc.Add("fpago", "ECOMERCIO");
                nvc.Add("pagado", "1");
                nvc.Add("ruc", documento);
                nvc.Add("local", localEnv);
                nvc.Add("localorg", localEnv);
                if(tipoCompra != "9") { nvc.Add("localdst", Datos[0].IdPeruShop); }
                nvc.Add("razon", NombreCompleto);
                nvc.Add("dir_ruc", direccionComprador);
                nvc.Add("log", "CDR");
                nvc.Add("not", Nota);
                nvc.Add("delivery", "0");
                nvc.Add("dir", "");
                nvc.Add("ubi", "");
                nvc.Add("ref", "");
                nvc.Add("moneda", "PEN");
                nvc.Add("prod", prod);

                var data = wc.UploadValues(url, "POST", nvc);
                var responseString = UnicodeEncoding.UTF8.GetString(data);
                idop = (responseString != "") ? responseString : "8989898";
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return idop;
        }

        private void Comisiones_Linea_CDR(List<DetalleCompra> listaDetalleCompra, string CDR, DateTime fechaActual)
        {
            int IDPeriodoCDR = 0, i2 = 0;
            bool okCDR = false;
            List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR = CdrLN.getInstance().ListaPeriodoCDR();
            while (!okCDR && i2 < ListaPeriodoCDR.Count())
            {
                DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                if (fechaActual >= primerDiafechaActual & fechaActual < ultimoDiafechaActual)
                {
                    okCDR = true; IDPeriodoCDR = ListaPeriodoCDR[i2].IdPeriodo;
                }
                i2++;
            }

            foreach (var item in listaDetalleCompra)
            {
                List<ProductoPais.Linea_PrecioCDR> ListaLineaPrecioCDR = ProductoLN.getInstance().ObtenerLineaPrecioCDR_By_IdProductoPais(item.CodigoProductoDC);
                string DniCDR = CdrLN.getInstance().ObtenerDniCDR_By_IDPSCDR(CDR);
                double porcentajeLinea = ProductoLN.getInstance().ObtenerPorcentajeLineaCDR(DniCDR, ListaLineaPrecioCDR[0].idLinea) / 100;
                Cliente.CDR.ComisionLineaCDR objCDR = new Cliente.CDR.ComisionLineaCDR()
                {
                    Comision = (ListaLineaPrecioCDR[0].PrecioCDR * porcentajeLinea) * item.Cantidad,
                    LineaCredito = item.Cantidad * ListaLineaPrecioCDR[0].PrecioCDR,
                    IDPeriodoCDR = IDPeriodoCDR,
                    DniCDR = DniCDR
                };
                bool ActualizarComisionLinea = CdrLN.getInstance().ActualizarComisionLineaCDR(objCDR);
            }
        }

        private void EnviarArchivoCSV(List<StockCDR> ListaPedido, string idop)
        {
            string fileName = Path.Combine(Server.MapPath("~/csv/"), idop + ".csv");
            using (var file = File.CreateText(fileName))
            {
                foreach (var item in ListaPedido)
                {
                    string cantidad = (item.Cantidad / StockLN.getInstance().UnidadPorPresentacionXProducto(item.IDProducto)).ToString();
                    file.Write(item.IDPS + "," + cantidad);
                    file.WriteLine();
                }
            }
        }

        private void actualizarPuntos(string dni, string dniFactorComision, string dniPatrocinador, string packetesocio, Compra compraPC, DateTime fechaSimple, string idCliente)
        {
            double PP = compraPC.PuntosTotal;
            double VIP = compraPC.PuntosTotal;
            double VP = compraPC.PuntosTotalPromo;
            double VR = compraPC.PuntosTotalPromo;
            double VG = compraPC.PuntosTotalPromo;
            string DOC = dni.Trim();
            int i = 0;
            int i2 = 0;
            int IDPeriodo = 0;
            int IDPeriodoComision = getPeriodoRango_Comision(fechaSimple);
            int IdPeriodoRango_Comi = PeriodoLN.getInstance().ObtenerIdperiodoRango_Comi(IDPeriodoComision);
            List<string> LFactorComision = new List<string>();
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarIdPeriodoTop2();
            bool ok = false;

            while (!ok && i2 < ListaPeriodos.Count())
            {
                DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                if (fechaSimple >= primerDiafechaActual & fechaSimple < ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodo;
                }
                i2++;
            }

            while (DOC != "")
            {
                DOC = ClienteLN.getInstance().ListarFactorComision(DOC);
                if (DOC != "") { LFactorComision.Add(DOC); }
            }

            bool todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(dni, 0, VP, 0, VG, 0, IDPeriodo);
            todoPC = CompraNegocios.getInstance().ActualizarPuntosComisiones(dni, PP, 0, 0, 0, 0, IdPeriodoRango_Comi);
            bool PVIP = CompraNegocios.getInstance().ActualizarPuntosVIP(dni, VIP, IdPeriodoRango_Comi);
            CompraNegocios.getInstance().ActualizarCorazonesRed(IdPeriodoRango_Comi, idCliente, compraPC.Corazones);

            if ((packetesocio == "05" | packetesocio == "06") & dniFactorComision != "")
            {
                bool VPP = CompraNegocios.getInstance().ActualizarPuntosComisiones(dniPatrocinador, 0, VP, 0, 0, 0, IDPeriodo);
                bool VIPP = CompraNegocios.getInstance().ActualizarPuntosVIP(dniPatrocinador, VIP, IdPeriodoRango_Comi);
            }

            foreach (var item in LFactorComision)
            {
                if ((packetesocio == "05" | packetesocio == "06") & i == 0)
                { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, 0, VG, 0, IDPeriodo); }
                else { bool VGP = CompraNegocios.getInstance().ActualizarPuntosComisiones(LFactorComision[i], 0, 0, VR, VG, 0, IDPeriodo); }
                i++;
            }

            if (packetesocio != "05" & packetesocio != "06")
            {
                bool VPR = CompraNegocios.getInstance().ActualizarVPR(dniFactorComision, IDPeriodo);
            }
            PuntosNegocios.getInstance().RegistrarDatosRangoIndividual(dni, IDPeriodo);
        }

        private void actualizarComisiones(string dni, string dniFactorComision, string dniPatrocinador, string packetesocio, Compra compraPC, DateTime fechaSimple, string idTPC, string tipoCliente)
        {
            double MCCI = compraPC.montoComisionCI;
            double MCCA = compraPC.montoComision;
            double MCCU = compraPC.montoComision;
            double MCCON = compraPC.montoComision * 0.05;
            string TCL = tipoCliente;
            int IDPeriodo = getPeriodoRango_Comision(fechaSimple);
            List<string> LFactorComision = new List<string>();
            string fechaRegistro = ClienteLN.getInstance().ObtenerFechaRegistro(dniPatrocinador);
            double montoTiburon = CompraNegocios.getInstance().ObtenerMontoTiburon(dniPatrocinador);
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            DateTime fechaConvertida = DateTime.ParseExact(fechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
            DateTime primerDia = new DateTime(fechaConvertida.Year, fechaConvertida.Month, 1);
            DateTime ultimoDia = primerDia.AddMonths(1).AddDays(-1);
            DateTime DiaMenos5 = ultimoDia.AddDays(-5);
            DateTime PrimerDiaSig = ultimoDia.AddDays(1);

            double PuntosPatro = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(dniFactorComision, IDPeriodo);
            int datoTiburon = PuntosNegocios.getInstance().ObtenerEvaluacionTiburon(dniPatrocinador, IDPeriodo);
            /*COMISION POR CONSULTOR*/
            if (packetesocio == "05" & dniFactorComision != "")
            { if (PuntosPatro >= 20) { bool COM = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniFactorComision, 0, 0, MCCON, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo); } }

            /*COMISION POR CLIENTE INTELIGENTE*/
            if (packetesocio == "06" & dniFactorComision != "")
            { if (PuntosPatro >= 20) { bool COM = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniFactorComision, 0, MCCI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo); } }

            /*COMISION POR AFILIACIONES*/
            if (idTPC != "05" & idTPC != "06" & idTPC != "07")
            {
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = dni;
                List<ComAfiliacion> ListaPatrocinadores = new List<ComAfiliacion>();
                while (cc < 5 && !terminado)
                {
                    cc++;
                    ComAfiliacion agregar = ClienteLN.getInstance().ListaDatosPatrocinioAfiliaciones(IDPeriodo, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaPatrocinadores.Add(agregar); doc = agregar.documento.Trim(); }
                }
                foreach (var item in ListaPatrocinadores)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item.documento, IDPeriodo);
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "01" | ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 0)
                    {
                        double COMAFI = MCCA * 0.30;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, COMAFI, 0, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 1)
                    {
                        double COMAFI = MCCA * 0.05;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, COMAFI, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 2)
                    {
                        double COMAFI = MCCA * 0.03;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, COMAFI, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 3)
                    {
                        double COMAFI = MCCA * 0.02;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, 0, COMAFI, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 4)
                    {
                        double COMAFI = MCCA * 0.01;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDPeriodo, 0, 0, 0, 0, COMAFI);
                    }

                    ccc++;
                }
            }

            /*COMISION BONO TIBURON*/
            if (idTPC == "04" | idTPC == "23" | idTPC == "10" | idTPC == "12" | idTPC == "13" |
                idTPC == "24" | idTPC == "25" | idTPC == "26" | idTPC == "27")
            {
                if (PuntosPatro >= 20)
                {
                    if (datoTiburon < 2)
                    {
                        if (datoTiburon == 0)
                        {
                            if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                            {
                                string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                if (montoTiburon == 360)
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 0);
                                    }
                                }

                            }
                            else
                            {
                                string fecIniPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaInicio.Trim()).FirstOrDefault();
                                string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                if (montoTiburon == 360)
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 0);
                                    }
                                }

                            }
                        }
                        else if (datoTiburon == 1)
                        {
                            if (montoTiburon == 0)
                            {
                                if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                                {
                                    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 1);
                                    }
                                }
                                else
                                {
                                    string fecIniPe = Convert.ToString(PrimerDiaSig.ToShortDateString());
                                    string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 720, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                    else if (cantAfi >= 3)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 1);
                                    }
                                }
                            }
                            else if (montoTiburon == 360)
                            {
                                if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                                {
                                    string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    string RangoInicio = Convert.ToString(DiaMenos5.ToShortDateString());
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, RangoInicio, RangoFin);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                                else
                                {
                                    string fecIniPe = Convert.ToString(PrimerDiaSig.ToShortDateString());
                                    string fecFinPe = (from c in ListaPeriodos where c.idPeriodoComision == IDPeriodo select c.fechaFin.Trim()).FirstOrDefault();
                                    int cantAfi = ClienteLN.getInstance().CantidadAfiliacionesGeneradas(dniPatrocinador, fecIniPe, fecFinPe);
                                    if (cantAfi >= 6)
                                    {
                                        bool COAFI = CompraNegocios.getInstance().ActualizarComisionesAutomatico(dniPatrocinador, 0, 0, 0, 0, 360, 0, 0, 0, 0, 0, 0, 0, 0, IDPeriodo);
                                        bool updt = ClienteLN.getInstance().ActualizarEstadoTiburon(dniPatrocinador, 2);
                                    }
                                }
                            }
                        }
                    }
                }

            }

            /*COMISION BONO UNILEVEL -- ES UPLINE NO PATROCINADOR*/
            if (idTPC == "05" | idTPC == "06" | idTPC == "07")
            {
                if (TCL == "03") { MCCU = compraPC.montoComision - compraPC.montoComisionCI; }
                if (TCL == "05") { MCCU = compraPC.montoComision - MCCON; }
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = dni; if (TCL == "03" | TCL == "05") { doc = dniPatrocinador; ccc = 1; }
                double COMUNI = 0;
                List<PatrocinadorUnilivel> ListaUpline = new List<PatrocinadorUnilivel>();

                while (cc < 15 && !terminado)
                {
                    cc++;
                    PatrocinadorUnilivel agregar = ClienteLN.getInstance().ListaDatosUplineUnilevel(IDPeriodo, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaUpline.Add(agregar); doc = agregar.documento.Trim(); }
                }

                foreach (var item2 in ListaUpline)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDPeriodo);
                    if (PP >= 20 & ccc == 0 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.05;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 1 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 2 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 3 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 4 & (item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 5 & (item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.04;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 6 & (item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.03;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 7 & (item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.02;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 8 & (item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 9 & (item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 10 & (item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 11 & (item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 12 & (item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 13 & (item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 14 & (item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 15 & (item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 16 & (item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 17 & (item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 18 & (item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDPeriodo, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    ccc++;
                }
            }

        }

        private int getPeriodoRango_Comision(DateTime fechaSimple)
        {
            int i2 = 0;
            bool ok = false;
            int IDPeriodo = 0;
            DateTime primerDiafechaActual = DateTime.Now;
            DateTime ultimoDiafechaActual = DateTime.Now;
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            while (!ok && i2 < ListaPeriodos.Count())
            {
                primerDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodos[i2].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                if (fechaSimple >= primerDiafechaActual & fechaSimple < ultimoDiafechaActual)
                {
                    ok = true; IDPeriodo = ListaPeriodos[i2].idPeriodoComision;
                }
                i2++;
            }
            return IDPeriodo;
        }

        public bool SendMailPE(string DNI, string estableMail, string fechaS, string montoEf, string notaDelivery, string idopPShop,
                                string nombreEnviar, string CIP, string fechaPagada, string NumeroOperacion, string comprobante, string NotaPS)
        {
            bool estado = true;
            String merror;
            string correo = "";
            if (estableMail == "AlmacenSUR")
            {
                correo = "compras.multinivel@santanaturaperu.net";
            }
            else if (estableMail == "OlivosRed")
            {
                correo = "cdr.losolivos@santanaturaperu.net";
            }
            else
            {
                correo = "compras.multinivel@santanaturaperu.net";
            }

            bool ok = true;
            MailMessage mail = new MailMessage();
            mail.To.Add(new MailAddress(correo));
            mail.CC.Add(new MailAddress("pedidos.snn@gmail.com"));
            mail.CC.Add(new MailAddress("santanaturavouchers@gmail.com"));
            mail.From = new MailAddress("santanaturavoucher@mundosantanatura.com", "Santa Natura Voucher", System.Text.Encoding.UTF8);
            mail.Subject = "Nuevo Pago Efectico de " + nombreEnviar + " Establecimiento: " + estableMail;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = string.Format("Fecha de Creacion: {1}{0}Monto: {2}{0}TipoPago: {3}{0}Nota Delivery: {4}{0}DNI: {5}{0}IDOP: {6}{0}CIP: {7}{0}Numero Operacion: {8}{0}TipoComprobante: {9}{0}Fecha de Pago: {10}{0}Tipo de Compra: {11}{0}",
                                       Environment.NewLine, fechaS, montoEf, "Pago Efectivo", notaDelivery, DNI, idopPShop, CIP, NumeroOperacion, comprobante, fechaPagada, NotaPS);
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;

            SmtpClient smtp = new SmtpClient("mundosantanatura.com", 25);
            smtp.Credentials = new System.Net.NetworkCredential("santanaturavoucher@mundosantanatura.com", "s@nt@2019");
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            try
            {
                smtp.Send(mail);
            }
            catch (SmtpException ex)
            {
                estado = false;
                bool registro = ClienteLN.getInstance().RegistroModificaciones("73011583", DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Error Sistema", Convert.ToString(ex));
                merror = ex.Message.ToString();
            }

            return ok;
        }

        private string ObtenerIdRUC(string documento, string comprobante, string nombreDNI, string direccionDNI)
        {
            string idruc = "", Nombre = "", Direccion = "", Mail = "", Tipo = "";
            Tipo = (comprobante == "Boleta") ? "1" : "6";
            idruc = CompraNegocios.getInstance().ConsultaIdRUC(documento);

            if (idruc == "")
            {
                if (comprobante == "Factura")
                {
                    Cliente.DatosFacturaPS DatosEnviar = DatosFacturaWS(documento);
                    if (DatosEnviar != null)
                    {
                        Nombre = DatosEnviar.Nombre;
                        Direccion = DatosEnviar.Direccion.Replace("'", "");
                    }
                }
                else
                {
                    Nombre = nombreDNI;
                    Direccion = direccionDNI;
                }
                if (Nombre != "")
                {
                    string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                    WebClient wc = new WebClient();
                    NameValueCollection nvc = new NameValueCollection();
                    nvc.Add("a", "addruc");
                    nvc.Add("login", documento);
                    nvc.Add("ruc", documento);
                    nvc.Add("razon", Nombre);
                    nvc.Add("dir_ruc", Direccion);
                    nvc.Add("tipod", Tipo);
                    nvc.Add("mail", Mail);

                    var data = wc.UploadValues(url, "POST", nvc);
                    var responseString = UnicodeEncoding.UTF8.GetString(data);
                    idruc = responseString;
                    if (documento != "") { bool IdR = CompraNegocios.getInstance().RegistroIdRuc(documento, idruc, DateTime.Now.AddHours(-2)); }
                }
            }

            return idruc;
        }

        private string ObtenerIdPuntoVenta(string cdrVenta, string Comprobante)
        {
            string idpuntoVenta = "", Serie = "", Sigla = "";
            Sigla = (Comprobante == "Factura") ? "F" : "B";
            Serie = Sigla + CdrLN.getInstance().ObtenerSerieCDR(cdrVenta).Trim();
            List<Cliente.CDR.DatoSeriePS> ListaPS = new List<Cliente.CDR.DatoSeriePS>();

            string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("a", "puntosvemta");
            nvc.Add("local", cdrVenta);

            var data = wc.UploadValues(url, "POST", nvc);
            var responseString = UnicodeEncoding.UTF8.GetString(data);
            JavaScriptSerializer js = new JavaScriptSerializer();
            Cliente.CDR.ListaCDRPuntoVenta resultado = js.Deserialize<Cliente.CDR.ListaCDRPuntoVenta>(responseString);
            foreach (object[] item in resultado.puntos)
            {
                Cliente.CDR.DatoSeriePS PS = new Cliente.CDR.DatoSeriePS();
                PS.TipoDoc = item[0].ToString();
                PS.SerieDoc = item[1].ToString();
                PS.IdPuntoVenta = item[2].ToString();
                ListaPS.Add(PS);
            }
            idpuntoVenta = (from c in ListaPS where c.SerieDoc.Trim() == Serie select c.IdPuntoVenta.Trim()).FirstOrDefault();

            return idpuntoVenta;
        }

        private bool ValidacionNumero(string num)
        {
            try
            {
                Int32.Parse(num);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private Cliente.DatosFacturaPS DatosFacturaWS(string documento)
        {
            Cliente.DatosFacturaPS Datos = new Cliente.DatosFacturaPS();

            WebRequest request = WebRequest.Create("https://servicio.apirest.pe/api/getRuc");
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Authorization", "Bearer " + "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgzMmUzYTI3ZDA0YzI2ZjFmNTIwNTJhMjE5ODRmOTI1NDY2ZmM1YjA2MzcyOWNiNDc4NjExM2VlYzRjZDA5NjY2OTRjNzEzY2U5YTNjNmMyIn0.eyJhdWQiOiIxIiwianRpIjoiODMyZTNhMjdkMDRjMjZmMWY1MjA1MmEyMTk4NGY5MjU0NjZmYzViMDYzNzI5Y2I0Nzg2MTEzZWVjNGNkMDk2NjY5NGM3MTNjZTlhM2M2YzIiLCJpYXQiOjE1ODk5Mjc5MTAsIm5iZiI6MTU4OTkyNzkxMCwiZXhwIjoxOTA1NDYwNzEwLCJzdWIiOiIxMDcyIiwic2NvcGVzIjpbIioiXX0.b_D5SKP8vifhZ38XY88NLKoO_0djb42S8KRqG76ZRLoSYI8f_Y4hEbGiyA0lmEk1miTd5I7_qbE7VPYIYpwy4CxaEN2lVJaqyTTnjupALsDiV0i2a5JMZ5kklXs-HdE6m7Ikovax-ZREcRMTKHxcpAIznYvcL-j3lutmlXRLoUVpRdyWFpVLGpWLagxhu7wHIrURW3ssGZPZCcv6hci3Xs9y8iJUKCt1AU1oNRBiwoL25O-4740jrQRzLLX2PP5KnHsFVNxy79y4LeC4zlhpPmpAuiJKi4KnHj4m3A0oBy9mql8rlCXzGGK9pyd8XjiO4gpiqa82xaYSawk6F6M6h8RkVAaEXZKeFuofDIQjKsp0QClw6egRA24TA-j_ZyvzDX5HzTG6y_1-H1zjPhSmVPH-3fsU8YZ52EFQcHJ28m3AaEDR9NOmaOgy-nLEIvAjunFMc67tO8A6f4I6QInnpcexW4S6FHcDT3Dws3v38BBCac31Pkmn4OKNl4ymIX37h-cztmBKC-tUDFI5Xssy26GxRPt-7NRlO-3BSU9HPEGke855TaigjnObSf6opHUxR9NjDzqVecsEAtNsz9cgozX_EqyGhRiSH2YwnYFOp-IU22v3SI1VNu0ijd263pUhVFfH1JM1OtqWVU8-vOLSC68tvFsF0ylS2HT3LO96U2Q");
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                var obj = new DatosFactura.rucC(documento);
                string json = JsonConvert.SerializeObject(obj);
                streamWriter.Write(json);
            }
            HttpWebResponse httpResponse;
            var result = "";
            DatosFactura.WS resOK = new DatosFactura.WS();
            try
            {
                httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                resOK = JsonConvert.DeserializeObject<DatosFactura.WS>(result);
                Datos.Nombre = (resOK.result.direccion != "-") ? resOK.result.razonSocial : "";
                Datos.Direccion = resOK.result.direccion + " / " + resOK.result.distrito + " / " + resOK.result.provincia + " / " + resOK.result.departamento;
                Datos.Mail = "";
            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                Datos = null;
            }

            return Datos;
        }

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        private string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

        [WebMethod]
        public string EliminarSocioSinCompra(string prueba)
        {
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteSinCompras();
            foreach (var item in Lista)
            {
                int cantDirectos = ClienteLN.getInstance().ConsultaDirectosGeneral(item.numeroDoc.Trim());
                if (cantDirectos == 0)
                {
                    bool eliminar = CompraNegocios.getInstance().EliminarClienteSinCompras(item.idCliente);
                }
            }
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ReduccionHorasLimitePagoEfectivo(string prueba)
        {
            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(3);
            int limiteSend = listaFecha[0].numeroDias - 1;
            LimiteDias objLimite = new LimiteDias()
            {
                idLimite = 3,
                numeroDias = Convert.ToInt32(limiteSend)
            };
            bool ok = FechasLimiteLN.getInstance().ActualizarLimite(objLimite);
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarMaximoRango(string prueba)
        {
            List<Cliente> Lista = ClienteLN.getInstance().ListarClientesMapaRed();
            foreach (var item in Lista)
            {
                Periodo.MaximoRango MaxRango = PuntosNegocios.getInstance().ObtenerRangoMaximoXSocio(item.numeroDoc);
                bool update = PuntosNegocios.getInstance().ActualizarMaximoRango(item.numeroDoc, MaxRango.rango.Trim(), MaxRango.idRango);
            }
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public List<string> AutcompletePatro(string palabra)
        {
            listaNombreSocio = ClienteLN.getInstance().AutocompletePatro(palabra);

            return listaNombreSocio;
        }

        private void GenerarRangoIndividual(string Documento, int IDP)
        {
            List<PuntosComisiones> ListaDirectos = null;
            List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();
            List<PuntosComisiones> ListaDatosIndividual = PuntosNegocios.getInstance().ListaDatosRangoIndividual(Documento, IDP);

            int canPPDirectos = 0;
            double VPRSocio = ListaDatosIndividual[0].VolumenRed;
            double PP = ListaDatosIndividual[0].PuntosPersonales;
            double VP = ListaDatosIndividual[0].VolumenPersonal;
            double VQ = 0;
            string RangoDefinido = "";
            int IDRangoDefinido = 1;
            int i = 0;
            bool ok = false;
            ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPRPeriodo(Documento, IDP);
            canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectosPeriodo(Documento, IDP);
            List<PuntosComisiones.RecalculoIndividual> DatosRango = null;

            if (PP >= 20)
            {
                if (canPPDirectos >= 2)
                {

                    while (!ok && i < ListaRangos.Count())
                    {
                        int CS1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                                                    (i == 12) ? 2 : 0;

                        if (ListaDirectos.Count() >= CS1)
                        {
                            DatosRango = ObtenerRangoIndividual(VPRSocio, ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP,
                                                            ListaRangos[i].Nombre, IDRangoDefinido = ListaRangos[i].IdRango, i, IDP);
                            ok = DatosRango[0].Estado;
                        }
                        i++;
                    }
                    RangoDefinido = DatosRango[0].Rango; IDRangoDefinido = DatosRango[0].IdRango; VQ = DatosRango[0].VQ;
                }
                else { RangoDefinido = "EMPRENDEDOR"; IDRangoDefinido = 1; }
            }
            else { RangoDefinido = "NO ACTIVO"; IDRangoDefinido = 0; }

            bool update = PuntosNegocios.getInstance().ActualizarEstadoByPeriodo(Documento, RangoDefinido, IDP, IDRangoDefinido);
            bool AVQ = PuntosNegocios.getInstance().ActualizarVQPeriodo(Documento, VQ, IDP);
        }

        private List<PuntosComisiones.RecalculoIndividual> ObtenerRangoIndividual(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos,
                                                                                  double VP, string rango, int idRango, int i, int IDP)
        {
            List<PuntosComisiones.RecalculoIndividual> RangoVQ = new List<PuntosComisiones.RecalculoIndividual>();
            bool Validar = false;
            double VPRA = 0;

            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VP = (VP >= PML) ? PML : VP;
                VRA = VRA + VP;
                if (VRA >= PR) { Validar = true; VPRA = VRA; }
            }

            if (Validar == true)
            {
                int CR1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                                                    (i == 12) ? 2 : 0;
                int CR2 = (i == 5) ? 1 : 0;
                Validar = ValidarLineasCalificadas(i, CR1, CR2, Directos, IDP);
                VPRA = (Validar == false) ? 0: VPRA;
            }

            PuntosComisiones.RecalculoIndividual Datos = new PuntosComisiones.RecalculoIndividual
            {
                Estado = Validar,
                VQ = VPRA,
                Rango = rango,
                IdRango = idRango
            };
            RangoVQ.Add(Datos);

            return RangoVQ;
        }

        [WebMethod]
        public string GeneraRangoCompleto(string prueba)
        {
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteCalculoPuntos();
            List<PuntosComisiones> ListaDirectos = null;
            List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();

            foreach (var item in Lista)
            {
                int canPPDirectos = 0;
                double VPRSocio = item.VPR;
                double PP = item.PP;
                double VP = item.VP;
                string RangoDefinido = "";
                int IDRangoDefinido = 1;
                int i = 0;
                bool ok = false;
                ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPR(item.numeroDoc);
                canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectos(item.numeroDoc);

                if (PP >= 20)
                {
                    if (canPPDirectos >= 2)
                    {
                        while (!ok && i < ListaRangos.Count())
                        {
                            int CS1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                                                    (i == 12) ? 2 : 0;

                            if (ListaDirectos.Count() >= CS1)
                            {
                                ok = ObtenerRango(VPRSocio, ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP, i, item.numeroDoc);
                                if (ok == true) { RangoDefinido = ListaRangos[i].Nombre; IDRangoDefinido = ListaRangos[i].IdRango; }
                            }
                            i++;
                        }
                    }
                    else { RangoDefinido = "EMPRENDEDOR"; IDRangoDefinido = 1; }
                }
                else { RangoDefinido = "NO ACTIVO"; IDRangoDefinido = 0; }

                bool update = PuntosNegocios.getInstance().ActualizarEstado(item.numeroDoc, RangoDefinido, IDRangoDefinido);
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        private bool ValidarLineasCalificadasGeneral(int posicion, int CS1, int CS2, List<PuntosComisiones> Directos)
        {
            int sumaCS1 = 0, sumaCS2 = 0;
            bool resultado = false;
            List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRango = null;

            foreach (var item in Directos)
            {
                ListaUplinesRango = PuntosNegocios.getInstance().ListaUplinesRango(item.dni);
                if (ListaUplinesRango.Count() > 0)
                {
                    if (posicion == 0 && ListaUplinesRango[0].canSuperiorIM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 1 && ListaUplinesRango[0].canSuperiorTDM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 2 && ListaUplinesRango[0].canSuperiorDDM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 3 && ListaUplinesRango[0].canSuperiorDM >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 4 && ListaUplinesRango[0].canSuperiorTD >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 5 && (ListaUplinesRango[0].canSuperiorTD >= 1 || ListaUplinesRango[0].canSuperiorDD >= 1))
                    {
                        if (ListaUplinesRango[0].canSuperiorTD >= 1) { sumaCS1 += 1; }
                        if (ListaUplinesRango[0].canSuperiorDD >= 1) { sumaCS2 += 1; }
                    }
                    else if (posicion == 6 && ListaUplinesRango[0].canSuperiorDD >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 7 && ListaUplinesRango[0].canSuperiorD >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 8 && ListaUplinesRango[0].canSuperiorR >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 9 && ListaUplinesRango[0].canSuperiorZ >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 10 && ListaUplinesRango[0].canSuperiorO >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 11 && ListaUplinesRango[0].canSuperiorP >= 1)
                    {
                        sumaCS1 += 1;
                    }
                    else if (posicion == 12 && ListaUplinesRango[0].canSuperiorB >= 1)
                    {
                        sumaCS1 += 1;
                    }
                }
            }

            resultado = (sumaCS1 >= CS1 && sumaCS2 >= CS2) ? true : false;
            return resultado;
        }

        private bool ValidarLineasCalificadas(int posicion, int CS1, int CS2, List<PuntosComisiones> Directos, int IDP)
        {
            int sumaCS1 = 0, sumaCS2 = 0;
            bool resultado = false;
            List<PuntosComisiones.CantidadRangoMapaRED> ListaUplinesRango = null;

            foreach (var item in Directos)
            {
                if (CS1 > 0) {
                    ListaUplinesRango = PuntosNegocios.getInstance().ListaUplinesRangoPeriodo(item.dni, IDP);
                    if (ListaUplinesRango.Count() > 0)
                    {
                        if (posicion == 0 && ListaUplinesRango[0].canSuperiorIM >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 1 && ListaUplinesRango[0].canSuperiorTDM >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 2 && ListaUplinesRango[0].canSuperiorDDM >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 3 && ListaUplinesRango[0].canSuperiorDM >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 4 && ListaUplinesRango[0].canSuperiorTD >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 5 && (ListaUplinesRango[0].canSuperiorTD >= 1 || ListaUplinesRango[0].canSuperiorDD >= 1))
                        {
                            if (ListaUplinesRango[0].canSuperiorTD >= 1) { sumaCS1 += 1; }
                            if (ListaUplinesRango[0].canSuperiorDD >= 1) { sumaCS2 += 1; }
                        }
                        else if (posicion == 6 && ListaUplinesRango[0].canSuperiorDD >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 7 && ListaUplinesRango[0].canSuperiorD >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 8 && ListaUplinesRango[0].canSuperiorR >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 9 && ListaUplinesRango[0].canSuperiorZ >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 10 && ListaUplinesRango[0].canSuperiorO >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 11 && ListaUplinesRango[0].canSuperiorP >= 1)
                        {
                            sumaCS1 += 1;
                        }
                        else if (posicion == 12 && ListaUplinesRango[0].canSuperiorB >= 1)
                        {
                            sumaCS1 += 1;
                        }
                    }
                }
            }

            resultado = (sumaCS1 >= CS1 && sumaCS2 >= CS2) ? true: false;
            return resultado;
        }

        private bool ObtenerRango(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos, double VP, int i, string documento)
        {

            bool Rango = false;
            double VPRA = 0;
            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VP = (VP >= PML) ? PML : VP;
                VRA = VRA + VP;
                if (VRA >= PR) { Rango = true; VPRA = VRA; }
            }

            if (Rango == true)
            {
                int CR1 = (i == 0) ? 5 : (i == 1) ? 4 : (i == 2) ? 4 :
                                                    (i == 3) ? 4 : (i == 4) ? 4 : (i == 5) ? 2 :
                                                    (i == 6) ? 3 : (i == 7) ? 3 : (i == 8) ? 3 :
                                                    (i == 9) ? 3 : (i == 10) ? 3 : (i == 11) ? 2 :
                                                    (i == 12) ? 2 : 0;
                int CR2 = (i == 5) ? 1 : 0;
                Rango = ValidarLineasCalificadasGeneral(i, CR1, CR2, Directos);
            }

            if (Rango == true)
            {
                bool AVQ = PuntosNegocios.getInstance().ActualizarVQ(documento, VPRA);
            }

            return Rango;
        }

        [WebMethod]
        public string GenerarVQ(string prueba)
        {
            string documento = "99999999";
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteRedVQ(documento);
            List<PuntosComisiones> ListaDirectos = new List<PuntosComisiones>();
            foreach (var item in Lista)
            {
                int canPPDirectos = 0;
                double VPRSocio = item.VPR;
                double PP = item.PP;
                double VP = item.VP;
                double VQSocio = 0.0;
                bool ok2 = false;
                ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPR(item.numeroDoc);
                canPPDirectos = PuntosNegocios.getInstance().ListaGenerarPPDirectos(item.numeroDoc);
                if (PP >= 20)
                {
                    if (canPPDirectos >= 2)
                    {
                        List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();
                        int i = 0;
                        while (!ok2 && i < ListaRangos.Count())
                        {
                            VQSocio = CalculoVQ(Convert.ToDouble(VPRSocio), ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP);
                            ok2 = CalculoVQ2(Convert.ToDouble(VPRSocio), ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP);
                            i++;
                        }
                        bool AVQ = PuntosNegocios.getInstance().ActualizarVQ(item.numeroDoc, VQSocio);
                        ok2 = false;
                    }
                    else
                    {
                        bool AVQ1 = PuntosNegocios.getInstance().ActualizarVQ(item.numeroDoc, 0);
                        ok2 = false;
                    }
                }
                else
                {
                    bool AVQ1 = PuntosNegocios.getInstance().ActualizarVQ(item.numeroDoc, 0);
                    ok2 = false;
                }
                
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        private double CalculoVQ(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos, double VP)
        {
            double VPRA = 0;
            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VRA = VRA + VP;
                if (VRA >= PR) { VPRA = VRA; }
            }

            return VPRA;
        }

        private bool CalculoVQ2(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos, double VP)
        {
            double VPRA = 0;
            bool calculo2 = false;
            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VRA = VRA + VP;
                if (VRA >= PR) { calculo2 = true; VPRA = VRA; }
            }

            return calculo2;
        }

        [WebMethod]
        public string ActualizarEstadoTiburon(string prueba)
        {
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodoComisionTop3();
            DateTime fechaActual = DateTime.Now.AddHours(-2);
            bool okI = false;
            int i = 0;
            int IDPeriodoA = 0;
            while (!okI && i < ListaPeriodos.Count())
            {
                DateTime primerDia = DateTime.ParseExact(ListaPeriodos[i].fechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                DateTime ultimoDia = DateTime.ParseExact(ListaPeriodos[i].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture).AddDays(1);
                if (fechaActual >= primerDia & fechaActual < ultimoDia)
                {
                    okI = true; IDPeriodoA = ListaPeriodos[i].idPeriodoComision;
                }
                i++;
            }
            List<Cliente> ListaTiburon = ClienteLN.getInstance().ListarClienteTiburon(IDPeriodoA);


            foreach (var item in ListaTiburon)
            {
                DateTime fechaConvertida = DateTime.ParseExact(item.FechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                int Tiburon = PuntosNegocios.getInstance().ObtenerEvaluacionTiburon(item.numeroDoc, IDPeriodoA);
                if (Tiburon == 1)
                {
                    DateTime primerDia = new DateTime(fechaConvertida.Year, fechaConvertida.Month, 1);
                    DateTime ultimoDia = primerDia.AddMonths(1).AddDays(-1);
                    DateTime DiaMenos5 = ultimoDia.AddDays(-0);
                    if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                    {
                        if (fechaActual >= primerDia.AddMonths(2))
                        {
                            bool updt = ClienteLN.getInstance().ActualizarEvalTiburonFec(item.numeroDoc, 2, IDPeriodoA);
                        }
                    }
                    else if (fechaConvertida >= primerDia & fechaConvertida <= primerDia.AddDays(14))
                    {
                        if (fechaActual >= ultimoDia.AddDays(1))
                        {
                            bool updt = ClienteLN.getInstance().ActualizarEvalTiburonFec(item.numeroDoc, 2, IDPeriodoA);
                        }
                    }
                    else
                    {
                        if (fechaActual > ultimoDia.AddDays(15))
                        {

                            bool updt = ClienteLN.getInstance().ActualizarEvalTiburonFec(item.numeroDoc, 2, IDPeriodoA);
                        }
                    }
                }
                else
                {
                    DateTime primerDia = new DateTime(fechaConvertida.Year, fechaConvertida.Month, 1);
                    DateTime ultimoDia = primerDia.AddMonths(1).AddDays(-1);
                    DateTime DiaMenos5 = ultimoDia.AddDays(-0);
                    if (fechaConvertida >= DiaMenos5 & fechaConvertida <= ultimoDia)
                    {
                        if (fechaActual > ultimoDia.AddDays(15))
                        {
                            bool updt = ClienteLN.getInstance().ActualizarEvalTiburonFec(item.numeroDoc, 1, IDPeriodoA);
                        }
                    }
                    else if (fechaConvertida >= primerDia & fechaConvertida <= primerDia.AddDays(14))
                    {
                        if (fechaActual > primerDia.AddDays(14))
                        {
                            bool updt = ClienteLN.getInstance().ActualizarEvalTiburonFec(item.numeroDoc, 1, IDPeriodoA);
                        }
                    }
                    else
                    {
                        if (fechaActual > ultimoDia.AddDays(1))
                        {

                            bool updt = ClienteLN.getInstance().ActualizarEvalTiburonFec(item.numeroDoc, 1, IDPeriodoA);
                        }
                    }
                }
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarBonoMercadeo(string prueba)
        {

            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteConCompras();

            foreach (var item in ClienteConCompra)
            {
                int evaluacion = PuntosNegocios.getInstance().ObtenerEvaluacionMercadeo(item.numeroDoc);
                List<Periodo> Ultimos4Periodos = PeriodoLN.getInstance().ListarUltimos4Periodos();
                DateTime FecRgistro = DateTime.ParseExact(item.FechaRegistro, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                double pp1 = 0, pp2 = 0, pp3 = 0, pp4 = 0, puntosCiclo = 0;
                int cc = 0;
                bool acMerca = false;

                while (cc < Ultimos4Periodos.Count)
                {
                    cc++;
                    DateTime FecFin = DateTime.ParseExact(Ultimos4Periodos[cc].fechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    if (FecRgistro > FecFin)
                    {
                        puntosCiclo = PuntosNegocios.getInstance().ObtenerPuntosXFechas(item.numeroDoc, Ultimos4Periodos[cc].fechaInicio, Ultimos4Periodos[cc].fechaFin);
                    }
                    if (cc == 0) { pp1 = puntosCiclo; }
                    else if (cc == 1) { pp2 = puntosCiclo; }
                    else if (cc == 2) { pp3 = puntosCiclo; } else if (cc == 3) { pp4 = puntosCiclo; }
                }

                if (evaluacion == 0)
                {
                    if (pp4 >= 800 & pp3 >= 700 & pp2 >= 600 & pp1 >= 500)
                    {
                        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeo(item.numeroDoc, 700);
                    }
                    else if (pp4 >= 700 & pp3 >= 600 & pp2 >= 500 & pp1 >= 400)
                    {
                        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeo(item.numeroDoc, 600);
                    }
                    else if (pp4 >= 600 & pp3 >= 500 & pp2 >= 400 & pp1 >= 300)
                    {
                        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeo(item.numeroDoc, 500);
                    }
                    else if (pp4 >= 500 & pp3 >= 400 & pp2 >= 300 & pp1 >= 200)
                    {
                        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeo(item.numeroDoc, 400);
                    }
                    else if (pp4 >= 400 & pp3 >= 300 & pp2 >= 200 & pp1 >= 100)
                    {
                        acMerca = PuntosNegocios.getInstance().ActualizarComisionMercadeo(item.numeroDoc, 300);
                    }

                }

            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarBonoEscolaridad(string prueba)
        {

            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteConCompras();
            bool resetear = PuntosNegocios.getInstance().ResetearComEscolaridadAutomatico();

            foreach (var item in ClienteConCompra)
            {
                int evaluacion = PuntosNegocios.getInstance().ObtenerEvaluacionEscolaridadAutomatico(item.numeroDoc);
                int requerimiento = PuntosNegocios.getInstance().ObtenerRequerimientoEscolaridadAutomatico(item.numeroDoc);

                if (evaluacion < 1)
                {
                    if (requerimiento >= 2)
                    {
                        bool update = PuntosNegocios.getInstance().ActualizarEscolaridadAutomatico(item.numeroDoc, 800);
                    }
                }
                else if (evaluacion >= 1)
                {
                    if (requerimiento >= 2)
                    {
                        bool update = PuntosNegocios.getInstance().ActualizarEscolaridadAutomatico(item.numeroDoc, 500);
                    }
                }
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarBonoBronce(string prueba)
        {

            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteConCompras();

            foreach (var item in ClienteConCompra)
            {

                int evaluacion = PuntosNegocios.getInstance().ObtenerEvaluacionBronce(item.numeroDoc);
                if (evaluacion < 1)
                {
                    int requerimiento = PuntosNegocios.getInstance().ObtenerRequerimientoBronce(item.numeroDoc);
                    if (requerimiento > 1)
                    {
                        bool update = PuntosNegocios.getInstance().ActualizarComisionBronce(item.numeroDoc, 150);
                    }
                }

            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarBonoAfiliacion(string prueba)
        {

            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodosCalculoComision();
            bool resetar = PuntosNegocios.getInstance().ResetearComAfiAutomatico();
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            string RangoInicio = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaInicio.Trim()).FirstOrDefault();
            string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaFin.Trim()).FirstOrDefault();
            List<Compra> ListaCompras = CompraNegocios.getInstance().ListaCompraAfiliacion(RangoInicio, RangoFin);

            foreach (var item in ListaCompras)
            {
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = item.DNICliente;
                List<ComAfiliacion> ListaPatrocinadores = new List<ComAfiliacion>();
                while (cc < 5 && !terminado)
                {
                    cc++;
                    ComAfiliacion agregar = ClienteLN.getInstance().ListaDatosPatrocinioAfiliaciones(IDP, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaPatrocinadores.Add(agregar); doc = agregar.documento.Trim(); }
                }

                foreach (var item2 in ListaPatrocinadores)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDP);
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "01" | ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 0)
                    {
                        double COMAFI = item.montoComision * 0.30;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, COMAFI, 0, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "02" | ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 1)
                    {
                        double COMAFI = item.montoComision * 0.05;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, COMAFI, 0, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "03" | ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 2)
                    {
                        double COMAFI = item.montoComision * 0.03;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, 0, COMAFI, 0, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "04" | ListaPatrocinadores[ccc].paquete == "23") & ccc == 3)
                    {
                        double COMAFI = item.montoComision * 0.02;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, 0, 0, COMAFI, 0);
                    }
                    if (PP >= 20 & (ListaPatrocinadores[ccc].paquete == "23") & ccc == 4)
                    {
                        double COMAFI = item.montoComision * 0.01;
                        bool ok1 = CompraNegocios.getInstance().ActualizarComisionAfiliacion(ListaPatrocinadores[ccc].documento, COMAFI, IDP, 0, 0, 0, 0, COMAFI);
                    }
                    ccc++;
                }
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarBonoUnilevel(string prueba)
        {

            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarPeriodosCalculoComision();
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            bool resetar = PuntosNegocios.getInstance().ResetearComUnilevel(IDP);
            string RangoInicio = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaInicio.Trim()).FirstOrDefault();
            string RangoFin = (from c in ListaPeriodos where c.idPeriodoComision == IDP select c.fechaFin.Trim()).FirstOrDefault();
            List<Compra> ListaCompras = CompraNegocios.getInstance().ListaComprUnilevel(RangoInicio, RangoFin);

            foreach (var item in ListaCompras)
            {
                double MCCU = item.montoComision;
                double MCCON = item.montoComision * 0.05;

                if (item.tipoCliente == "03") { MCCU = item.montoComision - item.montoComisionCI; }
                if (item.tipoCliente == "05") { MCCU = item.montoComision - MCCON; }
                int cc = 0;
                int ccc = 0;
                bool terminado = false;
                string doc = item.DNICliente; if (item.tipoCliente == "03" | item.tipoCliente == "05") { doc = item.DNIPatrocinador; ccc = 1; }
                double COMUNI = 0;
                List<PatrocinadorUnilivel> ListaUpline = new List<PatrocinadorUnilivel>();

                while (cc < 15 && !terminado)
                {
                    cc++;
                    PatrocinadorUnilivel agregar = ClienteLN.getInstance().ListaDatosUplineUnilevel(IDP, doc);
                    if (agregar == null) { terminado = true; }
                    else { ListaUpline.Add(agregar); doc = agregar.documento.Trim(); }
                }

                foreach (var item2 in ListaUpline)
                {
                    double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculo(item2.documento, IDP);
                    if (PP >= 20 & ccc == 0 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.05;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 1 & (item2.rango == "EMPRENDEDOR" | item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" |
                                               item2.rango == "RUBI" | item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 2 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 3 & (item2.rango == "BRONCE" | item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 4 & (item2.rango == "PLATA" | item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.10;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 5 & (item2.rango == "ORO" | item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.04;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 6 & (item2.rango == "ZAFIRO" | item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.03;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 7 & (item2.rango == "RUBI" |
                                               item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.02;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 8 & (item2.rango == "DIAMANTE" | item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 9 & (item2.rango == "D. DIAMANTE" | item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                               item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 10 & (item2.rango == "T. DIAMANTE" | item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                               item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 11 & (item2.rango == "D. MILLONARIO" |
                                                item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 12 & (item2.rango == "D.D. MILLONARIO" | item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 13 & (item2.rango == "T.D. MILLONARIO" | item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI, 0);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 14 & (item2.rango == "D. IMPERIAL" |
                                                item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 15 & (item2.rango == "IMPERIAL ELITE" | item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 16 & (item2.rango == "IMPERIAL FUNDADOR" | item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 17 & (item2.rango == "IMPERIAL CORONA" | item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    if (PP >= 20 & ccc == 18 & (item2.rango == "ROYAL"))
                    {
                        COMUNI = MCCU * 0.01;
                        bool update = PuntosNegocios.getInstance().ActualizarComisionUnilevel(item2.documento, COMUNI, IDP, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, COMUNI);
                        MCCU = MCCU - COMUNI;
                    }
                    ccc++;
                }
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarBonoMatricial(string prueba)
        {

            List<Cliente> ClienteConCompra = ClienteLN.getInstance().ListarClienteConCompras();
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            bool resetear = PuntosNegocios.getInstance().ResetearComMatricial(IDP);

            foreach (var item in ClienteConCompra)
            {

                double PP = PuntosNegocios.getInstance().ObtenerSumaPPRecalculoMatricial(item.numeroDoc, IDP);
                List<Periodo> ListaEvaluacion = PuntosNegocios.getInstance().ListaEvaluacionMatricial(item.numeroDoc, IDP);
                List<Cliente> ListaDirectosEvaluacion = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item.numeroDoc, IDP);

                int Directos60PP = (from x in ListaDirectosEvaluacion where x.PP >= 60 select x.PP).Count();
                int contador = 0;
                bool aprobado = false;

                if (ListaEvaluacion[0].evaluacionMatrocial4x4 < 1 | ListaEvaluacion[0].evaluacionMatrocial5x5 < 1 | ListaEvaluacion[0].evaluacionMatrocial6x6 < 1)
                {
                    if (ListaEvaluacion[0].evaluacionMatrocial6x6 < 1 && aprobado == false)
                    {
                        if (PP >= 60 && Directos60PP >= 6)
                        {
                            foreach (var item2 in ListaDirectosEvaluacion)
                            {
                                List<Cliente> ListaDirectosNivel2 = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item2.numeroDoc, IDP);
                                int Directos60PPNivel2 = (from x in ListaDirectosNivel2 where x.PP >= 60 select x.PP).Count();
                                if (Directos60PPNivel2 >= 6) { contador++; }
                            }
                            if (contador >= 6)
                            {
                                bool actualizar = PuntosNegocios.getInstance().ActualizarMatricialRecalculo(item.numeroDoc, 420, IDP, 0, 0, 1);
                                aprobado = true;
                            }
                        }
                    }
                    if (ListaEvaluacion[0].evaluacionMatrocial5x5 < 1 && aprobado == false)
                    {
                        if (PP >= 60 && Directos60PP >= 5)
                        {
                            contador = 0;
                            foreach (var item2 in ListaDirectosEvaluacion)
                            {
                                List<Cliente> ListaDirectosNivel2 = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item2.numeroDoc, IDP);
                                int Directos60PPNivel2 = (from x in ListaDirectosNivel2 where x.PP >= 60 select x.PP).Count();
                                if (Directos60PPNivel2 >= 5) { contador++; }
                            }
                            if (contador >= 5)
                            {
                                bool actualizar = PuntosNegocios.getInstance().ActualizarMatricialRecalculo(item.numeroDoc, 320, IDP, 0, 1, 0);
                                aprobado = true;
                            }
                        }
                    }
                    if (ListaEvaluacion[0].evaluacionMatrocial4x4 < 1 && aprobado == false)
                    {
                        if (PP >= 60 && Directos60PP >= 4)
                        {
                            contador = 0;
                            foreach (var item2 in ListaDirectosEvaluacion)
                            {
                                List<Cliente> ListaDirectosNivel2 = PuntosNegocios.getInstance().ListarEvaluacionDirectosMatricial(item2.numeroDoc, IDP);
                                int Directos60PPNivel2 = (from x in ListaDirectosNivel2 where x.PP >= 60 select x.PP).Count();
                                if (Directos60PPNivel2 >= 4) { contador++; }
                            }
                            if (contador >= 4)
                            {
                                bool actualizar = PuntosNegocios.getInstance().ActualizarMatricialRecalculo(item.numeroDoc, 220, IDP, 1, 0, 0);
                                aprobado = true;
                            }
                        }
                    }
                }
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string EliminarComprasAntiguas(string prueba)
        {

            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(1);
            List<LimiteDias> listaFechaPE = FechasLimiteLN.getInstance().ListaLimite(2);
            int horas = listaFecha[0].numeroDias, horasPE = listaFechaPE[0].numeroDias;
            List<Compra> listaCompraAntigua = CompraNegocios.getInstance().ListarComprasAntiguasGeneral();
            DateTime fechaActual = DateTime.Now.AddHours(-2);
            DateTime FechaCompra;

            foreach (var item in listaCompraAntigua)
            {
                int horasLimite = (item.TipoPago == "04") ? horasPE : horas;
                FechaCompra = item.FechaPago.AddHours(horasLimite);
                if (FechaCompra < fechaActual)
                {
                    RecuperarStock(item.Ticket);
                    bool ok = CompraNegocios.getInstance().EliminarCompraCliente(item.Ticket);
                }
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        private void RecuperarStock(string ticket)
        {
            List<Compra> ListaP = CompraNegocios.getInstance().ListarProductosxTicket(ticket);
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();

            foreach (var item in ListaP)
            {
                string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == item.Despacho.Trim() select c.DNICDR.Trim()).FirstOrDefault();
                bool recuperar = StockLN.getInstance().IncrementarStock(dniCDR, item.IdProductoPais, item.Cantidad);
            }
        }

        [WebMethod]
        public string ActualizarCantidadRetencion(string prueba)
        {
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            List<Cliente> ListaCliente = ClienteLN.getInstance().ListarClienteRetencion(IDP);

            foreach (var item in ListaCliente)
            {
                List<DatosSocios> Lista = ClienteLN.getInstance().ListarCantidadDirectos(item.numeroDoc, IDP);
                bool updateRetencion = ClienteLN.getInstance().ActualizarRetencionSocio(Lista[0].CantSocios, Lista[0].CantSociosDirectos, Lista[0].NuevoSocios,
                                                                                        Lista[0].CantCON, Lista[0].CantCONDirectos, Lista[0].NuevoCON,
                                                                                        Lista[0].CantCI, Lista[0].CantCIDirectos, Lista[0].NuevoCI,
                                                                                        Lista[0].CantSociosRed, Lista[0].CantSociosRedActivos, Lista[0].CantNuevosSociosRed,
                                                                                        Lista[0].CantCONRed, Lista[0].CantCONRedActivos, Lista[0].CantNuevosCONRed,
                                                                                        Lista[0].CantCIRed, Lista[0].CantCIRedActivos, Lista[0].CantNuevosCIRed, item.numeroDoc, IDP);
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarRangoComision(string prueba)
        {
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoPuntosActivo();
            List<Cliente> ListaSocios = PuntosNegocios.getInstance().ListarDniXRangoComisiones(IDP);
            int IDPCom = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();

            foreach (var item in ListaSocios)
            {
                bool update = PuntosNegocios.getInstance().ActualizarRangoComision(item.numeroDoc, item.rango, IDPCom);
            }

            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public List<Cliente> ListarCliente100()
        {
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarCliente100();
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public string CompresionSocios(string prueba)
        {
            List<Cliente.Compresion> Lista = ClienteLN.getInstance().ListarSociosComprimir();
            foreach (var item in Lista)
            {
                List<Cliente.Compresion.Directos> Directos = ClienteLN.getInstance().ListarDirectoSocios(item.Documento);
                if (Directos.Count > 0)
                {
                    List<Cliente.Compresion.DocumentoUP> DatosUP = ClienteLN.getInstance().ListarDocumentoUP(item.Documento);
                    foreach (var socios in Directos)
                    {
                        if (socios.TipoCliente == "01")
                        {
                            ClienteLN.getInstance().ActualizarMapaRedComisionCompresion(DatosUP[0].Upline, DatosUP[0].IdUpline, socios.Documento);
                            ClienteLN.getInstance().ActualizarDatos_Directos_Compresion(DatosUP[0].Upline, DatosUP[0].Patrocinador, DatosUP[0].FactorComision, socios.Documento);
                        }
                        else
                        {
                            ClienteLN.getInstance().ActualizarMapaRedComisionCompresion(DatosUP[0].Upline, DatosUP[0].IdUpline, socios.Documento);
                            ClienteLN.getInstance().ActualizarDatos_Directos_Compresion("", DatosUP[0].Patrocinador, DatosUP[0].FactorComision, socios.Documento);
                        }
                    }
                    if (item.TipoCliente == "01")
                    {
                        ClienteLN.getInstance().ActualizarDocumentoSocioComprimido(item.IdCliente);
                    }
                    else
                    {
                        ClienteLN.getInstance().ActualizarDocumento_Consultor_CI_Comprimido(item.IdCliente);
                    }
                }
                else
                {
                    if (item.TipoCliente == "01")
                    {
                        ClienteLN.getInstance().ActualizarDocumentoSocioComprimido(item.IdCliente);
                    }
                    else
                    {
                        ClienteLN.getInstance().ActualizarDocumento_Consultor_CI_Comprimido(item.IdCliente);
                    }
                }
            }
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

        [WebMethod]
        public string ActualizarIdPatrocinio(string prueba)
        {
            List<Cliente> listaCliente = ClienteLN.getInstance().ListarClientePeriodo();
            foreach (var item in listaCliente)
            {
                string idSocioPatrocinador = PuntosNegocios.getInstance().ObtenerIdSocioByDocumento(item.patrocinador);
                PuntosNegocios.getInstance().ActualizarIdSocioUplinePuntos(item.idCliente, idSocioPatrocinador);
            }
            respuestaPrueba = "2xx";
            return respuestaPrueba;
        }

    }
}
