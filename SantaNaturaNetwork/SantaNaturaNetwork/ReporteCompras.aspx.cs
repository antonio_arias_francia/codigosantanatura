﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using Modelos;
using Negocios;
using System.Text;
using System.IO;
using Newtonsoft.Json;

namespace SantaNaturaNetwork
{
    public partial class ReporteCompras : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        protected void btnValidarEstado_Click(object sender, EventArgs e)
        {
            //bool TB = true;
            //List<Compra> compra = new List<Compra>();
            //compra = CompraNegocios.getInstance().ListaComprasPendientesGeneral();
            //foreach (var item in compra)
            //{
            //    string estadoUpdate = ComprobarEstado(item.IdMove);
            //    var jsonObje = JObject.Parse(estadoUpdate);
            //    string estadoGo = (string)jsonObje["estado"];
            //    string liquiGo = (string)jsonObje["horavalida"];
            //    if (liquiGo != null)
            //    {
            //        bool ok = CompraNegocios.getInstance().ActualizarDepositoValidado(item.Ticket, 0);
            //    }
            //    if (estadoGo == "2")
            //    {
            //        bool ok = CompraNegocios.getInstance().ActualizarDepositoValidado(item.Ticket, 3);
            //    }
            //    else
            //    {
            //        TB = true;
            //    }
            //}
        }

        private void WriteContent(HttpWebRequest request, string body)
        {
            var encoding = Encoding.UTF8;

            request.ContentLength = encoding.GetByteCount(body);

            using (var writeStream = request.GetRequestStream())
            {
                var bytes = encoding.GetBytes(body);

                writeStream.Write(bytes, 0, bytes.Length);

                writeStream.Flush();
            }
        }

        private static string ReadContent(HttpWebResponse webResponse)
        {
            using (var stream = webResponse.GetResponseStream())
            {
                if (stream == null) return string.Empty;

                var streamReader = new StreamReader(stream);

                return streamReader.ReadToEnd();
            }
        }

        public class WebResponse
        {
            public HttpStatusCode StatusCode { get; set; }

            public string Content { get; set; }

            public string StatusDescription { get; set; }
        }

        [WebMethod]
        public static List<Compra> ListarCompraGeneral()
        {
            List<Compra> Lista = null;
            try
            {
                Lista = CompraNegocios.getInstance().ListaComprasGeneral();

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<Compra> ListarCompraGeneralFiltrado(string fecha1, string fecha2)
        {
            List<Compra> Lista = null;
            try
            {
                Lista = CompraNegocios.getInstance().ListaComprasGeneralFiltrado(fecha1, fecha2);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<DetalleCompra> ListaDetalleComprasGeneral(string id)
        {
            List<DetalleCompra> listaDetalleCompra = null;
            try
            {
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraGeneral(id);

            }
            catch (Exception ex)
            {
                listaDetalleCompra = null;
            }
            return listaDetalleCompra;
        }

        [WebMethod]
        public static List<DetalleCompra> ListaTipoCompraDetalle()
        {
            List<DetalleCompra> listaDetalleCompra = null;
            try
            {
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaTipoCompraDetalle();

            }
            catch (Exception ex)
            {
                listaDetalleCompra = null;
            }
            return listaDetalleCompra;
        }

        [WebMethod]
        public static List<DetalleCompra> ListaTipoPagoDetalle()
        {
            List<DetalleCompra> listaDetalleCompra = null;
            try
            {
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaTipoPagoDetalle();

            }
            catch (Exception ex)
            {
                listaDetalleCompra = null;
            }
            return listaDetalleCompra;
        }

        [WebMethod]
        public static List<DetalleCompra> ListaDespachoDetalle()
        {
            List<DetalleCompra> listaDetalleCompra = null;
            try
            {
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDespachoDetalle();

            }
            catch (Exception ex)
            {
                listaDetalleCompra = null;
            }
            return listaDetalleCompra;
        }

        [WebMethod]
        public static List<DetalleCompra> ListaEstadoDetalle()
        {
            List<DetalleCompra> listaDetalleCompra = null;
            try
            {
                listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaEstadoDetalle();

            }
            catch (Exception ex)
            {
                listaDetalleCompra = null;
            }
            return listaDetalleCompra;
        }

        [WebMethod]
        public static bool ActualizarCompraDetalle(string ticketCom1, string montoPagarCom1, string montoComisionCom1, string puntosTotalCom1,
                                                    string montoTotalCom1, string fechaCom1, string cantidadProdCom1, string idopCom1,
                                                    string idtipoCom1, string tipoCom1, string tipoPagoCom1, string estadoCom1, string despachoCom1)
        {
            DateTime pFecha = Convert.ToDateTime(fechaCom1);
            string fecha1S = pFecha.ToString("yyyy/MM/dd");
            montoPagarCom1 = montoPagarCom1.Replace(".",",");
            montoComisionCom1 = montoComisionCom1.Replace(".",",");
            puntosTotalCom1 = puntosTotalCom1.Replace(".",",");
            montoTotalCom1 = montoTotalCom1.Replace(".",",");
            DetalleCompra objCompra = new DetalleCompra()
            {
                TicketCOM = ticketCom1,
                MontoPagarCOM = Convert.ToDouble(montoPagarCom1),
                MontoComisionCOM = Convert.ToDouble(montoComisionCom1),
                PuntosTotalCOM = Convert.ToDouble(puntosTotalCom1),
                MontoTotalCOM = Convert.ToDouble(montoTotalCom1),
                FechaCOM = fecha1S,
                CantidadCOM = Convert.ToInt32(cantidadProdCom1),
                IdopCOM = idopCom1,
                idTipoCompra = idtipoCom1,
                nombreTipoCom = tipoCom1,
                idTipoPago = tipoPagoCom1,
                idEstado = Convert.ToInt32(estadoCom1),
                idDespacho = despachoCom1
            };
            bool ok = DetalleCompraNegocios.getInstance().ActualizarCompraDetalle(objCompra);
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Actualizar Compra", "IDOP: " + idopCom1 + " Monto: " + montoPagarCom1.Trim() + 
                                                                           " TipoPagoS: " + tipoPagoCom1.Trim());
            return true;
        }

        [WebMethod]
        public static bool RetornarCompra(string ticketS)
        {
            try
            {
                bool ok = DetalleCompraNegocios.getInstance().ActualizarCompraDetalle(ticketS);
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                               "Retornar Compra", "Ticket: " + ticketS);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static string ReenviarCompra(string ticketS)
        {
            string reenviar = "";
            try
            {
                List<Compra> Lista = CompraNegocios.getInstance().ListaCompraDatosxTicket(ticketS);
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                               "Reenviar Compra", "Ticket: " + ticketS);
                if (Lista.Count > 0)
                {
                    List<DetalleCompra> listaDetalleCompra = DetalleCompraNegocios.getInstance().ListaDetalleCompraDelCliente(ticketS);
                    string idopPShop = ObtenerIdopCompra(Lista[0].DNICliente, Lista[0].Despacho.Trim(), Lista[0].NombreCliente, 
                                                         Lista[0].DireccionCliente, Lista[0].NotaPS, "", "", "", listaDetalleCompra,
                                                         Lista[0].TipoPago, Lista[0].BRAND);
                    bool okVisa = CompraNegocios.getInstance().ActualizarIdopYEstadoCompra(ticketS, idopPShop);
                    reenviar = idopPShop;
                }
                
            }
            catch (Exception ex)
            {
                reenviar = "";
                throw ex;
            }

            return reenviar;

        }

        [WebMethod]
        public static string NuevoCIP(string ticketS)
        {
            string CIP = "";
            try
            {
                List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(3);
                List<Compra.NuevoCIP> listaDatos = CompraNegocios.getInstance().ListaDatosNuevoCIP(ticketS);
                int diasLimite = listaFecha[0].numeroDias;
                DateTime DayPE = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("SA Pacific Standard Time"));
                DateTime DayPEExpi = DayPE.AddHours(diasLimite);
                string accessKey = "N2JmODAyZTlhYWFjZDQx";
                string idService = "8434";
                string secretKey = "iXK8YpOVlz8EIGo/pUHWJwtw9baNZ5jbxG3hOw+E";
                string date = Convert.ToString(DayPE.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
                string dateExpi = Convert.ToString(DayPEExpi.ToString("yyyy-MM-ddTHH:mm:sszzz")).Replace("-03:00", "-05:00");
                decimal monto = Math.Round(listaDatos[0].MontoAPagar, 2);

                var token = CompraNegocios.getInstance().getTokenPE(accessKey, idService, secretKey, date);
                Authenticate.Data objectsSend = new Authenticate.Data()
                {
                    currency = "PEN",
                    amount = monto,
                    transactionCode = ticketS,
                    adminEmail = "pagoefectivosnn@gmail.com",
                    dateExpiry = dateExpi,
                    paymentConcept = "Nuevo CIP",
                    additionalData = "datos adicionales",
                    userEmail = listaDatos[0].Correo,
                    userId = listaDatos[0].IdCliente,
                    userName = listaDatos[0].Nombres,
                    userLastName = listaDatos[0].Apellidos,
                    userUbigeo = "",
                    userCountry = "PERU",
                    userDocumentType = "DNI",
                    userDocumentNumber = listaDatos[0].Documento,
                    userCodeCountry = "+51",
                    serviceID = 8434
                };
                TokenPE.GetObjects objectsCIP = CompraNegocios.getInstance().GetCIP(objectsSend, Convert.ToString(token));
                DatosPagoEfectivo objDatos = new DatosPagoEfectivo()
                {
                    Ticket = ticketS,
                    FechaCreacion = DayPE,
                    FechaExpiracion = DayPEExpi,
                    CIP = Convert.ToString(objectsCIP.cip),
                    Monto = Convert.ToDouble(monto),
                    CodeStatus = objectsCIP.code
                };
                CompraNegocios.getInstance().ActualizarDatosPagoEfectivo(objDatos);
                CIP = objectsCIP.cip.ToString();
            }
            catch (Exception ex)
            {
                CIP = "";
                throw ex;
            }

            return CIP;

        }

        [WebMethod]
        public static bool EliminarCompra(string ticketS, string idopS, string nombreS, string montoS, string tipoPagoS)
        {
            try
            {
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                bool recuperar = RecuperarStock(ticketS);
                bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32), 
                                                                               "Eliminar Compra", "IDOP: "+idopS.Trim()+" Nombre: "+nombreS.Trim() + 
                                                                               " Monto: "+montoS.Trim() + " TipoPagoS: "+tipoPagoS.Trim());
                bool ok = CompraNegocios.getInstance().EliminarCompraCliente(ticketS);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private static bool RecuperarStock(string ticket)
        {
            List<Compra> ListaP = CompraNegocios.getInstance().ListarProductosxTicket(ticket);
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();

            foreach (var item in ListaP)
            {
                string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == item.Despacho.Trim() select c.DNICDR.Trim()).FirstOrDefault();
                bool recuperar = StockLN.getInstance().IncrementarStock(dniCDR, item.IdProductoPais, item.Cantidad);
            }
            return true;
        }

        private static string ObtenerIdopCompra(string dniComprador, string local, string nombreEnvia, string direccionComprador, string not, string dir, string ubi, string refe, 
                                         List<DetalleCompra> listaDetalleCompra, string FormaPago, string brand)
        {
            List<List<string>> listProductos = new List<List<string>>();
            List<ProductoV2> ListaProductoG = ProductoLN.getInstance().ListaIdopXNombre();
            string idop = "";
            string fpago = "";
            string pagado = "0";
            int numPromo = 100;

            fpago = (FormaPago == "01") ? "DEPOSITO" : 
                    (FormaPago == "02" | FormaPago == "05") ? "EFECTIVO" : 
                    (FormaPago == "03" && brand == "visa") ? "POSVISA" : 
                    (FormaPago == "03" && brand == "mastercard") ? "POSMC" : "ECOMERCIO";
            pagado = (FormaPago == "03" | FormaPago == "04") ? "1" : "0";

            try
            {
                foreach (var item in listaDetalleCompra)//22 y 56
                {
                    int cantC = 0;
                    string regalo = "0";
                    double sendMoney = 0.0000;
                    double precioSend = 0.0000;

                    if (item.IdProductoPeruShop == "110038")
                    {

                        precioSend = 6.00;
                        List<string> productos = new List<string>();
                        productos.Add("2069");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("1");
                        productos.Add("0");
                        productos.Add("2069");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        cantC = item.CantiPS * 2;
                        double precioSend2 = 7.00;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108683");
                        productos2.Add(Convert.ToString(cantC));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("2");
                        productos2.Add("0");
                        productos2.Add("108683");
                        productos.Add(regalo);
                        listProductos.Add(productos2);

                    }
                    else if (item.IdProductoPeruShop == "0001")
                    {
                        cantC = item.CantiPS * 12;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110107");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("3");
                        productos.Add("0");
                        productos.Add("110107");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0002")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109949");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("4");
                        productos.Add("0");
                        productos.Add("109949");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0003")
                    {
                        cantC = item.CantiPS * 50;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109948");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("5");
                        productos.Add("0");
                        productos.Add("109948");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0004")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("6");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0005")
                    {
                        cantC = 3;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("7");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0006")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("94");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("8");
                        productos.Add("0");
                        productos.Add("94");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0007")
                    {
                        cantC = 5;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("9");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0008")
                    {
                        cantC = 10;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("8606");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("10");
                        productos.Add("0");
                        productos.Add("8606");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0009")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("11");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0010")
                    {
                        cantC = item.CantiPS * 10;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("12");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0011")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("13");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0012")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.05) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110208");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("14");
                        productos.Add("0");
                        productos.Add("110208");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0013")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("15");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0014")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110249");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("16");
                        productos.Add("0");
                        productos.Add("110249");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0015")
                    {
                        cantC = 1;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("17");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0016")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("109986");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("18");
                        productos.Add("0");
                        productos.Add("109986");
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0017")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(1));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("19");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(2));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("20");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0018")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(2));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("21");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(4));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("22");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0019")
                    {
                        precioSend = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(4));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add("23");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);

                        double precioSend2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("327");
                        productos2.Add(Convert.ToString(8));
                        productos2.Add(precioSend2.ToString("N2").Replace(",", "."));
                        productos2.Add("24");
                        productos2.Add("0");
                        productos2.Add("327");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0020")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("47");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("25");
                        productos.Add("0");
                        productos.Add("47");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0021")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("255");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("26");
                        productos.Add("0");
                        productos.Add("255");
                        productos.Add(regalo);
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        double precioSend2 = item.PrecioPS * item.CantiPS;
                        double sendMoney2 = precioSend2 / cantC2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("256");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("27");
                        productos2.Add("0");
                        productos2.Add("256");
                        productos2.Add("0");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0022")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("28");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0023")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110242");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("29");
                        productos.Add("0");
                        productos.Add("110242");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("6063");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("30");
                        productos2.Add("0");
                        productos2.Add("6063");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0024")
                    {
                        cantC = item.CantiPS * 6;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("6295");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("31");
                        productos.Add("0");
                        productos.Add("6295");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0025")
                    {
                        cantC = 2;
                        sendMoney = item.PrecioPS / cantC;
                        if (sendMoney <= 0.06) { regalo = "1"; }
                        List<string> productos = new List<string>();
                        productos.Add("110324");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("32");
                        productos.Add("0");
                        productos.Add("110324");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0026")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("33");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110342");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("34");
                        productos2.Add("0");
                        productos2.Add("110342");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0027")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / (item.CantiPS * 19);
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("35");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("328");
                        productos2.Add(Convert.ToString(cantC2));
                        productos2.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos2.Add("36");
                        productos2.Add("0");
                        productos2.Add("328");
                        productos2.Add("0");
                        listProductos.Add(productos2);

                        int cantC3 = item.CantiPS * 2;
                        List<string> productos3 = new List<string>();
                        productos3.Add("1750");
                        productos3.Add(Convert.ToString(cantC3));
                        productos3.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos3.Add("37");
                        productos3.Add("0");
                        productos3.Add("1750");
                        productos3.Add("0");
                        listProductos.Add(productos3);

                        int cantC4 = item.CantiPS * 3;
                        List<string> productos4 = new List<string>();
                        productos4.Add("47");
                        productos4.Add(Convert.ToString(cantC4));
                        productos4.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos4.Add("38");
                        productos4.Add("0");
                        productos4.Add("47");
                        productos4.Add("0");
                        listProductos.Add(productos4);

                        int cantC5 = item.CantiPS * 1;
                        List<string> productos5 = new List<string>();
                        productos5.Add("35");
                        productos5.Add(Convert.ToString(cantC5));
                        productos5.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos5.Add("39");
                        productos5.Add("0");
                        productos5.Add("35");
                        productos5.Add("0");
                        listProductos.Add(productos5);

                        int cantC6 = item.CantiPS * 2;
                        List<string> productos6 = new List<string>();
                        productos6.Add("107");
                        productos6.Add(Convert.ToString(cantC6));
                        productos6.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos6.Add("40");
                        productos6.Add("0");
                        productos6.Add("107");
                        productos6.Add("0");
                        listProductos.Add(productos6);

                        int cantC7 = item.CantiPS * 1;
                        List<string> productos7 = new List<string>();
                        productos7.Add("69");
                        productos7.Add(Convert.ToString(cantC7));
                        productos7.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos7.Add("41");
                        productos7.Add("0");
                        productos7.Add("69");
                        productos7.Add("0");
                        listProductos.Add(productos7);

                        int cantC8 = item.CantiPS * 3;
                        List<string> productos8 = new List<string>();
                        productos8.Add("269");
                        productos8.Add(Convert.ToString(cantC8));
                        productos8.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos8.Add("42");
                        productos8.Add("0");
                        productos8.Add("269");
                        productos8.Add("0");
                        listProductos.Add(productos8);

                        int cantC9 = item.CantiPS * 2;
                        List<string> productos9 = new List<string>();
                        productos9.Add("6");
                        productos9.Add(Convert.ToString(cantC9));
                        productos9.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos9.Add("43");
                        productos9.Add("0");
                        productos9.Add("6");
                        productos9.Add("0");
                        listProductos.Add(productos9);

                        int cantC10 = item.CantiPS * 1;
                        List<string> productos10 = new List<string>();
                        productos10.Add("81");
                        productos10.Add(Convert.ToString(cantC10));
                        productos10.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos10.Add("44");
                        productos10.Add("0");
                        productos10.Add("81");
                        productos10.Add("0");
                        listProductos.Add(productos10);
                    }
                    else if (item.IdProductoPeruShop == "0028")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / (item.CantiPS * 15);
                        List<string> productos = new List<string>();
                        productos.Add("110245");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("45");
                        productos.Add("0");
                        productos.Add("110245");
                        productos.Add("0");
                        listProductos.Add(productos);

                        int cantC2 = item.CantiPS * 2;
                        List<string> productos2 = new List<string>();
                        productos2.Add("108954");
                        productos2.Add(Convert.ToString(cantC2));
                        productos2.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos2.Add("46");
                        productos2.Add("0");
                        productos2.Add("108954");
                        productos2.Add("0");
                        listProductos.Add(productos2);

                        int cantC3 = item.CantiPS * 2;
                        List<string> productos3 = new List<string>();
                        productos3.Add("110242");
                        productos3.Add(Convert.ToString(cantC3));
                        productos3.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos3.Add("47");
                        productos3.Add("0");
                        productos3.Add("110242");
                        productos3.Add("0");
                        listProductos.Add(productos3);

                        int cantC4 = item.CantiPS * 2;
                        List<string> productos4 = new List<string>();
                        productos4.Add("47");
                        productos4.Add(Convert.ToString(cantC4));
                        productos4.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos4.Add("48");
                        productos4.Add("0");
                        productos4.Add("47");
                        productos4.Add("0");
                        listProductos.Add(productos4);

                        int cantC5 = item.CantiPS * 2;
                        List<string> productos5 = new List<string>();
                        productos5.Add("311");
                        productos5.Add(Convert.ToString(cantC5));
                        productos5.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos5.Add("49");
                        productos5.Add("0");
                        productos5.Add("311");
                        productos5.Add("0");
                        listProductos.Add(productos5);

                        int cantC6 = item.CantiPS * 3;
                        List<string> productos6 = new List<string>();
                        productos6.Add("21");
                        productos6.Add(Convert.ToString(cantC6));
                        productos6.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos6.Add("50");
                        productos6.Add("0");
                        productos6.Add("21");
                        productos6.Add("0");
                        listProductos.Add(productos6);

                        int cantC7 = item.CantiPS * 2;
                        List<string> productos7 = new List<string>();
                        productos7.Add("35");
                        productos7.Add(Convert.ToString(cantC7));
                        productos7.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos7.Add("51");
                        productos7.Add("0");
                        productos7.Add("35");
                        productos7.Add("0");
                        listProductos.Add(productos7);
                    }
                    else if (item.IdProductoPeruShop == "0029")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("108954");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("52");
                        productos.Add("0");
                        productos.Add("108954");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("311");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("53");
                        productos2.Add("0");
                        productos2.Add("311");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0030")
                    {
                        cantC = 3;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("53");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0031")
                    {
                        cantC = 5;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("87");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("54");
                        productos.Add("0");
                        productos.Add("87");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0032")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110239");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("55");
                        productos.Add("0");
                        productos.Add("110239");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110395");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("56");
                        productos2.Add("0");
                        productos2.Add("110395");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0033")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("57");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0034")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("58");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0035")
                    {
                        cantC = 6;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110393");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("59");
                        productos.Add("0");
                        productos.Add("110393");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0036")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110243");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("60");
                        productos.Add("0");
                        productos.Add("110243");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0037")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110100");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("61");
                        productos.Add("0");
                        productos.Add("110100");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0038")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("24");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("62");
                        productos.Add("0");
                        productos.Add("24");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0039")
                    {
                        List<string> productos = new List<string>();
                        productos.Add("110241");
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(item.PrecioPS.ToString("N2").Replace(",", "."));
                        productos.Add("63");
                        productos.Add("0");
                        productos.Add("110241");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110415");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("64");
                        productos2.Add("0");
                        productos2.Add("110415");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0040")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("65");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0041")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110397");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("66");
                        productos.Add("0");
                        productos.Add("110397");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0042")
                    {
                        cantC = item.CantiPS * 3;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("269");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("67");
                        productos.Add("0");
                        productos.Add("269");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0043")
                    {
                        cantC = item.CantiPS * 2;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("110240");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("68");
                        productos.Add("0");
                        productos.Add("110240");
                        productos.Add("0");
                        listProductos.Add(productos);

                        double sendMoney2 = 0.05;
                        List<string> productos2 = new List<string>();
                        productos2.Add("110191");
                        productos2.Add(Convert.ToString(item.CantiPS));
                        productos2.Add(sendMoney2.ToString("N2").Replace(",", "."));
                        productos2.Add("69");
                        productos2.Add("0");
                        productos2.Add("110191");
                        productos2.Add("1");
                        listProductos.Add(productos2);
                    }
                    else if (item.IdProductoPeruShop == "0044")
                    {
                        cantC = 1;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("70");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0045")
                    {
                        cantC = 2;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("71");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0046")
                    {
                        cantC = 4;
                        sendMoney = 0.05;
                        List<string> productos = new List<string>();
                        productos.Add("110301");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("72");
                        productos.Add("0");
                        productos.Add("110301");
                        productos.Add("1");
                        listProductos.Add(productos);
                    }
                    else if (item.IdProductoPeruShop == "0047")
                    {
                        cantC = item.CantiPS * 20;
                        precioSend = item.PrecioPS * item.CantiPS;
                        sendMoney = precioSend / cantC;
                        List<string> productos = new List<string>();
                        productos.Add("109645");
                        productos.Add(Convert.ToString(cantC));
                        productos.Add(sendMoney.ToString("N2").Replace(",", "."));
                        productos.Add("73");
                        productos.Add("0");
                        productos.Add("109645");
                        productos.Add("0");
                        listProductos.Add(productos);
                    }
                    else
                    {
                        string promo = "";
                        if (item.LineaSend == "05" | item.LineaSend == "02" | item.LineaSend == "01") { numPromo += 1; promo = Convert.ToString(numPromo); }
                        if (item.PrecioPS <= 0.06) { regalo = "1"; precioSend = 0.05; }
                        else { precioSend = item.PrecioPS; }
                        List<string> productos = new List<string>();
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(Convert.ToString(item.CantiPS));
                        productos.Add(precioSend.ToString("N2").Replace(",", "."));
                        productos.Add(promo);
                        productos.Add("0");
                        productos.Add(item.IdProductoPeruShop);
                        productos.Add(regalo);
                        listProductos.Add(productos);
                    }

                }

                string serializeProds = JsonConvert.SerializeObject(listProductos);

                string prod = serializeProds;

                string url = "https://santanatura.cti.lat/santa2/webservices/pedidos.php";
                WebClient wc = new WebClient();
                NameValueCollection nvc = new NameValueCollection();
                nvc.Add("a", "pCatalogo");
                nvc.Add("canal", "RED");
                nvc.Add("tipo", "2");
                nvc.Add("fpago", fpago);
                nvc.Add("pagado", pagado);
                nvc.Add("ruc", dniComprador.Trim());
                nvc.Add("local", local);
                nvc.Add("localorg", local);
                nvc.Add("razon", nombreEnvia);
                nvc.Add("dir_ruc", direccionComprador);
                nvc.Add("log", "Multinivel");
                nvc.Add("not", not);
                nvc.Add("delivery", "0");
                nvc.Add("dir", dir);
                nvc.Add("ubi", ubi);
                nvc.Add("ref", refe);
                nvc.Add("moneda", "PEN");
                nvc.Add("prod", prod);

                var data = wc.UploadValues(url, "POST", nvc);

                var responseString = UnicodeEncoding.UTF8.GetString(data);

                idop = responseString;
            }
            catch (WebException wex)
            {
                var webResponse = wex.Response as HttpWebResponse;

                if (wex.Status == WebExceptionStatus.ProtocolError)
                {
                    var content = ReadContent(webResponse);

                    var test = new WebResponse
                    {
                        StatusCode = webResponse.StatusCode,

                        StatusDescription = webResponse.StatusDescription,

                        Content = content
                    };
                }
            }
            return idop;
        }

    }
}