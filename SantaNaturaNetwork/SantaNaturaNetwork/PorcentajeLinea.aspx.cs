﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class PorcentajeLinea : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static bool InicioPorcentajeLineaCDR()
        {
            bool respuesta;
            try
            {
                List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDR();
                List<CombosProducto> ListaLinea = ProductoLN.getInstance().ListaComboLinea();
                ListaLinea.RemoveRange(3, 3);

                foreach (var cdr in ListaCDR)
                {
                    foreach (var linea in ListaLinea)
                    {
                        Cliente.CDR objCDR = new Cliente.CDR()
                        {
                            IdLinea = linea.idLinea,
                            CDRPS = cdr.DNICDR,
                            Porcentaje = 0
                        };
                        bool registro = CdrLN.getInstance().RegistroPorcentajeLineaCDR(objCDR);
                    }
                }
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }

        [WebMethod]
        public static List<Cliente.CDR> ListaCDRPorcentaje()
        {
            List<Cliente.CDR> ListaCDR = CdrLN.getInstance().ListaCDRPorcentaje();
            return ListaCDR;
        }

        [WebMethod]
        public static List<Cliente.CDR> ListaPorcentajeLineaXCDR(string DNICDR)
        {
            List<Cliente.CDR> ListaCDR = CdrLN.getInstance().ListaPorcentajeLineaXCDR(DNICDR);
            return ListaCDR;
        }

        [WebMethod]
        public static bool ActualizarPorcentajeLineaCDR(int idPorcentaje, double porcentaje)
        {
            bool respuesta;
            try
            {
                Cliente.CDR objCDR = new Cliente.CDR()
                {
                    IdLineaCDR = idPorcentaje,
                    Porcentaje = porcentaje
                };
                bool registro = CdrLN.getInstance().ActualizarPorcentajeLineaCDR(objCDR);
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }

        [WebMethod]
        public static string ObtenerFecha()
        {
            string respuesta = "";
            try
            {
                respuesta = DateTime.Now.ToString();
            }
            catch (Exception ex)
            {
                respuesta = "";
                throw ex;
            }

            return respuesta;
        }
    }
}