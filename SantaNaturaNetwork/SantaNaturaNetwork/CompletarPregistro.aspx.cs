﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class CompletarPregistro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.ClientePreRegistro> ListarPreRegistro()
        {
            List<Cliente.ClientePreRegistro> Lista = null;
            try
            {
                string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                Lista = ClienteLN.getInstance().ListarClientesPreRegistro(documento.Trim());
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static void GuardarDatosPreRegis(string idCliente)
        {
            if (idCliente != "0" && idCliente != "" && idCliente != null)
            {
                string patro = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
                string paquete = (from d in ListaPreRe where d.IdCliente == idCliente select d.IdPaquete).FirstOrDefault();
                System.Web.HttpContext.Current.Session["sTipoCompraSelect"] = paquete;
                System.Web.HttpContext.Current.Session["CBOPreRegistro"] = idCliente;
            }
        }

    }
}