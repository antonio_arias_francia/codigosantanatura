﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;
using System.Data;

namespace SantaNaturaNetwork
{
    public partial class SociosRed1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832"
                    && Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
                IniciarLlenadoCombo();
                LlenarEstablecimiento();
            }

        }

        private void IniciarLlenadoCombo()
        {
            List<TipoCliente> ListaTipo = TipoClienteLN.getInstance().ListarTipo();
            cboTipoCliente.DataSource = ListaTipo;
            cboTipoCliente.DataTextField = "tipo";
            cboTipoCliente.DataValueField = "IdTipo";
            cboTipoCliente.DataBind();
            cboTipoCliente.Items.FindByValue("06").Enabled = false;
            cboTipoCliente.Items.FindByValue("08").Enabled = false;
            cboTipoCliente.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }

        private void LlenarEstablecimiento()
        {
            List<Cliente> ListaTipo = ClienteLN.getInstance().ListaEstablecimientoColombia();
            cboTipoEstablecimiento.DataSource = ListaTipo;
            cboTipoEstablecimiento.DataTextField = "IdPeruShop";
            cboTipoEstablecimiento.DataValueField = "ruc";
            cboTipoEstablecimiento.DataBind();
            cboTipoEstablecimiento.Items.Insert(0, new ListItem("SELECCIONE", "0"));
        }

        [WebMethod]
        public static List<Cliente> ListarCliente()
        {
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarCliente();

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<Cliente> ListarCliente100()
        {
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarCliente100();
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<Cliente> ListarClienteFiltrado(string documento, string datos)
        {
            List<Cliente> Lista = null;
            try
            {
                Lista = ClienteLN.getInstance().ListarClienteFiltrado(documento, datos);
            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static bool ActualizarCliente(string idUd, string numeroDocUd, string usuarioUd, string claveUd,
                                                   string nombresUd, string apellidoPatUd, string apellidoMatUd,
                                                   string apodoUd, string fechaNacUd, string sexoUd, string tipoDocUd,
                                                   string correoUd, string telefonoUd, string celularUd, string paisUd,
                                                   string departamentoUd, string provinciaUd, string distritoUd,
                                                   string direccionUd, string referenciaUd, string detraccionUd,
                                                   string rucUd, string bancoUd, string depositoUd, string interbancariaUd,
                                                   string tipoClienteUd, string patrocinadorUd, string uplineUd,
                                                   string establecimientoUd, string imagenUd, string idPeruShop,
                                                   string idPackete, string idDescuento, string fechaPackete, string paisTienda)
        {
            string factorComi = "";
            if (uplineUd == "" & (tipoClienteUd == "03" | tipoClienteUd == "05"))
            {
                factorComi = patrocinadorUd;
            }
            else
            {
                factorComi = uplineUd;
            }
            Cliente objCliente = new Cliente()
            {
                idCliente = idUd,
                numeroDoc = numeroDocUd,
                usuario = usuarioUd.ToUpper(),
                clave = claveUd,
                nombre = nombresUd.ToUpper(),
                apellidoPat = apellidoPatUd.ToUpper(),
                apellidoMat = apellidoMatUd.ToUpper(),
                apodo = apodoUd.ToUpper(),
                fechaNac = fechaNacUd,
                sexo = sexoUd,
                tipoDoc = tipoDocUd,
                correo = correoUd,
                telefono = telefonoUd,
                celular = celularUd,
                pais = paisUd,
                departamento = departamentoUd,
                provincia = provinciaUd,
                ditrito = distritoUd,
                direccion = direccionUd.ToUpper(),
                referencia = referenciaUd.ToUpper(),
                nroCtaDetraccion = detraccionUd,
                ruc = rucUd,
                nombreBanco = bancoUd.ToUpper(),
                nroCtaDeposito = depositoUd,
                nroCtaInterbancaria = interbancariaUd,
                tipoCliente = tipoClienteUd,
                patrocinador = patrocinadorUd,
                upline = uplineUd,
                tipoEstablecimiento = establecimientoUd,
                imagen = imagenUd,
                IdPeruShop = idPeruShop,
                IdPackete = Convert.ToInt32(idDescuento),
                Packete = idPackete,
                FechaPackete = fechaPackete,
                factorComision = factorComi,
                paisTienda = paisTienda
            };
            if (tipoClienteUd == "07" | tipoClienteUd == "09") { bool updateCDR = ClienteLN.getInstance().ActualizarDniCDR(numeroDocUd, idPeruShop); }
            if (tipoClienteUd == "09") { bool updateCDR = ClienteLN.getInstance().ResetearEstablecimientoFavorito(numeroDocUd); }
            bool ok = ClienteLN.getInstance().ActualizarCLiente(objCliente);
            bool retencion = PeriodoLN.getInstance().ActualizarDocumentoRetencion(numeroDocUd, idUd);
            bool todoOK = PeriodoLN.getInstance().ActualizarDocumentoPeriodo(numeroDocUd, idUd);
            bool todoOK3 = PeriodoLN.getInstance().ActualizarDocumentoPeriodoComision(numeroDocUd, idUd);
            int IDP = PuntosNegocios.getInstance().MostrarIdPeriodoComisionActivo();
            int IDPPuntos = PuntosNegocios.getInstance().MostrarIdPeriodoPuntosActivo();
            bool todoOK2 = PeriodoLN.getInstance().ActualizarUplinePeriodo(factorComi, idUd, IDPPuntos);
            bool todoOK4 = PeriodoLN.getInstance().ActualizarUplinePeriodoComision(factorComi, idUd, IDP);
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Actualizar Cliente", "idCliente: " + idUd + " Nombre: " + nombresUd + " " + apellidoPatUd +
                                                                           " Documento: " + numeroDocUd);


            return true;
        }
        [WebMethod]
        public static bool RegistrarCliente(string numeroDocUd, string usuarioUd, string claveUd, string paisTienda,
                                                 string nombresUd, string apellidoPatUd, string apellidoMatUd,
                                                 string apodoUd, string fechaNacUd, string sexoUd, string tipoDocUd,
                                                 string correoUd, string telefonoUd, string celularUd, string paisUd,
                                                 string departamentoUd, string provinciaUd, string distritoUd,
                                                 string direccionUd, string referenciaUd, string detraccionUd,
                                                 string rucUd, string bancoUd, string depositoUd, string interbancariaUd,
                                                 string tipoClienteUd, string patrocinadorUd, string uplineUd,
                                                 string establecimientoUd, string imagenUd, string fecharegistro,
                                                 string idPeruShop, string codigoPackete)
        {
            string factorComi = "";
            if (uplineUd == "" & (tipoClienteUd == "03" | tipoClienteUd == "05"))
            {
                factorComi = patrocinadorUd;
            }
            else
            {
                factorComi = uplineUd;
            }
            Cliente objCliente = new Cliente()
            {
                numeroDoc = numeroDocUd,
                usuario = usuarioUd.ToUpper(),
                clave = claveUd,
                nombre = nombresUd.ToUpper(),
                apellidoPat = apellidoPatUd.ToUpper(),
                apellidoMat = apellidoMatUd.ToUpper(),
                apodo = apodoUd.ToUpper(),
                fechaNac = fechaNacUd,
                sexo = sexoUd,
                tipoDoc = tipoDocUd,
                correo = correoUd,
                telefono = telefonoUd,
                celular = celularUd,
                pais = paisUd,
                departamento = departamentoUd,
                provincia = provinciaUd,
                ditrito = distritoUd,
                direccion = direccionUd.ToUpper(),
                referencia = referenciaUd.ToUpper(),
                nroCtaDetraccion = detraccionUd,
                ruc = rucUd,
                nombreBanco = bancoUd.ToUpper(),
                nroCtaDeposito = depositoUd,
                nroCtaInterbancaria = interbancariaUd,
                tipoCliente = tipoClienteUd,
                patrocinador = patrocinadorUd,
                upline = uplineUd,
                tipoEstablecimiento = establecimientoUd,
                imagen = imagenUd,
                FechaRegistro = fecharegistro,
                IdPeruShop = idPeruShop,
                factorComision = factorComi,
                paisTienda = paisTienda
            };

            Packete objPackete = new Packete()
            {
                Codigo = codigoPackete,
                fechaDescuento = fecharegistro
            };
            int IDPeriodoCDR = 0, i2 = 0;
            DateTime fechaActual = DateTime.Now.AddHours(-2);
            bool okCDR = false;
            bool ok = ClienteLN.getInstance().RegistroCliente(objCliente, objPackete);
            List<Periodo> ListaPeriodos = PeriodoLN.getInstance().ListarIdPeriodo();
            List<Periodo> ListaPeriodosComision = PeriodoLN.getInstance().ListarIdPeriodoComision();
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<Cliente.CDR.PERIODOCDR> ListaPeriodoCDR = CdrLN.getInstance().ListaPeriodoCDR();
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Actualizar Cliente", "Usuario: " + usuarioUd + " Nombre: " + nombresUd + " " + apellidoPatUd +
                                                                           " Documento: " + numeroDocUd);
            foreach (var item in ListaPeriodos)
            {
                bool ok2 = PeriodoLN.getInstance().RegistroClientePeriodoMultiple(numeroDocUd, factorComi, item.idPeriodo, tipoClienteUd, patrocinadorUd);
            }
            foreach (var item in ListaPeriodosComision)
            {
                bool ok2 = PeriodoLN.getInstance().RegistroClientePeriodoMultipleComision(numeroDocUd, factorComi, patrocinadorUd, item.idPeriodoComision, tipoClienteUd);
                bool retencion = PeriodoLN.getInstance().RegistroSocioRetencionXPeriodo(numeroDocUd, item.idPeriodoComision);
            }

            if (tipoClienteUd == "07" | tipoClienteUd == "09")
            {
                while (!okCDR && i2 < ListaPeriodoCDR.Count())
                {
                    DateTime primerDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaInicio, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    DateTime ultimoDiafechaActual = DateTime.ParseExact(ListaPeriodoCDR[i2].FechaFin, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    if (fechaActual >= primerDiafechaActual & fechaActual <= ultimoDiafechaActual)
                    {
                        okCDR = true; IDPeriodoCDR = ListaPeriodoCDR[i2].IdPeriodo;
                    }
                    i2++;
                }
                Cliente.CDR.InversionCDR objCDRInversion = new Cliente.CDR.InversionCDR()
                {
                    Fecha = DateTime.Now.AddHours(-2),
                    Monto = 0,
                    DNICDR = numeroDocUd.Trim(),
                    IdInicial = "01",
                    TipoCompra = 0
                };
                Cliente.CDR.DatosPersonalesCDR objDatosPersonales = new Cliente.CDR.DatosPersonalesCDR()
                {
                    DniCDR = numeroDocUd.Trim(),
                    Nombres = "",
                    ApellidoPat = "",
                    ApellidoMat = "",
                    Documento = ""
                };
                bool registroDatosPersonales = CdrLN.getInstance().RegistrarDatosPersonalesCDR(objDatosPersonales);
                bool registroInversion = CdrLN.getInstance().RegistroInversionCDR(objCDRInversion);
                bool registroSerie = CdrLN.getInstance().RegistroSeriePuntoVenta(numeroDocUd.Trim(), "");
                bool registroCLC = CdrLN.getInstance().RegistroInicial_CLC_CDR(IDPeriodoCDR, numeroDocUd.Trim());
                List<CombosProducto> ListaLinea = ProductoLN.getInstance().ListaComboLinea();
                ListaLinea.RemoveRange(3, 3);
                foreach (var linea in ListaLinea)
                {
                    Cliente.CDR objCDR = new Cliente.CDR()
                    {
                        IdLinea = linea.idLinea,
                        CDRPS = numeroDocUd.Trim(),
                        Porcentaje = 0
                    };
                    bool registroLinea = CdrLN.getInstance().RegistroPorcentajeLineaCDR(objCDR);
                }
            }
            return true;
        }
        [WebMethod]
        public static List<Pais> GetPais()
        {
            List<Pais> ListaPais = PaisLN.getInstance().ListarPais();
            var query = from item in ListaPais
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<Departamento> GetDepartamentosByPais(string pais)
        {
            List<Departamento> ListaDepartamento = DepartamentoLN.getInstance().GetDepartamentos();
            var query = from item in ListaDepartamento
                        where item.CodigoPais == pais
                        select item;

            return query.ToList();

        }

        [WebMethod]
        public static List<Provincia> GetProvinciaByDepartamento(string departamento)
        {
            List<Provincia> ListaProvincia = ProvinciaLN.getInstance().GetProvincia();
            var query = from item in ListaProvincia
                        where item.CodigoDepartamento == departamento
                        select item;

            return query.ToList();

        }

        [WebMethod]
        public static List<Distrito> GetDistritoByProvincia(string provincia)
        {
            List<Distrito> ListaDistrito = DistritoLN.getInstance().GetDistrito();
            var query = from item in ListaDistrito
                        where item.CodigoProvincia == provincia
                        select item;

            return query.ToList();

        }

        [WebMethod]
        public static bool EliminarFilaCliente(string id, int idpaquete, string nombreS, string documentoS)
        {
            List<Compra> ComprasEliminar = CompraNegocios.getInstance().ListaTicketXCliente(id);
            foreach (var item in ComprasEliminar)
            {
                bool compra = CompraNegocios.getInstance().EliminarCompraCliente(item.Ticket);
            }
            bool ok = ClienteLN.getInstance().EliminarCliente(id, idpaquete);
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            bool registro = ClienteLN.getInstance().RegistroModificaciones(documento.Trim(), DateTime.Now.AddHours(-2).AddMinutes(-32),
                                                                           "Eliminar Cliente", "idCliente: " + id + " Nombre: " + nombreS + " Documento: " + documentoS);
            return true;
        }

        [WebMethod]
        public static string EXISTEDNI(string doc)
        {
            List<Cliente> ListaCliente = ClienteLN.getInstance().DNIEXISTE();
            if (ListaCliente.Any(x => x.numeroDoc.Trim() == doc.ToString().Trim()))
            {
                return "SI";
            }
            else
            {
                return "NO";
            }

        }

        [WebMethod]
        public static string VALIDARPATROCINADOR(string patro)
        {
            Cliente ListaCliente = ClienteLN.getInstance().ValidarPatrocinador(patro);

            if (ListaCliente.numeroDoc != "")
            {
                return ListaCliente.numeroDoc.Trim();
            }
            else
            {
                return "NO";
            }

        }

        [WebMethod]
        public static string EXISTEUSUARIO(string usu)
        {

            List<Cliente> ListaCliente = ClienteLN.getInstance().DNIEXISTE();
            if (ListaCliente.Any(x => x.usuario.Trim() == usu.ToString().Trim()))
            {
                return "SI";
            }
            else
            {
                return "NO";
            }
        }

        [WebMethod]
        public static bool EliminarImagen(string imagen)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/uploads/" + imagen);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static List<Packete> GetPackete()
        {
            List<Packete> ListaPackete = PacketeNegocios.getInstance().ListarPacketeGeneral();
            var query = from item in ListaPackete
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static void ComprimirCliente(string documentoC, string tipoClienteC, string idClienteC)
        {
            List<Cliente.Compresion.Directos> Directos = ClienteLN.getInstance().ListarDirectoSocios(documentoC);
            if (Directos.Count > 0)
            {
                List<Cliente.Compresion.DocumentoUP> DatosUP = ClienteLN.getInstance().ListarDocumentoUP(documentoC);
                foreach (var socios in Directos)
                {
                    if (socios.TipoCliente == "01")
                    {
                        ClienteLN.getInstance().ActualizarMapaRedComisionCompresion(DatosUP[0].Upline, DatosUP[0].IdUpline, socios.Documento);
                        ClienteLN.getInstance().ActualizarDatos_Directos_Compresion(DatosUP[0].Upline, DatosUP[0].Patrocinador, DatosUP[0].FactorComision, socios.Documento);
                    }
                    else
                    {
                        ClienteLN.getInstance().ActualizarMapaRedComisionCompresion(DatosUP[0].Upline, DatosUP[0].IdUpline, socios.Documento);
                        ClienteLN.getInstance().ActualizarDatos_Directos_Compresion("", DatosUP[0].Patrocinador, DatosUP[0].FactorComision, socios.Documento);
                    }
                }
                if (tipoClienteC == "01")
                {
                    ClienteLN.getInstance().ActualizarDocumentoSocioComprimido(idClienteC);
                }
                else
                {
                    ClienteLN.getInstance().ActualizarDocumento_Consultor_CI_Comprimido(idClienteC);
                }
            }
            else
            {
                if (tipoClienteC == "01")
                {
                    ClienteLN.getInstance().ActualizarDocumentoSocioComprimido(idClienteC);
                }
                else
                {
                    ClienteLN.getInstance().ActualizarDocumento_Consultor_CI_Comprimido(idClienteC);
                }
            }

        }

    }
}