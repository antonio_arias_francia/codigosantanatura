﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;

namespace SantaNaturaNetworkV3
{
    public partial class DatosPersonalesCDR_ADMIN : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832"
                    && Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.DatosPersonalesCDR> ListarDatosCDR()
        {
            List<Cliente.CDR.DatosPersonalesCDR> Lista = null;
            try
            {
                Lista = CdrLN.getInstance().ListaDatosPersonalesCDR_Admin();

            }
            catch (Exception ex)
            {
                Lista = null;
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static bool ActualizarDatosPersonalesCDR(string NombreS, string ApellidoPatS, string ApellidoMatS,
                                                        string documentoS, string docExtorno, string razonExtorno,
                                                        string dirExtorno, int idDatos)
        {
            try
            {
                Cliente.CDR.DatosPersonalesCDR objDatosPersonales = new Cliente.CDR.DatosPersonalesCDR()
                {
                    IdDatos = idDatos,
                    Nombres = NombreS,
                    ApellidoPat = ApellidoPatS,
                    ApellidoMat = ApellidoMatS,
                    Documento = documentoS,
                    DocExtorno = docExtorno,
                    RazonExtorno = razonExtorno,
                    DirExtorno = dirExtorno
                };
                CdrLN.getInstance().ActualizarDatosPersonalesCDR_ADMIN(objDatosPersonales);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

    }
}