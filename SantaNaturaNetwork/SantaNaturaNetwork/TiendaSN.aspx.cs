﻿using Modelos;
using Negocios;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetwork
{
    public partial class TiendaSN : System.Web.UI.Page { 
   
        public List<Producto> listaProductos = new List<Producto>();
        public List<ArchivosPDF.ImagenesBanners> ListaBanners = new List<ArchivosPDF.ImagenesBanners>();
        public string lineaNombre = "Linea";

        public List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
        public List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
        public string desProducto = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            else if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("Principal.aspx");
            }
            else
            {
                int cantComprasIDOP = CompraNegocios.getInstance().CantidadComprasConIDOP(Convert.ToString(Session["IdCliente"]));
                int cantComprasPend = CompraNegocios.getInstance().CantidadComprasPendientes(Convert.ToString(Session["IdCliente"]));
                if ((Convert.ToInt32(Session["PreRegistro"]) == 0 && cantComprasIDOP == 0) || (cantComprasPend > 0 && Convert.ToInt32(Session["PreRegistro"]) == 1))
                {
                    Response.Redirect("MisComprasV2.aspx");
                }
            }

            ListaTipoPago();
            ListaTienda();
            LlenarPackete();
            ListaPreRegistroCBO();
            ListaPreRegistroCBO2();
            CargarProductosByPais();
            ListaBanners = ArchivosLN.getInstance().ListarImagenesBanners();
            LbPrecioTotal.Text = "S/." + Session["SubTotal"].ToString();
            LbPrecioPagar.Text = "S/." + Session["MontoAPagar"].ToString();
            LbPuntosCompra.Text = Session["SubTotalPuntos"].ToString();
            LbPuntosRango.Text = Session["SubTotalPuntosPromocion"].ToString();
            LbPrecioTotal2.Text = "S/." + Session["SubTotal"].ToString();
            LbPrecioPagar2.Text = "S/." + Session["MontoAPagar"].ToString();
            LbPuntosCompra2.Text = Session["SubTotalPuntos"].ToString();
            LbPuntosRango2.Text = Session["SubTotalPuntosPromocion"].ToString();
            Session["PaginaAnterior"] = "Carrito";
            Session["mostrarCompraTerminada"] = 0;

            //Validamos Caducidad de Clave
            string idCliente = Convert.ToString(Session["IdCliente"]);
            string result = "";

            result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


            if (result == "Clave Vencida")
            {
                Session["clave_vencida"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }

            //Validamos si ya agregó sus preguntas de Seguridad
            result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
            if (result == "No Existe")
            {
                Session["preguntas_seguridad"] = result;
                Response.Redirect("EditarPerfil.aspx");
            }
        }

        private void LlenarPackete()
        {
            string tipoCiente = Convert.ToString(Session["TipoCliente"]);
            string packete = Convert.ToString(Session["PacketeSocio"]);
            string sessionTipoC = Session["sTipoCompraSelect"].ToString();
            int pregistro = Convert.ToInt32(Session["PreRegistro"]);
            List<Packete> ListaPackete = (pregistro == 1) ? PacketeNegocios.getInstance().ListarPacketes_PreRegistro(packete, tipoCiente) :
                                                            PacketeNegocios.getInstance().ListarPackete(packete, tipoCiente);
            STipoCompra.DataSource = ListaPackete;
            STipoCompra.DataTextField = "_Packete";
            STipoCompra.DataValueField = "Codigo";
            STipoCompra.DataBind();
            STipoCompra.Items.Insert(0, new ListItem("Seleccione", "0"));
            if (sessionTipoC != "")
            {
                STipoCompra.Value = sessionTipoC;
            }
        }

        private void ListaTipoPago()
        {
            List<TipoPago> ListaTipo = CompraNegocios.getInstance().ListaTipoPago();
            SMedioPago.DataSource = ListaTipo;
            SMedioPago.DataTextField = "descripcion";
            SMedioPago.DataValueField = "idPago";
            SMedioPago.DataBind();
            SMedioPago.Items.Insert(0, new ListItem("Seleccione", "0"));
            string TP = Convert.ToString(Session["TipoPago"]);

            if (TP != "")
            {
                SMedioPago.Value = TP;
            }
        }

        private void ListaTienda()
        {
            string sTipoCom = Convert.ToString(Session["sTipoCompraSelect"]);
            List<Cliente> ListaCDR = ClienteLN.getInstance().ListaEstablecimiento();
            ComboTienda.DataSource = ListaCDR;
            ComboTienda.DataTextField = "apodo";
            ComboTienda.DataValueField = "IdPeruShop";
            ComboTienda.DataBind();
            ComboTienda.Items.Insert(0, new ListItem("Seleccione", "0"));
            string tiendaSeleccionada = Convert.ToString(Session["comboTiendaSelect"]);
            string establecimiento = Convert.ToString(Session["Establecimiento"]);
            string linqApodo = (establecimiento == "") ? "" : (from c in ListaCDR where c.numeroDoc.Trim() == establecimiento select c.IdPeruShop.Trim()).FirstOrDefault();

            ComboTienda.Value = (establecimiento != "" && sTipoCom == "07") ? linqApodo :
                                 (tiendaSeleccionada != "") ? tiendaSeleccionada : "0";
        }

        private void ListaPreRegistroCBO()
        {
            string patro = Convert.ToString(Session["NumDocCliente"]);
            List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
            cboPreRegistro.DataSource = ListaPreRe;
            cboPreRegistro.DataTextField = "CboPreregistro";
            cboPreRegistro.DataValueField = "idcliente";
            cboPreRegistro.DataBind();
            cboPreRegistro.Items.Insert(0, new ListItem("Ninguno", "0"));

            string PR = Convert.ToString(Session["CBOPreRegistro"]);
            if (PR != "0" && PR != "" && PR != null)
            {
                cboPreRegistro.Value = PR;
            }
        }

        private void ListaPreRegistroCBO2()
        {
            string patro = Convert.ToString(Session["NumDocCliente"]);
            List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
            cboPreRegistro2.DataSource = ListaPreRe;
            cboPreRegistro2.DataTextField = "CboPreregistro";
            cboPreRegistro2.DataValueField = "idcliente";
            cboPreRegistro2.DataBind();
            cboPreRegistro2.Items.Insert(0, new ListItem("Ninguno", "0"));

            string PR = Convert.ToString(Session["CBOPreRegistro"]);
            if (PR != "0" && PR != "" && PR != null)
            {
                cboPreRegistro2.Value = PR;
            }
        }

        [WebMethod()]
        public static List<Producto> ListaProductos(string Pais, string Buscar) //Charles 24/03/2021
        {
            string tipoCL = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            return ProductoNegocios.getInstance().ListarProductosByPaisv3(Pais, Buscar, tipoCL);
        }

        void CargarProductosByPais()
        {
            string tipoCL = Convert.ToString(Session["TipoCliente"]);
            listaProductos = ProductoNegocios.getInstance().ListarProductosByPais("Peru", tipoCL);

            int cantPromoUnicaActiva = PromocionUnicaLN.getInstance().ListaCantPromoUnica();
            PromocionUnicaModel ListaPromoUnica = PromocionUnicaLN.getInstance().DatosPromoUnica();

            if (cantPromoUnicaActiva >= 1 && Convert.ToString(Session["NumDocCliente"]) != "")
            {
                string documento = Convert.ToString(Session["NumDocCliente"]);
                string idcliente = Convert.ToString(Session["IdCliente"]);
                int cantPromoUnica = ListaPromoUnica.Cantidad + 1;
                List<Cliente> DatosPuntos = ClienteLN.getInstance().ListarDatosIndex(documento);
                string IdProductoPromoUnica = PromocionUnicaLN.getInstance().ListaIdProductoPromoUnica();
                int EvalPromoUnica = PromocionUnicaLN.getInstance().ListaEvaluarCantProductosPromocionUnica(idcliente);
                double PPPromo = DatosPuntos[0].PP;
                if (PPPromo < 0 || EvalPromoUnica >= cantPromoUnica)
                {
                    listaProductos.RemoveAll(x => x.Codigo == IdProductoPromoUnica);
                }
            }

        }

        protected void btnBuscarByNombre_Click(object sender, EventArgs e)
        {
            string palabra = txtNomProducto.Text;
            BuscarProductoByNombre(palabra);
        }

        void BuscarProductoByNombre(string nombre)
        {
            string CDRSeleccionado = Convert.ToString(Session["TiendaAyuda"]);
            int cantPromoUnicaActiva = PromocionUnicaLN.getInstance().ListaCantPromoUnica();
            PromocionUnicaModel ListaPromoUnica = PromocionUnicaLN.getInstance().DatosPromoUnica();
            listaProductos = ProductoNegocios.getInstance().ListarProductosByNombre(nombre);

            if (CDRSeleccionado != "1 SAN ISIDRO")
            {
                listaProductos.RemoveAll(x => x.Codigo == "P0180");
            }

            if (cantPromoUnicaActiva >= 1)
            {
                string documento = Convert.ToString(Session["NumDocCliente"]);
                string idcliente = Convert.ToString(Session["IdCliente"]);
                List<Cliente> DatosPuntos = ClienteLN.getInstance().ListarDatosIndex(documento);
                string IdProductoPromoUnica = PromocionUnicaLN.getInstance().ListaIdProductoPromoUnica();
                int cantPromoUnica = ListaPromoUnica.Cantidad + 1;
                int EvalPromoUnica = PromocionUnicaLN.getInstance().ListaEvaluarCantProductosPromocionUnica(idcliente);
                double PPPromo = DatosPuntos[0].PP;
                if (PPPromo < 0 || EvalPromoUnica >= cantPromoUnica)
                {
                    listaProductos.RemoveAll(x => x.Codigo == IdProductoPromoUnica);
                }
            }
        }

        [WebMethod()]
        public static List<Producto> Lineas_Productos(string Linea) //Charles 30/03/2021
        {
            return ProductoNegocios.getInstance().ListarProductosByLinea(Linea);
        }

        [WebMethod]
        public static int CantidadProdCarrito()
        {
            int cantidad = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]).Count();
            return cantidad;
        }

        [WebMethod]
        public static Cliente.RetornarDatosCombo GuardarIDCombos(string comboS, string idComboS)
        {
            Cliente.RetornarDatosCombo RetornarDatos = null;
            double MontoPago = 0; string paquete = "";
            System.Web.HttpContext.Current.Session["sTipoCompraSelect"] = (comboS == "TCompra") ? idComboS : System.Web.HttpContext.Current.Session["sTipoCompraSelect"].ToString();
            System.Web.HttpContext.Current.Session["TipoPago"] = (comboS == "MPago") ? idComboS : System.Web.HttpContext.Current.Session["TipoPago"].ToString();
            System.Web.HttpContext.Current.Session["comboTiendaSelect"] = (comboS == "TiendaS") ? idComboS : System.Web.HttpContext.Current.Session["comboTiendaSelect"].ToString();
            System.Web.HttpContext.Current.Session["CBOPreRegistro"] = (comboS == "Pregis") ? idComboS : System.Web.HttpContext.Current.Session["CBOPreRegistro"].ToString();
            if (comboS == "TCompra") { MontoPago = AplicarDescuento(idComboS); }
            if (comboS == "Pregis" && (idComboS != "0" && idComboS != "" && idComboS != null)) {
                string patro = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
                paquete = (from d in ListaPreRe where d.IdCliente == idComboS select d.IdPaquete).FirstOrDefault();
                System.Web.HttpContext.Current.Session["sTipoCompraSelect"] = paquete;
            }
            RetornarDatos = new Cliente.RetornarDatosCombo(){
                MontoPago = MontoPago,
                IdPaquete = paquete
            };
            return RetornarDatos;
        }

        [WebMethod]
        public static string ObtenerTipoCliente()
        {
            string tipoCliente = "", cboPrere = System.Web.HttpContext.Current.Session["CBOPreRegistro"].ToString();
            if (cboPrere != "0" && cboPrere != "" && cboPrere != null)
            {
                string patro = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
                List<Cliente.ClientePreRegistro> ListaPreRe = ClienteLN.getInstance().ListarClientesPreRegistro(patro);
                tipoCliente = (from d in ListaPreRe where d.IdCliente == cboPrere select d.TipoCliente).FirstOrDefault();
            }
            
            return tipoCliente;
        }

        private static double AplicarDescuento(string sTipoCom)
        {
            List<ProductoCarrito> productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            List<ProductoCarrito> productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);
            string packeteSocio = Convert.ToString(System.Web.HttpContext.Current.Session["PacketeSocio"]);
            string tipCliente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            double montoAPagar = 0.0;

            foreach (var itemR in productosCarritoPaquete)
            {
                itemR.SubTotalNeto = 0;
            }
            foreach (var item in productosCarrito)
            {

                item.SubTotalNeto = ObtenerSubTotalNeto(sTipoCom, packeteSocio, item.SubTotal, tipCliente, item.Linea);
                foreach (var itemP in productosCarritoPaquete)
                {
                    if (item.idPaquete != "0" && itemP.idPaquete == item.idPaquete)
                    {
                        itemP.SubTotalNeto = itemP.SubTotalNeto + item.SubTotalNeto;
                    }
                    else if (itemP.idPaquete == "0" && item.idPaquete == "0")
                    {
                        itemP.SubTotalNeto = ObtenerSubTotalNeto(sTipoCom, packeteSocio, itemP.SubTotal, tipCliente, itemP.Linea);
                    }
                    System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
                }

                System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
                //System.Web.HttpContext.Current.Session["MontoAPagar"] = 0.0; //Reseteo para darle el montoAPagar con descuento
                montoAPagar = montoAPagar + item.SubTotalNeto;
            }
            System.Web.HttpContext.Current.Session["MontoAPagar"] = montoAPagar;
            return montoAPagar;
        }

        [WebMethod]
        public static ProductoV2.RetornarDatosCarrito AgregarProducto(string codProductoCarrito, string cantidadProductos)
        {
            List<Cliente> ListaCDR = ClienteLN.getInstance().ListaEstable();
            ProductoV2.AgregarCarrito Agregar = null;
            ProductoV2.RetornarDatosCarrito RetornarDatos = null;
            string idCDR = System.Web.HttpContext.Current.Session["comboTiendaSelect"].ToString();
            string establecimiento = (from c in ListaCDR where c.IdPeruShop.Trim() == idCDR select c.apodo.Trim()).FirstOrDefault();
            string tipoPago = System.Web.HttpContext.Current.Session["TipoPago"].ToString();
            string sTipCompra = System.Web.HttpContext.Current.Session["sTipoCompraSelect"].ToString();
            string cantidadExisteM = "";

            System.Web.HttpContext.Current.Session["TiendaAyuda"] = establecimiento;
            System.Web.HttpContext.Current.Session["TipoPago"] = tipoPago;
            System.Web.HttpContext.Current.Session["TiendaRetorna"] = 0;

            ResponseStock responseStock = new ResponseStock();
            responseStock.Productos = new List<string>();
            responseStock.Stock = true;
            List<Producto> productoSeleccionado = ProductoNegocios.getInstance().ListarProductosByCodigo(codProductoCarrito);
            List<ProductoV2> ListaProductoG = ProductoLN.getInstance().ListaIdopXNombre();
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            List<ProductoCarrito> productosCarritoPaquete = new List<ProductoCarrito>();
            List<PaqueteNatura> listaPaqueteNatura = new List<PaqueteNatura>();
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);

            string packeteSocio = Convert.ToString(System.Web.HttpContext.Current.Session["PacketeSocio"]);
            string tipCliente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            string idcliente = Convert.ToString(System.Web.HttpContext.Current.Session["IdCliente"]);
            double aumento = 1;
            double sumaPVP = 0;
            PromocionUnicaModel ListaPromoUnica = PromocionUnicaLN.getInstance().DatosPromoUnica();
            int EVAL1 = 0, EVAL2 = 0;
            int cantPromoUnicaActiva = PromocionUnicaLN.getInstance().ListaCantPromoUnica();
            string IdProductoPromoUnica = PromocionUnicaLN.getInstance().ListaIdProductoPromoUnica();

            //Verificar stock de los productos

            string idProdPeruShop = productoSeleccionado[0].IdProdPeruShop;
            string idProdPais = productoSeleccionado[0].Codigo;
            string nombreProducto = productoSeleccionado[0].NombreProducto;
            string idPaquete = productoSeleccionado[0].idPaquete;
            int EvalPromoUnica = 0;
            int cantidad = Convert.ToInt32(cantidadProductos);

            if (cantPromoUnicaActiva >= 1 & codProductoCarrito == IdProductoPromoUnica)
            {
                string IdPPxPromo = ListaPromoUnica.ProductoPais;
                int cantPromoUnica = ListaPromoUnica.Cantidad;
                int cantProductosCarrito = (from c in productosCarrito where c.Codigo.Trim() == IdPPxPromo.Trim() select c.Cantidad).FirstOrDefault();
                if (sTipCompra != "01" | sTipCompra != "02" | sTipCompra != "03" | sTipCompra != "04" | sTipCompra != "05" | sTipCompra != "06" | sTipCompra != "13")
                {
                    EvalPromoUnica = PromocionUnicaLN.getInstance().ListaEvaluarCantProductosPromocionUnica(idcliente);
                }

                int SumaCantProdPromo = cantProductosCarrito + Convert.ToInt32(cantidadProductos) + EvalPromoUnica;
                //if (Convert.ToInt32(cantidadProductos) > cantPromoUnica) { EVAL1 = 1; }
                if (SumaCantProdPromo > cantPromoUnica) { EVAL2 = 1; }
            }
            int SumaEvaluaciones = EVAL1 + EVAL2;
            if (SumaEvaluaciones == 0)
            {
                if (idPaquete != "0")
                {
                    listaPaqueteNatura = PaqueteLN.getInstance().ListaPaqueteByCodigoPeru(idPaquete);
                    foreach (var item2 in listaPaqueteNatura)
                    {
                        string IDPS = (from c in ListaProductoG where c.IdProducto.Trim() == item2.idProductoGeneral.Trim() select c.IdProductoPeruShop.Trim()).FirstOrDefault();
                        string IDPP = (from c in ListaProductoG where c.IdProducto.Trim() == item2.idProductoGeneral.Trim() select c.ProductoPais.Trim()).FirstOrDefault();
                        int canSend = cantidad * item2.cantidadGeneral;
                        Agregar = VerificarSiHayStock3(IDPS, canSend, IDPP);
                        cantidadExisteM = (Agregar.Estado == true) ? "0": Agregar.CantidadExistente;
                        bool stock = Agregar.Estado;

                        if (stock)
                        {
                            responseStock.Stock = (responseStock.Stock == false) ? false : true;
                        }
                        else
                        {
                            string productoSinStock = (from c in productosCarrito where c.IdProdPeruShop.Trim() == IDPS.Trim() select c.NombreProducto).FirstOrDefault();
                            responseStock.Stock = false;
                            responseStock.Establecimiento = establecimiento;
                            responseStock.Productos.Add(productoSinStock);
                        }
                    }

                    if (responseStock.Stock)
                    {
                        if (productosCarritoPaquete.Any(x => x.idPaquete == idPaquete))
                        {
                            int CC = 0;
                            double subTotalNetoPaquete = 0.00;
                            double subTotalPuntosPaquete = 0.00;
                            double subTotalPPromocionPaquete = 0.00;
                            foreach (var itemP in listaPaqueteNatura)
                            {
                                double nuevoMontoTotal = 0.00;
                                double nuevoCorazonesTotal = 0.00;
                                double nuevoPuntosTotal = 0.00;
                                double nuevoPuntosTotalPromocion = 0.00;
                                double nuevoMontoAPagar = 0.00;
                                int cantidadPaquete = cantidad * itemP.cantidadGeneral;
                                CC++;

                                foreach (var item in productosCarrito)
                                {
                                    if (item.CodigoValidar == itemP.idProductoGeneral & item.idPaquete == idPaquete)
                                    {
                                        item.Cantidad = item.Cantidad + cantidadPaquete;
                                        item.cantidadPeruShop = item.cantidadPeruShop + cantidadPaquete;
                                        item.SubTotal = item.Cantidad * item.PrecioUnitario;
                                        item.SubTotalNeto = ObtenerSubTotalNeto(sTipCompra, packeteSocio, item.SubTotal, tipCliente, item.Linea);
                                        subTotalNetoPaquete = subTotalNetoPaquete + item.SubTotalNeto;
                                        item.SubTotalPuntos = item.Cantidad * item.Puntos;
                                        subTotalPuntosPaquete = subTotalPuntosPaquete + item.SubTotalPuntos;
                                        item.SubTotalPuntosPromocion = item.Cantidad * item.PuntosPromocion;
                                        subTotalPPromocionPaquete = subTotalPPromocionPaquete + item.SubTotalPuntosPromocion;

                                        System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
                                    }
                                    nuevoCorazonesTotal = nuevoCorazonesTotal + item.SubTotalCorazones;
                                    nuevoPuntosTotal = nuevoPuntosTotal + item.SubTotalPuntos;
                                    nuevoPuntosTotalPromocion = nuevoPuntosTotalPromocion + item.SubTotalPuntosPromocion;
                                    nuevoMontoTotal = nuevoMontoTotal + item.SubTotal;
                                    nuevoMontoAPagar = nuevoMontoAPagar + item.SubTotalNeto;
                                }
                                if (CC == listaPaqueteNatura.Count)
                                {
                                    foreach (var item2 in productosCarritoPaquete)
                                    {
                                        if (item2.CodigoValidar == codProductoCarrito & item2.idPaquete == idPaquete)
                                        {
                                            item2.Cantidad = item2.Cantidad + cantidad;
                                            item2.cantidadPeruShop = item2.cantidadPeruShop + cantidad;
                                            item2.SubTotal = item2.Cantidad * item2.PrecioUnitario;
                                            item2.SubTotalNeto = subTotalNetoPaquete;
                                            item2.SubTotalPuntos = subTotalPuntosPaquete;
                                            item2.SubTotalPuntosPromocion = subTotalPPromocionPaquete;

                                            System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
                                        }
                                    }
                                }

                                System.Web.HttpContext.Current.Session["SubTotalPuntos"] = nuevoPuntosTotal;
                                System.Web.HttpContext.Current.Session["SubTotalCorazones"] = nuevoCorazonesTotal;
                                System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = nuevoPuntosTotalPromocion;
                                System.Web.HttpContext.Current.Session["SubTotal"] = nuevoMontoTotal;
                                System.Web.HttpContext.Current.Session["MontoAPagar"] = nuevoMontoAPagar;
                            }
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "OK",
                                Total = System.Web.HttpContext.Current.Session["SubTotal"].ToString(),
                                Monto = System.Web.HttpContext.Current.Session["MontoAPagar"].ToString(),
                                Puntos = System.Web.HttpContext.Current.Session["SubTotalPuntos"].ToString(),
                                PuntosPromo = System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"].ToString()
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            int CC = 0;
                            double subTotalPaquete = 0, subTotalNetoPaquete = 0, subTotalPSPaquete = 0, subTotalPuntosPaquete = 0,
                                   subTotalPuntosPromocionPaquete = 0, subTotalCorazonesPaquete = 0;
                            foreach (var item in listaPaqueteNatura)
                            {
                                CC++;
                                productoSeleccionado = ProductoNegocios.getInstance().ListarProductosByCodigo(item.idProductoGeneral.Trim());
                                double puntosProducto = productoSeleccionado[0].Puntos;
                                double corazonesProducto = productoSeleccionado[0].Corazones;
                                double puntosProductoPromocion = productoSeleccionado[0].PuntosPromocion;
                                double precioProducto = productoSeleccionado[0].PrecioUnitario;
                                int cantidadPaquete = cantidad * item.cantidadGeneral;
                                string foto = productoSeleccionado[0].Foto;
                                string idProducto = productoSeleccionado[0].Codigo;
                                string producto = productoSeleccionado[0].NombreProducto;
                                double PrecioUnitario = productoSeleccionado[0].PrecioUnitario;
                                string linea = productoSeleccionado[0].Linea;

                                idProdPeruShop = productoSeleccionado[0].IdProdPeruShop;

                                System.Web.HttpContext.Current.Session["Foto"] = foto;
                                System.Web.HttpContext.Current.Session["IdProducto"] = idProducto;
                                System.Web.HttpContext.Current.Session["Producto"] = producto;
                                System.Web.HttpContext.Current.Session["PrecioUnitario"] = PrecioUnitario;
                                System.Web.HttpContext.Current.Session["Cantidad"] = cantidadPaquete;

                                double subTotalCorazones = corazonesProducto * cantidadPaquete;
                                double subTotalPuntos = puntosProducto * cantidadPaquete;
                                double subTotalPuntosPromocion = puntosProductoPromocion * cantidadPaquete;
                                double cantidadSave = cantidadPaquete;
                                double subTotal = precioProducto * cantidadPaquete;
                                double subSend = precioProducto * Double.Parse("1");
                                double subTotalNeto = ObtenerSubTotalNeto(sTipCompra, packeteSocio, subTotal, tipCliente, linea);
                                double subTotalPS = ObtenerSubTotalNeto(sTipCompra, packeteSocio, subSend, tipCliente, linea);
                                string precioStr = Convert.ToString(subTotalPS);
                                subTotalPaquete = subTotalPaquete + subTotal;
                                subTotalNetoPaquete = subTotalNetoPaquete + subTotalNeto;
                                subTotalPSPaquete = subTotalPSPaquete + subTotalPS;
                                subTotalPuntosPaquete = subTotalPuntosPaquete + subTotalPuntos;
                                subTotalPuntosPromocionPaquete = subTotalPuntosPromocionPaquete + subTotalPuntosPromocion;
                                subTotalCorazonesPaquete = subTotalCorazonesPaquete + subTotalCorazones;

                                ProductoCarrito productoAgregado = new ProductoCarrito();
                                productoAgregado.Codigo = idProducto;
                                productoAgregado.CodigoValidar = item.idProductoGeneral.Trim();
                                productoAgregado.IdProdPeruShop = idProdPeruShop;
                                productoAgregado.NombreProducto = producto;
                                productoAgregado.PrecioUnitario = precioProducto;
                                productoAgregado.Foto = foto;
                                productoAgregado.Corazones = corazonesProducto;
                                productoAgregado.Puntos = puntosProducto;
                                productoAgregado.PuntosPromocion = puntosProductoPromocion;
                                productoAgregado.Cantidad = cantidadPaquete;
                                productoAgregado.cantidadPeruShop = cantidadPaquete;
                                productoAgregado.SubTotal = subTotal;
                                productoAgregado.Linea = linea;
                                productoAgregado.precioStr = precioStr;
                                productoAgregado.SubTotalNeto = subTotalNeto;
                                productoAgregado.montoSend = subTotalPS;
                                productoAgregado.SubTotalCorazones = subTotalCorazones;
                                productoAgregado.SubTotalPuntos = subTotalPuntos;
                                productoAgregado.SubTotalPuntosPromocion = subTotalPuntosPromocion;
                                productoAgregado.idPaquete = idPaquete;

                                ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]).Add(productoAgregado);

                                System.Web.HttpContext.Current.Session["SubTotalCorazones"] = (double)System.Web.HttpContext.Current.Session["SubTotalCorazones"] + subTotalCorazones;
                                System.Web.HttpContext.Current.Session["SubTotalPuntos"] = (double)System.Web.HttpContext.Current.Session["SubTotalPuntos"] + subTotalPuntos;
                                System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = (double)System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] + subTotalPuntosPromocion;
                                System.Web.HttpContext.Current.Session["SubTotal"] = (double)System.Web.HttpContext.Current.Session["SubTotal"] + subTotal;
                                System.Web.HttpContext.Current.Session["MontoAPagar"] = (double)System.Web.HttpContext.Current.Session["MontoAPagar"] + (subTotalNeto);

                                double totalCorazones = (double)(System.Web.HttpContext.Current.Session["SubTotalCorazones"]);
                                double totalPuntos = (double)(System.Web.HttpContext.Current.Session["SubTotalPuntos"]);
                                double totalPuntosPromo = (double)(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]);
                                double montoTotal = (double)(System.Web.HttpContext.Current.Session["SubTotal"]);
                                double montoAPagar = (double)System.Web.HttpContext.Current.Session["MontoAPagar"];

                                if (CC == listaPaqueteNatura.Count())
                                {
                                    productoSeleccionado = ProductoNegocios.getInstance().ListarProductosByCodigo(codProductoCarrito);
                                    ProductoCarrito productoAgregadoPaquete = new ProductoCarrito();
                                    productoAgregadoPaquete.Codigo = productoSeleccionado[0].Codigo;
                                    productoAgregadoPaquete.CodigoValidar = codProductoCarrito;
                                    productoAgregadoPaquete.IdProdPeruShop = productoSeleccionado[0].IdProdPeruShop;
                                    productoAgregadoPaquete.NombreProducto = productoSeleccionado[0].NombreProducto;
                                    productoAgregadoPaquete.PrecioUnitario = productoSeleccionado[0].PrecioUnitario;
                                    productoAgregadoPaquete.Foto = productoSeleccionado[0].Foto;
                                    productoAgregadoPaquete.Puntos = productoSeleccionado[0].Puntos;
                                    productoAgregadoPaquete.PuntosPromocion = productoSeleccionado[0].PuntosPromocion;
                                    productoAgregadoPaquete.Cantidad = cantidad;
                                    productoAgregadoPaquete.cantidadPeruShop = cantidad;
                                    productoAgregadoPaquete.SubTotal = subTotalPaquete;
                                    productoAgregadoPaquete.Linea = linea;
                                    productoAgregadoPaquete.precioStr = precioStr;
                                    productoAgregadoPaquete.SubTotalNeto = subTotalNetoPaquete;
                                    productoAgregadoPaquete.montoSend = subTotalPSPaquete;
                                    productoAgregadoPaquete.SubTotalCorazones = subTotalCorazonesPaquete;
                                    productoAgregadoPaquete.SubTotalPuntos = subTotalPuntosPaquete;
                                    productoAgregadoPaquete.SubTotalPuntosPromocion = subTotalPuntosPromocionPaquete;
                                    productoAgregadoPaquete.idPaquete = idPaquete;
                                    ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]).Add(productoAgregadoPaquete);
                                }
                            }

                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "OK",
                                Total = System.Web.HttpContext.Current.Session["SubTotal"].ToString(),
                                Monto = System.Web.HttpContext.Current.Session["MontoAPagar"].ToString(),
                                Puntos = System.Web.HttpContext.Current.Session["SubTotalPuntos"].ToString(),
                                PuntosPromo = System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"].ToString()
                            };
                            return RetornarDatos;
                        }
                    }
                    else
                    {
                        string establecimientoSinStock = responseStock.Establecimiento;
                        string horaRestante = HorasRestantes(idProdPeruShop);

                        if (horaRestante == "0")
                        {
                            if (Convert.ToDouble(cantidadExisteM) < 0)
                            {
                                cantidadExisteM = "0";
                            }
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock + ". Actualmente contamos con " + cantidadExisteM + "",
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "" + horaRestante + "",
                            };
                            return RetornarDatos;
                        }
                    }

                }
                else
                {

                    Agregar = VerificarSiHayStock2(idProdPeruShop, cantidad, idProdPais);
                    cantidadExisteM = Agregar.CantidadExistente;
                    bool stock = Agregar.Estado;
                    if (stock)
                    {
                        responseStock.Stock = true;
                    }
                    else
                    {
                        responseStock.Stock = false;
                        responseStock.Establecimiento = establecimiento;
                        responseStock.Productos.Add(nombreProducto);
                    }

                    if (responseStock.Stock)
                    {
                        if (productosCarritoPaquete.Any(x => x.CodigoValidar == codProductoCarrito))
                        {
                            double nuevoMontoTotal = 0.00;
                            double nuevoCorazonesTotal = 0.00;
                            double nuevoPuntosTotal = 0.00;
                            double nuevoPuntosTotalPromocion = 0.00;
                            double nuevoMontoAPagar = 0.00;

                            foreach (var item in productosCarrito)
                            {
                                if (item.CodigoValidar == codProductoCarrito & item.idPaquete == "0")
                                {
                                    item.Cantidad = item.Cantidad + Convert.ToInt32(cantidadProductos);
                                    item.cantidadPeruShop = item.cantidadPeruShop + Convert.ToInt32(cantidadProductos);
                                    item.SubTotal = item.Cantidad * item.PrecioUnitario;
                                    item.SubTotalNeto = ObtenerSubTotalNeto(sTipCompra, packeteSocio, item.SubTotal, tipCliente, item.Linea);
                                    item.SubTotalPuntos = item.Cantidad * item.Puntos;
                                    item.SubTotalCorazones = item.Cantidad * item.Corazones;
                                    item.SubTotalPuntosPromocion = item.Cantidad * item.PuntosPromocion;

                                    System.Web.HttpContext.Current.Session["CarritoProducto"] = productosCarrito;
                                }
                                nuevoCorazonesTotal = nuevoCorazonesTotal + item.SubTotalCorazones;
                                nuevoPuntosTotal = nuevoPuntosTotal + item.SubTotalPuntos;
                                nuevoPuntosTotalPromocion = nuevoPuntosTotalPromocion + item.SubTotalPuntosPromocion;
                                nuevoMontoTotal = nuevoMontoTotal + item.SubTotal;
                                nuevoMontoAPagar = nuevoMontoAPagar + item.SubTotalNeto;
                            }
                            foreach (var itemP in productosCarritoPaquete)
                            {
                                if (itemP.CodigoValidar == codProductoCarrito & itemP.idPaquete == "0")
                                {
                                    itemP.Cantidad = itemP.Cantidad + Convert.ToInt32(cantidadProductos);
                                    itemP.cantidadPeruShop = itemP.cantidadPeruShop + Convert.ToInt32(cantidadProductos);
                                    itemP.SubTotal = itemP.Cantidad * itemP.PrecioUnitario;
                                    itemP.SubTotalNeto = ObtenerSubTotalNeto(sTipCompra, packeteSocio, itemP.SubTotal, tipCliente, itemP.Linea);
                                    itemP.SubTotalPuntos = itemP.Cantidad * itemP.Puntos;
                                    itemP.SubTotalCorazones = itemP.Cantidad * itemP.Corazones;
                                    itemP.SubTotalPuntosPromocion = itemP.Cantidad * itemP.PuntosPromocion;

                                    System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"] = productosCarritoPaquete;
                                }
                            }
                            System.Web.HttpContext.Current.Session["SubTotalCorazones"] = nuevoCorazonesTotal;
                            System.Web.HttpContext.Current.Session["SubTotalPuntos"] = nuevoPuntosTotal;
                            System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = nuevoPuntosTotalPromocion;
                            System.Web.HttpContext.Current.Session["SubTotal"] = nuevoMontoTotal;
                            System.Web.HttpContext.Current.Session["MontoAPagar"] = nuevoMontoAPagar;

                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "OK",
                                Total = System.Web.HttpContext.Current.Session["SubTotal"].ToString(),
                                Monto = System.Web.HttpContext.Current.Session["MontoAPagar"].ToString(),
                                Puntos = System.Web.HttpContext.Current.Session["SubTotalPuntos"].ToString(),
                                PuntosPromo = System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"].ToString()
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            double puntosProducto = productoSeleccionado[0].Puntos;
                            double corazonesProducto = productoSeleccionado[0].Corazones;
                            double puntosProductoPromocion = productoSeleccionado[0].PuntosPromocion;
                            double precioProducto = (productoSeleccionado[0].PrecioUnitario + sumaPVP) * aumento;

                            string foto = productoSeleccionado[0].Foto;
                            string idProducto = productoSeleccionado[0].Codigo;
                            string producto = productoSeleccionado[0].NombreProducto;
                            double PrecioUnitario = (productoSeleccionado[0].PrecioUnitario + sumaPVP) * aumento;
                            string linea = productoSeleccionado[0].Linea;
                            idProdPeruShop = productoSeleccionado[0].IdProdPeruShop;

                            System.Web.HttpContext.Current.Session["Foto"] = foto;
                            System.Web.HttpContext.Current.Session["IdProducto"] = idProducto;
                            System.Web.HttpContext.Current.Session["Producto"] = producto;
                            System.Web.HttpContext.Current.Session["PrecioUnitario"] = PrecioUnitario;
                            System.Web.HttpContext.Current.Session["Cantidad"] = cantidadProductos;

                            double subTotalPuntos = puntosProducto * Int32.Parse(cantidadProductos);
                            double subTotalCorazones = corazonesProducto * Int32.Parse(cantidadProductos);
                            double subTotalPuntosPromocion = puntosProductoPromocion * Int32.Parse(cantidadProductos);
                            double cantidadSave = Int32.Parse(cantidadProductos);
                            double subTotal = precioProducto * Double.Parse(cantidadProductos);
                            double subSend = precioProducto * Double.Parse("1");
                            double subTotalNeto = ObtenerSubTotalNeto(sTipCompra, packeteSocio, subTotal, tipCliente, linea);
                            double subTotalPS = ObtenerSubTotalNeto(sTipCompra, packeteSocio, subSend, tipCliente, linea);
                            string precioStr = Convert.ToString(subTotalPS);

                            //ProductoCarrito productoAgregado = new ProductoCarrito(idProducto, producto, precioProducto, foto, puntosProducto, Int32.Parse(cantidadProductos), subTotal, subTotalPuntos);

                            ProductoCarrito productoAgregado = new ProductoCarrito();
                            productoAgregado.Codigo = idProducto;
                            productoAgregado.CodigoValidar = codProductoCarrito;
                            productoAgregado.IdProdPeruShop = idProdPeruShop;
                            productoAgregado.NombreProducto = producto;
                            productoAgregado.PrecioUnitario = precioProducto;
                            productoAgregado.Foto = foto;
                            productoAgregado.Corazones = corazonesProducto;
                            productoAgregado.Puntos = puntosProducto;
                            productoAgregado.PuntosPromocion = puntosProductoPromocion;
                            productoAgregado.Cantidad = Int32.Parse(cantidadProductos);
                            productoAgregado.cantidadPeruShop = Int32.Parse(cantidadProductos);
                            productoAgregado.SubTotal = subTotal;
                            productoAgregado.Linea = linea;
                            productoAgregado.precioStr = precioStr;
                            productoAgregado.SubTotalNeto = subTotalNeto;
                            productoAgregado.montoSend = subTotalPS;
                            productoAgregado.SubTotalCorazones = subTotalCorazones;
                            productoAgregado.SubTotalPuntos = subTotalPuntos;
                            productoAgregado.SubTotalPuntosPromocion = subTotalPuntosPromocion;
                            productoAgregado.idPaquete = "0";

                            ProductoCarrito productoAgregadoPaquete = new ProductoCarrito();
                            productoAgregadoPaquete.Codigo = idProducto;
                            productoAgregadoPaquete.CodigoValidar = codProductoCarrito;
                            productoAgregadoPaquete.IdProdPeruShop = idProdPeruShop;
                            productoAgregadoPaquete.NombreProducto = producto;
                            productoAgregadoPaquete.PrecioUnitario = precioProducto;
                            productoAgregadoPaquete.Foto = foto;
                            productoAgregadoPaquete.Corazones = corazonesProducto;
                            productoAgregadoPaquete.Puntos = puntosProducto;
                            productoAgregadoPaquete.PuntosPromocion = puntosProductoPromocion;
                            productoAgregadoPaquete.Cantidad = Int32.Parse(cantidadProductos);
                            productoAgregadoPaquete.cantidadPeruShop = Int32.Parse(cantidadProductos);
                            productoAgregadoPaquete.SubTotal = subTotal;
                            productoAgregadoPaquete.Linea = linea;
                            productoAgregadoPaquete.precioStr = precioStr;
                            productoAgregadoPaquete.SubTotalNeto = subTotalNeto;
                            productoAgregadoPaquete.montoSend = subTotalPS;
                            productoAgregadoPaquete.SubTotalCorazones = subTotalCorazones;
                            productoAgregadoPaquete.SubTotalPuntos = subTotalPuntos;
                            productoAgregadoPaquete.SubTotalPuntosPromocion = subTotalPuntosPromocion;
                            productoAgregadoPaquete.idPaquete = "0";

                            //ProductoCarrito productoAgregado = new ProductoCarrito() { Foto = foto, Codigo = idProducto, NombreProducto = producto, PrecioUnitario = precioProducto, Cantidad = cantidadProductos, Puntos = puntosProducto, SubTotal = subTotal, SubTotalPuntos = subTotalPuntos };

                            ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]).Add(productoAgregado);
                            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);
                            ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]).Add(productoAgregadoPaquete);
                            productosCarritoPaquete = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProductoPaquete2"]);


                            System.Web.HttpContext.Current.Session["SubTotalPuntos"] = (double)System.Web.HttpContext.Current.Session["SubTotalPuntos"] + subTotalPuntos;
                            System.Web.HttpContext.Current.Session["SubTotalCorazones"] = (double)System.Web.HttpContext.Current.Session["SubTotalCorazones"] + subTotalCorazones;
                            System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] = (double)System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"] + subTotalPuntosPromocion;
                            System.Web.HttpContext.Current.Session["SubTotal"] = (double)System.Web.HttpContext.Current.Session["SubTotal"] + subTotal;
                            System.Web.HttpContext.Current.Session["MontoAPagar"] = (double)System.Web.HttpContext.Current.Session["MontoAPagar"] + (subTotalNeto);

                            double totalCorazones = (double)(System.Web.HttpContext.Current.Session["SubTotalCorazones"]);
                            double totalPuntos = (double)(System.Web.HttpContext.Current.Session["SubTotalPuntos"]);
                            double totalPuntosPromo = (double)(System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"]);
                            double montoTotal = (double)(System.Web.HttpContext.Current.Session["SubTotal"]);
                            double montoAPagar = (double)System.Web.HttpContext.Current.Session["MontoAPagar"];

                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "OK",
                                Total = System.Web.HttpContext.Current.Session["SubTotal"].ToString(),
                                Monto = System.Web.HttpContext.Current.Session["MontoAPagar"].ToString(),
                                Puntos = System.Web.HttpContext.Current.Session["SubTotalPuntos"].ToString(),
                                PuntosPromo = System.Web.HttpContext.Current.Session["SubTotalPuntosPromocion"].ToString()
                            };
                            return RetornarDatos;
                        }
                    }
                    else
                    {
                        string establecimientoSinStock = responseStock.Establecimiento;
                        string horaRestante = HorasRestantes(idProdPeruShop);

                        if (horaRestante == "0")
                        {
                            if (Convert.ToDouble(cantidadExisteM) < 0)
                            {
                                cantidadExisteM = "0";
                            }

                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "Stock insuficiente en el Establecimiento " + establecimientoSinStock + ". Actualmente contamos con " + cantidadExisteM + ""
                            };
                            return RetornarDatos;
                        }
                        else
                        {
                            RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                            {
                                Mensaje = "" + horaRestante + ""
                            };
                            return RetornarDatos;

                        }

                    }

                }
            }
            else
            {
                RetornarDatos = new ProductoV2.RetornarDatosCarrito()
                {
                    Mensaje = "Ha superado la cantidad máxima de esta promoción"
                };
            }
            return RetornarDatos;
        }

        private static string HorasRestantes(string IDPPS)
        {

            string respuesta = "";
            string tipoPago = System.Web.HttpContext.Current.Session["TipoPago"].ToString();
            int idLimite = (tipoPago == "04") ? 2 : 1;
            List<HoraCantidad> ListaHC = CompraNegocios.getInstance().ObtenerHoraCompraStock(IDPPS);
            List<LimiteDias> listaFecha = FechasLimiteLN.getInstance().ListaLimite(idLimite);
            int horasLimite = listaFecha[0].numeroDias;
            if (ListaHC[0].fecha != "0")
            {
                string cantidadTotal = CompraNegocios.getInstance().cantidadTotalStock(IDPPS);
                DateTime fechaCompra = Convert.ToDateTime(ListaHC[0].fecha);
                DateTime fechaLimite = fechaCompra.AddHours(horasLimite);
                DateTime ahora = DateTime.Now.AddHours(-2);
                TimeSpan resta = fechaLimite - ahora;
                if (resta.Minutes < 0)
                {
                    respuesta = "0";
                }
                else
                {
                    respuesta = "Stock total separado (" + cantidadTotal + " unds). En caso de no finalizarse la compra, en " + Convert.ToString(resta.Hours) + "h. y " + Convert.ToString(resta.Minutes) + " m. se podrán habilitar " + ListaHC[0].cantidad + " unds";
                }

            }
            else
            {
                respuesta = "0";
            }

            return respuesta;
        }
        
        private static ProductoV2.AgregarCarrito VerificarSiHayStock2(string IDPS, int cantidad, string idProdPais)
        {
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            ProductoV2.AgregarCarrito retornoStock = null;
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);

            bool stock = false;
            string cantidadExisteM = "";
            string tiendaSeleccionada = System.Web.HttpContext.Current.Session["comboTiendaSelect"].ToString();
            string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == tiendaSeleccionada.Trim() select c.DNICDR.Trim()).FirstOrDefault();
            bool ControlStock = StockLN.getInstance().ObtenerControlStockXIDPP(idProdPais, dniCDR);

            if (ControlStock == true)
            {
                int cantProductosCarrito = (from c in productosCarrito where c.IdProdPeruShop.Trim() == IDPS.Trim() select c.Cantidad).FirstOrDefault();
                int cantidadExistente = StockLN.getInstance().CantidadStockCDR(dniCDR, IDPS, "01", idProdPais);
                cantidadExisteM = cantidadExistente.ToString();
                if ((cantidad + cantProductosCarrito) <= cantidadExistente) { stock = true; }
            }
            else { stock = true; }

            retornoStock = new ProductoV2.AgregarCarrito() { CantidadExistente = cantidadExisteM, Estado = stock };

            return retornoStock;
        }

        private static ProductoV2.AgregarCarrito VerificarSiHayStock3(string IDPS, int cantidad, string IDPP)
        {
            List<StockCDR> ListaStock = StockLN.getInstance().ListarCDRStock();
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            ProductoV2.AgregarCarrito retornoStock = null;
            productosCarrito = ((List<ProductoCarrito>)System.Web.HttpContext.Current.Session["CarritoProducto"]);

            bool stock = false;
            string cantidadExisteM = "";
            string tiendaSeleccionada = System.Web.HttpContext.Current.Session["comboTiendaSelect"].ToString();
            string dniCDR = (from c in ListaStock where c.CDRPS.Trim() == tiendaSeleccionada.Trim() select c.DNICDR.Trim()).FirstOrDefault();
            bool ControlStock = StockLN.getInstance().ObtenerControlStockXIDPP(IDPP, dniCDR);

            if (ControlStock == true)
            {
                int cantProductosCarrito = (from c in productosCarrito where c.IdProdPeruShop.Trim() == IDPS.Trim() select c.Cantidad).FirstOrDefault();
                int cantidadExistente = StockLN.getInstance().CantidadStockCDR(dniCDR, IDPS, "01", IDPP);
                cantidadExisteM = cantidadExistente.ToString();
                if ((cantidad + cantProductosCarrito) <= cantidadExistente) { stock = true; }
            }
            else { stock = true; }

            retornoStock = new ProductoV2.AgregarCarrito() { CantidadExistente = cantidadExisteM, Estado = stock };

            return retornoStock;
        }

        private static double ObtenerSubTotalNeto(string tipoCompraSeleccionado, string packeteSocio, double subTotal, string tipCliente, string lineaDelProducto)
        {
            string patrocinadorPackete = Convert.ToString(System.Web.HttpContext.Current.Session["PatrocinadorPackete"]);
            List<Packete> listaPacketes = PacketeNegocios.getInstance().ListarPackete("", "");

            string consultorDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Consultor" select d.Descuento).SingleOrDefault();
            string patrocinaPacketeDescuento = (from d in listaPacketes where d.Codigo == patrocinadorPackete select d.Descuento).SingleOrDefault();
            string socioPacketeDescuento = (from d in listaPacketes where d.Codigo == packeteSocio select d.Descuento).SingleOrDefault();

            string emprendedorDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Básico" select d.Descuento).SingleOrDefault();
            string profesionalDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Profesional" select d.Descuento).SingleOrDefault();
            string empresarialDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Empresarial" select d.Descuento).SingleOrDefault();
            string millonarioDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Millonario" select d.Descuento).SingleOrDefault();
            string imperialDescuento = (from d in listaPacketes where d._Packete == "Afiliación: Imperial" select d.Descuento).SingleOrDefault();

            double subTotalNeto = 0.0;
            if (tipoCompraSeleccionado == "07") //Tipo consumo
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //Linea general
                {
                    if (tipCliente == "03") //C. Inteligente
                    {
                        double descuento = Convert.ToDouble(patrocinaPacketeDescuento, CultureInfo.InvariantCulture);
                        System.Web.HttpContext.Current.Session["descuentoCI"] = descuento;
                        subTotalNeto = subTotal * (1 - (descuento / 2));
                    }
                    else if (tipCliente == "05") //Consultor
                    {
                        double descuento = Convert.ToDouble(consultorDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuento);
                    }
                    else //Socio
                    {
                        subTotalNeto = ObtenerSubTotalNetoLineaGeneral(packeteSocio, subTotal, tipoCompraSeleccionado);
                    }
                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else //Consumo saludable
                {
                    if (tipCliente == "03") //C. Inteligente
                    {
                        subTotalNeto = subTotal * (1 - 0.075);
                    }
                    else if (tipCliente == "05") //Consultor
                    {
                        subTotalNeto = subTotal * (1 - 0.1);
                    }
                    else
                    {
                        subTotalNeto = subTotal * (1 - 0.15);
                    }
                }
            } // AFILIACION
            else if (tipoCompraSeleccionado == "01" || tipoCompraSeleccionado == "02" || tipoCompraSeleccionado == "03" || tipoCompraSeleccionado == "04" || tipoCompraSeleccionado == "05" || tipoCompraSeleccionado == "06" || tipoCompraSeleccionado == "23")
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //General
                {
                    if (tipoCompraSeleccionado == "01") // Afi: Emprendedor
                    {
                        double descuentoEmprend = Convert.ToDouble(emprendedorDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoEmprend);
                    }
                    else if (tipoCompraSeleccionado == "02") // Afi: Profesional
                    {
                        double descuentoProfesio = Convert.ToDouble(profesionalDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoProfesio);
                    }
                    else if (tipoCompraSeleccionado == "03") // Afi: Empresarial
                    {
                        double descuentoEmpresa = Convert.ToDouble(empresarialDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoEmpresa);
                    }
                    else if (tipoCompraSeleccionado == "04") // Afi: Millonario
                    {
                        double descuentoMillonar = Convert.ToDouble(millonarioDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuentoMillonar);
                    }
                    else if (tipoCompraSeleccionado == "05") //Afi: Consultor
                    {
                        double descuento = Convert.ToDouble(consultorDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuento);
                    }
                    else if (tipoCompraSeleccionado == "23") //Afi: Imperial
                    {
                        double descuento = Convert.ToDouble(imperialDescuento, CultureInfo.InvariantCulture);
                        subTotalNeto = subTotal * (1 - descuento);
                    }
                    else //Afi: C. Inteligente
                    {
                        double descuento = Convert.ToDouble(socioPacketeDescuento, CultureInfo.InvariantCulture);
                        System.Web.HttpContext.Current.Session["descuentoCI"] = descuento;
                        subTotalNeto = subTotal * (1 - (descuento / 2));
                    }

                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else //Consumo saludable
                {
                    if (tipoCompraSeleccionado == "06") //C. Inteligente
                    {
                        subTotalNeto = subTotal * (1 - 0.075);
                    }
                    else if (tipoCompraSeleccionado == "05") //Consultor
                    {
                        subTotalNeto = subTotal * (1 - 0.1);
                    }
                    else
                    {
                        subTotalNeto = subTotal * (1 - 0.15);
                    }
                }
            }//UPGRADE
            else if (tipoCompraSeleccionado == "08" || tipoCompraSeleccionado == "09" || tipoCompraSeleccionado == "10" || tipoCompraSeleccionado == "11" || tipoCompraSeleccionado == "12" || tipoCompraSeleccionado == "13" || tipoCompraSeleccionado == "24" ||
                     tipoCompraSeleccionado == "25" || tipoCompraSeleccionado == "26" || tipoCompraSeleccionado == "27")
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //Linea general
                {
                    subTotalNeto = ObtenerSubTotalNetoLineaGeneral(packeteSocio, subTotal, tipoCompraSeleccionado); //Socio
                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else
                {
                    subTotalNeto = subTotal * (1 - 0.15);
                }
            }//MIGRACIÓN
            else
            {
                if (lineaDelProducto == "02" | lineaDelProducto == "06") //Linea general
                {
                    subTotalNeto = ObtenerSubTotalNetoLineaGeneral(packeteSocio, subTotal, tipoCompraSeleccionado); //Socio
                }
                else if (lineaDelProducto == "03" | lineaDelProducto == "05") //Herramienta
                {
                    subTotalNeto = subTotal;
                }
                else
                {
                    subTotalNeto = subTotal * (1 - 0.15);
                }
            }
            return subTotalNeto;
        }

        private static double ObtenerSubTotalNetoLineaGeneral(string packeteSocio, double subTotal, string tipoCompraSeleccionado)
        {
            string tipoCiente = Convert.ToString(System.Web.HttpContext.Current.Session["TipoCliente"]);
            List<Packete> listaPacketes = PacketeNegocios.getInstance().ListarPackete(packeteSocio, tipoCiente);
            double subTotalNeto = 0.0;

            //CONSUMO
            if (tipoCompraSeleccionado == "07")
            {
                foreach (var packete in listaPacketes)
                {
                    if (packeteSocio == packete.Codigo)
                    {
                        double descuento = Convert.ToDouble(packete.Descuento, CultureInfo.InvariantCulture);
                        return subTotalNeto = subTotal * (1 - descuento);
                    }
                }
            } //UPGRADE
            else if (tipoCompraSeleccionado == "08" || tipoCompraSeleccionado == "09" || tipoCompraSeleccionado == "10" || tipoCompraSeleccionado == "11" || tipoCompraSeleccionado == "12" || tipoCompraSeleccionado == "13" ||
                     tipoCompraSeleccionado == "24" || tipoCompraSeleccionado == "25" || tipoCompraSeleccionado == "26" || tipoCompraSeleccionado == "27")
            {
                foreach (var packete in listaPacketes)
                {
                    if (tipoCompraSeleccionado == packete.Codigo)
                    {
                        double descuento = Convert.ToDouble(packete.Descuento, CultureInfo.InvariantCulture);
                        return subTotalNeto = subTotal * (1 - descuento);
                    }
                }
            } //MIGRACIÓN
            else
            {
                foreach (var packete in listaPacketes)
                {
                    if (tipoCompraSeleccionado == packete.Codigo)
                    {
                        double descuento = Convert.ToDouble(packete.Descuento, CultureInfo.InvariantCulture);
                        return subTotalNeto = subTotal * (1 - descuento);
                    }
                }
            }
            return subTotalNeto;
        }
    }
}