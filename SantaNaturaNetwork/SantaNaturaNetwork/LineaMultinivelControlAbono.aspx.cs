﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;

namespace SantaNaturaNetworkV3
{
    public partial class LineaMultinivelControlAbono : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaLineaMultinivelPatrocinio(int periodoS, string idClienteS, string variableS)
        {
            List<PuntosComisiones> Lista = null;
            try
            {
                Lista = (variableS == "upline") ? PuntosNegocios.getInstance().ListaLineaMultinivelUpline(periodoS, idClienteS) : PuntosNegocios.getInstance().ListaLineaMultinivelPatrocinio(periodoS, idClienteS);
            }
            catch (Exception ex)
            {
                Lista = null;
                throw ex;
            }
            return Lista;
        }
    }
}