﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;

namespace SantaNaturaNetwork
{
    public partial class GenerarPacketeProducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static bool RegistrarPaquete(string nombrePa, string observacionPa, string paisPa, string paisBoliviaPa, string paisEcuadorPa,
                                             string paisPanamaPa, string paisCRPa, string paisArgentinaPa, string paisChilePa,
                                             string estadoPeruPa, string estadoBoliviaPa, string estadoEcuadorPa, string estadoPanamaPa,
                                             string estadoCRPa, string estadoArgentinaPa, string estadoChilePa, string precioPaPeru1,
                                             string precioPaPeru2, string precioPaPeru3, string precioPaPeru4, string precioPaPeru5,
                                             string precioPaBolivia1, string precioPaBolivia2, string precioPaBolivia3, string precioPaBolivia4,
                                             string precioPaBolivia5, string precioPaEcuador1, string precioPaEcuador2, string precioPaEcuador3,
                                             string precioPaEcuador4, string precioPaEcuador5, string precioPaPanama1, string precioPaPanama2,
                                             string precioPaPanama3, string precioPaPanama4, string precioPaPanama5, string precioPaCR1,
                                             string precioPaCR2, string precioPaCR3, string precioPaCR4, string precioPaCR5,
                                             string precioPaArgentina1, string precioPaArgentina2, string precioPaArgentina3,
                                             string precioPaArgentina4, string precioPaArgentina5, string precioPaChile1,
                                             string precioPaChile2, string precioPaChile3, string precioPaChile4, string precioPaChile5,
                                             string idPeruPa1, string idPeruPa2, string idPeruPa3, string idPeruPa4, string idPeruPa5,
                                             string idBoliviaPa1, string idBoliviaPa2, string idBoliviaPa3, string idBoliviaPa4,
                                             string idBoliviaPa5, string idEcuadorPa1, string idEcuadorPa2, string idEcuadorPa3,
                                             string idEcuadorPa4, string idEcuadorPa5, string idPanamaPa1, string idPanamaPa2,
                                             string idPanamaPa3, string idPanamaPa4, string idPanamaPa5, string idCRPa1, string idCRPa2,
                                             string idCRPa3, string idCRPa4, string idCRPa5, string idArgentinaPa1, string idArgentinaPa2,
                                             string idArgentinaPa3, string idArgentinaPa4, string idArgentinaPa5, string idChilePa1,
                                             string idChilePa2, string idChilePa3, string idChilePa4, string idChilePa5)
        {
            #region LINQ NOMBREXIDO
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaIdopXNombre();
            string nombrePeru1 = idPeruPa1;
            string nombrePeru2 = idPeruPa2;
            string nombrePeru3 = idPeruPa3;
            string nombrePeru4 = idPeruPa4;
            string nombrePeru5 = idPeruPa5;
            string nombreBol1 = idBoliviaPa1;
            string nombreBol2 = idBoliviaPa2;
            string nombreBol3 = idBoliviaPa3;
            string nombreBol4 = idBoliviaPa4;
            string nombreBol5 = idBoliviaPa5;
            string nombreEcu1 = idEcuadorPa1;
            string nombreEcu2 = idEcuadorPa2;
            string nombreEcu3 = idEcuadorPa3;
            string nombreEcu4 = idEcuadorPa4;
            string nombreEcu5 = idEcuadorPa5;
            string nombrePana1 = idPanamaPa1;
            string nombrePana2 = idPanamaPa2;
            string nombrePana3 = idPanamaPa3;
            string nombrePana4 = idPanamaPa4;
            string nombrePana5 = idPanamaPa5;
            string nombreCR1 = idCRPa1;
            string nombreCR2 = idCRPa2;
            string nombreCR3 = idCRPa3;
            string nombreCR4 = idCRPa4;
            string nombreCR5 = idCRPa5;
            string nombreArg1 = idArgentinaPa1;
            string nombreArg2 = idArgentinaPa2;
            string nombreArg3 = idArgentinaPa3;
            string nombreArg4 = idArgentinaPa4;
            string nombreArg5 = idArgentinaPa5;
            string nombreChi1 = idChilePa1;
            string nombreChi2 = idChilePa2;
            string nombreChi3 = idChilePa3;
            string nombreChi4 = idChilePa4;
            string nombreChi5 = idChilePa5;
            idPeruPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCRPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idCRPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCRPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idCRPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCRPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idCRPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCRPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idCRPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCRPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idCRPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentinaPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentinaPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentinaPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentinaPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentinaPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentinaPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentinaPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentinaPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentinaPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentinaPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChilePa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idChilePa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChilePa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idChilePa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChilePa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idChilePa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChilePa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idChilePa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChilePa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idChilePa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            #endregion

            #region CONDICIONES NULL
            if (idPeruPa1 == null)
            {
                idPeruPa1 = "";
            }
            if (idPeruPa2 == null)
            {
                idPeruPa2 = "";
            }
            if (idPeruPa3 == null)
            {
                idPeruPa3 = "";
            }
            if (idPeruPa4 == null)
            {
                idPeruPa4 = "";
            }
            if (idPeruPa5 == null)
            {
                idPeruPa5 = "";
            }
            if (idBoliviaPa1 == null)
            {
                idBoliviaPa1 = "";
            }
            if (idBoliviaPa2 == null)
            {
                idBoliviaPa2 = "";
            }
            if (idBoliviaPa3 == null)
            {
                idBoliviaPa3 = "";
            }
            if (idBoliviaPa4 == null)
            {
                idBoliviaPa4 = "";
            }
            if (idBoliviaPa5 == null)
            {
                idBoliviaPa5 = "";
            }
            if (idEcuadorPa1 == null)
            {
                idEcuadorPa1 = "";
            }
            if (idEcuadorPa2 == null)
            {
                idEcuadorPa2 = "";
            }
            if (idEcuadorPa3 == null)
            {
                idEcuadorPa3 = "";
            }
            if (idEcuadorPa4 == null)
            {
                idEcuadorPa4 = "";
            }
            if (idEcuadorPa5 == null)
            {
                idEcuadorPa5 = "";
            }
            if (idPanamaPa1 == null)
            {
                idPanamaPa1 = "";
            }
            if (idPanamaPa2 == null)
            {
                idPanamaPa2 = "";
            }
            if (idPanamaPa3 == null)
            {
                idPanamaPa3 = "";
            }
            if (idPanamaPa4 == null)
            {
                idPanamaPa4 = "";
            }
            if (idPanamaPa5 == null)
            {
                idPanamaPa5 = "";
            }
            if (idCRPa1 == null)
            {
                idCRPa1 = "";
            }
            if (idCRPa2 == null)
            {
                idCRPa2 = "";
            }
            if (idCRPa3 == null)
            {
                idCRPa3 = "";
            }
            if (idCRPa4 == null)
            {
                idCRPa4 = "";
            }
            if (idCRPa5 == null)
            {
                idCRPa5 = "";
            }
            if (idArgentinaPa1 == null)
            {
                idArgentinaPa1 = "";
            }
            if (idArgentinaPa2 == null)
            {
                idArgentinaPa2 = "";
            }
            if (idArgentinaPa3 == null)
            {
                idArgentinaPa3 = "";
            }
            if (idArgentinaPa4 == null)
            {
                idArgentinaPa4 = "";
            }
            if (idArgentinaPa5 == null)
            {
                idArgentinaPa5 = "";
            }
            if (idChilePa1 == null)
            {
                idChilePa1 = "";
            }
            if (idChilePa2 == null)
            {
                idChilePa2 = "";
            }
            if (idChilePa3 == null)
            {
                idChilePa3 = "";
            }
            if (idChilePa4 == null)
            {
                idChilePa4 = "";
            }
            if (idChilePa5 == null)
            {
                idChilePa5 = "";
            }

            #endregion


            PaqueteNatura objPaquete = new PaqueteNatura()
            {
                nombre = nombrePa,
                observacion = observacionPa
            };

            PaquetePais objPaquetePais = new PaquetePais()
            {
                paisPP = paisPa,
                paisPBolivia = paisBoliviaPa,
                paisPEcuador = paisEcuadorPa,
                paisPPanama = paisPanamaPa,
                paisPCR = paisCRPa,
                paisPArgentina = paisArgentinaPa,
                paisPChile = paisChilePa,
                estadoPP = Convert.ToBoolean(estadoPeruPa),
                estadoBolivia = Convert.ToBoolean(estadoBoliviaPa),
                estadoEcuador = Convert.ToBoolean(estadoEcuadorPa),
                estadoPanama = Convert.ToBoolean(estadoPanamaPa),
                estadoCR = Convert.ToBoolean(estadoCRPa),
                estadoArgentina = Convert.ToBoolean(estadoArgentinaPa),
                estadoChile = Convert.ToBoolean(estadoChilePa),
                cantidad1PP = Convert.ToDouble(precioPaPeru1),
                cantidad2PP = Convert.ToDouble(precioPaPeru2),
                cantidad3PP = Convert.ToDouble(precioPaPeru3),
                cantidad4PP = Convert.ToDouble(precioPaPeru4),
                cantidad5PP = Convert.ToDouble(precioPaPeru5),
                cantidad1Bolivia = Convert.ToDouble(precioPaBolivia1),
                cantidad2Bolivia = Convert.ToDouble(precioPaBolivia2),
                cantidad3Bolivia = Convert.ToDouble(precioPaBolivia3),
                cantidad4Bolivia = Convert.ToDouble(precioPaBolivia4),
                cantidad5Bolivia = Convert.ToDouble(precioPaBolivia5),
                cantidad1Ecuador = Convert.ToDouble(precioPaEcuador1),
                cantidad2Ecuador = Convert.ToDouble(precioPaEcuador2),
                cantidad3Ecuador = Convert.ToDouble(precioPaEcuador3),
                cantidad4Ecuador = Convert.ToDouble(precioPaEcuador4),
                cantidad5Ecuador = Convert.ToDouble(precioPaEcuador5),
                cantidad1Panama = Convert.ToDouble(precioPaPanama1),
                cantidad2Panama = Convert.ToDouble(precioPaPanama2),
                cantidad3Panama = Convert.ToDouble(precioPaPanama3),
                cantidad4Panama = Convert.ToDouble(precioPaPanama4),
                cantidad5Panama = Convert.ToDouble(precioPaPanama5),
                cantidad1CR = Convert.ToDouble(precioPaCR1),
                cantidad2CR = Convert.ToDouble(precioPaCR2),
                cantidad3CR = Convert.ToDouble(precioPaCR3),
                cantidad4CR = Convert.ToDouble(precioPaCR4),
                cantidad5CR = Convert.ToDouble(precioPaCR5),
                cantidad1Argentina = Convert.ToDouble(precioPaArgentina1),
                cantidad2Argentina = Convert.ToDouble(precioPaArgentina2),
                cantidad3Argentina = Convert.ToDouble(precioPaArgentina3),
                cantidad4Argentina = Convert.ToDouble(precioPaArgentina4),
                cantidad5Argentina = Convert.ToDouble(precioPaArgentina5),
                cantidad1Chile = Convert.ToDouble(precioPaChile1),
                cantidad2Chile = Convert.ToDouble(precioPaChile2),
                cantidad3Chile = Convert.ToDouble(precioPaChile3),
                cantidad4Chile = Convert.ToDouble(precioPaChile4),
                cantidad5Chile = Convert.ToDouble(precioPaChile5),
                idProductoPais1PP = idPeruPa1,
                idProductoPais2PP = idPeruPa2,
                idProductoPais3PP = idPeruPa3,
                idProductoPais4PP = idPeruPa4,
                idProductoPais5PP = idPeruPa5,
                idProductoPais1Bolivia = idBoliviaPa1,
                idProductoPais2Bolivia = idBoliviaPa2,
                idProductoPais3Bolivia = idBoliviaPa3,
                idProductoPais4Bolivia = idBoliviaPa4,
                idProductoPais5Bolivia = idBoliviaPa5,
                idProductoPais1Ecuador = idEcuadorPa1,
                idProductoPais2Ecuador = idEcuadorPa2,
                idProductoPais3Ecuador = idEcuadorPa3,
                idProductoPais4Ecuador = idEcuadorPa4,
                idProductoPais5Ecuador = idEcuadorPa5,
                idProductoPais1Panama = idPanamaPa1,
                idProductoPais2Panama = idPanamaPa2,
                idProductoPais3Panama = idPanamaPa3,
                idProductoPais4Panama = idPanamaPa4,
                idProductoPais5Panama = idPanamaPa5,
                idProductoPais1CR = idCRPa1,
                idProductoPais2CR = idCRPa2,
                idProductoPais3CR = idCRPa3,
                idProductoPais4CR = idCRPa4,
                idProductoPais5CR = idCRPa5,
                idProductoPais1Argentina = idArgentinaPa1,
                idProductoPais2Argentina = idArgentinaPa2,
                idProductoPais3Argentina = idArgentinaPa3,
                idProductoPais4Argentina = idArgentinaPa4,
                idProductoPais5Argentina = idArgentinaPa5,
                idProductoPais1Chile = idChilePa1,
                idProductoPais2Chile = idChilePa2,
                idProductoPais3Chile = idChilePa3,
                idProductoPais4Chile = idChilePa4,
                idProductoPais5Chile = idChilePa5,
                nombre1Peru = nombrePeru1,
                nombre2Peru = nombrePeru2,
                nombre3Peru = nombrePeru3,
                nombre4Peru = nombrePeru4,
                nombre5Peru = nombrePeru5,
                nombre1Bolivia = nombreBol1,
                nombre2Bolivia = nombreBol2,
                nombre3Bolivia = nombreBol3,
                nombre4Bolivia = nombreBol4,
                nombre5Bolivia = nombreBol5,
                nombre1Ecuador = nombreEcu1,
                nombre2Ecuador = nombreEcu2,
                nombre3Ecuador = nombreEcu3,
                nombre4Ecuador = nombreEcu4,
                nombre5Ecuador = nombreEcu5,
                nombre1Panama = nombrePana1,
                nombre2Panama = nombrePana2,
                nombre3Panama = nombrePana3,
                nombre4Panama = nombrePana4,
                nombre5Panama = nombrePana5,
                nombre1CR = nombreCR1,
                nombre2CR = nombreCR2,
                nombre3CR = nombreCR3,
                nombre4CR = nombreCR4,
                nombre5CR = nombreCR5,
                nombre1Argentina = nombreArg1,
                nombre2Argentina = nombreArg2,
                nombre3Argentina = nombreArg3,
                nombre4Argentina = nombreArg4,
                nombre5Argentina = nombreArg5,
                nombre1Chile = nombreChi1,
                nombre2Chile = nombreChi2,
                nombre3Chile = nombreChi3,
                nombre4Chile = nombreChi4,
                nombre5Chile = nombreChi5
            };

            bool ok = PaqueteLN.getInstance().RegistroPaquete(objPaquete, objPaquetePais);

            return true;
        }

        [WebMethod]
        public static bool ActualizarPaquete(string idPaquete, string idPaquetePaisPeru, string idPaquetePaisBolivia,
                                              string idPaquetePaisEcuador, string idPaquetePaisPanama, string idPaquetePaisCR,
                                              string idPaquetePaisArgentina, string idPaquetePaisChile, string nombrePa,
                                              string observacionPa, string estadoPeruPa, string estadoBoliviaPa,
                                              string estadoEcuadorPa, string estadoPanamaPa, string estadoCRPa,
                                              string estadoArgentinaPa, string estadoChilePa, string precioPaPeru1,
                                              string precioPaPeru2, string precioPaPeru3, string precioPaPeru4,
                                              string precioPaPeru5, string precioPaBolivia1, string precioPaBolivia2,
                                              string precioPaBolivia3, string precioPaBolivia4, string precioPaBolivia5,
                                              string precioPaEcuador1, string precioPaEcuador2, string precioPaEcuador3,
                                              string precioPaEcuador4, string precioPaEcuador5, string precioPaPanama1,
                                              string precioPaPanama2, string precioPaPanama3, string precioPaPanama4,
                                              string precioPaPanama5, string precioPaCR1, string precioPaCR2,
                                              string precioPaCR3, string precioPaCR4, string precioPaCR5,
                                              string precioPaArgentina1, string precioPaArgentina2, string precioPaArgentina3,
                                              string precioPaArgentina4, string precioPaArgentina5, string precioPaChile1,
                                              string precioPaChile2, string precioPaChile3, string precioPaChile4,
                                              string precioPaChile5, string idPeruPa1, string idPeruPa2, string idPeruPa3,
                                              string idPeruPa4, string idPeruPa5, string idBoliviaPa1, string idBoliviaPa2,
                                              string idBoliviaPa3, string idBoliviaPa4, string idBoliviaPa5, string idEcuadorPa1,
                                              string idEcuadorPa2, string idEcuadorPa3, string idEcuadorPa4, string idEcuadorPa5,
                                              string idPanamaPa1, string idPanamaPa2, string idPanamaPa3, string idPanamaPa4,
                                              string idPanamaPa5, string idCR1, string idCR2, string idCR3, string idCR4,
                                              string idCR5, string idArgentina1, string idArgentina2, string idArgentina3,
                                              string idArgentina4, string idArgentina5, string idChile1, string idChile2,
                                              string idChile3, string idChile4, string idChile5)
        {

            #region LINQ NOMBREXIDO
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaIdopXNombre();
            string nombrePeru1 = idPeruPa1;
            string nombrePeru2 = idPeruPa2;
            string nombrePeru3 = idPeruPa3;
            string nombrePeru4 = idPeruPa4;
            string nombrePeru5 = idPeruPa5;
            string nombreBol1 = idBoliviaPa1;
            string nombreBol2 = idBoliviaPa2;
            string nombreBol3 = idBoliviaPa3;
            string nombreBol4 = idBoliviaPa4;
            string nombreBol5 = idBoliviaPa5;
            string nombreEcu1 = idEcuadorPa1;
            string nombreEcu2 = idEcuadorPa2;
            string nombreEcu3 = idEcuadorPa3;
            string nombreEcu4 = idEcuadorPa4;
            string nombreEcu5 = idEcuadorPa5;
            string nombrePana1 = idPanamaPa1;
            string nombrePana2 = idPanamaPa2;
            string nombrePana3 = idPanamaPa3;
            string nombrePana4 = idPanamaPa4;
            string nombrePana5 = idPanamaPa5;
            string nombreCR1 = idCR1;
            string nombreCR2 = idCR2;
            string nombreCR3 = idCR3;
            string nombreCR4 = idCR4;
            string nombreCR5 = idCR5;
            string nombreArg1 = idArgentina1;
            string nombreArg2 = idArgentina2;
            string nombreArg3 = idArgentina3;
            string nombreArg4 = idArgentina4;
            string nombreArg5 = idArgentina5;
            string nombreChi1 = idChile1;
            string nombreChi2 = idChile2;
            string nombreChi3 = idChile3;
            string nombreChi4 = idChile4;
            string nombreChi5 = idChile5;
            idPeruPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPeruPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idPeruPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idBoliviaPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idBoliviaPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idEcuadorPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idEcuadorPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa1 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa2 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa3 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa4 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idPanamaPa5 = (from c in ListaProducto where c.NombreProducto.Trim() == idPanamaPa5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCR1 = (from c in ListaProducto where c.NombreProducto.Trim() == idCR1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCR2 = (from c in ListaProducto where c.NombreProducto.Trim() == idCR2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCR3 = (from c in ListaProducto where c.NombreProducto.Trim() == idCR3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCR4 = (from c in ListaProducto where c.NombreProducto.Trim() == idCR4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idCR5 = (from c in ListaProducto where c.NombreProducto.Trim() == idCR5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentina1 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentina1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentina2 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentina2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentina3 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentina3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentina4 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentina4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idArgentina5 = (from c in ListaProducto where c.NombreProducto.Trim() == idArgentina5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChile1 = (from c in ListaProducto where c.NombreProducto.Trim() == idChile1.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChile2 = (from c in ListaProducto where c.NombreProducto.Trim() == idChile2.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChile3 = (from c in ListaProducto where c.NombreProducto.Trim() == idChile3.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChile4 = (from c in ListaProducto where c.NombreProducto.Trim() == idChile4.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            idChile5 = (from c in ListaProducto where c.NombreProducto.Trim() == idChile5.Trim() select c.IdProducto.Trim()).FirstOrDefault();
            #endregion

            #region CONDICIONES NULL
            if (idPeruPa1 == null)
            {
                idPeruPa1 = "";
            }
            if (idPeruPa2 == null)
            {
                idPeruPa2 = "";
            }
            if (idPeruPa3 == null)
            {
                idPeruPa3 = "";
            }
            if (idPeruPa4 == null)
            {
                idPeruPa4 = "";
            }
            if (idPeruPa5 == null)
            {
                idPeruPa5 = "";
            }
            if (idBoliviaPa1 == null)
            {
                idBoliviaPa1 = "";
            }
            if (idBoliviaPa2 == null)
            {
                idBoliviaPa2 = "";
            }
            if (idBoliviaPa3 == null)
            {
                idBoliviaPa3 = "";
            }
            if (idBoliviaPa4 == null)
            {
                idBoliviaPa4 = "";
            }
            if (idBoliviaPa5 == null)
            {
                idBoliviaPa5 = "";
            }
            if (idEcuadorPa1 == null)
            {
                idEcuadorPa1 = "";
            }
            if (idEcuadorPa2 == null)
            {
                idEcuadorPa2 = "";
            }
            if (idEcuadorPa3 == null)
            {
                idEcuadorPa3 = "";
            }
            if (idEcuadorPa4 == null)
            {
                idEcuadorPa4 = "";
            }
            if (idEcuadorPa5 == null)
            {
                idEcuadorPa5 = "";
            }
            if (idPanamaPa1 == null)
            {
                idPanamaPa1 = "";
            }
            if (idPanamaPa2 == null)
            {
                idPanamaPa2 = "";
            }
            if (idPanamaPa3 == null)
            {
                idPanamaPa3 = "";
            }
            if (idPanamaPa4 == null)
            {
                idPanamaPa4 = "";
            }
            if (idPanamaPa5 == null)
            {
                idPanamaPa5 = "";
            }
            if (idCR1 == null)
            {
                idCR1 = "";
            }
            if (idCR2 == null)
            {
                idCR2 = "";
            }
            if (idCR3 == null)
            {
                idCR3 = "";
            }
            if (idCR4 == null)
            {
                idCR4 = "";
            }
            if (idCR5 == null)
            {
                idCR5 = "";
            }
            if (idArgentina1 == null)
            {
                idArgentina1 = "";
            }
            if (idArgentina2 == null)
            {
                idArgentina2 = "";
            }
            if (idArgentina3 == null)
            {
                idArgentina3 = "";
            }
            if (idArgentina4 == null)
            {
                idArgentina4 = "";
            }
            if (idArgentina5 == null)
            {
                idArgentina5 = "";
            }
            if (idChile1 == null)
            {
                idChile1 = "";
            }
            if (idChile2 == null)
            {
                idChile2 = "";
            }
            if (idChile3 == null)
            {
                idChile3 = "";
            }
            if (idChile4 == null)
            {
                idChile4 = "";
            }
            if (idChile5 == null)
            {
                idChile5 = "";
            }

            #endregion

            PaqueteNatura objPaquete = new PaqueteNatura()
            {
                idPaquete = idPaquete,
                nombre = nombrePa,
                observacion = observacionPa
            };

            PaquetePais objPaquetePais = new PaquetePais()
            {
                idPaquetePais = idPaquetePaisPeru,
                idPaquetePaisBolivia = idPaquetePaisBolivia,
                idPaquetePaisEcuador = idPaquetePaisEcuador,
                idPaquetePaisPanama = idPaquetePaisPanama,
                idPaquetePaisCR = idPaquetePaisCR,
                idPaquetePaisArgentina = idPaquetePaisArgentina,
                idPaquetePaisChile = idPaquetePaisChile,
                estadoPP = Convert.ToBoolean(estadoPeruPa),
                estadoBolivia = Convert.ToBoolean(estadoBoliviaPa),
                estadoEcuador = Convert.ToBoolean(estadoEcuadorPa),
                estadoPanama = Convert.ToBoolean(estadoPanamaPa),
                estadoCR = Convert.ToBoolean(estadoCRPa),
                estadoArgentina = Convert.ToBoolean(estadoArgentinaPa),
                estadoChile = Convert.ToBoolean(estadoChilePa),
                cantidad1PP = Convert.ToDouble(precioPaPeru1),
                cantidad2PP = Convert.ToDouble(precioPaPeru2),
                cantidad3PP = Convert.ToDouble(precioPaPeru3),
                cantidad4PP = Convert.ToDouble(precioPaPeru4),
                cantidad5PP = Convert.ToDouble(precioPaPeru5),
                cantidad1Bolivia = Convert.ToDouble(precioPaBolivia1),
                cantidad2Bolivia = Convert.ToDouble(precioPaBolivia2),
                cantidad3Bolivia = Convert.ToDouble(precioPaBolivia3),
                cantidad4Bolivia = Convert.ToDouble(precioPaBolivia4),
                cantidad5Bolivia = Convert.ToDouble(precioPaBolivia5),
                cantidad1Ecuador = Convert.ToDouble(precioPaEcuador1),
                cantidad2Ecuador = Convert.ToDouble(precioPaEcuador2),
                cantidad3Ecuador = Convert.ToDouble(precioPaEcuador3),
                cantidad4Ecuador = Convert.ToDouble(precioPaEcuador4),
                cantidad5Ecuador = Convert.ToDouble(precioPaEcuador5),
                cantidad1Panama = Convert.ToDouble(precioPaPanama1),
                cantidad2Panama = Convert.ToDouble(precioPaPanama2),
                cantidad3Panama = Convert.ToDouble(precioPaPanama3),
                cantidad4Panama = Convert.ToDouble(precioPaPanama4),
                cantidad5Panama = Convert.ToDouble(precioPaPanama5),
                cantidad1CR = Convert.ToDouble(precioPaCR1),
                cantidad2CR = Convert.ToDouble(precioPaCR2),
                cantidad3CR = Convert.ToDouble(precioPaCR3),
                cantidad4CR = Convert.ToDouble(precioPaCR4),
                cantidad5CR = Convert.ToDouble(precioPaCR5),
                cantidad1Argentina = Convert.ToDouble(precioPaArgentina1),
                cantidad2Argentina = Convert.ToDouble(precioPaArgentina2),
                cantidad3Argentina = Convert.ToDouble(precioPaArgentina3),
                cantidad4Argentina = Convert.ToDouble(precioPaArgentina4),
                cantidad5Argentina = Convert.ToDouble(precioPaArgentina5),
                cantidad1Chile = Convert.ToDouble(precioPaChile1),
                cantidad2Chile = Convert.ToDouble(precioPaChile2),
                cantidad3Chile = Convert.ToDouble(precioPaChile3),
                cantidad4Chile = Convert.ToDouble(precioPaChile4),
                cantidad5Chile = Convert.ToDouble(precioPaChile5),
                idProductoPais1PP = idPeruPa1,
                idProductoPais2PP = idPeruPa2,
                idProductoPais3PP = idPeruPa3,
                idProductoPais4PP = idPeruPa4,
                idProductoPais5PP = idPeruPa5,
                idProductoPais1Bolivia = idBoliviaPa1,
                idProductoPais2Bolivia = idBoliviaPa2,
                idProductoPais3Bolivia = idBoliviaPa3,
                idProductoPais4Bolivia = idBoliviaPa4,
                idProductoPais5Bolivia = idBoliviaPa5,
                idProductoPais1Ecuador = idEcuadorPa1,
                idProductoPais2Ecuador = idEcuadorPa2,
                idProductoPais3Ecuador = idEcuadorPa3,
                idProductoPais4Ecuador = idEcuadorPa4,
                idProductoPais5Ecuador = idEcuadorPa5,
                idProductoPais1Panama = idPanamaPa1,
                idProductoPais2Panama = idPanamaPa2,
                idProductoPais3Panama = idPanamaPa3,
                idProductoPais4Panama = idPanamaPa4,
                idProductoPais5Panama = idPanamaPa5,
                idProductoPais1CR = idCR1,
                idProductoPais2CR = idCR2,
                idProductoPais3CR = idCR3,
                idProductoPais4CR = idCR4,
                idProductoPais5CR = idCR5,
                idProductoPais1Argentina = idArgentina1,
                idProductoPais2Argentina = idArgentina2,
                idProductoPais3Argentina = idArgentina3,
                idProductoPais4Argentina = idArgentina4,
                idProductoPais5Argentina = idArgentina5,
                idProductoPais1Chile = idChile1,
                idProductoPais2Chile = idChile2,
                idProductoPais3Chile = idChile3,
                idProductoPais4Chile = idChile4,
                idProductoPais5Chile = idChile5,
                nombre1Peru = nombrePeru1,
                nombre2Peru = nombrePeru2,
                nombre3Peru = nombrePeru3,
                nombre4Peru = nombrePeru4,
                nombre5Peru = nombrePeru5,
                nombre1Bolivia = nombreBol1,
                nombre2Bolivia = nombreBol2,
                nombre3Bolivia = nombreBol3,
                nombre4Bolivia = nombreBol4,
                nombre5Bolivia = nombreBol5,
                nombre1Ecuador = nombreEcu1,
                nombre2Ecuador = nombreEcu2,
                nombre3Ecuador = nombreEcu3,
                nombre4Ecuador = nombreEcu4,
                nombre5Ecuador = nombreEcu5,
                nombre1Panama = nombrePana1,
                nombre2Panama = nombrePana2,
                nombre3Panama = nombrePana3,
                nombre4Panama = nombrePana4,
                nombre5Panama = nombrePana5,
                nombre1CR = nombreCR1,
                nombre2CR = nombreCR2,
                nombre3CR = nombreCR3,
                nombre4CR = nombreCR4,
                nombre5CR = nombreCR5,
                nombre1Argentina = nombreArg1,
                nombre2Argentina = nombreArg2,
                nombre3Argentina = nombreArg3,
                nombre4Argentina = nombreArg4,
                nombre5Argentina = nombreArg5,
                nombre1Chile = nombreChi1,
                nombre2Chile = nombreChi2,
                nombre3Chile = nombreChi3,
                nombre4Chile = nombreChi4,
                nombre5Chile = nombreChi5
            };

            bool ok = PaqueteLN.getInstance().ActualizarPaquete(objPaquete, objPaquetePais);
            return true;
        }

        [WebMethod]
        public static List<PaqueteNatura> ListaPaquetes()
        {
            List<PaqueteNatura> Lista = PaqueteLN.getInstance().ListaPaquetes();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

    }
}