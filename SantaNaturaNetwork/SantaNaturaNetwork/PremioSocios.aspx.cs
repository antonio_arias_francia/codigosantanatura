﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class PremioSocios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["mostrarCompraTerminada"] = 0;
            if (!Page.IsPostBack)
            {
                if (Session["IdCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "" | Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaGanadoresPremio()
        {
            int IDPPuntos = Convert.ToInt32(PeriodoLN.getInstance().ListarCodigoPeriodoActivaComision());
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                Lista = PuntosNegocios.getInstance().ListaGanadoresPremio(documento.Trim(), IDPPuntos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaSociosCompresion()
        {
            int IDPPuntos = Convert.ToInt32(PeriodoLN.getInstance().ListarCodigoPeriodoActivaComision());
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                Lista = PuntosNegocios.getInstance().ListaSociosComprimir(documento.Trim(), IDPPuntos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaGanadoresPremioFiltro(int IDPPuntos)
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                Lista = PuntosNegocios.getInstance().ListaGanadoresPremio(documento.Trim(), IDPPuntos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }
    }
}