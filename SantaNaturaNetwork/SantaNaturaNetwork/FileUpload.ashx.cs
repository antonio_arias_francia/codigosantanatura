﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SantaNaturaNetwork
{
    /// <summary>
    /// Descripción breve de FileUpload1
    /// </summary>
    public class FileUpload1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string archivo = context.Request.Form["archivo"].ToString();
            
            string ruta = (archivo == "promo") ? context.Server.MapPath("~/promociones/") : 
                          (archivo == "premio") ? context.Server.MapPath("~/premios/") :
                          (archivo == "banner") ? context.Server.MapPath("~/banners/") :
                          (archivo == "documento") ? context.Server.MapPath("~/documentos/") :
                          (archivo == "factura") ? context.Server.MapPath("~/facturacion/") :
                          (archivo == "marketing") ? context.Server.MapPath("~/marketing/") :
                          context.Server.MapPath("~/uploads/");
            

            if (!Directory.Exists(ruta))
            {
                Directory.CreateDirectory(ruta);
            }

            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFile file = files[i];
                    string fname = (archivo == "promo") ? context.Server.MapPath("~/promociones/" + file.FileName) :
                                   (archivo == "premio") ? context.Server.MapPath("~/premios/" + file.FileName) :
                                   (archivo == "banner") ? context.Server.MapPath("~/banners/" + file.FileName) :
                                   (archivo == "documento") ? context.Server.MapPath("~/documentos/" + file.FileName) :
                                   (archivo == "factura") ? context.Server.MapPath("~/facturacion/" + file.FileName) :
                                   (archivo == "marketing") ? context.Server.MapPath("~/marketing/" + file.FileName) :
                                   context.Server.MapPath("~/uploads/" + file.FileName);
                    if (File.Exists(fname))
                    {
                        context.Response.ContentType = "text/plain";
                        context.Response.Write("El nombre de la imagen ya existe");
                        
                    }
                    else
                    {
                        file.SaveAs(fname);
                    }     
                }
                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}