﻿using System;
using Modelos;
using Negocios;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SantaNaturaNetworkV3
{
    public partial class CompraVisaNet : System.Web.UI.Page
    {
        public List<DatosPedidosVisaNet.ListProductos> productos = new List<DatosPedidosVisaNet.ListProductos>();
        public string nameProductos = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IdCliente"] == null)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "")
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "10")
            {
                Response.Redirect("Principal.aspx");
            }

            ListarPedidoVisaNet();
        }

        private void ListarPedidoVisaNet()
        {
            List<ProductoCarrito> productosCarrito = new List<ProductoCarrito>();
            string x = Convert.ToString(Session["Transaction_ID"]);
            if (Convert.ToString(Session["Transaction_ID"]) != "")
            {
                string idpago = Convert.ToString(Session["IdPagoCliente"]);
                nameProductos = "Productos Comprados:";
                List<DatosPedidosVisaNet> ListaDatosPedido = CompraNegocios.getInstance().ListarComprasxIdPago(idpago);
                txtDespacho.Text = ListaDatosPedido[0].Despacho;
                txtTelefono.Text = ListaDatosPedido[0].Telefono;
                txtDireccion.Text = ListaDatosPedido[0].Direccion;
                txtPedido.Text = ListaDatosPedido[0].IdPedido;
                txtTarjeta.Text = ListaDatosPedido[0].NumTarjeta;
                txtFechaHora.Text = ListaDatosPedido[0].FechaHora.Substring(0, 2) + "/" + ListaDatosPedido[0].FechaHora.Substring(2, 2) + "/" + ListaDatosPedido[0].FechaHora.Substring(4, 2) + " " + ListaDatosPedido[0].FechaHora.Substring(6, 2) + ":" + ListaDatosPedido[0].FechaHora.Substring(8, 2) + ":" + ListaDatosPedido[0].FechaHora.Substring(10, 2);
                txtImporte.Text = ListaDatosPedido[0].Importe;
                txtNombres.Text = ListaDatosPedido[0].TarjetaHabiente;
                txtCodigoAccion.Text = ListaDatosPedido[0].CodigoAccion;
                productos = ListaDatosPedido[0].Productos;
            }
            else
            {
                string fechaError = Convert.ToString(Session["Transaction_Date"]);
                nameProductos = "Productos No Comprados:";
                List<DatosPedidosVisaNet.ListProductos> productosAgregar = new List<DatosPedidosVisaNet.ListProductos>();
                productosCarrito = ((List<ProductoCarrito>)Session["CarritoProducto"]);
                foreach (var item in productosCarrito)
                {
                    DatosPedidosVisaNet.ListProductos agreguemos = new DatosPedidosVisaNet.ListProductos();
                    agreguemos.Descripcion = item.NombreProducto;
                    productosAgregar.Add(agreguemos);
                }
                errorDespacho.Attributes.Add("style", "display:none;");
                errorTelefono.Attributes.Add("style", "display:none;");
                errorDireccion.Attributes.Add("style", "display:none;");
                txtPedido.Text = Convert.ToString(Session["STATUSS"]);
                txtTarjeta.Text = Convert.ToString(Session["CardS"]);
                txtFechaHora.Text = fechaError.Substring(0, 2) + "/" + fechaError.Substring(2, 2) + "/" + fechaError.Substring(4, 2) + " " + fechaError.Substring(6, 2) + ":" + fechaError.Substring(8, 2) + ":" + fechaError.Substring(10, 2);
                txtImporte.Text = Convert.ToString(Session["AMOUNT"]);
                txtNombres.Text = Convert.ToString(Session["Nombres"]);
                txtCodigoAccion.Text = Convert.ToString(Session["ACTION_DESCRIPTION"]);
                productos = productosAgregar;
            }
        }
    }
}