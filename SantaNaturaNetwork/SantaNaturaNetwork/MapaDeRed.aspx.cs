﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetwork
{
    public partial class MapaDeRed : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["mostrarCompraTerminada"] = 0;
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<ElementosTreeGrid> ListarEstructura()
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<ElementosTreeGrid> Lista = null;
            try
            {
                Lista = ElementosTreeGridLN.getInstance().ListaTreeGrid(documento.Trim());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<ElementosTreeGrid> ListarEstructurabyPeriodo(int IDP)
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<ElementosTreeGrid> Lista = null;
            try
            {
                Lista = ElementosTreeGridLN.getInstance().ListaTreeGridByPeriodo(documento, IDP);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static string CapturarDNI()
        {
            string documento = "";
            try
            {
                documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return documento.Trim();
        }

        [WebMethod]
        public static bool GenerarVQ()
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<Cliente> Lista = ClienteLN.getInstance().ListarClienteRedVQ(documento);
            List<PuntosComisiones> ListaDirectos = new List<PuntosComisiones>();
            foreach (var item in Lista)
            {
                double VPRSocio = item.VPR;
                double PP = item.PP;
                double VP = item.VP;
                double VQSocio = 0.0;
                ListaDirectos = PuntosNegocios.getInstance().ListaDirectosVPR(item.numeroDoc);
                List<DatosRango> ListaRangos = PuntosNegocios.getInstance().ListaCalculaRango();
                bool ok = Convert.ToBoolean(System.Web.HttpContext.Current.Session["ok2"]);
                int i = 0;
                while (!ok && i < ListaRangos.Count())
                {
                    VQSocio = CalculoVQ(Convert.ToDouble(VPRSocio), ListaRangos[i].PuntajeRango, ListaRangos[i].PP, ListaRangos[i].PML, PP, ListaDirectos, VP);
                    ok = Convert.ToBoolean(System.Web.HttpContext.Current.Session["ok2"]);
                    i++;
                }
                bool AVQ = PuntosNegocios.getInstance().ActualizarVQ(item.numeroDoc, VQSocio);
                System.Web.HttpContext.Current.Session["ok2"] = false;
            }

            return true;
        }

        [WebMethod]
        public static List<Periodo> ListaPeriodos()
        {
            List<Periodo> ListaPeriodo = PeriodoLN.getInstance().ListarPeriodosTotalesByCliente();
            var query = from item in ListaPeriodo
                        select item;

            return query.ToList();
        }

        private static double CalculoVQ(double VPR, int PR, int PP, int PML, double PPS, List<PuntosComisiones> Directos, double VP)
        {
            double VPRA = 0;
            if ((VPR >= PR) & (PPS >= PP))
            {
                double VRA = 0;
                foreach (var item in Directos)
                {
                    if (item.VolumenGeneral >= PML) { VRA = VRA + PML; }
                    else { VRA = VRA + item.VolumenGeneral; }
                }
                VRA = VRA + VP;
                if (VRA >= PR) { System.Web.HttpContext.Current.Session["ok2"] = true; VPRA = VRA; }
            }

            return VPRA;
        }

    }
}