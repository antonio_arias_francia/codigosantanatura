﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class Documentos : System.Web.UI.Page
    {
        public List<ArchivosPDF.DocumentosInformacion> Lista = new List<ArchivosPDF.DocumentosInformacion>();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["mostrarCompraTerminada"] = 0;
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
            Lista = ArchivosLN.getInstance().ListarDocumentosInformacion();
        }
    }
}