﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class PromocionUnica : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) != "10")
            {
                Response.Redirect("Index.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "07")
            {
                Response.Redirect("Principal.aspx");
            }
            if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
            {
                Response.Redirect("Principal.aspx");
            }
        }

        [WebMethod]
        public static List<PromocionUnicaModel> ListaPromocion()
        {
            List<PromocionUnicaModel> Lista = PromocionUnicaLN.getInstance().ListarPromocion();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static int ListaCantPromoUnica()
        {
            int cantidad = PromocionUnicaLN.getInstance().ListaCantPromoUnica();

            return cantidad;
        }

        [WebMethod]
        public static string ListaCodigoPromoActiva()
        {
            string codigo = PromocionUnicaLN.getInstance().ListarCodigoPromoActiva();

            return codigo;
        }

        [WebMethod]
        public static bool RegistrarPromoUnica(string nom_promoSend, string fecInicioSend, string fecFinSend, string estado_genSend, string cantidadSend)
        {
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaIdopXNombre();
            nom_promoSend = (from c in ListaProducto where c.NombreProducto.Trim() == nom_promoSend.Trim() select c.ProductoPais.Trim()).FirstOrDefault();
            if (nom_promoSend == null){ nom_promoSend = ""; }

            PromocionUnicaModel objPromo = new PromocionUnicaModel()
            {
                ProductoPais = nom_promoSend,
                FechaInicio = fecInicioSend,
                FechaFin = fecFinSend,
                Estado = Convert.ToBoolean(estado_genSend),
                Cantidad = Convert.ToInt32(cantidadSend)
            };

            bool ok = PromocionUnicaLN.getInstance().RegistroPromoUnica(objPromo);
            return true;
        }

        [WebMethod]
        public static bool ActualizarPromocionUnica(string id_promoSend, string nom_promoSend, string fecInicioSend, string fecFinSend, string estado_genSend, string cantidadSend)
        {
            List<ProductoV2> ListaProducto = ProductoLN.getInstance().ListaIdopXNombre();
            nom_promoSend = (from c in ListaProducto where c.NombreProducto.Trim() == nom_promoSend.Trim() select c.ProductoPais.Trim()).FirstOrDefault();
            if (nom_promoSend == null) { nom_promoSend = ""; }

            PromocionUnicaModel objPromo = new PromocionUnicaModel()
            {
                IdPromo = Convert.ToInt32(id_promoSend),
                ProductoPais = nom_promoSend,
                FechaInicio = fecInicioSend,
                FechaFin = fecFinSend,
                Estado = Convert.ToBoolean(estado_genSend),
                Cantidad = Convert.ToInt32(cantidadSend)
            };
            bool ok = PromocionUnicaLN.getInstance().ActualizarPromocionUnica(objPromo);
            return true;
        }

    }
}