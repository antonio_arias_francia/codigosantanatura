﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class Bonificaciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05" | Convert.ToString(Session["NumDocCliente"]).Trim() == "99999999")
                {
                    Response.Redirect("Index.aspx");
                }
                else if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }

                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }

            //string documento = Convert.ToString(Session["NumDocCliente"]);
            //MomentaneoComi objCli = new MomentaneoComi();
            //objCli = ClienteLN.getInstance().ComisionMomentanea(documento);
            //if (objCli == null)
            //{
            //    txtGrati.Text = "0";
            //    txtFamiliar.Text = "0";
            //}
            //else
            //{
            //    txtGrati.Text = Convert.ToString(objCli.unilevel).Trim();
            //    txtFamiliar.Text = Convert.ToString(objCli.afiliacion).Trim();
            //}
        }

        [WebMethod]
        public static List<DatosSocios> ComboSocios()
        {
            List<DatosSocios> ListaSocios = SocioLN.getInstance().ListarUpline("0");
            var query = from item in ListaSocios
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<PeriodoComision> ComboPeriodoComision()
        {
            List<PeriodoComision> ListaPeriodo = PeriodoComisionLN.getInstance().ListarPeriodoComisionCombo();
            var query = from item in ListaPeriodo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<PeriodoComision> ListaComisionesLinea()
        {
            List<PeriodoComision> Lista = null;
            string documentoSend = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                Lista = PeriodoComisionLN.getInstance().ListaComisionesLinea(documentoSend);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PeriodoComision> ListaComisionesXPeriodo(int IDP)
        {
            List<PeriodoComision> Lista = null;
            string documentoSend = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                Lista = (IDP <= 33) ? PeriodoComisionLN.getInstance().ListaComisionesXPeriodo_Historico(documentoSend, IDP) : 
                                      PeriodoComisionLN.getInstance().ListaComisionesXPeriodo(documentoSend, IDP);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static MomentaneoComi ListaComisionesExtra()
        {
            MomentaneoComi objCli = new MomentaneoComi();
            string documentoSend = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                objCli = ClienteLN.getInstance().ComisionMomentanea(documentoSend);
                objCli = (objCli == null) ? objCli = new MomentaneoComi("", "", 0, 0, 0, 0, 0, 0, 0, 0, 0): objCli;
            }
            catch (Exception ex)
            {
                objCli = new MomentaneoComi("","",0,0,0,0,0,0,0,0,0);
            }
            return objCli;
        }

        [WebMethod]
        public static List<DatosSocios> ListarCantidadDirectos(string documentoSend)
        {
            List<DatosSocios> Lista = null;
            int IDP = PeriodoComisionLN.getInstance().MostrarIdPeriodoComisionActivo();
            try
            {
                Lista = SocioLN.getInstance().ListarCantidadDirectos(documentoSend, IDP);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<DatosSocios> ListarCantidadDirectosXPeriodo(int IDP)
        {
            List<DatosSocios> Lista = null;
            string documentoSend = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            try
            {
                Lista = SocioLN.getInstance().ListarRetencionGenerada(documentoSend, IDP);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }

        [WebMethod]
        public static List<DatosSocios> ListarCantidadDirectosGenerado()
        {
            List<DatosSocios> Lista = null;
            string documentoSend = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            int IDP = PeriodoComisionLN.getInstance().MostrarIdPeriodoComisionActivo();
            try
            {
                Lista = SocioLN.getInstance().ListarRetencionGenerada(documentoSend, IDP);

            }
            catch (Exception ex)
            {
                Lista = null;
            }
            return Lista;
        }
    }
}