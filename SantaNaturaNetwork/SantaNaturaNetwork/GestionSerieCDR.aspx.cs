﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class GestionSerieCDR : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832" &&
                    Convert.ToString(Session["NumDocCliente"]) != "01901931")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<Cliente.CDR.SerieCDR> ListaSerieCDR()
        {
            List<Cliente.CDR.SerieCDR> Lista = CdrLN.getInstance().ListaSerieCDR();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistrarSerieDR(string dniCDRSend, string serieSend)
        {
            try
            {
                bool registro = CdrLN.getInstance().RegistroSeriePuntoVenta(dniCDRSend, serieSend.Trim());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        [WebMethod]
        public static bool ActualizarSerieCDR(int idSerieSend, string serieSend)
        {
            try
            {
                bool update = CdrLN.getInstance().ActualizarSeriePuntoVenta(idSerieSend, serieSend.Trim());
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        [WebMethod]
        public static bool InicioSerieCDR()
        {
            bool respuesta;
            try
            {
                List<StockCDR> ListaCDR = StockLN.getInstance().ListarCDR();

                foreach (var cdr in ListaCDR)
                {
                    bool registro = CdrLN.getInstance().RegistroSeriePuntoVenta(cdr.DNICDR.Trim(), "");
                }
                respuesta = true;
            }
            catch (Exception ex)
            {
                respuesta = false;
                throw ex;
            }

            return respuesta;
        }

    }
}