﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;
using System.Web.Services;
using System.IO;
namespace SantaNaturaNetworkV3
{
    public partial class GestionarPromociones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) != "10")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "07")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
                {
                    Response.Redirect("Principal.aspx");
                }
            }
        }

        [WebMethod]
        public static List<ArchivosPDF> ListarPromocionesPDF()
        {
            List<ArchivosPDF> Lista = ArchivosLN.getInstance().ListarPromocionesPDF();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistroPromocionPDF(string nombreS, string vigenciaS, string archivoS)
        {
            try
            {
                ArchivosLN.getInstance().RegistroPromocionPDF(nombreS, vigenciaS, archivoS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
            
        }

        [WebMethod]
        public static bool EliminarArchivo(string archivo)
        {
            bool ok;
            var filePath = HttpContext.Current.Server.MapPath("~/promociones/" + archivo);
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
                ok = true;
            }
            else
            {
                ok = false;
            }
            return ok;
        }

        [WebMethod]
        public static bool ActualizarPromocionPDF(string nombreS, string vigenciaS, string archivoS, int idS)
        {
            try
            {
                ArchivosLN.getInstance().ActualizarPromocionPDF(nombreS, vigenciaS, archivoS, idS);
                return true;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }
    }
}