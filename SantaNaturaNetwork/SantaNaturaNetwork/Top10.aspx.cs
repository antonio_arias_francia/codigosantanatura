﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class Top10 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["mostrarCompraTerminada"] = 0;
            if (!Page.IsPostBack)
            {
                if (Session["IdCliente"] == null)
                {
                    Response.Redirect("Login.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "" | Convert.ToString(Session["TipoCliente"]) == "03" | Convert.ToString(Session["TipoCliente"]) == "05")
                {
                    Response.Redirect("Index.aspx");
                }
                if (Convert.ToString(Session["TipoCliente"]) == "10")
                {
                    Response.Redirect("Principal.aspx");
                }
                if (Convert.ToString(Session["NumDocCliente"]).Trim() != "99999999")
                {
                    comisionesdiv.Attributes.Add("style", "display:none;");
                    div1.Attributes.Add("style", "display:none;");
                    div2.Attributes.Add("style", "display:none;");
                }

                //Validamos Caducidad de Clave
                string idCliente = Convert.ToString(Session["IdCliente"]);
                string result = "";

                result = ClienteLN.getInstance().ValidadCaducidadClave(idCliente);


                if (result == "Clave Vencida")
                {
                    Session["clave_vencida"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
                //Validamos si ya agregó sus preguntas de Seguridad
                result = ClienteLN.getInstance().ValidadPreguntasSeguridadUsuario(idCliente);
                if (result == "No Existe")
                {
                    Session["preguntas_seguridad"] = result;
                    Response.Redirect("EditarPerfil.aspx");
                }
            }
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaTop10VQActual()
        {
            int IDPPuntos = PuntosNegocios.getInstance().MostrarIdPeriodoPuntosActivo();
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                Lista = PuntosNegocios.getInstance().ListaTop10VQ(documento.Trim(), IDPPuntos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaTop10VQFiltro(int IDPPuntos)
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                Lista = PuntosNegocios.getInstance().ListaTop10VQ(documento.Trim(), IDPPuntos);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaTop10ComisionesActual()
        {
            int IDPPuntos = Convert.ToInt32(PeriodoLN.getInstance().ListarCodigoPeriodoActivaComision());
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                if (documento.Trim() == "99999999") { Lista = PuntosNegocios.getInstance().ListaTop10Comisiones(documento.Trim(), IDPPuntos); }
                else { Lista = new List<PuntosComisiones>(); }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }

        [WebMethod]
        public static List<PuntosComisiones> ListaTop10ComisionesFiltro(int IDPPuntos)
        {
            string documento = Convert.ToString(System.Web.HttpContext.Current.Session["NumDocCliente"]);
            List<PuntosComisiones> Lista = null;
            try
            {
                if (documento.Trim() == "99999999") { Lista = PuntosNegocios.getInstance().ListaTop10Comisiones(documento.Trim(), IDPPuntos); }
                else { Lista = new List<PuntosComisiones>(); }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Lista;
        }
    }
}