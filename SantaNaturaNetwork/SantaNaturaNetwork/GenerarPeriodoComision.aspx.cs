﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Modelos;
using Negocios;

namespace SantaNaturaNetworkV3
{
    public partial class GenerarPeriodoComision : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                Response.Redirect("Login.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) != "10")
            {
                Response.Redirect("Index.aspx");
            }
            if (Convert.ToString(Session["TipoCliente"]) == "07")
            {
                Response.Redirect("Principal.aspx");
            }
            if (Convert.ToString(Session["NumDocCliente"]) != "45750970" && Convert.ToString(Session["NumDocCliente"]) != "730115832")
            {
                Response.Redirect("Principal.aspx");
            }
        }

        [WebMethod]
        public static int ListaCantPeriodo()
        {
            int cantidad = PeriodoLN.getInstance().ListarCantPeriodoComision();

            return cantidad;
        }

        [WebMethod]
        public static string ListaCodigoPeriodoActiva()
        {
            string codigo = PeriodoLN.getInstance().ListarCodigoPeriodoActivaComision();

            return codigo;
        }

        [WebMethod]
        public static List<Periodo> ListaPeriodo()
        {
            List<Periodo> Lista = PeriodoLN.getInstance().ListarPeriodoComision();
            var query = from item in Lista
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static List<Periodo> ListaPeriodoRango()
        {
            List<Periodo> ListaPeriodo = PeriodoLN.getInstance().ListarPeriodosTotales();
            var query = from item in ListaPeriodo
                        select item;

            return query.ToList();
        }

        [WebMethod]
        public static bool RegistrarPeriodo(string nom_periodoSend, string fecInicioSend, string fecFinSend, string estado_genSend, string periodoSend, bool visual)
        {


            Periodo objPeriodo = new Periodo()
            {
                nombre = nom_periodoSend,
                fechaInicio = fecInicioSend,
                fechaFin = fecFinSend,
                estado = Convert.ToBoolean(estado_genSend),
                idPeriodo= Convert.ToInt32(periodoSend),
                visualizar = visual
            };

            bool ok = PeriodoLN.getInstance().RegistroPeriodoComision(objPeriodo);

            List<Cliente> listaCliente = ClienteLN.getInstance().ListarClientePeriodo();
            foreach (var item in listaCliente)
            {
                bool todoOK = PeriodoLN.getInstance().RegistroClientePeriodoComision(item.numeroDoc.Trim(), item.upline.Trim(), item.patrocinador.Trim(), 
                                                                                     item.idCliente.Trim(), item.idClienteUpline, item.tipoCliente);
                bool retencion = PeriodoLN.getInstance().RegistroSociosRetencion(item.idCliente.Trim(), item.numeroDoc.Trim());
            }

            return true;
        }

        [WebMethod]
        public static bool ActualizarPeriodo(string id_periodoSend, string nom_periodoSend, string fecInicioSend, string fecFinSend, string estado_genSend, string idRangoSend, bool visual)
        {

            Periodo objPeriodo = new Periodo()
            {
                idPeriodoComision = Convert.ToInt32(id_periodoSend),
                nombre = nom_periodoSend,
                fechaInicio = fecInicioSend,
                fechaFin = fecFinSend,
                estado = Convert.ToBoolean(estado_genSend),
                idPeriodo= Convert.ToInt32(idRangoSend),
                visualizar = visual
            };
            bool ok = PeriodoLN.getInstance().ActualizarPeriodoComision(objPeriodo);
            return true;
        }

    }
}