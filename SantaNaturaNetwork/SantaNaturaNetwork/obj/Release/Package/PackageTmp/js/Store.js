﻿ObtenerTipoCliente();
var input = document.getElementById('SMedioPago');
var inputTCompra = document.getElementById('STipoCompra');
var inputTienda = document.getElementById('ComboTienda');
var inputPregis = document.getElementById('cboPreRegistro');
var inputPregis2 = document.getElementById('cboPreRegistro2');
var tipoCliente = "";

input.onchange = function () {
    localStorage['SMedioPago'] = this.value;
    GuardarIDCombos('MPago', this.value);
}
inputTCompra.onchange = function () {
    var preregis = $('#cboPreRegistro').val();
    if (preregis != "0" && preregis != "" && preregis != null) {
        if (this.value == "01" || this.value == "02" || this.value == "03" ||
            this.value == "04" || this.value == "05" || this.value == "06" ||
            this.value == "23") {
            if ((tipoCliente == "01" && (this.value == "05" || this.value == "06")) ||
                (tipoCliente != "01" && (this.value != "05" || this.value != "06"))) {
                $('#STipoCompra').val(localStorage['STipoCompra']);
                NoCambioTipoCliente();
            } else {
                localStorage['STipoCompra'] = this.value;
                GuardarIDCombos('TCompra', this.value);
            }
        } else {
            $('#STipoCompra').val(localStorage['STipoCompra']);
            NoCambioPreRegistro();
        }
    } else {
        localStorage['STipoCompra'] = this.value;
        GuardarIDCombos('TCompra', this.value);
    }
}
inputTienda.onchange = function () {
    localStorage['ComboTienda'] = this.value;
    GuardarIDCombos('TiendaS', this.value);
}
inputPregis.onchange = function () {
    localStorage['cboPreRegistro'] = this.value;
    localStorage['cboPreRegistro2'] = this.value;
    GuardarIDCombos('Pregis', this.value);
}
inputPregis2.onchange = function () {
    localStorage['cboPreRegistro'] = this.value;
    localStorage['cboPreRegistro2'] = this.value;
    GuardarIDCombos('Pregis', this.value);
}

function ObtenerTipoCliente() {
    $.ajax({
        type: "POST",
        url: "TiendaSN.aspx/ObtenerTipoCliente",
        data: "{}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            tipoCliente = dataS.d;
        }
    });
}

function GuardarIDCombos(combo, idcombo) {
    var obj = JSON.stringify({
        comboS: combo, idComboS: idcombo
    });
    $.ajax({
        type: "POST",
        url: "TiendaSN.aspx/GuardarIDCombos",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            if (combo == "TCompra") {
                $('#LbPrecioPagar').text("S/." + Datos.MontoPago);
            }
            if (combo == "Pregis" && (idcombo != "0" && idcombo != "" && idcombo != null)) {
                localStorage['STipoCompra'] = Datos.IdPaquete;
                $('#STipoCompra').val(Datos.IdPaquete);
            }
        }
    });
}

function AgregarCarrito(codigo) {
    $('#page_loader').show();
    var cMPago = $("#SMedioPago").val();
    var cCom = $('#STipoCompra').val();
    var cTienda = $('#ComboTienda').val();

    if (cCom != '0' && cTienda != '0' && cMPago != '0') {
        var cantidad = $('#' + codigo + '').val();
        var obj = JSON.stringify({
            codProductoCarrito: codigo, cantidadProductos: cantidad
        });
        $.ajax({
            type: "POST",
            url: "TiendaSN.aspx/AgregarProducto",
            data: obj,
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (dataS) {
                var Datos = dataS.d;
                if (Datos.Mensaje == "OK") {
                    $('#LbPrecioTotal').text("S/." + parseFloat(Datos.Total).toFixed(2));
                    $('#LbPrecioPagar').text("S/." + parseFloat(Datos.Monto).toFixed(2));
                    $('#LbPuntosCompra').text(Datos.Puntos);
                    $('#LbPuntosRango').text(Datos.PuntosPromo);
                    $('#LbPrecioTotal2').text("S/." + parseFloat(Datos.Total).toFixed(2));
                    $('#LbPrecioPagar2').text("S/." + parseFloat(Datos.Monto).toFixed(2));
                    $('#LbPuntosCompra2').text(Datos.Puntos);
                    $('#LbPuntosRango2').text(Datos.PuntosPromo);
                    $('#page_loader').hide();
                    Swal.fire({
                        title: 'Perfecto!',
                        text: 'Producto Agregado',
                        type: "success"
                    });
                }
                else {
                    $('#page_loader').hide();
                    Swal.fire({
                        title: 'Ooops...!',
                        text: Datos.Mensaje,
                        type: "error"
                    });
                }
            }
        });
    } else {
        Swal.fire({
            title: 'Ooops...!',
            text: 'Debe seleccionar todos los datos de la compra',
            type: "error"
        });
    }

}

function irCarrito() {
    var cMPago = $("#SMedioPago").val();
    var cCom = $('#STipoCompra').val();
    var cTienda = $('#ComboTienda').val();
    if (cCom != '0' && cTienda != '0' && cMPago != '0') {
        $.ajax({
            type: "POST",
            url: "TiendaSN.aspx/CantidadProdCarrito",
            data: "{}",
            contentType: 'application/json; charset=utf-8',
            error: function (xhr, ajaxOptions, throwError) {
                console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
            },
            success: function (dataS) {
                if (dataS.d > 0) {
                    location.href = "DetalleDeCompra.aspx";
                } else {
                    Swal.fire({
                        title: 'Ooops...!',
                        text: "Debes agregar productos al carrito",
                        type: "error"
                    });
                }
            }
        });
    } else {
        Swal.fire({
            title: 'Ooops...!',
            text: 'Debe seleccionar todos los datos de la compra',
            type: "error"
        });
    }

}

function VerTodosProductos() {
    location.href = "TiendaSN.aspx";
}

$(function () {
    $("[id$=txtNomProducto]").autocomplete({
        source: function (request, responce) {
            $.ajax({
                url: "Autocompletado.asmx/FiltrarNombreProductos",
                method: "post",
                CORS: true,
                contentType: "application/json;charset=utf-8",
                data: "{ 'palabra': '" + request.term + "'}",
                dataType: 'json',
                success: function (data) {
                    responce(data.d);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("In The ERROR");
                    console.log(XMLHttpRequest);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }
    });
});

function NoCambioPreRegistro() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Está completando un preregistro, no puede seleccionar esa opción',
    })
}

function NoCambioTipoCliente() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'No puede realizar cambio en el tipo de cliente',
    })
}