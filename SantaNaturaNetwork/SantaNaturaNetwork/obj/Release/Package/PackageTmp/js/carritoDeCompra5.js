﻿var input = document.getElementById('SMedioPago');
var inputTCompra = document.getElementById('STipoCompra');
var inputTienda = document.getElementById('ComboTienda');
var inputCompro = document.getElementById('cboComprobante');
var inputTitular = document.getElementById('ddlTitularRUC');
var RUC_Validado = 0;
var Ruc_Perfil = "", montoCompraEval = 0, puntosCompraEval = 0, tipoCliente = "", filtro_preregistro = 0;
$('#cboComprobante').val(localStorage['cboComprobante']);
$('#ddlTitularRUC').val(localStorage['ddlTitularRUC']);

ObtenerRUC_Perfil();
CambioXComprobante();
CambioXTitular();
CambioTipoCliente();
ObtenerTipoCliente();

input.onchange = function () {
    localStorage['SMedioPago'] = this.value;
    GuardarIDCombos('MPago', this.value);
}
inputTCompra.onchange = function () {
    var preregis = $('#cboPreRegistro').val();
    if (preregis != "0" && preregis != "" && preregis != null) {
        if (this.value == "01" || this.value == "02" || this.value == "03" ||
            this.value == "04" || this.value == "05" || this.value == "06" ||
            this.value == "23") {
            if ((tipoCliente == "01" && (this.value == "05" || this.value == "06")) ||
                (tipoCliente != "01" && (this.value != "05" || this.value != "06"))) {
                $('#STipoCompra').val(localStorage['STipoCompra']);
                NoCambioTipoCliente();
            } else {
                localStorage['STipoCompra'] = this.value;
                GuardarIDCombos('TCompra', this.value);
            }
        } else {
            $('#STipoCompra').val(localStorage['STipoCompra']);
            NoCambioPreRegistro();
        }
    } else {
        localStorage['STipoCompra'] = this.value;
        GuardarIDCombos('TCompra', this.value);
    }
}
inputTienda.onchange = function () {
    localStorage['ComboTienda'] = this.value;
    GuardarIDCombos('TiendaS', this.value);
}
inputCompro.onchange = function () {
    localStorage['cboComprobante'] = this.value;
    CambioXComprobante();
}
inputTitular.onchange = function () {
    localStorage['ddlTitularRUC'] = this.value;
    CambioXTitular();
}

$.ajax({
    type: "POST",
    url: "EditarPerfil.aspx/GetPais",
    data: "{}",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    async: false,
    success: function (result) {
        $("#cboPais").empty();
        $("#cboPais").append("<option value='0'>--Select--</option>");
        $.each(result.d, function (key, value) {
            $("#cboPais").append($("<option></option>").val(value.Codigo).html(value.Nombre));
        });
    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        alert(textStatus + ": " + XMLHttpRequest.responseText);
    }
});

$("#cboPais").change(function () {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>--Select--</option>");
            CargarProvincia();
            CargarDistrito();
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboDepartamento").change(function () {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetProvinciaByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboProvincia").empty();
            $("#cboProvincia").append("<option value='0'>--Select--</option>");
            CargarDistrito();
            $.each(result.d, function (key, value) {
                $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

$("#cboProvincia").change(function () {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
});

function CargarDepartamentos() {
    var params = new Object();
    params.pais = $("#cboPais").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDepartamentosByPais",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDepartamento").empty();
            $("#cboDepartamento").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDepartamento").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarProvincia() {
    var params = new Object();
    params.departamento = $("#cboDepartamento").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetProvinciaByDepartamento",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboProvincia").empty();
            $("#cboProvincia").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboProvincia").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CargarDistrito() {
    var params = new Object();
    params.provincia = $("#cboProvincia").val();
    params = JSON.stringify(params);

    $.ajax({
        type: "POST",
        url: "EditarPerfil.aspx/GetDistritoByProvincia",
        data: params,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (result) {
            $("#cboDistrito").empty();
            $("#cboDistrito").append("<option value='0'>--Select--</option>");
            $.each(result.d, function (key, value) {
                $("#cboDistrito").append($("<option></option>").val(value.Codigo).html(value.Nombre));
            });
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(textStatus + ": " + XMLHttpRequest.responseText);
        }
    });
}

function CambioTipoCliente() {

    var Tcompra = $('#STipoCompra').val();
    if (Tcompra == "01" || Tcompra == "02" || Tcompra == "03" ||
        Tcompra == "04" || Tcompra == "23") {
        $('#cboTipoCliente').val("01");
    }
    else if (Tcompra == "05") {
        $('#cboTipoCliente').val("05");
    }
    else {
        $('#cboTipoCliente').val("03");
    }
}

function CambioXComprobante() {
    var Tcomprob = $('#cboComprobante').val();

    if (Tcomprob == "2") {
        $('#MostrarBotonValidar').show();
    } else {
        $('#MostrarBotonValidar').hide();
    }
}

function CambioXTitular() {
    var Ttitular = $('#ddlTitularRUC').val();

    if (Ttitular == "2") {
        $('#dvRUC').show();
        $('#dvBtnValRUC').show();
    } else {
        $('#dvRUC').hide();
        $('#dvBtnValRUC').hide();
    }
}

function ObtenerRUC_Perfil() {
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/ObtenerRUC_Perfil",
        data: "{}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            Ruc_Perfil = Datos.RUC;
            filtro_preregistro = Datos.PreRegistro;
            montoCompraEval = parseFloat(Datos.montoTotalPagar);
            puntosCompraEval = parseFloat(Datos.puntosTotal);
            MostrarBotonCompra();
        }
    });
}

function ObtenerTipoCliente() {
    $.ajax({
        type: "POST",
        url: "TiendaSN.aspx/ObtenerTipoCliente",
        data: "{}",
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            tipoCliente = dataS.d;
        }
    });
}

function MostrarBotonCompra() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var SpreRe = $("#cboPreRegistro").val();

    if (SpreRe == "0" && filtro_preregistro == 0) {
        if (StipoC == "01" || StipoC == "02" || StipoC == "03" || StipoC == "04" ||
            StipoC == "05" || StipoC == "06" || StipoC == "23") {
            $("#CompraNormal").hide();
            $("#CompraPE").hide();
            $("#btnRegistrarAfiliado").show();
        }
        else if ((StipoC != "01" || StipoC != "02" || StipoC != "03" || StipoC != "04" ||
            StipoC != "05" || StipoC != "06" || StipoC != "23") && StipoP == "05") {
            $("#CompraNormal").show();
            $("#CompraPE").hide();
            $("#btnRegistrarAfiliado").hide();
        }
        else if ((StipoC != "01" || StipoC != "02" || StipoC != "03" || StipoC != "04" ||
            StipoC != "05" || StipoC != "06" || StipoC != "23") && StipoP == "04") {
            $("#CompraNormal").hide();
            $("#CompraPE").show();
            $("#btnRegistrarAfiliado").hide();
        }
    }
    else {
        if (StipoP == "05") {
            $("#CompraNormal").show();
            $("#CompraPE").hide();
            $("#btnRegistrarAfiliado").hide();
        }
        else if (StipoP == "04") {
            $("#CompraNormal").hide();
            $("#CompraPE").show();
            $("#btnRegistrarAfiliado").hide();
        }
    }

}

function GuardarIDCombos(combo, idcombo) {

    MostrarBotonCompra();
    var obj = JSON.stringify({
        comboS: combo, idComboS: idcombo
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/GuardarIDCombos",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            montoCompraEval = parseFloat(Datos.MontoPago).toFixed(2);
            if (combo == "TCompra") {
                $('#LbPrecioPagar').text("S/." + parseFloat(Datos.MontoPago).toFixed(2));
            }
            if (combo == "Pregis" && (idcombo != "0" && idcombo != "" && idcombo != null)) {
                localStorage['STipoCompra'] = Datos.IdPaquete;
                $('#STipoCompra').val(Datos.IdPaquete);
            }
        }
    });
}

function ActualizarProducto(idProductoPais) {

    var cant = $("#" + idProductoPais + "").val();
    var StipoC = $("#STipoCompra").val();

    var obj = JSON.stringify({
        idProdPais: idProductoPais, nuevaCantidad: cant, tipCompra: StipoC
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/ActualizarProducto",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            montoCompraEval = parseFloat(Datos.Monto);
            puntosCompraEval = parseFloat(Datos.Puntos);
            $('#LbPuntosRango').text(Datos.PuntosPromo);
            $('#LbPuntosCompra').text(Datos.Puntos);
            $('#LbPrecioTotal').text("S/." + parseFloat(Datos.Total).toFixed(2));
            $('#LbPrecioPagar').text("S/." + parseFloat(Datos.Monto).toFixed(2));
            $('#Sub' + idProductoPais + '').text("S/." + parseFloat(Datos.SubTotalProd).toFixed(2));
            $('#SubP' + idProductoPais + '').text(Datos.SubTotalPunt);

            $('#lbPuntosRango2').text(Datos.PuntosPromo);
            $('#lblPuntos2').text(Datos.Puntos);
            $('#lblPTotal2').text("S/." + parseFloat(Datos.Total).toFixed(2));
            $('#lblPPagar2').text("S/." + parseFloat(Datos.Monto).toFixed(2));
            Swal.fire({
                title: 'Perfect!',
                text: 'Producto Actualizado',
                type: "success"
            });
        }
    });
}

function EliminarProducto(idProductoPais) {
    var index = $(this).parent().parent().index();
    var obj = JSON.stringify({
        idProdPais: idProductoPais
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/EliminarProducto",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;
            montoCompraEval = parseFloat(Datos.Monto);
            puntosCompraEval = parseFloat(Datos.Puntos);
            $('#LbPuntosRango').text(Datos.PuntosPromo);
            $('#LbPuntosCompra').text(Datos.Puntos);
            $('#LbPrecioTotal').text("S/." + parseFloat(Datos.Total).toFixed(2));
            $('#LbPrecioPagar').text("S/." + parseFloat(Datos.Monto).toFixed(2));


            $('#lbPuntosRango2').text(Datos.PuntosPromo);
            $('#lblPuntos2').text(Datos.Puntos);
            $('#lblPTotal2').text("S/." + parseFloat(Datos.Total).toFixed(2));
            $('#lblPPagar2').text("S/." + parseFloat(Datos.Monto).toFixed(2));
            $("#tbl_datosPro> tbody > tr").eq(index).remove();
            Swal.fire({
                title: 'Perfecto!',
                text: 'Producto Eliminado',
                type: "success"
            }).then(function () {
                if (Datos.Mensaje == "0") {
                    window.location = "TiendaSN.aspx";
                }
            });
        }
    });
}

$("#ButtonValida").click(function (e) {
    e.preventDefault();
    var ruc = $("#TextBoxRUC").val();
    validarRUC_Texto(ruc.trim());
});

function validarRUC_Texto(numruc) {
    var obj = JSON.stringify({
        ruc: numruc
    });
    $.ajax({
        type: "POST",
        url: "https://servicio.apirest.pe/api/getRuc",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgzMmUzYTI3ZDA0YzI2ZjFmNTIwNTJhMjE5ODRmOTI1NDY2ZmM1YjA2MzcyOWNiNDc4NjExM2VlYzRjZDA5NjY2OTRjNzEzY2U5YTNjNmMyIn0.eyJhdWQiOiIxIiwianRpIjoiODMyZTNhMjdkMDRjMjZmMWY1MjA1MmEyMTk4NGY5MjU0NjZmYzViMDYzNzI5Y2I0Nzg2MTEzZWVjNGNkMDk2NjY5NGM3MTNjZTlhM2M2YzIiLCJpYXQiOjE1ODk5Mjc5MTAsIm5iZiI6MTU4OTkyNzkxMCwiZXhwIjoxOTA1NDYwNzEwLCJzdWIiOiIxMDcyIiwic2NvcGVzIjpbIioiXX0.b_D5SKP8vifhZ38XY88NLKoO_0djb42S8KRqG76ZRLoSYI8f_Y4hEbGiyA0lmEk1miTd5I7_qbE7VPYIYpwy4CxaEN2lVJaqyTTnjupALsDiV0i2a5JMZ5kklXs-HdE6m7Ikovax-ZREcRMTKHxcpAIznYvcL-j3lutmlXRLoUVpRdyWFpVLGpWLagxhu7wHIrURW3ssGZPZCcv6hci3Xs9y8iJUKCt1AU1oNRBiwoL25O-4740jrQRzLLX2PP5KnHsFVNxy79y4LeC4zlhpPmpAuiJKi4KnHj4m3A0oBy9mql8rlCXzGGK9pyd8XjiO4gpiqa82xaYSawk6F6M6h8RkVAaEXZKeFuofDIQjKsp0QClw6egRA24TA-j_ZyvzDX5HzTG6y_1-H1zjPhSmVPH-3fsU8YZ52EFQcHJ28m3AaEDR9NOmaOgy-nLEIvAjunFMc67tO8A6f4I6QInnpcexW4S6FHcDT3Dws3v38BBCac31Pkmn4OKNl4ymIX37h-cztmBKC-tUDFI5Xssy26GxRPt-7NRlO-3BSU9HPEGke855TaigjnObSf6opHUxR9NjDzqVecsEAtNsz9cgozX_EqyGhRiSH2YwnYFOp-IU22v3SI1VNu0ijd263pUhVFfH1JM1OtqWVU8-vOLSC68tvFsF0ylS2HT3LO96U2Q');
        },
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            var error_mostrar = '';
            if (xhr.status === 0) {
                error_mostrar = ('Verificar conexión a internet');
                ErrorSunat(error_mostrar);
            } else if (xhr.status == 404) {
                error_mostrar = ('URL solicitada no encontrada [Error 404] - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (xhr.status == 500) {
                error_mostrar = ('Error interno del servidor [Error 500] - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (status === 'parsererror') {
                error_mostrar = ('JSON solicitado falló - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (status === 'timeout') {
                error_mostrar = ('Error en tiempo de espera - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (status === 'abort') {
                error_mostrar = ('Solicitud Ajax abortada - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else {
                error_mostrar = ('Error desconocido: - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            }
        },
        success: function (data) {
            var success = data.success;
            var estado = data.result.Estado;
            if (success == true /*&& estado == "ACTIVO"*/) {
                var RS = data.result.RazonSocial;
                RUC_Validado = 1;
                Swal.fire({
                    title: 'RUC Válido!',
                    text: 'Al continuar con la compra está aceptando que los datos de facturación estarán a nombre de:' + RS,
                    type: "success"
                });
                //ValidacionOK(RS, numruc);
            } else {
                ErrorRUC();
            }


        }
    });
}

function ValidacionOK(RS, numruc) {
    var obj = JSON.stringify({
        ruc: numruc
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/ValidarRUC",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);

        }
    });
}

function MostrarRegistroCliente(value) {
    var dropdown = value/*value.options[value.selectedIndex].value*/;
    if (dropdown == "07" | dropdown == "08" | dropdown == "09" | dropdown == "10" |
        dropdown == "11" | dropdown == "12" | dropdown == "13") {
        //document.getElementById("circle02").hide;
        $("#circle02").hide();
        $("#circulito").text("02");
    }
    else {
        $("#circle02").show();
        $("#circulito").text("03");
    }

    var divRegistroCliente = document.getElementById('MostrarRegistroCliente');

    if (value == 1 || value == 2 || value == 3 || value == 4 || value == 5 || value == 6) {
        divRegistroCliente.style.display = "block";
        fijarCampoRequeridoRegistrarCliete();
    }
    else {
        divRegistroCliente.style.display = "none";
        quitarCampoRequeridoRegistrarCliete();
    }

    location.href = ("DetalleDeCompra.aspx?sTipoCom=" + value);
}

function GuardarDatosDelivery2() {

    var nombreD = $("#txtNombreDatosCompra").val();
    var DNID = $("#txtDNIDatosCompra").val();
    var CelularD = $("#txtCelularDatosCompra").val();
    var DireccionD = $("#txtDireccionDatosCompra").val();
    var NomTransporteD = $("#txtTransporteDatosCompra").val();
    var DirecTransporteD = $("#txtDirecTransporteDatosCompra").val();
    var ProvinciaD = $("#txtProvinciaDatosCompra").val();
    var DirecProvinciaD = $("#txtDirecProvinciaDatosCompra").val();
    var obj2 = JSON.stringify({
        Nombre: nombreD, DNI: DNID, Celular: CelularD, Direccion: DireccionD,
        Transporte: NomTransporteD, DirTransporte: DirecTransporteD, Provincia: ProvinciaD, DirProvincia: DirecProvinciaD
    });

    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/GuardarDatosDelivery",
        data: obj2,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (data2) {
            console.log(data2.d);
        }
    });
}

$(".tablaSiguiente").click(function (e) {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    if (StipoC == "07" | StipoC == "08" | StipoC == "09" | StipoC == "10" |
        StipoC == "11" | StipoC == "12" | StipoC == "13") {
        var ordenCompletada = document.getElementById("ordenCompletada");
        var elemento = document.getElementById("circle03");
        $("#03ordenCompletada").fadeIn(300);
        $("#02afiliacion").slideUp(0);
        $("#01carritoDeCompras").slideUp(0);
        $("#resumenDeLaCompra").slideUp(0);
        $("#tablaCompra").slideUp(0);
        elemento.className += " active"
        ordenCompletada.className += " active"
    } else {
        var afiliacion = document.getElementById("afiliacion");
        var elemento = document.getElementById("circle02");
        $("#tablaCompra").slideUp(0);
        $("#resumenDeLaCompra").slideUp(0);
        $("#02afiliacion").fadeIn(300);
        elemento.className += " active";
        afiliacion.className += " active";
    }

    if (StipoP == "05") {
        $("#CompraNormal2").show();
        $("#CompraPE2").hide();
    }
    else if (StipoP == "04") {
        $("#CompraNormal2").hide();
        $("#CompraPE2").show();
    }


});

function MostrarComboTiendaAndDatosDelivery(value) {
    var divComboTienda = document.getElementById('MostrarComboTiena');
    var divDatosDelivery = document.getElementById('MostrarDatosParaDelivery');

    if (value == "1") {
        divComboTienda.style.display = "block";
        divDatosDelivery.style.display = "none"
    }
    else {
        divComboTienda.style.display = "none";
        divDatosDelivery.style.display = "block"
    }
}

function validarLetras(e) {
    var keyCode = (e.keyCode ? e.keyCode : e.which);
    if (keyCode > 47 && keyCode < 58 || keyCode > 95 && keyCode < 107) {
        e.preventDefault();
    }
}

function validarNumeros(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$("#BntComprar").click(function (e) {
    var SpreRe = $("#cboPreRegistro").val();

    if (filtro_preregistro == 0) {
        ValidarPedidoNormal();
    }
    else {
        ValidarPedidoPreRegistro();
    }
});

$("#btnCompraPE").click(function (e) {
    var SpreRe = $("#cboPreRegistro").val();

    if (filtro_preregistro == 0) {
        ValidarPedidoPE();
    }
    else {
        ValidarPedidoPreRegistroPE();
    }

});

$("#CompraNormal2").click(function (e) {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Scomprob = $("#cboComprobante").val();
    var Stitular = $("#ddlTitularRUC").val();
    var tUsuario = $("#txtUl").val();
    var tClave = $("#TxtCl").val();
    var cUpline = $("#CboUpLine").val();
    var cDistrito = $("#cboDistrito").val();
    var cProvincia = $("#cboProvincia").val();
    var cDepartamento = $("#cboDepartamento").val();
    var cPremio = $("#cboPremio").val();
    var tCelular = $("#TxtCelular").val();
    var tDireccion = $("#txtDireccion").val();
    var tDocumento = $("#txtNumDocumento").val();
    var tFechaNac = $("#datepicker").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (tUsuario == "") {
        FaltaSeleccionar("Debe ingresar un usuario valido");
    }
    else if (tClave == "") {
        FaltaSeleccionar("Debe ingresar una clave valida");
    }
    else if (tFechaNac == "") {
        FaltaSeleccionar("Debe ingresar una fecha de nacimiento correcta");
    }
    else if (cDepartamento == "") {
        FaltaSeleccionar("Debe seleccionar el Departamento");
    }
    else if (cProvincia == "") {
        FaltaSeleccionar("Debe seleccionar la Provincia");
    }
    else if (cDistrito == "") {
        FaltaSeleccionar("Debe seleccionar el Distrito");
    }
    else if (cPremio == "") {
        FaltaSeleccionar("Debe seleccionar el Y.W. Premio");
    }
    else if (tCelular == "" || tCelular.length < 9) {
        FaltaSeleccionar("Debe ingresar un número de celular válido");
    }
    else if (tDireccion == "") {
        FaltaSeleccionar("Debe ingresar una dirección válida");
    }
    else if (tDocumento == "" || tDocumento.length < 8) {
        FaltaSeleccionar("Debe ingresar una documento válido");
    }
    else if (Scomprob == "0" || Scomprob == "" || Scomprob == null) {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Scomprob == "2" && (Stitular == "0" || Stitular == "" || Stitular == null)) {
        FaltaTitular();
    }
    else if (Scomprob == "2" && Stitular == "2" && RUC_Validado == 0) {
        ErrorRUC();
    }
    else if (cUpline == "" && (StipoC != "05" && StipoC != "06")) {
        FaltaUpline();
    }
    else if (Scomprob == "2" && Stitular == "1") {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                validarRUC_Perfil(Ruc_Perfil, "CCAF");
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            validarRUC_Perfil(Ruc_Perfil, "CCAF");
        }
    }
    else {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                CompraCanjeAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            CompraCanjeAfiliacion();
        }
    }

});

$("#CompraPE2").click(function (e) {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Scomprob = $("#cboComprobante").val();
    var Stitular = $("#ddlTitularRUC").val();
    var tUsuario = $("#txtUl").val();
    var tClave = $("#TxtCl").val();
    var cUpline = $("#CboUpLine").val();
    var cDistrito = $("#cboDistrito").val();
    var cProvincia = $("#cboProvincia").val();
    var cDepartamento = $("#cboDepartamento").val();
    var cPremio = $("#cboPremio").val();
    var tCelular = $("#TxtCelular").val();
    var tDireccion = $("#txtDireccion").val();
    var tDocumento = $("#txtNumDocumento").val();
    var tFechaNac = $("#datepicker").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (tFechaNac == "") {
        FaltaSeleccionar("Debe ingresar una fecha de nacimiento correcta");
    }
    else if (tUsuario == "") {
        FaltaSeleccionar("Debe ingresar un usuario valido");
    }
    else if (tClave == "") {
        FaltaSeleccionar("Debe ingresar una clave valida");
    }
    else if (cDepartamento == "") {
        FaltaSeleccionar("Debe seleccionar el Departamento");
    }
    else if (cProvincia == "") {
        FaltaSeleccionar("Debe seleccionar la Provincia");
    }
    else if (cDistrito == "") {
        FaltaSeleccionar("Debe seleccionar el Distrito");
    }
    else if (cPremio == "") {
        FaltaSeleccionar("Debe seleccionar el Y.W. Premio");
    }
    else if (tCelular == "" || tCelular.length < 9) {
        FaltaSeleccionar("Debe ingresar un número de celular válido");
    }
    else if (tDireccion == "") {
        FaltaSeleccionar("Debe ingresar una dirección válida");
    }
    else if (tDocumento == "" || tDocumento.length < 8) {
        FaltaSeleccionar("Debe ingresar una documento válido");
    }
    else if (Scomprob == "0" || Scomprob == "" || Scomprob == null) {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Scomprob == "2" && (Stitular == "0" || Stitular == "" || Stitular == null)) {
        FaltaTitular();
    }
    else if (Scomprob == "2" && Stitular == "2" && RUC_Validado == 0) {
        ErrorRUC();
    }
    else if (cUpline == "" && (StipoC != "05" && StipoC != "06")) {
        FaltaUpline();
    }
    else if (Scomprob == "2" && Stitular == "1") {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                validarRUC_Perfil(Ruc_Perfil, "PEAF");
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            validarRUC_Perfil(Ruc_Perfil, "PEAF");
        }
    }
    else {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                CompraPEAfiliacion();
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            CompraPEAfiliacion();
        }
    }

});

function ValidarPedidoNormal() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Scomprob = $("#cboComprobante").val();
    var Stitular = $("#ddlTitularRUC").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (Scomprob == "0" || Scomprob == "" || Scomprob == null) {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Scomprob == "2" && (Stitular == "0" || Stitular == "" || Stitular == null)) {
        FaltaTitular();
    }
    else if (Scomprob == "2" && Stitular == "2" && RUC_Validado == 0) {
        ErrorRUC();
    }
    else if (Scomprob == "2" && Stitular == "1") {
        if (StipoC == "08") {
            if (Spuntos >= 50 & SprecioPago >= 187.50) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 187.50 para realizar la compra");
            }
        }
        else if (StipoC == "09") {
            if (Spuntos >= 200 & SprecioPago >= 700) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 200 puntos y un monto de S/. 700 para realizar la compra");
            }
        }
        else if (StipoC == "10") {
            if (Spuntos >= 450 & SprecioPago >= 1350) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 450 puntos y un monto de S/. 1350 para realizar la compra");
            }
        }
        else if (StipoC == "11") {
            if (Spuntos >= 150 & SprecioPago >= 525) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 150 puntos y un monto de S/. 525 para realizar la compra");
            }
        }
        else if (StipoC == "12") {
            if (Spuntos >= 400 & SprecioPago >= 1200) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 400 puntos y un monto de S/. 1200 para realizar la compra");
            }
        }
        else if (StipoC == "13") {
            if (Spuntos >= 250 & SprecioPago >= 750) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 750 para realizar la compra");
            }
        }
        else if (StipoC == "24") {
            if (Spuntos >= 950 & SprecioPago >= 2850) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 950 puntos y un monto de S/. 2850 para realizar la compra");
            }
        }
        else if (StipoC == "25") {
            if (Spuntos >= 900 & SprecioPago >= 2700) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 900 puntos y un monto de S/. 2700 para realizar la compra");
            }
        }
        else if (StipoC == "26") {
            if (Spuntos >= 750 & SprecioPago >= 2250) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 750 puntos y un monto de S/. 2250 para realizar la compra");
            }
        }
        else if (StipoC == "27") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        } else {
            validarRUC_Perfil(Ruc_Perfil, "CCUP");
        }
    }
    else {
        if (StipoC == "08") {
            if (Spuntos >= 50 & SprecioPago >= 187.50) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 187.50 para realizar la compra");
            }
        }
        else if (StipoC == "09") {
            if (Spuntos >= 200 & SprecioPago >= 700) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 200 puntos y un monto de S/. 700 para realizar la compra");
            }
        }
        else if (StipoC == "10") {
            if (Spuntos >= 450 & SprecioPago >= 1350) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 450 puntos y un monto de S/. 1350 para realizar la compra");
            }
        }
        else if (StipoC == "11") {
            if (Spuntos >= 150 & SprecioPago >= 525) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 150 puntos y un monto de S/. 525 para realizar la compra");
            }
        }
        else if (StipoC == "12") {
            if (Spuntos >= 400 & SprecioPago >= 1200) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 400 puntos y un monto de S/. 1200 para realizar la compra");
            }
        }
        else if (StipoC == "13") {
            if (Spuntos >= 250 & SprecioPago >= 750) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 750 para realizar la compra");
            }
        }
        else if (StipoC == "24") {
            if (Spuntos >= 950 & SprecioPago >= 2850) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 950 puntos y un monto de S/. 2850 para realizar la compra");
            }
        }
        else if (StipoC == "25") {
            if (Spuntos >= 900 & SprecioPago >= 2700) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 900 puntos y un monto de S/. 2700 para realizar la compra");
            }
        }
        else if (StipoC == "26") {
            if (Spuntos >= 750 & SprecioPago >= 2250) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 750 puntos y un monto de S/. 2250 para realizar la compra");
            }
        }
        else if (StipoC == "27") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        } else {
            CompraCanjeConsumo_Upgrade();
        }
    }
}

function ValidarPedidoPreRegistro() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Scomprob = $("#cboComprobante").val();
    var Stitular = $("#ddlTitularRUC").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (Scomprob == "0" || Scomprob == "" || Scomprob == null) {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Scomprob == "2" && (Stitular == "0" || Stitular == "" || Stitular == null)) {
        FaltaTitular();
    }
    else if (Scomprob == "2" && Stitular == "2" && RUC_Validado == 0) {
        ErrorRUC();
    }
    else if (Scomprob == "2" && Stitular == "1") {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                validarRUC_Perfil(Ruc_Perfil, "CCUP");
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            validarRUC_Perfil(Ruc_Perfil, "CCUP");
        }
    }
    else {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                CompraCanjeConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            CompraCanjeConsumo_Upgrade();
        }
    }
}

function ValidarPedidoPE() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Scomprob = $("#cboComprobante").val();
    var Stitular = $("#ddlTitularRUC").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (Scomprob == "0" || Scomprob == "" || Scomprob == null) {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Scomprob == "2" && (Stitular == "0" || Stitular == "" || Stitular == null)) {
        FaltaTitular();
    }
    else if (Scomprob == "2" && Stitular == "2" && RUC_Validado == 0) {
        ErrorRUC();
    }
    else if (Scomprob == "2" && Stitular == "1") {
        if (StipoC == "08") {
            if (Spuntos >= 50 & SprecioPago >= 187.50) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 187.50 para realizar la compra");
            }
        }
        else if (StipoC == "09") {
            if (Spuntos >= 200 & SprecioPago >= 700) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 200 puntos y un monto de S/. 700 para realizar la compra");
            }
        }
        else if (StipoC == "10") {
            if (Spuntos >= 450 & SprecioPago >= 1350) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 450 puntos y un monto de S/. 1350 para realizar la compra");
            }
        }
        else if (StipoC == "11") {
            if (Spuntos >= 150 & SprecioPago >= 525) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 150 puntos y un monto de S/. 525 para realizar la compra");
            }
        }
        else if (StipoC == "12") {
            if (Spuntos >= 400 & SprecioPago >= 1200) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 400 puntos y un monto de S/. 1200 para realizar la compra");
            }
        }
        else if (StipoC == "13") {
            if (Spuntos >= 250 & SprecioPago >= 750) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 750 para realizar la compra");
            }
        }
        else if (StipoC == "24") {
            if (Spuntos >= 950 & SprecioPago >= 2850) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 950 puntos y un monto de S/. 2850 para realizar la compra");
            }
        }
        else if (StipoC == "25") {
            if (Spuntos >= 900 & SprecioPago >= 2700) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 900 puntos y un monto de S/. 2700 para realizar la compra");
            }
        }
        else if (StipoC == "26") {
            if (Spuntos >= 750 & SprecioPago >= 2250) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 750 puntos y un monto de S/. 2250 para realizar la compra");
            }
        }
        else if (StipoC == "27") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        } else {
            validarRUC_Perfil(Ruc_Perfil, "PECUP");
        }
    }
    else {
        if (StipoC == "08") {
            if (Spuntos >= 50 & SprecioPago >= 187.50) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 187.50 para realizar la compra");
            }
        }
        else if (StipoC == "09") {
            if (Spuntos >= 200 & SprecioPago >= 700) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 200 puntos y un monto de S/. 700 para realizar la compra");
            }
        }
        else if (StipoC == "10") {
            if (Spuntos >= 450 & SprecioPago >= 1350) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 450 puntos y un monto de S/. 1350 para realizar la compra");
            }
        }
        else if (StipoC == "11") {
            if (Spuntos >= 150 & SprecioPago >= 525) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 150 puntos y un monto de S/. 525 para realizar la compra");
            }
        }
        else if (StipoC == "12") {
            if (Spuntos >= 400 & SprecioPago >= 1200) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 400 puntos y un monto de S/. 1200 para realizar la compra");
            }
        }
        else if (StipoC == "13") {
            if (Spuntos >= 250 & SprecioPago >= 750) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 750 para realizar la compra");
            }
        }
        else if (StipoC == "24") {
            if (Spuntos >= 950 & SprecioPago >= 2850) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 950 puntos y un monto de S/. 2850 para realizar la compra");
            }
        }
        else if (StipoC == "25") {
            if (Spuntos >= 900 & SprecioPago >= 2700) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 900 puntos y un monto de S/. 2700 para realizar la compra");
            }
        }
        else if (StipoC == "26") {
            if (Spuntos >= 750 & SprecioPago >= 2250) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 750 puntos y un monto de S/. 2250 para realizar la compra");
            }
        }
        else if (StipoC == "27") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else {
            CompraPEConsumo_Upgrade();
        }
    }
}

function ValidarPedidoPreRegistroPE() {
    var StipoC = $("#STipoCompra").val();
    var StipoP = $("#SMedioPago").val();
    var Stienda = $("#ComboTienda").val();
    var Scomprob = $("#cboComprobante").val();
    var Stitular = $("#ddlTitularRUC").val();
    var Spuntos = puntosCompraEval;
    var SprecioPago = montoCompraEval;

    if (StipoC == "0" || StipoP == "0" || Stienda == "0") {
        FaltanDatos();
    }
    else if (Scomprob == "0" || Scomprob == "" || Scomprob == null) {
        FaltanDatos();
    }
    else if (SprecioPago <= 0) {
        FaltanProductos();
    }
    else if (Scomprob == "2" && (Stitular == "0" || Stitular == "" || Stitular == null)) {
        FaltaTitular();
    }
    else if (Scomprob == "2" && Stitular == "2" && RUC_Validado == 0) {
        ErrorRUC();
    }
    else if (Scomprob == "2" && Stitular == "1") {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                validarRUC_Perfil(Ruc_Perfil, "PECUP");
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            validarRUC_Perfil(Ruc_Perfil, "PECUP");
        }
    }
    else {
        if (StipoC == "01") {
            if (Spuntos >= 50 & SprecioPago >= 200) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 50 puntos y un monto de S/. 200 para realizar la compra");
            }
        }
        else if (StipoC == "02") {
            if (Spuntos >= 100 & SprecioPago >= 375) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 100 puntos y un monto de S/. 375 para realizar la compra");
            }
        }
        else if (StipoC == "03") {
            if (Spuntos >= 250 & SprecioPago >= 875) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 250 puntos y un monto de S/. 875 para realizar la compra");
            }
        }
        else if (StipoC == "04") {
            if (Spuntos >= 500 & SprecioPago >= 1500) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 500 puntos y un monto de S/. 1500 para realizar la compra");
            }
        }
        else if (StipoC == "05") {
            if (SprecioPago >= 100) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "06") {
            if (SprecioPago >= 100) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita un monto de S/. 100 para realizar la compra");
            }
        }
        else if (StipoC == "23") {
            if (Spuntos >= 1000 & SprecioPago >= 3000) {
                CompraPEConsumo_Upgrade();
            } else {
                FaltaMontoUpgrade("Necesita 1000 puntos y un monto de S/. 3000 para realizar la compra");
            }
        }
        else {
            CompraPEConsumo_Upgrade();
        }
    }
}

function validarRUC_Perfil(numruc, tipoC) {
    var obj = JSON.stringify({
        ruc: numruc
    });
    $.ajax({
        type: "POST",
        url: "https://servicio.apirest.pe/api/getRuc",
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjgzMmUzYTI3ZDA0YzI2ZjFmNTIwNTJhMjE5ODRmOTI1NDY2ZmM1YjA2MzcyOWNiNDc4NjExM2VlYzRjZDA5NjY2OTRjNzEzY2U5YTNjNmMyIn0.eyJhdWQiOiIxIiwianRpIjoiODMyZTNhMjdkMDRjMjZmMWY1MjA1MmEyMTk4NGY5MjU0NjZmYzViMDYzNzI5Y2I0Nzg2MTEzZWVjNGNkMDk2NjY5NGM3MTNjZTlhM2M2YzIiLCJpYXQiOjE1ODk5Mjc5MTAsIm5iZiI6MTU4OTkyNzkxMCwiZXhwIjoxOTA1NDYwNzEwLCJzdWIiOiIxMDcyIiwic2NvcGVzIjpbIioiXX0.b_D5SKP8vifhZ38XY88NLKoO_0djb42S8KRqG76ZRLoSYI8f_Y4hEbGiyA0lmEk1miTd5I7_qbE7VPYIYpwy4CxaEN2lVJaqyTTnjupALsDiV0i2a5JMZ5kklXs-HdE6m7Ikovax-ZREcRMTKHxcpAIznYvcL-j3lutmlXRLoUVpRdyWFpVLGpWLagxhu7wHIrURW3ssGZPZCcv6hci3Xs9y8iJUKCt1AU1oNRBiwoL25O-4740jrQRzLLX2PP5KnHsFVNxy79y4LeC4zlhpPmpAuiJKi4KnHj4m3A0oBy9mql8rlCXzGGK9pyd8XjiO4gpiqa82xaYSawk6F6M6h8RkVAaEXZKeFuofDIQjKsp0QClw6egRA24TA-j_ZyvzDX5HzTG6y_1-H1zjPhSmVPH-3fsU8YZ52EFQcHJ28m3AaEDR9NOmaOgy-nLEIvAjunFMc67tO8A6f4I6QInnpcexW4S6FHcDT3Dws3v38BBCac31Pkmn4OKNl4ymIX37h-cztmBKC-tUDFI5Xssy26GxRPt-7NRlO-3BSU9HPEGke855TaigjnObSf6opHUxR9NjDzqVecsEAtNsz9cgozX_EqyGhRiSH2YwnYFOp-IU22v3SI1VNu0ijd263pUhVFfH1JM1OtqWVU8-vOLSC68tvFsF0ylS2HT3LO96U2Q');
        },
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            var error_mostrar = '';
            if (xhr.status === 0) {
                error_mostrar = ('Verificar conexión a internet');
                ErrorSunat(error_mostrar);
            } else if (xhr.status == 404) {
                error_mostrar = ('URL solicitada no encontrada [Error 404] - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (xhr.status == 500) {
                error_mostrar = ('Error interno del servidor [Error 500] - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (status === 'parsererror') {
                error_mostrar = ('JSON solicitado falló - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (status === 'timeout') {
                error_mostrar = ('Error en tiempo de espera - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else if (status === 'abort') {
                error_mostrar = ('Solicitud Ajax abortada - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            } else {
                error_mostrar = ('Error desconocido: - ' + error_especifico.Message);
                ErrorSunat(error_mostrar);
            }
        },
        success: function (data) {
            var success = data.success;
            if (success == true) {
                if (tipoC == "CCUP") {
                    CompraCanjeConsumo_Upgrade();
                }
                else if (tipoC == "PECUP") {
                    CompraPEConsumo_Upgrade();
                }
                else if (tipoC == "CCAF") {
                    CompraCanjeAfiliacion();
                } else {
                    CompraPEAfiliacion();
                }
            } else {
                ErrorRUC();
            }
        }
    });
}

function CompraCanjeConsumo_Upgrade() {

    var Stienda = $("#ComboTienda").val();
    var tiendaTx = $("#ComboTienda option:selected").text();
    var Scomprob = $("#cboComprobante").val();
    var comprobTx = $("#cboComprobante option:selected").text();
    var Stitular = $("#ddlTitularRUC").val();
    var StipoC = $("#STipoCompra").val();
    var tipoCTx = $("#STipoCompra option:selected").text();
    var StipoP = $("#SMedioPago").val();
    var Sruc = $("#TextBoxRUC").val();

    var obj = JSON.stringify({
        cTienda: Stienda, txTienda: tiendaTx, cComprobante: Scomprob, txComprobante: comprobTx,
        cTitularRUC: Stitular, idTCompra: StipoC, txTCompra: tipoCTx, cTipoPago: StipoP, txRUCCOM: Sruc
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/RealizarCompraConsumoCanje",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;

            if (Datos.Mensaje == "OK") {
                IrCompraTerminada();
            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: Datos.Mensaje,
                    type: "error"
                });
            }

        }
    });
}

function CompraPEConsumo_Upgrade() {

    var Stienda = $("#ComboTienda").val();
    var tiendaTx = $("#ComboTienda option:selected").text();
    var Scomprob = $("#cboComprobante").val();
    var comprobTx = $("#cboComprobante option:selected").text();
    var Stitular = $("#ddlTitularRUC").val();
    var StipoC = $("#STipoCompra").val();
    var tipoCTx = $("#STipoCompra option:selected").text();
    var StipoP = $("#SMedioPago").val();
    var Sruc = $("#TextBoxRUC").val();

    var obj = JSON.stringify({
        cTienda: Stienda, txTienda: tiendaTx, cComprobante: Scomprob, txComprobante: comprobTx,
        cTitularRUC: Stitular, idTCompra: StipoC, txTCompra: tipoCTx, cTipoPago: StipoP, txRUCCOM: Sruc
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/RealizarCompraConsumoPE",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;

            if (Datos.Mensaje == "OK") {
                IrCompraTerminada();
                Swal.fire({
                    title: 'Perfecto!',
                    text: 'Compra Realizada',
                    type: "success"
                }).then(function () {
                    window.open(Datos.URL, '_blank');
                });

            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: Datos.Mensaje,
                    type: "error"
                });
            }

        }
    });
}

function CompraCanjeAfiliacion() {

    var Stienda = $("#ComboTienda").val();
    var tiendaTx = $("#ComboTienda option:selected").text();
    var Scomprob = $("#cboComprobante").val();
    var comprobTx = $("#cboComprobante option:selected").text();
    var Stitular = $("#ddlTitularRUC").val();
    var StipoC = $("#STipoCompra").val();
    var tipoCTx = $("#STipoCompra option:selected").text();
    var StipoP = $("#SMedioPago").val();
    var Sruc = $("#TextBoxRUC").val();

    var obj = JSON.stringify({
        cTienda: Stienda, txTienda: tiendaTx, cComprobante: Scomprob, txComprobante: comprobTx,
        cTitularRUC: Stitular, idTCompra: StipoC, txTCompra: tipoCTx, cTipoPago: StipoP, txCorreo: $("#TxtCorreo").val(),
        txDocumento: $("#txtNumDocumento").val(), txUsuario: $("#txtUl").val(), txFechaNac: $("#datepicker").val(),
        txCDRPreferido: $("#cboTipoEstablecimiento").val(), txCDRPremio: $("#cboPremio").val(), txClave: $("#TxtCl").val(),
        txNombre: $("#txtNombre").val(), txApellidoPat: $("#txtApPaterno").val(), txApellidoMat: $("#txtApMaterno").val(),
        txSexo: $("#ComboSexo option:selected").text(), txTipoDoc: $("#ComboTipoDocumento option:selected").text(),
        txTelefono: $("#TxtTelefono").val(), txCelular: $("#TxtCelular").val(), cPais: $("#cboPais").val(),
        cDepartamento: $("#cboDepartamento").val(), cProvincia: $("#cboProvincia").val(), cDistrito: $("#cboDistrito").val(),
        txDireccion: $("#txtDireccion").val(), txReferencia: $("#TxtReferencia").val(), txDetraccion: $("#TxtNumCuenDetracciones").val(),
        txRUC: $("#TxtRUC").val(), txBanco: $("#TxtBanco").val(), txNumCuenta: $("#TxtNumCuenDeposito").val(),
        txInterbancaria: $("#TxtNumCuenInterbancaria").val(), cTCliente: $("#cboTipoCliente").val(), txUpline: $("#CboUpLine").val(), txRUCCOM: Sruc
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/RealizarCompraAfiliacionCanje",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;

            if (Datos.Mensaje == "OK") {
                IrCompraTerminada();
            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: Datos.Mensaje,
                    type: "error"
                });
            }

        }
    });
}

function CompraPEAfiliacion() {

    var Stienda = $("#ComboTienda").val();
    var tiendaTx = $("#ComboTienda option:selected").text();
    var Scomprob = $("#cboComprobante").val();
    var comprobTx = $("#cboComprobante option:selected").text();
    var Stitular = $("#ddlTitularRUC").val();
    var StipoC = $("#STipoCompra").val();
    var tipoCTx = $("#STipoCompra option:selected").text();
    var StipoP = $("#SMedioPago").val();
    var Sruc = $("#TextBoxRUC").val();

    var obj = JSON.stringify({
        cTienda: Stienda, txTienda: tiendaTx, cComprobante: Scomprob, txComprobante: comprobTx,
        cTitularRUC: Stitular, idTCompra: StipoC, txTCompra: tipoCTx, cTipoPago: StipoP, txCorreo: $("#TxtCorreo").val(),
        txDocumento: $("#txtNumDocumento").val(), txUsuario: $("#txtUl").val(), txFechaNac: $("#datepicker").val(),
        txCDRPreferido: $("#cboTipoEstablecimiento").val(), txCDRPremio: $("#cboPremio").val(), txClave: $("#TxtCl").val(),
        txNombre: $("#txtNombre").val(), txApellidoPat: $("#txtApPaterno").val(), txApellidoMat: $("#txtApMaterno").val(),
        txSexo: $("#ComboSexo option:selected").text(), txTipoDoc: $("#ComboTipoDocumento option:selected").text(),
        txTelefono: $("#TxtTelefono").val(), txCelular: $("#TxtCelular").val(), cPais: $("#cboPais").val(),
        cDepartamento: $("#cboDepartamento").val(), cProvincia: $("#cboProvincia").val(), cDistrito: $("#cboDistrito").val(),
        txDireccion: $("#txtDireccion").val(), txReferencia: $("#TxtReferencia").val(), txDetraccion: $("#TxtNumCuenDetracciones").val(),
        txRUC: $("#TxtRUC").val(), txBanco: $("#TxtBanco").val(), txNumCuenta: $("#TxtNumCuenDeposito").val(),
        txInterbancaria: $("#TxtNumCuenInterbancaria").val(), cTCliente: $("#cboTipoCliente").val(), txUpline: $("#CboUpLine").val(), txRUCCOM: Sruc
    });
    $.ajax({
        type: "POST",
        url: "DetalleDeCompra.aspx/RealizarCompraAfiliacionPE",
        data: obj,
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, throwError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);
        },
        success: function (dataS) {
            var Datos = dataS.d;

            if (Datos.Mensaje == "OK") {
                IrCompraTerminada();
                Swal.fire({
                    title: 'Perfecto!',
                    text: 'Compra Realizada',
                    type: "success"
                }).then(function () {
                    window.open(Datos.URL, '_blank');
                });
            } else {
                Swal.fire({
                    title: 'Ooops...!',
                    text: Datos.Mensaje,
                    type: "error"
                });
            }

        }
    });
}

function ErrorRUC() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'El RUC ingresado es inválido o está Inactivo',
    })
}
function FaltaUpline() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Debe seleccionar el Upline',
    })
}
function FaltanDatos() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Debe seleccionar todos los datos de compra',
    })
}
function FaltaTitular() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Debe seleccionar el titular de la compra',
    })
}
function ErrorSunat(mensaje) {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: mensaje,
    })
}
function FaltaMontoUpgrade(mensaje) {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: mensaje,
    })
}
function FaltaSeleccionar(mensaje) {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: mensaje,
    })
}
function FaltanProductos() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Debe agregar productos al carrito',
    })
}
function NoCambioPreRegistro() {
    Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Está completando un preregistro, no puede seleccionar esa opción',
    })
}